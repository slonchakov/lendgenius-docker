=== ShortyWP ===
Contributors: https://profiles.wordpress.org/kreydle
Donate link: http://www.shortywp.com/
Tags: shorten url, affiliate tracking, conversion tracking
Requires at least: 4.4.0
Tested up to: 4.9
Stable tag: 2.0.12
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Shorty is a WordPress plugin that helps marketers shorten long URLs,  track conversions, and monitor PPC costs.

== Description ==
Shorty is a WordPress plugin that helps marketers shorten long URLs, cloak affiliate links, track conversions, and monitor PPC costs.

To purchase a license for Shorty please visit http://www.shortywp.com

To retrieve your license code, view purchase details or renew your support, please visit: http://www.kreydle.com/clients/


== Installation ==

To install Shorty, you must have a self-hosted WordPress blog running WordPress version 4.3 or higher. 

1. Installing Via the WordPress Dashboard
==========================================

Download the ZIP file containing the Shorty plugin, and upload the entire zipped file to your WordPress blog using the “Upload Plugin” button in the Add Plugins page.

Activate the Shorty plugin. You will be prompted to enter your license code before the plugin can be used. 

1. Installing Via FTP
==========================================

Unzip the ZIP file, and upload the entire “shorty” folder to the wp-content/plugins folder. 

Make you you do not upload nested folders. The folder “shorty” should not contain another folder called “shorty”. 

Activate the Shorty plugin. You will be prompted to enter your license code before the plugin can be used. 

 
 == Frequently Asked Questions ==

Watch video tutorials: 
http://www.shortywp.com/tutorials/

Join our affiliate program: 
http://www.shortywp.com/affiliates

== Change Log ==
= V.2.0.12 =
* Remove encoded version

= V.2.0.11 =
* Encode php 7.1

= V.2.0.10 =
* Allow bing ads & adwords bot

= V.2.0.9 =
= V.2.0.8 =
= V.2.0.7 =
* Update shareasale affiliate tracking

= V.2.0.6 =
* add Cue Links affiliate tracking 

= V.2.0.5 =
* add %%CTID%% features

= V.2.0.4 =
* add Cue Links import 

= V.2.0.3 =
* add ClickBank import 

= v.2.0.2 =
* Update freegeoip path

= v.2.0.1 =
* Update description
* Update role

= v.2.0.0 =
*major update 
*New Features  - Split Testing

= v.1.4.2 =
*Improvement
*Bug fixing - referrer

= v.1.4.0 =
*Improvement
*Bug fixing

= v.1.2.6 =
*Improvement - Shorty's White Label

= v.1.2.5 =
*Improvement - Shorty's TinyMce button
*Hotfix - Create DB

= v.1.2.4 =
*Improvement - Detail support token info

= v.1.2.3 =
*Hotfix - Custom domain
 
= v.1.2.2 =
*Hotfix - Shareasale commision value & ctid
 
= v.1.2.1 =
*New Features - Custom Domain settings
*New Features - Import ShareASale conversion txt

= v.1.2.0 =
*Relaunch

= v.1.1.1 =
*New Features - Add GeoIp redirect
*New Features - Custom Cookies settings

= V.1.1.0 =
*New Features - Access Permission
*New Features - Backup & Reset
*New Features - Drill Down Report
*New Features - Dynamic parameter tagging
*Improvement - MISC update

= v.1.0.9 =
*Update unique id

= v.1.0.8 =
*Improvement - Goal log filter
*Hotfix - Goal page lookup


= v.1.0.7 =
*Improvement - Campaign Better route function
*Improvement - add demo mode capabilities
*Improvement - restructure import module
*New Features - Better menu positioning

= v.1.0.6 =
*Hotfix - Campaign validation

= v.1.0.5 =
*New Features - allow conversion report's goal edit
*Improvement - options is persistent

= v.1.0.4 =
*add autoupdate features
*Improvement - tinymce css
*Improvement - Typeahead validation improvement
*Hotfix - Autokeyword linking

= v.1.0.3 =
*Hotfix

=v.1.0.2=
*add url activation

=v.1.0.1=
*bug fixed on campaigns and goals module

=v.1.0.0=
*initial release
