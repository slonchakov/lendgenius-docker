<?php

class Srty_goals extends Srty_core {

    public $tbl_links;
    public $tbl_visits_log;
    public $tbl_conversions_log;

    public function __construct() {
        parent::__construct();
        $this->table_name = $this->wpdb->prefix . SH_PREFIX . 'goals';
        $this->tbl_links = $this->wpdb->prefix . SH_PREFIX . 'links';
        $this->tbl_visits_log = $this->wpdb->prefix . SH_PREFIX . 'visits_log';
        $this->tbl_conversions_log = $this->wpdb->prefix . SH_PREFIX . 'conversions_log';
    }

    public function init() {
        add_action('wp_ajax_srty_goals_datatable', array($this, 'ajax_datatable'));
        add_action('wp_ajax_srty_goals_delete', array($this, 'ajax_delete'));
        add_action('wp_ajax_srty_goals_typeahead', array($this, 'ajax_typeahead_pages'));
    }

    public function display() {
        $this->_check_license();
        add_action('admin_footer', array($this, 'page_js'));

        $this->view('v_goals', array('data' => 'hello world'));
    }

    public function add() {
        //disable add function since it is legacy
        wp_redirect('?page=sh_goals_page');
        exit;

        if (!$this->check_access(3)) {
            wp_redirect('?page=sh_goals_page');
            exit;
        }
        $this->_check_license();
        if (isset($_POST['btnAdd'])) {

            if ($this->_post('conversion_option') == 'on-site') {
                $this->gump->validation_rules(array(
                    'goal_name' => 'required',
                    'goal_type' => 'required',
                    'goal_value' => 'required|currency',
                    'goal_tracking_url' => 'required|post_url'
                ));
            } else {
                $this->gump->validation_rules(array(
                    'goal_name' => 'required',
                    'goal_type' => 'required',
                    'goal_value' => 'required|currency',
                ));
            }

            $this->gump->filter_rules(array(
                'goal_name' => 'trim|sanitize_string',
                'goal_type' => 'trim',
                'goal_value' => 'trim',
            ));

            $validated_data = $this->gump->run($_POST);
            if ($validated_data !== FALSE) {

                $goal_name = $this->_post('goal_name');
                $goal_type = $this->_post('goal_type');
                $goal_value = $this->_post('goal_value');

                if ($this->_post('conversion_option') == 'on-site') {
                    $reference_id = '';
                    $use_ssl_tracking = 0;
                    $page_id = $this->_post('page_id');
                    $goal_tracking_url = $this->_post('goal_tracking_url');
                } else {
                    $reference_id = $this->_post('reference_id');
                    $use_ssl_tracking = $this->_post('use_ssl_tracking', 0);
                    $goal_tracking_url = '';
                    $page_id = '';
                }

                $this->wpdb->insert(
                        $this->table_name, array(
                    'goal_name' => $goal_name,
                    'goal_type' => $goal_type,
                    'goal_value' => $goal_value,
                    'reference_id' => $reference_id,
                    'use_ssl_tracking' => $use_ssl_tracking,
                    'goal_tracking_url' => $goal_tracking_url,
                    'page_id' => $page_id,
                        )
                );

                $goal_id = $this->wpdb->insert_id;
                $this->view_data['msg'] = array(
                    'status' => 'alert-success',
                    'text' => SRTY_MSG_GOAL_ADDED
                );
                $this->set_top_message($this->view_data['msg']);
                wp_redirect('?page=sh_goals_page&action=edit&id=' . $goal_id);
                exit();
            } else {
                $this->view_data['error'] = $this->gump->get_errors_array();
                $this->view_data['msg'] = array(
                    'status' => 'alert-danger',
                    'text' => SRTY_MSG_GOAL_TOP_ERROR_MESSAGE
                );
            }
        }
        add_action('admin_footer', array($this, 'js_typeahead_goal'));

        $this->view('v_goal-new', $this->view_data);
    }

    public function edit($goal_id = FALSE) {
        $this->_check_license();
        $id = $_GET['id'];

        if (isset($_POST['btnEdit'])) {

            if ($this->_post('conversion_option') == 'on-site') {
                $this->gump->validation_rules(array(
                    'goal_name' => 'required',
                    'goal_type' => 'required',
                    'goal_value' => 'required|currency',
                    'goal_tracking_url' => 'required|post_url'
                ));
            } else {
                $this->gump->validation_rules(array(
                    'goal_name' => 'required',
                    'goal_type' => 'required',
                    'goal_value' => 'required|currency',
                ));
            }

            $this->gump->filter_rules(array(
                'goal_name' => 'trim|sanitize_string',
                'goal_type' => 'trim',
                'goal_value' => 'trim',
            ));

            $validated_data = $this->gump->run($_POST);
            if ($validated_data !== FALSE) {

                $goal_name = $this->_post('goal_name');
                $goal_type = $this->_post('goal_type');
                $goal_value = $this->_post('goal_value');

                if ($this->_post('conversion_option') == 'on-site') {
                    $reference_id = '';
                    $use_ssl_tracking = 0;
                    $page_id = $this->_post('page_id');
                    $goal_tracking_url = $this->_post('goal_tracking_url');
                } else {
                    $reference_id = $this->_post('reference_id');
                    $use_ssl_tracking = $this->_post('use_ssl_tracking', 0);
                    $goal_tracking_url = '';
                    $page_id = '';
                }

                $this->wpdb->update(
                        $this->table_name, array(
                    'goal_name' => $goal_name,
                    'goal_type' => $goal_type,
                    'goal_value' => $goal_value,
                    'reference_id' => $reference_id,
                    'use_ssl_tracking' => $use_ssl_tracking,
                    'goal_tracking_url' => $goal_tracking_url,
                    'page_id' => $page_id,
                        ), array('id' => $id)
                );

                $this->view_data['msg'] = array(
                    'status' => 'alert-success',
                    'text' => SRTY_MSG_GOAL_EDITED
                );
            } else {
                $this->view_data['error'] = $this->gump->get_errors_array();
                $this->view_data['msg'] = array(
                    'status' => 'alert-danger',
                    'text' => SRTY_MSG_GOAL_TOP_ERROR_MESSAGE
                );
            }
        }
        add_action('admin_footer', array($this, 'js_typeahead_goal'));
        $this->view_data['goal'] = $this->_by_id();
        $this->view('v_goal-edit', $this->view_data);
    }

    public function clone_page() {
        $this->_check_license();
        add_action('admin_footer', array($this, 'page_js'));
        $id = $_GET['id'];

        /**
         * get the existing data
         */
        $sql = "SELECT * FROM {$this->table_name} WHERE id = %d";
        $goal = $this->wpdb->get_row($this->wpdb->prepare($sql, array($id)), OBJECT);

        $this->wpdb->insert(
                $this->table_name, array(
            'goal_name' => $goal->goal_name,
            'goal_type' => $goal->goal_type,
            'goal_value' => $goal->goal_value,
            'reference_id' => $goal->reference_id,
            'use_ssl_tracking' => $goal->use_ssl_tracking,
            'goal_tracking_url' => $goal->goal_tracking_url,
            'page_id' => $goal->page_id,
                )
        );
        $cloned_id = $this->wpdb->insert_id;
        $this->view_data['msg'] = array(
            'status' => 'alert-success',
            'text' => SRTY_MSG_CLONE_LINK
        );
        $this->set_top_message($this->view_data['msg']);
        wp_redirect('?page=sh_goals_page&action=edit&id=' . $cloned_id);
        exit();
    }

    public function page_js() {
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                var oTable = jQuery('#v_goals').DataTable({
                    "dom": '<"report panel panel-default"<"panel-heading"<"form form-inline clearfix"<"pull-left"<"#bulk_action.form-group">><"pull-right"<"form-group"f><"form-group"l>>>> <"table-responsive"t><"panel-footer clearfix"<"pull-left"<"form-group text-muted"<"form-control-static"i>>><"pull-right"p>>>',
                    "language": {
                        "search": '',
                        "lengthMenu": '&nbsp;_MENU_',
                        "paginate": {
                            "previous": "«",
                            "next": "»"
                        },
                        "searchPlaceholder": 'Type to Search...',
                    },
                    "serverSide": true,
                    "ajax": {
                        "url": ajaxurl,
                        "type": "POST",
                        "data": function (d) {
                            return jQuery.extend({}, d, {
                                "action": 'srty_goals_datatable'
                            });
                        },
                    },
                    "order": [[0, "desc"]],
                    "columnDefs": [
                        {
                            "targets": 0,
                            "orderable": false
                        },
                        {
                            "targets": 4,
                            "orderable": false
                        },
                    ]
                });
                jQuery('#bulk_action').html('<select name="slcAction" class="form-control input-sm slcAction"><option>Bulk Actions</option><option>Delete</option></select> <button data-action="srty_goals_delete" class="btnAction btn btn-default btn-sm" type="submit">Apply</button>');
            });

        </script>
        <?php

    }

    /**
     * ajax call
     */
    public function ajax_datatable() {
        wp_send_json($this->_datatable_get_all());
        wp_die();
    }

    public function ajax_delete() {

        $this->_delete_batch($this->explode_trim($_POST['ids']));
        $this->view_data['msg'] = array(
            'status' => 'alert-success',
            'text' => SRTY_MSG_GOAL_DELETED
        );
        $this->set_top_message($this->view_data['msg']);
        wp_send_json(array('result' => 1));
        wp_die();
    }

    public function ajax_typeahead_pages() {
        global $wpdb;

        $search = isset($_REQUEST['q']) ? $_REQUEST['q'] : '';
        if ($search == '') {
            $sql = "SELECT * FROM {$wpdb->posts} WHERE post_status = 'publish' AND (post_type = 'page' OR post_type = 'post');";
            $pages = $wpdb->get_results($sql, OBJECT);
        } else {
            $sql = "SELECT * FROM {$wpdb->posts} WHERE post_status = 'publish' AND (post_type = 'page' OR post_type = 'post') AND (post_title LIKE %s OR post_name LIKE %s);";
            $pages = $wpdb->get_results($this->wpdb->prepare($sql, array($search . '%', '%' . $search . '%')), OBJECT);
        }

        $result = array();
        foreach ($pages as $page) {
            $result[] = array(
                'id' => $page->ID,
                'post_title' => $page->post_title,
                'url' => get_page_link($page->ID),
            );
        }
        wp_send_json($result);
        wp_die();
    }

    /**
     * 
     * @global type $wpdb
     * @return type
     */
    private function _by_id($id = FALSE) {
        if ($id === FALSE) {
            $id = isset($_GET['id']) ? $_GET['id'] : -1;
        }

        return $this->wpdb->get_row($this->wpdb->prepare("SELECT * FROM $this->table_name WHERE id=%d", $id));
    }

    private function _datatable_get_all() {
        $params = array();

        $where = '';
        if (isset($_POST['search'])) {
            $_search = $_POST['search'];
            if (trim($_search['value']) != '') {
                $where = " WHERE ( id LIKE %s OR goal_name LIKE %s OR goal_value LIKE %s )";
                $params = array_merge($params, array('%%' . $_search['value'] . '%%', '%%' . $_search['value'] . '%%', '%%' . $_search['value'] . '%%'));
            }
        }

        $order = $this->_order($params);
        $limit = $this->_limit($params);

        $results = $this->wpdb->get_results($this->wpdb->prepare("SELECT SQL_CALC_FOUND_ROWS * FROM {$this->table_name} {$where} {$order} {$limit}", $params));
        $found = $this->wpdb->get_row("SELECT FOUND_ROWS() AS total;");
        $record = $this->wpdb->get_row("SELECT COUNT(id) AS total FROM $this->table_name;");

        $data = array();
        foreach ($results as $row) {
            $data[] = array(
                '<input type="checkbox" name="cbAction[]" value="' . $row->id . '">',
                $row->id,
                $row->goal_name,
                $row->goal_value,
                '<div class="btn-group btn-group-xs"><a href="?page=sh_goals_page&action=edit&id=' . $row->id . '" class="btn btn-default">edit</a>' .
//                '<div class="btn-group btn-group-xs"><a href="?page=sh_goals_page&action=clone&id=' . $row->id . '" class="btn btn-default">clone</a>' .
                (($this->check_access(3)) ? '<a href="?page=sh_goals_page&action=delete&id=' . $row->id . '" class="btn btn-default confirm">delete</a>' : '') .
                '</div>',
            );
        }

        return array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => intval($found->total),
            "recordsFiltered" => intval($found->total),
            "data" => $data
        );
    }

    public function display_page() {
        $args = array(
            'authors' => '',
            'child_of' => 0,
            'date_format' => get_option('date_format'),
            'depth' => 0,
            'echo' => 1,
            'exclude' => '',
            'include' => '',
            'link_after' => '',
            'link_before' => '',
            'post_type' => 'page',
            'post_status' => 'publish',
            'show_date' => '',
            'sort_column' => 'menu_order, post_title',
            'sort_order' => '',
            'title_li' => __('Pages'),
            'walker' => new Walker_Page
        );
        return $this->view_data['wp_page'] = wp_list_pages($args);
    }

    public function js_typeahead_goal() {
        if (wp_script_is('jquery', 'done')) {
            ?>
            <script type="text/javascript">
                jQuery(document).ready(function () {
                    function prepare(query, settings) {
                        settings.url = ajaxurl + '?action=srty_goals_typeahead&q=' + query;
                        settings.type = 'POST';
                        return settings;
                    }
                    var engine = new Bloodhound({
                        name: 'wp_page',
                        remote: {
                            url: ajaxurl,
                            prepare: prepare,
                        },
                        datumTokenizer: function (d) {
                            return Bloodhound.tokenizers.whitespace(d.val);
                        },
                        queryTokenizer: Bloodhound.tokenizers.whitespace
                    });

                    engine.initialize();
                    engine.get('');


                    jQuery('.typeahead').typeahead({
                        hint: false,
                        highlight: false,
                        limit: 20,
                        minLength: 0,
                    }, {
                        name: 'goal_tracking_url',
                        displayKey: 'url',
                        templates: {
                            suggestion: Handlebars.compile('<p style="font-size:14px;"><strong>{{post_title}}</strong> – {{url}}</p>')
                        },
                        // `ttAdapter` wraps the suggestion engine in an adapter that
                        // is compatible with the typeahead jQuery plugin
                        source: engine.ttAdapter(),
                    });

                    var idSelectedHandler = function (eventObject, suggestionObject, suggestionDataset) {
                        console.log(suggestionObject.id);
                        jQuery('#page_id').val(suggestionObject.id);
                    };
                    jQuery('.typeahead').on('typeahead:selected', idSelectedHandler);

                    jQuery('#conversion_option').change(function () {
                        //                        jQuery('.collapse').collapse('hide');
                        jQuery('.collapse').hide();
                        //                        jQuery('#' + jQuery(this).val()).collapse('show');
                        jQuery('#' + jQuery(this).val()).show();
                    }).trigger('change');

                });
            </script>
            <?php

        }
    }

    public function form_validation_js() {
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                var $form = $("#frmNewGoal");
                $form.validate({
                    errorElement: "span",
                    errorClass: "has-error",
                    validClass: "has-success",
                    highlight: function (element, errorClass, validClass) {
                        $(element).closest('.form-group').removeClass(validClass).addClass(errorClass);
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        $(element).closest('.form-group').removeClass(errorClass).addClass(validClass);
                        $(element).closest('span').remove();
                    },
                    success: function (label, errorClass, validClass) {
                        $(label).closest('.form-group').removeClass(errorClass).addClass(validClass);
                        $(label).html(" ").addClass("checked").hide();// set   as text for IE
                        $(label).closest('span').remove();
                    },
                    errorPlacement: function (error, element) {
                        $(error).insertAfter(element.closest('.input-group'));
                    }
                });

            });

        </script>
        <?php

    }

    public function conversion() {
        if (!is_admin()) {
            $request_uri = preg_replace('#/$#', '', urldecode($_SERVER['REQUEST_URI']));
            $url = get_site_url() . rtrim(trim(strtok($_SERVER["REQUEST_URI"], '?')), '/') . '/';
            $click_id = isset($_COOKIE['click_id']) ? $_COOKIE['click_id'] : 0;
            $crc32 = crc32($url . '-' . $click_id);

            if ((bool) get_option(SH_PREFIX . 'is_demo_mode') || in_array(get_option(SH_PREFIX . 'license_scheme_id'), array(SHORTY_STANDARD_LICENSE, SHORTY_PRO_LICENSE, SHORTY_AGENCY_LICENSE, AUDIENCEPRESS_LITE, AUDIENCEPRESS_UNLIMITED, AUDIENCEPRESS_DEVELOPER, AUDIENCEPRESS_WHITE_LABEL))) {
                $this->postback($crc32);
            }
            $this->goal_by_url($crc32);
            $this->goal_by_pixel($crc32);

            return 1;
        }
    }

    private function postback($crc32) {
        $request_uri = preg_replace('#/$#', '', urldecode($_SERVER['REQUEST_URI']));

        $site_url_subfolder = parse_url(site_url());
        $site_url_subfolder = ltrim($site_url_subfolder['path'], '/');
        $site_url_subfolder = trim($site_url_subfolder) != '' ? $site_url_subfolder . '/' : '';

        preg_match('#^/' . $site_url_subfolder . 'srty/postback#', $request_uri, $match);
        if (isset($match[0]) && ($match[0] == (trim($site_url_subfolder) != '' ? '/' . rtrim($site_url_subfolder, '/') : '') . '/srty/postback')) {
//        if (preg_match('#^/srty/postback#', $request_uri, $match) && $match[0] == '/srty/postback') {

            if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
                $_SERVER['HTTPS'] = 'on';
            }
            $http = (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on')) ? 'https://' : 'http://';

            header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
            header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
            header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
            header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
//            header("Content-type: image/gif");
            $gn = $this->_get('gn');
            $gt = $this->_get('gt');
            $gv = $this->_get('gv');
            $rid = $this->_get('rid');
            $click_id = $this->_get('ctid');

            /**
             * check visitor log
             */
            if ($click_id > 0) {
                $sql = "SELECT id,link_id FROM {$this->tbl_visits_log} WHERE id = %d";
                $visitor_log = $this->wpdb->get_row($this->wpdb->prepare($sql, array($click_id)), OBJECT);
            }

            if (isset($visitor_log->id)) {
                $status = STATUS_ACCEPTED;
                $message = 'Postback URL' . PHP_EOL;

                if (get_option(SH_PREFIX . 'settings_duplicate_handling') == DUPLICATE_HANDLING_IGNORE) {
                    if (trim($rid) != '') {
                        $sql = "SELECT COUNT(*) AS total FROM {$this->tbl_conversions_log} WHERE goal_reference = %s";
                        $conversion_log = $this->wpdb->get_row($this->wpdb->prepare($sql, array($rid)), OBJECT);
                        if ($conversion_log->total > 0) {
                            $status = STATUS_REJECTED;
                            $message .= 'Duplicate Goal Reference ID ' . $rid . PHP_EOL;
                        }
                    }
                }

                if (get_option(SH_PREFIX . 'settings_duplicate_click_id_handling') == DUPLICATE_HANDLING_IGNORE) {
                    if (trim($click_id) != '') {
                        $sql = "SELECT COUNT(*) AS total FROM {$this->tbl_conversions_log} WHERE visits_log_id = %d";
                        $conversion_log = $this->wpdb->get_row($this->wpdb->prepare($sql, array($click_id)), OBJECT);
                        if ($conversion_log->total > 0) {
                            $status = STATUS_REJECTED;
                            $message .= 'Duplicate Goal Click ID ' . $click_id . PHP_EOL;
                        }
                    }
                }

                /**
                 * check for duplicate 
                 */
//                if (trim($rid) == '') {
//                    $sql = "SELECT COUNT(*) AS total FROM {$this->tbl_conversions_log} WHERE goal_reference = '' AND visits_log_id=%d AND goal_name = %s AND goal_type = %s AND goal_value = %.2f ";
//                    $conversion_log = $this->wpdb->get_row($this->wpdb->prepare($sql, array(isset($visitor_log->id) ? $visitor_log->id : 0, $gn, $gt, $gv)), OBJECT);
//                    if ($conversion_log->total > 0) {
//                        $status = STATUS_REJECTED;
//                        $message .= 'Duplicate CTID' . $rid . PHP_EOL;
//                    }
//                }


                $country = $this->ip_info();
                $this->wpdb->insert(
                        $this->tbl_conversions_log, array(
                    'link_id' => isset($visitor_log->link_id) ? $visitor_log->link_id : 0,
                    'goal_id' => 0,
                    'visits_log_id' => isset($visitor_log->id) ? $visitor_log->id : 0,
                    'goal_name' => $gn,
                    'goal_value' => $gv,
                    'goal_type' => $gt,
                    'goal_reference' => $rid,
                    'referrer_url' => isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '',
                    'ip_address' => $_SERVER['REMOTE_ADDR'],
                    'ip_country_code' => isset($country['country_code']) ? $country['country_code'] : '',
                    'ip_country_name' => isset($country['country']) ? $country['country'] : '',
                    'ip_city_name' => isset($country['city']) ? $country['city'] : '',
                    'ip_latitude' => isset($country['latitude']) ? $country['latitude'] : '',
                    'ip_longitude' => isset($country['longitude']) ? $country['longitude'] : '',
                    'user_agent_string' => $_SERVER['HTTP_USER_AGENT'],
                    'status' => $status,
                    'message' => $message,
                    'conversion_date' => current_time("Y-m-d H:i:s"),
                    'created_date' => current_time("Y-m-d H:i:s"),
                        )
                );
                echo 'OK';
                exit;
            } else {
                echo 'Invalid CTID';
                exit;
            }
        }
    }

    private function goal_by_pixel($crc32) {
        $site_url_subfolder = parse_url(site_url());
        $site_url_subfolder = ltrim($site_url_subfolder['path'], '/');
        $site_url_subfolder = trim($site_url_subfolder) != '' ? $site_url_subfolder . '/' : '';
        $request_uri = preg_replace('#/$#', '', urldecode($_SERVER['REQUEST_URI']));

        preg_match('#^/' . $site_url_subfolder . 'srty/pixel#', $request_uri, $match);

        if (isset($match[0]) && ($match[0] == (trim($site_url_subfolder) != '' ? '/' . rtrim($site_url_subfolder, '/') : '') . '/srty/pixel')) {
            if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
                $_SERVER['HTTPS'] = 'on';
            }
            $http = (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on')) ? 'https://' : 'http://';

            header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
            header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
            header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
            header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
            header("Content-type: image/gif");

            $gn = $this->_get('gn');
            $gt = $this->_get('gt');
            $gv = $this->_get('gv');
            $rid = $this->_get('rid');

            /**
             * check visitor log
             */
            $click_id = isset($_COOKIE['click_id']) ? $_COOKIE['click_id'] : $click_id;
            if ($click_id > 0) {
                $sql = "SELECT id,link_id FROM {$this->tbl_visits_log} WHERE id = %d";
                $visitor_log = $this->wpdb->get_row($this->wpdb->prepare($sql, array($click_id)), OBJECT);
            }

            if (isset($visitor_log->id)) {
                $status = STATUS_ACCEPTED;
                $message = '';

                if (get_option(SH_PREFIX . 'settings_duplicate_handling') == DUPLICATE_HANDLING_IGNORE) {
                    if (trim($rid) != '') {
                        $sql = "SELECT COUNT(*) AS total FROM {$this->tbl_conversions_log} WHERE goal_reference = %s";
                        $conversion_log = $this->wpdb->get_row($this->wpdb->prepare($sql, array($rid)), OBJECT);
                        if ($conversion_log->total > 0) {
                            $status = STATUS_REJECTED;
                            $message .= 'Duplicate Goal Reference ID ' . $rid . PHP_EOL;
                        }
                    }
                }

                if (get_option(SH_PREFIX . 'settings_duplicate_click_id_handling') == DUPLICATE_HANDLING_IGNORE) {
                    if (trim($click_id) != '') {
                        $sql = "SELECT COUNT(*) AS total FROM {$this->tbl_conversions_log} WHERE visits_log_id = %d";
                        $conversion_log = $this->wpdb->get_row($this->wpdb->prepare($sql, array($click_id)), OBJECT);
                        if ($conversion_log->total > 0) {
                            $status = STATUS_REJECTED;
                            $message .= 'Duplicate Goal Click ID ' . $click_id . PHP_EOL;
                        }
                    }
                }

                /**
                 * check for duplicate 
                 */
//                if (trim($rid) == '') {
//                    $sql = "SELECT COUNT(*) AS total FROM {$this->tbl_conversions_log} WHERE goal_reference = '' AND visits_log_id=%d AND referrer_url=%s AND ip_address = %s ";
//                    $conversion_log = $this->wpdb->get_row($this->wpdb->prepare($sql, array(isset($visitor_log->id) ? $visitor_log->id : 0, isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '', $_SERVER['REMOTE_ADDR'])), OBJECT);
//                    if ($conversion_log->total > 0) {
//                        $status = STATUS_REJECTED;
//                        $message .= 'Duplicate CTID and IP address for this goal' . $rid . PHP_EOL;
//                    }
//                }


                $country = $this->ip_info();
                $this->wpdb->insert(
                        $this->tbl_conversions_log, array(
                    'link_id' => isset($visitor_log->link_id) ? $visitor_log->link_id : 0,
                    'goal_id' => 0,
                    'visits_log_id' => isset($visitor_log->id) ? $visitor_log->id : 0,
                    'goal_name' => $gn,
                    'goal_value' => $gv,
                    'goal_type' => $gt,
                    'goal_reference' => $rid,
                    'referrer_url' => isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '',
                    'ip_address' => $_SERVER['REMOTE_ADDR'],
                    'ip_country_code' => isset($country['country_code']) ? $country['country_code'] : '',
                    'ip_country_name' => isset($country['country']) ? $country['country'] : '',
                    'ip_city_name' => isset($country['city']) ? $country['city'] : '',
                    'ip_latitude' => isset($country['latitude']) ? $country['latitude'] : '',
                    'ip_longitude' => isset($country['longitude']) ? $country['longitude'] : '',
                    'user_agent_string' => $_SERVER['HTTP_USER_AGENT'],
                    'status' => $status,
                    'message' => $message,
                    'conversion_date' => current_time("Y-m-d H:i:s"),
                    'created_date' => current_time("Y-m-d H:i:s"),
                        )
                );
            }
            $fp = fopen("php://output", "wb");
            fwrite($fp, "GIF89a\x01\x00\x01\x00\x80\x00\x00\xFF\xFF", 15);
            fwrite($fp, "\xFF\x00\x00\x00\x21\xF9\x04\x01\x00\x00\x00\x00", 12);
            fwrite($fp, "\x2C\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02\x02", 12);
            fwrite($fp, "\x44\x01\x00\x3B", 4);
            fclose($fp);
            exit;
        }
    }

    private function goal_by_url($crc32) {
        $request_uri = preg_replace('#/$#', '', urldecode($_SERVER['REQUEST_URI']));

        $url = get_site_url() . rtrim(trim(strtok($_SERVER["REQUEST_URI"], '?')), '/') . '/';
        $sql = "SELECT * FROM {$this->table_name} WHERE goal_tracking_url = %s ";
        $goal = $this->wpdb->get_row($this->wpdb->prepare($sql, array($url)), OBJECT);
        if (isset($goal->id)) {

            /**
             * check visitor log
             */
            $click_id = isset($_COOKIE['click_id']) ? $_COOKIE['click_id'] : 0;
            if ($click_id > 0) {
                $sql = "SELECT id,link_id FROM {$this->tbl_visits_log} WHERE id = %d";
                $visitor_log = $this->wpdb->get_row($this->wpdb->prepare($sql, array($click_id)), OBJECT);
            }
            if (isset($visitor_log->id)) {
                $status = STATUS_ACCEPTED;
                $message = '';

                if (get_option(SH_PREFIX . 'settings_duplicate_handling') == DUPLICATE_HANDLING_IGNORE) {
                    if (trim($goal->reference_id) != '') {
                        $sql = "SELECT COUNT(*) AS total FROM {$this->tbl_conversions_log} WHERE goal_reference = %s";
                        $conversion_log = $this->wpdb->get_row($this->wpdb->prepare($sql, array($goal->reference_id)), OBJECT);
                        if ($conversion_log->total > 0) {
                            $status = STATUS_REJECTED;
                            $message .= 'Duplicate Goal Reference ID ' . $goal->reference_id . PHP_EOL;
                        }
                    }
                }

                if (get_option(SH_PREFIX . 'settings_duplicate_click_id_handling') == DUPLICATE_HANDLING_IGNORE) {
                    if (trim($click_id) != '') {
                        $sql = "SELECT COUNT(*) AS total FROM {$this->tbl_conversions_log} WHERE visits_log_id = %d";
                        $conversion_log = $this->wpdb->get_row($this->wpdb->prepare($sql, array($click_id)), OBJECT);
                        if ($conversion_log->total > 0) {
                            $status = STATUS_REJECTED;
                            $message .= 'Duplicate Goal Click ID ' . $click_id . PHP_EOL;
                        }
                    }
                }

                /**
                 * check for duplicate 
                 */
//                if (trim($goal->reference_id) == '') {
//                    $sql = "SELECT COUNT(*) AS total FROM {$this->tbl_conversions_log} WHERE goal_reference = '' AND visits_log_id=%d AND referrer_url=%s AND ip_address = %s ";
//                    $conversion_log = $this->wpdb->get_row($this->wpdb->prepare($sql, array(isset($visitor_log->id) ? $visitor_log->id : 0, $url, $_SERVER['REMOTE_ADDR'])), OBJECT);
//                    if ($conversion_log->total > 0) {
//                        $status = STATUS_REJECTED;
//                        $message .= 'Duplicate CTID and IP address for this goal' . PHP_EOL;
//                    }
//                }

                $country = $this->ip_info();
                $this->wpdb->insert(
                        $this->tbl_conversions_log, array(
                    'link_id' => isset($visitor_log->link_id) ? $visitor_log->link_id : 0,
                    'goal_id' => $goal->id,
                    'visits_log_id' => isset($visitor_log->id) ? $visitor_log->id : 0,
                    'goal_name' => $goal->goal_name,
                    'goal_value' => $goal->goal_value,
                    'goal_type' => $goal->goal_type,
                    'goal_reference' => $goal->reference_id,
                    'referrer_url' => $url, //isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '',
                    'ip_address' => $_SERVER['REMOTE_ADDR'],
                    'ip_country_code' => isset($country['country_code']) ? $country['country_code'] : '',
                    'ip_country_name' => isset($country['country']) ? $country['country'] : '',
                    'ip_city_name' => isset($country['city']) ? $country['city'] : '',
                    'ip_latitude' => isset($country['latitude']) ? $country['latitude'] : '',
                    'ip_longitude' => isset($country['longitude']) ? $country['longitude'] : '',
                    'user_agent_string' => $_SERVER['HTTP_USER_AGENT'],
                    'status' => $status,
                    'message' => $message,
                    'conversion_date' => current_time("Y-m-d H:i:s"),
                    'created_date' => current_time("Y-m-d H:i:s"),
                        )
                );
            }
        }
    }

}
