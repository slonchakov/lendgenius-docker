<?php

class Srty_split_tests extends Srty_core {

    public function __construct() {
        parent::__construct();
        $this->table_name = $this->wpdb->prefix . SH_PREFIX . 'split_tests';
        $this->tbl_links = $this->wpdb->prefix . SH_PREFIX . 'links';
        $this->tbl_split_test_allocations = $this->wpdb->prefix . SH_PREFIX . 'split_test_allocations';
        $this->tbl_visits_log = $this->wpdb->prefix . SH_PREFIX . 'visits_log';
        $this->tbl_conversions_log = $this->wpdb->prefix . SH_PREFIX . 'conversions_log';
        $this->view_data['currency'] = new Srty_currency();
    }

    public function init() {
        add_action('wp_ajax_srty_split_test_datatable', array($this, 'ajax_datatable'));
        add_action('wp_ajax_srty_split_test_delete', array($this, 'ajax_delete'));
    }

    public function display() {
        $this->_check_license();
        add_action('admin_footer', array($this, 'page_js'));
        wp_cache_delete(SH_PREFIX . 'cache_the_content');
        $this->view('v_split_tests', $this->view_data);
    }

    public function add() {
        if (!$this->check_access(3)) {
            wp_redirect('?page=' . SH_MENU_SLUG);
            exit;
        }

        $this->_check_license();
        add_action('admin_footer', array($this, 'page_js'));
        $this->view_data['allocation_fields_count'] = $this->_post('allocation_fields_count', 2);

        if (isset($_POST['btnAdd'])) {

            wp_cache_delete(SH_PREFIX . 'cache_the_content');



            $rules = array(
                'split_test_name' => 'required',
                'tracking_link' => 'required|alpha_dash_slash|split_test_tracking_link',
            );

            $allocation_fields_count = $this->_post('allocation_fields_count', 1);
            for ($i = 1; $i <= $allocation_fields_count; $i++) {
                $rules['traffic_' . $i] = 'required|integer|total_percentage,traffic_;' . $allocation_fields_count;
                $rules['tracker_' . $i] = 'required|tracking_link_exist';
            }

            $this->gump->validation_rules($rules);

            $this->gump->filter_rules(array(
                'split_test_name' => 'trim|sanitize_string',
                'tracking_link' => 'trim',
            ));
            $validated_data = $this->gump->run($_POST);

            if ($validated_data !== FALSE) {
                $this->wpdb->insert(
                        $this->table_name, array(
                    'split_test_name' => $this->_post('split_test_name'),
                    'tracking_link' => $this->_post('tracking_link'),
                        )
                );

                $split_test_id = $this->wpdb->insert_id;
                if ($split_test_id > 0) {
                    /**
                     * insert traffic allocation
                     */
                    $score = array(
                        'next_index' => 0,
                        'skip_list' => array(),
                        'total_count' => 0,
                        'allocation' => array(
                            'link_id' => 0,
                            'traffic' => 0,
                            'count' => 0
                        )
                    );

                    $allocation = array();
                    for ($i = 1; $i <= $allocation_fields_count; $i++) {
                        $link_id = $this->get_link_id($this->_post('tracker_' . $i));
                        $traffic_allocation = array(
                            'split_test_id' => $split_test_id,
                            'link_id' => $link_id,
                            'tracker_url' => $this->_post('tracker_' . $i),
                            'traffic' => $this->_post('traffic_' . $i),
                        );
                        $this->wpdb->insert($this->tbl_split_test_allocations, $traffic_allocation);

                        $allocation[] = array(
                            'link_id' => $link_id,
                            'traffic' => $traffic_allocation['traffic'],
                            'count' => 0
                        );
                    }

                    $score = array(
                        'next_index' => 0,
                        'skip_list' => array(),
                        'total_count' => 0,
                        'allocation' => $allocation
                    );

                    $this->wpdb->update($this->table_name, array('score' => json_encode($score)), array('id' => $split_test_id));

                    $this->view_data['msg'] = array(
                        'status' => 'alert-success',
                        'text' => SRTY_MSG_SPLIT_TEST_ADDED
                    );
                    $this->set_top_message($this->view_data['msg']);
                    wp_redirect('?page=sh_split_tests&action=edit&id=' . $split_test_id);
                    exit();
                } else {
                    $this->view_data['error'] = $this->gump->get_errors_array();
                    $this->view_data['msg'] = array(
                        'status' => 'alert-danger',
                        'text' => SRTY_MSG_SPLIT_TEST_TOP_ERROR_MESSAGE
                    );
                }
            } else {
                $this->view_data['error'] = $this->gump->get_errors_array();
                $this->view_data['msg'] = array(
                    'status' => 'alert-danger',
                    'text' => SRTY_MSG_SPLIT_TEST_TOP_ERROR_MESSAGE
                );
            }
        }

        add_action('admin_footer', array($this, 'js_typeahead_split_test'));
        $this->view_data['tracking_link'] = $this->generate_random_letters();
        $this->view('v_split_test-new', $this->view_data);
    }

    public function edit() {
        $this->_check_license();
        add_action('admin_footer', array($this, 'page_js'));
        $split_test_id = $_GET['id'];
        $this->view_data['allocation_fields_count'] = $this->_post('allocation_fields_count', 2);

        if (isset($_POST['btnEdit'])) {
            wp_cache_delete(SH_PREFIX . 'cache_the_content');

            $rules = array(
                'split_test_name' => 'required',
                'tracking_link' => 'required|alpha_dash_slash|split_test_tracking_link,' . $split_test_id,
            );

            $allocation_fields_count = $this->_post('allocation_fields_count', 1);
            for ($i = 1; $i <= $allocation_fields_count; $i++) {
                $rules['traffic_' . $i] = 'required|integer|total_percentage,traffic_;' . $allocation_fields_count;
                $rules['tracker_' . $i] = 'required|tracking_link_exist';
            }

            $this->gump->validation_rules($rules);

            $this->gump->filter_rules(array(
                'split_test_name' => 'trim|sanitize_string',
                'tracking_link' => 'trim',
            ));
            $validated_data = $this->gump->run($_POST);
            if ($validated_data !== FALSE) {

                $this->wpdb->update(
                        $this->table_name, array(
                    'split_test_name' => $this->_post('split_test_name'),
                    'tracking_link' => $this->_post('tracking_link'),
                        ), array('id' => $split_test_id)
                );

                /**
                 * delete previous split test allocation records
                 */
                $sql = "DELETE FROM {$this->tbl_split_test_allocations} WHERE split_test_id = %d;";
                $this->wpdb->get_results($this->wpdb->prepare($sql, array($split_test_id)), OBJECT);


                /**
                 * insert traffic allocation
                 */
                $score = array(
                    'next_index' => 0,
                    'skip_list' => array(),
                    'total_count' => 0,
                    'allocation' => array(
                        'link_id' => 0,
                        'traffic' => 0,
                        'count' => 0
                    )
                );

                $allocation = array();
                for ($i = 1; $i <= $allocation_fields_count; $i++) {
                    $link_id = $this->get_link_id($this->_post('tracker_' . $i));
                    $traffic_allocation = array(
                        'split_test_id' => $split_test_id,
                        'link_id' => $link_id,
                        'tracker_url' => $this->_post('tracker_' . $i),
                        'traffic' => $this->_post('traffic_' . $i),
                    );
                    $this->wpdb->insert($this->tbl_split_test_allocations, $traffic_allocation);

                    $allocation[] = array(
                        'link_id' => $link_id,
                        'traffic' => $traffic_allocation['traffic'],
                        'count' => 0
                    );
                }

                $score = array(
                    'next_index' => 0,
                    'skip_list' => array(),
                    'total_count' => 0,
                    'allocation' => $allocation
                );

                $this->wpdb->update($this->table_name, array('score' => json_encode($score)), array('id' => $split_test_id));

                $this->view_data['msg'] = array(
                    'status' => 'alert-success',
                    'text' => SRTY_MSG_SPLIT_TEST_EDITED
                );
            } else {
                $this->view_data['error'] = $this->gump->get_errors_array();
                $this->view_data['msg'] = array(
                    'status' => 'alert-danger',
                    'text' => SRTY_MSG_SPLIT_TEST_TOP_ERROR_MESSAGE
                );
            }
        }
        add_action('admin_footer', array($this, 'js_typeahead_split_test'));
        $this->view_data['split_test'] = $this->_by_id();
        $this->view_data['split_test_allocations'] = $this->_get_allocation($this->view_data['split_test']->id);
        $this->view_data['allocation_fields_count'] = $this->_post('allocation_fields_count', count($this->view_data['split_test_allocations']));
        $this->view_data['currency_symbol'] = $this->view_data['currency']->to_currency_symbol(get_option(SH_PREFIX . 'settings_currency'));

        $this->view('v_split_test-edit', $this->view_data);
    }

    public function clone_page() {
        $this->_check_license();
        add_action('admin_footer', array($this, 'page_js'));
        $id = $_GET['id'];

        /**
         * get the existing data
         */
        $sql = "SELECT * FROM {$this->table_name} WHERE id = %d";
        $split_test = $this->wpdb->get_row($this->wpdb->prepare($sql, array($id)), OBJECT);

        $this->wpdb->insert(
                $this->table_name, array(
            'split_test_name' => $split_test->split_test_name,
            'tracking_link' => $this->generate_random_letters(),
                )
        );
        $cloned_id = $this->wpdb->insert_id;

        /**
         * copy over split test allocation
         */
        $sql = "SELECT * FROM {$this->tbl_split_test_allocations} WHERE split_test_id = %d";
        $split_test_allocation = $this->wpdb->get_results($this->wpdb->prepare($sql, array($id)), OBJECT);
        foreach ($split_test_allocation as $row) {
            $traffic_allocation = array(
                'split_test_id' => $cloned_id,
                'link_id' => $row->link_id,
                'tracker_url' => $row->tracker_url,
                'traffic' => $row->traffic,
            );
            $this->wpdb->insert($this->tbl_split_test_allocations, $traffic_allocation);
        }


        $this->view_data['msg'] = array(
            'status' => 'alert-success',
            'text' => SRTY_MSG_CLONE_SPLIT_TEST
        );
        $this->set_top_message($this->view_data['msg']);
        wp_redirect('?page=sh_split_tests&action=edit&id=' . $cloned_id);
        exit();
    }

    public function page_js() {
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                var network;
                ZeroClipboard.config({swfPath: "<?php echo SH_JS_URL; ?>/ZeroClipboard.swf"});
                var oTable = jQuery('#v_split_tests').DataTable({
                    "dom": '<"report panel panel-default"<"panel-heading"<"form form-inline clearfix"<"pull-left"<"#bulk_action.form-group">><"pull-right"<"form-group"f><"form-group"l>>>> <"table-responsive"t><"panel-footer clearfix"<"pull-left"<"form-group text-muted"<"form-control-static"i>>><"pull-right"p>>>',
                    "language": {
                        "search": '',
                        "lengthMenu": '&nbsp;_MENU_',
                        "paginate": {
                            "previous": "«",
                            "next": "»"
                        },
                        "searchPlaceholder": 'Type to Search...',
                    },
                    "serverSide": true,
                    "ajax": {
                        "url": ajaxurl,
                        "type": "POST",
                        "data": function (d) {
                            return jQuery.extend({}, d, {
                                "action": 'srty_split_test_datatable'
                            });
                        },
                    },
                    "order": [[0, "desc"]],
                    "columnDefs": [
                        {
                            "targets": 0,
                            "orderable": false
                        },
                        {
                            "targets": 3,
                            "orderable": false
                        },
                    ]
                });
                jQuery('#bulk_action').html('<select name="slcAction" class="form-control input-sm slcAction"><option>Bulk Actions</option><option>Delete</option></select> <button data-action="srty_split_test_delete" class="btnAction btn btn-default btn-sm" type="submit">Apply</button>');
                jQuery('#v_split_tests').on('draw.dt', function () {
                    var client = new ZeroClipboard(jQuery(".clippy"));
                    client.on("ready", function (readyEvent) {
                        client.on("copy", function (event) {
                            var clipboard = event.clipboardData;
                            clipboard.setData("text/plain", event.target.dataset.text);
                        });
                        client.on("aftercopy", function (event) {
                            jQuery(event.target)
                                    .attr('data-original-title', 'Copied!')
                                    .tooltip('show');
                            jQuery(event.target).on('hidden.bs.tooltip', function () {
                                jQuery(event.target).attr('data-original-title', 'Copy to Clipboard');
                            });
                        });
                    });
                });
                jQuery('body').tooltip({selector: '[rel="tooltip"]', placement: 'top', title: 'Copy to Clipboard'});
                var client = new ZeroClipboard(jQuery(".clippy"));
                client.on("ready", function (readyEvent) {
                    client.on("copy", function (event) {
                        var clipboard = event.clipboardData;
                        clipboard.setData("text/plain", event.target.dataset.text);
                    });
                    client.on("aftercopy", function (event) {
                        jQuery(event.target)
                                .attr('data-original-title', 'Copied!')
                                .tooltip('show');
                        jQuery(event.target).on('hidden.bs.tooltip', function () {
                            jQuery(event.target).attr('data-original-title', 'Copy to Clipboard');
                        });
                    });
                });
                jQuery(".chosen-select").chosen({
                    no_results_text: "Oops, nothing found!",
                    width: "100%"
                });
                jQuery("#split_test_new ").on('click', '.btnRemoveAllocation', function () {
                    if (confirm('Are you sure you want to delete this?')) {
                        jQuery(this).closest('fieldset').remove();
                        var count = parseInt(jQuery('#allocation_fields_count').val());
                        jQuery('#allocation_fields_count').val(count - 1);
                        update_field_name();
                    }
                });
                jQuery("#split_test_new .btnAddAllocation").on('click', function () {
                    add_tracker();
                });
                remove_button_condition();
                function remove_button_condition() {
                    if (jQuery('#allocation_fields_count').val() <= 2) {
                        jQuery('.btnRemoveAllocation').hide();
                    }
                    else {
                        jQuery('.btnRemoveAllocation').show();
                    }
                }

                function update_field_name() {
                    jQuery('#allocation_section fieldset').each(function (i) {
                        console.log(i);
                        jQuery(this).find("[name^='tracker_']").attr("name", "tracker_" + (i + 1));
                        jQuery(this).find("[name^='traffic_']").attr("name", "traffic_" + (i + 1));
                        jQuery(this).find("[name^='split_test_allocations_id_']").attr("name", "split_test_allocations_id_" + (i + 1));
                    });
                    remove_button_condition();
                }

                function add_tracker() {
                    var html = '';
                    html += '<fieldset>';
                    html += '<div class="row">';
                    html += '<div class="col-md-4">';
                    html += '<div class="form-group">';
                    html += '<label for="input28"> Choose Tracker: </label>';
                    html += '<div class="controls">';
                    html += '<input name="tracker_" type="text" class="form-control typeahead" style="margin: 0 auto;" value="" >';
                    html += '<input name="split_test_allocations_id_" type="hidden" value="0" />';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    html += '<div class="col-md-2">';
                    html += '<div class="form-group">';
                    html += '<label for="input29"> Traffic: </label>';
                    html += '<div class="controls">';
                    html += '<div class="input-group">';
                    html += '<div class="input-group">';
                    html += '<input name="traffic_" type="text" class="form-control" placeholder="50" value="">';
                    html += '<span class="input-group-addon">%</span>';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    html += '<div class="col-md-6">';
                    html += '<div class="table-responsive">';
                    html += '<table class="table table-condensed table-bordered" style="font-size:12px;">';
                    html += '<tr>';
                    html += '<th>Visits</th>';
                    html += '<th>Visitors</th>';
                    html += '<th>Conv.</th>';
                    html += '<th>Conv. %</th>';
                    html += '<th>Cost</th>';
                    html += '<th>CPA</th>';
                    html += '<th>CPC</th>';
                    html += '<th>Revenue</th>';
                    html += '<th>RPV</th>';
                    html += '<th>Profit</th>';
                    html += '</tr>';
                    html += '<tr role="row">';
                    html += '<td>0</td>';
                    html += '<td>0</td>';
                    html += '<td>0</td>';
                    html += '<td>0.00</td>';
                    html += '<td>$0.00</td>';
                    html += '<td>$0.00</td>';
                    html += '<td>$0.00</td>';
                    html += '<td>$0.00</td>';
                    html += '<td>$0.00</td>';
                    html += '<td>$0.00</td>';
                    html += '</tr>';
                    html += '</table>';
                    html += '</div>';
                    html += '<button type="button" class=" btnRemoveAllocation btn btn-default btn-xs">Remove</button></div>';
                    html += '</div>';
                    html += '<hr>';
                    html += '</fieldset>';
                    jQuery('#allocation_section').append(html);
                    var count = parseInt(jQuery('#allocation_fields_count').val());
                    jQuery('#allocation_fields_count').val(count + 1);
                    update_field_name();

                    jQuery('.typeahead').trigger('added');
                }

            });</script>
        <?php
    }

    /**
     * ajax call
     */
    public function ajax_datatable() {
        wp_send_json($this->_datatable_get_all());
        wp_die();
    }

    public function ajax_delete() {
        if (!$this->check_access(3)) {
            wp_redirect('?page=' . SH_MENU_SLUG);
            exit;
        }
        $this->_delete_batch($this->explode_trim($_POST['ids']));
        $this->view_data['msg'] = array(
            'status' => 'alert-success',
            'text' => SRTY_MSG_SPLIT_TEST_DELETED
        );
        $this->set_top_message($this->view_data['msg']);
        wp_send_json(array('result' => 1));
        wp_die();
    }

    /**
     * Get link id from url
     * @param type $tracker
     * @return type
     */
    public function get_link_id($tracker) {
        $tracker = str_replace($this->current_domain(TRUE,TRUE,TRUE), '', $tracker);
        $tracker = str_replace(get_option(SH_PREFIX . 'settings_tracking_domain').'/', '', $tracker);
        $sql = "SELECT id FROM {$this->tbl_links} WHERE tracking_link = %s;";
        $link = $this->wpdb->get_row($this->wpdb->prepare($sql, array($tracker)), OBJECT);
        return $link->id;
    }


    public function js_typeahead_split_test() {
        if (wp_script_is('jquery', 'done')) {
            ?>
            <script type="text/javascript">
                jQuery(document).ready(function () {
                    function prepare(query, settings) {
                        settings.url = ajaxurl + '?action=srty_campaigns_typeahead&q=' + query;
                        settings.type = 'POST';
                        return settings;
                    }
                    var engine = new Bloodhound({
                        name: 'wp_page',
                        remote: {
                            url: ajaxurl,
                            prepare: prepare
                        },
                        datumTokenizer: function (d) {
                            return Bloodhound.tokenizers.whitespace(d.val);
                        },
                        queryTokenizer: Bloodhound.tokenizers.whitespace
                    });
                    engine.initialize();
                    engine.get('');

                    jQuery('.typeahead').on('added', function () {
                        jQuery('.typeahead').typeahead('destroy');
                        jQuery('.typeahead').typeahead({
                            autoselect: true,
                            hint: false,
                            highlight: false,
                            limit: 40,
                            minLength: 0,
                        }
                        , {
                            name: 'link_tracking_url',
                            displayKey: 'url',
                            templates: {
                                suggestion: Handlebars.compile('<p style="font-size:14px;"><strong>{{link_name}}</strong> – {{url}}</p>')
                            },
                            // `ttAdapter` wraps the suggestion engine in an adapter that
                            // is compatible with the typeahead jQuery plugin
                            source: engine.ttAdapter(),
                        });
                    }).trigger('added');

                    var client = new ZeroClipboard(jQuery(".clippy"));
                    client.on("ready", function (readyEvent) {
                        client.on("copy", function (event) {
                            var clipboard = event.clipboardData;
                            clipboard.setData("text/plain", event.target.dataset.text);
                        });
                        client.on("aftercopy", function (event) {
                            jQuery(event.target)
                                    .attr('data-original-title', 'Copied!')
                                    .tooltip('show');
                            jQuery(event.target).on('hidden.bs.tooltip', function () {
                                jQuery(event.target).attr('data-original-title', 'Copy to Clipboard');
                            });
                        });
                    });
                });
            </script>
            <?php
        }
    }

    /**
     * DB section
     */

    /**
     * 
     * @global type $wpdb
     * @return type
     */
    private function _by_id($id = FALSE) {
        if ($id === FALSE) {
            $id = isset($_GET['id']) ? $_GET['id'] : -1;
        }

        return $this->wpdb->get_row($this->wpdb->prepare("SELECT * FROM $this->table_name WHERE id=%d", $id));
    }

    private function _get_allocation($split_test_id) {
        $sql = "SELECT 
a.* ,
COUNT(DISTINCT(b.visitor_id)) AS visitors,
COUNT(b.visitor_session) AS visits,
COUNT(c.id) AS conv,
IFNULL(FORMAT((COUNT(c.id)/COUNT(b.visitor_session))*100,2),0.00) AS conv_percentage,
IFNULL(SUM(b.cpc),0) AS cost,
IFNULL(FORMAT(IFNULL(SUM(b.cpc),0)/COUNT(c.id),2),0.00) AS cpa,
IFNULL(FORMAT(IFNULL(SUM(b.cpc),0)/COUNT(b.visitor_session),2),0.00) AS cpc,
IFNULL(SUM(c.goal_value),0) AS revenue,
IFNULL(FORMAT((IFNULL(SUM(c.goal_value),0)/COUNT(DISTINCT(b.visitor_id))),2),0.00) AS rpv,
FORMAT(IFNULL(SUM(c.goal_value),0)-IFNULL(SUM(b.cpc),0),2) AS profit
FROM  $this->tbl_split_test_allocations a
LEFT JOIN $this->tbl_visits_log b ON  b.split_test_id = a.split_test_id AND b.link_id = a.link_id
LEFT JOIN $this->tbl_conversions_log c ON c.visits_log_id = b.id
WHERE a.split_test_id = %d
GROUP BY a.id";
        return $this->wpdb->get_results($this->wpdb->prepare($sql, $split_test_id));
    }

    private function _get_all() {
        return $this->wpdb->get_results("SELECT * FROM $this->table_name ");
    }

    private function _datatable_get_all() {

        $params = array();


        $where = '';
        if (isset($_POST['search'])) {
            $_search = $_POST['search'];
            if (trim($_search['value']) != '') {
                $where = " WHERE ( split_test_name LIKE %s OR tracking_link LIKE %s )";
                $params = array_merge($params, array('%%' . $_search['value'] . '%%', '%%' . $_search['value'] . '%%'));
            }
        }

        $order = $this->_order($params);
        $limit = $this->_limit($params);

        $results = $this->wpdb->get_results($this->wpdb->prepare("SELECT SQL_CALC_FOUND_ROWS id,split_test_name,tracking_link FROM {$this->table_name} {$where} {$order} {$limit}", $params));

        $found = $this->wpdb->get_row("SELECT FOUND_ROWS() AS total;");
        $record = $this->wpdb->get_row("SELECT COUNT(id) AS total FROM $this->table_name;");

        $data = array();
        foreach ($results as $row) {
            $data[] = array(
                '<input type="checkbox" name="cbAction[]" value="' . $row->id . '">',
                $row->split_test_name,
                '<span id="clip_' . $row->id . '" data-text="' . $this->current_domain(TRUE, TRUE, TRUE) . 'r/' . $row->tracking_link . '" class="clippy btn btn-xs btn-default" rel="tooltip" ><i class="fa fa-copy"></i></span> ' . '<a href=' . $this->current_domain(TRUE, TRUE, TRUE) . 'r/' . $row->tracking_link . ' class="linked" target="_blank"> ' . $this->ellipsize($this->current_domain(TRUE, TRUE, TRUE) . 'r/' . $row->tracking_link) . '</a>',
                '<div class="btn-group btn-group-xs">' .
                '<a href="?page=sh_split_tests&action=edit&id=' . $row->id . '" class="btn btn-default">edit</a>' .
                '<a href="?page=sh_split_tests&action=clone&id=' . $row->id . '" class="btn btn-default">clone</a>' .
                (($this->check_access(3)) ? '<a href="?page=sh_split_tests&action=delete&id=' . $row->id . '" class="btn btn-default confirm">delete</a>' : '') .
                '</div>',
            );
        }

        return array(
            "draw" => intval($_POST['draw']),
            "recordsTotal" => intval($found->total),
            "recordsFiltered" => intval($found->total),
            "data" => $data
        );
    }

    private function _filter($request, $columns) {
        $where = '';
        return $where;
    }

    /* private function validate_form($link_name, $destination_url, $tracking_link, $link_redirect_type, $cloaking_status_enable, $cloaking_type, $bar_position, $frame_content, $meta_title, $meta_title, $meta_description, $param_tag_affiliate_tracking, $param_tag_affiliate_network, $auto_keyword_linking_enable, $meta_keyword) {
      // Make the WP_Error object global
      global $form_error;

      // instantiate the class
      $form_error = new WP_Error;

      // If any field is left empty, add the error message to the error object
      if (empty($link_name) || empty($destination_url) || empty($tracking_link)) {
      $form_error->add('field', 'No field should be left empty');
      }

      // if the name field isn't alphabetic, add the error message
      if (!ctype_alnum($tracking_link)) {
      $form_error->add('invalid_link', 'Invalid tracking link entered');
      }

      // Check if the email is valid
      if (!$this->is_valid_url($destination_url)) {
      $form_error->add('invalid_url', 'URL is not valid');
      }

      // if $form_error is WordPress Error, loop through the error object
      // and echo the error
      if (is_wp_error($form_error)) {
      foreach ($form_error->get_error_messages() as $error) {
      //                echo '<div>';
      //                echo '<strong>ERROR</strong>:';
      //                echo $error . '<br/>';
      //                echo '</div>';
      return TRUE;
      }
      }
      } */

    public function srty_get_meta() {
        $meta_type = $this->_post('meta_type');
        $target_url = $this->_post('url');
        $result = $this->curl($target_url);
        $meta = $this->get_preg_meta_tags($result);
        wp_send_json(array('data' => isset($meta[$meta_type]) ? $meta[$meta_type] : ''));
        wp_die();
    }

    public function srty_checkurl() {
        $url = $_POST["purl"];

        $pattern = '%^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@|\d{1,3}(?:\.\d{1,3}){3}|(?:(?:[a-z\d\x{00a1}-\x{ffff}]+-?)*[a-z\d\x{00a1}-\x{ffff}]+)(?:\.(?:[a-z\d\x{00a1}-\x{ffff}]+-?)*[a-z\d\x{00a1}-\x{ffff}]+)*(?:\.[a-z\x{00a1}-\x{ffff}]{2,6}))(?::\d+)?(?:[^\s]*)?$%iu';
        if (!preg_match($pattern, $url)) { // Check for valid URL Format
            echo FALSE;
        } else {
            $c = curl_init();
            curl_setopt($c, CURLOPT_URL, $url);
            curl_setopt($c, CURLOPT_HEADER, 1); //get the header
            curl_setopt($c, CURLOPT_NOBODY, 1); //and *only* get the header
            curl_setopt($c, CURLOPT_RETURNTRANSFER, 1); //get the response as a string from curl_exec(), rather than echoing it
            curl_setopt($c, CURLOPT_FRESH_CONNECT, 1); //don't use a cached version of the url
            curl_setopt($c, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($c, CURLOPT_USERAGENT, 'shortywp.com');
            curl_exec($c);
            $httpcode = curl_getinfo($c, CURLINFO_HTTP_CODE);
            if ($httpcode > 0) {
                echo TRUE;
            } else {
                echo FALSE;
            }
        }
        wp_die();
    }

    protected function _delete($id) {
        /**
         * delete previous split test allocation records
         */
        $sql = "DELETE FROM {$this->tbl_split_test_allocations} WHERE split_test_id = %d;";
        $this->wpdb->get_results($this->wpdb->prepare($sql, array($id)), OBJECT);

        $this->wpdb->delete($this->table_name, array('id' => $id), array('%d'));
    }

}
