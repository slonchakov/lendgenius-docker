<?php

class Srty_tools extends Srty_core {

    public function __construct() {
        parent::__construct();
    }

    public function display() {
        $this->_check_license();
        add_action('admin_footer', array($this, 'page_js'));

        if (isset($_POST['btnSave'])) {

            update_option(SH_PREFIX . 'tools_pixel_name', $this->_post('tools_pixel_name'));
            update_option(SH_PREFIX . 'tools_pixel_type', $this->_post('tools_pixel_type'));
            update_option(SH_PREFIX . 'tools_pixel_value', $this->_post('tools_pixel_value'));
            update_option(SH_PREFIX . 'tools_pixel_rid', $this->_post('tools_pixel_rid'));
            update_option(SH_PREFIX . 'tools_pixel_ssl', $this->_post('tools_pixel_ssl'));

            if (in_array(get_option(SH_PREFIX . 'license_scheme_id'), array(SHORTY_PRO_LICENSE, SHORTY_AGENCY_LICENSE, SHORTY_JV, SHORTY_WHITE_LABEL))) {
                update_option(SH_PREFIX . 'tools_network', $this->_post('network'));
                update_option(SH_PREFIX . 'tools_goal_type', $this->_post('tools_goal_type'));
                update_option(SH_PREFIX . 'tools_ssl', $this->_post('tools_ssl'));
            }
            $this->view_data['msg'] = array(
                'status' => 'alert-success',
                'text' => SRTY_MSG_TOOLS_EDITED
            );
        }
        $this->view('v_tools');
    }

    public function page_js() {
        ?>
        <script type="text/javascript">
            function qs(network) {
                switch (network) {
                    case 'a4d':
                        query_string = 'ctid=#s1#&gn=a4d&gv=#price#&rid=';
                        break;
                    case 'abovealloffers':
                        query_string = 'ctid=#s1#&gn=abovealloffers&gv=#price#&rid=';
                        break;
                    case 'adsimilis':
                        query_string = 'ctid={aff_sub}&gn=adsimilis&gv={payout}&rid=';
                        break;
                    case 'c2m':
                        query_string = 'ctid=#s1#&gn=c2m&gv=#price#&rid=';
                        break;
                    case 'cashnetwork':
                        query_string = 'ctid=#s1#&gn=cashnetwork&gv=#price#&rid=';
                        break;
                    case 'clickbooth':
                        query_string = 'ctid=#s1#&gn=clickbooth&gv=#price#&rid=';
                        break;
                    case 'clickpromise':
                        query_string = 'ctid=[=SID=]&gn=clickpromise&gv=[=AffiliateCommission=]&rid=';
                        break;
                    case 'cpaway':
                        query_string = 'ctid=#subid#&gn=cpaway&gv=#rate#&rid=';
                        break;
                    case 'globalwide':
                        query_string = 'ctid=#s1#&gn=globalwide&gv=#price#&rid=';
                        break;
                    case 'maxbounty':
                        query_string = 'ctid=#S1#&gn=maxbounty&gv=#RATE#&rid=';
                        break;
                    case 'neverblue':
                        query_string = 'ctid=#s1#&gn=neverblue&gv=#price#&rid=';
                        break;
                    case 'peerfly':
                        query_string = 'ctid=%subid1%&gn=%offer% peerfly&gv=%commission%&rid=';
                        break;
                    case 'w4':
                        query_string = 'ctid=xxc1xx&gn=w4&gv=xxpayoutxx&rid=';
                        break;
                    case 'xy7':
                        query_string = 'ctid=#s1#&gn=xy7&gv=#price#&rid=';
                        break;
                    case 'cake':
                        query_string = 'ctid=[=SID=]&gn=cake&gv=[=AffiliateCommission=]&rid=';
                        break;
                    case 'hasoffers':
                        query_string = 'ctid={aff_sub}&gn=hasoffers&gv={payout}&rid=';
                        break;
                    case 'hitpath':
                        query_string = 'ctid=xxc1xx&gn=hitpath&gv=xxpayoutxx&rid=';
                        break;
                    case 'linktrust':
                        query_string = 'ctid=[=SID=]&gn=linktrust&gv=[=AffiliateCommission=]&rid=';
                        break;
                    default:
                        query_string = 'ctid=&gn=&gv=&rid=';
                }
                return query_string;
            }

            jQuery(document).ready(function () {
                var qs_pixel_main = '<?php echo $this->current_domain(TRUE, TRUE); ?>srty/pixel?gt=SALE&';
                jQuery('#tools_pixel_type,#tools_pixel_ssl,#tools_pixel_name,#tools_pixel_value,#tools_pixel_rid').change(function () {
                    pixel = qs_pixel_main + "gn=" + jQuery('#tools_pixel_name').val() + "&gv=" + jQuery('#tools_pixel_value').val() + "&rid="+ jQuery('#tools_pixel_rid').val();



                    pixel = pixel.replace('SALE', 'LEAD');
                    if (jQuery('#tools_pixel_type').val() == 'SALE') {
                        pixel = pixel.replace('LEAD', 'SALE');
                    }

                    pixel = pixel.replace('https://', 'http://');
                    if (jQuery('#tools_pixel_ssl').is(":checked")) {
                        pixel = pixel.replace('http://', 'https://');
                    }

                    pixel = '<img src="' + pixel + '" alt="_" width="1" height="1" border="0" />';
                    jQuery('#pixel_code').val(pixel);
                }).trigger('change');

                var qs_main = '<?php echo $this->current_domain(TRUE, TRUE); ?>srty/postback?gt=SALE&';
                jQuery('#tools_goal_type,#network,#tools_ssl').change(function () {
                    var network = jQuery('#network').val();
                    postback = qs_main + qs(network);

                    postback = postback.replace('SALE', 'LEAD');
                    if (jQuery('#tools_goal_type').val() == 'SALE') {
                        postback = postback.replace('LEAD', 'SALE');
                    }

                    postback = postback.replace('https://', 'http://');
                    if (jQuery('#tools_ssl').is(":checked")) {
                        postback = postback.replace('http://', 'https://');
                    }

                    jQuery('#postback').val(postback);
                }).trigger('change');

            });
        </script>
        <?php
    }

}
