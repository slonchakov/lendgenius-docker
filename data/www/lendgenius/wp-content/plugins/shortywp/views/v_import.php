<div class="page-header">
    <h2><i class="fa fa-upload"></i> Import / Add Conversions</h2>
    <p>Manually add conversions or upload conversion reports in Microsoft Excel or CSV from third-party sites and affiliate networks. Items marked <i class="fa fa-exclamation-circle text-silver"></i>  are required.</p>
</div>
<p>&nbsp;</p>
<div class="form">
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title"> Add Conversion</div>
                </div>
                <div class="panel-body">
                    <form method="POST" name="frmNewConversion">
                        <div class="form-group">
                            <label class="control-label" for="conversion_date"><i class="fa fa-exclamation-circle text-silver"></i> Date &amp; Time: </label>
                            <div class="controls">
                                <input required name="conversion_date" type="text" class="form-control datetimepicker" id="conversion_date" placeholder="Select Date & Time">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="goal_name"><i class="fa fa-exclamation-circle text-silver"></i> Conversion Name: </label>
                            <div class="controls">
                                <input required name="goal_name" type="text" class="form-control" id="goal_name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="goal_type"><i class="fa fa-exclamation-circle text-silver"></i> Goal Type: </label>
                            <div class="controls">
                                <select required name="goal_type" id="goal_type" class="form-control">
                                    <option value="<?php echo GOAL_TYPE_LEAD; ?>">Lead</option>
                                    <option value="<?php echo GOAL_TYPE_SALE; ?>">Sale</option>
                                </select>
                                <p class="help-block">A lead and sale is treated differently based on your conversion settings.</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="goal_value"><i class="fa fa-exclamation-circle text-silver"></i> Goal Value: </label>
                            <div class="controls">
                                <div class="input-group">
                                    <input required type="text" name="goal_value" class="form-control" id="goal_value" placeholder="Example: 9.90">
                                    <span class="input-group-addon"><?php echo get_option(SH_PREFIX . 'settings_currency'); ?></span></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="goal_reference">Unique ID: </label>
                            <div class="controls">
                                <input type="text" name="goal_reference" class="form-control" id="goal_reference" placeholder="Example: 0123456">
                                <p class="help-block">Enter a unique ID so we can tell when to recognize a goal conversion, and when to ignore it. If you have no unique ID to pass, leave this blank. <a href="#" rel="tooltip" data-original-title="This is normally your order ID, customer email address or transaction ID. You can automatically replace this unique id in your shopping cart process."><i class="fa fa-question-circle text-silver"></i></a></p>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="click_id">CTID: </label>
                            <div class="controls">
                                <input name="click_id" type="text" class="form-control" id="click_id">
                                <p class="help-block">You should have passed the CTID variable to your affiliate network in the format of a Sub ID. Refer to your network's report and enter that Sub ID here.</p>
                            </div>
                        </div>
                        <div class="form-actions"><button name="btnAdd" type="submit" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Add Conversion</button></div>
                    </form>
                </div>
            </div>
        </div>        
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">IMPORT FILE</div>
                </div>
                <div class="panel-body">
                    <form method="POST" name="frmImport" enctype="multipart/form-data">
                        <fieldset>
                            <p>The easiest way to import conversion data from affiliate networks or third-party tracking systems is to use the pre-defined import options. </p>
                            <div class="controls">
                                <label class="control-label" for="import_option">Predefined or Custom: </label>
                                <select name="import_option" id="import_option" class="form-control">
                                    <option value="custom">Custom</option>
                                    <option value="jvzoo">JVZoo</option>
                                    <option value="cj">Commission Junction</option>
                                    <option value="shareasale">Shareasale</option>
                                    <option value="clickbank">ClickBank</option>
                                    <option value="cuelinks">Cue Links</option>
                                </select>
                                <p class="help-block">Select the predefined format. You will need to upload the file provided by your affiliate metwork. Do not modify the file before uploading.</p>

                            </div>
                            <fieldset id="custom_import_option"  >
                                <p>If you cannot find a suitable pre-defined option, please create your own Microsoft Excel or CSV file according to the following sample: </p>
                                <div class="table-responsive">
                                    <table class="table table-condensed table-bordered">
                                        <tr>
                                            <th>date time</th>
                                            <th>gn</th>
                                            <th>type</th>
                                            <th>gv</th>
                                            <th>rid</th>
                                            <th>ctid</th>
                                        </tr>
                                        <tr>
                                            <td>2015-12-20 01:50:00</td>
                                            <td>LinkTrackr Extreme</td>
                                            <td>LEAD</td>
                                            <td>1.25</td>
                                            <td>00012432</td>
                                            <td>4533523</td>
                                        </tr>
                                        <tr>
                                            <td>2015-12-21 11:30:00</td>
                                            <td>LinkTrackr Extreme</td>
                                            <td>SALE</td>
                                            <td>1067.00</td>
                                            <td>00012442</td>
                                            <td>4423423</td>
                                        </tr>
                                    </table>
                                </div>
                                <p>
                                    <a href="#import_info" class="btn btn-default" data-toggle="collapse"><i class="fa fa-question-circle"></i> Instructions</a> 
                                    <a download href="<?php echo SH_URL . '/assets/import/template_import.csv'; ?>" target="_blank" class="btn btn-default"><i class="fa fa-download"></i> Download Sample</a>
                                </p>
                                <div class="well collapse" id="import_info">
                                    <ul>
                                        <li>
                                            <strong>Date Time:</strong> Must be in the following formats <code>Y-m-d H:i:s</code>.<br/>
                                            <code>Y</code> A full numeric representation of a year, 4 digits.<br/>
                                            <code>m</code> Numeric representation of a month, with leading zeros. 01 through 12.<br/>
                                            <code>d</code> Day of the month, 2 digits with leading zeros. 01 to 31.<br/>
                                            <code>H</code> 24-hour format of an hour with leading zeros. 00 through 23.<br/>
                                            <code>i</code> Minutes with leading zeros. 00 to 59.<br/>
                                            <code>s</code> Seconds, with leading zeros. 00 through 59.<br/>
                                            For example <code>2015-03-20 23:10:00</code>
                                            <br/><br/>
                                            For MS Excel, please Format Cell using 'Custom Format' and put in <code>yyyy-mm-dd hh:mm:ss</code><br/><br/>
                                        </li>
                                        <li><strong>GN (Goal Name):</strong> The name of your conversion, usually the product name or lead name.</li>
                                        <li><strong>TYPE:</strong> Conversion type, which is either <code>LEAD</code> or <code>SALE</code></li>
                                        <li><strong>GV (Goal Value):</strong> The total value of the conversion, numbers only up to two decimal places, without any currency codes. Everything you upload here will be in <?php echo get_option(SH_PREFIX . 'settings_currency'); ?> as per your <a href="?page=sh_settings_page"> settings</a>. You should convert amounts not in this currency <em>before</em> uploading your data.</li>
                                        <li><strong>RID (Reference): </strong>Unique reference number, usually the order number, transaction ID, or lead ID. Duplicate reference names will not be counted as a conversion.</li>
                                        <li><strong>CTID (Click Tracking ID):</strong> The most important piece of information, the <code>ctid</code> column must consist of actual Click IDs created by our system. This data will help us attribute a conversion to the correct visitor, click, country and other important data. </li>
                                        <li><span class="label label-danger">IMPORTANT:</span> Data without valid <code>ctid</code> will be rejected.</li>
                                    </ul>
                                </div>
                            </fieldset>
                        </fieldset>

                        <div class="control-group" id="file_upload">
                            <label class="control-label" for="btnUpload">Choose File: </label>
                            <div class="controls">
                                <input type="file" name="upload_file" id="upload_file">
                                <p class="help-block">Choose the <code>csv</code> or <code>txt</code> file you want to upload. Limited to 1Mb per file.</p>
                            </div>
                        </div>
                        <div class="form-actions"><button type="submit" name="btnImport" class="btn btn-primary"><i class="fa fa-upload"></i> Import File</button></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
