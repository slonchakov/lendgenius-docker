<div class="page-header">
    <h2><i class="fa fa-random"></i> Campaigns 
    </h2>
    <p>Create a campaign to track paid banner ads, PPC spend, emails ad swaps and more. </p>
</div>
<table id="v_campaigns" class="table table-hover" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th class="col-sm-1"><input type="checkbox" name="checkbox" class="cbAll"></th>
            <th>Campaign Name</th>
            <th class="col-sm-2">Cost</th>
            <th>Campaign Link</th>
            <th class="col-sm-3">Actions</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="5">Loading</td>
        </tr>
    </tbody>
</table>


