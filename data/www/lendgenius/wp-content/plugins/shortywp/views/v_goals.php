<div class="page-header">
    <h2><i class="fa fa-crosshairs"></i> Goals 
    </h2>
    <p>Create a goal to track sales, leads and options. You can then enter the goal tracking codes within your WordPress editor.</p>
</div>
<table id="v_goals" class="table table-hover" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th class="col-sm-1"><input type="checkbox" name="checkbox" class="cbAll"></th>
            <th class="col-sm-1">ID</th>
            <th >Goal</th>
            <th class="col-sm-2">Value</th>
            <th class="col-sm-3">Action</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="5">Loading</td>
        </tr>
    </tbody>
</table>
