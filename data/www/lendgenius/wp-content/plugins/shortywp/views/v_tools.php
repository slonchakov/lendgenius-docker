<div class="page-header">
    <h2><i class="fa fa-crosshairs"></i> Tools</h2>
    <p>Automation tools to track sales & leads.</p>
</div>
<form method="POST" name="frmTools">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title">Global Pixel URL</div>
        </div>
        <div class="panel-body">
            <fieldset>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="input01"><i class="fa fa-exclamation-circle text-silver"></i> Goal Name: </label>
                            <div class="controls">
                                <input type="text" class="form-control" name="tools_pixel_name" id="tools_pixel_name" value="<?php echo get_option(SH_PREFIX . 'tools_pixel_name'); ?>">
                                <p class="help-block">Give your goal a name so you can recognize it easily in your reports and graphs.</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="input2">Goal Type: </label>
                            <div class="controls">
                                <select name="tools_pixel_type" id="tools_pixel_type" class="form-control">
                                    <option value="LEAD" <?php echo selected($this->set_value('tools_pixel_type', get_option(SH_PREFIX . 'tools_pixel_type')), 'LEAD', true); ?>>Lead</option>
                                    <option value="SALE" <?php echo selected($this->set_value('tools_pixel_type', get_option(SH_PREFIX . 'tools_pixel_type')), 'SALE', true); ?>>Sale</option>
                                </select>
                                <p class="help-block">A lead and sale is treated differently based on your conversion settings.</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="tools_pixel_value"><i class="fa fa-exclamation-circle text-silver"></i> Goal Value: </label>
                            <div class="controls">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="tools_pixel_value" id="tools_pixel_value" value="<?php echo get_option(SH_PREFIX . 'tools_pixel_value'); ?>" placeholder="Example: 9.90">
                                    <span class="input-group-addon"><?php echo get_option(SH_PREFIX . 'settings_currency'); ?></span>
                                </div>
                                <p class="help-block">Enter the goal conversion value, numbers only. You can always change the value manually when inserting the tracking codes.</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="tools_pixel_rid">Reference ID: </label>
                            <div class="controls">
                                <input type="text" class="form-control" id="tools_pixel_rid" name="tools_pixel_rid" value="<?php echo get_option(SH_PREFIX . 'tools_pixel_rid'); ?>" placeholder="Optional..">
                                <p class="help-block">Enter the goal conversion value, numbers only. You can always change the value manually when inserting the tracking codes.</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="label2">Use SSL Tracking: </label>
                            <div class="controls">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="tools_pixel_ssl" id="tools_pixel_ssl" value="1" <?php echo checked($this->set_value('tools_pixel_ssl', get_option(SH_PREFIX . 'tools_pixel_ssl')), 1, true); ?>>
                                        Enable SSL (secure link) </label>
                                </div>
                                <p class="help-block">Enable SSL if a secure link is required. Please note that you must have SSL installed on this domain in order for it to work. <a href="http://gigurl.com/ssl" target="_blank">Get your SSL certs here.</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="control-label" for="label">Pixel code: </label>
                            <div class="controls">
                                <textarea name="pixel_code" class="form-control" id="pixel_code">
&lt;img src=&quot;<?php echo ((get_option(SH_PREFIX . 'tools_pixel_ssl') == 1) ? 'https' : 'http') . '://' . $this->current_domain(FALSE, TRUE, TRUE) . 'srty/pixel/?gn=' . stripslashes(get_option(SH_PREFIX . 'tools_pixel_name')) . '&amp;gt=' . get_option(SH_PREFIX . 'tools_pixel_type') . '&amp;gv=' . get_option(SH_PREFIX . 'tools_pixel_value') . '&amp;rid=' . get_option(SH_PREFIX . 'tools_pixel_rid') . '&quot; alt=&quot;_&quot; width=&quot;1&quot; height=&quot;1&quot; border=&quot;0&quot; /&gt;'; ?>
                                </textarea>
                                <p class="help-block">To get the unique reference ID of a conversion and to dynamically replace the goal variables, you have to manually insert and configure this pixel code in your page.</p>
                                <div class="table-responsive">
                                    <table class="table table-condensed">
                                        <tr>
                                            <td><code>gn</code></td>
                                            <td>The conversion name, alphabets and numbers only.</td>
                                        </tr>
                                        <tr>
                                            <td><code>gt</code></td>
                                            <td>Specify either SALE or LEAD only.</td>
                                        </tr>
                                        <tr>
                                            <td><code>gv</code></td>
                                            <td>The value of the conversion, two decimal points without comma or curency symbols.</td>
                                        </tr>
                                        <tr>
                                            <td><code>rid</code></td>
                                            <td>The unique conversion tracking value, usually generated by your shopping cart or lead generation system.</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title">Global Postback URL</div>
        </div>
        <div class="panel-body">
            <?php
            if (in_array(get_option(SH_PREFIX . 'license_scheme_id'), array(SHORTY_PRO_LICENSE, SHORTY_AGENCY_LICENSE, SHORTY_JV, SHORTY_WHITE_LABEL))) {
                ?>
                <fieldset>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label" for="input4">Select network: </label>
                                <div class="controls">
                                    <select name="network" id="network" class="form-control">
                                        <option value="" <?php echo selected($this->set_value('network', get_option(SH_PREFIX . 'tools_network')), '', true); ?>>Select Affiliate Network</option>
                                        <option value="a4d" <?php echo selected($this->set_value('network', get_option(SH_PREFIX . 'tools_network')), 'a4d', true); ?>>A4D</option>
                                        <option value="abovealloffers" <?php echo selected($this->set_value('network', get_option(SH_PREFIX . 'tools_network')), 'abovealloffers', true); ?>>Above All Offers</option>
                                        <option value="adsimilis" <?php echo selected($this->set_value('network', get_option(SH_PREFIX . 'tools_network')), 'adsimilis', true); ?>>Adsimilis</option>
                                        <option value="c2m" <?php echo selected($this->set_value('network', get_option(SH_PREFIX . 'tools_network')), 'c2m', true); ?>>C2M</option>
                                        <option value="cashnetwork" <?php echo selected($this->set_value('network', get_option(SH_PREFIX . 'tools_network')), 'cashnetwork', true); ?>>Cash Network</option>
                                        <option value="clickbooth" <?php echo selected($this->set_value('network', get_option(SH_PREFIX . 'tools_network')), 'clickbooth', true); ?>>Clickbooth</option>
                                        <option value="clickpromise" <?php echo selected($this->set_value('network', get_option(SH_PREFIX . 'tools_network')), 'clickpromise', true); ?>>ClickPromise</option>
                                        <option value="cpaway" <?php echo selected($this->set_value('network', get_option(SH_PREFIX . 'tools_network')), 'cpaway', true); ?>>CPAWay</option>
                                        <option value="globalwide" <?php echo selected($this->set_value('network', get_option(SH_PREFIX . 'tools_network')), 'globalwide', true); ?>>GlobalWide Media</option>
                                        <option value="maxbounty" <?php echo selected($this->set_value('network', get_option(SH_PREFIX . 'tools_network')), 'maxbounty', true); ?>>MaxBounty</option>
                                        <option value="neverblue" <?php echo selected($this->set_value('network', get_option(SH_PREFIX . 'tools_network')), 'neverblue', true); ?>>NeverBlue</option>
                                        <option value="peerfly" <?php echo selected($this->set_value('network', get_option(SH_PREFIX . 'tools_network')), 'peerfly', true); ?>>Peerfly</option>
                                        <option value="w4" <?php echo selected($this->set_value('network', get_option(SH_PREFIX . 'tools_network')), 'w4', true); ?>>W4</option>
                                        <option value="xy7" <?php echo selected($this->set_value('network', get_option(SH_PREFIX . 'tools_network')), 'xy7', true); ?>>XY7</option>
                                        <option value="cake" <?php echo selected($this->set_value('network', get_option(SH_PREFIX . 'tools_network')), 'cake', true); ?>>CAKE</option>
                                        <option value="hasoffers" <?php echo selected($this->set_value('network', get_option(SH_PREFIX . 'tools_network')), 'hasoffers', true); ?>>HasOffers</option>
                                        <option value="hitpath" <?php echo selected($this->set_value('network', get_option(SH_PREFIX . 'tools_network')), 'hitpath', true); ?>>HitPath</option>
                                        <option value="linktrust" <?php echo selected($this->set_value('network', get_option(SH_PREFIX . 'tools_network')), 'linktrust', true); ?>>LinkTrust</option>
                                    </select>
                                    <p class="help-block">Build your postback format by selecting your affiliate network. </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input3">Goal Type: </label>
                                <div class="controls">
                                    <select name="tools_goal_type" id="tools_goal_type" class="form-control">
                                        <option value="LEAD" <?php echo selected($this->set_value('tools_goal_type', get_option(SH_PREFIX . 'tools_goal_type')), 'LEAD', true); ?>>Lead</option>
                                        <option value="SALE" <?php echo selected($this->set_value('tools_goal_type', get_option(SH_PREFIX . 'tools_goal_type')), 'SALE', true); ?>>Sale</option>
                                    </select>
                                    <p class="help-block">A lead and sale is treated differently based on your conversion settings.</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="label7">Use SSL Tracking: </label>
                                <div class="controls">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="tools_ssl" id="tools_ssl" value="1" <?php echo checked($this->set_value('tools_ssl', get_option(SH_PREFIX . 'tools_ssl')), 1, true); ?>>
                                            Enable SSL (secure link)
                                        </label>
                                    </div>
                                    <p class="help-block">Enable SSL if a secure link is required. Please note that you must have SSL installed on this domain in order for it to work. <a href="http://gigurl.com/ssl" target="_blank">Get your SSL certs here.</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <label class="control-label" for="label4">Postback URL: </label>
                                <div class="controls">
                                    <input name="postback" type="text" class="form-control" id="postback" placeholder="URL here.." value="<?php echo $this->current_domain(TRUE, TRUE); ?>srty/postback/?gt=LEAD&amp;ctid=&amp;gn=&amp;gv=&amp;rid=" >
                                    <p class="help-block">To get the unique reference ID of a conversion and to dynamically replace the goal variables, you have to manually insert and configure this pixel code in your page. </p>
                                </div>
                                <div class="controls">
                                    <div class="table-responsive">
                                        <table class="table table-condensed">
                                            <tbody><tr>
                                                    <td><code>ctid</code></td>
                                                    <td>The unique click ID. Pass this automatically to your network using the "Affiliate Tracking" feature in each link you create. Retrieve this value from your network's subid parameter.</td>
                                                </tr>
                                                <tr>
                                                    <td><code>gn</code></td>
                                                    <td>The conversion name, alphabets and numbers only.</td>
                                                </tr>
                                                <tr>
                                                    <td><code>gt</code></td>
                                                    <td>Specify either SALE or LEAD only.</td>
                                                </tr>
                                                <tr>
                                                    <td><code>gv</code></td>
                                                    <td>The value of the conversion, two decimal points without comma or curency symbols.</td>
                                                </tr>
                                                <tr>
                                                    <td><code>rid</code></td>
                                                    <td>The unique conversion tracking value, usually generated by your shopping cart or lead generation system.</td>
                                                </tr>
                                            </tbody></table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </fieldset>
                <?php
            } else {
                ?>
                <div class="alert alert-block alert-info"><i class="fa fa-info-circle"></i> This feature is not available in <?php echo get_option(SH_PREFIX . 'license_scheme_name'); ?>. <a href="http://www.shortywp.com/?utm_source=wordpress&amp;utm_medium=referral&amp;utm_campaign=shorty_upgrade" target="_blank" class="alert-link">Upgrade now</a> to get additional features and dedicated support.</div>
                <?php
            }
            ?>
        </div>
    </div>
    <div class="form-actions"><button name="btnSave" type="submit" class="btn btn-primary btn-lg"><i class="fa fa-check-circle"></i> Save Changes</button></div>
</form>