<div class="container-fluid">
    <div class="row">
        <div class="ltbody">
            <div class="page-header">
                <h2><i class="fa fa-random"></i> Campaign <small>|  New</small></h2>
                Create a new campaign  to track your advertising. Items marked <i class="fa fa-exclamation-circle text-silver"></i> are required. 
            </div>
            <form method="POST" name="frmNewCampaign">
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title">Campaign Details</div>
                            </div>
                            <div class="panel-body">
                                <fieldset>
                                    <div class="form-group <?php echo $this->form_error_class('link_id'); ?> <?php echo $this->form_error_class('tracking_link'); ?>">
                                        <label for="input3">   Choose Tracking Link: </label>
                                        <div class="controls">
                                            <input type="text" name="tracking_link" class="form-control typeahead" placeholder="Type keyword to choose.." data-items="4" value="<?php echo $this->set_value('tracking_link', ''); ?>">
                                            <input id="link_id" name="link_id" type="hidden" value="<?php echo $this->set_value('link_id', ''); ?>" />
                                            <?php echo $this->form_error_message('link_id'); ?> <?php echo $this->form_error_message('tracking_link'); ?>
                                            <p class="help-block">Choose the tracking link you want to use for your campaign.</p>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset><legend>Add Campaign Parameters</legend>
                                    <div class="form-group <?php echo $this->form_error_class('source'); ?>">
                                        <label for="source"><i class="fa fa-exclamation-circle text-silver"></i> Source: </label>
                                        <div class="controls">
                                            <input type="text" class="form-control" name="source" value="<?php echo $this->set_value('source', ''); ?>">
                                            <?php echo $this->form_error_message('source'); ?>
                                            <p class="help-block">The referring source. E.g: google, facebook, citysearch, soloads, newsletter4.</p>
                                        </div>
                                    </div>
                                    <div class="form-group <?php echo $this->form_error_class('medium'); ?>">
                                        <label for="medium">Medium: </label>
                                        <div class="controls">
                                            <input type="text" class="form-control" name="medium" value="<?php echo $this->set_value('medium', ''); ?>">
                                            <?php echo $this->form_error_message('medium'); ?>
                                            <p class="help-block">Marketing medium. E.g: cpc, banner, email. <a href="https://support.google.com/analytics/answer/1191184?hl=en" target="_blank">Read more about MCF Channels</a>.</p>
                                        </div>
                                    </div>
                                    <div class="form-group <?php echo $this->form_error_class('campaign'); ?>">
                                        <label for="campaign">Campaign: </label>
                                        <div class="controls">
                                            <input type="text" class="form-control" name="campaign" value="<?php echo $this->set_value('campaign', ''); ?>">
                                            <?php echo $this->form_error_message('campaign'); ?>
                                            <p class="help-block">Product, promo code, or slogan. E.g: christmas sale, exit offer, launch email.</p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="content">Content: </label>
                                        <div class="controls">
                                            <input type="text" class="form-control" name="content" value="<?php echo $this->set_value('content', ''); ?>">
                                            <p class="help-block">Use to differentiate ads. E.g: 250x250 banner, followup2, autoresponder6.</p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="term">Term: </label>
                                        <div class="controls">
                                            <input type="text" class="form-control" name="term" value="<?php echo $this->set_value('term', ''); ?>">
                                            <p class="help-block">Identify the paid keywords used.</p>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title">Campaign Cost</div>
                            </div>
                            <div class="panel-body">
                                <fieldset>
                                    <div class="control-group">
                                        <label class="control-label" for="calculate_cost">Calculate Cost: </label>
                                        <div class="controls">
                                            <div class="switch btn-group btn-group-sm" data-toggle="buttons-radio" style="margin-bottom:10px;">
                                                <button type="button" data-value="1" class="btn btn-small <?php echo $this->set_value('calculate_cost') == 1 ? 'active' : ''; ?>" data-target="#cpc">On</button>
                                                <button type="button" data-value="0" class="btn btn-small <?php echo $this->set_value('calculate_cost', 0) == 0 ? 'active' : ''; ?>" data-target="#">Off</button>
                                            </div>
                                            <p class="help-block">Turn this option on if you want to calculate the cost per click from this campaign.</p>
                                        </div>
                                        <input id="calculate_cost" name="calculate_cost" class="switch-data" type="hidden"/>
                                        <div id="cpc" class="collapse out form-group <?php echo $this->form_error_class('cpc'); ?>">
                                            <label for="cpc"><i class="fa fa-exclamation-circle text-silver"></i> Cost Per Click: </label>
                                            <div class="controls">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="cpc" placeholder="0.00" value="<?php echo $this->set_value('cpc', ''); ?>">
                                                    <span class="input-group-addon"><?php echo get_option(SH_PREFIX . 'settings_currency'); ?></span>                </div>
                                                <?php echo $this->form_error_message('cpc'); ?>
                                                <p class="help-block">Enter the cost per click here. The currency is defined in the <a href="?page=sh_settings_page">settings</a> page.</p>
                                            </div>
                                        </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions"><button name="btnCreateCampaign" type="submit" class="btn btn-primary btn-lg"><i class="fa fa-check-circle"></i> Create Campaign</button></div>
            </form>
        </div>
    </div>
</div>