<div id="split_test_new" class="container-fluid">
    <div class="row">
        <div class="ltbody">
            <div class="page-header">
                <h2><i class="fa fa-random"></i> New Split Test</small>
                </h2>
                Create a rotator to split test your tracking links. Items marked <i
                    class="fa fa-exclamation-circle text-silver"></i> are required.
            </div>

            <div class="form">
                <form method="POST" name="frmNewSplitTest">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title">Rotator Details</div>
                        </div>
                        <div class="panel-body">
                            <fieldset>
                                <div class="row">
                                    <div class="form-groupc col-md-6 <?php echo $this->form_error_class('split_test_name'); ?>">
                                        <label class="control-label" for="split_test_name"><i class="fa fa-exclamation-circle text-silver"></i> Split Test Name: </label>
                                        <div class="controls">
                                            <input name="split_test_name" type="text" class="form-control" value="<?php echo $this->set_value('split_test_name'); ?>">
                                            <p class="help-block">A reference name for your split test.</p>
                                        </div>
                                        <?php echo $this->form_error_message('split_test_name'); ?>
                                    </div>
                                    <div class="form-group col-md-6 <?php echo $this->form_error_class('tracking_link'); ?>">
                                        <label class="control-label" for="tracking_link"><i class="fa fa-exclamation-circle text-silver"></i> Split Test Link: </label>
                                        <div class="controls">
                                            <div class="input-group"><span class="input-group-addon"><?php echo $this->current_domain(FALSE, TRUE, TRUE); ?>r/</span>
                                                <input name="tracking_link" type="text" class="form-control" placeholder="something" value="<?php echo $this->set_value('tracking_link', $tracking_link); ?>">
                                            </div>
                                            <?php echo $this->form_error_message('tracking_link'); ?>
                                            <p class="help-block">This is a redirect link used to mask and track the split test.</p>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title">Traffic Allocation</div>
                        </div>
                        <input id="allocation_fields_count" type="hidden" name="allocation_fields_count" value="<?php echo $this->set_value('allocation_fields_count', $allocation_fields_count); ?>" />
                        <div  class="panel-body">
                            <fieldset id="allocation_section" >
                                <?php
                                for ($i = 1; $i <= $allocation_fields_count; $i++) {
                                    ?>
                                    <fieldset>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group <?php echo $this->form_error_class('tracker_' . $i); ?>">
                                                    <label for="input28"> Choose Tracker: </label>
                                                    <div class="controls">
                                                        <input name="tracker_<?php echo $i; ?>" type="text" class="form-control typeahead" style="margin: 0 auto;" value="<?php echo $this->set_value('tracker_' . $i); ?>" >
                                                        <input name="split_test_allocations_id_" type="hidden" value="0" />
                                                    </div>
                                                    <?php echo $this->form_error_message('tracker_' . $i); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group <?php echo $this->form_error_class('traffic_' . $i); ?>">
                                                    <label for="input29"> Traffic: </label>
                                                    <div class="controls">
                                                        <div class="input-group">
                                                            <div class="input-group">
                                                                <input name="traffic_<?php echo $i; ?>" type="text" class="form-control" placeholder="50" value="<?php echo $this->set_value('traffic_' . $i); ?>">
                                                                <span class="input-group-addon">%</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php echo $this->form_error_message('traffic_' . $i); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="table-responsive">
                                                    <table class="table table-condensed table-bordered" style="font-size:12px;">
                                                        <tr>
                                                            <th>Visits</th>
                                                            <th>Visitors</th>
                                                            <th>Conv.</th>
                                                            <th>Conv. %</th>
                                                            <th>Cost</th>
                                                            <th>CPA</th>
                                                            <th>CPC</th>
                                                            <th>Revenue</th>
                                                            <th>RPV</th>
                                                            <th>Profit</th>
                                                        </tr>
                                                        <tr role="row">
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0.00</td>
                                                            <td>$0.00</td>
                                                            <td>$0.00</td>
                                                            <td>$0.00</td>
                                                            <td>$0.00</td>
                                                            <td>$0.00</td>
                                                            <td>$0.00</td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <button type="button" class=" btnRemoveAllocation btn btn-default btn-xs">Remove</button></div>
                                        </div>
                                        <hr>
                                    </fieldset>
                                    <?php
                                }
                                ?>
                            </fieldset>
                            <button type="button" class="btnAddAllocation btn btn-sm btn-default">Add another tracker</button>
                        </div>
                    </div>
                    <div class="col-md-6"></div>

                    <div class="form-actions">
                        <button name="btnAdd" type="submit" class="btn btn-primary btn-lg"><i class="fa fa-check-circle"></i> Create Split Test</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
