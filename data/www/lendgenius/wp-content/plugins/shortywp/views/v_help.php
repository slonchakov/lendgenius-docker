<div class="page-header">
    <h2><i class="fa fa-heart"></i> License &amp; Help</h2>
    Check your license key and get important data about our support.
</div>
<form method="POST" class="form" action="?page=sh_help">
    <div class="row">
        <?php
        if (trim(SH_LICENSE) == '') {
            ?>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">License &amp; Support</div>
                    </div>
                    <div class="panel-body">
                        <fieldset>
                            <div class="form-group">
                                <label class="control-label" for="license_key"><i class="fa fa-exclamation-circle text-silver"></i> License Key: </label>
                                <div class="controls">
                                    <input type="text" class="form-control" name="license_key" value="<?php echo (!(bool) get_option(SH_PREFIX . 'is_demo_mode')) ? $license_key : ''; ?>">
                                    <p class="help-block">Please get your license key from the members area.</p>
                                    <p class="help-block">If your license stopped validating, click <em>"Save Changes"</em> again below to check status. Tracking is not effected when your license doesn't validate temporarily.</p>
                                </div>
                            </div>
                            <?php echo isset($license) ? $license : ''; ?>
                            <p>Support is only available to users with active support &amp; updates renewal.</p>
                            <p>You can renew your subscription, upgrade or download the latest version <a href="http://www.kreydle.com/clients/member/" target="_blank">at the members area.</a></p>

                        </fieldset>
                    </div>
                </div>
            </div>
            <?php
        }

        if (trim(SH_LICENSE) == '') {
            ?>
            <div class="col-md-8">
                <?php
            } else {
                ?>
                <div class="col-md-12">
                    <?php
                }
                ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">Backup &amp; Reset</div>
                    </div>
                    <div class="panel-body">
                        <fieldset>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="controls">
                                            <a href="?page=sh_help&action=download" class="btn btn-default btn-block"><i class="fa fa-download"></i> Download Data</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8 text-muted">Download complete information for your links.</div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="controls">
                                            <a href="?page=sh_help&action=clear_reports" class="btn btn-warning btn-block confirm" data-message="Are you sure you want to clear reports?"><i class="fa fa-history"></i> Clear Reports</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8 text-muted">Safely clear your report data without losing your links, campaigns, goals or settings.</div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="controls">
                                            <button id="btnNuke" type="button" class="btn btn-danger btn-block" ><i class="fa fa-trash"></i> Nuke Everything</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8 text-muted">Wipe out all data and start over again. You will be required to enter your license information.</div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">Legacy Features</div>
                    </div>
                    <div class="panel-body">
                        <p>Here are some old features that will be removed completely in the next update. You can access them for now to copy codes and links, but you cannot any anything new: </p>
                        <ul>
                            <li><a href="?page=sh_goals_page">Old goals &amp; conversions page</a></li>
                            <li><a href="?page=sh_campaigns_page">Old campaign tracking page</a></li>
                        </ul>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">Important Stuff</div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="control-label" for="support_token">Support Token: </label>
                            <div class="controls">
                                <textarea rows="15" class="form-control" id="support_token"><?php echo $info; ?></textarea>
                                <p class="help-block">Please provide this token to our customer support team. It will help us identify important technical data about your hosting so we can help serve you faster.</p>
                            </div>
                        </div>
                        <p><strong>News & Updates</strong></p>
                  <script src="http://feeds.feedburner.com/shortywp?format=sigpro" type="text/javascript" ></script><noscript><p>Subscribe to RSS headline updates from: <a href="http://feeds.feedburner.com/shortywp"></a><br/>Powered by FeedBurner</p> </noscript>
                    </div>
                </div>
            </div>
        </div>
        <div class=""><button type="submit" name="btnLicenseKey" class="btn btn-primary btn-lg"><i class="fa fa-check-circle"></i> Save Changes</button></div>
    </div>

</form>
<div class="modal fade" id="nuke" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Nuke Warning</h4>
            </div>
            <div class="modal-body">
                <p>Sorry dude, this button is so important that we just had to annoy you with another popup so that you don't nuke innocent civilians.</p>
                <p>Everything will be wiped clean and nothing will survive this.</p>
                <p>Are you sure? <strong>I repeat:</strong> are you sure?</p>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times-circle"></i> Abort Nuke</button>
                <a href="?page=sh_help&action=nuke" class="btn btn-danger"><i class="fa fa-check-circle"></i> Yes, Nuke Everything Now!</a>
            </div>
        </div>
    </div>
</div>