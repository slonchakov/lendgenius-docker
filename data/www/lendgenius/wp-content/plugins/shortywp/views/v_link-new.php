<div class="container-fluid">
    <div class="row">
        <div class="ltbody">
            <div class="page-header">
                <h2><i class="fa fa-list-alt"></i> New Tracker</small>
                </h2>
                Create a new tracking link to track clicks and visitors. Items marked <i
                    class="fa fa-exclamation-circle text-silver"></i> are required.
            </div>
            <?php
            if (!get_option('permalink_structure')) {
                ?>
                <div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> You are using the default Wordpress permalink. <a href="/wp-admin/options-permalink.php" class="alert-link">Please change it</a> to a "pretty" link or your cloaked URLs will not work!</div>
                <?php
            }
            ?>
            <div class="form">
                <form method="POST" name="frmNewLink">
                    <div class="row">
                        <div class="col-md-6">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="panel-title">BASIC TRACKING</div>
                                </div>
                                <div class="panel-body">
                                    <fieldset>
                                        <div class="form-group <?php echo $this->form_error_class('link_name'); ?>">
                                            <label class="control-label" for="link_name"><i class="fa fa-exclamation-circle text-silver"></i> Tracker
                                                Name: </label>

                                            <div class="controls">
                                                <input type="text" class="form-control" id="link_name" name="link_name" placeholder="Facebook Link"
                                                       value="<?php echo $this->set_value('link_name', ''); ?>">
                                                       <?php echo $this->form_error_message('link_name'); ?>
                                                <p class="help-block">A reference name for your link. Make it easy to remember so you can find it
                                                    later.</p>
                                            </div>
                                        </div>
                                        <div class="form-group <?php echo $this->form_error_class('primary_url'); ?>">
                                            <label class="control-label" for="primary_url"><i class="fa fa-exclamation-circle text-silver"></i>
                                                Primary URL: </label>

                                            <div class="controls has-feedback">
                                                <input type="text" class="form-control" id="primary_url" name="primary_url" placeholder="http://"
                                                       value="<?php echo $this->set_value('primary_url', ''); ?>">
                                                <span id="url_availability" class="form-control-feedback" aria-hidden="true"></span>
                                                <?php echo $this->form_error_message('primary_url'); ?>
                                                <p class="help-block">The actual URL of the link you want to track. This could be a URL to your own
                                                    website, or an affiliate link.</p>
                                            </div>
                                        </div>
                                        <div class="form-group <?php echo $this->form_error_class('mobile_url'); ?>">
                                            <label class="control-label" for="mobile_url"> Mobile URL: </label>

                                            <div class="controls has-feedback">
                                                <input name="mobile_url" type="text" class="form-control" id="mobile_url" placeholder="http://"
                                                       value="<?php echo $this->set_value('mobile_url', ''); ?>">
                                                <span id="url_availability3" class="form-control-feedback" aria-hidden="true"></span>
                                                <?php echo $this->form_error_message('mobile_url'); ?>
                                                <p class="help-block">The mobile URL is used instead of your Primary URL for visitors using mobile devices.</p>
                                            </div>
                                        </div>
                                        <div class="form-group <?php echo $this->form_error_class('tracking_link'); ?>">
                                            <label class="control-label" for="tracking_link"><i class="fa fa-exclamation-circle text-silver"></i>
                                                Cloaked Link: </label>

                                            <div class="controls">
                                                <div class="input-group"><span
                                                        class="input-group-addon"><?php echo $this->current_domain(FALSE, FALSE, TRUE); ?></span>
                                                    <input type="text" class="form-control" id="tracking_link" name="tracking_link"
                                                           placeholder="something"
                                                           value="<?php echo $this->set_value('tracking_link', $tracking_link); ?>">
                                                </div>
                                                <?php echo $this->form_error_message('tracking_link'); ?>
                                                <p class="help-block">This is a redirect link used to mask and track the Destination URL. Make it
                                                    short and easy to remember.</p>
                                            </div>
                                        </div>
                                        <div id="reference_tags" class="form-group">
                                            <label class="control-label" for="reference_tags">Reference Tags: </label>
                                            <div class="controls">
                                                <input type="text" class="form-control" name="reference_tags" value="<?php echo $this->set_value('reference_tags', ''); ?>" placeholder="keyword1, keyword 2, keyword 3">
                                                <p class="help-block">Use tags instead of groups to organize your links and find them easily later.</p>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <div class="control-group">
                                            <label for="cloaking_status_enable">Cloaking: </label>

                                            <div class="controls">
                                                <div class="switch btn-group btn-group-sm" data-toggle="buttons-radio" style="margin-bottom:10px;">
                                                    <button type="button" data-value="1"
                                                            class="btn btn-small <?php echo $this->set_value('cloaking_status_enable') == 1 ? 'active' : ''; ?>"
                                                            data-target="#ct">On
                                                    </button>
                                                    <button type="button" data-value="0"
                                                            class="btn btn-small <?php echo $this->set_value('cloaking_status_enable', 0) == 0 ? 'active' : ''; ?>"
                                                            data-target="#">Off
                                                    </button>
                                                </div>
                                                <p class="help-block">You can choose to cloak this link and display a viral bar. <span
                                                        class="text-error"><strong>NOTE:</strong></span> Some websites block iframes, so our
                                                    cloaking feature will not work on their pages. Please also check with your affiliate network to
                                                    confirm that cloaking is not against their terms.</p>
                                            </div>
                                            <input type="hidden" id="cloaking_status_enable" name="cloaking_status_enable" class="switch-data"/>
                                        </div>
                                        <!--collapse-->
                                        <div class="collapse out form-group" id="ct">
                                            <label class="control-label" for="cloaking_type">Cloaking Type: </label>

                                            <div class="controls">
                                                <select id="cloaking_type" name="cloaking_type" class="form-control">
                                                    <option
                                                        value="<?php echo CLOAKING_TYPE_BASIC; ?>" <?php echo selected($this->set_value('cloaking_type'), CLOAKING_TYPE_BASIC, true); ?>><?php echo CLOAKING_TYPE_BASIC; ?></option>
                                                    <option
                                                        value="<?php echo CLOAKING_TYPE_VIRAL; ?>" <?php echo selected($this->set_value('cloaking_type'), CLOAKING_TYPE_VIRAL, true); ?>><?php echo CLOAKING_TYPE_VIRAL; ?></option>
                                                </select>

                                                <p class="help-block">You can opt to display or hide the viral bar.</p>
                                            </div>
                                            <fieldset class="cloaking_type_child collapse out ">
                                                <label class="control-label" for="bar_position">Bar Position: </label>

                                                <div class="controls">
                                                    <select id="bar_position" name="bar_position" class="form-control">
                                                        <option
                                                            value="<?php echo BAR_POSITION_TOP; ?>" <?php echo selected($this->set_value('bar_position'), BAR_POSITION_TOP, true); ?>><?php echo BAR_POSITION_TOP; ?></option>
                                                        <option
                                                            value="<?php echo BAR_POSITION_BOTTOM; ?>" <?php echo selected($this->set_value('bar_position'), BAR_POSITION_BOTTOM, true); ?>><?php echo BAR_POSITION_BOTTOM; ?></option>
                                                    </select>

                                                    <p class="help-block">You can opt to display viral bar on top or bottom.</p>
                                                </div>
                                            </fieldset>
                                            <label class="control-label" for="frame_content">Frame Content: </label>

                                            <div class="controls">
                                                <select id="frame_content" name="frame_content" class="form-control">
                                                    <option
                                                        value="<?php echo FRAME_CONTENT_VISIBLE; ?>" <?php echo selected($this->set_value('frame_content', FRAME_CONTENT_HIDDEN), FRAME_CONTENT_VISIBLE, true); ?>>
                                                        Visible to search engines (index)
                                                    </option>
                                                    <option
                                                        value="<?php echo FRAME_CONTENT_HIDDEN; ?>" <?php echo selected($this->set_value('frame_content', FRAME_CONTENT_HIDDEN), FRAME_CONTENT_HIDDEN, true); ?>>
                                                        Hidden from search engines (noindex)
                                                    </option>
                                                </select>

                                                <p class="help-block">You can choose to hide the content of the cloaked link from search engines
                                                    (recommended) or make it visible.</p>
                                            </div>
                                            <fieldset class=" ">
                                                <div class="form-group <?php echo $this->form_error_class('meta_title'); ?>">
                                                    <label class="control-label" for="meta_title">Meta Title: </label>

                                                    <div class="controls">
                                                        <!--<div class="input-group">-->
                                                        <input type="text" class="form-control" name="meta_title"
                                                               value="<?php echo $this->set_value('meta_title'); ?>">
                                                        <!--<div class="input-group-btn"><a href="#" class="btn btn-default">Get</a></div>-->
                                                        <!--</div>-->
                                                        <?php echo $this->form_error_message('meta_title'); ?>
                                                        <p class="help-block">Create a compelling and interesting title. This title will be used
                                                            when posting your link to social media services, and in all public places.</p>
                                                    </div>
                                                </div>
                                                <div class="form-group <?php echo $this->form_error_class('meta_description'); ?>">
                                                    <label class="control-label" for="meta_description">Meta Description: </label>

                                                    <div class="controls">
                                                        <!--<div class="input-group">-->
                                                        <input type="text" class="form-control" name="meta_description"
                                                               value="<?php echo $this->set_value('meta_description'); ?>">
                                                        <!--<div class="input-group-btn"><a href="#" class="btn btn-default">Get</a></div>-->
                                                        <!--</div>-->
                                                        <?php echo $this->form_error_message('meta_description'); ?>
                                                        <p class="help-block">Type in a brief description about the link. This description will be
                                                            used when posting your link to social media services, and in all public places.</p>
                                                    </div>
                                                </div>
                                                <div class="form-group <?php echo $this->form_error_class('meta_image'); ?>">
                                                    <label class="control-label" for="label47">Meta Image: </label>

                                                    <div class="controls">
                                                        <input name="meta_image" type="text" class="form-control" id="meta_image"
                                                               value="<?php echo $this->set_value('meta_image'); ?>">
                                                               <?php echo $this->form_error_message('meta_image'); ?>
                                                        <p class="help-block">The image will be automatically picked up and displayed by social
                                                            media services when someone shares your cloaked link.</p>
                                                    </div>
                                                </div>
                                            </fieldset>

                                            <div class="form-group">
                                                <label class="control-label" for="retargeting_code">Custom Codes: </label>
                                                <div class="controls">
                                                    <textarea name="retargeting_code" type="text" class="form-control" ><?php echo $this->set_value('retargeting_code'); ?></textarea>
                                                    <p class="help-block">You can enter other codes in here, for example an optin popup.</p>
                                                </div>
                                            </div>

                                        </div>
                                    </fieldset>

                                    <fieldset>
                                        <div class="control-group">
                                            <label for="split_test_enable" >Split Testing: </label>
                                            <div class="controls">
                                                <div class="switch btn-group btn-group-sm" data-toggle="buttons-radio" style="margin-bottom:10px;">
                                                    <button type="button" data-value="1" class="btn btn-small <?php echo $this->set_value('split_test_enable') == 1 ? 'active' : ''; ?>" data-target="#splt">On</button>
                                                    <button type="button" data-value="0" class="btn btn-small <?php echo $this->set_value('split_test_enable') == 0 ? 'active' : ''; ?>" data-target="#">Off</button>
                                                </div>
                                                <p class="help-block">The total traffic percentage must equal 100%.  If you need to specify a different tag format for SubID tracking, put the code <code>%%CTID%%</code> as a placeholder. </p>
                                            </div>
                                            <input type="hidden" id="split_test_enable" name="split_test_enable" class="switch-data" />
                                        </div>
                                        <!--collapse-->
                                        <div class="collapse out form-group" id="splt">
                                            <div class="row">
                                                <div class="col-md-9">
                                                    <div class="form-group">
                                                        <label for="split_primary_url"> Primary URL: </label>
                                                        <div class="controls">
                                                            <input id="split_primary_url" type="text" disabled="disabled" class="form-control" placeholder="Enter split testing URL here.." style="margin: 0 auto;" value="<?php echo $link->destination_url; ?>" >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="input23"> Traffic: </label>
                                                        <div class="controls">
                                                            <div class="input-group">
                                                                <div class="input-group">
                                                                    <input name="split_primary_url_value" type="text" class="form-control" id="label3" placeholder="50" value="<?php echo $this->set_value('split_primary_url_value', $split_test_data[0]->weight); ?>">
                                                                    <span class="input-group-addon">%</span></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <input id="allocation_fields_count" type="hidden" name="allocation_fields_count" value="<?php echo $this->set_value('allocation_fields_count', $allocation_fields_count); ?>" />
                                            <?php
                                            if ($allocation_fields_count == 0) {
                                                ?>
                                                <div class="row <?php echo $this->form_error_class('split_test_url_1'); ?> <?php echo $this->form_error_class('split_test_value_1'); ?>">
                                                    <div class="col-md-9">
                                                        <div class="form-group">
                                                            <label for="input22"> Split Test URL: </label>
                                                            <div class="controls">
                                                                <input name="split_test_url_1" value="<?php echo $this->set_value('split_test_url_1'); ?>" type="text" class="form-control" style="margin: 0 auto;" >
                                                            </div>
                                                        </div>
                                                        <?php echo $this->form_error_message('split_test_url_1'); ?>
                                                        <?php echo $this->form_error_message('split_test_value_1'); ?>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="input23"> Traffic: </label>
                                                            <div class="controls">
                                                                <div class="input-group">
                                                                    <div class="input-group">
                                                                        <input name="split_test_value_1" value="<?php echo $this->set_value('split_test_value_1'); ?>" type="text" class="form-control" placeholder="50">
                                                                        <span class="input-group-addon">%</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            ?>

                                            <?php
                                            for ($i = 1; $i <= $allocation_fields_count; $i++) {
                                                ?>
                                                <div class="row <?php echo $this->form_error_class('split_test_url_' . $i); ?> <?php echo $this->form_error_class('split_test_value_' . $i); ?>">
                                                    <div class="col-md-9">
                                                        <div class="form-group">
                                                            <label for="input22"> Split Test URL: </label>
                                                            <div class="controls">
                                                                <input name="split_test_url_<?php echo $i; ?>" value="<?php echo $this->set_value('split_test_url_' . $i); ?>" type="text" class="form-control" style="margin: 0 auto;" >
                                                            </div>
                                                        </div>
                                                        <?php echo $this->form_error_message('split_test_url_' . $i); ?>
                                                        <?php echo $this->form_error_message('split_test_value_' . $i); ?>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="input23"> Traffic: </label>
                                                            <div class="controls">
                                                                <div class="input-group">
                                                                    <div class="input-group">
                                                                        <input name="split_test_value_<?php echo $i; ?>" value="<?php echo $this->set_value('split_test_value_' . $i); ?>" type="text" class="form-control" placeholder="50">
                                                                        <span class="input-group-addon">%</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <?php
                                            }
                                            ?>

                                            <section id="allocation_section"></section>
                                            <div class="form-group">
                                                <div class="controls"> <button type="button" class="btnAddAllocation btn btn-xs btn-default">Add MORE</button>
                                                    <p class="help-block">To remove URLs, just delete it along with the traffic percentage and save this form. To deactivate URLs set the percentage to zero. </p>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>


                                </div>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="panel-title">ADVANCED TRACKING</div>
                                </div>
                                <div class="panel-body">
                                    <fieldset>

                                        <div class="control-group">
                                            <label for="param_tag_affiliate_tracking">Affiliate Tracking: </label>

                                            <div class="controls">
                                                <div class="switch btn-group btn-group-sm" data-toggle="buttons-radio" style="margin-bottom:10px;">
                                                    <button type="button" data-value="1"
                                                            class="btn btn-small <?php echo $this->set_value('param_tag_affiliate_tracking') == 1 ? 'active' : ''; ?>"
                                                            data-target="#ast">On
                                                    </button>
                                                    <button type="button" data-value="0"
                                                            class="btn btn-small <?php echo $this->set_value('param_tag_affiliate_tracking', 0) == 0 ? 'active' : ''; ?>"
                                                            data-target="#">Off
                                                    </button>
                                                </div>
                                                <p class="help-block">Turn this on to automatically pass any tag within your campaigns to your
                                                    affiliate network's SubID parameters, as specified below.</p>
                                            </div>

                                            <input id="param_tag_affiliate_tracking" name="param_tag_affiliate_tracking" class="switch-data"
                                                   type="hidden" value="0"/>
                                        </div>

                                        <div class="collapse out form-group" id="ast">

                                            <label class="control-label" for="param_tag_affiliate_network">SubID Format: </label>
                                            <div class="controls">
                                                <select id="param_tag_affiliate_network" name="param_tag_affiliate_network" class="form-control">
                                                    <option
                                                        value="<?php echo AFFILIATE_NETWORK_AFF_SUB; ?>" <?php echo selected($this->set_value('param_tag_affiliate_network'), AFFILIATE_NETWORK_AFF_SUB, true); ?>><?php echo AFFILIATE_NETWORK_AFF_SUB; ?></option>
                                                    <option
                                                        value="<?php echo AFFILIATE_NETWORK_AFFTRACK; ?>" <?php echo selected($this->set_value('param_tag_affiliate_network'), AFFILIATE_NETWORK_AFFTRACK, true); ?>><?php echo AFFILIATE_NETWORK_AFFTRACK; ?></option>
                                                    <option
                                                        value="<?php echo AFFILIATE_NETWORK_TID; ?>" <?php echo selected($this->set_value('param_tag_affiliate_network'), AFFILIATE_NETWORK_TID, true); ?>><?php echo AFFILIATE_NETWORK_TID; ?></option>
                                                    <option
                                                        value="<?php echo AFFILIATE_NETWORK_SID; ?>" <?php echo selected($this->set_value('param_tag_affiliate_network'), AFFILIATE_NETWORK_SID, true); ?>><?php echo AFFILIATE_NETWORK_SID; ?></option>
                                                    <option
                                                        value="<?php echo AFFILIATE_NETWORK_S1; ?>" <?php echo selected($this->set_value('param_tag_affiliate_network'), AFFILIATE_NETWORK_S1, true); ?>><?php echo AFFILIATE_NETWORK_S1; ?></option>
                                                    <option
                                                        value="<?php echo AFFILIATE_NETWORK_U1; ?>" <?php echo selected($this->set_value('param_tag_affiliate_network'), AFFILIATE_NETWORK_U1, true); ?>><?php echo AFFILIATE_NETWORK_U1; ?></option>
                                                    <option
                                                        value="<?php echo AFFILIATE_NETWORK_C1; ?>" <?php echo selected($this->set_value('param_tag_affiliate_network'), AFFILIATE_NETWORK_C1, true); ?>><?php echo AFFILIATE_NETWORK_C1; ?></option>
                                                    <option
                                                        value="<?php echo AFFILIATE_NETWORK_SUBID; ?>" <?php echo selected($this->set_value('param_tag_affiliate_network'), AFFILIATE_NETWORK_SUBID, true); ?>><?php echo AFFILIATE_NETWORK_U1; ?></option>
                                                    <option
                                                        value="<?php echo AFFILIATE_NETWORK_SUBID1; ?>" <?php echo selected($this->set_value('param_tag_affiliate_network'), AFFILIATE_NETWORK_SUBID1, true); ?>><?php echo AFFILIATE_NETWORK_U1; ?></option>
                                                    <option
                                                        value="<?php echo AFFILIATE_NETWORK_UID; ?>" <?php echo selected($this->set_value('param_tag_affiliate_network'), AFFILIATE_NETWORK_UID, true); ?>><?php echo AFFILIATE_NETWORK_UID; ?></option>
                                                    <option <?php echo selected($this->set_value('param_tag_affiliate_network'), 'Custom', true); ?>>
                                                        Custom
                                                    </option>
                                                </select>

                                                <p class="help-block">If you're not sure which SubID tag to use, <a href="#subid_types"
                                                                                                                    data-toggle="collapse">refer to
                                                        this chart</a> for most popular affiliate networks and systems.</p>

                                                <div id="subid_types" class="out collapse" style="margin-top: 10px;">
                                                    <table class="table table-condensed">
                                                        <thead>
                                                            <tr>
                                                                <th>SubID</th>
                                                                <th>Network</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td><code>aff_sub</code></td>
                                                                <td>HasOffers, Adsimilis</td>
                                                            </tr>
                                                            <tr>
                                                                <td><code>afftrack</code></td>
                                                                <td>Shareasale</td>
                                                            </tr>
                                                            <tr>
                                                                <td><code>tid</code></td>
                                                                <td>ClickBank, JVZoo</td>
                                                            </tr>

                                                            <tr>
                                                                <td><code>sid</code></td>
                                                                <td>CJ, PepperJam, ClickPromise, CAKE, LinkTrust</td>
                                                            </tr>
                                                            <tr>
                                                                <td><code>s1</code></td>
                                                                <td>PeerFly, A4D, Above All Offers, C2M, Cash Network, Clickbooth, GlobalWide,
                                                                    MaxBounty, NeverBlue, XY7
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><code>u1</code></td>
                                                                <td>LinkShare</td>
                                                            </tr>
                                                            <tr>
                                                                <td><code>c1</code></td>
                                                                <td>HitPath, W4</td>
                                                            </tr>
                                                            <tr>
                                                                <td><code>subid</code></td>
                                                                <td>CPAWay</td>
                                                            </tr>
                                                            <tr>
                                                                <td><code>uid</code></td>
                                                                <td>CueLinks</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <p class="alert alert-info">
                                                        Some networks may allow more than one SubID to be passed along with your affiliate link.
                                                        However you only need to use one.
                                                    </p>
                                                </div>
                                            </div>

                                            <div
                                                class="collapse out param_tag_affiliate_network_custom control-group <?php echo $this->form_error_class('param_tag_affiliate_network_custom'); ?>">
                                                <label class="control-label" for="sub_id_custom"><i
                                                        class="fa fa-exclamation-circle text-silver"></i> Custom SubID: </label>

                                                <div class="controls">
                                                    <input name="param_tag_affiliate_network_custom" maxlength="12" class="form-control" type="text"
                                                           placeholder="custom.."
                                                           value="<?php echo $this->set_value('param_tag_affiliate_network_custom', ''); ?>"/>
                                                           <?php echo $this->form_error_message('param_tag_affiliate_network_custom'); ?>
                                                    <p class="help-block">Specify your own SubID format. Make sure your SubID allows up to 12
                                                        characters.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>

                                    <div class="control-group">
                                        <label for="param_tag_forward_param">Forward Parameters: </label>

                                        <div class="controls">
                                            <div class="switch btn-group btn-group-sm" data-toggle="buttons-radio" style="margin-bottom:10px;">
                                                <button type="button" data-value="1"
                                                        class="btn btn-small <?php echo $this->set_value('param_tag_forward_param') == 1 ? 'active' : ''; ?>"
                                                        data-target="#">On
                                                </button>
                                                <button type="button" data-value="0"
                                                        class="btn btn-small <?php echo $this->set_value('param_tag_forward_param', 0) == 0 ? 'active' : ''; ?>"
                                                        data-target="#">Off
                                                </button>
                                            </div>
                                            <p class="help-block">Turn this on to automatically forward all parameters from tracking link to
                                                destination URL</p>
                                        </div>
                                        <input id="param_tag_forward_param" name="param_tag_forward_param" class="switch-data" type="hidden"/>
                                    </div>

                                    <div class="control-group">
                                        <label for="param_tag_forward_campaign">Forward Campaign Tags: </label>

                                        <div class="controls">
                                            <div class="switch btn-group btn-group-sm" data-toggle="buttons-radio" style="margin-bottom:10px;">
                                                <button type="button" data-value="1"
                                                        class="btn btn-small <?php echo $this->set_value('param_tag_forward_campaign') == 1 ? 'active' : ''; ?>"
                                                        data-target="#">On
                                                </button>
                                                <button type="button" data-value="0"
                                                        class="btn btn-small <?php echo $this->set_value('param_tag_forward_campaign', 0) == 0 ? 'active' : ''; ?>"
                                                        data-target="#">Off
                                                </button>
                                            </div>
                                            <p class="help-block">Turn this on to automatically pass any campaign tag within your campaigns to
                                                the destination URL using the correct <code>utm_</code> format. Do not enable if you are not
                                                promoting your own website.</p>
                                        </div>
                                        <input id="param_tag_forward_campaign" name="param_tag_forward_campaign" class="switch-data"
                                               type="hidden"/>
                                    </div>

                                    <fieldset>
                                        <div class="control-group">
                                            <label for="param_tag_retargeting">Retargeting: </label>

                                            <div class="controls">
                                                <div class="switch btn-group btn-group-sm" data-toggle="buttons-radio" style="margin-bottom:10px;">
                                                    <button type="button" data-value="1"
                                                            class="btn btn-small <?php echo $this->set_value('param_tag_retargeting') == 1 ? 'active' : ''; ?>"
                                                            data-target="#rc">On
                                                    </button>
                                                    <button type="button" data-value="0"
                                                            class="btn btn-small <?php echo $this->set_value('param_tag_retargeting') == 0 ? 'active' : ''; ?>"
                                                            data-target="#">Off
                                                    </button>
                                                </div>
                                                <p class="help-block">Enable retargeting to buy ads for people who have clicked on this link. No iFrames are used.</p>
                                            </div>

                                            <input id="param_tag_retargeting" name="param_tag_retargeting" class="switch-data" type="hidden" value="0"/>
                                        </div>

                                        <div class="collapse out form-group" id="rc">
                                            <div class="control-group">
                                                <label class="control-label" for="retargeting_adwords">Google AdWords Code: </label>
                                                <div class="controls">
                                                    <textarea name="retargeting_adwords" type="text" class="form-control" ><?php echo $this->set_value('retargeting_adwords'); ?></textarea>
                                                    <p class="help-block">Enter your Google AdWords retargeting codes in here.</p>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label" for="retargeting_fb">FaceBook Pixel Code: </label>
                                                <div class="controls">
                                                    <textarea name="retargeting_fb" type="text" class="form-control" ><?php echo $this->set_value('retargeting_fb'); ?></textarea>
                                                    <p class="help-block">Enter your Facebook retargeting codes in here.</p>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label" for="retargeting_adroll">AdRoll Code: </label>
                                                <div class="controls">
                                                    <textarea name="retargeting_adroll" type="text" class="form-control"><?php echo $this->set_value('retargeting_adroll'); ?></textarea>
                                                    <p class="help-block">Enter your AdRoll retargeting codes in here.</p>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label" for="retargeting_perfect">Perfect Audience Code: </label>
                                                <div class="controls">
                                                    <textarea name="retargeting_perfect" type="text" class="form-control" ><?php echo $this->set_value('retargeting_perfect'); ?></textarea>
                                                    <p class="help-block">Enter your Perfect Audience retargeting codes in here.</p>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label" for="retargeter_code">Retargeter Code: </label>
                                                <div class="controls">
                                                    <textarea name="retargeter_code" type="text" class="form-control" ><?php echo $this->set_value('retargeter_code'); ?></textarea>
                                                    <p class="help-block">Well... you get the point!</p>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>


                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="panel-title">Link Automation</div>
                                </div>
                                <div class="panel-body">
                                    <fieldset>
                                        <div class="control-group">
                                            <label class="control-label" for="label45">Uptime monitoring: </label>

                                            <div class="controls">
                                                <div class="switch btn-group btn-group-sm" data-toggle="buttons-radio"
                                                     style="margin-bottom:10px;">
                                                    <button type="button" data-value="1"
                                                            class="btn btn-small <?php echo $this->set_value('uptime_monitoring_enabled') == 1 ? 'active' : ''; ?>"
                                                            data-target="#uptime">On
                                                    </button>
                                                    <button type="button" data-value="0"
                                                            class="btn btn-small <?php echo $this->set_value('uptime_monitoring_enabled') == 0 ? 'active' : ''; ?>"
                                                            data-target="#">Off
                                                    </button>
                                                </div>
                                                <p class="help-block">Monitor the primary destination URL and alert by email if there is a
                                                    downtime. Turning this one may cause require more work for your server, so do it only if its
                                                    critical!</p>
                                            </div>
                                            <input id="uptime_monitoring_enabled" name="uptime_monitoring_enabled" class="switch-data"
                                                   type="hidden"/>
                                        </div>
                                        <section id="uptime" class="collapse out">
                                            <?php
                                            if (in_array(get_option(SH_PREFIX . 'license_scheme_id'), array(SHORTY_AGENCY_LICENSE, SHORTY_JV, SHORTY_WHITE_LABEL))) {
                                                ?>
                                                <div class="form-group <?php echo $this->form_error_class('backup_url'); ?>">
                                                    <label class="control-label" for="label41"> Backup URL: </label>

                                                    <div class="controls has-feedback">
                                                        <input name="backup_url" type="text" class="form-control typeahead" id="backup_url" placeholder="http://"
                                                               value="<?php echo $this->set_value('backup_url', ''); ?>">
                                                        <span id="url_availability2" class="form-control-feedback" aria-hidden="true"></span>
                                                        <?php echo $this->form_error_message('backup_url'); ?>
                                                        <p class="help-block">Enter the backup URL to redirect traffic to if the primary URL is down.</p>
                                                    </div>
                                                </div>
                                                <div class="form-group" >
                                                    <label class="control-label" for="label46">Downtime limit: </label>

                                                    <div class="controls">
                                                        <div class="label label-info"><?php echo get_option(SH_PREFIX . 'downtime_alert_threshold'); ?>
                                                            minutes
                                                        </div>
                                                    </div>
                                                    <p class="help-block">You will get an alert when this link is down for more than 10 minutes. You can
                                                        <a href="?page=sh_settings_page">control options here</a>.</p>
                                                </div>
                                                <?php
                                            } else {
                                                ?>
                                                <div class="alert alert-block alert-info"><i class="fa fa-info-circle"></i> This feature is not available in <?php echo get_option(SH_PREFIX . 'license_scheme_name'); ?>. <a href="http://www.shortywp.com/?utm_source=wordpress&amp;utm_medium=referral&amp;utm_campaign=shorty_upgrade" target="_blank" class="alert-link">Upgrade now</a> to get additional features and dedicated support.</div>
                                                <?php
                                            }
                                            ?>
                                        </section>
                                    </fieldset>
                                    <fieldset>
                                        <!--not yet done-->
                                        <div class="control-group">
                                            <label class="control-label" for="auto_keyword_linking_enable">Auto Keyword Linking: </label>

                                            <div class="controls">
                                                <div class="switch btn-group btn-group-sm" data-toggle="buttons-radio" style="margin-bottom:10px;">
                                                    <button type="button" data-value="1"
                                                            class="btn btn-small <?php echo $this->set_value('auto_keyword_linking_enable') == 1 ? 'active' : ''; ?>"
                                                            data-target="#akl">On
                                                    </button>
                                                    <button type="button" data-value="0"
                                                            class="btn btn-small <?php echo $this->set_value('auto_keyword_linking_enable', 0) == 0 ? 'active' : ''; ?>"
                                                            data-target="#">Off
                                                    </button>
                                                </div>
                                                <p class="help-block">Turn on automatic keyword linking to get instant, targeted traffic from all
                                                    your blog posts and pages. You can <a href="?page=sh_settings_page">control options here</a>.
                                                </p>
                                            </div>
                                            <input id="auto_keyword_linking_enable" name="auto_keyword_linking_enable" class="switch-data"
                                                   type="hidden"/>
                                        </div>

                                        <div class="collapse out form-group <?php echo $this->form_error_class('meta_keyword'); ?>" id="akl">
                                            <label class="control-label" for="meta_keyword">Meta Keywords: </label>

                                            <div class="controls">
                                                <!--<div class="input-group">-->
                                                <input type="text" class="form-control" name="meta_keyword"
                                                       placeholder="keyword1, keyword 2, keyword 3"
                                                       value="<?php echo $this->set_value('meta_keyword', ''); ?>">
                                                <!--<div class="input-group-btn"><a href="#" class="btn btn-default">Get</a></div>-->
                                                <!--</div>-->
                                                <?php echo $this->form_error_message('meta_keyword'); ?>
                                                <p class="help-block">Type in keywords most relevant to the link. Separate each keyword with a
                                                    comma. If you do not enter any keywords, this tracking link will not be used in automatic
                                                    keyword conversions.</p>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <div class="control-group">
                                            <label class="control-label" for="geo_redirect_enable">Automatic Geo Redirect: </label>

                                            <div class="controls">
                                                <div class="switch btn-group btn-group-sm" data-toggle="buttons-radio"
                                                     style="margin-bottom:10px;">
                                                    <button type="button" data-value="1"
                                                            class="btn btn-small <?php echo $this->set_value('geo_redirect_enable') == 1 ? 'active' : ''; ?>"
                                                            data-target="#geo_redirect">On
                                                    </button>
                                                    <button type="button" data-value="0"
                                                            class="btn btn-small <?php echo $this->set_value('geo_redirect_enable') == 0 ? 'active' : ''; ?>"
                                                            data-target="#">Off
                                                    </button>
                                                </div>
                                                <p class="help-block">Turn on automatic geographical redirects to send users to a different
                                                    landing page based on country.</p>
                                            </div>
                                            <input id="geo_redirect_enable" name="geo_redirect_enable" class="switch-data" type="hidden"/>
                                        </div>
                                        <div class="collapse out form-group" id="geo_redirect">
                                            <?php
                                            if (in_array(get_option(SH_PREFIX . 'license_scheme_id'), array(SHORTY_PRO_LICENSE, SHORTY_AGENCY_LICENSE, SHORTY_JV, SHORTY_WHITE_LABEL))) {
                                                ?>
                                                <div class="form-group <?php echo $this->form_error_class('geo_redirect_option'); ?>">
                                                    <label class="control-label" for="geo_redirect_option">Redirect Option: </label>

                                                    <div class="controls">
                                                        <select id="geo_redirect_option" name="geo_redirect_option" class="form-control">
                                                            <option
                                                                value="ALL_EXCEPT" <?php echo selected($this->set_value('geo_redirect_option'), 'ALL_EXCEPT', true); ?>>
                                                                All Except Selected
                                                            </option>
                                                            <option
                                                                value="SELECTED" <?php echo selected($this->set_value('geo_redirect_option'), 'SELECTED', true); ?>>
                                                                Selected Countries Only
                                                            </option>
                                                        </select>
                                                        <?php echo $this->form_error_message('geo_redirect_option'); ?>
                                                        <p class="help-block">Choose to redirect only for selected countries, or excluded
                                                            countries.</p>
                                                    </div>
                                                </div>
                                                <div class="form-group <?php echo $this->form_error_class('geo_redirect_countries'); ?>">
                                                    <label class="control-label" for="geo_redirect_countries"><i
                                                            class="fa fa-exclamation-circle text-silver"></i> Choose Countries: </label>

                                                    <div class="controls">
                                                        <select name="geo_redirect_countries[]" multiple data-placeholder="Choose a Country..."
                                                                class="chosen-select form-control">
                                                            <option value=""></option>
                                                            <?php
                                                            $selected = $this->set_value('geo_redirect_countries', array());
                                                            foreach ($countries->dropdown() as $country_code => $country_name) {
                                                                ?>
                                                                <option value="<?php echo $country_code; ?>" <?php echo(in_array($country_code, $selected) ? 'selected="selected"' : ''); ?>><?php echo $country_name; ?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                        <?php echo $this->form_error_message('geo_redirect_countries'); ?>
                                                        <p class="help-block">Type the country name to select. You can choose multiple
                                                            countries.</p>
                                                    </div>
                                                </div>
                                                <div class="form-group <?php echo $this->form_error_class('geo_redirect_destination_url'); ?>">
                                                    <label class="control-label" for="geo_redirect_destination_url"><i
                                                            class="fa fa-exclamation-circle text-silver"></i> Destination URL: </label>

                                                    <div class="controls">
                                                        <input name="geo_redirect_destination_url" type="text" class="form-control"
                                                               value="<?php echo $this->set_value('geo_redirect_destination_url'); ?>">
                                                               <?php echo $this->form_error_message('geo_redirect_destination_url'); ?>
                                                        <p class="help-block">Your backup URL will be used if you do not enter anything here.</p>
                                                    </div>
                                                </div>
                                                <?php
                                            } else {
                                                ?>
                                                <div class="alert alert-block alert-info"><i class="fa fa-info-circle"></i> This feature is not available in  <?php echo get_option(SH_PREFIX . 'license_scheme_name'); ?>. <a href="http://www.shortywp.com/?utm_source=wordpress&amp;utm_medium=referral&amp;utm_campaign=shorty_upgrade" target="_blank" class="alert-link">Upgrade now</a> to get additional features and dedicated support.</div>
                                                <?php
                                            }
                                            ?>
                                        </div>

                                    </fieldset>
                                    <fieldset>
                                        <div class="control-group">
                                            <label class="control-label" for="click_limiter_enable">Click Limiter: </label>

                                            <div class="controls">
                                                <div class="switch btn-group btn-group-sm" data-toggle="buttons-radio" style="margin-bottom:10px;">
                                                    <button type="button" data-value="1"
                                                            class="btn btn-small <?php echo $this->set_value('click_limiter_enable') == 1 ? 'active' : ''; ?>"
                                                            data-target="#click_limiter">On
                                                    </button>
                                                    <button type="button" data-value="0"
                                                            class="btn btn-small <?php echo $this->set_value('click_limiter_enable') == 0 ? 'active' : ''; ?>"
                                                            data-target="#">Off
                                                    </button>
                                                </div>
                                                <p class="help-block">You can choose to send traffic to a different URL once you've reach a certain
                                                    click limit.</p>
                                            </div>
                                            <input id="geo_redirect_enable" name="click_limiter_enable" class="switch-data" type="hidden"/>
                                        </div>
                                        <fieldset class="collapse out " id="click_limiter">
                                            <div class="form-group <?php echo $this->form_error_class('click_limiter_max_clicks'); ?>">
                                                <label class="control-label" for="click_limiter_max_clicks"><i
                                                        class="fa fa-exclamation-circle text-silver"></i> Maximum clicks: </label>

                                                <div class="controls">
                                                    <input name="click_limiter_max_clicks" type="text" class="form-control"
                                                           value="<?php echo $this->set_value('click_limiter_max_clicks'); ?>" placeholder="">
                                                           <?php echo $this->form_error_message('click_limiter_max_clicks'); ?>
                                                    <p class="help-block">Enter the maximum clicks to allow for the primary URL. This link has
                                                        already received <code>0 lifetime clicks.</code></p>
                                                </div>
                                            </div>
                                            <div class="form-group <?php echo $this->form_error_class('click_limiter_url'); ?>">
                                                <label class="control-label" for="click_limiter_url">Click limit redirect URL: </label>

                                                <div class="controls has-feedback">
                                                    <input name="click_limiter_url" type="text" class="form-control"
                                                           value="<?php echo $this->set_value('click_limiter_url'); ?>" placeholder="http://">
                                                           <?php echo $this->form_error_message('click_limiter_url'); ?>
                                                    <p class="help-block">Your backup URL will be used if you do not enter anything here.</p>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </fieldset>
                                    <fieldset>
                                        <div class="control-group">
                                            <label class="control-label" for="link_expired_enable">Expiration Timer: </label>

                                            <div class="controls">
                                                <div class="switch btn-group btn-group-sm" data-toggle="buttons-radio">
                                                    <button type="button" data-value="1"
                                                            class="btn btn-small <?php echo $this->set_value('link_expired_enable') == 1 ? 'active' : ''; ?>"
                                                            data-target="#link-expired">On
                                                    </button>
                                                    <button type="button" data-value="0"
                                                            class="btn btn-small <?php echo $this->set_value('link_expired_enable', 0) == 0 ? 'active' : ''; ?>"
                                                            data-target="#">Off
                                                    </button>
                                                </div>
                                                <p class="help-block">You can choose to expire this link automatically based on a date and time, and
                                                    redirect clicks to another URL.</p>
                                            </div>
                                            <input type="hidden" id="link_expired_enable" name="link_expired_enable" class="switch-data"/>
                                        </div>
                                        <div class="collapse out form-group" id="link-expired">
                                            <div class="form-group <?php echo $this->form_error_class('link_expired_date'); ?>">
                                                <label class="control-label" for="link_expired_date">Choose Expiration Date: </label>

                                                <div class="controls">
                                                    <div class="input-group datetimepicker">
                                                        <input type="text" class="form-control datetimepicker" name="link_expired_date"
                                                               id="link_expired_date" value="<?php echo $this->set_value('link_expired_date'); ?>">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    </div>
                                                    <?php echo $this->form_error_message('link_expired_date'); ?>
                                                    <p class="help-block">Select the date and time your link should expire. Actual expiration is
                                                        based on the timezone settings for this WordPress blog.</p>
                                                </div>
                                            </div>
                                            <div class="form-group <?php echo $this->form_error_class('link_expired_url'); ?>">
                                                <label class="control-label" for="link_expired_url">Expired redirect URL: </label>

                                                <div class="controls">
                                                    <input name="link_expired_url" type="text" class="form-control" placeholder="http://"
                                                           value="<?php echo $this->set_value('link_expired_url'); ?>">
                                                           <?php echo $this->form_error_message('link_expired_url'); ?>
                                                    <p class="help-block">Your backup URL will be used if you do not enter anything here.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="form-actions">
                        <button name="btnAdd" type="submit" class="btn btn-primary btn-lg"><i class="fa fa-check-circle"></i> Create Link
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
