<div class="page-header">
    <h2><i class="fa fa-random"></i> Split Tests
        <?php
        if ($this->check_access(3)) {
            ?>
            <a href="?page=sh_split_tests&action=add" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Add New</a>
            <?php
        }
        ?>
    </h2>
    <p>Create a split test to rotate different affiliate offers or landing pages. </p>
</div>
<table id="v_split_tests" class="table table-hover" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th class="col-sm-1"><input type="checkbox" name="checkbox" class="cbAll"></th>
            <th>Rotator Name</th>
            <th>Rotator Link</th>
            <th class="col-sm-2">Actions</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="4">Loading</td>
        </tr>
    </tbody>
</table>

