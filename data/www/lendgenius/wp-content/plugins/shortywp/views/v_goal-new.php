<div class="container-fluid">
    <div class="row">
        <div class="ltbody">
            <div class="page-header">
                <h2><i class="fa fa-crosshairs"></i> Goal <small>|  New</small></h2>
                Generate a conversion code to track important goals like sales, leads and options. Items marked <i class="fa fa-exclamation-circle text-silver"></i> are required.
            </div>
            <div class="form">
                <form method="POST" name="frmNewGoal">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="panel-title">Goal Details</div>
                                </div>
                                <div class="panel-body">
                                    <fieldset>
                                        <div class="form-group <?php echo $this->form_error_class('goal_name'); ?>">
                                            <label class="control-label" for="goal_name"><i class="fa fa-exclamation-circle text-silver"></i> Goal Name: </label>

                                            <div class="controls">
                                                <input type="text" class="form-control" id="goal_name" name="goal_name" value="<?php echo $this->set_value('goal_name', ''); ?>">
                                                <?php echo $this->form_error_message('goal_name'); ?>
                                                <p class="help-block">Give your goal a name so you can recognize it easily in your reports and graphs.</p>
                                            </div>
                                        </div>
                                        <div class="form-group <?php echo $this->form_error_class('goal_type'); ?>">
                                            <label class="control-label" for="input2"><i class="fa fa-exclamation-circle text-silver"></i> Goal Type: </label>
                                            <div class="controls">
                                                <label for="goal_type"></label>
                                                <select name="goal_type" id="goal_type" class="form-control">
                                                    <option value="<?php echo GOAL_TYPE_LEAD; ?>" <?php echo selected($this->set_value('goal_type'), GOAL_TYPE_LEAD, true); ?>>Lead</option>
                                                    <option value="<?php echo GOAL_TYPE_SALE; ?>" <?php echo selected($this->set_value('goal_type'), GOAL_TYPE_SALE, true); ?>>Sale</option>
                                                </select>
                                                <?php echo $this->form_error_message('goal_type'); ?>
                                                <p class="help-block">A lead and sale is treated differently based on your conversion settings.</p>
                                            </div>
                                        </div>
                                        <div class="form-group <?php echo $this->form_error_class('goal_value'); ?>">
                                            <label class="control-label" for="goal_value"><i class="fa fa-exclamation-circle text-silver"></i> Goal Value: </label>
                                            <div class="controls">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="goal_value" name="goal_value" placeholder="Example: 9.90" value="<?php echo $this->set_value('goal_value', ''); ?>">
                                                    <span class="input-group-addon"><?php echo get_option(SH_PREFIX . 'settings_currency'); ?></span></div>
                                                <?php echo $this->form_error_message('goal_value'); ?>
                                                <p class="help-block">Enter the goal conversion value, numbers only. You can always change the value manually when inserting the tracking codes.</p>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="panel-title">Conversion Tracking</div>
                                </div>
                                <div class="panel-body">
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="control-label" for="input3">Where does the conversion happen?</label>
                                            <div class="controls">
                                                <select name="conversion_option" id="conversion_option" class="form-control">
                                                    <option value="on-site" <?php echo selected($this->set_value('conversion_option'), 'on-site', true); ?>>A post or page on this site</option>
                                                    <option value="off-site" <?php echo selected($this->set_value('conversion_option'), 'off-site', true); ?>>External website URL</option>
                                                </select>
                                                <p class="help-block">Do you want to track conversions on your pages and post, or an external URL?</p>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset id="on-site" class="collapse out">
                                        <legend>On-Site Conversion</legend>
                                        <div class="form-group <?php echo $this->form_error_class('goal_tracking_url'); ?>">
                                            <label class="control-label" for="goal_tracking_url">Choose the post or page: </label>
                                            <div class="controls">
                                                <input name="goal_tracking_url" type="text" class="form-control input-lg typeahead" id="goal_tracking_url" placeholder="Click to select page.." value="<?php echo $this->set_value('goal_tracking_url', ''); ?>">
                                                <input id="page_id" name="page_id" type="hidden" value="<?php echo $this->set_value('page_id', ''); ?>" />
                                                <?php echo $this->form_error_message('goal_tracking_url'); ?>
                                                <p class="help-block">Visits to this page on your WordPress blog will automatically trigger this conversion. </p>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset id="off-site" class="collapse out">
                                        <legend>Off-Site Conversion</legend>
                                        <div class="form-group">
                                            <label class="control-label" for="reference_id">Reference  ID: </label>
                                            <div class="controls">
                                                <input type="text" class="form-control" id="reference_id" name="reference_id" placeholder="Example: 0123456">
                                                <p class="help-block">Enter a unique ID so we can tell when to recognize a goal conversion, and when to ignore it. If you have no unique ID to pass, leave this blank. <a href="#" rel="tooltip" data-original-title="This is normally your order ID, customer email address or transaction ID. You can automatically replace this unique id in your shopping cart process."><i class="fa fa-question-circle text-silver"></i></a></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label" for="use_ssl_tracking">Use SSL Tracking: </label>
                                            <div class="controls">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="use_ssl_tracking" id="use_ssl_tracking" value="1">
                                                        Enable SSL (secure link) </label>
                                                </div>
                                                <p class="help-block">Enable SSL if a secure link is required. Please note that you must have SSL installed on this domain in order for it to work. <a href="http://gigurl.com/ssl" target="_blank">Get your SSL certs here.</a></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label" for="label">Tracking code: </label>
                                            <div class="controls">
                                                <textarea name="textfield" class="form-control" id="textfield">
&lt;img src=&quot;http://<?php echo $this->current_domain(FALSE, TRUE, TRUE); ?>srty/pixel/?gn=&amp;gt=&amp;gv=&amp;rid=&quot; alt=&quot;_&quot; width=&quot;1&quot; height=&quot;1&quot; border=&quot;0&quot; /&gt;'; ?>
                                                </textarea>
                                                <p class="help-block">To get the unique reference ID of a conversion and to dynamically replace the goal variables, you have to manually insert and configure this pixel code in your page.</p>
                                                <div class="table-responsive">
                                                    <table class="table table-condensed">
                                                        <tr>
                                                            <td><code>gn</code></td>
                                                            <td>The goal name, alphabets and numbers only.</td>
                                                        </tr>
                                                        <tr>
                                                            <td><code>gt</code></td>
                                                            <td>Specify either SALE or LEAD only.</td>
                                                        </tr>
                                                        <tr>
                                                            <td><code>gv</code></td>
                                                            <td>The value of the goal, two decimal points without comma or currency symbols.</td>
                                                        </tr>
                                                        <tr>
                                                            <td><code>rid</code></td>
                                                            <td>Reference Id. The unique conversion tracking value, usually generated by your shopping cart or lead generation system.</td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions"><button name="btnAdd" type="submit" class="btn btn-primary btn-lg"><i class="fa fa-check-circle"></i> Create Goal</button></div>
                </form>
            </div>
        </div>
    </div>
</div>

