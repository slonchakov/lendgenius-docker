<?php

if (!defined('ABSPATH'))
    die('You are not allowed to call this page directly.');

/**
 * SH_PREFIX will be use as table prefix. You can current value if you want to 
 * use custom prefix for your table
 */
define('SH_PREFIX', 'srty_');

/**
 * This is your plugin name. You can rename it to use different name 
 * than Audience Press
 */
define('SH_MENU_DISPLAY', 'Shorty');

/**
 * This is for white label. put your license key here
 * eg:
 *      define('SH_LICENSE','SHTY-123457TNL6JHH');
 */
define('SH_LICENSE', '');

/**
 * always point this to freegeoip url
 */
define('SH_FREEGEOIP_URL', 'https://freegeoip.net/json/');

