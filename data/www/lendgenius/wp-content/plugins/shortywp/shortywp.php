<?php

/*
  Plugin Name: ShortyWP
  Plugin URI: http://www.audiencepress.co/?utm_source=wordpress_plugin&utm_medium=referral&utm_campaign=plugin_meta
  Description: The best affiliate link cloaking and click tracking plugin ever made for WordPress. Works with all major affiliate networks and PPC ad platforms.
  Author: Shorty
  Author URI: http://www.shortywp.com?utm_source=shorty&utm_medium=referral&utm_term=plugin_meta&utm_campaign=wordpress_plugin
  Version: 2.0.12
  Text Domain: shortywp
  License: GPL version 2 or later - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

global $sh_db_version;
$sh_db_version = '1.4.1';

register_activation_hook(__FILE__, 'srty_install');
register_deactivation_hook(__FILE__, 'srty_deactivation');

require_once('sh-config.php');
require_once('srty_lang.php');
require_once('autoload.php');
require_once('srty_loader.php');
