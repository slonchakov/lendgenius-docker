<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 25.08.17
 * @var $headline
 */
$have_headline = true;
?>
<!--start CTA-2 section-->
<div class="cta-section cta-section_small cta-2 lightgray-bg">
    <div class="cta-logo-icon">
        <i class="icomoon icomoon-idea-dollar ico-gradient"></i>
    </div>

    <div class="desk light-font text-center">
        <?= $headline; ?>
    </div>
    <a href="<?= $link; ?>" class="btn btn-success"><?= $button; ?></a>
</div>
<!--End CTA-2 section-->
