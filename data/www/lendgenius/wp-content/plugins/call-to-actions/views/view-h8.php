<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 25.08.17
 * @var $headline
 */
$have_headline = true;
?>
<!-- Start CTA horizontal-8 -->
<div class="cta-section cta-section_section lightgray-bg">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="txt-logo delimiter">
                    <span>Lend</span>Genius
                </div>
                <div class="desk">
                    <?= $headline; ?>
                </div>
                <div class="text-center">
                    <a href="<?= $link; ?>" class="btn btn-success"><?= $button; ?></a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end CTA horizontal-8 -->