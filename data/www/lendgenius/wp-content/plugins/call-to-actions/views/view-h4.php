<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 25.08.17
 *
 */
$have_headline = true;
?>
<!-- Start CTA horizontal-4-->
<div class="cta-section cta-section_section brand-gradient">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="txt-logo delimiter">
                    <span>Lend</span>Genius
                </div>
                <div class="desk">
                    <?= $headline; ?>
                </div>
                <div class="text-center">
                    <a href="<?= $link; ?>" class="btn btn-default-success"><?= $button; ?></a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end CTA horizontal-4 -->
