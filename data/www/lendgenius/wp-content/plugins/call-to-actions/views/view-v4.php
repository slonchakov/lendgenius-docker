<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 25.08.17
 * @var $headline
 * @var $subtitle
 */
$have_headline = true;
$have_subtitle = true;
?>
<!--start CTA-4 section-->
<div class="cta-section cta-section_small cta-4 lightgray-bg">
    <div class="cta-body">
        <div class="bold h4 text-center"><?= $headline; ?></div>
        <div class="desk text-center">
            <?= $subtitle; ?>
        </div>
    </div>
    <div class="cta-footer">
        <a href="<?= $link; ?>" class="btn btn-success"><?= $button; ?></a>
    </div>
</div>
<!--End CTA-4 section-->
