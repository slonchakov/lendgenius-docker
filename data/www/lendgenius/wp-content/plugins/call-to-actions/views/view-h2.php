<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 25.08.17
 *
 */

$have_headline = true;

?>
<!-- Start CTA horizontal-2-->
<div class="cta-section cta-section_section brand-gradient">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="cta-logo-icon">
                    <i class="icomoon icomoon-idea-dollar"></i>
                </div>
                <div class="txt-logo">
                    <span>Lend</span>Genius
                </div>
                <div class="desk medium">
                    <?= $headline; ?>
                </div>
                <div class="text-center">
                    <a href="<?= $link; ?>" class="btn btn-default-success"><?= $button; ?></a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end CTA horizontal-2 -->
