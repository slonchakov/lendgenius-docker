<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 25.08.17
 * @var $headline
 */
$have_headline = true;
?>
<!--start CTA-5 section-->
<div class="cta-section cta-section_small cta-5">
    <div class="cta-logo-icon">
        <i class="icomoon icomoon-idea-dollar"></i>
    </div>
    <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/logo-cta.png" class="img-responsive center-block">
    <div class="desk medium-plus text-center">
        <?= $headline; ?>
    </div>
    <a href="<?= $link; ?>" class="btn btn-default-success"><?= $button; ?></a>
</div>
<!--End CTA-5 section-->
