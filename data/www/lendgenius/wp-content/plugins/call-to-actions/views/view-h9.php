<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 04.09.17
 * @var $headline
 */
$have_headline = true;
?>
<div class="cta-business-holder lightgray-bg flex flex-align-center">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h4 class="h4 medium">
                    <?= $headline; ?>
                </h4>
                <a href="<?= $link; ?>" class="btn btn-warning-custom btn-x2"><?= $button; ?></a>
            </div>
        </div>
    </div>
</div>