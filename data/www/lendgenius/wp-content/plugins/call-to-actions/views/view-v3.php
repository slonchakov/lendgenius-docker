<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 25.08.17
 * @var $headline
 */
$have_headline = true;
?>
<!--start CTA-3 section-->
<div class="cta-section cta-section_small cta-3 lightgray-bg">
    <div class="desk light-font text-center">
        <?= $headline; ?>
    </div>
    <a href="<?= $link; ?>" class="btn btn-success"><?= $button; ?></a>
</div>
<!--End CTA-3 section-->
