<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 25.08.17
 * @var $headline
 */

$have_headline = true;
?>
<!--start CTA-1 section-->
<div class="cta-section cta-section_small cta-1">
    <div class="cta-logo-icon">
        <i class="icomoon icomoon-idea-dollar"></i>
    </div>
    <div class="desk medium-plus text-center">
        <?= $headline; ?>
    </div>
    <a href="<?= $link; ?>" class="btn btn-default-success"><?= $button; ?></a>
</div>
<!--End CTA-1 section-->
