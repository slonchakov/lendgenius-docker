<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 25.08.17
 *
 */

/*
Plugin Name: Call To Actions
Description: Call to action sections
Version:     1.0.0
Author:      sergey.s
Email: sergey.s@lendgenius.com
*/


defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

if ('/wp-admin/post.php' === $PHP_SELF) return;
require_once 'classes/CTA.php';

function cpa_plugin_settings_link($links)
{
    $settings_link = '<a href="options-general.php?page=call-to-actions/classes/CTA.php">Settings</a>';
    array_unshift($links, $settings_link);
    return $links;
}

$plugin = plugin_basename(__FILE__);
add_filter('plugin_action_links_'.$plugin, 'cpa_plugin_settings_link');

$cpa = new CTA\CTA();


