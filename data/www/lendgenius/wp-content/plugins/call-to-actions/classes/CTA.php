<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 25.08.17
 *
 */

namespace CTA;


class CTA
{
    public $filenames;
    public $dir;

    const HEADLINE_DEF = "Apply to the Industry's Best Lenders with LendGenius Today!";
    const LINK_DEF = '/apply-now/a';
    const BUTTON_DEF = 'See Loan Options';

    public function __construct()
    {

        add_action('admin_menu', function() {

            add_submenu_page( 'tools.php','CTA Sections', 'CTA Sections', 'manage_options', __FILE__, array( $this, 'add_menu_page' ) );

            add_action( 'admin_init', array($this,'set_plugin_settings') );
        } );

        if (is_admin()) {
            $this->dir = WP_CONTENT_DIR . '/plugins/call-to-actions/views';
            $this->getViews();
        }
        add_shortcode( 'CTA', array($this,'get_cpa_section') );
    }

    public function set_plugin_settings()
    {
        register_setting( 'CTA-settings-group', 'cta_sections_settings' );
    }

    public function add_menu_page()
    {
        $filenames = $this->filenames;
        $dir = $this->dir;
        include(dirname(__FILE__).'/../templates/plugin_form.php');
    }

    public function get_cpa_section($atts)
    {
        $cta_id       = strtolower($atts[0]);
        $cta_settings = get_option('cta_sections_settings');

        $fields = array('headline','subtitle','link','button');

        try {

            foreach ($fields as $field) {
                switch(isset($atts[$field]) <=> isset($cta_settings[$cta_id][$field])) {
                    case 1:
                        $$field = $atts[$field];
                        break;
                    case 0:
                        if(isset($atts[$field])) {
                            $$field = $atts[$field];
                        }
                        break;
                    case -1:
                        $$field = $cta_settings[$cta_id][$field];
                        break;
                }
            }

        } catch (\Exception $e) {
            echo "PHP version lower than 7: php version is ".PHP_VERSION_ID;
        }


        if (is_file(dirname(__FILE__).'/../views/view-'. $cta_id .'.php')) {
            include(dirname(__FILE__).'/../views/view-'. $cta_id .'.php');
        } else {
            if (is_user_logged_in()) {
                echo "[ Incorrect CTA ]: ". $atts[0];
            }

        }


    }

    private function getViews()
    {

        if (is_dir($this->dir)) {
            if ($dh = opendir($this->dir)) {
                while (($file = readdir($dh)) !== false) {
                    if ( $file == '.' || $file == '..') continue;

                    preg_match('/view-(.*).php/',$file,$matches);
                    $shortcode_name = strtoupper($matches[1]);

                    $this->filenames[$shortcode_name] = $file;

                }
                closedir($dh);
            }
        }
    }
}