<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 25.08.17
 * @var $filenames
 * @var $dir
 * @var $subtitle
 */

$cta_settings = get_option('cta_sections_settings');
echo  apply_filters('add_custom_styles','');
?>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/assets/styles/icomoon.min.css?ver=<?php echo get_option( 'sts_script_version' ); ?>">
<link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,700&amp;subset=latin-ext" rel="stylesheet">
<style>
    .container {
        width: 100%;
    }

</style>
<div class="wrap">

<h1>CTA Sections</h1>

    <div class="updated">
        <div class="example disable"><p>Example: [CTA H1 headline="Hуadline Example" link="/apply-now/a" button="See Loan Options"]</p></div>
    </div>

<form method="post" action="options.php">
    <?php settings_fields( 'CTA-settings-group' ); ?>
    <?php do_settings_sections( 'CTA-settings-group' ); ?>
            <?php

            if (count($filenames) > 0) {

                sort($filenames);

                foreach ($filenames as $file) {

                    $headline = CTA\CTA::HEADLINE_DEF;
                    $link     = CTA\CTA::LINK_DEF;
                    $button   = CTA\CTA::BUTTON_DEF;

                    preg_match('/view-(.*).php/',$file,$matches);
                    $shortcode_name = $matches[1];

                    if ($cta_settings) {

                        if ($cta_settings[$shortcode_name]) {

                            if (isset($cta_settings[$shortcode_name]['headline'])) {
                                $headline = $cta_settings[$shortcode_name]['headline'];
                            }
                            if (isset($cta_settings[$shortcode_name]['subtitle'])) {
                                $subtitle = $cta_settings[$shortcode_name]['subtitle'];
                            }
                            if (isset($cta_settings[$shortcode_name]['link'])) {
                                $link = $cta_settings[$shortcode_name]['link'];
                            }
                            if (isset($cta_settings[$shortcode_name]['button'])) {
                                $button = $cta_settings[$shortcode_name]['button'];
                            }
                        }
                    }

                    ?>

                    <div class='row'>
                        <div class='col-md-3'>
                        <?php include_once( $dir ."/". $file); ?>
                        </div>

                        <div class="col-md-9">
                            <?php if ($have_headline) {
                                ?>
                                <div class="form-group">
                                    <label for="">Headline</label>
                                    <input type="text" class="form-control" value="<?= $headline; ?>" name="cta_sections_settings[<?= $shortcode_name; ?>][headline]">
                                </div>
                            <?php } ?>

                            <?php if ($have_subtitle) {
                                ?>
                                <div class="form-group">
                                    <label for="">Sub title</label>
                                    <input type="text" class="form-control" value="<?= $subtitle; ?>" name="cta_sections_settings[<?= $shortcode_name; ?>][subtitle]">
                                </div>
                            <?php } ?>

                            <div class="form-group">
                                <label for="">Link</label>
                                <input type="text" class="form-control" value="<?= $link; ?>" name="cta_sections_settings[<?= $shortcode_name; ?>][link]">
                            </div>

                            <div class="form-group">
                                <label for="">Button</label>
                                <input type="text" class="form-control" value="<?= $button; ?>" name="cta_sections_settings[<?= $shortcode_name; ?>][button]">
                            </div>
                            <hr>
                            <label for="">Short code:</label> <input type="text" onClick="this.select()" disabled class="unstyled cta-code" value="<?= sprintf("[CTA %s]",strtoupper($shortcode_name)); ?>">
                        </div>
                    </div>
                    <hr>
                <?php
                    unset($have_headline,$have_subtitle);
                }

            }
        ?>

    <?php submit_button('Save Settings','primary','submit',false, array('id'=>'submit_options')); ?>
    </form>
</div>