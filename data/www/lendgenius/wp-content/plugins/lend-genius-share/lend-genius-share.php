<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 06.07.17
 * Time: 15:25
 */

/*
Plugin Name: LendGenius Share
Description: Share buttons for posts
Version:     1.1.0
Author:      sergey.s
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
if ('/wp-admin/post.php' === $PHP_SELF) return;
function lg_autoloader($class) {
    if(!strpos($class,'share')) return;
    include 'classes/' . $class . '.php';
}

spl_autoload_register('lg_autoloader');

require 'classes/LendGeniusShare.php';

register_activation_hook( __FILE__, array('WP_Plugin_LendGeniusShare', 'activate') );

// cleanup
register_deactivation_hook(__FILE__, array('WP_Plugin_LendGeniusShare', 'deactivate') );

function lgs_plugin_settings_link($links)
{
    $settings_link = '<a href="options-general.php?page=lend-genius-share/classes/LendGeniusShare.php">Settings</a>';
    array_unshift($links, $settings_link);
    return $links;
}

$plugin = plugin_basename(__FILE__);
add_filter('plugin_action_links_'.$plugin, 'lgs_plugin_settings_link');

$lgsshare = new WP_Plugin_LendGeniusShare();