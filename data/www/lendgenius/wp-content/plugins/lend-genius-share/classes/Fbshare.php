<?php

/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 18.07.17
 * Time: 11:38
 */
class Fbshare extends Counter
{

    public function getCount($current_url)
    {

        $url = 'http://graph.facebook.com/?fields=og_object{likes.summary(true).limit(0)},share&id='. $current_url; // shares and likes 
//        $url = 'https://graph.facebook.com/v2.2/?id='. $current_url; just shares
        $s   = curl_init($url);

        curl_setopt($s,CURLOPT_RETURNTRANSFER,true);

        $json     = curl_exec($s);
        $response = json_decode($json);
        $likes    = 0;
        $shares   = 0;

        if (isset($response->og_object->likes->summary->total_count)) {
            $likes = $response->og_object->likes->summary->total_count;
        }

        if(isset($response->share->share_count)) {
            $shares = $response->share->share_count;
        }

        return $likes + $shares;
    }

}