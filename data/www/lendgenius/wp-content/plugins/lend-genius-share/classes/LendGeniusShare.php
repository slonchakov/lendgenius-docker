<?php

/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 06.07.17
 * Time: 15:32
 */
class WP_Plugin_LendGeniusShare
{

    public function __construct()
    {

        add_action('admin_menu', function() {

            add_menu_page( 'LGSShare', 'LGSShare', 'manage_options', __FILE__, array( $this, 'add_menu_page' ), '',140 );

            add_action( 'admin_init', array($this,'LGSShare_plugin_settings') );
        } );


        add_action( 'wp_ajax_update_share_counter', array($this,'update_share_counter') );
        add_action( 'wp_ajax_nopriv_update_share_counter', array($this,'update_share_counter') );



        add_shortcode( 'lgsshare_buttons', array($this,'show_buttons') );
    }

    public function LGSShare_plugin_settings()
    {
        register_setting( 'LGSShare-settings-group', 'lgsshare_buttons_counter' );
        register_setting( 'LGSShare-settings-group', 'lgsshare_buttons_active' );
    }

    public function show_buttons()
    {

        wp_enqueue_style('lgsshare',plugins_url( '/../assets/lgs-style.css?v=1', __FILE__ ));

        if (!is_admin()) {
            wp_enqueue_script( 'lend-genius-share', '/wp-content/plugins/lend-genius-share/assets/script.min.js', array( 'jquery' ), 1, true );
        }
        
        $current_url = get_the_permalink();

        $active_buttons  = get_option('lgsshare_buttons_active');

        $buttons = '';
        $content = '';

        if (empty($active_buttons) || !is_array($active_buttons) ) {
            return $content;
        }

        include 'Counter.php';
        include 'CounterResult.php';

        foreach($active_buttons as $button => $value) {

            $currentCounter = CounterResult::getCounterResult($button,$current_url);

            $buttons .= str_replace(
                array('{$current_url}',
                    '{$counter}'
                ),
                array(
                    $current_url,
                    $currentCounter
                ),
                file_get_contents(__DIR__.'/../templates/'. $button .'.html'));
        }

        $layout   = file_get_contents(__DIR__.'/../templates/layout.html');
        $content .= str_replace('{$buttons}',$buttons,$layout);

        return $content;
    }

    public static function activate()
    {
    // Must be
    }
    public static function deactivate()
    {
    // Must be
        delete_option('lgsshare_buttons_counter');
        delete_option('lgsshare_buttons_active');
    }

    public function add_menu_page()
    {
        include(__DIR__.'/../templates/plugin_form.php');
    }

    public function update_share_counter()
    {
        $all_counters = get_option('lgsshare_buttons_counter');

        if (!is_array($all_counters)) {
            $all_counters = array();
        }

        $share_counter = $_POST['button_id'];
        $page          = $_POST['page'];
        $count         = (int) $_POST['count'];


        if(!isset($all_counters[$share_counter])) {

            $all_counters[$share_counter] = array();
        }

        if(!isset($all_counters[$share_counter][$page])) {
            $all_counters[$share_counter][$page] = array(
                'fake' => 0,
                'real' => 0
            );
        }

        $all_counters[$share_counter][$page]['fake'] = $count;
        $all_counters[$share_counter][$page]['real']++;

        if (update_option('lgsshare_buttons_counter', $all_counters)) {

            echo "updated";
        }

        die();
    }

}