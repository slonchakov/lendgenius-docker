<?php

/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 18.07.17
 * Time: 12:01
 */
class CounterResult
{

    static public function getCounterResult($button,$current_url)
    {
        $shareClassName = ucfirst(str_replace( "_","",$button ));
        
        $counter = new $shareClassName();
        
        if ($count = $counter->getCount($current_url)) {
            return $count;
        }

        return $counter->getCountFromOption($button,$current_url);
    }

}