<?php

/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 18.07.17
 * Time: 11:55
 */
class Linkedinshare extends Counter
{

    public function getCount($current_url)
    {

        $url = 'https://www.linkedin.com/countserv/count/share?format=json&url='. $current_url;
        $s = curl_init($url);

        curl_setopt($s,CURLOPT_RETURNTRANSFER,true);

        $json = curl_exec($s);
        $response = json_decode($json);

        return $response->count;
    }

}