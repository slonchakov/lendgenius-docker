<?php

/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 18.07.17
 * Time: 11:35
 */
abstract class Counter
{

    /**
     * @param string $share_item - name of option for which counter's value needed
     * @param string $current_url - url of current page
     * @return int*
     * Looking for counter value in db if it is not exists return random digit from 4 to 20
     */
    public function getCountFromOption($share_item,$current_url)
    {
        $counter_buttons = get_option('lgsshare_buttons_counter');

        if (empty($counter_buttons) || !is_array($counter_buttons) ) {
            return rand(4,20);
        }

        if(isset($counter_buttons[$share_item][$current_url]['real'])) {
            return $counter_buttons[$share_item][$current_url]['real'];
        }
        
        return rand(4,20);
    }

    /**
     * @param $current_url
     * @return mixed
     * Must have method. Realisation for getting counter value from social network
     * or from db with method $this->getCountFromOption
     */
    abstract function getCount($current_url);
}