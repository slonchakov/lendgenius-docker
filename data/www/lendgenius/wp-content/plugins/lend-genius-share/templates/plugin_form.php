<?php
/**
 * Created by PhpStorm.
 * User: sergey.s
 * Date: 07.07.17
 * Time: 15:26
 */

$share_buttons_active  = get_option('lgsshare_buttons_active');
$share_buttons_counter = get_option('lgsshare_buttons_counter');

/*
 *  Example option fb_share => class Fbshare extends Counter in file Fbshare.php for autoloading
 *  template for button must be named as option. Example: templates/fb_share.html
 */
$shares = array(
    [
        'id' => 'fb_share',
        'name' => 'Facebook'
    ],
    [
        'id' => 'twitter_share',
        'name' => 'Twitter'
    ],
    [
        'id' => 'linkedin_share',
        'name' => 'LinkedIn'
    ],
    [
        'id' => 'gplus_share',
        'name' => 'Google +'
    ],
);
?>

<div class="wrap">

    <h1>LendGenius Share Buttons</h1>

    <form method="post" action="options.php">
        <?php settings_fields( 'LGSShare-settings-group' ); ?>
        <?php do_settings_sections( 'LGSShare-settings-group' ); ?>
        <table class="form-table">

            <?php foreach ($shares as $share) {
                ?>
                <tr valign="top">
                    <th scope="row"><?php echo $share['name']; ?></th>
                    <td>
                        <input type="checkbox"
                               name="lgsshare_buttons_active[<?php echo $share['id']; ?>]"
                               value="1" <?php if ($share_buttons_active[$share['id']]) { echo 'checked="checked"'; }?> />
                    </td>
                </tr>

                <?php
            }?>
        </table>

        <?php submit_button(); ?>

    </form>

    <div class="panel">
        <br><br>
        <h3>Statistics</h3>
        <hr>
        <div>
            <?php foreach ($shares as $share) {
                if (isset($share_buttons_counter[$share['id']])) {
                    arsort($share_buttons_counter[$share['id']]);

                    ?>

                    <div id="<?php echo $share['id']; ?>" class="tab_content">
                        <h4><?php echo $share['name']; ?> <span class="dashicons dashicons-arrow-down-alt2 pull-right"></span></h4>
                        <div class="content">

                            <table>
                               <thead>
                                   <tr>
                                       <th>URL</th>
                                       <th>Shares</th>
                                   </tr>
                               </thead>

                                <tbody>
                                <?php

                                foreach ($share_buttons_counter[$share['id']] as $url => $count) {

                                    $path = parse_url($url);
                                ?>
                                    <tr>
                                        <td><a href='<?= $url; ?>' target='_blank'><?= $path['path']; ?></a></td>
                                        <td><?= $count['real']; ?></td>
                                    </tr>
                                <?php

                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                <?php
                }
            }?>
        </div>
    </div>
</div>

<script>
    jQuery('.tab_content h4').on('click',function(){
        var $this = jQuery(this),
            childSpan = $this.find('span');

        $this.next().toggle();

        if (childSpan.hasClass('dashicons-arrow-up-alt2')) {
            childSpan.removeClass('dashicons-arrow-up-alt2').addClass('dashicons-arrow-down-alt2')
        } else {
            childSpan.removeClass('dashicons-arrow-down-alt2').addClass('dashicons-arrow-up-alt2')
        }
    });
</script>
