/**
 * Created by sergey.s on 06.07.17.
 */

jQuery(document).ready(function(){

    function checkClick(item)
    {
        var count = parseInt(item.children('span').text()) + 1;
        var data = {
            page: location.href,
            button_id: item.attr('id'),
            count: count
        };

        jQuery.post('/wp-admin/admin-ajax.php?action=update_share_counter', data, function(response) {

            if ('updated' === response) {

                item.find('span').text(count);
            }
        });
    }

    function setCenterPosition(parentSize,targetSize){

        return ((parentSize - targetSize)/2 > 0) ? (parentSize - targetSize)/2 : 0;
    }

    jQuery('.share-list-wrapper a').on('click',function(e){

        var $this = jQuery(this);

        checkClick($this);

        if(jQuery(window).width() < 1024 || $this.attr('target') !== '_blank') return;
        e.preventDefault();

        var href = $this.attr('href'),
            windowWidth = jQuery(window).width(),
            windowHeight = jQuery(window).height(),
            newWindowWidth = 500,
            newWindowHeight = 500;

        window.open(href,"_blank", "scrollbars=yes,resizable=yes,width="+newWindowWidth+",height="+newWindowHeight+",left="+ setCenterPosition(windowWidth,newWindowWidth) +",top="+ setCenterPosition(windowHeight,newWindowHeight));
    });
});