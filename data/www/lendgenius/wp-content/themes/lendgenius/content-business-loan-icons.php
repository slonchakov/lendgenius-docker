<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 17.10.16
 * Time: 13:46
 */
$loan_icon = get_post_meta( $loan->ID, 'loan_icon', true );
?>

<div class="finance_product">
    <a href="<?php echo get_permalink($loan->ID); ?>">
        <div class="image">
            <i class="<?php echo $loan_icon; ?>"></i>
        </div>
        <span class="h5"><?php echo $loan->post_title; ?></span>
    </a>
</div>
