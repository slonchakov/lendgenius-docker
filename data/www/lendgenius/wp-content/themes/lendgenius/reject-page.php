<?php
/*
    Template Name: Reject-page
*/
get_header();
?>
<div class="body-rejection">
    <div class="container-rejection">
        <div class="header_only-logo">
            <a href="/">
                <img src="\wp-content\themes\lendgenius\assets\images\logo.svg">
            </a>
        </div>
    </div>
    <div class="container-rejection">
        <div class="white-bg panel-reject">
            <h1 class="panel-reject_title">
                Darn! We can’t get you a business loan
            </h1>       
            <img src="\wp-content\themes\lendgenius\assets\images\emoji-negative.svg" alt="Face Negative" class="img-responsive center-block emoji-face">
            <p>
                Based on what you told us, Aleks, we can’t provide you with a business loan when your business doesn’t have a business credit checking account. Check out this article on <a href="#">how to get a business checking account.</a>
            </p>
            <div class="hr-success"></div>
            <p>
                Why not try to get a
            </p>
            <a href="#" class="btn btn-success btn-x2">Business Credit Card</a>
            <span>
                If you feel like there is an error here, feel free to call a LendGenius Loan Specialist at
            </span>

            <a class="panel-reject_tel" href="tel:1-800-348-0997">1 (800) 348-0997</a>
        </div>
        <div class="list-posts-wrapper">
            <div class="blog-item-sub">
                <div class="blog-item-sub-img">
                    <a href="#">
                    <img width="370" height="207" src="https://lendgeniuslandingstorage.azureedge.net/wp-uploads/2017/08/Business-Loan-Funding-for-Restaurants-LendGenius2-370x207.jpg?v=3" class=" wp-post-image" alt="Restaurant Business Loans - In The Biz">
                    </a>
                </div>
                <h4 class="blog-item-sub-title matchheight">
                    <a href="#">Small Business Grants – A Guide to Free Business Funding</a>
                </h4>
                <div class="blog-item-sub-author">
                    <span><a href="#">Jackie Lam</a></span> <i class="fa fa-circle" aria-hidden="true"></i> <span>May 15, 2017</span>
                </div>
            </div>
            <div class="blog-item-sub">
                <div class="blog-item-sub-img">
                    <a href="#">
                        <img width="370" height="207" src="https://lendgeniuslandingstorage.azureedge.net/wp-uploads/2017/08/Best-Lenders-for-Business-Loans-Online-370x207.jpg?v=3" class=" wp-post-image" alt="Best Lenders for Business Loans Online">
                    </a>
                </div>
                <h4 class="blog-item-sub-title matchheight">
                    <a href="#">The Best Lenders in 2017 for Small Business Loans Online</a>
                </h4>
                <div class="blog-item-sub-author">
                    <span><a href="#">Jackie Lam</a></span> <i class="fa fa-circle" aria-hidden="true"></i> <span>May 15, 2017</span>
                </div>
            </div>
        </div>

    </div>

<?php
get_footer();