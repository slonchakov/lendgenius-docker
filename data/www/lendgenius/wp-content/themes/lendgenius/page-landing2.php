<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 4/19/17
 * Time: 4:37 PM
 */
/*
Template Name: Landing2
*/
$template_directory_uri = get_template_directory_uri();

$post_id             = get_the_ID();
$rates_starting      = get_post_meta( $post_id, 'rates', true );
$loans_from          = get_post_meta( $post_id, 'loans_from', true );
$loans_from_interval = get_post_meta( $post_id, 'loans_from_interval', true );
$funding             = get_post_meta( $post_id, 'funding', true );
$heading_title       = get_post_meta( $post_id, 'heading_title', true );
$sub_title           = get_post_meta( $post_id, 'sub_title', true );

$input_orange_text  = get_post_meta( $post_id, 'orange_button_text', true );
$input_orange_url   = get_post_meta( $post_id, 'orange_button_url', true );
$orange_button_text = !empty($input_orange_text) ?  $input_orange_text : 'Get Started';
//$orange_button_url  = !empty($input_orange_url) ? $input_orange_url : '/loan-request';
$orange_button_url  = '/loan-request/?EmbedCampID='. get_option('sts_company_id');

get_header();
get_template_part( 'views/headers/header-top-landing', get_post_format() );
?>
<!-- <section class="slogan-section vertical-view-form">
    <div class="another-mobile-view">
        <div class="container">
            <div class="row">
                 <div class="col-xs-12">
                     <div class="content-txt">
                         <h1><?= $heading_title; ?></h1>
                         <h2><?= $sub_title; ?></h2>
                     </div>   
                </div> 
            </div>
        </div>
    </div>
    <div class="container">
         <div class="row">
            <div class="col-xs-12">
                <?= do_shortcode("[lead form='2']"); ?>
            </div>
        </div>
    </div>
</section> -->

<section class="slogan-section">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="content-txt">
                    <h1><?= $heading_title; ?></h1>
                    <h2><?= $sub_title; ?></h2>
                </div>
                <div class="text-center margin-top-40">
                    <a class="btn btn-x2 btn-warning-custom" href="<?= $orange_button_url; ?>"><?= $orange_button_text; ?></a>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="sky-bg section-padding">
    <div class="container">
        <div class="reasons-apply">
            <div class="row">
                <div class="col-sm-4 reasons-item">
                    <div class="reasons-item-icon">
                        <i class="icomoon-dollar-circle" aria-hidden="true"></i>
                    </div>
                    <div class="reasons-title">
                        <?php echo $rates_starting; ?>
                    </div>
                </div>
                <div class="col-sm-4 reasons-item">
                    <div class="reasons-item-icon">
                        <i class="icomoon-compare-options" aria-hidden="true"></i>
                    </div>
                    <div class="reasons-title">
                        <?php echo $loans_from; ?>
                    </div>
                    <span class="reasons-post-title"><?php echo $loans_from_interval; ?></span>
                </div>
                <div class="col-sm-4 reasons-item">
                    <div class="reasons-item-icon">
                        <i class="icomoon-doc-time" aria-hidden="true"></i> 
                    </div>
                    <div class="reasons-title">
                        <?php echo $funding; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="default-section_large content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <?= the_content(); ?>
            </div>
        </div>
    </div>
</div>
<!--<section class="blog-holder">-->
<!--    <div class="container">-->
<!--        --><?php //get_template_part( 'views/parts/content-landing-blog', get_post_format() ); ?>
<!--    </div>-->
<!--</section>-->

<?php  //get_template_part('views/parts/how-it-works'); ?>

<!--<div class="button-section lightgray-bg">-->
<!--    <div class="container">-->
<!--        <div class="row">-->
<!--            <div class="col-lg-12 text-center">-->
<!--                <a href="--><?//= cta_button_link(); ?><!--" class="btn btn-warning-custom btn-x2">See Loan Options</a>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->

<div class="container">
    <hr>
    <div class="disclaimer">
        <br>
        <p><strong>Important Disclosures. Please Read Carefully.</strong></p>
        <p>
            Persons facing serious financial difficulties should consider other alternatives or should seek out professional financial advice.
        </p>
        <p>
            This website is not an offer to lend.
            Lendgenius.com is not a lender or lending partner and does not make loan or credit decisions.
            Lendgenius.com connects interested persons with a lender or lending partner from its network of approved lenders and lending partners.
            Lendgenius.com does not control and is not responsible for the actions or inactions of any lender or lending partner, is not an agent, representative or broker of any lender or lending partner, and does not endorse any lender or lending partner.
            Lendgenius.com receives compensation from its lenders and lending partners, often based on a ping-tree model similar to Google AdWords where the highest available bidder is connected to the consumer.
            Regardless, Lendgenius.com’s service is always free to you.
        </p>
        <p>
            This service is not available in all states.
            If you request to connect with a lender or lending partner in a particular state where such loans are prohibited, or in a location where Lendgenius.com does not have an available lender or lending partner, you will not be connected to a lender or lending partner.
            You are urged to read and understand the terms of any loan offered by any lender or lending partner, and to reject any particular loan offer that you cannot afford to repay or that includes terms that are not acceptable to you.
        </p>
        <p>
            By submitting your information via this website, you are authorizing Lendgenius.com
            and/or lenders and lending partners in its network or other intermediaries to do a credit check,
            which may include verifying your social security number, driver license number or other identification,
            and a review of your creditworthiness. Credit checks are usually performed by one of the major credit bureaus such as Experian,
            Equifax and Trans Union, but also may include alternative credit bureaus such as Teletrack, DP Bureau or others. You also authorize
            Lendgenius.com to share your information and credit history with its network of approved lenders and lending partners.
        </p>
        <p>Our lenders offer loans with an Annual Percentage Rate (APR) of 35.99% and below. For qualified consumers, the maximum APR (including the interest rates plus fees and other costs) is 35.99%. All loans are subject to the lender’s approval based on its own unique underwriting criteria.</p>
        <p>Example: Loan Amount: $4,300.00, Annual Percentage Rate: 35.99%. Number of Monthly Payments: 30. Monthly Payment Amount: $219.36. Total Amount Payable: $6,581.78</p>
        <p>Loans include a minimum repayment plan of 12 months and a maximum repayment plan of 30 months.</p>
        <p>
            In some cases, you may be given the option of obtaining a loan from a tribal lender. Tribal lenders are subject to tribal and certain federal laws while being immune from state law including usury caps. If you are connected to a tribal lender, please understand that the tribal lender’s rates and fees may be higher than state-licensed lenders. Additionally, tribal lenders may require you to agree to resolve any disputes in a tribal jurisdiction. You are urged to read and understand the terms of any loan offered by any lender, whether tribal or state-licensed, and to reject any particular loan offer that you cannot afford to repay or that includes terms that are not acceptable to you.
        </p>
        <br>
        <p><strong>Lender’s or Lending Partner’s Disclosure of Terms.</strong></p>
        <p>The lenders and lending partners you are connected to will provide documents that contain all fees and rate information pertaining to the loan being offered, including any potential fees for late-payments and the rules under which you may be allowed (if permitted by applicable law) to refinance, renew or rollover your loan. Loan fees and interest rates are determined solely by the lender or lending partner based on the lender’s or lending partner’s internal policies, underwriting criteria and applicable law. Lendgenius.com has no knowledge of or control over the loan terms offered by a
            lender and lending partner. You are urged to read and understand the terms of any loan offered by any lenders and lending partners and to reject any particular loan offer that you cannot afford to repay or that includes terms that are not acceptable to you.</p>
        <br>
        <p><strong>Late Payments Hurt Your Credit Score</strong></p>
        <p>
            Please be aware that missing a payment or making a late payment can negatively impact your credit score.
            To protect yourself and your credit history, make sure you only accept loan terms that you can afford to repay.
            If you cannot make a payment on time, you should contact your lenders and lending partners immediately and discuss how to handle late payments.
        </p>
    </div>
</div>
<?php get_footer(); ?>
