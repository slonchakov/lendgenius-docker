<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 17.10.16
 * Time: 12:59
 */

$loan_icon = get_post_meta( get_the_ID(), 'loan_icon', true );
$unique_value_proposition = get_post_meta( get_the_ID(), 'unique_value_calc', true );
$repayment = get_post_meta( get_the_ID(), 'repayment_calc', true );
$interest_rates = get_post_meta( get_the_ID(), 'rates_calc', true );
?>

<div class="grid_row">
    <div class="column column_1">
        <div>
            <div class="image">
                <i class="<?php echo $loan_icon; ?>"></i>
            </div>
            <b><?php the_title(); ?></b>
        </div>
    </div>
    <div class="column column_2">
        <div>
            <p><?php echo $unique_value_proposition; ?></p>
        </div>
    </div>
    <div class="column column_3">
        <div>
            <p><?php echo $repayment; ?></p>
        </div>
    </div>
    <div class="column column_4">
        <div>
            <strong><?php echo $interest_rates; ?></strong>
        </div>
    </div>
</div>
