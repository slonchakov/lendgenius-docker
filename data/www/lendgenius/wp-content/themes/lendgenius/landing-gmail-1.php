<?php
/*
    Template Name: Landing Gmail 1
*/
$template_directory_uri = get_template_directory_uri();

get_header();
?>

    <div class="seashell-bg wrapper-full-height">
        <div class="header-page">
            <div class="brand-logo">
                Lend<span>Genius</span>
            </div>
        </div>
        <div class="container">
        <div class='sign-up'>
            <div class="row row-relative">
                <div class="col-md-6 col-md-6-custom">
					<div class="gmail-form">
                        <div class="form-header">
                            <h2><?php the_title(); ?></h2>
                        </div>
						<form-app>
							<div class="form-loader">
								<div class="centeringElement">        
									<div class="cube1"></div>
									<div class="cube2"></div>
								</div>
							</div>
						</form-app>
					</div>
                </div>
				<div class="col-md-6 col-md-6-custom">
                    
						<div class="img-pattern">
                            <?php
                            // Post thumbnail.
                            single_post_thumbnail('landing-gmail-general', 'img-responsive center-block');
                            ?>
                        </div>
						<div class="bullet-wrapper">
                            <?php
                            // Start the loop.
                            while ( have_posts() ) : the_post();
                                the_content();
                            endwhile; ?>
						<!--<div class="arrow-md">
							<div class="arrow"></div>
						</div>-->
					</div>
                </div>
            </div>
            </div>



        </div>
    </div>
<?php get_footer(); ?>