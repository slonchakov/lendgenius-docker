<?php
/*
    Template Name: About Us(v2)
*/
get_header();
//Custom fields init
$our_mission_1 = get_post_meta( get_the_ID(), 'our_mission_1', true );
$our_culture_1 = get_post_meta( get_the_ID(), 'our_culture_1', true );
$our_values_label_1 = get_post_meta( get_the_ID(), 'our_values_label_1', true );
$our_values_label_2 = get_post_meta( get_the_ID(), 'our_values_label_2', true );
$our_values_label_3 = get_post_meta( get_the_ID(), 'our_values_label_3', true );
$our_values_label_4 = get_post_meta( get_the_ID(), 'our_values_label_4', true );
$our_values_1 = get_post_meta( get_the_ID(), 'our_values_1', true );
$our_values_2 = get_post_meta( get_the_ID(), 'our_values_2', true );
$our_values_3 = get_post_meta( get_the_ID(), 'our_values_3', true );
$our_values_4 = get_post_meta( get_the_ID(), 'our_values_4', true );
$work_at_lendgenius_url = get_post_meta( get_the_ID(), 'work_at_lendgenius_url', true );
$more_about_culture = get_post_meta( get_the_ID(), 'more_about_culture', true );

?>
<div class="ovarlay"></div>
<div class="page-about-us">
    <div class="page-wraper">
        <div class="page-head">
            <div class="container">
                <h1 class="item-title-primary"><?php the_title(); ?></h1>
                <p><?php echo $our_mission_1; ?></p>
            </div>
        </div>
        <div class="page-body">

            <div class="container embed-form-container">
                <?= do_shortcode('[embed-form]'); ?>
            </div>

            <div class="container about-us-address">
                <p><?php echo $our_culture_1; ?></p>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();
?>
