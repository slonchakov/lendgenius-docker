<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 03.11.17
 *
 */
namespace App\Classes\RequireOnce;

$paths = [
    // array of all custom post types we include
    'app/custom_post_types',
    // set cache engine
    'app/cache',
    // helper functions
    'app/Helpers/Helpers',
    // all registered routes
    'app/routes',
    // bootstrap Register with list of actions and filters
    'app/register',
    // all shortcodes. needs refactoring
    'includes/shortcodes',
    'includes/widget_featured_posts',
    'includes/widget_category_latest_posts',
    'includes/widget_related_posts',
    'includes/loan_form'
];

if (is_admin()) {

    array_push($paths,
        'includes/admin_loan_edit',
        'includes/admin_info_page',
        'includes/admin_api_settings',
        'includes/admin_settings',
        'includes/admin_loans_page',
        'includes/admin_redirect_settings'
    );

} else {
    $paths[] = 'includes/breadcrumbs';
}

RequireService::get()->setPaths($paths);