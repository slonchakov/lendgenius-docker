<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 02.11.17
 *
 */

/**
 *  {slug} will be available in controller via $this->vars['slug']
 */
App\Classes\Router::get('redirect/{slug}', 'RedirectController@redirect');

/**
 * api routes
 *
 */
App\Classes\Router::get('api/lenders', 'LendersController@list');
App\Classes\Router::get('api/lender_categories', 'LendersController@categories');
App\Classes\Router::get('api/lender_filters', 'LenderFiltersController@list');

/**
 * Form endpoints
 */
App\Classes\Router::get('login-submit', 'FormLoginSubmitController@login');