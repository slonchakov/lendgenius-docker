<?php

namespace App\Filters;

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 14.09.17
 *
 */
use App\Classes\Filter\FilterRegister;

class FilterAdmin extends FilterRegister
{
    public function __construct()
    {
        $this->only_for = 'admin';
        $this->arguments = 2;

        $this->filter = [
            'add_custom_styles' => 'index',
            'post_row_actions'  => 'rd_duplicate_post_link',
            'admin_footer'      => 'mce_blank_target',
            'gettext'           => 'lg_post_author_field'
        ];
    }

    public function index($arg)
    {
        $arg .= '<link rel="stylesheet" href="/wp-content/themes/lendgenius/assets/styles/_table.min.css">';
        return $arg;
    }

    /**
     * @param $translation
     * @param $original
     * @return string
     * Change label at author select on post edit page. Very usefull function :)
     */
    public function lg_post_author_field( $translation, $original )
    {
        global $typenow;
        if ( isset($_GET['action'])  && $_GET['action'] === 'edit' && 'Author' == $original && $typenow == 'post') {
            return 'User';
        }
        return $translation;
    }

    public function mce_blank_target() {
        ?>
        <script>
            jQuery(document).on('click','.mce-btn button',function(){

                jQuery('#content_ifr').contents().find('a')
                    .each(function(){

                        var $this = jQuery(this);
                        $this.attr('data-mce-href') && !$this.attr('target') && $this.attr('target','_blank');

                    })
            });
        </script>
        <?php
    }

    /**
     * @param $actions
     * @param $post
     * @return mixed
     *  Add link to posts in admin for duplicating
     */
    public function rd_duplicate_post_link( $actions, $post ) {
        if (current_user_can('edit_posts')) {
            $actions['duplicate'] = '<a href="admin.php?action=rd_duplicate_post_as_draft&amp;post=' . $post->ID . '" title="Duplicate this item" rel="permalink">Duplicate</a>';
        }
        return $actions;
    }


}