<?php

namespace App\Filters;

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 14.09.17
 *
 */
use App\Classes\Filter\FilterRegister;

class FilterAdminTemplateDropdown extends FilterRegister
{
    public function __construct()
    {
        $post_id = $_GET['post'];

        $post_type = get_post_type($post_id);

        $this->filter = "theme_{$post_type}_templates";
    }

    public function index($post_templates)
    {

        return $post_templates;
    }


}