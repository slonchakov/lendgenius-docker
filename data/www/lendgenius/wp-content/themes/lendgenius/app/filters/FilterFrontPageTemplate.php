<?php


namespace App\Filters;


use App\Classes\Filter\FilterRegister;

class FilterFrontPageTemplate extends FilterRegister
{
    public function __construct()
    {
        $this->filter = [
            'frontpage_template_hierarchy' => 'index',
        ];
    }

    public function index($templates)
    {

        if (is_front_page() && get_page_template_slug() !== 'front-page.php') {
            array_unshift($templates, get_page_template_slug());
        }

        return $templates;
    }
}
