<?php

namespace App\Filters;

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 14.09.17
 *
 */
use App\Classes\Filter\FilterRegister;

class FilterCss extends FilterRegister
{
    public function __construct()
    {
        $this->filter = [
            'body_class' => 'index'
        ];
    }

    public function index($arg)
    {
        $bdy_classes = array('swipe-area','custom_body');
        if(is_page_template( 'page-sorry.php' )){
            $bdy_classes []= 'page-info';
        }


        return array_merge($arg, $bdy_classes );
    }


}