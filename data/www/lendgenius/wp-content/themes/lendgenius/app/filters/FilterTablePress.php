<?php

namespace App\Filters;

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 14.09.17
 *
 */
use App\Classes\Filter\FilterRegister;

class FilterTablePress extends FilterRegister
{
    public function __construct()
    {
        $this->filter = [
            'tablepress_table_css_classes' => 'index',
            'tablepress_cell_content' => 'add_css_class_to_cells_content',
            'tablepress_datatables_parameters' => 'add_tablepress_datatables_parameters',
            'tablepress_shortcode_table_shortcode_atts' => 'add_tablepress_shortcode_table_shortcode_atts'
        ];
        
        $this->arguments = 4;
        
    }

    public function index($css_classes)
    {
        if ( in_array('simple',$css_classes) )
        {
            $css_classes = array('responsive');
        }

        return $css_classes;
    }

    /**
     * @param $cell_content
     * @return mixed|string
     */
    public function add_css_class_to_cells_content( $cell_content )
    {
        if ( strpos($cell_content,'<img') !== false) {
            $cell_content = '<div class="table-funding-options">'. $cell_content .'</div>';
        }

        if ( strpos($cell_content,'<ul>') !== false) {
            $cell_content = str_replace('<ul>','<ul class="list-success-circle">',$cell_content);
        }

        return $cell_content;

    }

    /**
     * @param $parametres
     * @param $table_id
     * @param $html_id
     * @param $js_options
     * @return mixed
     * add css class to datatables js plugin
     */
    public function add_tablepress_datatables_parameters( $parametres, $table_id, $html_id, $js_options )
    {
//	Tables simple, complex table-responsive table-sm

        $tables                = get_option('tablepress_tables');
        $table_posts           = json_decode($tables);
        $current_table_post_id = $table_posts->table_post->{$table_id};
        $postmetastring        = get_post_meta($current_table_post_id,'_tablepress_table_options',true);
        $postmetaobject        = json_decode($postmetastring);
        $classname             = $postmetaobject->{'extra_css_classes'};

        $classname .= ($classname == 'complex') ? ' table-responsive ' : '';
        $parametres["dom"] = "dom: '<\"table-". $classname ."\"t>'";
        return $parametres;
    }

    /**
     * @param $arg
     * @return mixed
     * Remove <br /> line breaks from cells
     */
    function add_tablepress_shortcode_table_shortcode_atts( $arg )
    {
        $arg['convert_line_breaks'] = false;
        return $arg;
    }


}