<?php

namespace App\Filters;

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 14.09.17
 *
 */
use App\Classes\Filter\FilterRegister;

class FilterReturnFalse extends FilterRegister
{
    public function __construct()
    {
        $this->filter = [
            'wpcf7_load_js' => 'index',
            'wpcf7_load_css' => 'index',
            'the_generator' => 'index',
            'wp_calculate_image_sizes' => 'index',
            'wp_calculate_image_srcset' => 'index',
            'use_block_editor_for_post' => 'index'
        ];

        $this->priority = PHP_INT_MAX;
    }

    public function index($arg)
    {
        return false;
    }


}