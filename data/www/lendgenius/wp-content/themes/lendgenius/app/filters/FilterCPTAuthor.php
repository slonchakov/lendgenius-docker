<?php

namespace App\Filters;

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 14.09.17
 *
 */
use App\Classes\Filter\FilterRegister;

class FilterCPTAuthor extends FilterRegister
{
    public function __construct()
    {
        $this->filter = [
            'enter_title_here' => 'index',
            'admin_post_thumbnail_html' => 'changeFeaturedImageText'
        ];
    }

    public function index($title)
    {
        $screen = get_current_screen();
        if  ( 'authors' == $screen->post_type ) {
            $title = 'Enter author name';
        }
        return $title;
    }

    function changeFeaturedImageText( $content ) {
        if ( 'authors' === get_post_type() ) {
            $content = str_replace( 'Set featured image', __( 'Set author image', 'lendgenius' ), $content );
            $content = str_replace( 'Remove featured image', __( 'Remove author image', 'lendgenius' ), $content );
        }
        return $content;
    }


}