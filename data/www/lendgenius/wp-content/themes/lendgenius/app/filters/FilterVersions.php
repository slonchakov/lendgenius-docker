<?php

namespace App\Filters;

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 14.09.17
 *
 */
use App\Classes\Filter\FilterRegister;

class FilterVersions extends FilterRegister
{
    public function __construct()
    {
        $this->filter = [
            'wp_get_attachment_image_attributes' => 'add_version_query_to_attachment',
            'wp_get_attachment_image_src' => 'add_wp_get_attachment_image_src',
            'style_loader_src' => 'index',
            'script_loader_src' => 'index'
        ];

        $this->priority = 9999;
        
    }

    /**
     * @param $src
     * @return mixed
     * Remove ver from assets url because of recomendations from tools.pingdom.com
     */
    public function index($src)
    {
        if ( strpos( $src, 'ver=' ) )
            $src = remove_query_arg( 'ver', $src );

        return $src;
    }

    /**
     * @param $attr
     * @return mixed
     * Add version query to attachments
     */
    function add_version_query_to_attachment($attr)
    {
        if ( strpos($attr['src'], '?v=3') === false ) {
            $attr['src'] .= '?v=3';
        }

        return $attr;
    }

    /**
     * @param $image
     * @return mixed
     * Add version query if add_version_query_to_attachment not does it
     */
    function add_wp_get_attachment_image_src($image)
    {
        if ( strpos($image[0], '?v=3') === false ) {
            $image[0] .= '?v=3';
        }
        return $image;
    }

}