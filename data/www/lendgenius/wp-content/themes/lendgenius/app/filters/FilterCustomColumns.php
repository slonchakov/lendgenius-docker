<?php

namespace App\Filters;

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 14.09.17
 *
 */
use App\Classes\Filter\FilterRegister;

class FilterCustomColumns extends FilterRegister
{
    public function __construct()
    {
        $this->filter = 'manage_resources_posts_columns';
        $this->only_for = 'admin';
    }

    public function index($columns)
    {
        unset($columns['author']);
        $columns['resource_type'] = __( 'Type', 'lendgenius' );
        return $columns;
    }


}