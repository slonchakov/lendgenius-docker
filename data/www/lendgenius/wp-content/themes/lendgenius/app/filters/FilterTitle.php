<?php

namespace App\Filters;

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 14.09.17
 *
 */
use App\Classes\Filter\FilterRegister;

class FilterTitle extends FilterRegister
{
    public function __construct()
    {
        $this->filter = 'the_title';
        $this->arguments = 2;
    }

    public function index($title)
    {
        $arguments = func_get_args();
        $id = $arguments[1];
        $sub_title = get_post_meta( $id, 'sub_title', true );
        if('resources' == get_post_type($id) && !empty($sub_title) && is_admin()) {
            return $sub_title;
        }
        else {
            return $title;
        }
    }


}