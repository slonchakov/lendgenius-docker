<?php


namespace App\Filters;


use App\Classes\Filter\FilterRegister;

class FilterReturnTrue extends FilterRegister
{

    public function __construct()
    {
        $this->filter = [
            'gtm4wp_event-outbound' => 'index',
            'gtm4wp_event-downloads' => 'index',
            'gtm4wp_event-email-clicks' => 'index',
            'gtm4wp_integrate-wpcf7' => 'index',
            'gtm4wp_event-form-move' => 'index',
            'gtm4wp_event-social' => 'index',
            'gtm4wp_scroller-enabled' => 'index',
        ];

        $this->priority = PHP_INT_MAX;
    }

    public function index($arg)
    {
        return true;
    }
}