<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 02.11.17
 *
 */

namespace App\Filters;

use App\Classes\Filter\FilterRegister;
use App\Classes\Kernel;

class FilterIncludeTemplate extends FilterRegister
{
    private $kernel;

    public function __construct(Kernel $kernel)
    {
        $this->kernel = $kernel;
        $this->filter = "template_include";
    }

    /**
     * @return mixed
     * method witch executes by default on action adding
     * @var $arg
     */
    public function index($template)
    {
        $wp = $this->kernel->getWp();
        $response = $this->kernel->response();

        if ($response) {
            header("HTTP/1.0 200 OK");

            if ($wp->query_string == "error=404") {
                $wp->query_string = null;
                $wp->query_vars = null;
            }
            return $response;
        }

        return $template;
    }
}