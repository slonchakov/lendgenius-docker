<?php

namespace App\Filters;

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 14.09.17
 *
 */
use App\Classes\Filter\FilterRegister;

class FilterAzure extends FilterRegister
{
    public function __construct()
    {
        $this->filter = [
            'wp_calculate_image_srcset' => 'index',
            'wp_get_attachment_image_attributes' => 'bea_remove_srcset'
        ];
        $this->arguments = 5;
        $this->priority = PHP_INT_MAX;
    }

    public function index($arg)
    {
        $arguments = func_get_args();

        $sources = $arg;
        if ($sources == false) return false;
        $attachment_id = $arguments[4];

        $media_info = get_post_meta( $attachment_id, 'windows_azure_storage_info', true );

        if ( !empty( $media_info ) ) {
            foreach ( $sources as &$source ) {
                $img_file_name = substr( $source['url'], strrpos( $source['url'], '/' ) + 1 );

                if ( $img_file_name == substr( $media_info['blob'], strrpos( $media_info['blob'], '/' ) + 1 ) ) {
                    $source['url'] = "https://" . get_option( 'azure_storage_account_name' ) . ".blob.core.windows.net/" . $media_info['container'] . "/" . $media_info['blob'];
                } else {
                    foreach ( $media_info['thumbnails'] as $thumbnail ) {
                        if ( $img_file_name == substr( $thumbnail, strrpos( $thumbnail, '/' ) + 1 ) ) {
                            $source['url'] = "https://" . get_option( 'azure_storage_account_name' ) . ".blob.core.windows.net/" . $media_info['container'] . "/" . $thumbnail;
                            break;
                        }
                    }
                }
            }
        }

        return $sources;
    }

    /**
     * @param $attr
     * @return mixed
     * remove srcset from attachments
     */
    function bea_remove_srcset( $attr ) {
        if ( class_exists( '\BEA_Images' ) ) {
            return $attr;
        }
        if ( isset( $attr['sizes'] ) ) {
            unset( $attr['sizes'] );
        }
        if ( isset( $attr['srcset'] ) ) {
            unset( $attr['srcset'] );
        }
        return $attr;
    }
}