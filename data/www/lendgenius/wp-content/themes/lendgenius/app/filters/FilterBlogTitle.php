<?php

namespace App\Filters;

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 08.05.19
 *
 */
use App\Classes\Filter\FilterRegister;

class FilterBlogTitle extends FilterRegister
{
    public function __construct()
    {
        $this->filter = [
            'wpseo_title' => 'index',
            'the_title' => 'article',
            'wpseo_canonical' => 'canonical',
            'wpseo_metadesc' => 'description'
        ];

        $this->priority = 50;
    }

    public function index($title)
    {
        if (strpos($title, 'Blog') === 0) {
            if (get_query_var( 'paged' ) && get_query_var( 'paged' ) > 1) {
                $title = str_replace('Blog', 'Blog page '. get_query_var( 'paged' ), $title);
            }
        }

        $title = preg_replace('/20\d{2}/i', date('Y'), $title);

        return $title;
    }
    
    public function canonical($canonical)
    {
        if (strpos($canonical, 'blog') !== false) {
            if (get_query_var( 'paged' ) && get_query_var( 'paged' ) > 1) {
                $canonical .= 'page/'. get_query_var( 'paged' );
            }
        }

        return $canonical;
    }

    public function article($title)
    {
        return preg_replace('/20\d{2}/i', date('Y'), $title);
    }

    public function description($content)
    {
        return preg_replace('/20\d{2}/i', date('Y'), $content);
    }
}