<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 07.09.17
 *
 */

//Logger
require 'includes/simple_logger.php';

//Breadcrumbs
require 'includes/breadcrumbs.php';

//Short codes
require 'includes/shortcodes.php';
//Widget Featured Posts
require 'includes/widget_featured_posts.php';
//Widget Category Latest Posts
require 'includes/widget_category_latest_posts.php';
//Widget Related Posts
require 'includes/widget_related_posts.php';
//Loan edit customization
require 'includes/admin_loan_edit.php';
//Info page in the admin panel
require 'includes/admin_info_page.php';
//API Settings page in the admin panel
require 'includes/admin_api_settings.php';
//Settings page in the admin panel
require 'includes/admin_settings.php';

//Custom Post Types
require 'includes/custom_post_type/loans.php';
require 'includes/custom_post_type/faqs.php';
require 'includes/custom_post_type/feedbacks.php';
require 'includes/custom_post_type/resources.php';
require 'includes/custom_post_type/lboxes.php';
require 'includes/custom_post_type/author.php';

//Loans page under Resources
require 'includes/admin_loans_page.php';
//Loan Form
require 'includes/loan_form.php';

function wpse_setup_theme() {
    add_theme_support( 'post-thumbnails' );
    add_image_size( 'sidebar-thumb', 370, 147, true );
    add_image_size( 'top-article-thumb', 370, 250, true );
    add_image_size( 'post-thumb', 770, 250, true );
    add_image_size( 'post-thumb-general', 870, 200, true );
    add_image_size( 'post-thumb-sub', 270, 115, true );
    add_image_size( 'lender-box', 150, 63, true );
    add_image_size( 'author-thumb', 170, 170, true );
    add_image_size( 'author-thumb-posts', 370, 207, true );
    add_image_size( 'category-latest-post', 54, 54, true );
//    add_image_size( 'landing-ppc-general', 2000, 667, true );
    add_image_size( 'landing-gmail-general', 570, 478, true );

//    add_image_size( 'landing-ppc-mobile', 400, 342, true );
//    add_image_size( 'new-post-general', 2000, 320, true );

    add_image_size( 'hero-image-2560-667', 2560, 667, true );
    add_image_size( 'hero-image-1280-350', 1280, 350, true );
    add_image_size( 'hero-image-1280-563', 1280, 563, true );
    add_image_size( 'hero-image-770-350', 770, 350, true );
    add_image_size( 'hero-image-400-342', 400, 342, true );

    add_image_size( 'get-started-general', 325, 685, true );
    add_image_size( 'form-front-mobile', 770, 350, true );
    add_image_size( 'feedback-post', 295, 417, true );
}

add_action( 'after_setup_theme', 'wpse_setup_theme' );

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    //wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

function cta_button_link(){
    return get_option( 'see_loan_option_url' );
}

function get_cus_link($key){
    switch ($key) {
        case 'resources-videos':
            return "/resources/videos/";
            break;
        case 'resources-tools':
            return "/resources/tools-and-templates/";
            break;
        case 'resources-credit-cards':
            return "/best-business-credit-cards/";
            break;
        case 'resources-calculators':
            return "/resources/loan-calculators/";
            break;
    }
}

add_action( 'wp_head', 'local_add_customizer_css' );
function local_add_customizer_css() {
    return false;
    $menu_height = et_get_option( 'menu_height', '66' );
    $fixed_menu_height = et_get_option( 'minimized_menu_height', '40' );
    $menu_height = $menu_height-20;
    $fixed_menu_height = $fixed_menu_height-20;

    $button_text_size = et_get_option( 'all_buttons_font_size', '20' );
    $button_text_color = et_get_option( 'all_buttons_text_color', '#ffffff' );
    $button_bg_color = et_get_option( 'all_buttons_bg_color', 'rgba(0,0,0,0)' );
    $button_border_width = et_get_option( 'all_buttons_border_width', '2' );
    $button_border_color = et_get_option( 'all_buttons_border_color', '#ffffff' );
    $button_border_radius = et_get_option( 'all_buttons_border_radius', '3' );
    $button_text_style = et_get_option( 'all_buttons_font_style', '', '', true );
    $button_icon = et_get_option( 'all_buttons_selected_icon', '5' );
    $button_spacing = et_get_option( 'all_buttons_spacing', '0' );
    $button_icon_color = et_get_option( 'all_buttons_icon_color', '#ffffff' );
    $button_text_color_hover = et_get_option( 'all_buttons_text_color_hover', '#ffffff' );
    $button_bg_color_hover = et_get_option( 'all_buttons_bg_color_hover', 'rgba(255,255,255,0.2)' );
    $button_border_color_hover = et_get_option( 'all_buttons_border_color_hover', 'rgba(0,0,0,0)' );
    $button_border_radius_hover = et_get_option( 'all_buttons_border_radius_hover', '3' );
    $button_spacing_hover = et_get_option( 'all_buttons_spacing_hover', '0' );
    $button_icon_size = 1.6 * intval( $button_text_size );
    $body_header_size = et_get_option( 'body_header_size', '30' );
    ?>
    <style>
        @media only screen and ( min-width: 981px ) {
            ol.h2 li { font-size: <?php echo esc_html( intval( $body_header_size * .86 ) ) ; ?>px; }

        <?php
 if ( '66' !== $menu_height ) { ?>
            .et_header_style_left #et-top-navigation, .et_header_style_split #et-top-navigation  { padding: <?php echo esc_html( round( $menu_height / 2 ) ); ?>px 0 0 0 !important; }
            .et_header_style_left #et-top-navigation nav > ul > li , .et_header_style_split #et-top-navigation nav > ul > li  { padding-bottom: <?php echo esc_html( round ( $menu_height / 2 ) ); ?>px !important; }
            .et_header_style_centered #main-header .logo_container { height: <?php echo esc_html( $menu_height ); ?>px !important; }
            .et_header_style_centered #top-menu > li  { padding-bottom: <?php echo esc_html( round ( $menu_height * .18 ) ); ?>px !important; }
            .et_header_style_split .centered-inline-logo-wrap { width: <?php echo esc_html( $menu_height ); ?>px !important; margin: -<?php echo esc_html( $menu_height ); ?>px 0 !important; }
            .et_header_style_split .centered-inline-logo-wrap #logo { max-height: <?php echo esc_html( $menu_height ); ?>px !important; }
        <?php } ?>
        <?php if ( '40' !== $fixed_menu_height ) { ?>
            .et_header_style_left .et-fixed-header #et-top-navigation, .et_header_style_split .et-fixed-header #et-top-navigation { padding: <?php echo esc_html( intval( round( $fixed_menu_height / 2 ) ) ); ?>px 0 0 0 !important; }
            .et_header_style_left .et-fixed-header #et-top-navigation nav > ul > li , .et_header_style_split .et-fixed-header #et-top-navigation nav > ul > li   { padding-bottom: <?php echo esc_html( round( $fixed_menu_height / 2 ) ); ?>px !important; }
            .et_header_style_centered #main-header.et-fixed-header .logo_container { height: <?php echo esc_html( $fixed_menu_height ); ?>px !important; }
            .et_header_style_split .et-fixed-header .centered-inline-logo-wrap { width: <?php echo esc_html( $fixed_menu_height ); ?>px !important; margin: -<?php echo esc_html( $fixed_menu_height ); ?>px 0 !important;  }
            .et_header_style_split .et-fixed-header .centered-inline-logo-wrap #logo { max-height: <?php echo esc_html( $fixed_menu_height ); ?>px !important; }
        <?php } ?>
        }

        <?php if ( '20' !== $button_text_size || '#ffffff' !== $button_text_color || 'rgba(0,0,0,0)' !== $button_bg_color || '2' !== $button_border_width || '#ffffff' !== $button_border_color || '3' !== $button_border_radius || '' !== $button_text_style || '0' !== $button_spacing ) { ?>
        body .gform_button.button
        {
        <?php if ( '20' !== $button_text_size ) { ?>
            font-size: <?php echo esc_html( $button_text_size ); ?>px;
        <?php } ?>
        <?php if ( 'rgba(0,0,0,0)' !== $button_bg_color ) { ?>
            background: <?php echo esc_html( $button_bg_color ); ?>;
        <?php } ?>
        <?php if ( '2' !== $button_border_width ) { ?>
            border-width: <?php echo esc_html( $button_border_width ); ?>px !important;
        <?php } ?>
        <?php if ( '#ffffff' !== $button_border_color ) { ?>
            border-color: <?php echo esc_html( $button_border_color ); ?>;
        <?php } ?>
        <?php if ( '3' !== $button_border_radius ) { ?>
            border-radius: <?php echo esc_html( $button_border_radius ); ?>px;
        <?php } ?>
        <?php if ( '' !== $button_text_style ) { ?>
        <?php echo esc_html( et_pb_print_font_style( $button_text_style ) ); ?>;
        <?php } ?>
        <?php if ( '0' !== $button_spacing ) { ?>
            letter-spacing: <?php echo esc_html( $button_spacing ); ?>px;
        <?php } ?>
        }
        body.et_pb_button_helper_class .gform_button.button {
        <?php if ( '#ffffff' !== $button_text_color ) { ?>
            color: <?php echo esc_html( $button_text_color ); ?>;
        <?php } ?>
        }
        <?php } ?>
        <?php if ( '5' !== $button_icon || '#ffffff' !== $button_icon_color || '20' !== $button_text_size ) { ?>
        body .gform_button.button:after
        {
        <?php if ( '5' !== $button_icon ) { ?>
        <?php if ( "'" === $button_icon ) { ?>
            content: "<?php echo htmlspecialchars_decode( $button_icon ); ?>";
        <?php } else { ?>
            content: '<?php echo htmlspecialchars_decode( $button_icon ); ?>';
        <?php } ?>
            font-size: <?php echo esc_html( $button_text_size ); ?>px;
        <?php } else { ?>
            font-size: <?php echo esc_html( $button_icon_size ); ?>px;
        <?php } ?>
        <?php if ( '#ffffff' !== $button_icon_color ) { ?>
            color: <?php echo esc_html( $button_icon_color ); ?>;
        <?php } ?>
        }
        <?php } ?>
        <?php if ( '#ffffff' !== $button_text_color_hover || 'rgba(255,255,255,0.2)' !== $button_bg_color_hover || 'rgba(0,0,0,0)' !== $button_border_color_hover || '3' !== $button_border_radius_hover || '0' !== $button_spacing_hover ) { ?>
        body .gform_button.button:hover
        {
        <?php if ( '#ffffff' !== $button_text_color_hover ) { ?>
            color: <?php echo esc_html( $button_text_color_hover ); ?> !important;
        <?php } ?>
        <?php if ( 'rgba(255,255,255,0.2)' !== $button_bg_color_hover ) { ?>
            background: <?php echo esc_html( $button_bg_color_hover ); ?> !important;
        <?php } ?>
        <?php if ( 'rgba(0,0,0,0)' !== $button_border_color_hover ) { ?>
            border-color: <?php echo esc_html( $button_border_color_hover ); ?> !important;
        <?php } ?>
        <?php if ( '3' !== $button_border_radius_hover ) { ?>
            border-radius: <?php echo esc_html( $button_border_radius_hover ); ?>px;
        <?php } ?>
        <?php if ( '0' !== $button_spacing_hover ) { ?>
            letter-spacing: <?php echo esc_html( $button_spacing_hover ); ?>px;
        <?php } ?>
        }
        <?php } ?>
    </style>
<?php }

/* REDIRECTS ATACHMENT PAGES TO THEIR PARENTS */
//add_action('template_redirect', 'gamma_attachment_redirect',1);
function gamma_attachment_redirect() {
    global $post;
    if ( is_attachment() && isset($post->post_parent) && is_numeric($post->post_parent) && ($post->post_parent != 0) ) {
        wp_redirect(get_permalink($post->post_parent), 301); // permanent redirect to post/page where image or document was uploaded
        exit;
    } elseif ( is_attachment() && isset($post->post_parent) && is_numeric($post->post_parent) && ($post->post_parent < 1) ) {   // for some reason it doesnt works checking for 0, so checking lower than 1 instead...
        wp_redirect(get_bloginfo('wpurl'), 302); // temp redirect to home for image or document not associated to any post/page
        exit;
    }
}

/* ADD BUILDER TO POSTS */
function gamma_builder_post_types($post_types = array()){
    if(!in_array('post',$post_types)) $post_types[] = 'post';
    return $post_types;
}
//add_filter('et_pb_builder_post_types','gamma_builder_post_types',10,1);

/* ADD SHORTCODE VIEWER TO BUILDER */
function gamma_before_main_editor( $post ) {
    if ( ! in_array( $post->post_type, array( 'post' ) ) ) return;
    $is_builder_used = 'on' === get_post_meta( $post->ID, '_et_pb_use_builder', true ) ? true : false;
    printf( '<a href="#" id="et_pb_toggle_builder" data-builder="%2$s" data-editor="%3$s" class="button button-primary button-large%5$s">%1$s</a><div id="et_pb_main_editor_wrap"%4$s>',
        ( $is_builder_used ? __( 'Use Default Editor', 'Divi' ) : __( 'Use Page Builder', 'Divi' ) ),
        __( 'Use Page Builder', 'Divi' ),
        __( 'Use Default Editor', 'Divi' ),
        ( $is_builder_used ? ' class="et_pb_hidden"' : '' ),
        ( $is_builder_used ? ' et_pb_builder_is_used' : '' )
    );
}
//add_action( 'edit_form_after_title', 'gamma_before_main_editor' );

function gamma_before_page_builder(){
    echo '
		<style>
			#gamma_layout_controls {
				padding-top:20px;
			}
			#gamma_layout_controls .gamma-layout-buttons-source:before {
			  content: "\50";
			}
			#gamma_layout_content_wrapper {
				display:none;
			}
			#TB_ajaxContent{
				width: 100% !important;
  				box-sizing: border-box;
			}
			.processing:before {
				opacity: 0;
				filter: alpha(opacity=0);
				visibility: hidden;
			}
			#gamma_layout_controls .spinner {
				position: absolute;
			}
			.processing .spinner{
				  visibility: visible;
				  opacity: 1;
				  filter: alpha(opacity=100);
				  position: absolute;
				  left: 10px;
				  top: 15px;
			}
		</style>
		<script type="text/javascript">
			jQuery(window).load( function() {
				
				jQuery("body").on("click", "#gamma-layout-buttons-source", function (e) {
					if(!jQuery(this).hasClass("processing")){
						jQuery(this).addClass("processing");
						var data = {
							"action": "gamma_get_shortcodes",
							"post_content": jQuery("#wp-content-editor-container .wp-editor-area").html() //tinyMCE.editors.content.getContent()
						};
						jQuery.post(ajaxurl, data, function(response) {
							jQuery("#gamma_layout_content").html(response.replace(/\\\/g,""));
							tb_show("View Shortcodes", "#TB_inline?inlineId=gamma_layout_content_wrapper");
							jQuery("#gamma-layout-buttons-source").removeClass("processing");
						});
					}
					
				});
				
			});
		</script>
		<div id="gamma_layout_controls">
			<a href="javascript:void(0);" id="gamma-layout-buttons-source" class="et-pb-layout-buttons gamma-layout-buttons-source"><span>View Shortcodes</span><div class="spinner"></div></a>
			<div id="gamma_layout_content_wrapper"><p><div id="gamma_layout_content"></div></p></div>
		</div>';
}
add_action( 'et_pb_before_page_builder', 'gamma_before_page_builder');

function gamma_get_shortcodes_callback() {
    $shortcodes  = gamma_find_shortcodes($_POST['post_content'],0);
    if(strlen($shortcodes)==0) echo 'No ShortCodes Found';
    echo $shortcodes;
    wp_die(); // this is required to terminate immediately and return a proper response
}
add_action( 'wp_ajax_gamma_get_shortcodes', 'gamma_get_shortcodes_callback' );
function gamma_find_shortcodes($content,$level) {
    $pattern = get_shortcode_regex();
    $result = '';
    $new_line = '<br /><br />';
    $level_offset = str_repeat("&nbsp; &nbsp; &nbsp; &nbsp;",$level);
    if (   preg_match_all( '/'. $pattern .'/s', $content, $matches, PREG_PATTERN_ORDER ) )
    {

        foreach($matches[2] as $key=>$shortode){
            $result .= $level_offset.'['.$shortode.((strlen($matches[3][$key]))?' '.$matches[3][$key]:'').']'.$new_line;
            if(strlen($matches[5][$key])>0){
                $result .= gamma_find_shortcodes($matches[5][$key],$level+1);
            }
            $closing_tag = '[/'.$shortode.']';
            if(strpos($content,$closing_tag)!==false){
                $result .= $level_offset.$closing_tag.$new_line;
            }
        }
        return $result;
    }else{
        return $level_offset.'...'.$new_line;
    }
}

if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
function my_jquery_enqueue() {
    wp_deregister_script('jquery');
    wp_enqueue_script('jquery');
}

if (!is_admin()) add_action("wp_enqueue_scripts", "my_datatables_enqueue", 11);
function my_datatables_enqueue() {
    if (!is_single() ) return;
    wp_deregister_script('datatables');
    $js_file = 'js/jquery.datatables.min.js';
    $js_url = plugins_url( $js_file, TABLEPRESS__FILE__ );
    $js_url = apply_filters( 'tablepress_datatables_js_url', $js_url, $js_file );
    wp_enqueue_script( 'tablepress-datatables', $js_url, array(  ), TablePress::version, true );
}

/*/*/
add_shortcode('slide_text','slide_text');
function slide_text($atts, $content = null) {
    extract(shortcode_atts(array(
        'class' => '',
        'align' => 'left'
    ), $atts, 'slide_text'));

    $content = et_content_helper($content);

    $output = do_shortcode('
	[et_pb_text admin_label="Text" background_layout="dark" text_orientation="'.$align.'" use_border_color="off" border_color="#ffffff" border_style="solid" module_class="'.$class.'"]
		'.$content.'
	[/et_pb_text]
	');
    ;

    return $output;
}
//add_shortcode('footer_social','footer_social');
function footer_social($atts, $content = null) {
    extract(shortcode_atts(array(
        'logo' => '',
        'align' => 'center'
    ), $atts, 'footer_social'));
    $image = ( $user_logo = et_get_option( 'divi_logo' ) ) && '' != $user_logo
        ? $user_logo
        : $template_directory_uri . '/images/logo.png';
    $output = '<div class="footer_social et_pb_bg_layout_dark"><img src="'.esc_attr( ($logo=='')?$image:$logo ).'" alt='.esc_attr( get_bloginfo( 'name' ) ).'" id="logo" />';
    if ( false !== et_get_option( 'show_footer_social_icons', true ) ) {
        $output .= '<ul class="et-social-icons">';

        if ( 'on' === et_get_option( 'divi_show_facebook_icon', 'on' ) ) :
            $output .= '<li class="et-social-icon et-social-facebook">
				<a href="'.esc_url( et_get_option( 'divi_facebook_url', '#' ) ).'" class="icon">
					<span>'. esc_html( 'Facebook', 'Divi' ).'</span>
				</a>
			</li>';
        endif;
        if ( 'on' === et_get_option( 'divi_show_twitter_icon', 'on' ) ) :
            $output .= '<li class="et-social-icon et-social-twitter">
				<a href="'.esc_url( et_get_option( 'divi_twitter_url', '#' ) ).'" class="icon">
					<span>'. esc_html( 'Twitter', 'Divi' ).'</span>
				</a>
			</li>';
        endif;
        if ( 'on' === et_get_option( 'divi_show_google_icon', 'on' ) ) :
            $output .= '<li class="et-social-icon et-social-google-plus">
				<a href="'.esc_url( et_get_option( 'divi_google_url', '#' ) ).'" class="icon">
					<span>'. esc_html( 'Google', 'Divi' ).'</span>
				</a>
			</li>';
        endif;
        if ( 'on' === et_get_option( 'divi_show_rss_icon', 'on' ) ) :
            $et_rss_url = '' !== et_get_option( 'divi_rss_url' )
                ? et_get_option( 'divi_rss_url' )
                : get_bloginfo( 'rss2_url' );

            $output .= '<li class="et-social-icon et-social-rss">
				<a href="'.esc_url( $et_rss_url ).'" class="icon">
					<span>'. esc_html( 'RSS', 'Divi' ).'</span>
				</a>
			</li>';
        endif;

        $output .= '</ul>';

    }
    $output .= '</div>';
    return $output;
}

/*add_action( 'init', 'gamma_setup_builder' );
function gamma_setup_builder(){
	$action_hook = is_admin() ? 'wp_loaded' : 'wp';
	add_action( $action_hook, 'gamma_builder_add_main_elements',9999 );
}*/
//add_action( 'init', 'gamma_divi_init_builder', 9999 );

function gamma_divi_init_builder() {
    global $pagenow;

    $is_admin = is_admin();
    if($is_admin) return;
    $action_hook = $is_admin ? 'wp_loaded' : 'wp';
    $required_admin_pages = array( 'edit.php', 'post.php', 'post-new.php', 'admin.php', 'customize.php', 'edit-tags.php', 'admin-ajax.php' ); // list of admin pages where we need to load builder files
    $specific_filter_pages = array( 'edit.php', 'admin.php', 'edit-tags.php' ); // list of admin pages where we need more specific filtering

    $is_edit_library_page = 'edit.php' === $pagenow && isset( $_GET['post_type'] ) && 'et_pb_layout' === $_GET['post_type'];
    $is_role_editor_page = 'admin.php' === $pagenow && isset( $_GET['page'] ) && 'et_divi_role_editor' === $_GET['page'];
    $is_import_page = 'admin.php' === $pagenow && isset( $_GET['import'] ) && 'wordpress' === $_GET['import']; // Page Builder files should be loaded on import page as well to register the et_pb_layout post type properly
    $is_edit_layout_category_page = 'edit-tags.php' === $pagenow && isset( $_GET['taxonomy'] ) && 'layout_category' === $_GET['taxonomy'];


    // load builder files on front-end and on specific admin pages only.
    if ( ! $is_admin || ( $is_admin && in_array( $pagenow, $required_admin_pages ) && ( ! in_array( $pagenow, $specific_filter_pages ) || $is_edit_library_page || $is_role_editor_page || $is_edit_layout_category_page || $is_import_page ) ) ) {

        add_action( $action_hook, 'gamma_builder_add_main_elements',9999 );
    }
}
function gamma_builder_add_main_elements(){
    //return;
    require 'includes/simple_html_dom.php';
    require 'includes/builder/extension.php';
}

add_action( 'after_setup_theme', 'register_custom_menus' );
function register_custom_menus() {
    register_nav_menus( array(
        'footer-menu-secondary' => 'Secondary Footer Menu',
    ) );
}


function cust_maintenance($content, $title = 'Under Construction')
{
    header("HTTP/1.1 503 Service Unavailable");
    require_once 'maintenance.php';
    exit;
}


add_filter( 'body_class', function( $classes ) {
    return array_merge($classes, array( 'custom_body' ) );
} );


add_action( 'wp_loaded', function (){

    $post = get_post(url_to_postid($_SERVER['REQUEST_SCHEME'].'://'. $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']));

    //For Divi content stay some functional
    if(!empty($post->post_content) and stripos($post->post_content,'[et_pb_section' !== false)){
        remove_action('wp_head', 'et_divi_add_customizer_css');
    }

    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('wp_print_styles', 'print_emoji_styles');

    remove_action('wp_head', 'head_addons', 7);
    remove_action('wp_head', 'wp_generator');

    remove_action('wp_head','feed_links_extra', 3);
    remove_action('wp_head','feed_links', 2);
    remove_action('wp_head','rsd_link');
    remove_action('wp_head','wlwmanifest_link');

    remove_action( 'wp_head', 'et_add_viewport_meta');

    remove_action( 'wp_head', 'rest_output_link_wp_head');
    remove_action( 'wp_head', 'wp_oembed_add_discovery_links');
    remove_action( 'template_redirect', 'rest_output_link_header', 11);

    remove_action( 'wp_enqueue_scripts', 'et_builder_load_modules_styles', 11);
    remove_action( 'wp_enqueue_scripts', 'et_shortcodes_css_and_js');

    add_filter('the_generator', '__return_empty_string');

    add_filter('style_loader_src', 'rem_wp_ver_css_js', 9999);

    add_filter('script_loader_src', 'rem_wp_ver_css_js', 9999);
});

function et_builder_enqueue_font(){
    return false;
}

function et_divi_fonts_url(){
    return false;
}

function rem_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );

    return $src;
}

if ( ! function_exists( 'single_post_thumbnail' ) ) :
    function single_post_thumbnail($size='thumbnail', $im_class = 'item-image-src', $get_post_thumbnail = false, $post_id = false) {
        global $post;
        if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
            return;
        }

        if ( is_singular() ) :
            if ( $get_post_thumbnail ) :
                if ( $post_id ) :
                    return get_the_post_thumbnail($post_id, $size, array( 'class' => $im_class ));
                else:
                    return get_the_post_thumbnail($post, $size, array( 'class' => $im_class ));
                endif;
            else:
                the_post_thumbnail($size, array( 'class' => $im_class ));
            endif;
        else :
            if ( $get_post_thumbnail ) :
                if ( $post_id ) :
                    return get_the_post_thumbnail($post_id, $size, array( 'class' => $im_class ));
                else:
                    return get_the_post_thumbnail($post, $size, array( 'class' => $im_class ));
                endif;
            else:
                the_post_thumbnail($size, array( 'class' => $im_class ));
            endif;

        endif; // End is_singular()
    }
endif;

if ( function_exists('register_sidebar') ) {
    register_sidebar(array(
        'name' => 'Blog Article Sidebar',
        'id' => 'sidebar-single',
        'description' => 'Appears as the sidebar on the "blog-article" page',
        'before_widget' => '<div class="content-aside-items">',
        'after_widget' => '</div>',
        'before_title' => '<div class="item-title-primary">',
        'after_title' => '</div>',
    ));

    register_sidebar(array(
        'name' => 'Blog Sidebar',
        'id' => 'sidebar-blog',
        'description' => 'Appears as the sidebar on the "blog" page',
        'before_widget' => '<div class="content-aside-items">',
        'after_widget' => '</div>',
        'before_title' => '<div class="item-title-primary">',
        'after_title' => '</div>',
    ));

    register_sidebar(array(
        'name' => 'Author Sidebar',
        'id' => 'sidebar-author',
        'description' => 'Appears as the sidebar on the "author" page',
        'before_widget' => '<div class="content-aside-items">',
        'after_widget' => '</div>',
        'before_title' => '<div class="item-title-primary">',
        'after_title' => '</div>',
    ));

    register_sidebar(array(
        'name' => 'How It Works Sidebar',
        'id' => 'sidebar-how-it-works',
        'description' => 'Appears as the sidebar on the "how it works" page',
        'before_widget' => '<div class="content-aside-items">',
        'after_widget' => '</div>',
        'before_title' => '<div class="item-title-primary">',
        'after_title' => '</div>',
    ));

    register_sidebar(array(
            'name' => 'Under Post Area',
            'id' => 'under-post-area',
        )
    );
    register_sidebar( array(
        'name' => esc_html__( 'Footer Area', 'lendgenius' ) . ' #1',
        'id' => 'sidebar-2',
        'before_widget' => '<div id="%1$s" class="fwidget et_pb_widget %2$s">',
        'after_widget' => '</div> <!-- end .fwidget -->',
        'before_title' => '<h4 class="title">',
        'after_title' => '</h4>',
    ) );

    register_sidebar( array(
        'name' => esc_html__( 'Footer Area', 'lendgenius' ) . ' #2',
        'id' => 'sidebar-3',
        'before_widget' => '<div id="%1$s" class="fwidget et_pb_widget %2$s">',
        'after_widget' => '</div> <!-- end .fwidget -->',
        'before_title' => '<h4 class="title">',
        'after_title' => '</h4>',
    ) );

    register_sidebar( array(
        'name' => esc_html__( 'Footer Area', 'lendgenius' ) . ' #3',
        'id' => 'sidebar-4',
        'before_widget' => '<div id="%1$s" class="fwidget et_pb_widget %2$s">',
        'after_widget' => '</div> <!-- end .fwidget -->',
        'before_title' => '<h4 class="title">',
        'after_title' => '</h4>',
    ) );
}
//Top Articles
function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);


function wpb_track_post_views ($post_id) {
    if ( !is_single() ) return;
    if ( empty ( $post_id) ) {
        global $post;
        $post_id = $post->ID;
    }
    wpb_set_post_views($post_id);
}
add_action( 'wp_head', 'wpb_track_post_views');

add_filter( 'manage_resources_posts_columns', 'set_custom_edit_resource_columns' );
add_action( 'manage_resources_posts_custom_column' , 'custom_resource_column', 10, 2 );

function set_custom_edit_resource_columns($columns) {
    unset($columns['author']);
    $columns['resource_type'] = __( 'Type', 'lendgenius' );
    return $columns;
}

function custom_resource_column( $column, $post_id ) {
    switch ( $column ) {
        case 'resource_type' :
            echo get_post_meta( $post_id , 'resource_type' , true );
            break;

    }
}

add_filter('the_title', 'my_meta_on_title',10, 2);
function my_meta_on_title($title, $id) {
    $sub_title = get_post_meta( $id, 'sub_title', true );
    if('resources' == get_post_type($id) && !empty($sub_title) && is_admin()) {
        return $sub_title;
    }
    else {
        return $title;
    }
}

// Register and load the featured posts widget
function wp_load_featured_posts_widget() {
    register_widget( 'widget_featured_posts' );
}
add_action( 'widgets_init', 'wp_load_featured_posts_widget' );

// Register and load the category latest posts widget
function wp_load_category_latest_posts_widget() {
    register_widget( 'widget_category_latest_posts' );
}
add_action( 'widgets_init', 'wp_load_category_latest_posts_widget' );

// Register and load the category latest posts widget
function wp_load_related_posts_widget() {
    register_widget( 'widget_related_posts' );
}
add_action( 'widgets_init', 'wp_load_related_posts_widget' );

/*
 * Function creates post duplicate as a draft and redirects then to the edit post screen
 */
function rd_duplicate_post_as_draft(){
    global $wpdb;
    if (! ( isset( $_GET['post']) || isset( $_POST['post'])  || ( isset($_REQUEST['action']) && 'rd_duplicate_post_as_draft' == $_REQUEST['action'] ) ) ) {
        wp_die('No post to duplicate has been supplied!');
    }

    /*
     * get the original post id
     */
    $post_id = (isset($_GET['post']) ? absint( $_GET['post'] ) : absint( $_POST['post'] ) );
    /*
     * and all the original post data then
     */
    $post = get_post( $post_id );

    /*
     * if you don't want current user to be the new post author,
     * then change next couple of lines to this: $new_post_author = $post->post_author;
     */
    $current_user = wp_get_current_user();
    $new_post_author = $current_user->ID;

    /*
     * if post data exists, create the post duplicate
     */
    if (isset( $post ) && $post != null) {

        /*
         * new post data array
         */
        $args = array(
            'comment_status' => $post->comment_status,
            'ping_status'    => $post->ping_status,
            'post_author'    => $new_post_author,
            'post_content'   => $post->post_content,
            'post_excerpt'   => $post->post_excerpt,
            'post_name'      => $post->post_name,
            'post_parent'    => $post->post_parent,
            'post_password'  => $post->post_password,
            'post_status'    => 'draft',
            'post_title'     => $post->post_title,
            'post_type'      => $post->post_type,
            'to_ping'        => $post->to_ping,
            'menu_order'     => $post->menu_order
        );

        /*
         * insert the post by wp_insert_post() function
         */
        $new_post_id = wp_insert_post( $args );

        /*
         * get all current post terms ad set them to the new post draft
         */
        $taxonomies = get_object_taxonomies($post->post_type); // returns array of taxonomy names for post type, ex array("category", "post_tag");
        foreach ($taxonomies as $taxonomy) {
            $post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
            wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
        }

        /*
         * duplicate all post meta just in two SQL queries
         */
        $post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
        if (count($post_meta_infos)!=0) {
            $sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
            foreach ($post_meta_infos as $meta_info) {
                $meta_key = $meta_info->meta_key;
                $meta_value = addslashes($meta_info->meta_value);
                $sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
            }
            $sql_query.= implode(" UNION ALL ", $sql_query_sel);
            $wpdb->query($sql_query);
        }


        /*
         * finally, redirect to the edit post screen for the new draft
         */
        wp_redirect( admin_url( 'post.php?action=edit&post=' . $new_post_id ) );
        exit;
    } else {
        wp_die('Post creation failed, could not find original post: ' . $post_id);
    }
}
add_action( 'admin_action_rd_duplicate_post_as_draft', 'rd_duplicate_post_as_draft' );

/*
 * Add the duplicate link to action list for post_row_actions
 */
function rd_duplicate_post_link( $actions, $post ) {
    if (current_user_can('edit_posts')) {
        $actions['duplicate'] = '<a href="admin.php?action=rd_duplicate_post_as_draft&amp;post=' . $post->ID . '" title="Duplicate this item" rel="permalink">Duplicate</a>';
    }
    return $actions;
}

add_filter( 'post_row_actions', 'rd_duplicate_post_link', 10, 2 );

function more_post_ajax(){

    $ppp = (isset($_POST["ppp"])) ? $_POST["ppp"] : 10;
    $author = (isset($_POST["author"])) ? $_POST["author"] : false;
    $page = (isset($_POST['pageNumber'])) ? $_POST['pageNumber'] : 0;

    //header("Content-Type: text/html");
    header('Content-type: application/json');

    $args = array(
//		'suppress_filters' => true,
        'post_type' => 'post',
        'meta_key'   => 'author',
        'meta_value' => $author,
        'post_status' => 'publish',
        'posts_per_page' => $ppp,
        'paged'    => $page,
        'orderby' => 'ID',
        'order' => 'DESC',
    );

    $loop = new WP_Query($args);

    $post_count = $loop->post_count;
    $out = '';

    if ($loop -> have_posts()) :  while ($loop -> have_posts()) : $loop -> the_post();
        ob_start();
        get_template_part( 'content-author-ajax', get_post_format() );
        $out .=  ob_get_contents();
        ob_end_clean();

    endwhile;
    endif;
    wp_reset_postdata();
    die(json_encode(array('content' => $out, 'more' => (($post_count < $ppp)? false:true))));
}
add_action('wp_ajax_nopriv_more_post_ajax', 'more_post_ajax');
add_action('wp_ajax_more_post_ajax', 'more_post_ajax');

function more_post_ajax_blog(){

    $ppp = (isset($_POST["ppp"])) ? $_POST["ppp"] : 15;
    $page = (isset($_POST['pageNumber'])) ? $_POST['pageNumber'] : 0;

    //header("Content-Type: text/html");
    header('Content-type: application/json');

    $args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'orderby' => 'post_date',
        'order' => 'DESC',
        'posts_per_page' => $ppp,
        'paged'    => $page,
    );

    $loop = new WP_Query($args);

    $post_count = $loop->post_count;
    $out = '';

    $cc_post = 0;
    if ($loop -> have_posts()) :  while ($loop -> have_posts()) : $loop -> the_post();
        ob_start();
        if($cc_post == 0 || ($cc_post % 5) == 0){
            get_template_part( 'content-blog-general-post-ajax', get_post_format() );
            ?>
            <div class="list-posts-wrapper">
            <?php
        }else{
            get_template_part( 'content-blog-sub-post-ajax', get_post_format() );
        }

        $cc_post += 1;
        if(($cc_post % 5) == 0 || $cc_post == $post_count){
            ?>
            </div>
            <?php
        }
        $out .=  ob_get_contents();
        ob_end_clean();

    endwhile;
    endif;
    wp_reset_postdata();
    die(json_encode(array('content' => $out, 'more' => (($post_count < $ppp)? false:true))));
}
add_action('wp_ajax_nopriv_more_post_ajax_blog', 'more_post_ajax_blog');
add_action('wp_ajax_more_post_ajax_blog', 'more_post_ajax_blog');

function custom_header_menu($menu_name='primary-menu') {
    if (($locations = get_nav_menu_locations()) && isset($locations[$menu_name])) {
        $menu = wp_get_nav_menu_object($locations[$menu_name]);
        $menu_items = wp_get_nav_menu_items($menu->term_id);

        $menu_list = '<div class="menu-header-menu-container">' ."\n";
        $menu_list .= "\t\t\t\t". '<ul id="menu-header-menu" class="list-inline">' ."\n";

        $count = 0;
        $submenu = false;
        $sub_submenu = false;
        $parent_id = $sub_parent_id = 999;

        foreach ((array) $menu_items as $key => $menu_item) {

            $link = $menu_item->url;
            $title = $menu_item->title;

            if ( !$menu_item->menu_item_parent ) {
                $parent_id = $menu_item->ID;
                $menu_list .= '<li class="menu-item menu-item-type-post_type menu-item-object-page '.(( isset($menu_items[$key+1]) && $parent_id == $menu_items[$key+1]->menu_item_parent )? 'menu-item-has-children':'').'" >' ."\n";
                $menu_list .= '<a href="'.$link.'" >'.$title.'</a>' ."\n";
                if ( $menu_items[ $count + 1 ]->menu_item_parent == $parent_id){
                    $menu_list .= '<div class="sub-menu col-2"><div class="col-holder">' . "\n";
                }
            }
            elseif ( $parent_id == $menu_item->menu_item_parent ) {
                $sub_parent_id = $menu_item->ID;
                $submenu = true;
                if($menu_items[ $count + 1 ]->menu_item_parent == $sub_parent_id) {
                    $menu_list .= '<div class="col"><div class="title-col"><a href="' . $link . '" class="title">' . $title . '</a></div>' . "\n";
                }else{
                    $menu_list .= '<ul>' . "\n";
                    $menu_list .= '<li class="item">' . "\n";
                    $menu_list .= '<a href="' . $link . '" class="title">' . $title . '</a>' . "\n";
                    $menu_list .= '</li>' . "\n";
                    $menu_list .= '</ul>' . "\n";
                }
            }
            elseif ( $sub_parent_id == $menu_item->menu_item_parent) {
                if (!$sub_submenu) {
                    $sub_submenu = true;
                    $menu_list .= '<ul>' . "\n";
                }
                $menu_list .= '<li class="item">' . "\n";
                $menu_list .= '<a href="' . $link . '" class="title">' . $title . '</a>' . "\n";
                $menu_list .= '</li>' . "\n";

                if ( $menu_items[ $count + 1 ]->menu_item_parent != $sub_parent_id && $sub_submenu) {
                    $menu_list .= '</ul>' . "\n";
                    $menu_list .= '</div>' . "\n";
                    $sub_submenu = false;
                }
            }

            if ($menu_items[ $count + 1 ]->menu_item_parent != $parent_id && $menu_items[ $count + 1 ]->menu_item_parent != $sub_parent_id  && !$sub_submenu) {
                if ( !$menu_items[ $count + 1 ]->menu_item_parent  && $submenu) {
                    $menu_list .= '</div></div>' . "\n";
                }
                $menu_list .= '</li>' ."\n";
                $submenu = false;
            }

            $count++;
        }
        $menu_list .= "\t\t\t\t". '</ul>' ."\n";
        $menu_list .= "\t\t\t". '</div>' ."\n";
    } else {
        // $menu_list = '<!-- no list defined -->';
    }
    echo $menu_list;
}

function custom_header_mobile_menu($menu_id=6) {
//    if (($locations = get_nav_menu_locations()) && isset($locations[$menu_name])) {
    $menu = wp_get_nav_menu_object($menu_id);
    $menu_items = wp_get_nav_menu_items($menu->term_id);

    $menu_list = '<div class="menu-main-menu-container container">' ."\n";
    $menu_list .= "\t\t\t\t". '<ul id="menu-main-menu" class="menu">' ."\n";

    $count = 0;
    $submenu = false;
    $sub_submenu = false;
    $parent_id = $sub_parent_id = 999;

    foreach ((array) $menu_items as $key => $menu_item) {
        $link = $menu_item->url;
        $title = $menu_item->title;

        if ( !$menu_item->menu_item_parent ) {
            $parent_id = $menu_item->ID;
            $menu_list .= '<li class="menu-item menu-item-type-post_type menu-item-object-page '.(( isset($menu_items[$key+1]) && $parent_id == $menu_items[$key+1]->menu_item_parent )? 'menu-item-has-children':'').'" >' ."\n";
            $menu_list .= '<a href="'.$link.'" >'.$title.'</a>' ."\n";
            if ( $menu_items[ $count + 1 ]->menu_item_parent == $parent_id){
                $menu_list .= '<div class="sub-menu col-2"><div class="col-holder">' . "\n";
            }
        }
        elseif ( $parent_id == $menu_item->menu_item_parent ) {
            $sub_parent_id = $menu_item->ID;
            $submenu = true;
            if($menu_items[ $count + 1 ]->menu_item_parent == $sub_parent_id) {
                $menu_list .= '<div class="col"><div class="title-col"><a href="' . $link . '" class="title">' . $title . '</a></div>' . "\n";
            }else{
                $menu_list .= '<ul>' . "\n";
                $menu_list .= '<li class="item">' . "\n";
                $menu_list .= '<a href="' . $link . '" class="title">' . $title . '</a>' . "\n";
                $menu_list .= '</li>' . "\n";
                $menu_list .= '</ul>' . "\n";
            }
        }
        elseif ( $sub_parent_id == $menu_item->menu_item_parent) {
            if (!$sub_submenu) {
                $sub_submenu = true;
                $menu_list .= '<ul>' . "\n";
            }
            $menu_list .= '<li class="item">' . "\n";
            $menu_list .= '<a href="' . $link . '" class="title">' . $title . '</a>' . "\n";
            $menu_list .= '</li>' . "\n";

            if ($menu_items[ $count + 1 ]->menu_item_parent != $sub_parent_id && $sub_submenu) {
                $menu_list .= '</ul>' . "\n";
                $menu_list .= '</div>' . "\n";
                $sub_submenu = false;
            }
        }

        if ( $menu_items[ $count + 1 ]->menu_item_parent != $parent_id && $menu_items[ $count + 1 ]->menu_item_parent != $sub_parent_id  && !$sub_submenu) {
            if ( !$menu_items[ $count + 1 ]->menu_item_parent  && $submenu) {
                $menu_list .= '</div></div>' . "\n";
            }
            $menu_list .= '</li>' ."\n";
            $submenu = false;
        }

        $count++;
    }
    $menu_list .= "\t\t\t\t". '</ul>' ."\n";
    $menu_list .= "\t\t\t". '</div>' ."\n";
//    } else {
//        // $menu_list = '<!-- no list defined -->';
//    }
    echo $menu_list;
}

//function custom_header_mobile_menu($menu_id=6) {
//    $menu = wp_get_nav_menu_object($menu_id);
//    $menu_items = wp_get_nav_menu_items($menu->term_id);
//
//    $menu_list = '<div class="menu-main-menu-container container">' ."\n";
//    $menu_list .= "\t\t\t\t". '<ul id="menu-main-menu" class="menu">' ."\n";
//
//    $count = 0;
//    $submenu = false;
//
//    $loans_list = array();
//
//    foreach ((array) $menu_items as $key => $menu_item) {
//        $link = $menu_item->url;
//        $title = $menu_item->title;
//
//        if ( !$menu_item->menu_item_parent ) {
//            $parent_id = $menu_item->ID;
//            $menu_list .= '<li class="menu-item menu-item-type-post_type menu-item-object-page '.(( isset($menu_items[$key+1]) && $parent_id == $menu_items[$key+1]->menu_item_parent )? 'menu-item-has-children':'').'" >' ."\n";
//            $menu_list .= '<a href="'.$link.'" >'.$title.'</a>' ."\n";
//        }
//        if ( $parent_id == $menu_item->menu_item_parent ) {
//            if($parent_id == 1091){
//                $loan_type = get_post_meta($menu_item->object_id, 'loan_type', true);
//                if(!empty($loan_type)) {
//                    if (!array_key_exists($loan_type, $loans_list)) {
//                        $loans_list[$loan_type] = array();
//                    }
//                    $loans_list[$loan_type][] = array('link' => $link, 'title' => $title);
//                }
//
//                if (!$submenu) {
//                    $submenu = true;
//                    $menu_list .= '<div class="sub-menu col-2"><div class="col-holder">' . "\n";
//                }
//
//                if ($menu_items[$count + 1]->menu_item_parent != $parent_id && $submenu) {
//                    krsort($loans_list);
//                    foreach($loans_list as $loan_type_name => $loan_type_items){
//                        $menu_list .= '<div class="col"><div class="title-col">' . $loan_type_name . '</div><ul>' . "\n";
//                        foreach($loan_type_items as $loan) {
//                            $menu_list .= '<li class="item">' . "\n";
//                            $menu_list .= '<a href="' . $loan['link'] . '" class="title">' . $loan['title'] . '</a>' . "\n";
//                            $menu_list .= '</li>' . "\n";
//                        }
//                        $menu_list .= '</ul></div>' . "\n";
//                    }
//                    $menu_list .= '</div></div>' . "\n";
//                    $submenu = false;
//                }
//            }else {
//                if (!$submenu) {
//                    $submenu = true;
//                    $menu_list .= '<ul class="sub-menu">' . "\n";
//                }
//                $menu_list .= '<li class="item">' . "\n";
//                $menu_list .= '<a href="' . $link . '" class="title">' . $title . '</a>' . "\n";
//                $menu_list .= '</li>' . "\n";
//
//
//                if ($menu_items[$count + 1]->menu_item_parent != $parent_id && $submenu) {
//                    $menu_list .= '</ul>' . "\n";
//                    $submenu = false;
//                }
//            }
//        }
//
//        if ( $menu_items[ $count + 1 ]->menu_item_parent != $parent_id ) {
//            $menu_list .= '</li>' ."\n";
//            $submenu = false;
//        }
//
//        $count++;
//    }
//    $menu_list .= "\t\t\t\t". '</ul>' ."\n";
//    $menu_list .= "\t\t\t". '</div>' ."\n";
//    echo $menu_list;
//}

function autocomplite_styles() {
    wp_register_style( 'jquery-ui-styles','http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css' );
    wp_enqueue_style( 'jquery-ui-styles' );
}

//add_action( 'wp_enqueue_scripts', 'autocomplite_styles' );

function my_search() {
    $term = strtolower( $_GET['term'] );
    $suggestions = array();

    $loop = new WP_Query( array(
        'post_type' => 'post',
        'post_status' => 'publish',
        's' => $term,
        'posts_per_page' => '5'
    ) );

    while( $loop->have_posts() ) {
        $loop->the_post();
        $suggestion = array();
        $suggestion['label'] = htmlspecialchars_decode(html_entity_decode(htmlspecialchars_decode(get_the_title())), ENT_QUOTES);
        $suggestion['link'] = get_permalink();

        $suggestions[] = $suggestion;
    }

    wp_reset_query();
    $response = json_encode( $suggestions );
    echo $response;
    exit();

}

add_action( 'wp_ajax_my_search', 'my_search' );
add_action( 'wp_ajax_nopriv_my_search', 'my_search' );

//Resources - subtitle under title position
add_action('edit_form_after_title', function() {
    global $post, $wp_meta_boxes;
    if( 'resources' == get_post_type() ) {
        ?>
        <script type="text/javascript">
            (function($) {

                $(document).ready(function(){

                    $('#titlediv').append( $('#acf-sub_title') );

                });

            })(jQuery);
        </script>
        <style type="text/css">
            #acf-sub_title .label label {
                color: #333333;
                font-size: 13px;
                line-height: 1.5em;
                font-weight: bold;
                padding: 0;
                margin: 0 0 3px;
                display: block;
                vertical-align: text-bottom;
            }
            #acf-sub_title #acf-field-sub_title {
                padding: 3px 8px;
                font-size: 1.7em;
                line-height: 100%;
                height: 1.7em;
                width: 100%;
                outline: 0;
                margin: 0 0 3px;
                background-color: #fff;
            }
        </style>
        <?php
    }
});

add_action('wp_enqueue_scripts', 'wpse26822_script_fix', 100);
function wpse26822_script_fix()
{
    wp_dequeue_script('parent_theme_script_handle');
}

function default_target_blank() {

    ?>
    <script>
        jQuery(document).on( 'wplink-open', function( wrap ) {
            if ( jQuery( 'input#wp-link-url' ).val() <= 0 )
                jQuery( 'input#wp-link-target' ).prop('checked', true );
        });
    </script>
    <?php
}
add_action( 'admin_footer-post-new.php', 'default_target_blank', 10, 0 );
add_action( 'admin_footer-post.php', 'default_target_blank', 10, 0 );

add_filter( 'manage_post_posts_columns', 'set_custom_edit_post_columns' );
add_action( 'manage_post_posts_custom_column' , 'custom_post_column', 10, 2 );

function set_custom_edit_post_columns($columns) {
    unset($columns['author']);
    $columns['author_lg'] = __( 'Author', 'lendgenius' );
    return $columns;
}

function custom_post_column( $column, $post_id ) {
    switch ( $column ) {
        case 'author_lg' :
            echo get_the_title(get_post_meta( $post_id , 'author' , true ));
            break;

    }
}

function sync_authors(){
    $exists_users = get_users();
    $exists_user_names = array();
    $exists_authors = array();
    $authors = array();
    foreach($exists_users as $exists_user){
        $exists_user_names[$exists_user->display_name] = $exists_user;
    }

    $loop_ae = new WP_Query(array(
        'post_type' => 'authors',
        'posts_per_page' => -1
    ));
    while ($loop_ae -> have_posts()) : $loop_ae -> the_post();
        $exists_authors []= get_the_title();
    endwhile;
    wp_reset_postdata();

    foreach($exists_user_names as $key=>$exists_user_name){
        if(!in_array($key, $exists_authors)){
            $author_post = array(
                'post_title'    => wp_strip_all_tags( $key ),
                'post_content'  => get_the_author_meta('description', $exists_user_name->ID),
                'post_status'   => 'publish',
                'post_author'   => get_current_user_id(),
                'post_type' => 'authors'
            );

            // Insert the post into the database
            if(wp_insert_post( $author_post )){
                $exists_authors []= $key;
            }
        }
    }

    $loop_a = new WP_Query(array(
        'post_type' => 'authors',
        'posts_per_page' => -1
    ));
    while ($loop_a -> have_posts()) : $loop_a -> the_post();
        $authors[get_the_title()] = get_the_ID();
    endwhile;
    wp_reset_postdata();

    $loop = new WP_Query(array(
        'post_type' => 'post',
        'posts_per_page' => -1
    ));

    while ($loop -> have_posts()) : $loop -> the_post();
        $id = get_the_ID();
        $post_author_name = get_the_author();
        if(array_key_exists($post_author_name, $authors)) {
            $author = $authors[$post_author_name];
            if (get_post_meta($id, 'author', true)) {
                update_post_meta($id, 'author', $author);
            } else {
                add_post_meta($id, 'author', $author);
            }
        }
    endwhile;
    wp_reset_postdata();
}

add_filter( 'gettext', 'lg_post_author_field', 10, 2 );
function lg_post_author_field( $translation, $original )
{
    global $typenow;
    if ( isset($_GET['action'])  && $_GET['action'] === 'edit' && 'Author' == $original && $typenow == 'post') {
        return 'User';
    }
    return $translation;
}

// this function must be commented !!!!
// flush_rewrite_rules();
// this function must be commented !!!!
//endregion


//	MCE

function mce_blank_target() {
    ?>
    <script>
        jQuery(document).on('click','.mce-btn button',function(){

            jQuery('#content_ifr').contents().find('a')
                .each(function(){

                    var $this = jQuery(this);
                    $this.attr('data-mce-href') && !$this.attr('target') && $this.attr('target','_blank');

                })
        });
    </script>
    <?php
}
add_filter( 'admin_footer', 'mce_blank_target' );

//add_action( 'template_redirect', 'wpse_999865444_redirect_post' );
//
//function wpse_999865444_redirect_post() {
//    $queried_post_type = get_query_var('post_type');
//    if ( is_single() && 'feedbacks' ==  $queried_post_type ) {
//        wp_redirect( home_url(), 301 );
//        exit;
//    }
//}

add_action( 'admin_menu' , 'lg_new_admin_menu_items' );
function lg_new_admin_menu_items() {
    $query = new WP_Query(array(
        'post_type' => 'loans',
        'post_status' => 'publish',
        'orderby' => 'title',
        'order' => 'ASC',
        'posts_per_page' => -1
    ));


    while ($query->have_posts()) {
        $query->the_post();
        $post_id = get_the_ID();
        add_submenu_page('edit.php?post_type=loans', __(get_the_title(),'lendgenius'), __(get_the_title(),'lendgenius'), 'manage_options', 'post.php?post='.$post_id.'&action=edit');
    }

    wp_reset_query();
}

// add a link to the WP Toolbar
function custom_toolbar_link($wp_admin_bar) {
    if(is_archive('business-loans')) {
        $args = array(
            'id' => 'edit',
            'title' => 'Edit Page',
            'href' => '/wp-admin/post.php?post=112&action=edit'
        );
        $wp_admin_bar->add_node($args);
    }
}
add_action('admin_bar_menu', 'custom_toolbar_link', 999);


// Fix azure srcset in thumbnails
function azure_fix_invalid_srcset_path( $sources, $size_array, $image_src, $image_meta, $attachment_id ) {
    $media_info = get_post_meta( $attachment_id, 'windows_azure_storage_info', true );

    if ( !empty( $media_info ) ) {
        foreach ( $sources as &$source ) {
            $img_file_name = substr( $source['url'], strrpos( $source['url'], '/' ) + 1 );

            if ( $img_file_name == substr( $media_info['blob'], strrpos( $media_info['blob'], '/' ) + 1 ) ) {
                $source['url'] = "https://" . get_option( 'azure_storage_account_name' ) . ".blob.core.windows.net/" . $media_info['container'] . "/" . $media_info['blob'];
            } else {
                foreach ( $media_info['thumbnails'] as $thumbnail ) {
                    if ( $img_file_name == substr( $thumbnail, strrpos( $thumbnail, '/' ) + 1 ) ) {
                        $source['url'] = "https://" . get_option( 'azure_storage_account_name' ) . ".blob.core.windows.net/" . $media_info['container'] . "/" . $thumbnail;
                        break;
                    }
                }
            }
        }
    }

    return $sources;
}

add_filter( 'wp_calculate_image_srcset', 'azure_fix_invalid_srcset_path', 10, 5 );

add_filter( 'wp_get_attachment_image_attributes', 'bea_remove_srcset', PHP_INT_MAX, 1 );
function bea_remove_srcset( $attr ) {
    if ( class_exists( 'BEA_Images' ) ) {
        return $attr;
    }
    if ( isset( $attr['sizes'] ) ) {
        unset( $attr['sizes'] );
    }
    if ( isset( $attr['srcset'] ) ) {
        unset( $attr['srcset'] );
    }
    return $attr;
}
// Override the calculated image sizes
add_filter( 'wp_calculate_image_sizes', '__return_false', PHP_INT_MAX );
// Override the calculated image sources
add_filter( 'wp_calculate_image_srcset', '__return_false', PHP_INT_MAX );
// Remove the reponsive stuff from the content
remove_filter( 'the_content', 'wp_make_content_images_responsive' );
// END Fix azure srcset in thumbnails

// Add query to attachments for browser caching
function add_version_query_to_attachment($attr)
{
    if ( strpos($attr['src'], '?v=3') === false ) {
        $attr['src'] .= '?v=3';
    }

    return $attr;

}
add_filter('wp_get_attachment_image_attributes','add_version_query_to_attachment');
function add_wp_get_attachment_image_src($image)
{

    if ( strpos($image[0], '?v=3') === false ) {
        $image[0] .= '?v=3';
    }
    return $image;

}
add_filter('wp_get_attachment_image_src','add_wp_get_attachment_image_src');
add_filter( 'wpcf7_load_js', '__return_false' );
add_filter( 'wpcf7_load_css', '__return_false' );

// TablePress styling
function add_css_class_to_tables( $css_classes )
{
    if ( in_array('simple',$css_classes) )
    {
        $css_classes = array('responsive');
    }
    else if ( in_array('complex',$css_classes) )
    {
        $css_classes = array('');
    }

    return $css_classes;
}

add_filter('tablepress_table_css_classes','add_css_class_to_tables',4);

function add_css_class_to_cells_content( $cell_content )
{
    if ( strpos($cell_content,'<img') !== false) {
        $cell_content = '<div class="table-funding-options">'. $cell_content .'</div>';
    }

    if ( strpos($cell_content,'<ul>') !== false) {
        $cell_content = str_replace('<ul>','<ul class="list-success-circle">',$cell_content);
    }

    return $cell_content;

}

add_filter('tablepress_cell_content','add_css_class_to_cells_content',4);

function add_tablepress_datatables_parameters( $parametres, $table_id, $html_id, $js_options )
{
//	Tables simple, complex table-responsive table-sm

    $tables                = get_option('tablepress_tables');
    $table_posts           = json_decode($tables);
    $current_table_post_id = $table_posts->table_post->{$table_id};
    $postmetastring        = get_post_meta($current_table_post_id,'_tablepress_table_options',true);
    $postmetaobject        = json_decode($postmetastring);
    $classname             = $postmetaobject->{'extra_css_classes'};

    $classname .= ($classname == 'complex') ? ' table-responsive ' : '';
    $parametres["dom"] = "dom: '<\"table-". $classname ."\"t>'";
    return $parametres;
}

add_filter('tablepress_datatables_parameters','add_tablepress_datatables_parameters',4,4);

function add_tablepress_shortcode_table_shortcode_atts( $arg )
{
    $arg['convert_line_breaks'] = false;
    return $arg;
}

add_filter('tablepress_shortcode_table_shortcode_atts','add_tablepress_shortcode_table_shortcode_atts',4);

function do_add_custom_styles($head_html)
{
    $head_html .= '<link rel="stylesheet" href="/wp-content/themes/lendgenius/assets/styles/assets-styles-6de112f4acb96549ed80cbbe7e4791c3.css">';
    return $head_html;
}
add_filter('add_custom_styles','do_add_custom_styles');

//function clear_dbq_cache( $post_id )
//{
//	DBQCache::clear();
//}
//
//add_action('save_post','clear_dbq_cache');
//add_filter('jpeg_quality', function($arg){return 100;});