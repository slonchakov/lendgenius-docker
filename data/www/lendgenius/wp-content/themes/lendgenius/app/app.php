<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 07.09.17
 *
 */
//$start = microtime(true);

/**
 * require composer autoload
 */
require_once ABSPATH. 'vendor/autoload.php';

require_once 'include.php';

/**
 * Using for including files
 * We are able to add files
 * require with method RequireService::addPath('path/to/file/relative/to/theme_root') without ".php"
 * but all paths we must add before executing of method require()
 */
App\Classes\RequireOnce\RequireService::get()->require();

/**
 * Register sidebars
 */
$sidebars = new \App\Classes\Sidebar\Sidebar();
$sidebars->register();

/**
 * Registering actions and filters
 */
\App\Classes\Register\Register::all();

/*  $end = microtime(true);
  echo $end - $start;*/