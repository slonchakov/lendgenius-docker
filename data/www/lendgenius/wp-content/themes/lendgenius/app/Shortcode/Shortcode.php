<?php

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 08.09.17
 *
 */
class Shortcode
{
    
    private $shortcodes = [
       
    ];
    
    public function add()
    {
        foreach ($this->shortcodes as $name => $shortcode) {
            add_shortcode($name,array($this,$shortcode));
        }
        
    }

}