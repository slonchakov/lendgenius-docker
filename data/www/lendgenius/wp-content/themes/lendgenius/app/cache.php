<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 22.01.18
 *
 */
if (class_exists('Memcached')) {
    /**
     * Use memcached if exists
     */
    \App\Classes\Storage::set(new \App\Classes\Cache\MemcachedCache());

} elseif (function_exists('wincache_ucache_add')) {
    /**
     * Try add wincache for azure
     */
    \App\Classes\Storage::set(new \App\Classes\Cache\WinCache());

} else {
    /**
     * Empty Cache class for avoiding errors im memcached is not exists
     */
    \App\Classes\Storage::set(new \App\Classes\Cache\NoCache());
}