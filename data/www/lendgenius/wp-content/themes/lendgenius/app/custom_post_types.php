<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 22.01.18
 *
 */
/**
 * Register all needed Custom post types from app/custom_post_types
 * Naming rules: loans === CPTLoans => prefix CPT first letter in uppercase
 */
define('CUSTOM_POST_TYPES', [
    'loans',
    'author',
//    'faqs',
//    'feedbacks',
//    'lboxes',
    'resources',
    'LoanApplication',
    'FundingOptions',
    'lenders',
    'LenderListFilter'
]);