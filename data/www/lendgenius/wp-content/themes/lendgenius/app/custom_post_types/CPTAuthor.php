<?php

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 15.09.17
 *
 */
namespace App\CustomPostTypes;

use App\Classes\CPT\CPTRegister;

class CPTAuthor extends CPTRegister
{
    public function __construct()
    {
        $this->name = 'authors';
        $this->labels = array(
            'name'                => _x( 'Authors', 'Post Type General Name', 'lendgenius' ),
            'singular_name'       => _x( 'Author', 'Post Type Singular Name', 'lendgenius' ),
            'menu_name'           => __( 'Authors', 'lendgenius' ),
            'parent_item_colon'   => __( 'Parent Author', 'lendgenius' ),
            'all_items'           => __( 'All Authors', 'lendgenius' ),
            'view_item'           => __( 'View Author', 'lendgenius' ),
            'add_new_item'        => __( 'Add New Author', 'lendgenius' ),
            'add_new'             => __( 'Add New', 'lendgenius' ),
            'edit_item'           => __( 'Edit Author', 'lendgenius' ),
            'update_item'         => __( 'Update Author', 'lendgenius' ),
            'search_items'        => __( 'Search Author', 'lendgenius' ),
            'not_found'           => __( 'Not Found', 'lendgenius' ),
            'not_found_in_trash'  => __( 'Not found in Trash', 'lendgenius' ),
        );

        $this->arguments = array(
            'label'               => __( 'author', 'lendgenius' ),
            'description'         => __( 'Author news and reviews', 'lendgenius' ),
            'labels'              => $this->labels,
            // Features this CPT(loans) supports in Post Editor
            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'menu_icon'           => 'dashicons-admin-users',
            'can_export'          => true,
            'has_archive'         => false,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'capability_type'     => 'page',
            'rewrite' => array('slug' => 'blog/author', 'with_front' => false),
        );

    }


}