<?php

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 15.09.17
 *
 */
namespace App\CustomPostTypes;

use App\Classes\CPT\CPTRegister;

class CPTFeedbacks extends CPTRegister
{
    public function __construct()
    {
        $this->name = 'feedbacks';
        $this->labels = array(
            'name'                => _x( 'Feedbacks', 'Post Type General Name', 'lendgenius' ),
            'singular_name'       => _x( 'Feedback', 'Post Type Singular Name', 'lendgenius' ),
            'menu_name'           => __( 'Feedbacks', 'lendgenius' ),
            'parent_item_colon'   => __( 'Parent Feedback', 'lendgenius' ),
            'all_items'           => __( 'All Feedbacks', 'lendgenius' ),
            'view_item'           => __( 'View Feedback', 'lendgenius' ),
            'add_new_item'        => __( 'Add New Feedback', 'lendgenius' ),
            'add_new'             => __( 'Add New', 'lendgenius' ),
            'edit_item'           => __( 'Edit Feedback', 'lendgenius' ),
            'update_item'         => __( 'Update Feedback', 'lendgenius' ),
            'search_items'        => __( 'Search Feedback', 'lendgenius' ),
            'not_found'           => __( 'Not Found', 'lendgenius' ),
            'not_found_in_trash'  => __( 'Not found in Trash', 'lendgenius' ),
        );

// Set other options for Custom Post Type

        $this->arguments = array(
            'label'               => __( 'feedbacks', 'lendgenius' ),
            'description'         => __( 'Feedback about LG', 'lendgenius' ),
            'labels'              => $this->labels,
            // Features this CPT(resources) supports in Post Editor
            'supports'            => array( 'title', 'editor', 'author', 'thumbnail',  'comments', 'revisions', ),
            // You can associate this CPT(resources) with a taxonomy or custom taxonomy.
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 6,
            'menu_icon'           => 'dashicons-admin-page',
            'can_export'          => true,
            'has_archive'         => false,
            'exclude_from_search' => false,
            'publicly_queryable'  => false,
            'capability_type'     => 'page'
        );
    }

}