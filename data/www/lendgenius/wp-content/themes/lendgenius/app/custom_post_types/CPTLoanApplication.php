<?php

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 15.09.17
 *
 */
namespace App\CustomPostTypes;

use App\Classes\CPT\CPTRegister;

class CPTLoanApplication extends CPTRegister
{
    public function __construct()
    {
        $this->name = 'loan-applications';
        $this->labels = array(
            'name'                => _x( 'Notifications', 'Post Type General Name', 'lendgenius' ),
            'singular_name'       => _x( 'Notification', 'Post Type Singular Name', 'lendgenius' ),
            'menu_name'           => __( 'Notifications', 'lendgenius' ),
            'parent_item_colon'   => __( 'Parent Notifications', 'lendgenius' ),
            'all_items'           => __( 'All Notifications', 'lendgenius' ),
            'view_item'           => __( 'View Notifications', 'lendgenius' ),
            'add_new_item'        => __( 'Add New Notifications', 'lendgenius' ),
            'add_new'             => __( 'Add New', 'lendgenius' ),
            'edit_item'           => __( 'Edit Notifications', 'lendgenius' ),
            'update_item'         => __( 'Update Notifications', 'lendgenius' ),
            'search_items'        => __( 'Search Notifications', 'lendgenius' ),
            'not_found'           => __( 'Not Found', 'lendgenius' ),
            'not_found_in_trash'  => __( 'Not found in Trash', 'lendgenius' ),
        );

// Set other options for Custom Post Type

        $this->arguments = array(
            'label'               => 'Notifications',
            'description'         => 'Notifications',
            'labels'              => $this->labels,
            'supports'            => array(  'title',),
            'taxonomies'          => array( 'types' ),
            'hierarchical'        => true,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 6,
            'can_export'          => true,
            'has_archive'         => false,
            'exclude_from_search' => true,
            'publicly_queryable'  => true,
            'capability_type'     => 'page',
            'rewrite' => array('slug' => 'notification', 'with_front' => false),
        );
    }

}