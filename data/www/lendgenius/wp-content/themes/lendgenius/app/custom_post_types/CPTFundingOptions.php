<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 07.11.17
 *
 */

namespace App\CustomPostTypes;


use App\Classes\CPT\CPTRegister;

class CPTFundingOptions extends CPTRegister
{
    public function __construct()
    {
        $this->name = "funding-options";
        $this->labels = array(
            'name'                => _x( 'Funding Options', 'Post Type General Name', 'lendgenius' ),
            'singular_name'       => _x( 'Funding Option', 'Post Type Singular Name', 'lendgenius' ),
            'menu_name'           => __( 'Funding Options', 'lendgenius' ),
            'parent_item_colon'   => __( 'Parent', 'lendgenius' ),
            'all_items'           => __( 'All Funding Options', 'lendgenius' ),
            'view_item'           => __( 'View Funding Option', 'lendgenius' ),
            'add_new_item'        => __( 'Add New Funding Option', 'lendgenius' ),
            'add_new'             => __( 'Add New', 'lendgenius' ),
            'edit_item'           => __( 'Edit Funding Option', 'lendgenius' ),
            'update_item'         => __( 'Update Funding Option', 'lendgenius' ),
            'search_items'        => __( 'Search Funding Option', 'lendgenius' ),
            'not_found'           => __( 'Not Found', 'lendgenius' ),
            'not_found_in_trash'  => __( 'Not found in Trash', 'lendgenius' ),
        );

        $this->arguments = array(
            'label'               => __( 'Funding Options', 'lendgenius' ),
            'description'         => __( 'Funding Options', 'lendgenius' ),
            'labels'              => $this->labels,
            // Features this CPT(loans) supports in Post Editor
            'supports'            => array( 'title', 'thumbnail', 'editor' ),
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'menu_icon'           => 'dashicons-admin-users',
            'can_export'          => true,
            'has_archive'         => false,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'capability_type'     => 'page',
            'rewrite' => array('slug' => 'funding-options', 'with_front' => false),
        );
    }

}