<?php

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 15.09.17
 *
 */
namespace App\CustomPostTypes;

use App\Classes\CPT\CPTRegister;

class CPTLoans extends CPTRegister
{
    public function __construct()
    {
        $this->name = 'loans';
        $this->labels = array(
            'name'                => _x( 'Loans', 'Post Type General Name', 'lendgenius' ),
            'singular_name'       => _x( 'Loan', 'Post Type Singular Name', 'lendgenius' ),
            'menu_name'           => __( 'Business Loans', 'lendgenius' ),
            'parent_item_colon'   => __( 'Parent Loan', 'lendgenius' ),
            'all_items'           => __( 'All Loans', 'lendgenius' ),
            'view_item'           => __( 'View Loan', 'lendgenius' ),
            'add_new_item'        => __( 'Add New Loan', 'lendgenius' ),
            'add_new'             => __( 'Add New', 'lendgenius' ),
            'edit_item'           => __( 'Edit Loan', 'lendgenius' ),
            'update_item'         => __( 'Update Loan', 'lendgenius' ),
            'search_items'        => __( 'Search Loan', 'lendgenius' ),
            'not_found'           => __( 'Not Found', 'lendgenius' ),
            'not_found_in_trash'  => __( 'Not found in Trash', 'lendgenius' ),
        );

// Set other options for Custom Post Type

        $this->arguments = array(
            'label'               => __( 'loans', 'lendgenius' ),
            'description'         => __( 'Loan news and reviews', 'lendgenius' ),
            'labels'              => $this->labels,
            // Features this CPT(loans) supports in Post Editor
            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
            // You can associate this CPT(loans) with a taxonomy or custom taxonomy.
            'taxonomies'          => array( 'genres' ),
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'menu_icon'           => 'dashicons-cart',
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'capability_type'     => 'page',
            'rewrite' => array('slug' => 'business-loans', 'with_front' => false),
        );
    }


}