<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 22.11.17
 *
 */

namespace App\CustomPostTypes;


use App\Classes\CPT\CPTRegister;

class CPTLenders extends CPTRegister
{
    public function __construct()
    {
        $this->name = 'lenders';
        $this->labels = array(
            'name'                => _x( 'Lenders List', 'Post Type General Name' ),
            'singular_name'       => _x( 'Lender', 'Post Type Singular Name'),
            'menu_name'           => __( 'Lenders List' ),
            'parent_item_colon'   => __( 'Parent Lender' ),
            'all_items'           => __( 'All Lenders' ),
            'view_item'           => __( 'View Lender'),
            'add_new_item'        => __( 'Add New Lender' ),
            'add_new'             => __( 'Add New'  ),
            'edit_item'           => __( 'Edit Lender'),
            'update_item'         => __( 'Update Lender'),
            'search_items'        => __( 'Search Lender' ),
            'not_found'           => __( 'Not Found'),
            'not_found_in_trash'  => __( 'Not found in Trash' ),
        );

// Set other options for Custom Post Type

        $this->arguments = array(
            'label'               => __( 'lenderlist'),
            'description'         => __( 'Lenders List' ),
            'labels'              => $this->labels,
            // Features this CPT(loans) supports in Post Editor
            'supports'            => array( 'title', 'thumbnail' ),
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 3,
            'menu_icon'           => 'dashicons-cart',
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'capability_type'     => 'page',
            'rewrite' => array('with_front' => false),
        );
    }

}