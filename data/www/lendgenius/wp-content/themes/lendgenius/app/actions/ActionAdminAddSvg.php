<?php

namespace App\Actions;

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 13.02.18
 *
 */
use App\Classes\Action\ActionRegister;

class ActionAdminAddSvg extends ActionRegister
{
    public function __construct()
    {
        $this->action = [
            'upload_mimes' => 'index',
        ];
    }

    public function index()
    {
        $args = func_get_args();
        $file_types = $args[0];
        $new_filetypes = array();
        $new_filetypes['svg'] = 'image/svg+xml';
        $file_types = array_merge($file_types, $new_filetypes );
        return $file_types;   
    }
}