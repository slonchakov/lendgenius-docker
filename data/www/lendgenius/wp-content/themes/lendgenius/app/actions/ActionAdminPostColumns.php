<?php

namespace App\Actions;

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 14.09.17
 *
 */
use App\Classes\Action\ActionRegister;
class ActionAdminPostColumns extends ActionRegister
{
    public function __construct()
    {
        $this->action = [
            'manage_post_posts_columns' => 'index',
            'manage_post_posts_custom_column' => 'custom_post_column',
            'manage_resources_posts_custom_column' => 'custom_resource_column',
//            'manage_lenders_posts_columns' => 'custom_lender_columns',
//            'manage_lenders_posts_custom_column' => 'custom_lender_column'
        ];
        $this->only_for = 'admin';
        $this->arguments = 2;
    }

    public function index()
    {
        $arguments = func_get_args();
        $columns = $arguments[0];
        unset($columns['author']);
        $columns['author_lg'] = __('Author', 'lendgenius');

        return $columns;
    }

    public function custom_post_column()
    {
        $arguments = func_get_args();
        $column = $arguments[0];
        $post_id = $arguments[1];

        switch ($column) {
            case 'author_lg' :
                echo get_the_title(get_post_meta($post_id, 'author', true));
                break;

        }
    }

    public function custom_resource_column()
    {

        $args = func_get_args();

        $column = $args[0];
        $post_id = $args[1];

        switch ($column) {
            case 'resource_type' :
                echo get_post_meta($post_id, 'resource_type', true);
                break;

        }
    }

    public function custom_lender_column()
    {
        $args = func_get_args();

        $column = $args[0];
        $post_id = $args[1];

        switch ($column) {
            case 'sort_order' :
                echo '<input name="sort_order_'. $post_id.'" value="'. get_post_meta($post_id, 'sort_order', true) .'" maxlength="3" size="3" />';
                break;
        }

        $columns[] = '<input type="text" name="name" maxlength="3" size="3">';

    }

    public function custom_lender_columns()
    {
        $arguments = func_get_args();
        $columns = $arguments[0];
        $columns['sort_order'] = "Sort Order";

        return $columns;

    }
}