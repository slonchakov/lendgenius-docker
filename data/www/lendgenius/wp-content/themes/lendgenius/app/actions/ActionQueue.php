<?php

namespace App\Actions;

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 14.09.17
 *
 */
use App\Classes\Action\ActionRegister;

class ActionQueue extends ActionRegister
{
    public function __construct()
    {
        $this->action = [
            'wp_enqueue_scripts' => 'index',
            'wp_print_styles'    => 'removeTablepressCss'
        ];
    }

    public function index()
    {
        wp_dequeue_script('parent_theme_script_handle');

        wp_scripts()->add_data( 'jquery', 'group', 1 );
        wp_scripts()->add_data( 'jquery-core', 'group', 1 );
        wp_scripts()->add_data( 'jquery-migrate', 'group', 1 );

        if (!is_admin()) {

//            wp_deregister_script('jquery');
//            wp_enqueue_script('jquery');

            if (!is_single() ) return;
            wp_deregister_script('datatables');
            $js_file = 'js/jquery.datatables.min.js';
            $js_url = plugins_url( $js_file, TABLEPRESS__FILE__ );
            $js_url = apply_filters( 'tablepress_datatables_js_url', $js_url, $js_file );
            wp_enqueue_script( 'tablepress-datatables', $js_url, array( 'jquery' ), \TablePress::version, true );
        }
    }

    public function removeTablepressCss()
    {
        wp_dequeue_style('tablepress-default');
    }


}