<?php

namespace App\Actions;

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 14.09.17
 *
 */
use App\Classes\Action\ActionRegister;

class ActionCustomResourceColumn extends ActionRegister
{
    public function __construct()
    {
        $this->action = 'manage_resources_posts_custom_column';
        $this->arguments = 2;
    }

    public function index()
    {

        $args = func_get_args();

        $column  = $args[0];
        $post_id = $args[1];

        switch ( $column ) {
            case 'resource_type' :
                echo get_post_meta( $post_id , 'resource_type' , true );
                break;

        }

    }


}