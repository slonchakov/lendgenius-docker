<?php

namespace App\Actions;

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 14.09.17
 *
 */
use App\Classes\Action\ActionRegister;

class ActionQuickSearch extends ActionRegister
{

    public function __construct()
    {
        $this->action = [
            'wp_ajax_my_search' => 'index',
            'wp_ajax_nopriv_my_search' => 'index'
        ];
    }

    public function index()
    {
        $term = strtolower( $_GET['term'] );
        $suggestions = array();

        $loop = new \WP_Query( array(
            'post_type' => 'post',
            'post_status' => 'publish',
            's' => $term,
            'posts_per_page' => '5'
        ) );

        while( $loop->have_posts() ) {
            $loop->the_post();

            $suggestions[] = [
                'label' => htmlspecialchars_decode(html_entity_decode(htmlspecialchars_decode(get_the_title())), ENT_QUOTES),
                'link'  => get_permalink()
            ];
        }

        wp_reset_query();
        $response = json_encode( $suggestions );
        echo $response;
        exit();
    }

}