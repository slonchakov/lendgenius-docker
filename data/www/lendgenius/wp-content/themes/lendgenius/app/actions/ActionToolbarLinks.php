<?php

namespace App\Actions;

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 14.09.17
 *
 */
use App\Classes\Action\ActionRegister;

class ActionToolbarLinks extends ActionRegister
{
    public function __construct()
    {
        $this->action = 'admin_bar_menu';
        $this->priority = 999;
    }

    public function index()
    {

        $arguments    = func_get_args();
        $wp_admin_bar = $arguments[0];

        if(is_post_type_archive('loans')) {
            $args = array(
                'id' => 'edit',
                'title' => 'Edit Page',
                'href' => '/wp-admin/post.php?post=112&action=edit'
            );
            $wp_admin_bar->add_node($args);
        }
    }


}