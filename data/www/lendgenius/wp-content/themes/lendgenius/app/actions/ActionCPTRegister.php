<?php

namespace App\Actions;

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 14.09.17
 *
 */
use App\Classes\Action\ActionRegister;

class ActionCPTRegister extends ActionRegister
{
    public function __construct()
    {
        $this->action = 'init';
    }

    public function index()
    {
        foreach (CUSTOM_POST_TYPES as $cpt) {
            $classname = '\\App\\CustomPostTypes\\CPT'. ucfirst($cpt);

            $CPTclass = new $classname();

            $CPTclass->index();
        }
    }
}