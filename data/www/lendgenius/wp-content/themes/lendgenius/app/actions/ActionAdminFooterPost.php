<?php
namespace App\Actions;

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 14.09.17
 *
 */
use App\Classes\Action\ActionRegister;
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 14.09.17
 *
 */
class ActionAdminFooterPost extends ActionRegister
{
    public function __construct()
    {
        $this->action = [
            'admin_footer-post-new.php' => 'index',
            'admin_footer-post.php' => 'index'
        ];

        $this->only_for = 'admin';
    }

    public function index()
    {
        ?>
        <style>
            body input,
            body select{
                font-family: "Work Sans", sans-serif !important;
            }
        </style>
        <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,700&amp;subset=latin-ext" rel="stylesheet">
        <script>
            jQuery(document).on( 'wplink-open', function( wrap ) {
                if ( jQuery( 'input#wp-link-url' ).val() <= 0 )
                    jQuery( 'input#wp-link-target' ).prop('checked', true );
            });
        </script>
        <?php
    }

}