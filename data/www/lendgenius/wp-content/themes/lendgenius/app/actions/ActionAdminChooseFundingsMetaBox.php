<?php

namespace App\Actions;

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 13.02.18
 *
 */
use App\Classes\Action\ActionRegister;

class ActionAdminChooseFundingsMetaBox extends ActionRegister
{
    public function __construct()
    {
        $this->action = [
            'add_meta_boxes' => 'index',
            'save_post'      => 'save',    
        ];
    }

    /**
     * @return mixed
     * method witch executes by default on action adding
     */
    public function index()
    {
        $screens = array( 'post', 'loans' );
        add_meta_box( 'choose_fundinf_options_meta', 'Select Funding Options', array($this,'writeBlock'), $screens );
    }

    public function writeBlock($post, $meta)
    {
        $selectedOptions = get_post_meta($post->ID, 'funding_options_post');

        add_filter('posts_fields', function($fields){
            return 'wp_posts.ID, wp_posts.post_title';
        });

        $allFundingOptions = new \WP_Query([
            'posts_per_page' => -1,
            'post_type'   => 'funding-options',
            'fields'      =>'id=>parent'
        ]);

        $output = '<ul>';

        foreach ($allFundingOptions->posts as $fundingPost) {

            $selected = is_array($selectedOptions[0]) && in_array($fundingPost->ID, $selectedOptions[0]) ? 'checked="checked"' : '';

            $output .= '<li><input '. $selected .' id="funding_options_post_'. $fundingPost->ID .'" type="checkbox" name="funding_options_post[]" value="'. $fundingPost->ID .'" />';
            $output .= '<label for="funding_options_post_'. $fundingPost->ID .'">'. $fundingPost->post_title .'</label></li>';
        }

        $output .= '</ul>';

        echo $output;
    }

    public function save($post_id)
    {


        if ( ! isset( $_POST['funding_options_post'] ) )
        {
            update_post_meta( $post_id, 'funding_options_post', [] );
            return;
        }

        if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
            return;

        if( ! current_user_can( 'edit_post', $post_id ) )
            return;

        update_post_meta( $post_id, 'funding_options_post', $_POST['funding_options_post'] );
        
    }
}