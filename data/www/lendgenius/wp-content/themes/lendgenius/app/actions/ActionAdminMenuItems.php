<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 14.09.17
 *
 */
namespace App\Actions;


use App\Classes\Action\ActionRegister;
/**
 * Class ActionAdminMenuItems
 * Add new menu items to admin menu
 */
class ActionAdminMenuItems extends ActionRegister
{
    public function __construct()
    {
        $this->action = 'admin_menu';
        $this->only_for = 'admin';
    }

    public function index()
    {
        $query = new \WP_Query(array(
            'post_type'      => 'loans',
            'post_status'    => 'publish',
            'orderby'        => 'title',
            'order'          => 'ASC',
            'posts_per_page' => -1
        ));

        while ($query->have_posts()) {
            $query->the_post();
            $post_id = get_the_ID();
            add_submenu_page('edit.php?post_type=loans', __(get_the_title(),'lendgenius'), __(get_the_title(),'lendgenius'), 'manage_options', 'post.php?post='.$post_id.'&action=edit');
        }
        wp_reset_query();

        $query = new \WP_Query(array(
            'post_type'      => 'lenders',
            'post_status'    => 'publish',
            'orderby'        => 'title',
            'order'          => 'ASC',
            'posts_per_page' => -1
        ));

        while ($query->have_posts()) {
            $query->the_post();
            $post_id = get_the_ID();
            add_submenu_page('edit.php?post_type=lenders', __(get_the_title(),'lendgenius'), __(get_the_title(),'lendgenius'), 'manage_options', 'post.php?post='.$post_id.'&action=edit');
        }
        wp_reset_query();
    }
}