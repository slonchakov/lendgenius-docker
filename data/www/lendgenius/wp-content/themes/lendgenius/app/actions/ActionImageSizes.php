<?php

namespace App\Actions;

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 14.09.17
 *
 */
use App\Classes\Action\ActionRegister;
use App\Classes\ImageSizes;

class ActionImageSizes extends ActionRegister
{
    private $image_sizes = [];

    public function __construct()
    {
        $this->action = 'after_setup_theme';
    }

    public function index()
    {

        $images = new ImageSizes();

        $this->image_sizes = $images->getImageSizes();

        add_theme_support( 'post-thumbnails' );
        foreach ($this->image_sizes as $name => $atts) {
            add_image_size( $name, $atts[0], $atts[1], $atts[2] );

        }

    }
}