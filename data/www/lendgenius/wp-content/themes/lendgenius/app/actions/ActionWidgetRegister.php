<?php

namespace App\Actions;

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 14.09.17
 *
 */
use App\Classes\Action\ActionRegister;

class ActionWidgetRegister extends ActionRegister
{
    private $widgets = [
        'widget_featured_posts',
        'widget_category_latest_posts',
        'widget_related_posts',
    ];
    
    public function __construct()
    {
        $this->action ='widgets_init';
    }

    public function index()
    {
        foreach ($this->widgets as $widget) {
            register_widget( $widget );
        }
    }


}