<?php

namespace App\Actions;

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 14.09.17
 *
 */
use App\Classes\Action\ActionRegister;

class ActionRemover extends ActionRegister
{
    public function __construct()
    {
        $this->action = 'wp_loaded';
    }


    public function index()
    {
        $post = get_post(url_to_postid($_SERVER['REQUEST_SCHEME'].'://'. $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']));

        //For Divi content stay some functional
        if(!empty($post->post_content) and stripos($post->post_content,'[et_pb_section' !== false)){
            remove_action('wp_head', 'et_divi_add_customizer_css');
        }

        remove_action('wp_head', 'print_emoji_detection_script', 7);
        remove_action('wp_print_styles', 'print_emoji_styles');

        remove_action('wp_head', 'head_addons', 7);
        remove_action('wp_head', 'wp_generator');

        remove_action('wp_head','feed_links_extra', 3);
        remove_action('wp_head','feed_links', 2);
        remove_action('wp_head','rsd_link');
        remove_action('wp_head','wlwmanifest_link');

        remove_action( 'wp_head', 'et_add_viewport_meta');

        remove_action( 'wp_head', 'rest_output_link_wp_head');
        remove_action( 'wp_head', 'wp_oembed_add_discovery_links');
        remove_action( 'template_redirect', 'rest_output_link_header', 11);

        remove_action( 'wp_enqueue_scripts', 'et_builder_load_modules_styles', 11);
        remove_action( 'wp_enqueue_scripts', 'et_shortcodes_css_and_js');


        // it works in wordpress-seo while it activated this method is useless
        remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10);

        remove_filter( 'the_content', 'wp_make_content_images_responsive' );

    }

}