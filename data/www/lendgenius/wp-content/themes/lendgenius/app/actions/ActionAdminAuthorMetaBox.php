<?php

namespace App\Actions;

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 13.02.18
 *
 */
use App\Classes\Action\ActionRegister;

class ActionAdminAuthorMetaBox extends ActionRegister
{
    public function __construct()
    {
        $this->action = [
            'add_meta_boxes' => 'index',
            'save_post' => 'save',
        ];
    }

    /**
     * @return mixed
     * method witch executes by default on action adding
     */
    public function index()
    {
        $screens = array( 'post' );
        add_meta_box( 'post_author_meta', 'Select Author', array($this,'writeBlock'), $screens );
    }

    public function writeBlock($post, $meta)
    {
        $selectedOption = get_post_meta($post->ID, 'post_author_native',true);

        add_filter('posts_fields', function($fields){
            return 'wp_posts.ID, wp_posts.post_title';
        });

        $allAuthors = new \WP_Query([
            'posts_per_page' => -1,
            'post_type'   => 'authors',
            'fields'      =>'id=>parent'
        ]);

        $output = '<ul>';

        foreach ($allAuthors->posts as $author) {

            $selected = $author->ID == $selectedOption ? 'checked="checked"' : '';

            $output .= '<li><input '. $selected .' id="post_author_'. $author->ID .'" type="radio" name="post_author_native" value="'. $author->ID .'" />';
            $output .= '<label for="post_author_'. $author->ID .'">'. $author->post_title .'</label></li>';
        }

        $output .= '</ul>';

        echo $output;
    }

    public function save($post_id)
    {
        if ( ! isset( $_POST['post_author_native'] ) )
            return;

        if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
            return;

        if( ! current_user_can( 'edit_post', $post_id ) )
            return;

        update_post_meta( $post_id, 'post_author_native', $_POST['post_author_native'] );

    }
}