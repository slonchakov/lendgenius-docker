<?php

namespace App\Actions;

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 14.09.17
 *
 */
use App\Classes\Action\ActionRegister;

class ActionMorePosts extends ActionRegister
{
    public function __construct()
    {
        $this->action = [
            'wp_ajax_nopriv_more_post_ajax' => 'index',
            'wp_ajax_more_post_ajax' => 'index',
            'wp_ajax_nopriv_more_post_ajax_blog' => 'blog',
            'wp_ajax_more_post_ajax_blog' => 'blog'
        ];
    }

    function index()
    {

        $posts_per_page = (isset($_POST["ppp"])) ? $_POST["ppp"] : 10;
        $author = (isset($_POST["author"])) ? $_POST["author"] : false;
        $page = (isset($_POST['pageNumber'])) ? $_POST['pageNumber'] : 0;

        //header("Content-Type: text/html");
        header('Content-type: application/json');

        $args = array(
//		'suppress_filters' => true,
            'post_type' => 'post',
            'meta_key'   => 'author',
            'meta_value' => $author,
            'post_status' => 'publish',
            'posts_per_page' => $posts_per_page,
            'paged'    => $page,
            'orderby' => 'ID',
            'order' => 'DESC',
        );

        $loop = new \WP_Query($args);

        $post_count = $loop->post_count;
        $out = '';

        if ($loop -> have_posts()) :  while ($loop -> have_posts()) : $loop -> the_post();
            ob_start();
            get_template_part( 'views/ajax/content-author', get_post_format() );
            $out .=  ob_get_contents();
            ob_end_clean();

        endwhile;
        endif;
        wp_reset_postdata();
        die(json_encode(array('content' => $out, 'more' => (($post_count < $posts_per_page)? false:true))));
    }

    function blog(){

        $posts_per_page = (isset($_POST["ppp"])) ? $_POST["ppp"] : 15;
        $page = (isset($_POST['pageNumber'])) ? $_POST['pageNumber'] : 0;

        //header("Content-Type: text/html");
        header('Content-type: application/json');

        $args = array(
            'post_type' => 'post',
            'post_status' => 'publish',
            'orderby' => 'post_date',
            'order' => 'DESC',
            'posts_per_page' => $posts_per_page,
            'paged'    => $page,
        );

        $loop = new \WP_Query($args);

        $post_count = $loop->post_count;
        $out = '';

        $cc_post = 0;
        if ($loop -> have_posts()) :  while ($loop -> have_posts()) : $loop -> the_post();
            ob_start();
            if($cc_post == 0 || ($cc_post % 5) == 0){
                get_template_part( 'views/ajax/content-blog-general-post', get_post_format() );
                ?>
                <div class="list-posts-wrapper">
                <?php
            }else{
                get_template_part( 'views/ajax/content-blog-sub-post', get_post_format() );
            }

            $cc_post += 1;
            if(($cc_post % 5) == 0 || $cc_post == $post_count){
                ?>
                </div>
                <?php
            }
            $out .=  ob_get_contents();
            ob_end_clean();

        endwhile;
        endif;
        wp_reset_postdata();
        die(json_encode(array('content' => $out, 'more' => (($post_count < $posts_per_page)? false:true))));
    }
    
    


}