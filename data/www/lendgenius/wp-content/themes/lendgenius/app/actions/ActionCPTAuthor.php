<?php

namespace App\Actions;

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 14.09.17
 *
 */
use App\Classes\Action\ActionRegister;

class ActionCPTAuthor extends ActionRegister
{
    public function __construct()
    {
        $this->action = 'admin_head';
    }

    public function index()
    {
        remove_meta_box( 'postimagediv', 'authors', 'side' );
        add_meta_box('postimagediv', __('Author image', 'lendgenius'), 'post_thumbnail_meta_box', 'authors', 'side', 'low');
    }




}