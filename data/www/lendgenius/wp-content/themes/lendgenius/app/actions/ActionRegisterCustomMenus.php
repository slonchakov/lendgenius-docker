<?php

namespace App\Actions;

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 14.09.17
 *
 */
use App\Classes\Action\ActionRegister;

class ActionRegisterCustomMenus extends ActionRegister
{
    public function __construct()
    {
        $this->action = 'after_setup_theme';
    }

    public function index()
    {
        register_nav_menus( array(
            'footer-menu-secondary' => 'Secondary Footer Menu',
        ) );
        
    }
}