<?php

namespace App\Actions;

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 14.09.17
 *
 */
use App\Classes\Action\ActionRegister;

class ActionAdminEditForm extends ActionRegister
{
    public function __construct()
    {
        $this->action = 'edit_form_after_title';
    }

    public function index()
    {
        global $post, $wp_meta_boxes;
        if( 'resources' == get_post_type() ) {
            ?>
            <script type="text/javascript">
                (function($) {

                    $(document).ready(function(){

                        $('#titlediv').append( $('#acf-sub_title') );

                    });

                })(jQuery);
            </script>
            <style type="text/css">
                #acf-sub_title .label label {
                    color: #333333;
                    font-size: 13px;
                    line-height: 1.5em;
                    font-weight: bold;
                    padding: 0;
                    margin: 0 0 3px;
                    display: block;
                    vertical-align: text-bottom;
                }
                #acf-sub_title #acf-field-sub_title {
                    padding: 3px 8px;
                    font-size: 1.7em;
                    line-height: 100%;
                    height: 1.7em;
                    width: 100%;
                    outline: 0;
                    margin: 0 0 3px;
                    background-color: #fff;
                }
            </style>
            <?php
        }
    }


}