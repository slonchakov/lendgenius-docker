<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 22.11.17
 *
 */

namespace App\Actions;


use App\Classes\Action\ActionRegister;
use App\Classes\FormElements;
use App\Classes\Repository\LenderFilterRepository;
use App\Classes\Repository\LoanRepository;
use App\Classes\Storage;

class ActionAdminLenderFilters extends ActionRegister
{
    private $formElements, $LenderFilter, $loanRepository;

    /**
     * ActionAdminLenderFilters constructor.
     * @param FormElements $formElements
     * @param LenderFilterRepository $lenderFilter
     * @param LoanRepository $loanRepository
     */
    public function __construct(FormElements $formElements, LenderFilterRepository $lenderFilter, LoanRepository $loanRepository)
    {
        $this->formElements   = $formElements;
        $this->LenderFilter   = $lenderFilter;
        $this->loanRepository = $loanRepository;

        $this->action = [
            'add_meta_boxes' => 'index',
            'save_post' => 'save'
        ];
    }

    public function index()
    {
        add_meta_box( 'lender_filters', 'Lender Filters', [$this,'callback'], 'lenders' );
    }

    /**
     * build additional block of filter options for Lender's admin page
     */
    public function callback()
    {
        global $post;

        $lenderFilter = $this->LenderFilter->all();

        $form   = '';
        $values = get_post_meta( $post->ID, 'lender_filters', true );

        $loans  = $this->loanRepository->all();

        $loanArgs   = [];
        $loanArgs[] = 'Category';
        $categories = [];

        foreach ($loans as $loan) {

            $categories[] = [
                'text'    => $loan->post_title,
                'name'    => 'lender_filters[category][categories]['. $loan->ID .']',
                'checked' => (!empty($values['category']['categories'][$loan->ID])) ? 'checked="checked"' : ''
            ];
        }

        $loanArgs[] = [
            'inputName' => 'Category',
            'items'     => $categories,
            'active'    => 'lender_filters[category][active]',
            'is_active' => (empty($values['category']['active']) || $values['category']['active'] == 'on') ? 'checked="checked"' : '',
        ];

        $form .= $this->formElements->checkbox(...$loanArgs);
        
        foreach ($lenderFilter as $filter) {

            $type   = get_post_meta( $filter->ID, 'type', true );
            $args   = [];
            $args[] = 'lender_filters['. $filter->ID . '][]';

            $valueNameWords = explode(' ', $filter->post_title);

            $valueName = '';

            foreach ( $valueNameWords as $key => $value) {
                if ($key == 0) {
                    $valueName .= strtolower($value);
                } else {
                    $valueName .= ucfirst(strtolower($value));
                }
            }

            switch ($type) {
                case 'number' :
                    $formType = 'number';

                    $args[] = [
                        'inputName' => $filter->post_title,
                        'active'    => 'lender_filters['. $valueName . '][active]',
                        'is_active' => (empty($values[$valueName]['active']) || $values[$valueName]['active'] == 'on') ? 'checked="checked"' : '',
                        'inputs'    => [
                            [
                                'label' => 'Minimal limit',
                                'name'  => 'lender_filters['. $valueName . '][minimal]',
                                'value' => (!empty($values[$valueName]['minimal'])) ? $values[$valueName]['minimal'] : ''
                            ],
                            [
                                'label' => 'Maximal limit',
                                'name'  => 'lender_filters['. $valueName . '][maximal]',
                                'value' => (!empty($values[$valueName]['maximal'])) ? $values[$valueName]['maximal'] : ''
                            ]
                        ]
                    ];
                    break;
                case 'checkbox' :
                    $formType = 'checkbox';
                    $items    = [];
                    $i        = 1;

                    while( have_rows('check_items', $filter->ID) ): the_row();

                        if (get_sub_field('active')) {
                            $items[] = [
                                'text'    => get_sub_field('item'),
                                'name'    => 'lender_filters['. $valueName . ']['. $i.']',
                                'checked' => (!empty($values[$valueName][$i])) ? 'checked="checked"' : ''
                            ];
                            $i++;
                        }

                    endwhile;

                    $args[] = [
                        'inputName' => $filter->post_title,
                        'items'     => $items,
                        'active'    => 'lender_filters['. $valueName . '][active]',
                        'is_active' => (empty($values[$valueName]['active']) || $values[$valueName]['active'] == 'on') ? 'checked="checked"' : '',
                    ];
                    break;
                case 'dropdown' :
                    $formType = 'checkbox';
                    $items    = [];
                    $i        = 1;

                    while( have_rows('dropdown_items', $filter->ID) ): the_row();

                        if (get_sub_field('active')) {

                            $items[]  = [
                                'text'    => get_sub_field('item'),
                                'name'    => 'lender_filters['. $valueName . ']['. $i .']',
                                'checked' => (!empty($values[$valueName][$i])) ? 'checked="checked"' : ''
                            ];
                            $i++;
                        }

                    endwhile;

                    $args[] = [
                        'inputName' => $filter->post_title,
                        'items'     => $items,
                        'active'    => 'lender_filters['. $valueName . '][active]',
                        'is_active' => (empty($values[$valueName]['active']) || $values[$valueName]['active'] == 'on') ? 'checked="checked"' : '',
                    ];
                    break;

                case 'range' :
                    $items = [];
                    $formType = 'checkbox';
                    $i = 1;

                    while( have_rows('range', $filter->ID) ): the_row();

                        if (get_sub_field('active')) {

                            $items[]  = [
                                'minimal' => get_sub_field('minimal'),
                                'maximal' => get_sub_field('maximal'),
                                'text'    => get_sub_field('text'),
                                'name'    => 'lender_filters['. $valueName . ']['. $i .']',
                                'checked' => (!empty($values[$valueName][$i])) ? 'checked="checked"' : ''
                            ];

                            $i++;
                        }
                    endwhile;

                    $args[] = [
                        'inputName' => $filter->post_title,
                        'items'     => $items,
                        'active'    => 'lender_filters['. $valueName . '][active]',
                        'is_active' => (empty($values[$valueName]['active']) || $values[$valueName]['active'] == 'on') ? 'checked="checked"' : '',
                    ];
                    break;
                case 'boolean' :
                    $formType = 'radio';

                    $args[] = [
                        'inputName' => $filter->post_title,
                        'items'     => [
                            [
                                'text'    => 'yes',
                                'name'    => 'lender_filters['. $valueName . '][value]',
                                'value'   => 1,
                                'checked' => (!empty($values[$valueName]['value']) && $values[$valueName]['value'] == 1 ) ? 'checked="checked"' : ''
                            ],
                            [
                                'text'    => 'no',
                                'name'    => 'lender_filters['. $valueName . '][value]',
                                'value'   => 0,
                                'checked' => (empty($values[$valueName]) || $values[$valueName]['value'] == 0 ) ? 'checked="checked"' : ''
                            ]
                        ],
                        'active'    => 'lender_filters['. $valueName . '][active]',
                        'is_active' => (!empty($values[$valueName]['active'])) ? 'checked="checked"' : '',
                    ];
                    break;
                default :
                    break;
            }

            if (count($args) && !empty($formType)) {

                $form .= $this->formElements->{$formType}(...$args);
            }
        }

        $form .= '';

        echo $form;
    }

    /**
     * save lender filters in database
     * @param $post_id
     * @return mixed
     */
    public function save($post_id)
    {

//        $cache = Storage::get();
//
//        $cache->flush();

        if (!isset($_POST)) return $post_id;

        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
            return $post_id;

        if (isset($_POST['lender_filters'])) {

            update_post_meta( $post_id, 'lender_filters', $_POST['lender_filters'] );
        }
        return $post_id;
    }
}