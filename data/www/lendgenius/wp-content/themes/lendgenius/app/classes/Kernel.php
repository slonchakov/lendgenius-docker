<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 02.11.17
 *
 */
namespace App\Classes;

class Kernel {

    private $wp = null;

    public function __construct()
    {
        global $wp;
        $this->wp = $wp;
    }
    
    public function response()
    {
        return (!$this->wp->request) ? false : (new Response(Router::getInstance(), $this->wp->request))->checkRoute();
    }

    public function getWp()
    {
        return $this->wp;
    }
}