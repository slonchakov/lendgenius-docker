<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 27.11.17
 *
 */

namespace App\Classes\Cache;


interface Cache
{
    /**
     * method use with cache object creating
     * cache object holding in static Storage class
     * @return mixed
     */
    public function init();

    /**
     * Add item to cache storage
     * @param $key
     * @param $value
     * @return mixed
     */
    public function add($key, $value);

    public function get($key);

    public function delete($key);

    public function flush();

}