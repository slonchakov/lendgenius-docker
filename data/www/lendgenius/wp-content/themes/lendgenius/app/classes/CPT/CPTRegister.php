<?php

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 15.09.17
 *
 */
namespace App\Classes\CPT;

class CPTRegister implements CPTInterface
{
    
    protected $labels = [];
    protected $arguments = [];
    protected $name = '';

    /**
     * Required method realized here and no necessity to define it in children
     */
    public function index()
    {
        register_post_type( $this->name, $this->arguments );
    }

}