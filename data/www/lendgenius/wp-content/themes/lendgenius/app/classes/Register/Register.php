<?php

namespace App\Classes\Register;
use App\Classes\Container;

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 13.09.17
 *
 */

class Register
{
    
    static $list = [];

    public static function all()
    {
        foreach (static::$list as $name => $list ) {
            array_map(function($element) use ($name){
                static::doAdd(static::getClassFromFile($name, $element), $name);
            }, $list);
        }
    }

    private static function getClassFromFile($name, $fileName)
    {
        $ucName = ucfirst($name);

        $className = '\\App\\'. $ucName . "s\\" . $ucName . $fileName;

        return Container::make( $className );
    }

    private static function doAdd($classInstance, $name)
    {
        if ( !$classInstance->is_active ) return;

        $hookFunc  = 'add_'. $name;
        $classHook = $classInstance->{$name};
        $args      = [$classInstance->priority, $classInstance->arguments];

        if ( !is_array($classHook) ) {
            array_unshift($args, $classHook, [$classInstance,'index']);
            call_user_func_array($hookFunc, $args);
        } else {
            foreach ( $classHook as $name => $method ) {
                array_unshift($args, $name, [$classInstance, $method]);
                if (!is_array($method)) {
                    call_user_func_array($hookFunc, $args);
                } else {
                    array_map(function($method) use ($classInstance, $args, $hookFunc){
                        $args[2] = [$classInstance, $method];
                        call_user_func_array($hookFunc, $args);
                    },$method);
                }
            }
        }
    }

    public static function addToList($type, $name)
    {
        self::$list[$type][] = $name;
    }
}