<?php

namespace App\Classes\Filter;
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 14.09.17
 *
 */
interface FilterInterface
{

    /**
     * @param $arg
     * @return mixed
     * Require method for filter registration
     */
    public function index($arg);
}