<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 24.11.17
 *
 */

namespace App\Classes\Repository;


class LenderRepository
{

    private $postType = 'lenders';

    public function all()
    {
        $args = array(
            'numberposts' => -1,
            'order'       => 'ASC',
            'post_type'   => $this->postType,
        );

        return get_posts( $args );
    }

}