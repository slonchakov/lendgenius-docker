<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 22.11.17
 *
 */

namespace App\Classes;


class FormElements
{

    /**
     * Build inputs for number field with limits
     * @param string $name
     * @param array $args
     * @return string
     */
    public function number(string $name, array $args) : string
    {
        $inputs = array_map(function($input){
            return '<label for="">'. $input['label'] .'</label><input type="text" name="'. $input['name']. '" value="'. $input['value']. '" />';
        }, $args['inputs']);

        return $this->inputBlock($args['inputName'], $inputs, $args['active'], $args['is_active']);
    }

    /**
     * Build select element
     * @param string $name
     * @param array $items
     * @return string
     */
    public function select(string $name,array $items) : string
    {
        $options = array_map(function($item){
            return '<option value="'. $item['value'] .'">'. $item['text'] .'</option>';
        },$items);

        return '<select name="'. $name .'" >'. implode($options) .'</select>';
    }

    /**
     * Build range element
     * @param string $name
     * @param $args
     * @return string
     */
    public function range(string $name, $args) : string
    {
        //
    }

    /**
     * Build set of checkboxes
     * @param string $name
     * @param array $args
     * @return string
     */
    public function checkbox(string $name,array $args) : string
    {
        $options = array_map(function($item) {
            return '<input type="checkbox" '. $item['checked'] .' name="'. $item['name'] .'" value="1" /><label>'. $item['text'] .'</label>';
        },$args['items']);

        return $this->checkboxBlock($args['inputName'],$options, $args['active'], $args['is_active']);
    }

    /**
     * Build set of radio boxes
     * @param string $name
     * @param array $args
     * @return string
     */
    public function radio(string $name,array $args) : string
    {
        $options = array_map(function($item) {
            return '<input type="radio" '. $item['checked'] .' name="'. $item['name'] .'" value="'. $item['value'] .'" /><label>'. $item['text'] .'</label>';
        },$args['items']);

        return $this->checkboxBlock($args['inputName'],$options, $args['active'], $args['is_active']);
    }

    /**
     * Build completed block for form with text inputs
     * @param string $label
     * @param array $inputs
     * @return string
     */
    private function inputBlock(string $label,array $inputs, $active, $is_active) : string
    {
        $output = '<div class="acf-field"><div class="acf-label"><label for="">'. $label .'</label><hr><input type="checkbox" name="'. $active .'" '. $is_active .'  /> <b>Active</b></div><div class="acf-input">';

            foreach ($inputs as $input) {
                $output .='<div class="acf-input-wrap">'. $input . '</div>';
            }

        $output .= '</div></div>';

        return $output;
    }

    /**
     * Build complete block for form with checkboxes
     * @param string $label
     * @param array $inputs
     * @param string $active
     * @param string $is_active
     * @return string
     */
    private function checkboxBlock(string $label,array $inputs, string $active, string $is_active = 'checked="checked"') : string
    {
        $output = '<div class="acf-field acf-field-checkbox"><h3>'. $label .'</h3><div class="acf-label"><hr><input type="checkbox" name="'. $active .'" '. $is_active .'  /> <b>Active</b></div>';

            foreach ($inputs as $input) {

                $output .= '<div class="acf-input">'. $input .'</div>';
            }

        $output .= '</div>';

        return $output;
    }
}