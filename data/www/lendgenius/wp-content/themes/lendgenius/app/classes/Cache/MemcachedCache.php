<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 27.11.17
 *
 */

namespace App\Classes\Cache;


class MemcachedCache implements Cache
{
    private $cache = null;

    public function init()
    {
        $this->cache = new \Memcached();
        $this->cache->addServer('localhost', 11211);
    }

    public function add($key, $value)
    {
        return $this->cache->add($key,$value);
    }

    public function append($key, $value)
    {
        return $this->cache->append($key,$value);
    }

    public function get($key)
    {
        return $this->cache->get($key);
    }

    public function delete($key)
    {
        return $this->cache->delete($key);
    }

    public function flush()
    {
        $this->cache->flush();

        return $this->cache->getResultCode();
    }
}