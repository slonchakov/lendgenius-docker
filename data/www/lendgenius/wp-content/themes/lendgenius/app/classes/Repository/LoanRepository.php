<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 24.11.17
 *
 */

namespace App\Classes\Repository;


class LoanRepository
{
    private $postType = 'loans';

    public function all($short = null)
    {
        $args = array(
            'numberposts' => -1,
            'order'       => 'ASC',
            'post_type'   => $this->postType,
        );

        if ($short) {
            return $this->short($args);
        }

        return get_posts( $args );
    }

    public function short($args)
    {
        $loans  = get_posts($args);
        $result = [];

        foreach ($loans as $loan) {

            $result[$loan->ID] = [
                'name' => $loan->post_title,
                'description' => $loan->post_excerpt
            ];
        }

        return $result;
    }
}