<?php

namespace App\Classes\Sidebar;
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 08.09.17
 *
 */
class Sidebar
{
    /**
     * @var array
     * Array of sidebars
     * keys "name" and "id" are required
     */
    private $sidebars = [
        [
            'name'          => 'Blog Article Sidebar',
            'id'            => 'sidebar-single',
            'description'   => 'Appears as the sidebar on the "blog-article" page',
            'before_widget' => '<div id="%1$s" class="content-aside-items %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<div class="item-title-primary">',
            'after_title'   => '</div>',
        ],
        [
            'name'          => 'Blog Sidebar',
            'id'            => 'sidebar-blog',
            'description'   => 'ppears as the sidebar on the "blog" page',
            'before_widget' => '<div class="content-aside-items">',
            'after_widget'  => '</div>',
            'before_title'  => '<div class="item-title-primary">',
            'after_title'   => '</div>',
        ],
        [
            'name'          => 'Author Sidebar',
            'id'            => 'sidebar-author',
            'description'   => 'Appears as the sidebar on the "author" page',
            'before_widget' => '<div class="content-aside-items">',
            'after_widget'  => '</div>',
            'before_title'  => '<div class="item-title-primary">',
            'after_title'   => '</div>',
        ],
        [
            'name'          => 'How It Works Sidebar',
            'id'            => 'sidebar-how-it-works',
            'description'   => 'Appears as the sidebar on the "how it works" page',
            'before_widget' => '<div class="content-aside-items">',
            'after_widget'  => '</div>',
            'before_title'  => '<div class="item-title-primary">',
            'after_title'   => '</div>',
        ],
        [
            'name' => 'Under Post Area',
            'id'   => 'under-post-area',
        ],
        [
            'name'          => 'Footer Area' . ' #1',
            'id'            => 'sidebar-2',
            'before_widget' => '<div id="%1$s" class="fwidget et_pb_widget %2$s">',
            'after_widget'  => '</div> <!-- end .fwidget -->',
            'before_title'  => '<h4 class="title">',
            'after_title'   => '</h4>',
        ],
        [
            'name'          => 'Footer Area' . ' #2',
            'id'            => 'sidebar-3',
            'before_widget' => '<div id="%1$s" class="fwidget et_pb_widget %2$s">',
            'after_widget'  => '</div> <!-- end .fwidget -->',
            'before_title'  => '<h4 class="title">',
            'after_title'   => '</h4>',
        ],
        [
            'name'          => 'Footer Area' . ' #3',
            'id'            => 'sidebar-4',
            'before_widget' => '<div id="%1$s" class="fwidget et_pb_widget %2$s">',
            'after_widget'  => '</div> <!-- end .fwidget -->',
            'before_title'  => '<h4 class="title">',
            'after_title'   => '</h4>'
        ]
    ];

    /**
     * register all sidebars from array $sidebars
     */
    public function register()
    {
        foreach ($this->sidebars as $sidebar) {
            register_sidebar($sidebar);
        }
    }

    /**
     * @param $sidebar
     * Add new sidebar
     */
    public function add($sidebar)
    {
        $this->sidebars[] = $sidebar;
    }
}