<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 04.12.17
 *
 */

namespace App\Classes\Model;


use App\Classes\Repository\LenderRepository;

class Lender
{
    private $lenderRepository, $lenderFilter;
    
    public function __construct(LenderRepository $lenderRepository, LenderFilter $lenderFilter)
    {
        $this->lenderFilter     = $lenderFilter;
        $this->lenderRepository = $lenderRepository;
        $this->filterTypes      = $this->lenderFilter->filterTypes();
    }

    public function getLenders($query = null) :array
    {
        $allLenders = $this->lenderRepository->all();

        if ($query == null) return $allLenders;

        $output = [];

        parse_str($query, $queryFilters);

        foreach ($allLenders as $lender) {

            $isAdd = null;
            $currentLenderFilters = get_post_meta($lender->ID,'lender_filters', true);

            foreach ($queryFilters as $filterId => $filter) {
                if ($isAdd === false) break;

                if ($filterId == 'invoiceFactoringLoans' && $isAdd !== false) {
                    $isAdd = true;
                    continue;
                }

                /**
                 * Here we check filters in lender with filters in query
                 *
                 * if $isAdd not false
                 * if in lender exists needed filter
                 * if needed filter in lender is active
                 * if in lender filter exists needed value
                 */

                if ($this->filterTypes[$filterId] == 'number') {
                    $isAdd = ($isAdd !== false && $this->checkNumber($filter,$currentLenderFilters[$filterId]));
                } else {
                    $isAdd = (  $isAdd !== false &&
                        !empty($currentLenderFilters[$filterId]) &&
                        $currentLenderFilters[$filterId]['active'] == 'on' &&
                        !empty($currentLenderFilters[$filterId][$filter])
                    );
                }
            }

            if ($isAdd) {
                $output[] = $lender;
            }
        }

        return $output;
    }

    private function checkNumber($count, $values)
    {


        $min = (int)$values['minimal'];
        $max = (int)$values['maximal'];

        if ($max == 0 && $count >= $min) return true;

        if ($count >= $min && $count <= $max) {
            return true;
        }

        return false;
    }
}