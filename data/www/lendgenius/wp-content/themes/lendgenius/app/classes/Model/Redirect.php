<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 04.12.17
 *
 */

namespace App\Classes\Model;


class Redirect
{
    private $link, $redirects;

    public function __construct()
    {
        $this->redirects = get_option('redirect_settings');
    }

    public function getLinkFromMeta($ID)
    {
        $this->link = '';

        $path = get_post_meta($ID,'redirect_link',true);

        if (strpos($path, 'http') !== false || strpos($path, 'go/') !== false) {
            $this->link = $path;
            return $this->link;
        }

        array_map(function($item) use ($path){
            if ($item['slug'] == $path) {
                $this->link = $item['link'];
            }
        },$this->redirects);

        return $this->link;

        // return "/redirect/". get_post_meta($ID,'redirect_link',true);
    }

    public function getLinkByPath($path)
    {
        $this->link = '';
        array_map(function($item) use ($path){
            if ($item['slug'] == $path) {
                $this->link = $item['link'];
            }
        },$this->redirects);

        return $this->link;
    }
}