<?php

namespace App\Classes\Filter;
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 14.09.17
 *
 */

abstract class FilterRegister implements FilterInterface
{
    public $is_active = true;
    /**
     * @var mixed
     * Can by admin, front or false for both
     */
    public $only_for  = false;
    /**
     * @var mixed
     * if string using for one action for several action use it as array key - action; value - method;
     * method index() using by default; for custom method, name is no matter
     */
    public $filter = '';
    public $priority = 10;
    public $arguments = 1;

    /**
     * @return mixed
     * method witch executes by default on action adding
     * @var $arg
     */
    abstract public function index($arg);
}

