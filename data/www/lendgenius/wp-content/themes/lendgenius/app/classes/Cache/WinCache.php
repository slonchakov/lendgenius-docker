<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 28.11.17
 *
 */

namespace App\Classes\Cache;


class WinCache implements Cache
{


    /**
     * method use with cache object creating
     * cache object holding in static Storage class
     * @return mixed
     */
    public function init()
    {

    }

    /**
     * Add item to cache storage
     * @param $key
     * @param $value
     * @return mixed
     */
    public function add($key, $value)
    {
        return wincache_ucache_set($key, $value);
    }

    public function get($key)
    {
        return wincache_ucache_get($key);
    }

    public function delete($key)
    {
        return wincache_ucache_delete($key);
    }

    public function flush()
    {
        return wincache_ucache_clear();
    }
}