<?php

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 07.09.17
 *
 */
namespace App\Classes\RequireOnce;

class RequirePath
{

    private $paths = [];

    public function __construct()
    {

    }
    /**
     * @param $paths
     * Add paths for including in app/include.php
     */
    public function setPaths(array $paths)
    {
        $this->paths = $paths;
        
    }

    /**
     * @param $path
     * add path for including must be used before RequireService::get()->require()
     */
    public function addPath($path)
    {
        $this->paths[] = $path;
    }

    /**
     * @return array
     * get all paths
     */
    public function getPaths():array
    {
        return $this->paths;
    }

    /**
     * do requiring files
     */
    public function require()
    {
        foreach ($this->paths as $path) {
            require_once LG_THEME_ROOT. $path .'.php';
        }
    }

}