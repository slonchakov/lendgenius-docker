<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 02.11.17
 *
 */

namespace App\Classes;


class Container
{

    public static $pool = [];
    /**
     * @param $class
     * @return mixed
     * Make new instance of given class with arguments if they needed
     */
    public static function make($class)
    {
        if (!empty(static::$pool[$class])) {
            return static::$pool[$class];
        }
        static::$pool[$class] = new $class(...static::arguments($class));

        return static::$pool[$class];
    }

    /**
     * @param $class
     * @return array
     * Return array of objects for dependency injection.
     * Use recursion for dependencies of dependencies.
     * Use Reflection API for getting all parameters of __construct()
     *
     *  TODO make it not only for __construct()
     */
    private static function arguments($class) : array
    {

        if (!method_exists($class, '__construct')) return [];

        return array_map(function(\ReflectionParameter $item){

            $injectedClass = $item->getClass()->name;

            return ($injectedClass) ? static::make($injectedClass) : null;

        }, (new \ReflectionMethod($class, '__construct'))->getParameters());
    }
}