<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 02.11.17
 *
 */

namespace App\Classes;


class View
{
    const VIEWS_PATH = LG_THEME_ROOT. 'views/pages/';

    public static function load(string $template,array $data =[]) : string
    {
        
        if (file_exists(static::VIEWS_PATH . $template .'.php')) {

            if (count($data)) {
                global $wp;
                $wp->data = $data;
            }

            return static::VIEWS_PATH . $template .'.php';
        }

        return '';
    }
}