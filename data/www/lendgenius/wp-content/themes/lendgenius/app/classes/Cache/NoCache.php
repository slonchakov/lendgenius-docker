<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 28.11.17
 *
 */

namespace App\Classes\Cache;


class NoCache implements Cache
{

    /**
     *
     * method use with cache object creating
     * cache object holding in static Storage class
     * @return mixed
     */
    public function init()
    {

    }

    /**
     * Add item to cache storage
     * @param $key
     * @param $value
     * @return mixed
     */
    public function add($key, $value)
    {
        return false;
    }

    public function get($key)
    {
        return false;
    }

    public function delete($key)
    {
       return false;
    }

    public function flush()
    {
        return false;
    }
}