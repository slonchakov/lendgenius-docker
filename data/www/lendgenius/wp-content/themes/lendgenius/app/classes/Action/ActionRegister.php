<?php

namespace App\Classes\Action;
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 13.09.17
 *
 */

abstract class ActionRegister implements ActionInterface
{
    public $is_active = true;
    /**
     * @var mixed
     * Can by admin, front or false for both
     */
    public $only_for  = false;
    /**
     * @var mixed
     * if string using for one action for several action use it as array key - action; value - method;
     * method index() using by default; for custom method, name is no matter
     */
    public $action = '';
    public $priority = 10;
    public $arguments = 1;

    /**
     * @return mixed
     * method witch executes by default on action adding
     */
    abstract public function index();
}