<?php

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 07.09.17
 *
 */

namespace App\Classes\RequireOnce;

class RequireService
{

    static $require = null;

    /**
     * @return RequirePath
     * Return instance of RequirePath
     */
    public static function get() :RequirePath
    {
        if (self::$require !== null) return self::$require;
        
        self::$require = new RequirePath();
        
        return self::$require;
    }
}