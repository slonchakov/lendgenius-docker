<?php

namespace App\Classes\Action;
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 13.09.17
 *
 */
interface ActionInterface
{
    /**
     * @return mixed
     * default method wich calls on action register
     */
    public function index();
}