<?php

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 18.09.17
 *
 */
namespace App\Classes;

class ImageSizes
{

    private $image_sizes = [
        'sidebar-thumb'         => [370, 147, true],
        'top-article-thumb'     => [370, 250, true],
        'post-thumb'            => [770, 250, true],
        'post-thumb-general'    => [870, 200, true],
        'post-thumb-sub'        => [270, 115, true],
        'lender-box'            => [150, 63, true ],
        'author-thumb'          => [170, 170, true],
        'author-thumb-posts'    => [370, 207, true],
        'category-latest-post'  => [54, 54, true],
        'landing-gmail-general' => [570, 478, true ],
        'hero-image-2560-667'   => [2560, 667, true],
        'hero-image-1280-350'   => [1280, 350, true],
        'hero-image-1280-563'   => [1280, 563, true],
        'hero-image-770-350'    => [770, 350, true],
        'hero-image-400-342'    => [400, 342, true],
        'get-started-general'   => [325, 685, true],
        'form-front-mobile'     => [770, 350, true],
        'feedback-post'         => [295, 417, true],
        'credit-card-grid'      => [150, 100, true]
    ];

    public function getImageSizes()
    {
        return $this->image_sizes;
    }

}