<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 04.12.17
 *
 */

namespace App\Classes\Model;


use App\Classes\Repository\LenderFilterRepository;

class LenderFilter
{
    private $lenderFilters;
    
    public function __construct(LenderFilterRepository $lenderFilters)
    {
        $this->lenderFilters = $lenderFilters->all();
    }

    public function all()
    {
        return $this->lenderFilters;
    }

    public function filterTypes() : array
    {
        $result = [];

        foreach ($this->lenderFilters as $lenderFilterItem) {

            $name = $this->camelCasedName($lenderFilterItem->post_title);

            $result[$name] = get_post_meta($lenderFilterItem->ID, 'type', true);
        }

        return $result;
    }

    public function camelCasedName($name)
    {
        return str_replace(" ","", lcfirst(ucwords(strtolower($name))));
    }
}