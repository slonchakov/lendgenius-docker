<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 27.11.17
 *
 */

namespace App\Classes;


use App\Classes\Cache\Cache;

class Storage
{
    private static $cache;

    public static function set(Cache $cache)
    {
        if (static::$cache !== null) return;

        static::$cache = $cache;
        static::$cache->init();
    }

    public static function get() : Cache
    {
        return static::$cache;
    }
}