<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 04.12.17
 *
 */

namespace App\Classes\Model;


use App\Classes\Repository\LoanRepository;
use App\Classes\Storage;

class Loan
{
    private $loanRepository;
    
    public function __construct(LoanRepository $loanRepository)
    {
        $this->loanRepository = $loanRepository;
    }

    public function all($short = null)
    {
        return $this->loanRepository->all($short);
    }
    
    public function categories() : array
    {
        $cache = Storage::get();

        if ($cache->get('all_lender_categories')) {
            return $cache->get('all_lender_categories');
        }

        $loans = $this->all();

        $output = array_map(function($loan){

            return [
                'id'          => $loan->ID,
                'name'        => $loan->post_title,
                'description' => $loan->post_excerpt
            ];

        },$loans);

        $cache->add('all_lender_categories', $output);

        return $output;
    }
}