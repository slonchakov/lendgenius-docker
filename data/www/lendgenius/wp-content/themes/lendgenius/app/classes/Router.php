<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 02.11.17
 *
 */

namespace App\Classes;


class Router
{
    private $get = [];

    private static $instance = null;
    
    public static function get($route,$controller)
    {
        if (null === static::$instance) {
            static::getInstance();
        }
        
        static::$instance->get[$route] = $controller;
    }

    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    public static function getRoutes()
    {
        return static::$instance->get;
    }

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }
}