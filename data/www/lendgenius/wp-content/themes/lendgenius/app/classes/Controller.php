<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 02.11.17
 *
 */

namespace App\Classes;


abstract class Controller
{
    protected $vars;
    /**
     * adding variables from Response for usage in controller
     * @param $vars
     */
    public function addPathVars($vars)
    {
        $this->vars = $vars;
    }

    /**
     * return real path of template
     * @param $template
     * @return string
     */
    public function view(string $template, array $data = [])
    {
        return View::load($template, $data);
    }

    /**
     * Use for returning json instead of template from controller. As template use empty.php, it have no content
     * @param array $data
     * @return string
     */
    public function output(array $data)
    {
        header('Content-Type: application/json');
        echo json_encode($data);

        return View::load('empty');
    }
}