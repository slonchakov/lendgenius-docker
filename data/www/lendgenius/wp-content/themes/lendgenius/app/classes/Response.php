<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 02.11.17
 *
 */

namespace App\Classes;


class Response
{
    private $router, $request, $pathVars;
    public function __construct(Router $router, string $request)
    {
        $this->router       = $router;
        $this->request      = $request;
        $this->requestParts = explode('/',$request);
    }

    /**
     * Check request path with routes and find more similar
     * @return string
     */
    public function checkRoute() : string
    {
        $results = [];
        $routes  = $this->router->getRoutes();

        foreach ($routes as $route => $controller) {

            if ($this->checkSimilarity(explode('/', $route))) {
                $results[similar_text ( $route , $this->request )] = $controller;
            }
        }

        if (!count($results)) return '';

        $moreSpecific = max($results);

        list( $controllerName, $method) = explode('@',$moreSpecific);

        $controller = Container::make('\\App\\Http\\'. $controllerName);

        /**
         * We add variables from url via this method
         * TODO make it with DI
         */
        $controller->addPathVars($this->pathVars);
        
        $response = $controller->{$method}();

        return $response;
    }

    /**
     * Checking situations when we have variables in route
     * @param string $item
     * @return bool
     */
    public function isVar(string $item) : bool
    {
        return (strpos($item,'{') === false ) ? false : true;
    }

    /**
     * Check similarity of request and route
     * @param array $path
     * @return bool
     */
    private function checkSimilarity(array $path) : bool
    {
        $similarity = false;

        for ($i = 0; $i < count($path); $i++) {

            if (empty($this->requestParts[$i])) {
                $similarity = false;
                continue;
            }

            if ($this->isVar($path[$i])) {
                $this->addPathVar($path[$i], $this->requestParts[$i]);
                continue;
            };

            $similarity = ($this->requestParts[$i] == $path[$i]);
        }

        return $similarity;
    }

    /**
     * If routes include variable add same path of request to array of path variables for using in controllers
     * @param $item
     * @param $request
     */
    private function addPathVar($item, $request)
    {
        $this->pathVars[preg_replace('/[\{\}]/', '', $item)] = $request;
    }
}