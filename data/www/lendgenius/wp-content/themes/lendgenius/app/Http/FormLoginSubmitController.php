<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 03.09.18
 * Time: 18:17
 */

namespace App\Http;

use App\Classes\Controller;


class FormLoginSubmitController extends Controller
{

    private $formUrl   = '/loan-request';
    private $remoteUrl = 'https://loanmatchingservice.com/misc/';
    private $options   = [
        'responseType' => 'json',
        '__salt'       => '1b5624ff-dde2-44c4-e8caef500275',
        'leadtypeid'   => 19
    ];

    public function login()
    {
        if($_SERVER['REQUEST_METHOD'] !== 'POST') {
            header('Location: '. $this->formUrl);
        }

        if (!empty($_POST['Email'])) {

            $this->options['action'] = 'leadreturnemail';
            $this->options['email']  = filter_var($_POST['Email'], FILTER_VALIDATE_EMAIL) ?? 'bad@email.not';

        } else if (!empty($_POST['PhoneHome'])) {

            $this->options['action'] = 'leadreturnphone';
            $this->options['phone']  = filter_var(str_replace("-","",$_POST['PhoneHome']), FILTER_SANITIZE_STRING) ?? 'bad_phone';
        }

        if (!empty($_POST['SsnLast4'])) {
            $this->options['ssn4'] = filter_var($_POST['SsnLast4'], FILTER_SANITIZE_NUMBER_INT) ?? 0000;
        }

        if (!empty($this->options['action'])) {

            $response = $this->sendRequest();

            if (!empty($response->RID))
                return $this->sendToForm($response);
        }

        return $this->returnBack();
    }

    private function sendRequest()
    {
        $curl = curl_init($this->remoteUrl . '?' . http_build_query($this->options));

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_USERAGENT, '');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $result = curl_exec($curl);
        curl_close($curl);

        return json_decode($result);
    }

    private function sendToForm($response)
    {
        setcookie('LastDate_'. $response->RID,  $response->LastDate);
        setcookie('Fields_'. $response->RID,  urlencode(json_encode($response->Fields, JSON_UNESCAPED_SLASHES)));
        header('Location: '. $this->formUrl .'?RID='. $response->RID);
    }

    private function returnBack()
    {
        $delimiter = (strpos($_SERVER['HTTP_REFERER'], '?') === false) ? '?' : '&';
        $query     = (strpos($_SERVER['HTTP_REFERER'], 'noCustomer') === false) ? $delimiter.'noCustomer' : '';
        header('Location: '. $_SERVER['HTTP_REFERER'] . $query);
    }
}