<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 22.11.17
 *
 */

namespace App\Http;


use App\Classes\Controller;
use App\Classes\Model\Lender;
use App\Classes\Model\LenderFilter;
use App\Classes\Model\Loan;
use App\Classes\Model\Redirect;
use App\Classes\Storage;

class LendersController extends Controller
{

    private $loan, $lender, $filterTypes, $redirect;

    /**
     * LendersController constructor.
     * @param LenderFilter $lenderFilter
     * @param Lender $lender
     * @param Loan $loan
     * @param Redirect $redirect
     * @internal param LenderRepository $lenderRepository
     * @internal param LenderFilterRepository $lenderFilters
     * @internal param LoanRepository $loanRepository
     */
    public function __construct(LenderFilter $lenderFilter, Lender $lender, Loan $loan, Redirect $redirect)
    {
        $this->loan         = $loan;
        $this->lender       = $lender;
        $this->redirect     = $redirect;
        $this->filterTypes  = $lenderFilter->filterTypes();
    }

    /**
     * Return list of lenders in json
     * @return string
     */
    public function list()
    {
        $query = $_SERVER['QUERY_STRING'];
        // $cache = Storage::get();
//
//        if (!empty($cache) && $cache->get('filtered_lenders_output'. $query)) {
//            return $this->output($cache->get('filtered_lenders_output'. $query));
//        }

        $loans  = $this->loan->all(true);
        $output = [];

        foreach ($this->lender->getLenders($query) as $lender) {

            $lenderFilters    = get_post_meta($lender->ID, 'lender_filters', true);
            $lenderCategories = $lenderFilters['category']['categories'];

            $qualifications = [];
            $pros = [];
            $cons = [];

            while (have_rows('qualifications', $lender->ID)): the_row();
                if (get_sub_field('active')) {
                    $qualifications[] = [
                        'name'  => get_sub_field('item'),
                    ];
                }
            endwhile;
            while (have_rows('pros', $lender->ID)): the_row();
                if (get_sub_field('active')) {
                    $pros[] = [
                        'name'  => get_sub_field('item'),
                    ];
                }
            endwhile;
            while (have_rows('cons', $lender->ID)): the_row();
                if (get_sub_field('active')) {
                    $cons[] = [
                        'name'  => get_sub_field('item'),
                    ];
                }
            endwhile;

            $interest = get_post_meta( $lender->ID, 'estimated_apr', true);
            $interestText = 'Estimated Apr';

            if ($interest == '') {
                $interest = get_post_meta( $lender->ID, 'interest', true);
                $interestText = get_post_meta( $lender->ID, 'interest_text', true);
            }

            $buttonText = get_post_meta( $lender->ID, 'button_text', true) ?? 'Get Your Rate';

            $lender = [
                'name'           => get_post_meta($lender->ID,'name', true),
                'logo'           => get_the_post_thumbnail_url($lender->ID),
                'apr'            => get_post_meta($lender->ID,'estimated_apr', true),
                'interest'       => $interest,
                'interestText'   => $interestText,
                'timeInBusiness' => get_post_meta($lender->ID,'time_in_business', true),
                'annualRevenue'  => get_post_meta($lender->ID,'annual_revenue', true),
                'creditScore'    => get_post_meta($lender->ID,'min_credit_score', true),
                'redirectLink'   => $this->redirect->getLinkFromMeta($lender->ID),
                'qualifications' => (count($qualifications)) ? $qualifications : null,
                'pros'           => (count($pros)) ? $pros : null,
                'cons'           => (count($cons)) ? $cons : null,
                'sort'           => get_post_meta($lender->ID,'sort_order', true),
                'buttonText'    => (!empty($buttonText)) ? $buttonText : 'Get Your Rate'
            ];

            foreach ($lenderCategories as $key => $category) {

                $itemId = $loans[$key]['name'];
                if (isset($_GET['invoiceFactoringLoans']) && $_GET['invoiceFactoringLoans'] == 0 && $itemId == 'Invoice Financing') {
                    continue;
                }

                if (empty($output[$itemId])) {
                    $output[$itemId] = [
                        'category'    => $itemId,
                        'description' => $loans[$key]['description']
                    ];
                }

                $output[$itemId]['items'][] = $lender;

                usort($output[$itemId]['items'], function($a, $b){
                    return ((int) $a['sort'] <=> (int) $b['sort']);
                });
            }
        }

        //$cache->add('filtered_lenders_output'. $query,array_values($output));

        return $this->output(array_values($output));
    }

    /**
     * Return json with all loans
     * @return string
     */
    public function categories() : string
    {
        return $this->output($this->loan->categories());
    }
}