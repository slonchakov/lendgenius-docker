<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 24.11.17
 *
 */

namespace App\Http;

use App\Classes\Controller;
use App\Classes\Model\LenderFilter;
use App\Classes\Storage;


class LenderFiltersController extends Controller
{

    private $lenderFilters;

    public function __construct(LenderFilter $lenderFilters)
    {
        $this->lenderFilters = $lenderFilters;
    }

    public function list()
    {
        $cache = Storage::get();

        if ($cache->get('all_lender_filters')) {
            return $this->output($cache->get('all_lender_filters'));
        }

        $filters = $this->lenderFilters->all();

        $output = [];
        foreach ($filters as $item) {

            $filter = new \stdClass();

            $options = [];

            switch ($item->type) {
                case 'dropdown' :
                    $i = 1;
                    while (have_rows('dropdown_items', $item->ID)): the_row();
                        if (get_sub_field('active')) {
                            $options[] = [
                                'name'  => get_sub_field('item'),
                                'value' => $i,
                            ];
                            $i++;
                        }
                    endwhile;
                    break;
                case 'range' :
                    $i = 1;
                    while (have_rows('range', $item->ID)): the_row();
                        if (get_sub_field('active')) {
                            $options[] = [
                                'name'  => get_sub_field('text'),
                                'value' => $i,
                            ];
                            $i++;
                        }
                    endwhile;
                    break;
            }

            $filter->name    = $this->lenderFilters->camelCasedName($item->post_title);
            $filter->title   = $item->post_title;
            $filter->type    = get_post_meta($item->ID,'type', true);
            $filter->options = $options;
            $filter->info    =  get_post_meta($item->ID,'hint', true);;

            $output[] = $filter;
        }

        $cache->add('all_lender_filters',$output);

        return $this->output($output);
    }
}