<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 02.11.17
 *
 */

namespace App\Http;

use App\Classes\Controller;

class RedirectController extends Controller
{
    private $link, $name, $slug, $logo;

    public function redirect() :string
    {
        global $wp;

        $path      = $this->vars['slug'];
        $redirects = get_option('redirect_settings');

        array_map(function($item) use ($path){

            if ($item['slug'] == $path) {
                $this->name = $item['name'];
                $this->slug = $item['slug'];
                $this->link = $item['link'];
                $this->logo = $item['logo'];
            }
        },$redirects);

        if (!empty($this->link)) {
            $wp->redirectName = ucfirst($this->name);
            $wp->redirectSlug = $this->slug;
            $wp->redirectUrl  = $this->link;
            $wp->redirectLogo = $this->logo;

            return $this->view('redirect');
        }

        return '';
    }
}