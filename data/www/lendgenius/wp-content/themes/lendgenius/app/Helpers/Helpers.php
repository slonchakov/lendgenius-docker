<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 08.09.17
 *
 */

if ( is_admin() ) {
//    include helpers for admin pages
    require_once 'AdminHelpers.php';
}

require_once 'Gamma.php';
require_once 'CustomMenu.php';

/**
 * @return mixed|void
 * returns link saved on admin settings page to buttons "See loan options"
 */
function cta_button_link(){
    return get_option( 'see_loan_option_url' );
}

/**
 * @param $key
 * @return string
 *
 */
function get_cus_link($key){
    switch ($key) {
        case 'resources-videos':
            return "/resources/videos/";
            break;
        case 'resources-tools':
            return "/resources/tools-and-templates/";
            break;
        case 'resources-credit-cards':
            return "/best-business-credit-cards/";
            break;
        case 'resources-calculators':
            return "/resources/loan-calculators/";
            break;
    }
}

/**
 * @param $content
 * @param string $title
 * Show Under Construction page
 */
function cust_maintenance($content, $title = 'Under Construction')
{
    header("HTTP/1.1 503 Service Unavailable");
    require_once '../../maintenance.php';
    exit;
}

/**
 * @return bool
 * TODO: know for what
 */
function et_builder_enqueue_font()
{
    return false;
}
/**
 * @return bool
 * TODO: know for what
 */
function et_divi_fonts_url()
{
    return false;
}

/**
 * Render post thumbnail
 */
if ( ! function_exists( 'single_post_thumbnail' ) ) :
    function single_post_thumbnail($size='thumbnail', $im_class = 'item-image-src', $get_post_thumbnail = false, $post_id = false) {
        global $post;
        if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
            return;
        }

        if ( is_singular() ) :
            if ( $get_post_thumbnail ) :
                if ( $post_id ) :
                    return get_the_post_thumbnail($post_id, $size, array( 'class' => $im_class ));
                else:
                    return get_the_post_thumbnail($post, $size, array( 'class' => $im_class ));
                endif;
            else:
                the_post_thumbnail($size, array( 'class' => $im_class ));
            endif;
        else :
            if ( $get_post_thumbnail ) :
                if ( $post_id ) :
                    return get_the_post_thumbnail($post_id, $size, array( 'class' => $im_class ));
                else:
                    return get_the_post_thumbnail($post, $size, array( 'class' => $im_class ));
                endif;
            else:
                the_post_thumbnail($size, array( 'class' => $im_class ));
            endif;

        endif; // End is_singular()
    }
endif;


/**
 * @param (array)$data
 * Set different backgrounds depends on datas
 * datas contains  such options as
 * 'ID': post id if not need use default
 * 'backgrounds': array of backgrounds witch will create
 *      key = class name for which we create set of backgrounds
 *      value = array of datas for creating background
 *          [0] = thumbnail slug
 *          [1] = media query for which we create background, if background for all resolutions we don't need it
 */
function setBackgroundImages($data)
{
    $post_id = $data['ID'];
    $string_pattern = "%s { background-image: url('%s');  }";

    foreach ($data['backgrounds'] as $stylename => $style) {

        foreach ($style as $item) {

            $args = [
                $stylename,
                get_the_post_thumbnail_url($post_id, $item[0])
            ];

            $style_string = vsprintf($string_pattern, $args);

            echo $style_string = (!empty($item[1])) ? "@media($item[1]){ $style_string }" : $style_string;


        }


    }
}


/**
 * not used method
 */
function autocomplite_styles() {
    wp_register_style( 'jquery-ui-styles','http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css' );
    wp_enqueue_style( 'jquery-ui-styles' );
}

/**
 * Print post date from now. for example: just now, 1 hour ago, 2 days ago etc.
 * @param $date
 * @return string
 */
function print_date_ago($date)
{
    $time_zone = new DateTimeZone(get_option('timezone_string')); // Get timezone from Wp settings

    $now = new DateTime();
    $now->setTimezone($time_zone);

    $post_date = new DateTime($date);
    $post_date->setTimezone($time_zone);

    $breakpoints = [
        'Y' => 'year',
        'm' => 'month',
        'd' => 'day',
        'H' => 'hour',
        'i' => 'min'
    ];

    $result        = 'just now';
    $week_name     = 'week';
    $result_string = '%d %s ago';

    foreach ($breakpoints as $breakpoint => $name) {

        if (($duration = $now->format($breakpoint) - $post_date->format($breakpoint)) > 0) {

            if ($breakpoint == 'd' ) {

                if ( $duration > 7) :
                    $duration = intval($duration / 7);
                    $name = $week_name;
                endif;
            }

            if ($duration > 1) $name .= 's';

            $result = sprintf($result_string, $duration, $name);

            break;
        }
    }

    return $result;

}


function showPaginate($page)
{
    $links = '';

    if ($page > 1) {
        $links = '<a href="/blog/page/'. ($page - 1) .'"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Previous</a>';
    }

    $nextPage = get_posts([
        'post_type' => 'post',
        'post_status' => 'publish',
        'orderby' => 'post_date',
        'order' => 'DESC',
        'posts_per_page' => 15,
        'paged' => $page + 1
    ]);

    if (count($nextPage)) {
        $links .= "\t". '<a href="/blog/page/'. ($page + 1) .'">Next <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>';
    }

    return $links;
}

function addStyle($name, $url)
{
    add_action( 'wp_enqueue_scripts', function() use ($name, $url){
        wp_enqueue_style( $name, get_template_directory_uri() . $url);
    } );
}

function addScript($name, $url)
{
    add_action( 'wp_enqueue_scripts', function() use ($name, $url){
        wp_enqueue_script( $name, get_template_directory_uri() . $url, array(), '1.0.0', true );
    } );
}

function imgpath($url)
{

    if( strpos( $_SERVER['HTTP_ACCEPT'], 'image/webp' ) !== false ) {

        $cdnPath = 'lendgeniuslandingstorage.azureedge.net';
        $webpRoot = '/wp-content/webp-express/webp-images/doc-root/';
        $parsedUrl = parse_url($url);

        if (!empty($parsedUrl['host']) && strpos($parsedUrl['host'], $cdnPath) !== false ) {

            $webp = str_replace('/wp-uploads/', '/wp-content/uploads/', $parsedUrl['path']);
        } else {

            $webp = $webpRoot . $parsedUrl['path'];
        }

        $webp .= '.webp';

        if (is_file(ABSPATH . $webp)) {
            $url = $webp;

            if (!empty($parsedUrl['query'])) {
                $url .= '?'. $parsedUrl['query'];
            }
        }

    }

    return $url;
}