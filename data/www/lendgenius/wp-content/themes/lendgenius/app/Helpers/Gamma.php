<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 08.09.17
 *
 */


add_action( 'wp_head', 'local_add_customizer_css' );
function local_add_customizer_css() {
    return false;
    $menu_height = et_get_option( 'menu_height', '66' );
    $fixed_menu_height = et_get_option( 'minimized_menu_height', '40' );
    $menu_height = $menu_height-20;
    $fixed_menu_height = $fixed_menu_height-20;

    $button_text_size = et_get_option( 'all_buttons_font_size', '20' );
    $button_text_color = et_get_option( 'all_buttons_text_color', '#ffffff' );
    $button_bg_color = et_get_option( 'all_buttons_bg_color', 'rgba(0,0,0,0)' );
    $button_border_width = et_get_option( 'all_buttons_border_width', '2' );
    $button_border_color = et_get_option( 'all_buttons_border_color', '#ffffff' );
    $button_border_radius = et_get_option( 'all_buttons_border_radius', '3' );
    $button_text_style = et_get_option( 'all_buttons_font_style', '', '', true );
    $button_icon = et_get_option( 'all_buttons_selected_icon', '5' );
    $button_spacing = et_get_option( 'all_buttons_spacing', '0' );
    $button_icon_color = et_get_option( 'all_buttons_icon_color', '#ffffff' );
    $button_text_color_hover = et_get_option( 'all_buttons_text_color_hover', '#ffffff' );
    $button_bg_color_hover = et_get_option( 'all_buttons_bg_color_hover', 'rgba(255,255,255,0.2)' );
    $button_border_color_hover = et_get_option( 'all_buttons_border_color_hover', 'rgba(0,0,0,0)' );
    $button_border_radius_hover = et_get_option( 'all_buttons_border_radius_hover', '3' );
    $button_spacing_hover = et_get_option( 'all_buttons_spacing_hover', '0' );
    $button_icon_size = 1.6 * intval( $button_text_size );
    $body_header_size = et_get_option( 'body_header_size', '30' );
    ?>
    <style>
        @media only screen and ( min-width: 981px ) {
            ol.h2 li { font-size: <?php echo esc_html( intval( $body_header_size * .86 ) ) ; ?>px; }

        <?php
 if ( '66' !== $menu_height ) { ?>
            .et_header_style_left #et-top-navigation, .et_header_style_split #et-top-navigation  { padding: <?php echo esc_html( round( $menu_height / 2 ) ); ?>px 0 0 0 !important; }
            .et_header_style_left #et-top-navigation nav > ul > li , .et_header_style_split #et-top-navigation nav > ul > li  { padding-bottom: <?php echo esc_html( round ( $menu_height / 2 ) ); ?>px !important; }
            .et_header_style_centered #main-header .logo_container { height: <?php echo esc_html( $menu_height ); ?>px !important; }
            .et_header_style_centered #top-menu > li  { padding-bottom: <?php echo esc_html( round ( $menu_height * .18 ) ); ?>px !important; }
            .et_header_style_split .centered-inline-logo-wrap { width: <?php echo esc_html( $menu_height ); ?>px !important; margin: -<?php echo esc_html( $menu_height ); ?>px 0 !important; }
            .et_header_style_split .centered-inline-logo-wrap #logo { max-height: <?php echo esc_html( $menu_height ); ?>px !important; }
        <?php } ?>
        <?php if ( '40' !== $fixed_menu_height ) { ?>
            .et_header_style_left .et-fixed-header #et-top-navigation, .et_header_style_split .et-fixed-header #et-top-navigation { padding: <?php echo esc_html( intval( round( $fixed_menu_height / 2 ) ) ); ?>px 0 0 0 !important; }
            .et_header_style_left .et-fixed-header #et-top-navigation nav > ul > li , .et_header_style_split .et-fixed-header #et-top-navigation nav > ul > li   { padding-bottom: <?php echo esc_html( round( $fixed_menu_height / 2 ) ); ?>px !important; }
            .et_header_style_centered #main-header.et-fixed-header .logo_container { height: <?php echo esc_html( $fixed_menu_height ); ?>px !important; }
            .et_header_style_split .et-fixed-header .centered-inline-logo-wrap { width: <?php echo esc_html( $fixed_menu_height ); ?>px !important; margin: -<?php echo esc_html( $fixed_menu_height ); ?>px 0 !important;  }
            .et_header_style_split .et-fixed-header .centered-inline-logo-wrap #logo { max-height: <?php echo esc_html( $fixed_menu_height ); ?>px !important; }
        <?php } ?>
        }

        <?php if ( '20' !== $button_text_size || '#ffffff' !== $button_text_color || 'rgba(0,0,0,0)' !== $button_bg_color || '2' !== $button_border_width || '#ffffff' !== $button_border_color || '3' !== $button_border_radius || '' !== $button_text_style || '0' !== $button_spacing ) { ?>
        body .gform_button.button
        {
        <?php if ( '20' !== $button_text_size ) { ?>
            font-size: <?php echo esc_html( $button_text_size ); ?>px;
        <?php } ?>
        <?php if ( 'rgba(0,0,0,0)' !== $button_bg_color ) { ?>
            background: <?php echo esc_html( $button_bg_color ); ?>;
        <?php } ?>
        <?php if ( '2' !== $button_border_width ) { ?>
            border-width: <?php echo esc_html( $button_border_width ); ?>px !important;
        <?php } ?>
        <?php if ( '#ffffff' !== $button_border_color ) { ?>
            border-color: <?php echo esc_html( $button_border_color ); ?>;
        <?php } ?>
        <?php if ( '3' !== $button_border_radius ) { ?>
            border-radius: <?php echo esc_html( $button_border_radius ); ?>px;
        <?php } ?>
        <?php if ( '' !== $button_text_style ) { ?>
        <?php echo esc_html( et_pb_print_font_style( $button_text_style ) ); ?>;
        <?php } ?>
        <?php if ( '0' !== $button_spacing ) { ?>
            letter-spacing: <?php echo esc_html( $button_spacing ); ?>px;
        <?php } ?>
        }
        body.et_pb_button_helper_class .gform_button.button {
        <?php if ( '#ffffff' !== $button_text_color ) { ?>
            color: <?php echo esc_html( $button_text_color ); ?>;
        <?php } ?>
        }
        <?php } ?>
        <?php if ( '5' !== $button_icon || '#ffffff' !== $button_icon_color || '20' !== $button_text_size ) { ?>
        body .gform_button.button:after
        {
        <?php if ( '5' !== $button_icon ) { ?>
        <?php if ( "'" === $button_icon ) { ?>
            content: "<?php echo htmlspecialchars_decode( $button_icon ); ?>";
        <?php } else { ?>
            content: '<?php echo htmlspecialchars_decode( $button_icon ); ?>';
        <?php } ?>
            font-size: <?php echo esc_html( $button_text_size ); ?>px;
        <?php } else { ?>
            font-size: <?php echo esc_html( $button_icon_size ); ?>px;
        <?php } ?>
        <?php if ( '#ffffff' !== $button_icon_color ) { ?>
            color: <?php echo esc_html( $button_icon_color ); ?>;
        <?php } ?>
        }
        <?php } ?>
        <?php if ( '#ffffff' !== $button_text_color_hover || 'rgba(255,255,255,0.2)' !== $button_bg_color_hover || 'rgba(0,0,0,0)' !== $button_border_color_hover || '3' !== $button_border_radius_hover || '0' !== $button_spacing_hover ) { ?>
        body .gform_button.button:hover
        {
        <?php if ( '#ffffff' !== $button_text_color_hover ) { ?>
            color: <?php echo esc_html( $button_text_color_hover ); ?> !important;
        <?php } ?>
        <?php if ( 'rgba(255,255,255,0.2)' !== $button_bg_color_hover ) { ?>
            background: <?php echo esc_html( $button_bg_color_hover ); ?> !important;
        <?php } ?>
        <?php if ( 'rgba(0,0,0,0)' !== $button_border_color_hover ) { ?>
            border-color: <?php echo esc_html( $button_border_color_hover ); ?> !important;
        <?php } ?>
        <?php if ( '3' !== $button_border_radius_hover ) { ?>
            border-radius: <?php echo esc_html( $button_border_radius_hover ); ?>px;
        <?php } ?>
        <?php if ( '0' !== $button_spacing_hover ) { ?>
            letter-spacing: <?php echo esc_html( $button_spacing_hover ); ?>px;
        <?php } ?>
        }
        <?php } ?>
    </style>
<?php }

/* REDIRECTS ATACHMENT PAGES TO THEIR PARENTS */
//add_action('template_redirect', 'gamma_attachment_redirect',1);
function gamma_attachment_redirect() {
    global $post;
    if ( is_attachment() && isset($post->post_parent) && is_numeric($post->post_parent) && ($post->post_parent != 0) ) {
        wp_redirect(get_permalink($post->post_parent), 301); // permanent redirect to post/page where image or document was uploaded
        exit;
    } elseif ( is_attachment() && isset($post->post_parent) && is_numeric($post->post_parent) && ($post->post_parent < 1) ) {   // for some reason it doesnt works checking for 0, so checking lower than 1 instead...
        wp_redirect(get_bloginfo('wpurl'), 302); // temp redirect to home for image or document not associated to any post/page
        exit;
    }
}

/* ADD BUILDER TO POSTS */
function gamma_builder_post_types($post_types = array()){
    if(!in_array('post',$post_types)) $post_types[] = 'post';
    return $post_types;
}
//add_filter('et_pb_builder_post_types','gamma_builder_post_types',10,1);

/* ADD SHORTCODE VIEWER TO BUILDER */
function gamma_before_main_editor( $post ) {
    if ( ! in_array( $post->post_type, array( 'post' ) ) ) return;
    $is_builder_used = 'on' === get_post_meta( $post->ID, '_et_pb_use_builder', true ) ? true : false;
    printf( '<a href="#" id="et_pb_toggle_builder" data-builder="%2$s" data-editor="%3$s" class="button button-primary button-large%5$s">%1$s</a><div id="et_pb_main_editor_wrap"%4$s>',
        ( $is_builder_used ? __( 'Use Default Editor', 'Divi' ) : __( 'Use Page Builder', 'Divi' ) ),
        __( 'Use Page Builder', 'Divi' ),
        __( 'Use Default Editor', 'Divi' ),
        ( $is_builder_used ? ' class="et_pb_hidden"' : '' ),
        ( $is_builder_used ? ' et_pb_builder_is_used' : '' )
    );
}
//add_action( 'edit_form_after_title', 'gamma_before_main_editor' );

function gamma_before_page_builder(){
    echo '
		<style>
			#gamma_layout_controls {
				padding-top:20px;
			}
			#gamma_layout_controls .gamma-layout-buttons-source:before {
			  content: "\50";
			}
			#gamma_layout_content_wrapper {
				display:none;
			}
			#TB_ajaxContent{
				width: 100% !important;
  				box-sizing: border-box;
			}
			.processing:before {
				opacity: 0;
				filter: alpha(opacity=0);
				visibility: hidden;
			}
			#gamma_layout_controls .spinner {
				position: absolute;
			}
			.processing .spinner{
				  visibility: visible;
				  opacity: 1;
				  filter: alpha(opacity=100);
				  position: absolute;
				  left: 10px;
				  top: 15px;
			}
		</style>
		<script type="text/javascript">
			jQuery(window).load( function() {
				
				jQuery("body").on("click", "#gamma-layout-buttons-source", function (e) {
					if(!jQuery(this).hasClass("processing")){
						jQuery(this).addClass("processing");
						var data = {
							"action": "gamma_get_shortcodes",
							"post_content": jQuery("#wp-content-editor-container .wp-editor-area").html() //tinyMCE.editors.content.getContent()
						};
						jQuery.post(ajaxurl, data, function(response) {
							jQuery("#gamma_layout_content").html(response.replace(/\\\/g,""));
							tb_show("View Shortcodes", "#TB_inline?inlineId=gamma_layout_content_wrapper");
							jQuery("#gamma-layout-buttons-source").removeClass("processing");
						});
					}
					
				});
				
			});
		</script>
		<div id="gamma_layout_controls">
			<a href="javascript:void(0);" id="gamma-layout-buttons-source" class="et-pb-layout-buttons gamma-layout-buttons-source"><span>View Shortcodes</span><div class="spinner"></div></a>
			<div id="gamma_layout_content_wrapper"><p><div id="gamma_layout_content"></div></p></div>
		</div>';
}
add_action( 'et_pb_before_page_builder', 'gamma_before_page_builder');

function gamma_get_shortcodes_callback() {
    $shortcodes  = gamma_find_shortcodes($_POST['post_content'],0);
    if(strlen($shortcodes)==0) echo 'No ShortCodes Found';
    echo $shortcodes;
    wp_die(); // this is required to terminate immediately and return a proper response
}
add_action( 'wp_ajax_gamma_get_shortcodes', 'gamma_get_shortcodes_callback' );
function gamma_find_shortcodes($content,$level) {
    $pattern = get_shortcode_regex();
    $result = '';
    $new_line = '<br /><br />';
    $level_offset = str_repeat("&nbsp; &nbsp; &nbsp; &nbsp;",$level);
    if (   preg_match_all( '/'. $pattern .'/s', $content, $matches, PREG_PATTERN_ORDER ) )
    {

        foreach($matches[2] as $key=>$shortode){
            $result .= $level_offset.'['.$shortode.((strlen($matches[3][$key]))?' '.$matches[3][$key]:'').']'.$new_line;
            if(strlen($matches[5][$key])>0){
                $result .= gamma_find_shortcodes($matches[5][$key],$level+1);
            }
            $closing_tag = '[/'.$shortode.']';
            if(strpos($content,$closing_tag)!==false){
                $result .= $level_offset.$closing_tag.$new_line;
            }
        }
        return $result;
    }else{
        return $level_offset.'...'.$new_line;
    }
}

//add_action( 'init', 'gamma_divi_init_builder', 9999 );
function gamma_divi_init_builder() {
    global $pagenow;

    $is_admin = is_admin();
    if($is_admin) return;
    $action_hook = $is_admin ? 'wp_loaded' : 'wp';
    $required_admin_pages = array( 'edit.php', 'post.php', 'post-new.php', 'admin.php', 'customize.php', 'edit-tags.php', 'admin-ajax.php' ); // list of admin pages where we need to load builder files
    $specific_filter_pages = array( 'edit.php', 'admin.php', 'edit-tags.php' ); // list of admin pages where we need more specific filtering

    $is_edit_library_page = 'edit.php' === $pagenow && isset( $_GET['post_type'] ) && 'et_pb_layout' === $_GET['post_type'];
    $is_role_editor_page = 'admin.php' === $pagenow && isset( $_GET['page'] ) && 'et_divi_role_editor' === $_GET['page'];
    $is_import_page = 'admin.php' === $pagenow && isset( $_GET['import'] ) && 'wordpress' === $_GET['import']; // Page Builder files should be loaded on import page as well to register the et_pb_layout post type properly
    $is_edit_layout_category_page = 'edit-tags.php' === $pagenow && isset( $_GET['taxonomy'] ) && 'layout_category' === $_GET['taxonomy'];


    // load builder files on front-end and on specific admin pages only.
    if ( ! $is_admin || ( $is_admin && in_array( $pagenow, $required_admin_pages ) && ( ! in_array( $pagenow, $specific_filter_pages ) || $is_edit_library_page || $is_role_editor_page || $is_edit_layout_category_page || $is_import_page ) ) ) {

        add_action( $action_hook, 'gamma_builder_add_main_elements',9999 );
    }
}
function gamma_builder_add_main_elements(){
    //return;
    require '../../includes/simple_html_dom.php';
    require '../../includes/builder/extension.php';
}


/*add_action( 'init', 'gamma_setup_builder' );
function gamma_setup_builder(){
	$action_hook = is_admin() ? 'wp_loaded' : 'wp';
	add_action( $action_hook, 'gamma_builder_add_main_elements',9999 );
}*/
