<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 12.09.17
 *
 */

/**
 * Add duplicate functionality if duplicate_post_link was clicked
 */
if ( ( isset($_REQUEST['action']) && 'rd_duplicate_post_as_draft' == $_REQUEST['action'] ) ) {

//    require_once LG_THEME_ROOT.'app/classes/Duplicate/Duplicate.php';
    $post_id = (isset($_GET['post']) ? absint( $_GET['post'] ) : absint( $_POST['post'] ) );
    $duplicate = new \App\Classes\Duplicate\Duplicate($post_id);
    $duplicate->make();
}

/**
 * Sync authors in database-sync plugin in main-screen.php
 * usage commented at the moment here: wp-content/plugins/database-sync/main-screen.php:53
 */
function sync_authors(){
    $exists_users = get_users();
    $exists_user_names = array();
    $exists_authors = array();
    $authors = array();
    foreach($exists_users as $exists_user){
        $exists_user_names[$exists_user->display_name] = $exists_user;
    }

    $loop_ae = new WP_Query(array(
        'post_type' => 'authors',
        'posts_per_page' => -1
    ));
    while ($loop_ae -> have_posts()) : $loop_ae -> the_post();
        $exists_authors []= get_the_title();
    endwhile;
    wp_reset_postdata();

    foreach($exists_user_names as $key=>$exists_user_name){
        if(!in_array($key, $exists_authors)){
            $author_post = array(
                'post_title'    => wp_strip_all_tags( $key ),
                'post_content'  => get_the_author_meta('description', $exists_user_name->ID),
                'post_status'   => 'publish',
                'post_author'   => get_current_user_id(),
                'post_type' => 'authors'
            );

            // Insert the post into the database
            if(wp_insert_post( $author_post )){
                $exists_authors []= $key;
            }
        }
    }

    $loop_a = new WP_Query(array(
        'post_type' => 'authors',
        'posts_per_page' => -1
    ));
    while ($loop_a -> have_posts()) : $loop_a -> the_post();
        $authors[get_the_title()] = get_the_ID();
    endwhile;
    wp_reset_postdata();

    $loop = new WP_Query(array(
        'post_type' => 'post',
        'posts_per_page' => -1
    ));

    while ($loop -> have_posts()) : $loop -> the_post();
        $id = get_the_ID();
        $post_author_name = get_the_author();
        if(array_key_exists($post_author_name, $authors)) {
            $author = $authors[$post_author_name];
            if (get_post_meta($id, 'author', true)) {
                update_post_meta($id, 'author', $author);
            } else {
                add_post_meta($id, 'author', $author);
            }
        }
    endwhile;
    wp_reset_postdata();
}