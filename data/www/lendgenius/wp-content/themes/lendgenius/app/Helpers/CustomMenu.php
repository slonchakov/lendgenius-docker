<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 12.09.17
 *
 */

function custom_header_menu($menu_name='primary-menu') {

//    if (($locations = get_nav_menu_locations()) && isset($locations[$menu_name])) {
    $locations = get_nav_menu_locations();
    if (true) {

        if (($locations = get_nav_menu_locations()) && isset($locations[$menu_name])) {
            $menu = wp_get_nav_menu_object($locations[$menu_name]);
        } else {
            $menu = wp_get_nav_menu_object(2);
        }
//        $menu = wp_get_nav_menu_object($locations[$menu_name]) ?? wp_get_nav_menu_object(6);
//        var_dump($menu);
        $menu_items = wp_get_nav_menu_items($menu->term_id);

        $menu_list = '<div class="menu-header-menu-container">' ."\n";
        $menu_list .= "\t\t\t\t". '<ul id="menu-header-menu" class="list-inline">' ."\n";

        $count = 0;
        $submenu = false;
        $sub_submenu = false;
        $parent_id = $sub_parent_id = 999;

        foreach ((array) $menu_items as $key => $menu_item) {

            $link = $menu_item->url;
            $title = $menu_item->title;

            if ( !$menu_item->menu_item_parent ) {
                $parent_id = $menu_item->ID;
                $menu_list .= '<li class="menu-item menu-item-type-post_type menu-item-object-page '.(( isset($menu_items[$key+1]) && $parent_id == $menu_items[$key+1]->menu_item_parent )? 'menu-item-has-children':'').'" >' ."\n";
                $menu_list .= '<a href="'.$link.'" >'.$title.'</a>' ."\n";
                if ( $menu_items[ $count + 1 ]->menu_item_parent == $parent_id){
                    $menu_list .= '<div class="sub-menu col-2"><div class="col-holder">' . "\n";
                }
            }
            elseif ( $parent_id == $menu_item->menu_item_parent ) {
                $sub_parent_id = $menu_item->ID;
                $submenu = true;
                if($menu_items[ $count + 1 ]->menu_item_parent == $sub_parent_id) {
                    $menu_list .= '<div class="col"><div class="title-col"><a href="' . $link . '" class="title">' . $title . '</a></div>' . "\n";
                }else{
                    $menu_list .= '<ul>' . "\n";
                    $menu_list .= '<li class="item">' . "\n";
                    $menu_list .= '<a href="' . $link . '" class="title">' . $title . '</a>' . "\n";
                    $menu_list .= '</li>' . "\n";
                    $menu_list .= '</ul>' . "\n";
                }
            }
            elseif ( $sub_parent_id == $menu_item->menu_item_parent) {
                if (!$sub_submenu) {
                    $sub_submenu = true;
                    $menu_list .= '<ul>' . "\n";
                }
                $menu_list .= '<li class="item">' . "\n";
                $menu_list .= '<a href="' . $link . '" class="title">' . $title . '</a>' . "\n";
                $menu_list .= '</li>' . "\n";

                if ( $menu_items[ $count + 1 ]->menu_item_parent != $sub_parent_id && $sub_submenu) {
                    $menu_list .= '</ul>' . "\n";
                    $menu_list .= '</div>' . "\n";
                    $sub_submenu = false;
                }
            }

            if ($menu_items[ $count + 1 ]->menu_item_parent != $parent_id && $menu_items[ $count + 1 ]->menu_item_parent != $sub_parent_id  && !$sub_submenu) {
                if ( !$menu_items[ $count + 1 ]->menu_item_parent  && $submenu) {
                    $menu_list .= '</div></div>' . "\n";
                }
                $menu_list .= '</li>' ."\n";
                $submenu = false;
            }

            $count++;
        }
        $menu_list .= "\t\t\t\t". '</ul>' ."\n";
        $menu_list .= "\t\t\t". '</div>' ."\n";
    } else {
        // $menu_list = '<!-- no list defined -->';
    }
    echo $menu_list;
}

function custom_header_mobile_menu($menu_id=6) {
//    if (($locations = get_nav_menu_locations()) && isset($locations[$menu_name])) {
    $menu = wp_get_nav_menu_object($menu_id);
    $menu_items = wp_get_nav_menu_items($menu->term_id);

    $menu_list = '<div class="menu-main-menu-container container">' ."\n";
    $menu_list .= "\t\t\t\t". '<ul id="menu-main-menu" class="menu">' ."\n";

    $count = 0;
    $submenu = false;
    $sub_submenu = false;
    $parent_id = $sub_parent_id = 999;

    foreach ((array) $menu_items as $key => $menu_item) {
        $link = $menu_item->url;
        $title = $menu_item->title;

        if ( !$menu_item->menu_item_parent ) {
            $parent_id = $menu_item->ID;
            $menu_list .= '<li class="menu-item menu-item-type-post_type menu-item-object-page '.(( isset($menu_items[$key+1]) && $parent_id == $menu_items[$key+1]->menu_item_parent )? 'menu-item-has-children':'').'" >' ."\n";
            $menu_list .= '<a href="'.$link.'" >'.$title.'</a>' ."\n";
            if ( $menu_items[ $count + 1 ]->menu_item_parent == $parent_id){
                $menu_list .= '<div class="sub-menu col-2"><div class="col-holder">' . "\n";
            }
        }
        elseif ( $parent_id == $menu_item->menu_item_parent ) {
            $sub_parent_id = $menu_item->ID;
            $submenu = true;
            if($menu_items[ $count + 1 ]->menu_item_parent == $sub_parent_id) {
                $menu_list .= '<div class="col"><div class="title-col"><a href="' . $link . '" class="title">' . $title . '</a></div>' . "\n";
            }else{
                $menu_list .= '<ul>' . "\n";
                $menu_list .= '<li class="item">' . "\n";
                $menu_list .= '<a href="' . $link . '" class="title">' . $title . '</a>' . "\n";
                $menu_list .= '</li>' . "\n";
                $menu_list .= '</ul>' . "\n";
            }
        }
        elseif ( $sub_parent_id == $menu_item->menu_item_parent) {
            if (!$sub_submenu) {
                $sub_submenu = true;
                $menu_list .= '<ul>' . "\n";
            }
            $menu_list .= '<li class="item">' . "\n";
            $menu_list .= '<a href="' . $link . '" class="title">' . $title . '</a>' . "\n";
            $menu_list .= '</li>' . "\n";

            if ($menu_items[ $count + 1 ]->menu_item_parent != $sub_parent_id && $sub_submenu) {
                $menu_list .= '</ul>' . "\n";
                $menu_list .= '</div>' . "\n";
                $sub_submenu = false;
            }
        }

        if ( $menu_items[ $count + 1 ]->menu_item_parent != $parent_id && $menu_items[ $count + 1 ]->menu_item_parent != $sub_parent_id  && !$sub_submenu) {
            if ( !$menu_items[ $count + 1 ]->menu_item_parent  && $submenu) {
                $menu_list .= '</div></div>' . "\n";
            }
            $menu_list .= '</li>' ."\n";
            $submenu = false;
        }

        $count++;
    }
    $menu_list .= "\t\t\t\t". '</ul>' ."\n";
    $menu_list .= "\t\t\t". '</div>' ."\n";
//    } else {
//        // $menu_list = '<!-- no list defined -->';
//    }
    echo $menu_list;
}
