<?php

namespace App\Classes\Register;
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 20.10.17
 *
 */
// cummon actions and filters
Register::$list['action'] = [
    'ImageSizes', // Configuring additional image sizes
    'MorePosts', // Button for adding blog posts with ajax
    'QuickSearch', // Ajax search in header
    'RegisterCustomMenus', // Additional menus
    'Remover', // Remove default actions and filters
    'TrackPostViews', // Counter for post views
    'WidgetRegister', // Action for registering all widgets
    'CPTRegister'
];

Register::$list['filter'] = [
    'Azure', // Filter for Azure Storage plugin
    'ReturnFalse', // All filters which must return false adding here
    'ReturnTrue', // All filters which must return true adding here
    'TablePress', // Filters for table press plugin
    'Title', // Filter for the_title
    'Versions', // Filter for css and js versions
    'IncludeTemplate',
    'BlogTitle',
    'FrontPageTemplate'
];


if (is_admin()) {// actions and filters just for back-end
    array_push(Register::$list['action'],
        'AdminEditForm', // Add changes to post edit form
        'AdminFooterPost', // Add scripts to the footer of post edit form
        'AdminMenuItems', // Add menu items to admin menu
        'AdminPostColumns', // Add custom columns to admin
        'CPTAuthor',
        'AdminLenderFilters',
        'AdminChooseFundingsMetaBox',
        'AdminAddSvg'
    );
    array_push(Register::$list['filter'],
        'Admin', // All filters for admin part
        'CustomColumns', // Adding custom columns to admin pages
        'CPTAuthor', // Custom post type for authors
        'AdminTemplateDropdown' // filer dropdown of templates at page's admin page,
    );
} else {// actions and filters just for front-end
    array_push(Register::$list['action'],
        'ToolbarLinks', // Add links to top toolbar on front
        'Queue' // Filters for queue of scripts and css
    );
    Register::$list['filter'][] = 'Css'; // Filter for adding css classes
}