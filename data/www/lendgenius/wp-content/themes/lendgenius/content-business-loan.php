<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 17.10.16
 * Time: 13:29
 */


$post_id                    = get_the_ID();
$loan_icon                  = get_post_meta( $post_id, 'loan_icon', true );
$item_1_title               = get_post_meta( $post_id, 'item_1_title', true );
$item_1_description         = get_post_meta( $post_id, 'item_1_description', true );
$item_2_title               = get_post_meta( $post_id, 'item_2_title', true );
$item_2_description         = get_post_meta( $post_id, 'item_2_description', true );
$item_3_title               = get_post_meta( $post_id, 'item_3_title', true );
$item_3_description         = get_post_meta( $post_id, 'item_3_description', true );
$item_4_title               = get_post_meta( $post_id, 'item_4_title', true );
$item_4_description         = get_post_meta( $post_id, 'item_4_description', true );
$description_for_loans_page = get_post_meta( $post_id, 'description_for_business_loans_page', true);
?>

<div class="flex flex-column-xs line">
    <div class="image-holder">
        <div class="image">
            <i class="home-sprite <?php echo $loan_icon; ?>"></i>
        </div>
    </div>
    <div class="content">
        <div class="line-success">
            <h4><?php the_title(); ?></h4>
<!--                <p>--><?php //the_excerpt(); ?><!--</p>-->

            <p><?= $description_for_loans_page; ?></p>
        </div>
        <a href="<?php echo get_permalink(); ?>" class="learn_more">Learn More <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
    </div>
    <div class="small_grid">
        <div class="col flex">
            <div class="image">
                <i class="ico-gradient icomoon-dollar-small"></i>
            </div>
            <div class="content">
                <div><b><?php echo $item_1_title; ?></b></div>
                <span><?php echo $item_1_description; ?></span>
            </div>
        </div>
        <div class="col flex">
            <div class="image">
                <i class="ico-gradient icomoon-percent-small"></i>
            </div>
            <div class="content">
                <div><b><?php echo $item_3_title; ?></b></div>
                <span><?php echo $item_3_description; ?></span>
            </div>
        </div>
        <div class="col flex">
            <div class="image">
                <i class="ico-gradient icomoon-calendar-small"></i>
            </div>
            <div class="content">
                <div><b><?php echo $item_2_title; ?></b></div>
                <span><?php echo $item_2_description; ?></span>
            </div>
        </div>
        <div class="col flex">
            <div class="image">
                <i class="ico-gradient icomoon-clock-small"></i>
            </div>
            <div class="content">
                <div><b><?php echo $item_4_title; ?></b></div>
                <span><?php echo $item_4_description; ?></span>
            </div>
        </div>
    </div>
</div>