<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="google-site-verification" content="0vHphrtA59XVVa8JNf9MU2PtfnAMp1UfQTKudNEurAc" />
    <link href="https://plus.google.com/114339560599887475172" rel="publisher" />
    <meta name="msvalidate.01" content="D07588DA24884897E4C89F3F32048F38" />

    <link rel="preconnect" href="https://addshoppers.s3.amazonaws.com">
    <link rel="preconnect" href="https://fl.adpxl.co">
    <link rel="preconnect" href="https://ip.freshmarketer.com">
    <link rel="preconnect" href="https://dx.steelhousemedia.com">
    <link rel="preconnect" href="https://px.steelhousemedia.com">

    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

    <!--[if lt IE 9]>
    <script src="<?php echo esc_url( get_template_directory_uri() . '/js/html5.js' ); ?>" type="text/javascript"></script>
    <![endif]-->

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />

    <title><?php wp_title( '|', true, 'right' ); ?></title>

    <?php wp_head(); ?>

    <link rel="SHORTCUT ICON" href="/favicon.ico">

    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/assets/styles/icomoon.min.css?ver=<?php echo get_option( 'sts_script_version' ); ?>">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/styles/style-installment/styles.min.css?ver=<?php echo get_option( 'sts_script_version' ); ?>">

    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "Organization",
            "name": "LendGenius",
            "url": "https://www.lendgenius.com",
            "logo": "https://lendgeniuslandingstorage.azureedge.net/wp-uploads/2017/04/lendgenius-thumbnail.png",
            "sameAs": [
                "https://twitter.com/lendgenius",
                "https://www.facebook.com/lendgenius"
            ]
        }
    </script>

</head>

<body <?php body_class('home-installment'); ?>>

<header id="header" class="clearfix home-installment">
    <div class="container">
        <div id="logo" class="pull-left"><a href="/"><span>Lend</span><span>Genius</span><sup>TM</sup></a> <img src="<?php echo get_stylesheet_directory_uri() ?>/styles/style-installment/logo-tv.svg" alt="as seen on TV"></div>

        <div class="pull-right hidden">
            <div class="nav-menu">
                <div class="nav-button">
                    <span>Menu</span>
                    <div class="humburger">
                        <div class="line"></div>
                        <div class="line"></div>
                        <div class="line"></div>
                    </div>
                </div>

                <?php wp_nav_menu(['menu' => 'Right menu']); ?>

            </div>
        </div>
    </div>
</header>