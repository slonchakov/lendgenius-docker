<?php
/*
Template Name: Blank Iframe
*/
get_header();

$iframe_url = get_post_meta( get_the_ID(), 'iframe_url', true );
?>
<style>
    html, body {
        overflow-x: hidden;
        overflow-y: hidden;
        height: 100%;
    }
</style>
<iframe src="<?php echo $iframe_url; ?>" width=100% height=100% style="border:0"></iframe>
<?php
get_footer();
?>
