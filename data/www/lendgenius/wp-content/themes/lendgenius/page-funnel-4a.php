<?php
/*
    Template Name: Funnel-4a
*/
get_header();
set_fields_for_loan();
?>

    <div class="page-funnel-3">
        <div class="page-wraper">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-6">
                        <div class="funnel-form">
                            <div class="funnel-form-steps">
                                <div class="steps-indication">
                                    <div class="step-title">2/2</div>
                                </div>
                                <div class="step-2">
                                    <div class="content-refine-results tpl-info">
                                        <div class="content-wrap">
                                            <form method="post" id="loan_form">
                                                <input type="hidden" name="action" value="loan_form">
                                                <input type="hidden" name="loan_amount" value="<?php echo $_SESSION['loan_data']['loan_amount']; ?>">
                                                <input type="hidden" name="gross_sales" value="<?php echo $_SESSION['loan_data']['gross_sales']; ?>">
                                                <input type="hidden" name="time_in_business" value="<?php echo $_SESSION['loan_data']['time_in_business']; ?>">
                                                <input type="hidden" name="business_name" value="<?php echo $_SESSION['loan_data']['business_name']; ?>">
                                                <input type="hidden" name="industry_description" value="<?php echo $_SESSION['loan_data']['industry_description']; ?>">
                                                <input type="hidden" name="business_address" value="<?php echo $_SESSION['loan_data']['business_address']; ?>">
                                                <input type="hidden" name="zip_code" value="<?php echo $_SESSION['loan_data']['zip_code']; ?>">
                                                <div class="refine-results-items">
                                                    <div class="item">
                                                        <div class="item-field">
                                                            <input class="field-input field-input-biger" id="FirstName" placeholder="First name" type="text" name="first_name">
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="item-field">
                                                            <input class="field-input field-input-biger" id="LastName" placeholder="Last name" type="text" name="last_name">
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="item-field">
                                                            <input class="field-input field-input-biger" id="PhoneHome" placeholder="Primary phone" type="tel" name="phone">
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="item-field">
                                                            <input class="field-input field-input-biger" id="Email" placeholder="Email" type="email" name="email">
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="item-field item-field-select">
                                                            <select class="field-styler-option field-input-biger" id="Credit" name="credit">
                                                                <option value="Please choose" disabled selected>Credit score</option>
                                                                <option value="EXCELLENT">Excellent Credit (760+)</option>
                                                                <option value="VERYGOOD">Very Good Credit (720-759)</option>
                                                                <option value="GOOD">Good Credit (660-719)</option>
                                                                <option value="FAIR">Fair Credit (600-659)</option>
                                                                <option value="POOR">Poor Credit (580-599)</option>
                                                                <option value="VERYPOOR">Very Poor Credit (500+)</option>
                                                                <option value="UNSURE">Not Sure</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="disclaimer">
                                                    <p>By clicking “Submit”, I confirm I have reviewed and agree to the <a href="/terms-of-service/" target="_blank">Terms of Service</a>, and I further agree that my click is my electronic signature, and I authorize LendGenius and its participating lenders to contact me using the information I provided above, including via email, phone, text message (SMS/MMS) and/or cell phone (including use of automated dialing equipment and pre-recorded calls and/or messages), in order to send me information about products, services and offers from LendGenius or its participating lenders. I understand that I have no obligation to accept a loan once connected with a lender. I further understand that this consent is not required to obtain a loan, and that I may revoke this consent at any time by contacting LendGenius at support@lendgenius.com.
                                                        LendGenius does not charge anything for borrowers to use it’s loan matching service. Generally, LendGenius only makes money from the lenders once a loan has been issued. </p>
                                                </div>
                                                <div class="refine-results-footer">
                                                    <button class="btn btn-primary" type="submit">Continue</button>
                                                    <a href="/get-started/" class="refine-results-back">Back</a>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="page-wraper-bg">
                <div class="funnel-info">
                    <div class="funnel-info-body">
                        <h3 class="item-title">Commonly Asked Questions</h3>
                        <?php
                        // Start the loop.
                        while ( have_posts() ) : the_post();

                            the_content();

                        endwhile; ?>
                    </div>
                    <div class="funnel-info-footer">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" >
        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }
        jQuery(document).ready(function($) {
            ga('send', {
                hitType: 'event',
                eventCategory: 'form-1',
                eventAction: 'step2',
                eventLabel: ''
            });
            jQuery('[name="phone"]').mask('000-000-0000');
            jQuery( "[name=\"first_name\"], [name=\"last_name\"]").keypress(function(event) {
                if( !/^[A-Za-z]$/.test(event.key) ){
                    event.preventDefault();
                }
            });
            jQuery( "[name=\"email\"], [name=\"first_name\"], [name=\"last_name\"], [name=\"phone\"], [name=\"credit\"]" ).focusout(function() {
                validate_funnel4(this);
            });
            jQuery( "[name=\"credit\"]" ).change(function() {
                jQuery(this).parent().find('.error-message').remove();
                if(this.value == '' || this.value == 'Please choose'){
                    jQuery(this).removeClass('success');
                    jQuery(this).addClass('error');
                    jQuery(this).parent().find('.jq-selectbox__dropdown').after('<span class="error-message">Please, select your credit score.</span>');
                }else{
                    jQuery(this).removeClass('error');
                    jQuery(this).addClass('success');
                }
            });

            function validate_funnel4(elem){
                jQuery(elem).parent().find('.error-message').remove();
                if(elem.value == '' || elem.value == 'Please choose' || (!validateEmail(elem.value) && elem.name == 'email')){
                    jQuery(elem).removeClass('success');
                    jQuery(elem).addClass('error');
                    if(elem.name == 'email'){
                        jQuery(elem).after('<span class="error-message">Please, insert a valid email address.</span>');
                    }else if(elem.name == 'phone'){
                        jQuery(elem).after('<span class="error-message">Please, insert a valid phone number.</span>');
                    }else if(elem.name == 'credit'){
                        jQuery(elem).parent().find('.jq-selectbox__dropdown').after('<span class="error-message">Please, select your credit score.</span>');
                    }else{
                        jQuery(elem).after('<span class="error-message">This is a required field.</span>');
                    }
                }else{
                    jQuery(elem).removeClass('error');
                    jQuery(elem).addClass('success');
                }
            }
            jQuery('#loan_form').on( "submit", function( event ) {
                event.preventDefault();
                var first_name = jQuery('[name="first_name"]').val(),
                    last_name = jQuery('[name="last_name"]').val(),
                    phone = jQuery('[name="phone"]').val(),
                    email = jQuery('[name="email"]').val(),
//                    disclaimer = jQuery('[name="disclaimer"]').is(":checked"),
                    credit = jQuery('[name="credit"]').val();

                //validation
                if(first_name == '' || last_name == '' || phone == '' ||
                    email == '' || !validateEmail(email) || credit == null){
                    jQuery( "[name=\"email\"], [name=\"first_name\"], [name=\"last_name\"], [name=\"phone\"], [name=\"credit\"]" ).each(function() {
                        validate_funnel4(this);
                    });
                    jQuery("html, body").animate({scrollTop: $('.error').first().offset().top-120}, "slow");
                    return;
                }
                ga('send', {
                    hitType: 'event',
                    eventCategory: 'form-1',
                    eventAction: 'submitted',
                    eventLabel: ''
                });
                jQuery.post( "<?php echo admin_url('admin-ajax.php'); ?>", jQuery(this).serialize(), function(response) {
                    obj = jQuery.parseJSON(response);
                    if(obj.RedirectURL) {
                        ga('send', {
                            hitType: 'event',
                            eventCategory: 'form-1',
                            eventAction: 'result',
                            eventLabel: obj.Result.toLowerCase()
                        });
                        document.location.href = obj.RedirectURL;
                    } else {
                        if(obj.Errors) {
                            arr_err = []
                            jQuery.each(obj.Errors, function( index, value ) {
                                if ( $.isArray(value) ) {
                                    jQuery.each(value, function( index_in, value_val ) {
                                        jQuery('#' + value_val.Field).removeClass('success');
                                        jQuery('#' + value_val.Field).addClass('error');
                                        arr_err.push(value_val.Field);
                                    });
                                } else {
                                    jQuery('#' + value.Field).removeClass('success');
                                    arr_err.push(value.Field);
                                }
                                alert("You have incorrect value for the field's: " + arr_err.join());
                            });

//                            if( jQuery( "#formSlide" ).find('[data-form_slide="1"] div input').hasClass( "error" ) ){
//                                to_slide = 1;
//                            }else if( jQuery( "#formSlide" ).find('[data-form_slide="2"] div input').hasClass( "error" ) ){
//                                to_slide = 2;
//                            }
                        }
                    }
                });
            });
        });
    </script>
<?php
get_footer();

