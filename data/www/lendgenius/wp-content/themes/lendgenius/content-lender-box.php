<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 14.12.16
 * Time: 16:33
 */

// The Query
$args = array(
    'post_type' => 'lboxes',
    'post_status' => 'publish',
    'name' => $title,
    'posts_per_page' => 1
);
$lender_boxes = '';
$query = new WP_Query( $args );
if ( $query->have_posts() ) {
    // The Loop
    while ( $query->have_posts() ) : $query->the_post();
        $get_started_url = get_post_meta( get_the_ID(), 'get_started_url', true );
        $text_under_button = get_post_meta( get_the_ID(), 'text_under_button', true );

        $lender_boxes .= "<div class=\"CTA-box\">
            <div class=\"CTA-box-head\">
                <a href=\"".$get_started_url."\">
                    ".single_post_thumbnail('lender-box', '', true)."
                </a>
            </div>
            <div class=\"CTA-box-content\">".get_the_content()."</div>
            <div class=\"CTA-box-bottom\">
                <a href=\"".$get_started_url."\" class=\"btn btn-success\">Get Started</a>
                <span>".$text_under_button."</span>
            </div>
        </div>";

    endwhile;
    wp_reset_postdata();
}else{
    $lender_boxes = "<span>Lender Box doesn't exist</span>";
}
?>
