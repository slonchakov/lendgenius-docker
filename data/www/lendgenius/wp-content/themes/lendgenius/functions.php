<?php
//Logger
define('LG_THEME_ROOT', WP_CONTENT_DIR .'/themes/lendgenius/');
$form_number = null;
require 'app/app.php';


/**
 * !!! All scripts must be in app directory
 * !!! This file just using as index point
 * 
 * -- app/app.php - entry point
 * -- app/bootstrap.php - define list of Custom Post Types for including to project.
 * -- app/custom_post_types - directory with all CPT which must be registered in  app/bootstrap.php
 * -- app/include.php - we use namespaces and autoloading if we need include some file without namespace use this file
 * -- app/register.php - here we register all actions and filters for using in project
 * -- app/actions - directory of all independent action calls  implements ActionInterface
 * -- app/filters - directory of all independent filter calls implements FilterInterface
 * -- app/routes.php - declaring routes for invoke Controllers from app/Http
 * -- app/Http - directory of all Controllers which we want call dependent on route
 * -- app/classes - logical classes, interfaces and abstract classes
 * -- app/classes/Container.php - create classes using DI
 * -- app/classes/Controller.php - abstract class for controllers
 * -- app/classes/Kernel.php - root of response creating with controllers
 * -- app/classes/Response.php - check uri with routes in Router, load controller via Container if checking successful
 *                               instance of Controller must return string with path to view to override default with method view()
 *                               or just empty string
 * -- app/classes/Router.php - [singleton] hold list of all routes used in app
 * -- app/classes/View.php - method load() makes full path to template from name given to the method. Pass array of datas to global $wp->data
 *
 * -- app/func_backup.php - old functions.php
 */


add_filter( 'wpseo_next_rel_link', '__return_empty_string' );
add_filter( 'wpseo_prev_rel_link', '__return_empty_string' );


add_filter('walker_nav_menu_start_el', function($item_output, $item, $depth, $args ){


    if ('#' == $item->url) {
        $item_output = '<span data-href="'. str_replace(' ', '-',strtolower($item->post_title)) .'">'. $item->post_title .'</span>';
    }

    return $item_output;
},10,4);
/**
 * TODO check why it here
 */
function wpa54064_inspect_scripts()
{
    wp_dequeue_style('tablepress-default');
}
add_action( 'wp_print_scripts', 'wpa54064_inspect_scripts' );