<?php
/*
    Template Name: Landing Gmail 2
*/
?>

<form-app></form-app>

<?php 
if(get_option( 'sts_enable_form' ) == 'on'){ ?>
    <script type="text/javascript">
        window.applicationOptions = {
            sendShortForm: '<?php echo get_option( 'sts_endpoint_url' ); ?>',
            zipCode: '<?php echo get_option( 'sts_zipcode' ); ?>',
            emailValidateUrl: '<?php echo get_option( 'sts_email_v_url' ); ?>',
            phoneValidateUrl: '<?php echo get_option( 'sts_phone_v_url' ); ?>'
        }
    </script>
<?php } ?>

<script src="<?php echo get_option( 'sts_script_url' ); ?>/form_3/form_3.js?ver=<?php echo get_option( 'sts_script_version' ); ?>"></script>