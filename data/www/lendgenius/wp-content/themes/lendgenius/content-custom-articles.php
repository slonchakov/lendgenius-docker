<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 25.10.16
 * Time: 18:22
 */

//You May Also Like
$you_may_also_like = get_post_meta( get_the_ID(), 'top_articles', true );
$you_may_also_like_title = get_post_meta( get_the_ID(), 'top_articles_title', true );
$you_may_also_like_title = empty($you_may_also_like_title) ? 'Top Articles':$you_may_also_like_title;
if ($you_may_also_like && is_array($you_may_also_like)) {
    $args=array(
        'post_type' => 'post',
        'post__in' => $you_may_also_like,
        'posts_per_page'=>4,
        'caller_get_posts'=>1
    );
    $tags_query = new WP_Query($args);
    $post_count = $tags_query->post_count;
    if( $tags_query->have_posts()  && $post_count > 0) :
        ?>
        <div class="content-blog content-sub">
            <div class="content-sub-item">
                <h2 class="item-title-primary"><?php echo $you_may_also_like_title; ?></h2>
                <div class="content-blog-items in-columns">
                    <?php
                    while ($tags_query->have_posts()) : $tags_query->the_post();

                        get_template_part( 'content-popular-related', get_post_format() );

                    endwhile;
                    ?>
                </div>
            </div>
        </div>
    <?php endif;
    wp_reset_query();
}
?>