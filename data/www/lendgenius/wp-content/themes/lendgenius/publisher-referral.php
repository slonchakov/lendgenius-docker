<?php
/*
    Template Name: Publisher Referral
*/
get_header();

$sign_up_url = get_post_meta( get_the_ID(), 'sign_up_url', true );
$affiliate_url = get_post_meta( get_the_ID(), 'affiliate_url', true );

$sub_title = get_post_meta( get_the_ID(), 'sub_title', true );
$pre_title = get_post_meta( get_the_ID(), 'pre_title', true );
$column_1 = get_post_meta( get_the_ID(), 'column_1', true );
$column_2 = get_post_meta( get_the_ID(), 'column_2', true );
$phone_info = get_post_meta( get_the_ID(), 'phone_info', true );
$why_join = get_post_meta( get_the_ID(), 'why_join', true );
?>

<section class="slogan-section" style="background-image: url(<?php echo get_stylesheet_directory_uri() ?>/assets/images/publesher-portal/patern-concept_05.jpg)">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <h1><?php the_title(); ?></h1>
                <div class="pre-title">
                    <?php echo $pre_title; ?>
                </div>
                <div class="button-holder">
                    <a href="<?php echo $sign_up_url; ?>" class="btn btn-success btn-block btn-circle">Sign up for the program</a>
                    <a href="<?php echo $affiliate_url; ?>" class="btn btn-default btn-block btn-circle">Affiliate login</a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="description-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 text-center">
                <h2><?php echo $sub_title; ?></h2>
                <span class="pre-title-h2">Become an affiliate in 15 minutes.</span>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="row">
                    <div class="col-sm-4">
                        <p>
                            <?php echo $column_1; ?>
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/publesher-portal/img-desc.png" class="img-responsive center-block" alt="" />
                    </div>
                    <div class="col-sm-4">
                        <p>
                            <?php echo $column_2; ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="how-works">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <h2>
                    How the program works?
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="step-holder step-shadow">
                            <div class="step-icon">
                                <span class="nc-icon-monitor_1"></span>
                            </div>
                            <div class="step-name">
                                    <span class="number">
                                        <i>1</i>
                                    </span>
                                Fill One Form
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="step-holder step-shadow">
                            <div class="step-icon">
                                <span class="nc-icon-monitor_2"></span>
                            </div>
                            <div class="step-name">
                                    <span class="number">
                                        <i>2</i>
                                    </span>
                                Browser Loan Offers
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="step-holder step-shadow">
                            <div class="step-icon">
                                <span class="nc-icon-money_2"></span>
                            </div>
                            <div class="step-name">
                                    <span class="number">
                                        <i>3</i>
                                    </span>
                                Get Funded
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 questions-holder">
                        <?php echo $phone_info; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="conviction">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 text-center">
                <h2>Why Join?</h2>
                <span class="pre-title-h2 br-lg"><?php echo $why_join; ?></span>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="row">
                    <?php echo get_the_content(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
get_footer();
?>