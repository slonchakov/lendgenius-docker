<?php
/*
Template Name: Apply To Work Here
*/
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Dosis:400,700|Open+Sans:300,400,700">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() ?>/LG_LP_UPD4/assets/styles/style-apply.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() ?>/LG_LP_UPD4/styles/custom.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() ?>/LG_LP_UPD4/assets/styles/fonts.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() ?>/LG_LP_UPD4/styles/style1.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <title>Apply to Work here</title>
  </head>
<body>
    <div class="apply_work_here">
        <div class="work_form-holder">
            <h1 class="text-center">Now Hiring <span>Geniuses</span></h1>
            <div class="form-holder">
                <form id="form" target="_self">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="entry.1660167337" class="form-control input-text" id="firstNameTxt" placeholder="First Name" value="" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="entry.872393750" class="form-control" id="lastNameTxt" placeholder="Last Name" value="" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="tel" name="entry.658755184" class="form-control" id="phoneNumber" placeholder="(818) 888-8888" value="" />
                    </div>
                    <div class="form-group">
                        <input type="email" name="entry.341369643" class="form-control email" id="emailTxt" placeholder="Email" value="" />
                    </div>
                    <div class="form-group list-checkbox">
                        <label class="control-label">What departments are you interested in?</label>
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="label-check">
                                    <input type="checkbox" name="entry.1510467025" value="Marketing & Growth" class="checkbox-det" />
                                    <span></span>
                                    Marketing & Growth
                                </label>
                            </div>
                            <div class="col-sm-6">
                                <label class="label-check">
                                    <input type="checkbox" name="entry.1510467025" value="Graphic Design" class="checkbox-det" />
                                    <span></span>
                                    Graphic Design
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="label-check">
                                    <input type="checkbox" name="entry.1510467025" value="Software Development" class="checkbox-det" />
                                    <span></span>
                                    Software Development
                                </label>
                            </div>
                            <div class="col-sm-6">
                                <label class="label-check">
                                    <input type="checkbox" name="entry.1510467025" value="Sales & Customer Service" class="checkbox-det" />
                                    <span></span>
                                    Sales & Customer Service
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="label-check">
                                    <input type="checkbox" name="entry.1510467025" value="Recruiting / HR" class="checkbox-det" />
                                    <span></span>
                                    Recruiting / HR
                                </label>
                            </div>
                            <div class="col-sm-6">
                                <label class="label-check">
                                    <input type="checkbox" name="entry.1510467025" value="Analytics" class="checkbox-det" />
                                    <span></span>
                                    Analytics
                                </label>
                            </div>
                        </div>
                        <label class="label-check">
                            <input type="checkbox" value="Product Management" name="entry.1510467025" class="checkbox-det" />
                            <span></span>
                            Product Management
                        </label>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <button class="btn btn-primary btn-block" type="submit">Let's do it</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="success-send">
        <div class="msg">Thank you, we will be in touch soon.</div>
    </div>
    <!--<script src="assets/js/jquery.formstyler.js"></script>-->
    <script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>

    <script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/additional-methods.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri() ?>/LG_LP_UPD4/assets/js/input-mask.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri() ?>/LG_LP_UPD4/assets/js/send-data-candidate.js"></script>
</body>
</html>
