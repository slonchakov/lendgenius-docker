<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 15.12.16
 * Time: 12:08
 */
$for_ga = 'form-3';
$general_text = 'Don’t let bad <br>credit hold <br>you back';
$sub_text = 'It’s easier than ever for you to <br> get approved for a small business <br>loan! Even if your credit is not good.';
$left_img = 'landing-container-credit';
include_once 'landing-content.php';
?>