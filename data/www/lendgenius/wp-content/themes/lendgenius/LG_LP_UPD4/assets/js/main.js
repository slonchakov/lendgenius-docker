function get_state_and_city() {
    var zipcode = $("#ZipCodeBusiness").val().substring(0, 5);
    if (zipcode.length == 5 && /^[0-9]+$/.test(zipcode))
    {
        // Check cache
        // Build url
        var url = "https://www.loanmatchingservice.com/misc/?responsetype=json&action=validatezipcode&zipcode=" + zipcode;

        // Make AJAX request
        $.ajax({
            "url": url,
            "dataType": "jsonp"
        }).done(function(data) {
            if(data.Conversion) {
                var obj = jQuery.parseJSON(data.Conversion);
                if (obj.StateShort != undefined && obj.City != undefined)
                    jQuery('#city_state').html(obj.City + ', ' + obj.StateShort);
                $("#ZipCodeBusiness").toggleClass('error', false);
            }else{
                $("#ZipCodeBusiness").toggleClass('error', true);
                jQuery('#city_state').html('');
            }
        }).fail(function(data) {
        });
    }
}


$(document).ready(function () {

    dataLayer.push({
        event: 'form',
        eventCategory: jQuery('[name="pg_name"]').val(),
        eventAction: 'step1'
    });
    jQuery('#ZipCodeBusiness').mask('00000');
    jQuery( '#ZipCodeBusiness' ).focusout(function() {
        get_state_and_city();
    });
    function getAllUrlParams(url) {
        var queryString = url ? url.split('?')[1] : window.location.search.slice(1);
        var obj = {};
        if (queryString) {
            queryString = queryString.split('#')[0];
            var arr = queryString.split('&');
            for (var i = 0; i < arr.length; i++) {
                var a = arr[i].split('=');
                var paramNum = undefined;
                var paramName = a[0].replace(/\[\d*\]/, function (v) {
                    paramNum = v.slice(1, -1);
                    return '';
                });
                var paramValue = typeof(a[1]) === 'undefined' ? true : a[1];
                paramName = paramName.toLowerCase();
                //paramValue = paramValue;
                if (obj[paramName]) {
                    if (typeof obj[paramName] === 'string') {
                        obj[paramName] = [obj[paramName]];
                    }
                    if (typeof paramNum === 'undefined') {
                        obj[paramName].push(paramValue);
                    }
                    else {
                        obj[paramName][paramNum] = paramValue;
                    }
                }
                else {
                    obj[paramName] = paramValue;
                }
            }
        }
        return obj;
    }

    var params = getAllUrlParams(window.location.href);

    // $('#RequestedAmount').val(params.requestedamount);
    // $('#IndustryDescription').val(params.industrydescription);
    // $('#TimeInBusiness').val(params.timeinbusiness);
    $('.partner1 select').styler();

    function MoneyMask(elem, useDollarSign) {
        $(elem).removeAttr('min').removeAttr('max').removeAttr('maxlength');
        var clonedElem = $(elem).clone().insertAfter($(elem));
        $(elem).prop('type', 'hidden');
        clonedElem.attr('id', clonedElem.attr('id') + 'Masked');
        clonedElem.attr('name', clonedElem.attr('id'));
        clonedElem.addClass('b2c-dont-send');

        var maskUseCurrencySign = useDollarSign ? true : false;
        var maskCurrencySign = maskUseCurrencySign ? '$' : '';
        var SPMaskBehavior = function (val) {
                var compare = val.replace(/\D/g, '').length;
                if (compare === 0) {
                    return '000000'
                }
                else if (compare < 4) {
                    return maskCurrencySign + '000000'
                }
                else if (compare === 4) {
                    return maskCurrencySign + '0,0000'
                }
                else if (compare === 5) {
                    return maskCurrencySign + '00,0000'
                }
                else if (compare === 6) {
                    return maskCurrencySign + '000,0000'
                }
                else if (compare > 6) {
                    return maskCurrencySign + '0,000,000'
                }
            },
            spOptions = {
                onKeyPress: function (val, e, field, options) {
                    field.mask(SPMaskBehavior.apply({}, arguments), options);
                }
            };

        clonedElem.mask(SPMaskBehavior, spOptions);

        var regex = new RegExp($(elem).attr('id') + '=(\\d*)', 'i');
        if (window.location.href.match(regex) !== null) {
            clonedElem.val(window.location.href.match(regex)[1]);
            clonedElem.trigger('keyup');
            var timerLabel = setInterval(function () {
                if ($('.b2c-btn').length) {
                    clonedElem.trigger('blur');
                    clearInterval(timerLabel);
                }
            }, 100);
        }

        clonedElem.on('change', function () {
            var value = $(this).val();
            var intValue = value.replace(/[^0-9]/g, '');
            $(elem).val(intValue);
        })
    }

    if ( $( '#RequestedAmount' ).length ) {
        // jQuery( "#RequestedAmount").keypress(function(event) {
        //     var this_val = jQuery(this).val();
        //     if( !/^[0-9]$/.test(event.key) ){
        //         event.preventDefault();
        //     }else{
        //         if(this_val.length === 0){
        //             jQuery(this).val('$' + this_val);
        //         }else if(this_val.length == 4){
        //             jQuery(this).val(jQuery(this).val() + ',');
        //         }
        //     }
        // }).keyup(function(event) {
        //     if(event.keyCode == 8 && jQuery(this).val().length == 1){
        //         jQuery(this).val('');
        //     }
        // });
       MoneyMask('#RequestedAmount', true);
        // VMasker(document.getElementById("RequestedAmount")).maskMoney({
        //     unit: '$',
        //     zeroCents: true,
        //     delimiter: ',',
        //     precision: 0
        // });
    }
    if ( $( '#GrossSales' ).length ) {
         MoneyMask('#GrossSales', true);
        // VMasker(document.getElementById("GrossSales")).maskMoney({
        //     unit: '$',
        //     zeroCents: true,
        //     delimiter: ',',
        //     precision: 0
        // });
    }
    if ( $( '#PhoneHome' ).length ) {
         $('#PhoneHome').mask('000-000-0000');
    }
    if ( $( '#PhoneWork' ).length ) {
         $('#PhoneWork').mask('000-000-0000');
    }

    $("#formSlide").owlCarousel({
        slideSpeed: 500,
        paginationSpeed: 400,
        singleItem: true,
        autoHeight: true,
        mouseDrag: false,
        touchDrag: false,
        addClassActive: true
    });

    var owl = $(".owl-carousel").data('owlCarousel');

    /**
     * Method toggle class of leader
     */
    function toggleLoader($element, state) {
        $element.toggleClass("loading", state);
    }

    /**
     * Send request
     */
    function sendRequest() {
        dataLayer.push({
            event: 'form',
            eventCategory: jQuery('[name="pg_name"]').val(),
            eventAction: 'submitted'
        });
        jQuery('[name="campaignid"]').val(window.lmpost.options.campaignid);
        jQuery('[name="hituid"]').val(window.lmpost.options.hituid);

        jQuery.post( "/wp-admin/admin-ajax.php", jQuery('#l_form').serialize(), function(response) {
            var obj = jQuery.parseJSON(response);
            toggleLoader($(".btn-container"), false);
            //modal button loading state
            $("#sendModal").button('reset');
            if (obj.RedirectURL) {
                dataLayer.push({
                    event: 'form',
                    eventCategory: jQuery('[name="pg_name"]').val(),
                    eventAction: 'result',
                    eventLabel: obj.Result.toLowerCase()
                });
                if(obj.Result == 'Accepted'){
                    if(typeof fbq === 'undefined') {
                        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                            document,'script','https://connect.facebook.net/en_US/fbevents.js');
                        fbq('init', '188498344916220');
                        fbq('track', 'Lead');
                    }else {
                        fbq('track', 'Lead');
                    }
                }
                window.location.href = obj.RedirectURL;
            } else {
                var to_slide = 0,
                    $toActionBtn = $(".to-action");

                if(obj.Errors) {
                    jQuery.each(obj.Errors, function( index, value ) {
                        if ( $.isArray(value) ) {
                            jQuery.each(value, function( index_in, value_val ) {
                                $('#' + value_val.Field).toggleClass('error', true);
                            });
                        } else {
                            $('#' + value.Field).toggleClass('error', true);
                        }
                    });

                    //close modal window
                    $("#submitModal").modal('hide');

                    $toActionBtn.html('Continue');

                    if( jQuery( "#formSlide" ).find('[data-form_slide="0"] div input').hasClass( "error" ) ){
                        to_slide = 0;
                        //add classes to make stepscorrect
                        if(jQuery('[name="pg_name"]').val() == 'form-1'){
                          jQuery('.step-2').addClass("step-1").removeClass("step-2");
                          jQuery('.step-title').html("1/2");
                        }
                        if($(".to-back").length){
                            $(".to-back").hide();
                        }
                    }else if( jQuery( "#formSlide" ).find('[data-form_slide="1"] div input').hasClass( "error" ) ){
                        to_slide = 1;
                    }else if( jQuery( "#formSlide" ).find('[data-form_slide="2"] div input').hasClass( "error" ) ){
                        to_slide = 2;
                        $toActionBtn.html('Complete My Submission');
                    }
                    $toActionBtn.attr('data-form_btn', to_slide);
                    owl.goTo( to_slide );
                    jQuery("html, body").animate({scrollTop: $('.error').first().offset().top-jQuery('#top').height()}, "slow");
                }
            }
        });
    }

    /**
     * Form validate
     */
    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    function validateField(elem){
        if (elem.value == "") {
            return true;
        }

        if (elem.id == 'ZipCodeBusiness') {
            var zipcode = elem.value,
                state = jQuery('#city_state').html();
            if (zipcode.length != 5 || /^[0-9]+$/.test(zipcode) == false || state == ''){
                return true;
            }
        }

        if (elem.id == 'Email' && !validateEmail(elem.value)) {
            return true;
        }

        return false;
    }

    (function setEventHandler() {
        var $toActionBtn = $(".to-action"),
            $toBackBtn = $(".to-back"),
            currentBtnSlide = Number($('.to-action').attr('data-form_btn')),
            $fieldItem = $('form.form').find('input,select'),
            $modalSendBtn = $("#sendModal");

        function toggleBackBtn() {
            $toBackBtn.toggle(currentBtnSlide !== 0);
            if (currentBtnSlide !== 2) {
                $toActionBtn.html('Continue');
            }
        }

        $fieldItem.on('keydown', function(e) {
            var keyCode = e.keyCode || e.which;

            if (keyCode == 9) {
                e.preventDefault();
            }
        });

        $fieldItem.on("blur", function () {
            $(this).toggleClass("error", validateField(this));
        });

        $toActionBtn.on("click", function () {
            var valid = true,
                $field = $('form.form').find('[data-form_slide="' + currentBtnSlide + '"]').find('input,select');

            currentBtnSlide = Number($('.to-action').attr('data-form_btn'));

            $field.each(function () {
                var isValid = validateField(this);

                if (isValid) {
                    $(this).toggleClass('error', isValid);
                    valid = false;
                }
            });

            //jQuery("html, body").animate({scrollTop: $('.error').first().offset().top}, "slow");
            if (valid) {

                $toActionBtn.attr('data-form_btn', ++currentBtnSlide);

                if(jQuery('[name="pg_name"]').val() == 'form-1' && currentBtnSlide == 2){
                    toggleLoader($(".btn-container"), true);
                    //prevent data-form-btn increment, we need to stay on current step with modal
                    $toActionBtn.attr('data-form_btn', --currentBtnSlide);
                    //show modal
                    $('#submitModal').modal('show');
                    return;
                }else if(jQuery('[name="pg_name"]').val() == 'form-1' && currentBtnSlide == 1){
                    jQuery('.step-1').addClass("step-2").removeClass("step-1");
                    jQuery('.step-title').html("2/2");
                    $toActionBtn.html('Complete My Submission');
                }
                if(currentBtnSlide > 3){
                    currentBtnSlide = 3;
                    $toActionBtn.attr('data-form_btn', currentBtnSlide);
                }
                owl.goTo(currentBtnSlide);
                toggleBackBtn();
                if (currentBtnSlide != 3){
                    jQuery( "body" ).scrollTop(0);
                }

                if (currentBtnSlide === 0 || currentBtnSlide === 1 || currentBtnSlide === 2){
                    dataLayer.push({
                        event: 'form',
                        eventCategory: jQuery('[name="pg_name"]').val(),
                        eventAction: 'step'+(currentBtnSlide+1)
                    });
                }

                if (currentBtnSlide === 3) {
                    /*toggleLoader($(".btn-container"), true);*/
                    //prevent data-form-btn increment, we need to stay on current step with modal
                    $toActionBtn.attr('data-form_btn', --currentBtnSlide);
                    //show modal
                    $('#submitModal').modal('show');
                } else {
                    $toActionBtn.html('Continue');
                }

                if (currentBtnSlide >= 2) {
                    $toActionBtn.html('Complete My Submission');
                }
            }else{
                jQuery("html, body").animate({scrollTop: $('.error').first().offset().top-jQuery('#top').height()}, "slow");
            }
        });

        $toBackBtn.on("click", function () {
            if (currentBtnSlide >= 1) {
                $toActionBtn.attr('data-form_btn', --currentBtnSlide);
                owl.goTo(currentBtnSlide);
                toggleBackBtn();
                if(jQuery('[name="pg_name"]').val() == 'form-1' && currentBtnSlide == 0){
                    jQuery('.step-2').addClass("step-1").removeClass("step-2");
                    jQuery('.step-title').html("1/2");
                }
            }
        });

        // modal button even handler
        $modalSendBtn.on("click", function() {
           $modalSendBtn.button('loading');
           sendRequest();
        });
        if (navigator.userAgent.match(/Trident\/7\./)) { // if IE
            $('body').on("mousewheel", function () {
                // remove default behavior
                event.preventDefault();

                //scroll without smoothing
                var wheelDelta = event.wheelDelta;
                var currentScrollPosition = window.pageYOffset;
                window.scrollTo(0, currentScrollPosition - wheelDelta);
            });
        }
    }());
});
