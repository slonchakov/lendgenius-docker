﻿$(function () {
    $('input[type="tel"]').mask("(999) 999-9999");
    $("form").validate({
        rules: {
            "entry.1660167337": "required", //first name
            "entry.872393750": "required", //last name
            "entry.341369643": { //email
                required: true,
                email: true
            },
            "entry.658755184": { //phone number
                required: true,
                minlength: 7,
                minlength: 10
            }
        },
        messages: {
            "entry.1660167337": "Please enter your First Name",
            "entry.872393750": "Please enter your Last Name",
            "entry.341369643": "Please enter a valid Email address",
            "entry.658755184": {
                required: "Please enter your phone number"
            }
        },
        submitHandler: function (form) {

            var $fields = $('form').find('input.checkbox-det:checked');
            if (!$fields.length) {
                $('.list-checkbox').append('<p>Please mark at least one checkbox!</p>');
                return false; // The form will *not* submit
            }
            var data = $("form").serialize(); // сериализуем данные формы в строку для отправки, обратите внимание что атрибуты name у полей полностью сопдают с нэймами у полей самой гугл формы
            $.ajax({ // инициализируем аякс
                url: "https://docs.google.com/a/nesklada.com/forms/d/e/1FAIpQLScYDAFFEGBR-6EPlcyOZR9ZXKLmreL0W4GgNkip-NBUmDpX9w/formResponse", // слать надо сюда, строку с буковками надо заменить на вашу, это атрибут action формы
                data: data, // данные  которые мы сериализовали
                type: "POST", // постом
                dataType: "xml", // ответ ждем в формате xml
                beforeSend: function () { // перед отправкой
                    $("form").find('button').attr('disabled'); // отключим кнопку
                },
                statusCode: { // после того как пришел ответ от сервера
                    0: function () { // это успешный случай
                        $('.success-send').addClass('on');
                        $('form button').attr('disabled', 'disabled');
                        $('.list-checkbox').find('p').remove();
                        setTimeout(function () {
                            $('form input[type="text"]').val("");
                            $('form input[type="email"]').val("");
                            $('form input[type="tel"]').val("");
                            $('form input[type="checkbox"]').prop('checked', false);
                            $('.success-send').removeClass('on');
                            $('form button').removeAttr('disabled');
                            location.reload();
                        }, 5000);
                    },
                    200: function () { // это тоже успешный случай
                        $('.success-send').addClass('on');
                        $('form button').attr('disabled', 'disabled');
                        $('form input[type="text"]').val("");
                        $('form input[type="email"]').val("");
                        $('form input[type="tel"]').val("");
                        $('form input[type="checkbox"]').prop('checked', false);
                        $('.list-checkbox').find('p').remove();
                        setTimeout(function () {
                            $('.success-send').removeClass('on');
                            $('form button').removeAttr('disabled');
                            $('form input[type="text"]').val("");
                            location.reload();
                        }, 5000);
                    }
                }
            });

        }
    });
});