var gulp = require('gulp');
var less = require('gulp-less');
var minifyCSS = require('gulp-minify-css');
var watchLess = require('gulp-watch-less');

var path = require('path');

gulp.task('less', function () {
    return gulp.src('./css/sources/main.less')
        .pipe(less({
            paths: [ path.join(__dirname, 'less', 'includes') ]
        }))
        //.pipe(gulp.dest('./css'));

        //.pipe(minifyCSS())

        .pipe(gulp.dest('./css'));

});

gulp.task('less:watch', function () {
    return gulp.src('./css/sources/main.less')
        .pipe(watchLess('./css/sources/*.less', function(){
            gulp.start('less');
        }))
});

gulp.task('default', ['less', 'less:watch']);