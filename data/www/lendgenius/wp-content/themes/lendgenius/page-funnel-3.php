<?php
/*
    Template Name: Funnel-3
*/
get_header();
?>
<div class="page-funnel-3">
    <div class="page-wraper">
        <div class="page-wraper-bg"></div>
        <div class="container">
            <a href="/">
                <img class="site-logo" src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/logo-lendgenius.png" alt="Legend Genius logo">
            </a>
            <div class="row">
                <div class="col-md-6">
                    <div class="funnel-form">
                        <div class="funnel-form-steps">
                            <div class="steps-indication">
                                <div class="step-title">Step 1 of 2</div>
                                <div class="step-selector"><span class="active" >1</span><span>2</span></div>
                            </div>
                            <div class="step-1">
                                <div class="content-refine-results tpl-info">
                                    <div class="content-wrap">
                                        <form method="post" action="/get-started2" id="loan_form1">
                                            <div class="refine-results-items">
                                                <div class="item">
                                                    <div class="item-label">
                                                        <label>Loan amount</label>
                                                    </div>
                                                    <div class="item-field">
                                                        <input class="field-input field-input-biger" type="text" id="loan_amount" name="loan_amount" placeholder="$X,XXX,XXX" value="<?php echo isset($_POST['how_much']) ? $_POST['how_much']:''?>">
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="item-label">
                                                        <label>Monthly revenue</label>
                                                    </div>
                                                    <div class="item-field">
                                                        <input class="field-input field-input-biger" type="text" id="gross_sales" name="gross_sales" placeholder="$X,XXX,XXX">
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="item-label">
                                                        <label>Time in business</label>
                                                    </div>
                                                    <div class="item-field">
                                                        <select class="field-styler-option field-input-biger" name="time_in_business">
                                                            <option disabled selected>Please choose</option>
                                                            <option value="0">I'm looking to start a business</option>
                                                            <option value="6">Less than 6 months</option>
                                                            <option value="24">1 - 2 years</option>
                                                            <option value="60">2 - 5 years</option>
                                                            <option value="84">5 - 8 years</option>
                                                            <option value="96">More than 8 years</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="item-label">
                                                        <label>Business name</label>
                                                    </div>
                                                    <div class="item-field">
                                                        <input class="field-input field-input-biger" type="text" name="business_name">
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="item-label">
                                                        <label>Industry</label>
                                                    </div>
                                                    <div class="item-field">
                                                        <select class="field-styler-option field-input-biger" name="industry_description">
                                                            <option disabled selected>Please choose</option>
                                                            <option value="Administrative">Administrative and Support Services</option>
                                                            <option value="Agriculture">Agriculture</option>
                                                            <option value="Art">Arts, Entertainment, and Recreation</option>
                                                            <option value="AutoDealer">Automobile Dealers</option>
                                                            <option value="AutoRepair">Automotive Repair and Maintenance</option>
                                                            <option value="BusinessServices">Business Services</option>
                                                            <option value="Construction">Construction</option>
                                                            <option value="Education">Educational Services</option>
                                                            <option value="Finance">Finance and Insurance</option>
                                                            <option value="Forestry">Forestry, Fishing, and Hunting</option>
                                                            <option value="FreightTrucking">Freight Trucking</option>
                                                            <option value="Gambling">Gambling Industries</option>
                                                            <option value="GasStations">Gas Stations</option>
                                                            <option value="Greenhouse">Greenhouse, Nursery, and Floriculture</option>
                                                            <option value="Healthcare">Healthcare and Social Assistance</option>
                                                            <option value="Hotels">Hotels and Travel Accomodations</option>
                                                            <option value="InformationTechnology">Information and Technology</option>
                                                            <option value="Legal">Legal Services</option>
                                                            <option value="Management">Management of Companies</option>
                                                            <option value="Manufacturing">Manufacturing</option>
                                                            <option value="Mining">Mining</option>
                                                            <option value="NonProfit">Non-Profit Organizations</option>
                                                            <option value="OtherServices">Other Services</option>
                                                            <option value="PersonalCare">Personal Care Services</option>
                                                            <option value="PhysicianDentistHealth">Physician, Dentist, or Health Practitioner</option>
                                                            <option value="PublicAdministration">Public Administration</option>
                                                            <option value="RealEstate">Real Estate</option>
                                                            <option value="ReligiousInstitutions">Religious Institutions</option>
                                                            <option value="RentalLeasing">Rental and Leasing</option>
                                                            <option value="FoodRestaurants">Restaurants and Food Services</option>
                                                            <option value="RetailStores">Retail Stores</option>
                                                            <option value="Transportation">Transportation</option>
                                                            <option value="Utilities">Utilities</option>
                                                            <option value="Veterinarians">Veterinarians</option>
                                                            <option value="Warehousing">Warehousing and Storage</option>
                                                            <option value="WasteManagement">Waste Management &amp;amp; Remediation Services</option>
                                                            <option value="WholesaleTrade">Wholesale Trade</option>
                                                            <option value="ScientificServices">Professional, Scientific, and Technical Services</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="item-label">
                                                        <label>Business street address</label>
                                                    </div>
                                                    <div class="item-field">
                                                        <input class="field-input field-input-biger" type="text" name="business_address">
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="item-label">
                                                        <label>ZIP code</label>
                                                    </div>
                                                    <div class="item-field">
                                                        <input class="field-input field-input-biger" type="text" name="zip_code" >
                                                        <span id="city_state"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="refine-results-footer">
                                                <button class="btn btn-primary" type="submit">Continue</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="funnel-info">
                        <div class="funnel-info-body">
                            <?php
                            // Start the loop.
                            while ( have_posts() ) : the_post();

                                the_content();

                            endwhile; ?>
                        </div>
                        <div class="funnel-info-footer">
                            <ul class="list-inline sprites">
                                <li class="icon-protect-comodo-green"></li>
                                <li class="icon-protect-mcafee-red"></li>
                                <li class="icon-protect-norton"></li>
                            </ul>
                            <div class="subtext">
                                <p>SBA loans are a great financing solution that gets rid of cash flow constraints thanks to their longer terms, often at the lowest rates.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" >
    jQuery(document).ready(function($) {
        jQuery( "[name=\"time_in_business\"], [name=\"business_name\"], [name=\"industry_description\"], [name=\"business_address\"], [name=\"zip_code\"]" ).focusout(function() {
            validate_funnel3(this);
            get_state_and_city();
        });
        jQuery( "[name=\"time_in_business\"], [name=\"industry_description\"]" ).change(function() {
            jQuery(this).parent().find('.error-message').remove();
            if(this.value == '' || this.value == 'Please choose'){
                jQuery(this).removeClass('success');
                jQuery(this).addClass('error');
                if(this.name == 'time_in_business'){
                    jQuery(this).parent().find('.jq-selectbox__dropdown').after('<span class="error-message">This is a required field.</span>');
                }else if(this.name == 'industry_description'){
                    jQuery(this).parent().find('.jq-selectbox__dropdown').after('<span class="error-message">Please, select your business industry.</span>');
                }
            }else{
                jQuery(this).removeClass('error');
                jQuery(this).addClass('success');
            }
        });
        jQuery('#loan_form1 button:submit').click(function( event ) {
            event.preventDefault();
            var loan_amount = jQuery('[name="loan_amount"]').val(),
                gross_sales = jQuery('[name="gross_sales"]').val(),
                time_in_business = jQuery('[name="time_in_business"]').val(),
                business_name = jQuery('[name="business_name"]').val(),
                industry_description = jQuery('[name="industry_description"]').val(),
                business_address = jQuery('[name="business_address"]').val(),
                zip_code = jQuery('[name="zip_code"]').val();

            //validation
            if(loan_amount == '' || gross_sales == '' || time_in_business == null || business_name == '' ||
                industry_description == null || business_address == '' || zip_code == ''){
                jQuery( "[name=\"time_in_business\"], [name=\"business_name\"], [name=\"industry_description\"], [name=\"business_address\"], [name=\"zip_code\"]" ).each(function() {
                    validate_funnel3(this);
                });
                return;
            }

            jQuery('#loan_form1').submit();
        });

        function validate_funnel3(elem){
            jQuery(elem).parent().find('.error-message').remove();
            if(elem.value == '' || elem.value == 'Please choose'){
                jQuery(elem).removeClass('success');
                jQuery(elem).addClass('error');
                if(elem.name == 'time_in_business'){
                    jQuery(elem).parent().find('.jq-selectbox__dropdown').after('<span class="error-message">This is a required field.</span>');
                }else if(elem.name == 'industry_description'){
                    jQuery(elem).parent().find('.jq-selectbox__dropdown').after('<span class="error-message">Please, select your business industry.</span>');
                }else{
                    jQuery(elem).after('<span class="error-message">This is a required field.</span>');
                }
            }else{
                jQuery(elem).removeClass('error');
                jQuery(elem).addClass('success');
                //get_state_and_city();
            }
        }

        /** Handle successful response */
        // Set up event handlers
        function get_state_and_city() {
            var zipcode = $("input[name='zip_code']").val().substring(0, 5);
            if (zipcode.length == 5 && /^[0-9]+$/.test(zipcode))
            {
                // Check cache
                    // Build url
                    var url = "https://www.loanmatchingservice.com/misc/?responsetype=json&action=validatezipcode&zipcode=" + zipcode;

                    // Make AJAX request
                    $.ajax({
                        "url": url,
                        "dataType": "jsonp"
                    }).done(function(data) {
                        if(data.Conversion) {
                            var obj = jQuery.parseJSON(data.Conversion);
                            if (obj.StateShort != undefined && obj.City != undefined)
                                jQuery('#city_state').html(obj.City + ', ' + obj.StateShort);
                        }
                    }).fail(function(data) {
                    });
            }
        }
    });
</script>
<?php
get_footer();
