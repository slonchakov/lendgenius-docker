<?php

get_header('personal');

$topButtons       = [];
$ourLenderNetwork = [];
$howItWorks       = [];
$chooseYourReason = [];
$featuredOn       = [];
$ctaBlock         = [];
$lastPosts        = [];

if( have_rows('posts') ):
    while( have_rows('posts') ): the_row();
        $lastPosts[] = get_sub_field('post');
    endwhile;
endif;

if( have_rows('top_buttons') ):
    while( have_rows('top_buttons') ): the_row();
        $topButtons[] = [
            'title' => get_sub_field('title'),
            'hint'  => get_sub_field('hint'),
            'url'   => get_sub_field('url')
        ];
    endwhile;
endif;

if( have_rows('how_it_works') ):
    while( have_rows('how_it_works') ): the_row();
        $howItWorks[] = [
            'image_desktop' => get_sub_field('image_desktop'). '?v=3',
            'image_mobile'  => get_sub_field('image_mobile'). '?v=3',
            'icon_class'    => get_sub_field('icon_class'),
            'title'         => get_sub_field('title'),
            'description'   => get_sub_field('description')
        ];
    endwhile;
endif;

if( have_rows('our_lender_network') ):
    while( have_rows('our_lender_network') ): the_row();
        $ourLenderNetwork[] = [
            'main'      => get_sub_field('main'),
            'secondary' => get_sub_field('secondary')
        ];
    endwhile;
endif;

if( have_rows('choose_your_reason') ):
    while( have_rows('choose_your_reason') ): the_row();
        $chooseYourReason[] = [
            'title'         => get_sub_field('title'),
            'image_desktop' => get_sub_field('image_desktop'). '?v=3',
            'image_mobile'  => get_sub_field('image_mobile'). '?v=3',
            'url'           => get_sub_field('url')
        ];
    endwhile;
endif;

if( have_rows('featured_on') ):
    while( have_rows('featured_on') ): the_row();
        $featuredOn[] = [
            'image'  => get_sub_field('image'). '?v=3',
            'title'  => get_sub_field('title')
        ];
    endwhile;
endif;

if( have_rows('cta_block') ):
    while( have_rows('cta_block') ): the_row();
        $ctaBlock = [
            'title'       => get_sub_field('title'),
            'button_text' => get_sub_field('button_text'),
            'url'         => get_sub_field('url'),
        ];
    endwhile;
endif;

?>

    <div class="top-content">
        <div class="hero-box text-center" style="background-image: url(<?= imgpath('/wp-content/themes/lendgenius/style-2018/img/hero-image-min.png?v=3')?>)">
            <div class="container">
                <h1><?= get_post_meta(get_the_ID(), 'page_title', true); ?></h1>
                <div class="subtitle"><?= get_post_meta(get_the_ID(), 'subtitle', true); ?></div>

                <div class="loan-amount-slider">
                    <div class="sum">$<span>5,000</span></div>
                    <div class="want">I want to borrow</div>

                    <div id="slider">
                        <div id="slider-body"></div>
                        <input type="text" id="loan-emount-slider-line" name="example_name" value="" />
                        <div class="irs-min">$1,000</div>
                        <div class="irs-max">$35,000</div>
                    </div>
                </div>

                <div class="buttons">
                    <a id="get-started-button" target="_blank" data-popunder="https://www.amazingoffersforyou.com/?campaignId=2eca4e0a-54f8-4915-8a23-15f1003682bd&hitId=606ef361-68ef-4eed-93cf-21b12569d1e8" href="/loan-request?RequestedAmount=5000" class="btn-yellow">Get Started</a>
                </div>
            </div>
        </div>

        <?php if ($ourLenderNetwork): ?>
            <div class="stats-box">
                <div class="container">
                    <h2>Our Lender Network</h2>
                    <div class="items">
                        <?php foreach ($ourLenderNetwork as $item): ?>
                            <div class="item">
                                <div class="main"><?= $item['main']; ?></div>
                                <div class="secondary"><?= $item['secondary']; ?></div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        <?php endif; ?>
    </div>
    <!--top-content-->

<?php if($howItWorks): ?>
    <div id="how-it-works" class="how-it-works-box">
        <div class="container">

            <h2>How It Works</h2>

            <div class="left-side">
                <ul>
                    <?php
                    $images = '';

                    foreach ($howItWorks as $i => $item):
                        $args = [
                            $i,
                            (!$i) ? 'active' : '',
                            imgpath(get_stylesheet_directory_uri() . '/style-2018/img/empty.png?v=3'),
                            imgpath($item['image_desktop']),
                            $item['title']
                        ];
                        $images .= vsprintf('<div id="show-work-%d-container" class="%s" ><img class="img-lazy img-lazy-desktop" src="%s" data-src="%s" alt="%s"></div>',  $args);
                        ?>
                        <li id="show-work-<?= $i;?>" class="<?= (!$i)? 'active' : ''; ?>">
                            <i class="icon <?= $item['icon_class'];?>"></i>
                            <h3><?= $item['title'];?></h3>
                            <div class="xs-img">
                                <img class="img-lazy img-lazy-mobile" data-src="<?= imgpath($item['image_mobile']);?>" alt="<?= $item['title'];?>">
                            </div>
                            <div class="hint">
                                <?= $item['description'];?>
                            </div>

                            <div class="progressbar"></div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>

            <div class="right-side">
                <div class="img-show">
                    <?= $images; ?>
                </div>
            </div>
        </div>
    </div>
    <!--how-it-works-box-->
<?php endif; ?>

<?php if($chooseYourReason): ?>
    <div class="choose-your-reason-box">
        <div class="container">
            <h2>Choose Your Reason</h2>
            <div class="items">
                <?php foreach ($chooseYourReason as $i => $item): ?>
                    <div class="item item-<?= $i ;?>">
                        <a href="<?= $item['url']; ?>">
                            <div class="img-desktop bg-lazy bg-lazy-desktop" data-style="background-image: url(<?= imgpath($item['image_desktop']) ?>);"></div>
                            <div class="img-mobile bg-lazy bg-lazy-mobile" data-style="background-image: url(<?= imgpath($item['image_mobile']) ?>);"></div>
                            <div class="name"><?= $item['title']; ?></div>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <!--choose-your-reason-box-->
<?php endif; ?>

<?php if ($featuredOn) : ?>
    <div class="featured-box">
        <div class="container">
            <h2>Featured On</h2>
            <div class="items">
                <?php foreach ($featuredOn as $item) : ?>
                    <div class="item <?php if (strpos($item['image'], 'e_logo')) { echo "mobile-hidden"; } ?>">
                        <img src="<?= $item['image']; ?>" alt="<?= $item['title']; ?>" class="logo">
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <!--featured-box-->
<?php endif; ?>
    <div class="posts-box">
        <h2>Class Is In Session</h2>
        <div class="subtitle">
            Learn about personal loans and more from a LendGenius Professor
        </div>

        <div class="container">
            <div class="items">

                <?php foreach ($lastPosts as $key => $lastPost): ?>
                    <div class="item clearfix">
                        <a href="<?php echo get_permalink($lastPost->ID); ?>">
                            <div class="img-container">
                                <div class="img bg-lazy" data-style="background-image: url(<?= get_the_post_thumbnail_url($lastPost->ID, ($key>0) ? 'thumbnail' : 'post-thumb-general') ?>);"></div>
                            </div>
                            <div class="post-content">
                                <h3><?= preg_replace('/^-{1,}/', '', $lastPost->post_title); ?></h3>
                                <p>
                                    <?= strip_shortcodes(wp_trim_words($lastPost->post_content, 21)); ?>
                                </p>
                            </div>
                        </a>
                    </div>
                <?php endforeach;?>
            </div>
        </div>
    </div>
    <!--posts-box-->
<?php if ($ctaBlock):?>
    <div class="cta-yellow">
        <div class="container">
            <h2><?= $ctaBlock['title']; ?></h2>
            <a href="<?= $ctaBlock['url']; ?>" class="btn-blue"><?= $ctaBlock['button_text']; ?></a>
        </div>
    </div>
    <!--cta-yellow-->
<?php endif;?>

<?php get_footer(); ?>