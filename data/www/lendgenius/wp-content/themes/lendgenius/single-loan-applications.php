<?php
/*
    Template Name: Reject-page
*/
get_header();

$the_ID = get_the_ID();

$headline    = get_post_meta($the_ID,'headline',true);
$text        = get_post_meta($the_ID,'text',true);
$bottom_text = get_post_meta($the_ID,'bottom_text',true);
$phone       = get_post_meta($the_ID,'phone',true);

?>
    <style>
        header,footer {
            display: none;
        }
        body {
            padding-top: 0 !important;
        }
    </style>
    <div class="body-rejection">
        <div class="container-rejection">
            <div class="header_only-logo">
                <a href="/">
                    <img src="\wp-content\themes\lendgenius\assets\images\logo.svg">
                </a>
            </div>
        </div>
        <div class="container-rejection">
            <div class="white-bg panel-reject">
                <h1 class="panel-reject_title">
                    <?= $headline; ?>
                </h1>
                <img src="\wp-content\themes\lendgenius\assets\images\emoji-negative.svg" alt="Face Negative" class="img-responsive center-block emoji-face">
                <?= do_shortcode($text); ?>
                <div class="hr-success"></div>

                <?php while( have_rows('button') ): the_row();
                    // vars

                    if ( get_sub_field('active') ) {
                        $title  = get_sub_field('title');
                        $text_on_button = get_sub_field('text_on_button');
                        $buttons_link   = get_sub_field('buttons_link');
                    ?>
                        <p><?= $title; ?></p>
                        <a href="<?= $buttons_link; ?>" class="btn btn-success btn-x2"><?= $text_on_button; ?></a>
                    <?php } ?>
                <?php endwhile; ?>
                <span>
                    <?= $bottom_text; ?>
                </span>
                <a class="panel-reject_tel" href="tel:<?= str_replace(' ','',$phone); ?>"><?= $phone; ?></a>
            </div>
            <div class="list-posts-wrapper">
                <?php while( have_rows('related_posts') ): the_row();
                    // vars
                    $data['blog_article'] = get_sub_field('blog_article');
                    ?>
                    <div class="blog-item-sub">
                        <div class="blog-item-sub-img">
                            <a href="<?= get_the_permalink($data['blog_article']->ID); ?>">
                                <?php echo get_the_post_thumbnail($data['blog_article']->ID,'author-thumb-posts'); ?>
                            </a>
                        </div>
                        <h4 class="blog-item-sub-title matchheight">
                            <a href="<?= get_the_permalink($data['blog_article']->ID); ?>"><?= $data['blog_article']->post_title; ?></a>
                        </h4>
                        <div class="blog-item-sub-author">
                            <span><a href="<?= get_permalink(get_post_meta( $data['blog_article']->ID , 'author' , true )); ?>"><?= get_the_title(get_post_meta( $data['blog_article']->ID , 'author' , true )); ?></a></span> <i class="fa fa-circle" aria-hidden="true"></i> <span><?= get_the_date('',$data['blog_article']->ID) ?></span>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
<?php
get_footer();