<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 3/21/17
 * Time: 2:57 PM
 */

?>

<div class="content-how-much">
    <div class="container">
        <div class="how-much-wraper">
            <div class="how-much-title">Are You Ready To See Your Options ?</div>
            <div class="how-much-form">
                <div class="row">
                    <div class="col-lg-12">
                        <a href="#" class="btn btn-success arrow-to-top" type="submit">See My Options</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
