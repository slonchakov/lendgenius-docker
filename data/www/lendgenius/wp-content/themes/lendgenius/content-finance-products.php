<section class="finance_products">
    <div class="container">
        <div class="row">
            <h2 class="main_title text-center">Learn More About Business Loans & Finance Products</h2>

            <div class="grid">
                <div class="product_line">
                    <?php
                    // The Query
                    $args = array(
                        'post_type' => 'loans',
                        'posts_per_page' => 10,
                        'orderby' => 'ID',
                        'order' => 'ASC'
                    );
                    $query = new WP_Query( $args );
                    if ( $query->have_posts() ) {
                        // The Loop
                        while ( $query->have_posts() ) : $query->the_post();

                            get_template_part( 'content-business-loan-icons', get_post_format() );

                        endwhile;
                        wp_reset_postdata();
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>