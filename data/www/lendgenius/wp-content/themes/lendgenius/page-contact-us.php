<?php
/*
    Template Name: Contact Us
*/

    if ( function_exists( 'wpcf7_enqueue_scripts' ) ) {
        wpcf7_enqueue_scripts();
    }

    if ( function_exists( 'wpcf7_enqueue_styles' ) ) {
        wpcf7_enqueue_styles();
    }


get_header();
?>
<div class="page-contact-us">
    <div class="page-head">
        <div class="container">
            <h1><?php the_title(); ?></h1>
        </div>
    </div>
    <div class="page-body">
        <div class="container">

            <div class="row">

                <div class="col-md-7">
                    <div class="page-contact-area">
                        <?php the_excerpt(); ?>
                    </div>
                </div>
                <div class="col-md-5">
                    <?= do_shortcode('[embed-form]'); ?>
                </div>
            </div>
        </div>

    </div>
</div>
<?php
get_footer();
?>