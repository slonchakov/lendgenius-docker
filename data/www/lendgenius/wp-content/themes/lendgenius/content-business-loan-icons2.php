<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 09.01.17
 * Time: 19:01
 */
$loan_icon = get_post_meta( get_the_ID(), 'loan_icon', true );
?>
<li>
    <a href="<?php echo get_permalink(); ?>">
        <i class="<?php echo $loan_icon; ?>"></i>
        <?php the_title(); ?>
    </a>
</li>
