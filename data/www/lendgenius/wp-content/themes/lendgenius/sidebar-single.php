<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 10.10.16
 * Time: 13:20
 */

if ( is_active_sidebar( 'sidebar-single' )  ) : ?>
    <div class="content-aside">
        <?php dynamic_sidebar( 'sidebar-single' ); ?>
    </div>
<?php endif; ?>