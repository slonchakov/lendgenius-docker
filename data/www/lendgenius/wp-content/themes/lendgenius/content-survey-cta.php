<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 15.01.17
 * Time: 15:35
 */

?>
<div class="survey-cta wrapper">
    <form action="#" method="#" id="steps">
        <div class="steps-wrapper steps-wrapper_one">
            <div class="steps-section_top">
                <h3>Funding a business loan shouldn’t be complicated</h3>
            </div>
            <div class="steps-section_middle">
                <div class="steps-section_wrapper">
                    <div class="steps-section_fullwidth" style="transition: all 1s ease-out 0s;">
                        <div class="wrapper-section wrapper-section_input active">
                            <div class="wrapper-section_select">
                                <select name="speed" id="speed">
                                    <option selected="selected">How much do you need?</option>
                                    <option>$100,00</option>
                                    <option>$1 000,00</option>
                                    <option>$10 000,00</option>
                                    <option>$100 000,00</option>
                                </select>
                            </div>
                        </div>
                        <div class="wrapper-section wrapper-section_input">
                            <div class="wrapper-section_select">
                                <select name="speed" id="speed">
                                    <option selected="selected">How's your credit?</option>
                                    <option>$100,00</option>
                                    <option>$1 000,00</option>
                                    <option>$10 000,00</option>
                                    <option>$100 000,00</option>
                                </select>
                            </div>
                        </div>
                        <div class="wrapper-section wrapper-section_input">
                            <div class="wrapper-section_select">
                                <select name="speed" id="speed">
                                    <option selected="selected">What are your monthly sales?</option>
                                    <option>$100,00</option>
                                    <option>$1 000,00</option>
                                    <option>$10 000,00</option>
                                    <option>$100 000,00</option>
                                </select>
                            </div>
                        </div>
                        <div class="wrapper-section wrapper-section_input">
                            <div class="wrapper-section_select">
                                <select name="speed" id="speed">
                                    <option selected="selected">How long have you been in business?</option>
                                    <option>$100,00</option>
                                    <option>$1 000,00</option>
                                    <option>$10 000,00</option>
                                    <option>$100 000,00</option>
                                </select>
                            </div>
                        </div>

                    </div>

                </div>
                <a id="submit" class="stepNext"/>Next</a>
            </div>
            <div class="steps-section_bottom">
                <p class="steps">1/4</p>
                <div class="progress-wrapper">
                    <div class="progress-wrapper_color" style="width:25%"></div>
                </div>
            </div>
        </div>
    </form>
</div>
