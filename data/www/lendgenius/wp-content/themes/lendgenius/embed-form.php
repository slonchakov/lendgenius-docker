<?php
    $formUrl = (strpos($_SERVER['HTTP_HOST'], 'dev') === false) ? 'formrequests.com' : 'formrequests-dev.azurewebsites.net';
?>
<!doctype html>
<html>
<head>
    <title></title><meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
</head>
<body>
<div>
    <jsf-form></jsf-form>
    <script>
        window.lmpost = window.lmpost || {};
        window.lmpost.options = {
            leadtypeid: 44,
            campaignKey: '8A20B784-A3E2-4D32-8ACE-58D1F821C099',
            campaignid: '246399'
        };
    </script>
    <script async src="https://<?= $formUrl; ?>/personal/1q_h_v1/form-loader.js"></script>
</div>
</body>
</html>
