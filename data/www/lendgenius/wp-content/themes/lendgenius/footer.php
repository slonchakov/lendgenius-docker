<?php
$template_directory_uri   = get_template_directory_uri();
$stylesheet_directory_uri = get_stylesheet_directory_uri();
$sts_script_version       = get_option( 'sts_script_version' );
$page_template            = get_post_meta( $post->ID, '_wp_page_template', true );

if ( is_page_template( 'page-under-constraction.php' ) ||
	is_page_template( 'page-blank-iframe.php' ) ||
    is_page_template( 'landing-gmail-1.php' ) ||
	is_404() )
{
    ?>
    <script src="<?php echo $template_directory_uri; ?>/assets/js/jquery-1.11.2.min.js"></script>
    <script src="<?php echo $template_directory_uri; ?>/js/jquery.mask.min.js"></script>
    <script src="<?php echo $template_directory_uri; ?>/assets/js/bootstrap.min.js"></script>
    <?php
    wp_footer();
    if(is_page_template( 'landing-gmail-1.php' )){
        if(get_option( 'sts_enable_form' ) == 'on'){ ?>
            <script type="text/javascript">
                window.applicationOptions = {
                    sendShortForm: '<?php echo get_option( 'sts_endpoint_url' ); ?>',
                    leadsource: '<?php echo get_option( 'sts_leadSource' ); ?>',
                    c: <?php echo get_option( 'sts_campaignId' ); ?>,
                    zipCode: '<?php echo get_option( 'sts_zipcode' ); ?>',
                    emailValidateUrl: '<?php echo get_option( 'sts_email_v_url' ); ?>',
                    phoneValidateUrl: '<?php echo get_option( 'sts_phone_v_url' ); ?>',
                    RequestedAmount: '<?php echo $_POST['RequestedAmount']; ?>',
                    CreditScore: '<?php echo $_POST['CreditScore']; ?>',
                    MonthlySales:'<?php echo $_POST['MonthlySales']; ?>',
                    MonthsInBusiness:'<?php echo $_POST['MonthsInBusiness']; ?>'
                }
            </script>
<!--            <script src="--><?php //echo get_option( 'sts_script_url' ); ?><!--/form_3/form-loader.js?ver=--><?php //echo get_option( 'sts_script_version' ); ?><!--"></script>-->
        <?php } ?>
        <?php
    }
    echo '</body></html>';
    return;
}
?>

<footer id="footer">

    <div class="container">
            <?php wp_nav_menu([
                'menu' => 'New Footer Menu',
                'container' => '',
                'items_wrap' => '<ul class="footer-nav">%3$s</ul>'
            ]); ?>

        <div class="social-buttons">
            <ul>
                <li><a aria-label="twitter" href="<?= get_option('sts_twitter_url');?>"><i class="icomoon-twitter"></i></a></li>
                <li><a aria-label="facebook" href="<?= get_option('sts_facebook_url');?>"><i class="icomoon-facebook"></i></a></li>
            </ul>
        </div>

        <div class="copyright">Copyright © <?= date('Y');?> LendGenius.com. All Rights Reserved</div>
    </div>

    <?php
    if( strpos($page_template,'page-landing') !== false  && !is_search() ) {
    ?>
    <style>
        footer{
            position: relative;
        }
    </style>
    <div class="secureLogo" style="position:absolute;right: 30px;bottom:19px;">
            <script type="text/javascript"> //<![CDATA[
                var tlJsHost = ((window.location.protocol == "https:") ? "https://secure.comodo.com/" : "http://www.trustlogo.com/");
                document.write(unescape("%3Cscript src='" + tlJsHost + "trustlogo/javascript/trustlogo.js' type='text/javascript'%3E%3C/script%3E"));
                //]]>
        </script>
            <script language="JavaScript" type="text/javascript">
            if (typeof TrustLogo === 'function') {
                TrustLogo("https://lendgeniuslandingstorage.azureedge.net/wp-uploads/2017/04/comodo_secure_seal_113x59_transp.png", "CL1", "none");
            }
        </script>
        <a  class="hide hidden" href="https://www.instantssl.com/" id="comodoTL">Essential SSL</a>

    </div>
    <?php } ?>
</footer>
</div>

<?php if( strpos($page_template,'page-landing') === false  ) { ?>
<div id="blog-disclosure-box" class="disclosure-box">
    <div class="container">
        <?= strip_shortcodes(get_page_by_path('personal')->post_content); ?>
    </div>
</div>
<?php } ?>

<?php wp_footer();  ?>

<?php if (!is_front_page()) : ?>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
<?php endif; ?>
<!--<script defer async src="https://www.sparning.com/hit/hit.core.js"></script>-->
<!-- inject:js -->
<!--<script src="/wp-content/themes/lendgenius/assets/js/bundle/assets-bundle.min-5fc39a089ac7d6e99ec10991bc5231c4.js"></script>-->
<!-- endinject -->

<script defer src="/wp-content/themes/lendgenius/style-2018/js/nouislider.min.js"></script>

<?php if (!empty($post) && $post->post_type == 'loans') : ?>
<script src="<?php echo $template_directory_uri; ?>/assets/js/bootstrap.min.js"></script>
<?php endif; ?>


<?php if (is_front_page()) : ?>
    <script src="/wp-content/themes/lendgenius/style-2018/js/front-page.js"></script>
<?php else: ?>
    <script src="/wp-content/themes/lendgenius/style-2018/js/bundle.js"></script>
    <script src="/wp-content/themes/lendgenius/style-2018/js/main.js"></script>

<?php endif; ?>



<?php if (is_page_template('page-funnel-3a.php')) { ?>
	<script src="<?= $stylesheet_directory_uri; ?>/LG_LP_UPD4/assets/js/owl.carousel.min.js"></script>
	<script src="<?= $stylesheet_directory_uri; ?>/LG_LP_UPD4/assets/js/main.js"></script>
<?php }


if ( is_page_template( 'publisher-partner.php' ) ||
    is_page_template( 'publisher-referral.php' ) ||
    is_page_template( 'publisher-affiliate-v2.php' ) ||
    is_page_template( 'publisher-referral-v2.php' ) ||
    is_page_template( 'publisher-affiliate.php' ) ) {
    ?>
    <script src="<?php echo get_stylesheet_directory_uri() ?>/assets/js/jquery.matchHeight.js"></script>
    <script>
    <?php if(is_page_template( 'publisher-affiliate-v2.php' )) {
    ?>
    function simpleAccordion() {
        $('.accordion-header').click(function() {
            var $this = $(this),
                accordionHolder = $this.parent(),
                accordionBody = accordionHolder.find('.accordion-body');
            accordionHolder.toggleClass('open');
            accordionBody.slideToggle();
        });
    }
    jQuery(document).ready(function () {
        $('.match-height').matchHeight();
        $('.match-height-always').matchHeight({
            byRow: false
        });

        simpleAccordion();
    });
    <?php
    } elseif (is_page_template( 'publisher-referral-v2.php' )) {
        ?>
    jQuery(document).ready(function ($) {
        $('.match-height').matchHeight();
        $('.match-height-always').matchHeight({
            byRow: false
        });
    });
    <?php
    } else { ?>
    jQuery(document).ready(function ($) {
            $('.match-height').matchHeight();
        });
    <?php } ?>
    </script>
    <?php
} elseif (is_page_template( 'page-see-loan-options.php' )) { ?>
    <script src="<?php echo $template_directory_uri; ?>/assets/js/bootstrap.min.js"></script>
    <script>
        var
            holderTitleCol = $('.col-bg_img .title-col'),
            holderTitleColWithoutBg = $('.col-without-bg_img .title-col'),
            contentTitleCol = holderTitleColWithoutBg.html(),
            contentTitleForm = $('.col-bg_img .title-col').html();
        function changePlaceTitle() {
            if ($(window).width() < 768) {
                $('.col-without-bg_img .title-col').find('h2').remove();
                holderTitleCol.find('h2').remove();
                holderTitleCol.append(contentTitleCol);

            } else {
                holderTitleCol.find('h2').remove();
                holderTitleCol.append(contentTitleForm);
                $('.col-without-bg_img .title-col').find('h2').remove();
                $('.col-without-bg_img .title-col').append(contentTitleCol);
            }

        }

        function accordionMobileQuestions() {
            jQuery('.btn-accordion').click(function() {
                $this = $(this),
                    accordionItem = $(this).closest('.accordion-holder').find('.accordion-body');
                $this.closest('.accordion-controls').slideUp();
                accordionItem.slideToggle();
            });
        }


        jQuery(document).ready(function($) {
            changePlaceTitle();
            accordionMobileQuestions();
            $(window).resize(function () {
                changePlaceTitle();
            });
        });
    </script>
    <?php
}

if (is_single()) {
    ?>
    <script src="<?= get_stylesheet_directory_uri() ?>/assets/js/additional_scripts.js?ver=<?= $sts_script_version; ?>"></script>
    <?php
}
?>
<?php
$active = 0;
if (is_front_page() && $active == 1) { ?>
    <script type='application/ld+json'>
    {
        "@context": "http://schema.org/",
        "@type": "Organization",
        "name": "LendGenius",
        "description": "LendGenius is an online business loan matching platform connecting small business owners with multiple lenders.",
        "email": "support@lendgenius.com",
        "logo": "https://www.lendgenius.com/wp-content/uploads/2017/04/lendgenius-thumbnail.png",
        "telephone": "18003480997",
    "url": "https://www.lendgenius.com",
        "address": {
            "@type": "PostalAddress",
            "addressCountry": "United States",
            "addressLocality": "Woodland Hills",
            "addressRegion": "California",
            "postalCode": "91367",
            "streetAddress": "21600 Oxnard St. #400",
            "email": "support@lendgenius.com",
            "telephone": "+18003480997",
            "name": "LendGenius"
        },
    "sameAs" :  [ "https://www.facebook.com/lendgenius",
    "https://twitter.com/lendgenius",
    "https://plus.google.com/114339560599887475172",
    "https://www.linkedin.com/company/lendgenius",
    "https://www.instagram.com/lendgenius/",
    "https://www.youtube.com/channel/UCCv7gNH7RQLQB0MoaHSIlCw/" ]}
    </script>
    <?php
}
?>

<?php
global $form_number;
if ($form_number) {
?>

<script type="text/javascript">
    window.applicationOptions = {
        sendShortForm:    '<?= get_option( 'sts_endpoint_url' ); ?>',
        c:                 <?= get_option( 'sts_campaignId' ); ?>,
        leadsource:       '<?= get_option( 'sts_leadSource' ); ?>',
        zipCode:          '<?= get_option( 'sts_zipcode' ); ?>',
        emailValidateUrl: '<?= get_option( 'sts_email_v_url' ); ?>',
        phoneValidateUrl: '<?= get_option( 'sts_phone_v_url' ); ?>'
    }
</script>
<!--<script type="text/javascript" src="--><?//= get_option( 'sts_script_url' ); ?><!--/form_--><?//= $form_number;?><!--/form-loader.js?ver=--><?//= get_option( 'sts_script_version' ); ?><!--"></script>-->
<?php } ?>

<script>
    var page = <?= get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1 ;?>;

    if (page > 1) {
        pageNumberBlog = page;
    }
</script>

<script defer async src='//cdn.freshmarketer.com/182106/488542.js'></script>

</body>
</html>
