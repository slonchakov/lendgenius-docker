<?php
/*
Template Name: Blog Page(v2)
*/
get_header();
?>

    <div class="page-blog">
        <div class="container">

            <h1>Blog<?= get_query_var( 'paged' ) ? ' page '. intval( get_query_var( 'paged' ) ) : '' ;?></h1>

            <div class="row">
                <div class="col-xs-12 col-lg-9 blog-content-holder">
                    <div class="content-blog">
                        <div class="blog-frame" id="ajax-posts">
                            <?php
                            //Top Articles
                            $args_1 = array(
                                'post_type' => 'post',
                                'post_status' => 'publish',
                                'meta_key'   => 'top_article',
                                'meta_value'   => '77',
                                'posts_per_page' => 1
                            );
                            $query_1 = new WP_Query( $args_1 );
                            if ( $query_1->have_posts() ) {
                                // The Loop
                                while ( $query_1->have_posts() ) : $query_1->the_post();
                                    get_template_part( 'views/content/blog/general-post', get_post_format() );
                                endwhile;
                                wp_reset_postdata();
                            }
                            ?>
                            <div class="list-posts-wrapper">
                                <?php
                            // The Query
                            $args_2 = array(
                                'post_type' => 'post',
                                'post_status' => 'publish',
                                'meta_key'   => 'top_article',
                                'meta_value'   => '1',
                                'posts_per_page' => 4
                            );
                            $query_2 = new WP_Query( $args_2 );
                            if ( $query_2->have_posts() ) {
                                // The Loop
                                while ( $query_2->have_posts() ) : $query_2->the_post();
                                    get_template_part( 'views/content/blog/sub-post', get_post_format() );
                                endwhile;
                                wp_reset_postdata();
                            }
                            ?>
                            </div>
                            <?php
                            // The Query
                            $args = array(
                                'post_type' => 'post',
                                'post_status' => 'publish',
                                'orderby' => 'post_date',
                                'order' => 'DESC',
                                'posts_per_page' => 15,
                                'paged' => get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1
                            );
                            $query = new WP_Query( $args );
                            $post_count = $query->post_count;
                            $cc_post = 0;
                            if ( $query->have_posts() ) {
                                // The Loop
                                while ( $query->have_posts() ) : $query->the_post();
                                    if($cc_post == 0 || ($cc_post % 5) == 0){
                                        get_template_part( 'views/content/blog/general-post', get_post_format() );
                                        ?>
                                        <div class="list-posts-wrapper">
                                        <?php
                                    }else{
                                        get_template_part( 'views/content/blog/sub-post', get_post_format() );
                                    }

                                    $cc_post += 1;
                                    if(($cc_post % 5) == 0 || $cc_post == $post_count){
                                        ?>
                                        </div>
                                        <?php

                                        if ($cc_post == 5) {
                                            echo do_shortcode('[embed-form]');
                                        }
                                    }

                                endwhile;
                                wp_reset_postdata();
                            }
                            ?>
                        </div>
                    </div>
                    <div class="content-blog-reamore text-center">
                        <?php if(!get_query_var( 'paged' )) : ?>
                        <button class="btn btn-success-custom" type="button" id="more_posts_blog">Load More <i class="fa fa-angle-down" aria-hidden="true"></i></button>
                        <?php else: ?>
                            <div class="paginate-links <?php if(get_query_var( 'paged' ) && get_query_var( 'paged' ) == 1) : ?>first<?php endif;?>">
                                <?= showPaginate(get_query_var( 'paged' ));?>
                            </div>
                        <?php endif;?>
                    </div>
                </div>
                <div class="col-xs-12 col-lg-3">
                    <div class="content-aside-holder">
                        <?php get_sidebar('blog'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php

get_footer();