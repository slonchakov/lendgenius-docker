<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 10.10.16
 * Time: 20:11
 */
?>
<div class="blog-item">
    <div class="item-wraper">
        <div class="item-image item-image-opacity">
            <a href="<?php echo get_permalink(); ?>">
                <?php
                // Post thumbnail.
                single_post_thumbnail();
                ?>
            </a>
            <div class="item-title"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></div>
        </div>
        <div class="item-content">
            <div class="item-author">by <strong><?php echo get_the_author(); ?></strong> on <?php the_date() ?></div>
            <div class="item-text"><?php echo wp_trim_words(get_the_content()); ?></div>
            <!--div class="item-social">
                <ul class="list-inline sprites item-social-list">
                    <li class="item-social-icon"><a href="#"><span class="sprite icon-comments"></span><span>12</span></a></li>
                    <li class="item-social-icon"><a href="#"><span class="sprite icon-facebook"></span><span>319</span></a></li>
                    <li class="item-social-icon"><a href="#"><span class="sprite icon-twitter"></span><span>319</span></a></li>
                </ul>
            </div-->
            <div class="item-readmore"><a href="<?php the_permalink(); ?>">Read more</a></div>
        </div>
    </div>
</div>