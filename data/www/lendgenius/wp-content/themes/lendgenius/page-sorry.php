<?php
/*
    Template Name: Sorry page
*/
get_header();
?>
<div class="page-info-holder">
    <div class="page-info-banner">
        <div class="container">
            <h1 class="page-info-banner-title">Sorry!</h1>
        </div>
    </div>
    <div class="page-info-content">
        <div class="page-info-content-container">
            <div class="page-info-content-head">
                <p>Unfortunately your application didn't match any of our lenders.</p>
            </div>
            <p>Please take a look at our <a href="/blog/">blog page</a> to find more information about small business loans.</p>
        </div>
    </div>
</div>
<?php
get_footer();
