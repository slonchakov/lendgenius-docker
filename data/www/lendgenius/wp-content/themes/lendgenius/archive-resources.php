<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 19.10.16
 * Time: 20:51
 */

get_header();
?>
<div class="content-funnel funnel-1">
    <section class="visual" style="background-image:url(<?php echo get_stylesheet_directory_uri() ?>/assets/images/bg-visual-block.jpg");>
        <div class="container">
            <div class="cycle-gallery gallery">
                <div class="mask">
                    <div class="slideset">
                        <div class="slide">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/img-card.png" alt="credit card" class="pull-right">
                            <div class="text-holder">
                                <h2>Find the best credit cards for you.</h2>
                                <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum...</p>
                            </div>
                        </div>
                        <div class="slide">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/img-card.png" alt="credit card" class="pull-right">
                            <div class="text-holder">
                                <h2>Find the best credit cards for you.</h2>
                                <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum...</p>
                            </div>
                        </div>
                        <div class="slide">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/img-card.png" alt="credit card" class="pull-right">
                            <div class="text-holder">
                                <h2>Find the best credit cards for you.</h2>
                                <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum...</p>
                            </div>
                        </div>
                        <div class="slide">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/img-card.png" alt="credit card" class="pull-right">
                            <div class="text-holder">
                                <h2>Find the best credit cards for you.</h2>
                                <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum...</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pagination"></div>
            </div>
        </div>
    </section>
    <section class="resources-section">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-4">
                    <h2><?php the_title(); ?></h2>
                    <?php
                    // The Query
                    $args = array(
                        'post_type' => 'page',
                        'name' => 'resources'
                    );
                    $query = new WP_Query( $args );
                    if ( $query->have_posts() ) {
                        // The Loop
                        while ($query->have_posts()) : $query->the_post(); ?>
                            <p><?php echo strip_tags(get_the_content()); ?></p>
                            <?php
                        endwhile;
                    }?>
                </div>
                <div class="col-xs-12 col-md-8">
                    <ul class="list-unstyled resources-list text-center">
                        <li>
                            <a href="/resources/loan-calculators/">
                                <i class="icon-calculator"></i>
                                Calculators
                            </a>
                        </li>
                        <li>
                            <a href="/best-business-credit-cards/">
                                <i class="icon-credit-cards"></i>
                                Business <br>Credit Cards
                            </a>
                        </li>
                        <li>
                            <a href="/resources/tools-and-templates/">
                                <i class="icon-tools"></i>
                                Tools & Templates
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
</div>
<?php
//get_template_part( 'content-how-much', get_post_format() );
?>
<section class="prt-from-article">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-lg-8">
                <div class="content-blog content-sub">
                    <?php //get_template_part( 'content-top-articles', get_post_format() ); ?>
                </div><span class="lendgenius-banner"><a class="btn btn-success" href="<?php echo cta_button_link(); ?>">See Loan Options</a></span>
            </div>
            <div class="col-xs-12 col-lg-4">
                <?php
                //Sidebar
                get_sidebar('how-it-works');
                ?>
            </div>
        </div>
    </div>
</section>
<?php
//Features Block
get_template_part( 'content-features-block', get_post_format() );

get_footer();
?>
