<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 20.10.16
 * Time: 12:52
 */

$loan_icon = get_post_meta( get_the_ID(), 'loan_icon', true );
?>

<li>
    <a href="<?php echo get_permalink(); ?>">
        <i class="<?php echo $loan_icon; ?>"></i>
        <?php the_title(); ?>
    </a>
</li>
