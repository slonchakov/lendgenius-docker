<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 19.10.16
 * Time: 20:34
 */

get_header();

// The Query
$args = array(
    'post_type' => 'page',
    'name' => 'business-loans'
);
$query = new WP_Query( $args );
if ( $query->have_posts() ) {
    while ($query->have_posts()) : $query->the_post();
        $bpost_id     = get_the_ID();
        $sub_title    = get_post_meta( $bpost_id, 'sub_title', true );
        $sub_title2   = get_post_meta( $bpost_id, 'sub_title2', true );
        $title        = get_post_meta( $bpost_id, 'title', true );
        $post_title   = get_the_title();
        $content_post = get_post($bpost_id);
        $content      = $content_post->post_content;
        $content      = apply_filters('the_content', $content);
        $content      = str_replace(']]>', ']]&gt;', $content);
    endwhile;
}else{
    $bpost_id = $sub_title = $content = $title = $post_title = '';
}
wp_reset_postdata();
?>
    <section class="slogan-section" >
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="title-page">
                        <?= $title; ?>
                    </h1>
                </div>
            </div>
        </div>
    </section>
    <div class='lightgray-bg'>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <?php
                        breadcrumbs('<div id="breadcrumbs" class="breadcrumbs text-center-xs">','</div>', true, '<span class="separator"></span>', $post_title);
                    ?>
                </div>
            </div>
        </div>
    </div>
    <section class="lg_loans find_loan lightgray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                   <h2><?= $sub_title; ?></h2>
                </div>
            </div>
            <div class="row filters-app-holder">

              <filters-app>
                  <div class="spinner-holder" *ngIf="loadingService.loading" >
                      <div class="spinner">
                          <div class="double-bounce1"></div>
                          <div class="double-bounce2"></div>
                      </div>
                      <div class="spinner-text">Loading Loan Options...</div>
                  </div>
              </filters-app>
              <!--script async src="http://localhost:9045/filters.js"></script-->
              <script async src="/wp-content/themes/lendgenius/assets/js/FiltersSPA/dist/filters.js?ver=<?= get_option( 'sts_script_version' ); ?>"></script>

            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="big-first-latter-paragraph">
                        <?= $content; ?>
                        <a href="<?= cta_button_link(); ?>" class="btn btn-success btn-x2 margin-top-30">See Loan Options</a>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="finance_products">
                        <div class="grid finance_products-slick">
                            <?php
                            $slider_count = 1;
                            $new_slide    = false;
                            $loans_data   = array();
                            if( have_rows('loans', $bpost_id) ) {
                                while (have_rows('loans', $bpost_id)) {
                                    the_row();
                                    // vars
                                    $icon        = get_sub_field('icon');
                                    $title       = get_sub_field('title');
                                    $description = get_sub_field('description');
                                    $permalink   = get_sub_field('permalink');
                                    $units       = array();
                                    if (have_rows('loan_units', $bpost_id)) {
                                        while (have_rows('loan_units', $bpost_id)) {
                                            the_row();
                                            $units[] = array(
                                                    'icon'        => get_sub_field('icon'),
                                                    'title'       => get_sub_field('title'),
                                                    'description' => get_sub_field('description')
                                            );
                                        }
                                    }
                                    $loans_data[] = array(
                                            'icon'        => $icon,
                                            'title'       => $title,
                                            'description' => $description,
                                            'permalink'   => $permalink,
                                            'units'       => $units
                                    );
                                    if($slider_count == 1){
                                        ?>
                                        <div class="product_line">
                                        <?php
                                    }
                                    if($new_slide){
                                        $new_slide = false;
                                        ?>
                                        </div>
                                        <div class="product_line">
                                        <?php
                                    }
                                    ?>
                                    <div class="finance_product">
                                        <a href="<?php echo $permalink; ?>">
                                            <div class="image">
                                                <i class="<?php echo $icon; ?>"></i>
                                            </div>
                                            <span class="h5"><?php echo $title; ?></span>
                                        </a>
                                    </div>
                                    <?php
                                    if($slider_count % 6 == 0 ){
                                        $new_slide = true;
                                    }
                                    $slider_count ++;
                                }
                                ?></div><?php
                            }
                            ?>
                        </div>
                        <?php if($slider_count > 6){ ?>
                        <div class="finance-slick-controls">
                            <span class="slick-controls slick-prev">
                                <i class="fa fa-angle-left"></i>
                            </span>
                            <span class="slick-controls slick-next">
                                <i class="fa fa-angle-right"></i>
                            </span>
                        </div>
                        <?php } ?>
                    </div>   
                </div>
            </div>
        </div>
    </section>

    <section class="lg_loans browse_loan">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 class="main_title"><?php echo $sub_title2; ?></h2>
                </div>
            </div>
            <div class="row browse_grid">
                <?php
                $loan_number = 1;
                if( !empty($loans_data) ) {
                    foreach ($loans_data as $loan_data) {
                        ?>
                        <div class="flex flex-column-xs line">
                            <div class="image-holder">
                                <div class="image">
                                    <a href="<?php echo $loan_data['permalink']; ?>">
                                        <i class="<?php echo $loan_data['icon']; ?>"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="content">
                                <div class="line-success">
                                    <h4 class="no-margin-cm"><a href="<?php echo $loan_data['permalink']; ?>" class="line-title"><?php echo $loan_data['title']; ?></a></h4>
                                    <?php echo $loan_data['description']; ?>
                                </div>
                                <a href="<?php echo $loan_data['permalink']; ?>" class="learn_more fs-18">Learn More <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                            </div>
                            <div class="small_grid">
                                <?php
                                if (!empty($loan_data['units'])) {
                                    foreach ($loan_data['units'] as $unit) {
                                        ?>
                                        <div class="col flex">
                                            <div class="image">
                                                <i class="<?php echo $unit['icon']; ?>"></i>
                                            </div>
                                            <div class="content">
                                                <div><b><?php echo $unit['title']; ?></b></div>
                                                <span><?php echo $unit['description']; ?></span>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                } ?>
                            </div>
                        </div>
                        <?php
//                        if ($loan_number == 5) {
                            ?>
<!--            </div>
        </div>-->

        <!--<div class="default-form-holder">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="default-form">
                        <?/*= do_shortcode("[lead form='2']"); */?>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->


        <!--<div class="container">
            <div class="row browse_grid">-->
                            <?php
//                        }
                        $loan_number ++;
                    }
                }
                ?>
        </div>
    </div>
    </section>
    <div class="button-section lightgray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <a href="<?= cta_button_link(); ?>" class="btn btn-success btn-x2">See My Options</a>
                </div>
            </div>
        </div>
    </div>

<?php

get_footer();
?>