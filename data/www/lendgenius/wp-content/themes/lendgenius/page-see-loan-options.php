<?php
/*
    Template Name: See Loan Options
*/
get_header();

$pre_title = get_post_meta( get_the_ID(), 'pre_title', true );

$question_1 = get_post_meta( get_the_ID(), 'question_1', true );
$answer_1 = get_post_meta( get_the_ID(), 'answer_1', true );
$question_2 = get_post_meta( get_the_ID(), 'question_2', true );
$answer_2 = get_post_meta( get_the_ID(), 'answer_2', true );
$question_3 = get_post_meta( get_the_ID(), 'question_3', true );
$answer_3 = get_post_meta( get_the_ID(), 'answer_3', true );
?>
<section class="lightgray-bg">
    <div class="container">
        <div class="row">
            <div class="col-bg_img">
                <div class="col--header">
                    <div class="title-col"> <h2>Commonly Asked Questions</h2></div>
                </div>
                <div class="accordion-holder">
                    <div class="col--body accordion-body">
                        <div class="question-holder">
                            <div class="h4-style">
                                <?php echo $question_1; ?>
                            </div>
                            <p><?php echo $answer_1; ?></p>
                        </div>
                        <div class="question-holder">
                            <div class="h4-style">
                                <?php echo $question_2; ?>
                            </div>
                            <p><?php echo $answer_2; ?></p>
                        </div>
                        <div class="question-holder">
                            <div class="h4-style">
                                <?php echo $question_3; ?>
                            </div>
                            <p><?php echo $answer_3; ?></p>
                        </div>
                    </div>
                    <div class="accordion-controls text-center">
                        <div class="pre-header">or</div>
                        <button class="btn btn-link btn-accordion" type="button">See Comonnly Asked Questions</button>
                    </div>
                </div>
            </div>
            <div class="col-without-bg_img get-started-form">
                <div class="col-lg-12">
                    <div class="title-col">
                        <h2>
                            <?php echo $pre_title; ?>
                        </h2>
                    </div>
                    <?php do_shortcode("[lead form='2']"); ?>
                </div>

            </div>
        </div>
    </div>
</section>
<?php
get_footer();