<?php
/*
Template Name: 404
*/

get_header(); ?>
    <style>
        header,footer {
            display: none;
        }
        body {
            padding-top: 0 !important;
        }
    </style>

<section class="page404">
    <div class="container">
        <div class="error_page_content">
            <h1>404</h1>
            <h2>Oops!</h2>
            <h3>This is awkward.</h3>
            <h4>We're sorry, but it looks like the page you were looking for couldn't be found.<br>Try one of these links instead:</h4>
            <div class="text-center">
                <a class="" href="/">Home</a> -
                <a class="" href="/get-started/">Get Started</a> -
                <a class="" href="/blog/">Blog</a>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>