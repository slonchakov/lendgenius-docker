<?php
/*
    Template Name: dev-page
*/
get_header();
?>
<div class="wrapper_body-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-3" style="margin-bottom: 30px;">
            <!--start CTA-1 section-->
                <div class="cta-section cta-section_small cta-1">
                    <div class="cta-logo-icon">
                        <i class="icomoon icomoon-idea-dollar"></i>
                    </div>
                    <div class="desk medium-plus text-center">
                        Find Out Which of These Business Loans You Qualify For
                    </div>
                    <a href="/apply-now/a" class="btn btn-default-success">See Loan Options</a>
                </div>
            <!--End CTA-1 section-->
            </div>
            <div class="col-lg-3" style="margin-bottom: 30px;">
            <!--start CTA-2 section-->
                <div class="cta-section cta-section_small cta-2 lightgray-bg">
                    <div class="cta-logo-icon">
                        <i class="icomoon icomoon-idea-dollar ico-gradient"></i>
                    </div>
                    
                    <div class="desk light-font text-center">
                        Apply to the Industry's Best Lenders with LendGenius Today!
                    </div>
                    <a href="/apply-now/a" class="btn btn-success">See Loan Options</a>
                </div>
            <!--End CTA-2 section-->
            </div>
            <div class="col-lg-3" style="margin-bottom: 30px;">
            <!--start CTA-3 section-->
                <div class="cta-section cta-section_small cta-3 lightgray-bg">
                    <div class="desk light-font text-center">
                        Compare dozens of small business loans at once
                    </div>
                    <a href="/apply-now/a" class="btn btn-success">See Loan Options</a>
                </div>
            <!--End CTA-3 section-->
            </div>
            <div class="col-lg-3" style="margin-bottom: 30px;">
            <!--start CTA-4 section-->
                <div class="cta-section cta-section_small cta-4 lightgray-bg">
                    <div class="cta-body">
                        <div class="bold h4 text-center">Ready to Grow Your Business?</div>
                        <div class="desk text-center">
                            It's free-prequalify without affecting your credit!
                        </div>
                    </div>
                    <div class="cta-footer">
                        <a href="/apply-now/a" class="btn btn-success">See Loan Options</a>
                    </div>
                </div>
            <!--End CTA-4 section-->
            </div>
            <div class="col-lg-3" style="margin-bottom: 30px;">
            <!--start CTA-5 section-->
                <div class="cta-section cta-section_small cta-5">
                    <div class="cta-logo-icon">
                        <i class="icomoon icomoon-idea-dollar"></i>
                    </div>
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/logo-cta.png" class="img-responsive center-block">
                    <div class="desk medium-plus text-center">
                        Find Out Which of These Business Loans You Qualify For
                    </div>
                    <a href="/apply-now/a" class="btn btn-default-success">See Loan Options</a>
                </div>
            <!--End CTA-5 section-->
            </div>
        </div>
    </div>
    <div style="margin-bottom: 30px;">
        <!-- Start CTA horizontal-1-->
        <div class="cta-section cta-section_section brand-gradient">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="cta-logo-icon">
                            <i class="icomoon icomoon-idea-dollar"></i>
                        </div>
                        <div class="txt-logo">
                            <span>Lend</span>Genius
                        </div>
                        <div class="text-center margin-top-40">
                            <a href="/apply-now/a" class="btn btn-default-success">See Loan Options</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end CTA horizontal-1 -->
    </div>
    <div style="margin-bottom: 30px;">
        <!-- Start CTA horizontal-2-->
        <div class="cta-section cta-section_section brand-gradient">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="cta-logo-icon">
                            <i class="icomoon icomoon-idea-dollar"></i>
                        </div>
                        <div class="txt-logo">
                            <span>Lend</span>Genius
                        </div>
                        <div class="desk medium">
                            Apply to the Industry's Best Lenders with LendGenius Today!
                        </div>
                        <div class="text-center">
                            <a href="/apply-now/a" class="btn btn-default-success">See Loan Options</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end CTA horizontal-2 -->
    </div>
    <div style="margin-bottom: 30px;">
        <!-- Start CTA horizontal-3-->
        <div class="cta-section cta-section_section brand-gradient">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="txt-logo delimiter">
                            <span>Lend</span>Genius
                        </div>
                        <div class="text-center">
                            <a href="/apply-now/a" class="btn btn-default-success">See Loan Options</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end CTA horizontal-4 -->
    </div>
    <div style="margin-bottom: 30px;">
        <!-- Start CTA horizontal-4-->
        <div class="cta-section cta-section_section brand-gradient">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="txt-logo delimiter">
                            <span>Lend</span>Genius
                        </div>
                        <div class="desk">
                            Compare dozens of small business loans at once
                        </div>
                        <div class="text-center">
                            <a href="/apply-now/a" class="btn btn-default-success">See Loan Options</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end CTA horizontal-4 -->
    </div>
    <div style="margin-bottom: 30px;">
        <!-- Start CTA horizontal-5 -->
         <div class="cta-section cta-section_section lightgray-bg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="cta-logo-icon">
                            <i class="icomoon icomoon-idea-dollar ico-gradient"></i>
                        </div>
                        <div class="txt-logo">
                            <span>Lend</span>Genius
                        </div>
                      
                        <div class="text-center margin-top-40">
                            <a href="/apply-now/a" class="btn btn-success">See Loan Options</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end CTA horizontal-6 -->
    </div>
    <div style="margin-bottom: 30px;">
        <!-- Start CTA horizontal-6-->
         <div class="cta-section cta-section_section lightgray-bg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="cta-logo-icon">
                            <i class="icomoon icomoon-idea-dollar ico-gradient"></i>
                        </div>
                        <div class="txt-logo">
                            <span>Lend</span>Genius
                        </div>
                        <div class="desk">
                            Apply to the Industry's Best Lenders with LendGenius Today!
                        </div>
                        <div class="text-center">
                            <a href="/apply-now/a" class="btn btn-success">See Loan Options</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end CTA horizontal-6 -->
    </div>
    <div style="margin-bottom: 30px;">
        <!-- Start CTA horizontal-7-->
         <div class="cta-section cta-section_section lightgray-bg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="txt-logo delimiter">
                            <span>Lend</span>Genius
                        </div>
                        <div class="text-center">
                            <a href="/apply-now/a" class="btn btn-success">See Loan Options</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end CTA horizontal-7 -->
    </div>
    <div style="margin-bottom: 30px;">
        <!-- Start CTA horizontal-8 -->
         <div class="cta-section cta-section_section lightgray-bg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="txt-logo delimiter">
                            <span>Lend</span>Genius
                        </div>
                        <div class="desk">
                            Compare dozens of small business loans at once
                        </div>
                        <div class="text-center">
                            <a href="/apply-now/a" class="btn btn-success">See Loan Options</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end CTA horizontal-8 -->
    </div>
</div>
<?php
get_footer();
?>