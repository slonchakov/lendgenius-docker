<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 10.10.16
 * Time: 12:16
 */

get_header();

$quote_for_twitter = get_post_meta( get_the_ID(), 'quote_for_twitter', true );
$header_type       = get_post_meta( get_the_ID(), 'header_type',true);

$header_style = !empty($header_type) ? $header_type : 'single-top-simple';
?>
<div class="page-blog page-blog-rd">
    <?php
    while ( have_posts() ) : the_post(); ?>

    <?php get_template_part('views/parts/'. $header_style); ?>

    <script type="application/ld+json">
        {
          "@context": "https://schema.org",
          "@type": "Article",
          "mainEntityOfPage": {
            "@type": "WebPage",
            "@id": "<?php the_permalink() ?>"
          },
          "headline": "<?= $post->post_title?>",
          "description": "<?= strip_tags(get_the_excerpt()) ?>",
          "image": {
            "@type": "ImageObject",
            "url": "<?php echo get_the_post_thumbnail_url(get_the_ID(), 'hero-image-2560-667'); ?>",
            "width": 2560,
            "height": 667
          },
          "author": {
            "@type": "Person",
            "name": "<?= get_the_title(get_post_meta( get_the_ID() , 'author' , true )) ?>"
          },
          "publisher": {
            "@type": "Organization",
            "name": "LendGenius",
            "logo": {
              "@type": "ImageObject",
              "url": "https://lendgeniuslandingstorage.azureedge.net/wp-uploads/2017/04/lendgenius-thumbnail.png",
              "width": 200,
              "height": 200
            }
          },
          "datePublished": "<?= get_the_date('Y-m-d') ?>",
          "dateModified": "<?= get_the_modified_date('Y-m-d') ?>"
        }
    </script>

    <div>
        <div class="container">
            <div class="row flex flex-wrap">
                <div class="col-xs-12">
                     <?php breadcrumbs('<div id="breadcrumbs" class="breadcrumbs">','</div>'); ?>
                </div>

                <?php
                   if (shortcode_exists('lgsshare_buttons'))
                        echo do_shortcode('[lgsshare_buttons]');
                ?>

                <div class="col-xs-12 col-lg-8 blog-content-holder">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="item-title-mobile">
                                <?php
                                    the_title();
                                ?>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="content-blog">
                                <div class="blog-item">
                                    <?php
                                    // Author short info.
                                    if ( is_single() ) :
                                        get_template_part( 'single-author-info' );
                                    endif;
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-blog content-rel-blog">
                        <?php
                         get_template_part( 'content-single', get_post_format() );
                        ?>
                        <?php
                        // Author full info.
                        if ( is_single() ) :
                          get_template_part( 'single-author-full-info' );
                        endif;
                        if( !function_exists('is_plugin_active') ) {

                          include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

                        }
                        if ( is_plugin_active( 'disqus-comment-system/disqus.php' ) ){
                        ?>
                        <div class="appeal-holder">
                            <p>Dear Entrepreneurs,</p>
                            <p>We want to get your feedback and encourage an open discussion among small business owners, but please help us keep our site safe by not disclosing personal or sensitive information such as bank account or phone numbers. Please note that comments are not reviewed or endorsed by LendGenius or representatives of the products or institutions mentioned, unless explicitly stated otherwise.</p>
                            <p>Your Friends at LendGenius</p>
                        </div>
                      <?php } ?>
                    </div>

                    <?php //get_template_part( 'content-custom-articles', get_post_format() ); ?>

                    <?php
                    if ( is_active_sidebar( 'under-post-area' ) )
                        dynamic_sidebar( 'under-post-area' );

                    get_template_part( 'views/parts/blog-comments' );
                    ?>
                </div>

                  <?php endwhile; ?>
                <div class="col-xs-12 col-lg-4">
                    <div class="content-aside-holder">
                      <!--Sidebar-->
                        <?php get_sidebar('single'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<?php
get_footer();
