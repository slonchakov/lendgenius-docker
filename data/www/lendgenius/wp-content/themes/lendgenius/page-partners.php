<?php
/*
Template Name: Partners
*/
get_header();
?>
<section class="partner1" style="background-image: url('<?php echo get_stylesheet_directory_uri() ?>/assets/images/partners1_bg.jpg')">

    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <a href="#" class="logo">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/logo-lendgenius.png" alt="Lend Genius">
                </a>
                <h1 class="main-title">Set Up<br> Your Clients <br>for Financing Success</h1>

                <h5 class="subtitle">
                    Access marketing materials and educational resources
                </h5>
                <h5 class="subtitle">
                    Refer customers to our free service and monitor their progress
                </h5>
                <h5 class="subtitle">
                    Earn success fees for funded loans, at no cost to the client
                </h5>
                <p>
                    Gain access to your very own partner portal, where you'll find all the tools you need to help your clients successfully find working capital.
                </p>
            </div>

            <div class="col-md-7">
                <div class="form">
                    <h2 class="form-title">
                        <span>Sign up</span> to become a partner
                    </h2>
                    <form action="#" method="post">

                        <div class="row">
                            <div class="col-md-6">
                                <label for="first_name">First Name</label>
                                <input type="text" name="first_name" id="first_name" required>
                            </div>
                            <div class="col-md-6">
                                <label for="last_name">Last Name</label>
                                <input type="text" name="last_name" id="last_name">
                            </div>

                            <div class="col-md-6">
                                <label for="email">Email</label>
                                <input type="email" name="email" id="email" required>
                            </div>
                            <div class="col-md-6">
                                <label for="phone">Phone</label>
                                <input type="tel" name="phone" id="phone">
                            </div>

                            <div class="col-md-6">
                                <label for="address">Address</label>
                                <input type="text" name="address" id="address">
                            </div>
                            <div class="col-md-6">
                                <label for="address2">Adddress line two (optional)</label>
                                <input type="text" name="address2" id="address2">
                            </div>

                            <div class="col-md-6">
                                <label for="city">City</label>
                                <input type="text" name="city" id="city">
                            </div>
                            <div class="col-md-6">
                                <label for="city">State</label>
                                <select class="field-styler-option field-input-biger" name="select-0">
                                    <option disabled selected>Please choose</option>
                                    <option value="0">Choose 1</option>
                                    <option value="1">Choose 2</option>
                                    <option value="2">Choose 3</option>
                                </select>
                            </div>

                            <div class="col-md-6">
                                <label for="zip">ZIP</label>
                                <input type="text" name="zip" id="zip">
                            </div>
                            <div class="col-md-6">
                                <label for="business">Business name</label>
                                <input type="text" name="business" id="business">
                            </div>

                            <div class="col-md-6">
                                <label>Partner type</label>
                                <select class="field-styler-option field-input-biger" name="select-1">
                                    <option disabled selected>Please choose</option>
                                    <option value="0">Choose 1</option>
                                    <option value="1">Choose 2</option>
                                    <option value="2">Choose 3</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="about_us">How did you hear about us?</label>
                                <input type="text" name="about_us" id="about_us">
                            </div>

                            <div class="col-md-12">
                                <label for="password">Password</label>
                                <input type="password" name="password" id="password" required>
                            </div>
                            <div class="col-md-12">
                                <input type="submit"  value="Become a Partner">
                            </div>


                            <div class="col-md-12">
                                <p>By clicking "Sign Up", I consent to the Lendgenius Partner <a href="#">Terms of Service</a> and <a href="#">Privacy Policy</a></p>
                            </div>

                        </div> <!-- end row -->

                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
get_footer();
?>
