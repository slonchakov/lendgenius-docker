<?php
/*
    Template Name: Resources Credit Cards
*/
get_header();
?>
<div class="content-funnel funnel-1">
    <section class="visual" style="background-image:url(<?php echo get_stylesheet_directory_uri() ?>/assets/images/bg-visual-block.jpg");>
        <div class="container">
            <div class="cycle-gallery">
                <div class="mask">
                    <div class="slideset">
                        <div class="slide">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/img-card.png" alt="credit card" class="pull-right">
                            <div class="text-holder">
                                <h2><?php the_title(); ?></h2>
                                <?php the_excerpt(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<section class="about-block">
    <div class="container">
        <?php while ( have_posts() ) : the_post();
            the_content();
        endwhile;
        ?>
        <div class="info-row">
            <div class="row">
                <?php
                $exists_posts = array();
                // The Query
                $args = array(
                    'post_type' => 'resources',
                    'meta_key'     => 'resource_type',
                    'meta_value'   => 'ccard',
                    'orderby' => 'ID',
                    'order' => 'DESC'
                );
                $query = new WP_Query( $args );
                if ( $query->have_posts() ) {
                    // The Loop
                    $exists_posts []= get_the_ID;
                    while ( $query->have_posts() ) : $query->the_post();

                        get_template_part( 'content-credit-card-short', get_post_format() );

                    endwhile;
                    wp_reset_postdata();
                }
                if( !empty($exists_posts) ) {
                    ?>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                            <?php
                            //Suggested posts
                            $args = array(
                                'post__not_in' => $exists_posts,
                                'meta_key'     => 'resource_type',
                                'meta_value'   => 'ccard',
                                'posts_per_page' => 3,
                                'caller_get_posts' => 1
                            );
                            $suggested_query = new WP_Query($args);
                            $suggested_count = $suggested_query->post_count;
                            if ($suggested_query->have_posts() && $suggested_count > 0) :
                                ?>
                        <strong class="title text-uppercase">Suggested For You</strong>
                        <ul class="list-unstyled">
                                <?php
                                while ($suggested_query->have_posts()) : $suggested_query->the_post();
                                ?>
                                    <li><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></li>
                                <?php
                                endwhile;
                                ?>
                        </ul>
                        <a href="#" class="btn btn-primary">Click to compare all business cards</a>
                            <?php endif;
                            wp_reset_query();
                            ?>
                    </div>
                    <?php
                }
                    ?>
            </div>
        </div>
        <div class="cards-info">
            <?php
            // The Query
            $args = array(
                'post_type' => 'resources',
                'meta_key'     => 'resource_type',
                'meta_value'   => 'ccard',
                'orderby' => 'ID',
                'order' => 'DESC'
            );
            $query = new WP_Query( $args );
            $post_count = $query->post_count;
            if ( $query->have_posts() ) {
                // The Loop
                while ( $query->have_posts() ) : $query->the_post();

                    get_template_part( 'content-credit-card', get_post_format() );

                endwhile;
                wp_reset_postdata();
            }
            ?>
            <!--div class="row">
                <div class="col-xs-12 col-sm-8 pull-right">
                    <div class="links-holder">
                        <a href="#" class="view pull-left">View comments</a>
                        <a href="#" class="btn btn-primary pull-right">Click to compare all business cards</a>
                    </div>
                </div>
            </div-->
        </div>
    </div>
</section>
<?php
//How much Block
//get_template_part( 'content-how-much', get_post_format() );

//Features Block
get_template_part( 'content-features-block', get_post_format() );

get_footer();