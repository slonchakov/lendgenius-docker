<?php
/*
    Template Name: Funnel-4
*/
get_header();
set_fields_for_loan();
?>

<div class="page-funnel-3 funnel-view">
    <div class="page-wraper">
        <div class="page-wraper-bg"></div>
        <div class="container">
            <a href="/">
                <img class="site-logo" src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/logo-lendgenius.png" alt="Legend Genius logo">
            </a>
            <div class="row">
                <div class="col-md-6">
                    <div class="funnel-form">
                        <div class="funnel-form-steps">
                            <div class="steps-indication">
                                <div class="step-title">Step 2 of 2</div>
                                <div class="step-selector"><span>1</span><span class="active">2</span></div>
                            </div>
                            <div class="step-1">
                                <div class="content-refine-results tpl-info">
                                    <div class="content-wrap">
                                        <form method="post" id="loan_form">
                                            <input type="hidden" name="action" value="loan_form">
                                            <input type="hidden" name="loan_amount" value="<?php echo $_SESSION['loan_data']['loan_amount']; ?>">
                                            <input type="hidden" name="gross_sales" value="<?php echo $_SESSION['loan_data']['gross_sales']; ?>">
                                            <input type="hidden" name="time_in_business" value="<?php echo $_SESSION['loan_data']['time_in_business']; ?>">
                                            <input type="hidden" name="business_name" value="<?php echo $_SESSION['loan_data']['business_name']; ?>">
                                            <input type="hidden" name="industry_description" value="<?php echo $_SESSION['loan_data']['industry_description']; ?>">
                                            <input type="hidden" name="business_address" value="<?php echo $_SESSION['loan_data']['business_address']; ?>">
                                            <input type="hidden" name="zip_code" value="<?php echo $_SESSION['loan_data']['zip_code']; ?>">
                                            <div class="refine-results-items">
                                                <div class="item">
                                                    <div class="item-label">
                                                        <label>First name</label>
                                                    </div>
                                                    <div class="item-field">
                                                        <input class="field-input field-input-biger" type="text" name="first_name">
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="item-label">
                                                        <label>Last name</label>
                                                    </div>
                                                    <div class="item-field">
                                                        <input class="field-input field-input-biger" type="text" name="last_name">
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="item-label">
                                                        <label>Primary phone</label>
                                                    </div>
                                                    <div class="item-field">
                                                        <input class="field-input field-input-biger" type="text" name="phone">
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="item-label">
                                                        <label>Email</label>
                                                    </div>
                                                    <div class="item-field">
                                                        <input class="field-input field-input-biger" type="email" name="email">
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="item-label">
                                                        <label>Credit score</label>
                                                    </div>
                                                    <div class="item-field">
                                                        <select class="field-styler-option field-input-biger" name="credit">
                                                            <option disabled selected>Please choose</option>
                                                            <option value="EXCELLENT">Excellent Credit (760+)</option>
                                                            <option value="VERYGOOD">Very Good Credit (720-759)</option>
                                                            <option value="GOOD">Good Credit (660-719)</option>
                                                            <option value="FAIR">Fair Credit (600-659)</option>
                                                            <option value="POOR">Poor Credit (580-599)</option>
                                                            <option value="VERYPOOR">Very Poor Credit (500+)</option>
                                                            <option value="UNSURE">Not Sure</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="refine-results-footer">
                                                <a href="/get-started/" class="refine-results-back">Back</a>
                                                <button class="btn btn-primary" type="submit">Continue</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="funnel-info">
                        <div class="funnel-info-body">
                            <?php
                            // Start the loop.
                            while ( have_posts() ) : the_post();

                                the_content();

                            endwhile; ?>
                        </div>
                        <div class="funnel-info-footer">
                            <ul class="list-inline sprites">
                                <li class="icon-protect-comodo-green"></li>
                                <li class="icon-protect-mcafee-red"></li>
                                <li class="icon-protect-norton"></li>
                            </ul>
                            <div class="subtext">
                                <p>SBA loans are a great financing solution that gets rid of cash flow constraints thanks to their longer terms, often at the lowest rates.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" >
    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    jQuery(document).ready(function($) {
        jQuery( "[name=\"email\"], [name=\"first_name\"], [name=\"last_name\"], [name=\"phone\"], [name=\"credit\"]" ).focusout(function() {
            validate_funnel4(this);
        });
        jQuery( "[name=\"credit\"]" ).change(function() {
            jQuery(this).parent().find('.error-message').remove();
            if(this.value == '' || this.value == 'Please choose'){
                jQuery(this).removeClass('success');
                jQuery(this).addClass('error');
                jQuery(this).parent().find('.jq-selectbox__dropdown').after('<span class="error-message">Please, select your credit score.</span>');
            }else{
                jQuery(this).removeClass('error');
                jQuery(this).addClass('success');
            }
        });

        function validate_funnel4(elem){
            jQuery(elem).parent().find('.error-message').remove();
            if(elem.value == '' || elem.value == 'Please choose' || (!validateEmail(elem.value) && elem.name == 'email')){
                jQuery(elem).removeClass('success');
                jQuery(elem).addClass('error');
                if(elem.name == 'email'){
                    jQuery(elem).after('<span class="error-message">Please, insert a valid email address.</span>');
                }else if(elem.name == 'phone'){
                    jQuery(elem).after('<span class="error-message">Please, insert a valid phone number.</span>');
                }else if(elem.name == 'credit'){
                    jQuery(elem).parent().find('.jq-selectbox__dropdown').after('<span class="error-message">Please, select your credit score.</span>');
                }else{
                    jQuery(elem).after('<span class="error-message">This is a required field.</span>');
                }
            }else{
                jQuery(elem).removeClass('error');
                jQuery(elem).addClass('success');
            }
        }
        jQuery('#loan_form').on( "submit", function( event ) {
            event.preventDefault();
            var first_name = jQuery('[name="first_name"]').val(),
                last_name = jQuery('[name="last_name"]').val(),
                phone = jQuery('[name="phone"]').val(),
                email = jQuery('[name="email"]').val(),
                credit = jQuery('[name="credit"]').val();

            //validation
            if(first_name == '' || last_name == '' || phone == '' ||
                email == '' || !validateEmail(email) || credit == null){
                jQuery( "[name=\"email\"], [name=\"first_name\"], [name=\"last_name\"], [name=\"phone\"], [name=\"credit\"]" ).each(function() {
                    validate_funnel4(this);
                });
                return;
            }
            jQuery.post( "<?php echo admin_url('admin-ajax.php'); ?>", jQuery(this).serialize(), function(response) {
                obj = jQuery.parseJSON(response);
                if(obj.RedirectURL) {
                    document.location.href = obj.RedirectURL;
                }else{
                    console.info(response);
                }
            });
        });
    });
</script>
<?php
get_footer();

