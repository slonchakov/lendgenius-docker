<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 09.01.17
 * Time: 14:12
 */

?>
<div class="content-how-much">
    <div class="container">
        <div class="how-much-wraper">
            <div class="how-much-title">So... How Much Do You Need?</div>
            <form action="<?php echo cta_button_link(); ?>" class="how-much-form" method="post">
                <div class="row">
                    <div class="col-lg-9">
                        <div class="row">
                            <div class="col-md-3 ">
                                <div class="row-form">
                                    <input id="RequestedAmount2" type="tel" name="how_much">
                                    <label for="RequestedAmount2Masked">Amount Seeking</label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="row-form">
                                    <div class="item-field-select">
                                        <select id="credit2" name="credit">
                                            <option disabled selected>Please choose</option>
                                            <option value="EXCELLENT">Excellent (760+)</option>
                                            <option value="VERYGOOD">Very Good (720-759)</option>
                                            <option value="GOOD">Good (660-719)</option>
                                            <option value="FAIR">Fair (600-659)</option>
                                            <option value="POOR">Poor (580-599)</option>
                                            <option value="VERYPOOR">Very Poor (500+)</option>
                                            <option value="UNSURE">Not Sure</option>
                                        </select>
                                        <label for="credit2">Credit Score</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="row-form">
                                    <input id="GrossSales2" type="tel" name="gross_sales">
                                    <label for="GrossSales2Masked">Monthly Sales</label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="row-form">
                                    <div class="item-field-select">
                                        <select id="TimeInBusiness2" name="time_in_business">
                                            <option value="" disabled selected>Please choose</option>
                                            <option value="0">Starting a business</option>
                                            <option value="6">Less than 6 months</option>
                                            <option value="24">1 - 2 years</option>
                                            <option value="60">2 - 5 years</option>
                                            <option value="84">5 - 8 years</option>
                                            <option value="96">More than 8 years</option>
                                        </select>
                                        <label for="TimeInBusiness2">Time in Business</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <button class="btn btn-success" type="submit">Get Your Money</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
