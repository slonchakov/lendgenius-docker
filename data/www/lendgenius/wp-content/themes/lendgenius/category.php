<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 21.12.16
 * Time: 18:27
 */

get_header();
?>
    <div class="page-blog">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-lg-9">
                    <div class="content-blog">
                        <div class="blog-frame">
                            <?php
                            //Top Articles
                            // The Query
                            $args_1 = array(
                                'post_type' => 'post',
                                'post_status' => 'publish',
                                'meta_key'   => 'top_article',
                                'meta_value'   => '77',
                                'posts_per_page' => 1
                            );
                            $query_1 = new WP_Query( $args_1 );
                            if ( $query_1->have_posts() ) {
                                // The Loop
                                while ( $query_1->have_posts() ) : $query_1->the_post();
                                    get_template_part( 'views/content/blog/general-post', get_post_format() );
                                endwhile;
                                wp_reset_postdata();
                            }
                            ?>
                            <div class="row">
                                <?php
                                // The Query
                                $args_2 = array(
                                    'post_type' => 'post',
                                    'post_status' => 'publish',
                                    'meta_key'   => 'top_article',
                                    'meta_value'   => '1',
                                    'posts_per_page' => 3
                                );
                                $query_2 = new WP_Query( $args_2 );
                                if ( $query_2->have_posts() ) {
                                    // The Loop
                                    while ( $query_2->have_posts() ) : $query_2->the_post();
                                        get_template_part( 'views/content/blog/sub-post', get_post_format() );
                                    endwhile;
                                    wp_reset_postdata();
                                }
                                ?>
                            </div>
                        </div>
                        <?php
                        // The Query
                        $args = array(
                            'post_type' => 'post',
                            'post_status' => 'publish',
                            'orderby' => 'ID',
                            'order' => 'DESC',
                            'posts_per_page' => 16
                        );
                        $query = new WP_Query( $args );
                        $post_count = $query->post_count;
                        $cc_post = 0;
                        if ( $query->have_posts() ) {
                            // The Loop
                            while ( $query->have_posts() ) : $query->the_post();
                                if($cc_post == 0 || ($cc_post % 4) == 0){
                                    ?>
                                    <div class="blog-frame">
                                    <?php
                                    get_template_part( 'views/content/blog/general-post', get_post_format() );
                                    ?>
                                    <div class="row">
                                    <?php
                                }else{
                                    get_template_part( 'views/content/blog/sub-post', get_post_format() );
                                }

                                $cc_post += 1;
                                if(($cc_post % 4) == 0 || $cc_post == $post_count){
                                    ?>
                                    </div>
                                    </div>
                                    <?php
                                }

                            endwhile;
                            wp_reset_postdata();
                        }
                        ?>
                    </div>
                </div>
                <div class="col-xs-12 col-lg-3">
                    <?php get_sidebar('blog'); ?>
                </div>
            </div>
        </div>
    </div>
<?php
get_template_part( 'content-how-much', get_post_format() );
?>
<?php

get_footer();