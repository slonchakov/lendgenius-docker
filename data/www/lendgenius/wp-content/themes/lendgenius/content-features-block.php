<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 13.10.16
 * Time: 15:30
 */
?>

<section class="features-block" style="background-image:url(<?php echo get_stylesheet_directory_uri() ?>/assets/images/bg-features-block.jpg");>
    <div class="container">
        <div class="logo">
            <a href="/">
                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/logo-lendgenius.png" alt="lendgenius">
            </a>
        </div>
        <!--div class="list-holder text-center">
            <ul class="list-unstyled text-uppercase">
                <li>Transparent Pricing + Terms</li>
                <li>Shop Several Offers at Once</li>
                <li>Lowest Rates, Guaranteed</li>
                <li>A Long-Term Relationship</li>
                <li>Free Resources + Tools</li>
            </ul>
        </div-->
    </div>
</section>
