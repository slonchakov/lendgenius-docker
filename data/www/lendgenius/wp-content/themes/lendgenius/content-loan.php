<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 14.10.16
 * Time: 19:09
 */

$min_credit_score = get_post_meta( get_the_ID(), 'min_credit_score', true );
$monthly_revenue = get_post_meta( get_the_ID(), 'monthly_revenue', true );
$time_in_business = get_post_meta( get_the_ID(), 'time_in_business', true );
$funds_in = get_post_meta( get_the_ID(), 'funds_in', true );
$typical_payback = get_post_meta( get_the_ID(), 'typical_payback', true );
?>

<div class="col-lg-6 col-responsive">
    <div class="item">
        <form>
            <div class="item-head">
                <div class="table">
                    <div class="table-cell-middle">
                        <div class="item-src icomoon font-icon-small_business"></div>
                        <div class="item-src-text"><?php the_title(); ?></div>
                    </div>
                </div>
            </div>
            <div class="item-body">
                <div class="table">
                    <div class="table-row">
                        <div class="table-cell">
                            <div class="item-option">
                                <label class="label-input">
                                    <input type="radio" name="option_1" value="1"><span class="label-input-circle">Credit Score <strong><?php echo $min_credit_score; ?></strong></span>
                                </label>
                            </div>
                            <div class="item-option">
                                <label class="label-input">
                                    <input type="radio" name="option_1" value="2"><span class="label-input-circle">Monthly Revenue <strong><?php echo $monthly_revenue; ?></strong></span>
                                </label>
                            </div>
                            <div class="item-option">
                                <label class="label-input">
                                    <input type="radio" name="option_1" value="3"><span class="label-input-circle">Time in Business <strong><?php echo $time_in_business; ?></strong></span>
                                </label>
                            </div>
                        </div>
                        <div class="table-cell">
                            <div class="item-option-description">
                                <p>Funds In</p>
                                <p class="strong"><?php echo $funds_in; ?></p>
                                <p>Typical payback</p>
                                <p class="strong">$<?php echo $typical_payback; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item-bottom">
                <div class="loan-request">
                    <div class="loan-request-title">Loan Request</div>
                    <div class="loan-request-price">$200.000</div>
                </div>
            </div>
        </form>
    </div>
</div>
