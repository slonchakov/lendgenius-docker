<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 17.10.16
 * Time: 11:29
 */

$sub_title = get_post_meta( get_the_ID(), 'sub_title', true );
?>

<div class="row">
    <div class="col-xs-12 col-sm-4">
        <h2><?php the_title(); ?></h2>
        <h3><?php echo $sub_title; ?></h3>
        <?php
        // Post thumbnail.
        single_post_thumbnail('top-article-thumb');
        ?>
        <!--a href="<?php echo get_permalink(); ?>">Show details</a-->
    </div>
    <div class="col-xs-12 col-sm-8">
        <?php the_content(); ?>
    </div>
</div>
