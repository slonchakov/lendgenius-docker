<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 24.10.16
 * Time: 20:40
 */
?>

<div class="our-customers-happy-stories">
        <div class="our-customers-happy-stories-wraper">
            <div class="container">
                <div class="our-customers-happy-stories-container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="item-title">Our Customer’s Happy Stories</div>
                            <div class="item-text">Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit.</div>
                            <div class="item-author"><strong>Roberta Scott</strong>, Bakery House</div>
                            <div class="item-readmore"><a href="#">See more stories</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>