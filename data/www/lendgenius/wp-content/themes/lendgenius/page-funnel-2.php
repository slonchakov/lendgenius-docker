<?php
/*
    Template Name: Funnel-2
*/
get_header();
?>

<div class="page-potential-loan-options">
    <div class="page-wraper">
        <div class="container">
            <div class="table">
                <div class="table-row">
                    <div class="table-cell col-aside">
                        <div class="content-refine-results">
                            <div class="content-wrap">
                                <form>
                                    <div class="refine-results-header">
                                        <div class="refine-results-title">Refine results
                                            <button class="btn btn-inline refine-results-reset" type="reset" value="Reset">Reset</button>
                                        </div>
                                    </div>
                                    <div class="refine-results-items">
                                        <div class="item">
                                            <div class="item-label">
                                                <label>Loan amount</label>
                                            </div>
                                            <div class="item-field">
                                                <select class="field-styler-option field-input-biger" name="select-0">
                                                    <option disabled selected>Please choose</option>
                                                    <option value="0">Choose 1</option>
                                                    <option value="1">Choose 2</option>
                                                    <option value="2">Choose 3</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="item-label">
                                                <label>Monthly Sales</label>
                                            </div>
                                            <div class="item-field">
                                                <select class="field-styler-option field-input-biger" name="select-1">
                                                    <option disabled selected>Please choose</option>
                                                    <option value="1">Choose 1</option>
                                                    <option value="2">Choose 2</option>
                                                    <option value="3">Choose 3</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="item-label">
                                                <label>Time in Business</label>
                                            </div>
                                            <div class="item-field">
                                                <select class="field-styler-option field-input-biger" name="select-2">
                                                    <option disabled selected>Please choose</option>
                                                    <option value="2">Choose 1</option>
                                                    <option value="3">Choose 2</option>
                                                    <option value="4">Choose 3</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="item-label">
                                                <label>Credit Score</label>
                                            </div>
                                            <div class="item-field">
                                                <select class="field-styler-option field-input-biger" name="select-3">
                                                    <option disabled selected>Please choose</option>
                                                    <option value="3">Choose 1</option>
                                                    <option value="4">Choose 2</option>
                                                    <option value="5">Choose 3</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="refine-results-footer">
                                        <button class="btn btn-success btn-big btn-block" type="submit">Get Started</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="table-cell col-body">
                        <div class="content-potential-loan-option">
                            <div class="content-wrap">
                                <div class="loan-option-items-header">
                                    <h1 class="item-title-primary">Potential Loan Options</h1>
                                    <div class="item-get-started"><a class="btn btn-success btn-big">Get Started</a></div>
                                </div>
                                <div class="loan-option-items">
                                    <div class="row">
                                        <?php
                                        // The Query
                                        $args = array(
                                            'post_type' => 'loans'
                                        );
                                        $query = new WP_Query( $args );

                                        if ( $query->have_posts() ) {
                                            // The Loop
                                            while ( $query->have_posts() ) : $query->the_post();

                                                get_template_part( 'content-loan', get_post_format() );

                                            endwhile;
                                            wp_reset_postdata();
                                        }
                                        ?>
                                        <!--div class="col-lg-6 col-responsive">
                                            <div class="item">
                                                <form>
                                                    <div class="item-head">
                                                        <div class="table">
                                                            <div class="table-cell-middle">
                                                                <div class="item-src icomoon font-icon-small_business"></div>
                                                                <div class="item-src-text">Small Business <br/> Loans</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item-body">
                                                        <div class="table">
                                                            <div class="table-row">
                                                                <div class="table-cell">
                                                                    <div class="item-option">
                                                                        <label class="label-input">
                                                                            <input type="radio" name="option_1" value="1"><span class="label-input-circle">Credit Score <strong>500+</strong></span>
                                                                        </label>
                                                                    </div>
                                                                    <div class="item-option">
                                                                        <label class="label-input">
                                                                            <input type="radio" name="option_1" value="2"><span class="label-input-circle">Monthly Revenue <strong>$4,000+</strong></span>
                                                                        </label>
                                                                    </div>
                                                                    <div class="item-option">
                                                                        <label class="label-input">
                                                                            <input type="radio" name="option_1" value="3"><span class="label-input-circle">Time in Business <strong>3 months </strong></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="table-cell">
                                                                    <div class="item-option-description">
                                                                        <p>Funds In</p>
                                                                        <p class="strong">3-5 days</p>
                                                                        <p>Typical payback</p>
                                                                        <p class="strong">$0</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item-bottom">
                                                        <div class="loan-request">
                                                            <div class="loan-request-title">Loan Request</div>
                                                            <div class="loan-request-price">$200.000</div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-responsive">
                                            <div class="item">
                                                <form>
                                                    <div class="item-head">
                                                        <div class="table">
                                                            <div class="table-cell-middle">
                                                                <div class="item-src icomoon font-icon-terms"></div>
                                                                <div class="item-src-text">Term <br/> Business Loan</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item-body">
                                                        <div class="table">
                                                            <div class="table-row">
                                                                <div class="table-cell">
                                                                    <div class="item-option">
                                                                        <label class="label-input">
                                                                            <input type="radio" name="option_1" value="1"><span class="label-input-circle">Credit Score <strong>500+</strong></span>
                                                                        </label>
                                                                    </div>
                                                                    <div class="item-option">
                                                                        <label class="label-input">
                                                                            <input type="radio" name="option_1" value="2"><span class="label-input-circle">Monthly Revenue <strong>$4,000+</strong></span>
                                                                        </label>
                                                                    </div>
                                                                    <div class="item-option">
                                                                        <label class="label-input">
                                                                            <input type="radio" name="option_1" value="3"><span class="label-input-circle">Time in Business <strong>3 months </strong></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="table-cell">
                                                                    <div class="item-option-description">
                                                                        <p>Funds In</p>
                                                                        <p class="strong">3-5 days</p>
                                                                        <p>Typical payback</p>
                                                                        <p class="strong">$0</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item-bottom">
                                                        <div class="loan-request">
                                                            <div class="loan-request-title">Loan Request</div>
                                                            <div class="loan-request-price">$200.000</div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-responsive">
                                            <div class="item">
                                                <form>
                                                    <div class="item-head">
                                                        <div class="table">
                                                            <div class="table-cell-middle">
                                                                <div class="item-src icomoon font-icon-invoice"></div>
                                                                <div class="item-src-text">Invoice <br/> Financing</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item-body">
                                                        <div class="table">
                                                            <div class="table-row">
                                                                <div class="table-cell">
                                                                    <div class="item-option">
                                                                        <label class="label-input">
                                                                            <input type="radio" name="option_1" value="1"><span class="label-input-circle">Credit Score <strong>500+</strong></span>
                                                                        </label>
                                                                    </div>
                                                                    <div class="item-option">
                                                                        <label class="label-input">
                                                                            <input type="radio" name="option_1" value="2"><span class="label-input-circle">Monthly Revenue <strong>$4,000+</strong></span>
                                                                        </label>
                                                                    </div>
                                                                    <div class="item-option">
                                                                        <label class="label-input">
                                                                            <input type="radio" name="option_1" value="3"><span class="label-input-circle">Time in Business <strong>3 months </strong></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="table-cell">
                                                                    <div class="item-option-description">
                                                                        <p>Funds In</p>
                                                                        <p class="strong">3-5 days</p>
                                                                        <p>Typical payback</p>
                                                                        <p class="strong">$0</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item-bottom">
                                                        <div class="loan-request">
                                                            <div class="loan-request-title">Loan Request</div>
                                                            <div class="loan-request-price">$200.000</div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div-- class="col-lg-6 col-responsive">
                                            <div class="item">
                                                <form>
                                                    <div class="item-head">
                                                        <div class="table">
                                                            <div class="table-cell-middle">
                                                                <div class="item-src icomoon font-icon-equipment"></div>
                                                                <div class="item-src-text">Equipment <br/> Financing</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item-body">
                                                        <div class="table">
                                                            <div class="table-row">
                                                                <div class="table-cell">
                                                                    <div class="item-option">
                                                                        <label class="label-input">
                                                                            <input type="radio" name="option_1" value="1"><span class="label-input-circle">Credit Score <strong>500+</strong></span>
                                                                        </label>
                                                                    </div>
                                                                    <div class="item-option">
                                                                        <label class="label-input">
                                                                            <input type="radio" name="option_1" value="2"><span class="label-input-circle">Monthly Revenue <strong>$4,000+</strong></span>
                                                                        </label>
                                                                    </div>
                                                                    <div class="item-option">
                                                                        <label class="label-input">
                                                                            <input type="radio" name="option_1" value="3"><span class="label-input-circle">Time in Business <strong>3 months </strong></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="table-cell">
                                                                    <div class="item-option-description">
                                                                        <p>Funds In</p>
                                                                        <p class="strong">3-5 days</p>
                                                                        <p>Typical payback</p>
                                                                        <p class="strong">$0</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item-bottom">
                                                        <div class="loan-request">
                                                            <div class="loan-request-title">Loan Request</div>
                                                            <div class="loan-request-price">$200.000</div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();
