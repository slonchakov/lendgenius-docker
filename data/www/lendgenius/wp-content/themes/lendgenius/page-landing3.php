<?php
/**
 * Created by PhpStorm.
 * User: stas
 * Date: 4/19/17
 * Time: 4:37 PM
 */
/*
Template Name: Landing3
*/
$template_directory_uri = get_template_directory_uri();

$post_id             = get_the_ID();
$rates_starting      = get_post_meta( $post_id, 'rates_starting', true );
$loans_from          = get_post_meta( $post_id, 'loans_from', true );
$loans_from_interval = get_post_meta( $post_id, 'loans_from_interval', true );
$funding             = get_post_meta( $post_id, 'funding', true );
$heading_title       = get_post_meta( $post_id, 'heading_title', true );
$sub_title           = get_post_meta( $post_id, 'sub_title', true );

$input_orange_text  = get_post_meta( $post_id, 'orange_button_text', true );
$input_orange_url   = get_post_meta( $post_id, 'orange_button_url', true );
$orange_button_text = !empty($input_orange_text) ?  $input_orange_text : 'See Loan Options';
//$orange_button_url  = !empty($input_orange_url) ? $input_orange_url : '/loan-request';
$orange_button_url  = '/loan-request/?EmbedCampID='. get_option('sts_company_id');

get_header();
get_template_part( 'views/headers/header-top-landing', get_post_format() );
?>
<section class="loans_subpage_banner">
    <div class="darken-lining">
        <div class="container">
            <!--.container-fluid (if full-screen)-->
            <div class="row flex-nc flex-align-stretch flex-column-xs">
                <div class="col-sm-7 col-md-8 flex-nc flex-align-end">
                    <div class="">
                        <h1><?= $heading_title; ?></h1>
                        <h2>
                            <?= $sub_title; ?>
                        </h2>
                    </div>
                </div>
                <!-- <div class="col-sm-7 col-md-6 banner-form-wrapper">
                    <?= do_shortcode("[lead form='7']"); ?>
                    <div class="secure-icons-form">
                        <div class="txt">Applying won't affect your credit score.</div>
                        <div><img src="<?php echo $template_directory_uri; ?>/landing-pages/img/comodo-logo-white.svg"  class="img-responsive center-block" /></div>
                        <div><img src="<?php echo $template_directory_uri; ?>/landing-pages/img/sc-logo-white.svg" class="img-responsive center-block" /></div>
                    </div> 
                    <a class="btn btn-warning-custom">See Loan Options</a>
                </div> -->

                <div class="col-sm-5 col-md-4 banner-form-wrapper flex-nc flex-align-end bg-transparent-imp no-padding-horizontal-sm">
                    <a href="<?= $orange_button_url; ?>" class="btn btn-x2 btn-warning-custom"><?= $orange_button_text; ?></a>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="sky-bg section-padding">
    <div class="container">
        <div class="reasons-apply">
            <div class="row">
                <div class="col-sm-4 reasons-item">
                    <div class="reasons-item-icon">
                        <i class="icomoon-dollar-circle" aria-hidden="true"></i>
                    </div>
                    <div class="reasons-title">
                        <?php echo $rates_starting; ?>
                    </div>
                </div>
                <div class="col-sm-4 reasons-item">
                    <div class="reasons-item-icon">
                        <i class="icomoon-compare-options" aria-hidden="true"></i>
                    </div>
                    <div class="reasons-title">
                        <?php echo $loans_from; ?>
                    </div>
                    <span class="reasons-post-title"><?php echo $loans_from_interval; ?></span>
                </div>
                <div class="col-sm-4 reasons-item">
                    <div class="reasons-item-icon">
                        <i class="icomoon-doc-time" aria-hidden="true"></i> 
                    </div>
                    <div class="reasons-title">
                        <?php echo $funding; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="section-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="partners-logo">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2>As Seen On</h2>
                        </div>
                    </div>
                    <div class="flex-row flex-wrap flex-justify-sa">
                        <div class="item-logo">
                            <a href="https://www.entrepreneur.com/article/289771" target="_blank">
                                <img src="<?php echo $template_directory_uri; ?>/landing-pages/img/enterpreteur_logo.png" alt="Entrepreneur">
                            </a>
                        </div>
                        <div class="item-logo inc">
                            <a href="https://www.forbes.com/sites/brianrashid/2017/05/27/how-to-power-through-when-your-startup-is-going-under/#240274333a20" target="_blank">
                                <img src="<?php echo $template_directory_uri; ?>/landing-pages/img/Forbes_logo.png" alt="Forbes">
                            </a>
                        </div>
                        <div class="item-logo inc">
                            <a href="https://www.inc.com/andrew-medal/5-resources-to-help-minority-startup-founders.html" target="_blank">
                                <img src="<?php echo $template_directory_uri; ?>/landing-pages/img/inc_logo.png" alt="Inc.">
                            </a>
                        </div>
                        <div class="item-logo">
                            <a href="https://www.desk.com/blog/4-startups-setting-a-high-bar-for-customer-service" target="_blank">
                                <img src="<?php echo $template_directory_uri; ?>/landing-pages/img/sales_force_logo.png" alt="Salesforce.">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="default-section_large content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <?= the_content(); ?>
            </div>
        </div>
    </div>
</div>
<!--<section class="blog-holder">-->
<!--    <div class="container">-->
<!--        --><?php //get_template_part( 'views/parts/content-landing-blog', get_post_format() ); ?>
<!--    </div>-->
<!--</section>-->

<?php  get_template_part('views/parts/how-it-works'); ?>
<div class="button-section lightgray-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <a href="<?= cta_button_link(); ?>" class="btn btn-warning-custom btn-x2">See Loan Options</a>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
