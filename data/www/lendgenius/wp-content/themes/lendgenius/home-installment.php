<?php
/*
 * Template Name: Home Installment
 */

the_post();

get_header('installment');
?>
    <svg style="display:none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <symbol id="icon-lock" viewBox="4 3.3 24 26.7">
            <path
                d="M27 14h-3v-2.7c0-4.4-3.6-8-8-8s-8 3.6-8 8V14H5c-.6 0-1 .4-1 1s.4 1 1 1h21v12H6v-9c0-.6-.4-1-1-1s-1 .4-1 1v10c0 .6.4 1 1 1h22c.6 0 1-.4 1-1V15c0-.6-.4-1-1-1zm-17-2.7c0-3.3 2.7-6 6-6s6 2.7 6 6V14H10v-2.7z"/>
            <path d="M20 22c0-2.2-1.8-4-4-4s-4 1.8-4 4 1.8 4 4 4 4-1.8 4-4zm-6 0c0-1.1.9-2 2-2s2 .9 2 2-.9 2-2 2-2-.9-2-2z"/>
        </symbol>
        <symbol id="icon-clock" viewBox="2 2 28 28">
            <path
                d="M16 30c-3.7 0-7.3-1.5-9.9-4.1C1.8 21.6.8 15 3.5 9.6c.3-.5.9-.7 1.3-.4.5.3.7.9.4 1.3-2.4 4.7-1.5 10.3 2.2 14C9.8 26.8 12.8 28 16 28c3.2 0 6.2-1.2 8.5-3.5S28 19.2 28 16s-1.2-6.2-3.5-8.5S19.2 4 16 4 9.8 5.2 7.5 7.5c-.4.4-1 .4-1.4 0-.4-.4-.4-1 0-1.4C8.7 3.5 12.3 2 16 2s7.3 1.5 9.9 4.1C28.5 8.7 30 12.3 30 16s-1.5 7.3-4.1 9.9C23.3 28.5 19.7 30 16 30z"/>
            <path
                d="M21 21c-.2 0-.4-.1-.6-.2l-5-4c-.3-.2-.4-.5-.4-.8V8c0-.6.4-1 1-1s1 .4 1 1v7.5l4.6 3.7c.4.3.5 1 .2 1.4-.2.3-.5.4-.8.4z"/>
        </symbol>
        <symbol id="icon-laptop" viewBox="123.3 94.6 595.3 406">
            <path
                d="M650.7 123.3H191.2c-8.1 0-14.7 6.6-14.7 14.7v271.6c0 8.1 6.6 14.7 14.7 14.7h459.5c8.1 0 14.7-6.6 14.7-14.7V138c0-8.1-6.6-14.7-14.7-14.7zm2 286.3c0 1.1-.9 2-2 2H191.2c-1.1 0-2-.9-2-2V138c0-1.1.9-2 2-2h459.5c1.1 0 2 .9 2 2v271.6z"/>
            <path
                d="M709 433.9h-15V131c0-20.1-16.3-36.4-36.4-36.4H184.2c-20.1 0-36.4 16.3-36.4 36.4v302.9h-15c-5.3 0-9.6 4.3-9.6 9.6v26.3c0 17.1 13.9 30.9 30.9 30.9h533.4c17.1 0 30.9-13.9 30.9-30.9v-26.3c.2-5.3-4.1-9.6-9.4-9.6zM166.9 131c0-9.5 7.8-17.3 17.3-17.3h473.4c9.5 0 17.3 7.8 17.3 17.3v302.9h-508V131zm226 322H449v5c0 2.2-1.8 4.1-4.1 4.1H397c-2.2 0-4.1-1.8-4.1-4.1v-5zm306.6 16.7c0 6.5-5.3 11.8-11.8 11.8H154.2c-6.5 0-11.8-5.3-11.8-11.8V453h237.8v5c0 9.3 7.5 16.8 16.8 16.8h47.9c9.3 0 16.8-7.5 16.8-16.8v-5h237.8v16.7z"/>
        </symbol>
    </svg>

    <div class="top-content" >
        <div class="hero-box text-center" style="background-image: url(<?= imgpath('/wp-content/themes/lendgenius/style-2018/img/hero-image-min.png?v=3')?>)" >
            <div class="container">
                <h1><?php the_field('form_title') ?></h1>
                <div class="subtitle"><?php the_field('sub_form_title') ?></div>

                <jsf-form></jsf-form>
                <script>
                    window.lmpost = window.lmpost || {};
                    window.lmpost.options = {
                        leadtypeid: 19,
                        campaignid: '<?= get_option('sts_company_id') ?>'
                    };
                </script>
                <script async src="https://formrequests.com/installment36/1q_pd_im/form-loader.js"></script>

            </div>
        </div>

        <div class="container-fluid benefits-wrapper">
            <div class="container benefits-container">
                <div class="benefits__item">
                    <i class="icon icon-lock">
                        <svg>
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-lock"></use>
                        </svg>
                    </i>
                    <span>Safe &amp; Secure</span>
                </div>
                <div class="benefits__item">
                    <i class="icon icon-clock">
                        <svg>
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-clock"></use>
                        </svg>
                    </i>
                    <span>Fast Lender-Approval</span>
                </div>
                <div class="benefits__item">
                    <i class="icon icon-laptop">
                        <svg>
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-laptop"></use>
                        </svg>
                    </i>
                    <span>Submit Online</span>
                </div>
            </div>
        </div>
    </div>

    <div class="main-content">
        <div class="container">
            <div class="features-container text-center">
                <?php the_content() ?>
            </div>
        </div>
    </div>

<?php
get_footer('installment');