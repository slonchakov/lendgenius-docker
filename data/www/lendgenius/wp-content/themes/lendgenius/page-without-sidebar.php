<?php
/*
    Template Name: Page Without Sidebar
*/
get_header();
?>
<style>
    .pattern-title{
        background-image: url('<?php echo get_the_post_thumbnail_url(get_the_ID(), 'hero-image-400-342') ?>');
    }
    @media(min-width: 401px){
        .pattern-title{
            background-image: url('<?php echo get_the_post_thumbnail_url(get_the_ID(), 'hero-image-770-350'); ?>');
        }
    }
    @media(min-width: 769px){
        .pattern-title{
        background-image: url('<?php echo get_the_post_thumbnail_url(get_the_ID(), 'hero-image-1280-350'); ?>');
        }
    }
    @media(min-width: 1201px ){
        .pattern-title{
            background-image: url('<?php echo get_the_post_thumbnail_url(get_the_ID(), 'hero-image-2560-667'); ?>');
        }
    }
</style>
<div class="page-privacy-policy">
  <div class="page-wraper">
    <div class="pattern-title">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="item-title center-block text-center"> <?php the_title(); ?> </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body font-setting big-first-latter-paragraph font-light">
      <?php
        while ( have_posts() ) : the_post();
            echo wpautop(get_the_content());
        endwhile; ?>
    </div>
  </div>

    </div>


<?php
get_footer();
?>
 