<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 22.11.17
 *
 */

get_header();

?>
<div class="container" style="min-height:100vh">
    <h1>Small Business Loans</h1>

    <button id="get_lender_filters">Get lender filters</button>
    <br>
    <br>
    <button id="get_lender_categories">Get lender categories</button>
    <br>
    <br>
    <button id="get_lenders">Get lenders</button>
</div>
<?php

get_footer();
?>
<script>
    jQuery(document).ready(function($){
        $('#get_lender_filters').on('click',function(){

            $.ajax({
                url: '/api/lender_filters',
                dataType: 'json',
                success: function (json) {
                    console.log(json)

                }
            })
        });

        $('#get_lender_categories').on('click',function(){

            $.ajax({
                url: '/api/lender_categories',
                dataType: 'json',
                success: function (json) {
                    console.log(json)

                }
            })
        })

        $('#get_lenders').on('click',function(){

            $.ajax({
                url: '/api/lenders',
                dataType: 'json',
                success: function (json) {
                    console.log(json)

                }
            })
        })
    })
</script>
