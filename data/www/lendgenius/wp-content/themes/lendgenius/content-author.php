<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 28.10.16
 * Time: 13:35
 */

?>

<div class="blog-item-sub">
    <div class="blog-item-sub-img">
        <a href="<?php echo get_permalink(); ?>">
            <?php
            // Post thumbnail.
            single_post_thumbnail('author-thumb-posts', 'img-responsive center-block');
            ?>
        </a>
    </div>
    <h4 class="blog-item-sub-title matchheight">
        <a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a>
    </h4>
    <?php
    get_template_part( 'content-author-info' );
    ?>
</div>