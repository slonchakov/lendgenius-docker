<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 28.10.16
 * Time: 13:13
 */
get_header();

?>
<div class="page-blog">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-lg-8">
                <div class="content-blog">
                    <div class="blog-author-block">
                        <div class="row">
                            <div class="col-xs-12 col-md-3 col-lg-3">
                                <img src="<?php echo get_avatar_url( get_the_author_meta( 'user_email' ), array('size' => 170)); ?>" alt="image description">
<!--                                <ul class="blog-social-list">-->
<!--                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>-->
<!--                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>-->
<!--                                    <li><a href="#"><i class="fa fa-google"></i></a></li>-->
<!--                                </ul>-->
                            </div>
                            <div class="col-xs-12 col-md-9 col-lg-9">
                                <strong class="blog-author-title"><?php echo get_the_author(); ?></strong>
                                <p><?php the_author_meta( 'description' ); ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="blog-article-list">
                        <h3 class="blog-article-list-title">Posts by <?php echo get_the_author(); ?>:</h3>
                        <?php
                        // The Query
                        $args = array(
                            'author' =>  get_the_author_meta( 'ID' ),
                            'post_type' => 'post',
                            'orderby' => 'ID',
                            'order' => 'DESC'
                        );
                        $query = new WP_Query( $args );
                        $post_count = $query->post_count;
                        if ( $query->have_posts() ) {
                            // The Loop
                            while ( $query->have_posts() ) : $query->the_post();

                                get_template_part( 'content-author', get_post_format() );

                            endwhile;
                            wp_reset_postdata();
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-lg-4">
                <div class="content-aside-holder">
                    <?php
                    //Sidebar
                    get_sidebar('author');
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();
?>
