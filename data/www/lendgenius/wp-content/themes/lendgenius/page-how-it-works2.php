<?php
/*
    Template Name: How It Works(version 2)
*/
get_header();

$sub_title = get_post_meta( get_the_ID(), 'sub_title', true );
$video_in_header = get_post_meta( get_the_ID(), 'video_in_header', true );
$sub_title2 = get_post_meta( get_the_ID(), 'sub_title2', true );
$sub_title3 = get_post_meta( get_the_ID(), 'sub_title3', true );
$sub_title4 = get_post_meta( get_the_ID(), 'sub_title4', true );
$how_much_title = get_post_meta( get_the_ID(), 'how_much_title', true );
$how_much_description = get_post_meta( get_the_ID(), 'how_much_description', true );

?>
<div class="work-sans content-rd">
    <section class="section-video">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="light-font">
                        <?php echo $sub_title; ?>
                    </h2>
                </div>
            </div>
        </div>
        <div class="video-inflow">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                        <div class="embed-responsive embed-responsive-16by9">
                          <iframe class="embed-responsive-item" src="<?php echo $video_in_header; ?>" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lightgray-bg text-center">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                        <?php while ( have_posts() ) : the_post();
                            the_content();
                        endwhile;
                        ?>
                        <a href="<?php echo cta_button_link(); ?>" class="btn btn-success btn-x2 margin-top-30">Get Started</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="various-sided-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="light-font">
                        <?php echo $sub_title2; ?>
                    </h2>
                </div>
            </div>
        </div>
        <div class="various-sided">
            <?php if( have_rows('how_we_make_money') ): ?>
                <?php while( have_rows('how_we_make_money') ): the_row();

                    // vars
                    $icon = get_sub_field('icon');
                    $title = get_sub_field('title');
                    $description = get_sub_field('description');
                    ?>
                    <div class="various-sided-item">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2 flex">
                                    <div class="sided-item-logo">
                                        <i class="<?php echo $icon; ?>"></i>
                                    </div>
                                    <div class="sided-item-content">
                                        <div class="sided-item-title"><?php echo $title; ?></div>
                                        <?php echo $description; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>

        </div>
    </section>
    <section class="steps-icon">
        <div class="container">
            <div class="row">   
                <div class="col-lg-12">
                    <h2><?php echo $sub_title3; ?></h2>
                </div>
            </div>
            <div class="row">
                <?php if( have_rows('how_we_earn_money') ): ?>
                    <?php while( have_rows('how_we_earn_money') ): the_row();

                        $icon = get_sub_field('icon');
                        $title = get_sub_field('htitle');
                        $description = get_sub_field('description');
                        ?>
                        <div class="col-sm-4 steps-item">
                            <div class="steps-item-icon">
                                <i class="<?php echo $icon; ?>"></i>
                            </div>
                            <div class="steps-item-title"><?php echo $title; ?></div>
                            <?php echo $description; ?>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </section>
    <section class="finance_products">
        <div class="container">
            <div class="row">
                <h2><?php echo $sub_title4; ?></h2>
                <div class="grid">
                    <div class="product_line">
                        <?php
                        // The Query
                        $args = array(
                            'post_type' => 'loans',
                            'orderby' => 'ID',
                            'order' => 'ASC',
                        );
                        $slider_count = 1;
                        $query = new WP_Query( $args );
                        if ( $query->have_posts() ) {
                        // The Loop
                        while ( $query->have_posts() ) : $query->the_post();
                        $loan_icon = get_post_meta( get_the_ID(), 'loan_icon', true );
                            ?>
                            <div class="finance_product">
                                <a href="<?php echo get_permalink(); ?>">
                                    <div class="image">
                                        <i class="home-sprite <?php echo $loan_icon; ?>"></i>
                                    </div>
                                    <h5><?php the_title(); ?></h5>
                                </a>
                            </div>
                            <?php
                        endwhile;
                        wp_reset_postdata();
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="content-how-much content-how-much--grandient content-how-much--thin">
        <div class="container">
            <div class="how-much-wraper">
                <div class="how-much-title"><?php echo $how_much_title; ?></div>
                <p><?php echo $how_much_description; ?><p>
                <div class="how-much-form">
                    <div class="row">
                        <div class="col-lg-12">
                            <a href="<?php echo cta_button_link(); ?>" class="btn btn-default-success">See Loan Options</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();