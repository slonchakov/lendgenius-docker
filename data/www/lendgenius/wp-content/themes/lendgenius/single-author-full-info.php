<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 6/7/17
 * Time: 11:04 AM
 */

$author_id = get_post_meta( get_the_ID() , 'author' , true );
$position = get_post_meta( $author_id, 'author_position', true );

// Remove social buttons from author block
if(empty($position))
$position = 'Finance Journalist';
$content_post = get_post($author_id);
$content = $content_post->post_content;
$content = apply_filters('the_content', $content);
$content = str_replace(']]>', ']]&gt;', $content);

//Author social links
$twitter = get_post_meta( $author_id, 'author_twitter', true );
$facebook = get_post_meta( $author_id, 'author_facebook', true );
$instagram = get_post_meta( $author_id, 'author_instagram', true );
$pinterest = get_post_meta( $author_id, 'author_pinterest', true );
$linkedin = get_post_meta( $author_id, 'author_linkedin', true );
$g_plus = get_post_meta( $author_id, 'author_g_plus', true );
$website = get_post_meta( $author_id, 'author_website', true );

?>
<div class="info-about-author">
    <div class="author_avatar" style="background-image: url(<?php echo get_the_post_thumbnail_url($author_id, '70'); ?>);"></div>
    <div class="flx-gr-2">
        <div class="author_info clearfix">
            <a href="<?php echo get_permalink($author_id); ?>"><?php echo get_the_title($author_id); ?></a>
            <span class='person-function'><?php echo $position; ?></span>
            
            <?php echo $content; ?>
            
            <?php if(!empty($twitter) || !empty($facebook) || !empty($instagram) ||
                !empty($pinterest) || !empty($linkedin) || !empty($g_plus) || !empty($website)){ ?>
            <div class="author_info-footer">
                <ul class="list-unstyled author_social-links">
                    <?php if(!empty($twitter)){ ?>
                    <li class="sc-twitter">
                        <a href="<?php echo $twitter; ?>" target="_blank">
                            <i class="fa fa-twitter" aria-hidden="true"></i>
                        </a>
                    </li>
                    <?php } ?>
                    <?php if(!empty($facebook)){ ?>
                    <li class="sc-facebook">
                        <a href="<?php echo $facebook; ?>" target="_blank">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                        </a>
                    </li>
                    <?php } ?>
                    <?php if(!empty($instagram)){ ?>
                    <li class="sc-insta">
                        <a href="<?php echo $instagram; ?>" target="_blank">
                            <i class="fa fa-instagram" aria-hidden="true"></i>
                        </a>
                    </li>
                    <?php } ?>
                    <?php if(!empty($pinterest)){ ?>
                    <li class="sc-pin">
                        <a href="<?php echo $pinterest; ?>" target="_blank">
                            <i class="fa fa-pinterest" aria-hidden="true"></i>
                        </a>
                    </li>
                    <?php } ?>
                    <?php if(!empty($linkedin)){ ?>
                    <li class="sc-linkin">
                        <a href="<?php echo $linkedin; ?>" target="_blank">
                            <i class="fa fa-linkedin-square" aria-hidden="true"></i>
                        </a>
                    </li>
                    <?php } ?>
                    <?php if(!empty($g_plus)){ ?>
                    <li class="sc-gplus">
                        <a href="<?php echo $g_plus; ?>" target="_blank">
                            <i class="fa fa-google-plus" aria-hidden="true"></i>
                        </a>
                    </li>
                    <?php } ?>
                </ul>
                <?php if(!empty($website)){ ?>
                <div class="author_profile">
                    <a href="<?php echo $website; ?>" target="_blank">Author's Website </a>
                </div>
                <?php } ?>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
