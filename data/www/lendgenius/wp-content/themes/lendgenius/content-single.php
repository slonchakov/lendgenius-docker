<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 10.10.16
 * Time: 12:28
 */
?>
<div class="blog-item">
    <div class="item-wraper">
        <div class="item-article">
            <div class="item-text">
                <?php
                    the_content();
                ?>
            </div>
        </div>
    </div>
</div>