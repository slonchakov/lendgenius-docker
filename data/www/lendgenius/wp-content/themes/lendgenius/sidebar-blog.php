<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 24.10.16
 * Time: 12:27
 */

if ( is_active_sidebar( 'sidebar-blog' )  ) : ?>
    <div class="content-aside">
        <?php dynamic_sidebar( 'sidebar-blog' ); ?>
    </div>
<?php endif; ?>