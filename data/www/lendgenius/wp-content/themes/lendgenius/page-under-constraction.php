<?php
/*
    Template Name: Under Constraction
*/
get_header();

$calculator_type = get_post_meta( get_the_ID(), 'calculator_type', true );
$title_text = (!empty($calculator_type))? 'This Calculator Will be Ready Soon':'Sorry, this page is under construction.';
?>

<div class="under_constraction_page" style="background-image: url('<?php echo get_stylesheet_directory_uri() ?>/assets/images/constructing-cranes-buildings.jpg')">

    <div class="caption">
        <a href="#" class="logo">
            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/logo-lendgenius.png" alt="Lend Genius">
        </a>
        <h1 class="title">
            <?php echo $title_text; ?>
        </h1>
        <h5 class="subtitle">
            If you want to see all business loans & financing
            products available for business, click below
        </h5>

        <div class="form">
            <form action="<?php echo cta_button_link(); ?>" method="post">
                <div class="how-much-search">
                    <input class="field-input field-input-big field-input-clear" id="how_much" name="how_much" type="tel" placeholder="How Much Does Your Business Need?">
                </div>
                <div class="how-much-button">
                    <button class="field-button btn btn-success" type="submit">See Loan Options</button>
                </div>
            </form>
        </div>

        <a href="/" class="go_home">Or take me to the homepage</a>
    </div>

</div>

