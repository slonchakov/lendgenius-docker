jQuery(document).ready(function () {
    var Calculator = {
        $GrossRevenue: jQuery('#GrossRevenue'),
        $BorrowAmount: jQuery('#BorrowAmount'),
        $Payback: jQuery('#Payback'),

        $resultTotalMonths: jQuery('[data-calc="resultTotalMonths"]'),
        $resultMonthlyPayment: jQuery('[data-calc="resultMonthlyPayment"]'),

        init: function () {
            jQuery('#calculator').on('submit', function (e) {
                this.doCalc();

                e.preventDefault();
                return false;
            }.bind(this));
            jQuery('#calculator').on('keydown', 'input', function () {
                jQuery(this).closest('.has-error').removeClass('has-error')
            });

            var slider = document.getElementById('slider');
            if(slider.length) {
                noUiSlider.create(slider, {
                    start: 20,
                    step: 1,
                    range: {
                        'min': 1,
                        'max': 100
                    },
                    pips: {
                        mode: 'values',
                        values: [1, 100],
                        density: 100
                    }
                });

                var sliderSet = false;
                slider.noUiSlider.on('update', function (values, handle) {
                    if (!sliderSet) {
                        jQuery('.noUi-handle').append('<span class="noUi-handle-value"></span>');
                        sliderSet = true;
                    }
                    jQuery('.slider_value, .noUi-handle-value').html(+values[handle]);
                    this.$Payback.val(values[handle]);
                    if (sliderSet) {
                        //this.doCalc();
                    }
                }.bind(this));
            }
            jQuery('#calculator').on('change', 'input', function () {
                //this.doCalc();
            }.bind(this))
        },
        doCalc: function () {
            if (!this.validation()) {
                return;
            }
            var $GrossRevenue = parseInt(this.$GrossRevenue.val());
            var $BorrowAmount = parseInt(this.$BorrowAmount.val());
            var $Payback = parseInt(this.$Payback.val());

            console.log($BorrowAmount, $Payback, $GrossRevenue)

            var changeFx = function (loan, percent, revenue) {
                var months = (loan / ((revenue * percent) / (1200))).toFixed(0);
                months = Math.max(1, months);
                var monthly = (loan / months).toFixed(0);
                monthly = Math.min(monthly, loan);
                if ((!isNaN(months)) && (!isNaN(monthly))) {
                    this.months = months;
                    this.monthly = monthly;
                }
            }.bind(this);
            changeFx($BorrowAmount, $Payback, $GrossRevenue);

            this.render();
        },
        validation: function () {
            var valid = true;
            jQuery([this.$GrossRevenue, this.$BorrowAmount, this.$Payback]).each(function () {
                if (jQuery(this).val() == 0 || jQuery(this).val() == '') {
                    valid = false;
                }
            });
            return valid;
        },
        render: function () {
            this.$resultTotalMonths.html(this.months);
            this.$resultMonthlyPayment.html('$' + this.monthly);
        }
    };
    if(jQuery('#calculator').length){
        Calculator.init();
    }

    if(jQuery('#GrossRevenue').length || jQuery('#BorrowAmount').length) {
        //Loan mask
        function MoneyMask(elem, useDollarSign) {

            jQuery(elem).removeAttr('min').removeAttr('max').removeAttr('maxlength');
            var clonedElem = jQuery(elem).clone().insertAfter(jQuery(elem));
            jQuery(elem).attr('type', 'hidden');
            clonedElem.attr('id', clonedElem.attr('id') + 'Masked');
            clonedElem.attr('name', clonedElem.attr('id'));

            var maskUseCurrencySign = useDollarSign ? true : false;
            var maskCurrencySign = maskUseCurrencySign ? '$' : '';
            var SPMaskBehavior = function (val) {
                    var compare = val.replace(/\D/g, '').length;
                    if (compare === 0) {
                        return '000000'
                    }
                    else if (compare < 4) {
                        return maskCurrencySign + '000000'
                    }
                    else if (compare === 4) {
                        return maskCurrencySign + '0,0000'
                    }
                    else if (compare === 5) {
                        return maskCurrencySign + '00,0000'
                    }
                    else if (compare === 6) {
                        return maskCurrencySign + '000,0000'
                    }
                    else if (compare > 6) {
                        return maskCurrencySign + '0,000,000'
                    }
                },
                spOptions = {
                    onKeyPress: function (val, e, field, options) {
                        field.mask(SPMaskBehavior.apply({}, arguments), options);
                    }
                };

            clonedElem.mask(SPMaskBehavior, spOptions);

            clonedElem.on('change', function () {
                var value = jQuery(this).val();
                var intValue = value.replace(/[^0-9]/g, '');
                jQuery(elem).val(intValue);
            })
        }

        MoneyMask('#GrossRevenue', false);
        MoneyMask('#BorrowAmount', false);
    }

    //Loan mask
});