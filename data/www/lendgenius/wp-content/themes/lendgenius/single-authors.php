<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 2/2/17
 * Time: 11:06 AM
 */

get_header();

$twitter = get_post_meta( $author_id, 'author_twitter', true );
$facebook = get_post_meta( $author_id, 'author_facebook', true );
$instagram = get_post_meta( $author_id, 'author_instagram', true );
$pinterest = get_post_meta( $author_id, 'author_pinterest', true );
$linkedin = get_post_meta( $author_id, 'author_linkedin', true );
$g_plus = get_post_meta( $author_id, 'author_g_plus', true );
$website = get_post_meta( $author_id, 'author_website', true )

?>
    <div class="page-blog">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-lg-8">
                    <div class="content-blog author-page">
                        <div class="blog-author-block">
                            <div class="blog-holder-photo">
                                <?php
                                // Post thumbnail.
                                single_post_thumbnail('author-thumb', 'blog-author_photo');
                                ?>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div>
                                        <div class="text-center">
                                            <h2 class="blog-author-title"><span class="line-default"><?php the_title(); ?></span> <span class="verify-icon"></span></h2></div>
                                        <?php
                                        // Start the loop.
                                        while ( have_posts() ) : the_post();
                                            the_content();
                                        endwhile; ?>
                                    </div>
                                    <?php if(!empty($twitter) || !empty($facebook) || !empty($instagram) ||
                                        !empty($pinterest) || !empty($linkedin) || !empty($g_plus) || !empty($website)){ ?>
                                    <div class="author_info-footer">
                                        <ul class="list-unstyled author_social-links">
                                            <?php if(!empty($twitter)){ ?>
                                            <li class="sc-twitter">
                                                <a href="<?php echo $twitter; ?>" target="_blank">
                                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                                </a>
                                            </li>
                                            <?php } ?>
                                            <?php if(!empty($facebook)){ ?>
                                            <li class="sc-facebook">
                                                <a href="<?php echo $facebook; ?>" target="_blank">
                                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                                </a>
                                            </li>
                                            <?php } ?>
                                            <?php if(!empty($instagram)){ ?>
                                            <li class="sc-insta">
                                                <a href="<?php echo $instagram; ?>" target="_blank">
                                                    <i class="fa fa-instagram" aria-hidden="true"></i>
                                                </a>
                                            </li>
                                            <?php } ?>
                                            <?php if(!empty($pinterest)){ ?>
                                            <li class="sc-pin">
                                                <a href="<?php echo $pinterest; ?>" target="_blank">
                                                    <i class="fa fa-pinterest" aria-hidden="true"></i>
                                                </a>
                                            </li>
                                            <?php } ?>
                                            <?php if(!empty($linkedin)){ ?>
                                            <li class="sc-linkin">
                                                <a href="<?php echo $linkedin; ?>" target="_blank">
                                                    <i class="fa fa-linkedin-square" aria-hidden="true"></i>
                                                </a>
                                            </li>
                                            <?php } ?>
                                            <?php if(!empty($g_plus)){ ?>
                                            <li class="sc-gplus">
                                                <a href="<?php echo $g_plus; ?>" target="_blank">
                                                    <i class="fa fa-google-plus" aria-hidden="true"></i>
                                                </a>
                                            </li>
                                            <?php } ?>
                                        </ul>
                                        <?php if(!empty($website)){ ?>
                                         <div class="author_profile">
                                            <a href="<?php echo $website; ?>" target="_blank">Author's Website </a>
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <?= do_shortcode('[embed-form]');?>
                        <?php
                        // The Query
                        $args_c = array(
                            'post_type' => 'post',
                            'meta_key'   => 'author',
                            'meta_value' => get_the_ID(),
                            'orderby' => 'ID',
                            'order' => 'DESC',
                            'posts_per_page' => -1

                        );
                        $query_c = new WP_Query( $args_c );
                        $post_count = $query_c->post_count;
                        ?>
                        <div class="text-center total-posts">
                            <?php echo $post_count; ?> posts
                        </div>
                        <div class="list-posts-wrapper" id="ajax-posts">
                            <?php
                            $args = array(
                                'post_type' => 'post',
                                'meta_key'   => 'author',
                                'meta_value' => get_the_ID(),
                                'orderby' => 'ID',
                                'order' => 'DESC',
                                'posts_per_page' => 10
                            );
                            $query = new WP_Query( $args );
                            if ( $query->have_posts() ) {
                                // The Loop
                                while ( $query->have_posts() ) : $query->the_post();

                                    get_template_part( 'content-author', get_post_format() );

                                endwhile;
                                wp_reset_postdata();
                            }
                            ?>
                        </div>
                        <?php if($post_count > 10){ ?>
                        <div class="content-blog-reamore text-center">
                            <button class="btn btn-success-custom" type="button" id="more_posts" data-author="<?php echo get_the_ID(); ?>">Load More <i class="fa fa-angle-down" aria-hidden="true"></i></button>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-xs-12 col-lg-4">
                    <div class="content-aside-holder">
                        <?php
                        //Sidebar
                        get_sidebar('author');
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer();
?>