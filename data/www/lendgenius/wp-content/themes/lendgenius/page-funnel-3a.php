<?php
/*
    Template Name: Funnel-3a
*/
get_header();

$campaignid = (isset($_GET['campaignid']) && !empty($_GET['campaignid']))? $_GET['campaignid']:'';
$hituid = (isset($_GET['hituid']) && !empty($_GET['hituid']))? $_GET['hituid']:'';
?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() ?>/LG_LP_UPD4/assets/styles/owl.carousel.css">
    <div class="page-funnel-3">
        <div class="page-wraper">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-6">
                        <h1 class="page-title">Business Information</h1>
                        <div class="funnel-form">
                            <div class="funnel-form-steps">
                                <div class="steps-indication">
                                    <div class="step-title">1/2</div>
                                </div>
                                <div class="step-1">
                                    <div class="content-refine-results tpl-info">
                                        <div class="content-wrap">
                                            <form method="post" action="#" id="l_form" class="form" onsubmit="return false;">
                                                <input type="hidden" name="action" value="loan_form">
                                                <input type="hidden" name="pg_name" value="form-1">
                                                <input type="hidden" name="campaignid" value="<?php echo $campaignid; ?>">
                                                <input type="hidden" name="hituid" value="<?php echo $hituid; ?>">
                                                <div id="formSlide">
                                                    <div class="refine-results-items" data-form_slide="0">
                                                        <div class="item">
                                                            <div class="item-field">
                                                                <input class="field-input field-input-biger" type="tel" id="RequestedAmount" name="loan_amount" placeholder="Loan amount" value="<?php echo isset($_POST['how_much']) ? $_POST['how_much']:''?>">
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="item-field">
                                                                <input class="field-input field-input-biger" type="tel" id="GrossSales" name="gross_sales" placeholder="Monthly revenue" value="<?php echo isset($_POST['gross_sales']) ? $_POST['gross_sales']:''?>">
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="item-field item-field-select">
                                                                <select class="field-styler-option field-input-biger" id="TimeInBusiness" name="time_in_business">
                                                                    <option value="" disabled <?php echo (!isset($_POST['time_in_business']) || $_POST['time_in_business'] == '') ? 'selected':''?>>Time in business</option>
                                                                    <option value="0" <?php echo (isset($_POST['time_in_business']) && $_POST['time_in_business'] == '0') ? 'selected':''?>>Starting a business</option>
                                                                    <option value="6" <?php echo (isset($_POST['time_in_business']) && $_POST['time_in_business'] == '6') ? 'selected':''?>>Less than 6 months</option>
                                                                    <option value="24" <?php echo (isset($_POST['time_in_business']) && $_POST['time_in_business'] == '24') ? 'selected':''?>>1 - 2 years</option>
                                                                    <option value="60" <?php echo (isset($_POST['time_in_business']) && $_POST['time_in_business'] == '60') ? 'selected':''?>>2 - 5 years</option>
                                                                    <option value="84" <?php echo (isset($_POST['time_in_business']) && $_POST['time_in_business'] == '84') ? 'selected':''?>>5 - 8 years</option>
                                                                    <option value="96" <?php echo (isset($_POST['time_in_business']) && $_POST['time_in_business'] == '96') ? 'selected':''?>>More than 8 years</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="item-field">
                                                                <input class="field-input field-input-biger" id="BusinessName" placeholder="Business name" type="text" name="business_name">
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="item-field item-field-select">
                                                                <select class="field-styler-option field-input-biger" id="IndustryDescription" name="industry_description">
                                                                    <option value="" disabled selected>Industry</option>
                                                                    <option value="Administrative">Administrative and Support Services</option>
                                                                    <option value="Agriculture">Agriculture</option>
                                                                    <option value="Art">Arts, Entertainment, and Recreation</option>
                                                                    <option value="AutoDealer">Automobile Dealers</option>
                                                                    <option value="AutoRepair">Automotive Repair and Maintenance</option>
                                                                    <option value="BusinessServices">Business Services</option>
                                                                    <option value="Construction">Construction</option>
                                                                    <option value="Education">Educational Services</option>
                                                                    <option value="Finance">Finance and Insurance</option>
                                                                    <option value="Forestry">Forestry, Fishing, and Hunting</option>
                                                                    <option value="FreightTrucking">Freight Trucking</option>
                                                                    <option value="Gambling">Gambling Industries</option>
                                                                    <option value="GasStations">Gas Stations</option>
                                                                    <option value="Greenhouse">Greenhouse, Nursery, and Floriculture</option>
                                                                    <option value="Healthcare">Healthcare and Social Assistance</option>
                                                                    <option value="Hotels">Hotels and Travel Accomodations</option>
                                                                    <option value="InformationTechnology">Information and Technology</option>
                                                                    <option value="Legal">Legal Services</option>
                                                                    <option value="Management">Management of Companies</option>
                                                                    <option value="Manufacturing">Manufacturing</option>
                                                                    <option value="Mining">Mining</option>
                                                                    <option value="NonProfit">Non-Profit Organizations</option>
                                                                    <option value="OtherServices">Other Services</option>
                                                                    <option value="PersonalCare">Personal Care Services</option>
                                                                    <option value="PhysicianDentistHealth">Physician, Dentist, or Health Practitioner</option>
                                                                    <option value="PublicAdministration">Public Administration</option>
                                                                    <option value="RealEstate">Real Estate</option>
                                                                    <option value="ReligiousInstitutions">Religious Institutions</option>
                                                                    <option value="RentalLeasing">Rental and Leasing</option>
                                                                    <option value="FoodRestaurants">Restaurants and Food Services</option>
                                                                    <option value="RetailStores">Retail Stores</option>
                                                                    <option value="Transportation">Transportation</option>
                                                                    <option value="Utilities">Utilities</option>
                                                                    <option value="Veterinarians">Veterinarians</option>
                                                                    <option value="Warehousing">Warehousing and Storage</option>
                                                                    <option value="WasteManagement">Waste Management &amp;amp; Remediation Services</option>
                                                                    <option value="WholesaleTrade">Wholesale Trade</option>
                                                                    <option value="ScientificServices">Professional, Scientific, and Technical Services</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="item-field">
                                                                <input class="field-input field-input-biger" id="BusinessAddress" type="text" placeholder="Business street address" name="business_address">
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="item-field">
                                                                <input class="field-input field-input-biger" id="ZipCodeBusiness" placeholder="ZIP code" type="tel" name="zip_code" >
                                                                <span id="city_state"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="refine-results-items" data-form_slide="1">
                                                        <div class="item">
                                                            <div class="item-field">
                                                                <input class="field-input field-input-biger" id="FirstName" placeholder="First name" type="text" name="first_name">
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="item-field">
                                                                <input class="field-input field-input-biger" id="LastName" placeholder="Last name" type="text" name="last_name">
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="item-field">
                                                                <input class="field-input field-input-biger" id="PhoneHome" placeholder="Primary phone" type="tel" name="phone">
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="item-field">
                                                                <input class="field-input field-input-biger" id="Email" placeholder="Email" type="email" name="email">
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="item-field item-field-select">
                                                                <select class="field-styler-option field-input-biger" id="Credit" name="credit">
                                                                    <option value="" disabled <?php echo (!isset($_POST['credit']) || $_POST['credit'] == '') ? 'selected':''?>>Credit score</option>
                                                                    <option value="EXCELLENT" <?php echo (isset($_POST['credit']) && $_POST['credit'] == 'EXCELLENT') ? 'selected':''?>>Excellent (760+)</option>
                                                                    <option value="VERYGOOD" <?php echo (isset($_POST['credit']) && $_POST['credit'] == 'VERYGOOD') ? 'selected':''?>>Very Good (720-759)</option>
                                                                    <option value="GOOD" <?php echo (isset($_POST['credit']) && $_POST['credit'] == 'GOOD') ? 'selected':''?>>Good (660-719)</option>
                                                                    <option value="FAIR" <?php echo (isset($_POST['credit']) && $_POST['credit'] == 'FAIR') ? 'selected':''?>>Fair (600-659)</option>
                                                                    <option value="POOR" <?php echo (isset($_POST['credit']) && $_POST['credit'] == 'POOR') ? 'selected':''?>>Poor (580-599)</option>
                                                                    <option value="VERYPOOR" <?php echo (isset($_POST['credit']) && $_POST['credit'] == 'VERYPOOR') ? 'selected':''?>>Very Poor (500+)</option>
                                                                    <option value="UNSURE" <?php echo (isset($_POST['credit']) && $_POST['credit'] == 'UNSURE') ? 'selected':''?>>Not Sure</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="disclaimer">
                                                            <p>By clicking “Continue”, I confirm I have reviewed and agree to the <a href="/terms-of-service/" target="_blank">Terms of Service</a>, and I further agree that my click is my electronic signature, and I authorize LendGenius and its participating lenders to contact me using the information I provided above, including via email, phone, text message (SMS/MMS) and/or cell phone (including use of automated dialing equipment and pre-recorded calls and/or messages), in order to send me information about products, services and offers from LendGenius or its participating lenders. I understand that I have no obligation to accept a loan once connected with a lender. I further understand that this consent is not required to obtain a loan, and that I may revoke this consent at any time by contacting LendGenius at support@lendgenius.com.
                                                                LendGenius does not charge anything for borrowers to use it’s loan matching service. Generally, LendGenius only makes money from the lenders once a loan has been issued. </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="refine-results-footer">
                                                    <button class="btn btn-primary to-action" data-form_btn="0">Continue</button>
                                                    <span data-form_btn="0" class="to-back refine-results-back" style="display:none;">
                                                        Back
                                                    </span>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="page-wraper-bg">
                <div class="funnel-info">
                    <div class="funnel-info-body">
                        <h3 class="item-title">Commonly Asked Questions</h3>
                        <?php
                        // Start the loop.
                        while ( have_posts() ) : the_post();

                            the_content();

                        endwhile; ?>
                    </div>
                    <div class="funnel-info-footer">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bs-example-modal-lg" id="submitModal" role="dialog" style="display:none;">
    	  <div class="modal-dialog">
    		<div class="modal-content">
    		  <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i></span></button>
         </div>
    		  <div class="modal-body">
            <h4 class="modal-title">Hey, just a quick notice before we connect you with a lender</h4>

          <div class="row modal-inner">
            <div class="col-md-8">
            <p class="left-col-content">
              We are still working on our comparison tool, so we are currently unable to show you multiple offers right now.<br/>
              Not to worry! You can still proceed and we will connect you to a single lender or finance company from our approved network.
              This service is free to you, and you are under no obligation to accept any particular financing offer.<br/>
              You can always come back to our website and try again. Our comparison tool should be up and running soon.
              Click continue to proceed.
            </p>
          </div>
          <div class="col-md-4 right-col-img">
             <img src="<?php echo get_template_directory_uri(); ?>/LG_LP_UPD4/images/lamp.jpg"  />
          </div>
        </div>

    			<form action="#" method="#">
    				<button type="button" class="btn btn-primary customform_auto" id="sendModal" data-loading-text="Loading...">Continue</button>
    			</form>
    		  </div>
    		</div><!-- /.modal-content -->
    	  </div><!-- /.modal-dialog -->
    	</div><!-- /.modal -->

<?php
get_footer();
