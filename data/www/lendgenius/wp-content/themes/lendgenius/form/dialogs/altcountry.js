﻿(function () {

    var s = "";
    s += " <div class=\"b2c-dialog-container\">";
    s += "        <div class=\"b2c-country\">";
    s += "            <p class=\"b2c-form-title\">We have a deal for you!<\/p>";
    s += "            <p class=\"b2c-form-subtitle\">Click to apply a loan in {country}<\/p>";
    s += "            <a href=\"{url}\" class=\"b2c-btn b2c-btn-{code} b2c-btn-link\">{site}<\/a>";
    s += "        <\/div>";
    s += "    <\/div>";

    var data = $('div.pd-content-frame').data();

    s = s.replace('{url}', data.AlternativeLink);
    s = s.replace('{site}', data.AlternativeSite);
    s = s.replace('{country}', data.ResolvedCountryName);
    s = s.replace('{code}', data.ResolvedCountryCode);

    data.setFixedDialog();

    $('div.pd-content-frame').html(s);
    $('div.pd-content-frame a.b2c-btn-link').on('click', function () { $(window).off('beforeunload'); window.location = $(this).attr('href');  return false; });
})();