﻿(function () {

    var data = $('div.pd-content-frame').data();

    var s = "<div class=\"b2c-dialog-container\">";
    s += "<div class=\"b2c-title-returned b2c-dialog-returned\">";
    s += "<p>Looks like we have your email on file.<\/p>";
    s += "<p>Would you like to pull up your existing application?<\/p>";
    s += "<\/div>";
    s += "<div class=\"b2c-dialog-email\">" + data.UserEmail + "<\/div>";
    //s += "<div class=\"b2c-btn-wrap\"><a id='btnPullUp' class=\"b2c-btn\">Pull Up Existing Application<\/a></div><\/div>";
    s += "<\/div>";

    data.appendButton('Pull Up Existing Application', 'pullup', '', function () { data.CloseDialog(); data.LoadRetUserSetup(); });
    data.setFixedDialog();
    $('div.pd-content-frame').html(s);
    //  $('div.pd-content-frame #btnPullUp').on('click', function () { data.CloseDialog(); data.LoadRetUserSetup(); });
})();