﻿(function plugin() {

    if (!lmpost.options.plugins) {
        lmpost.options.plugins = {};
    }

    var plugins = lmpost.options.plugins;
    if (!plugins['funnel']) {
        plugins['funnel'] = plugin;
    }

    $('iframe.b2c-funnel').remove();

    var form,
    funnels = {},
    body = $('body'),
    baseUrl = 'http:\\localhost\funnel';

    var openFunnel = function (cls) {
        if (!funnels[cls]) {
            body.append('<iframe class="b2c-funnel" id="fnl-' + cls + '" style="display:none" src="' + baseUrl + '\\' + cls + ".html&uts" + new Date().getTime() + '" />');
            funnels[cls] = true;
        }
    };

    var checkStepFunc = function () {
        if (!form) {
            form = $('form.b2cform');
            return;
        }

        var currentStep = form.find('div.b2c-form:visible');
        if (currentStep && currentStep.length) {
            var classNames = currentStep.attr('class').split(' ');
            for (var i = 0; i < classNames.length; i++) {
                if (classNames[i].substr(0, 8) == 'b2c-step') {
                    openFunnel(classNames[i]);
                    return;
                }
            }
        }
    };

    window.setInterval(checkStepFunc, 1000);

})();
