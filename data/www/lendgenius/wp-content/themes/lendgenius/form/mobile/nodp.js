﻿(function () {
    lmpost.initMobileDP = function (ui) {
        var dtRegex = /^((0*[1-9]{1})|10|11|12)\/(0*[1-9]{1}|[1-2]{1}[0-9]{1}|3[0-1]{1})\/[0-9]{4}$/;
        ui.find('.b2c-date-mobile')

                .on('change', function () {
                    var value = $(this).val();

                    if (value && value.indexOf('/') < 0) {
                        value = (value.length == 0) ? "" : value.replace(/\D/g, '');
                        value = value.substr(0, 2) + '/' + value.substr(2, 2) + '/' + value.substr(4, 4);
                        $(this).val(value);
                    }
                }).rules('add', { regex: dtRegex });
    }
})();