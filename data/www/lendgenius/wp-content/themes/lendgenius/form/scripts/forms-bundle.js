(function() {
    function loadScripts(sequence, callback) {

        var next = function(headNode) {
            var me = this,
                seq,
                nextLvl;

            me.head = (headNode) ? headNode : me;

            me.run = function() {
                var onNext = function() {
                    nextLvl && nextLvl.run();
                };

                if (seq instanceof Function) {
                    seq();
                    nextLvl && nextLvl.run();

                    return;
                }

                if (seq) {
                    if (!seq.length) {
                        onNext(); // skip empty seqs
                    } else {
                        loadScripts(seq, onNext);
                    }
                }
            };

            me.then = function(list) {
                seq = list;
                nextLvl = new next(me.head);
                return nextLvl;
            };

            me.nowait = function(list) {
                seq = list;
                list.noblock = true;
                nextLvl = new next(me.head);
                return nextLvl;
            };

            me.chain = function(continuation) {

                if (continuation) {
                    me.run = function() {
                        // Continuation can be defined statically or dynamically.
                        if (continuation instanceof Function) {
                            continuation = continuation();
                        }

                        if (!continuation) {
                            continuation = new lmpost.loader(); // Create a fake continuatuion element if not found.
                        }

                        continuation.then(
                            function() {
                                nextLvl && nextLvl.run(); // After executing the continuation chain, pass control to the following action in current chain.
                            });
                        continuation.head.run();
                    };
                }

                nextLvl = new next();
                return nextLvl;
            };

            me.waitFor = function(condition) {
                nextLvl = new next(me.head);

                var wait = function() {
                    if (condition()) {
                        nextLvl && nextLvl.run();
                    } else {
                        window.setTimeout(wait, 50); // May be we're lucky next time.
                    }
                };

                me.run = wait; // Here is the magic, the wait func s/b triggered by run in the chain.

                return nextLvl;
            };

            me.waitForScript = function(scriptAliases, scriptUrl, condition) {
                var toRun = [],
                    url,
                    referenced;

                if (!condition || condition()) {
                    return me;
                }


                var testFun = function() {
                    url = (scriptUrl instanceof Function) ? scriptUrl() : scriptUrl;


                    // Search for defined script.
                    var scripts = document.getElementsByTagName('script');

                    for (var i = 0; i < scripts.length; i++) {
                        var srcElem = scripts[i],
                            src = srcElem.src;

                        for (var j = 0; j < scriptAliases.length; j++) {
                            if (src && (endsWith(src, scriptAliases[j]))) {
                                referenced = true;
                                break;
                            }
                        }

                        if (referenced) break;
                    }

                    if (!referenced && !condition()) {
                        // Include jq script in the loading sequence if not already referenced by teh page.
                        toRun.push(url.replace(/^http:|^https:|^file:/gi, getResourcesProtocol()));
                    }
                };

                return me.then(testFun).then(toRun).waitFor(condition);
            };
        };

        var nextSeq = new next();
        if (!sequence) return nextSeq;

        sequence.reverse();

        var len = sequence.length;

        var onload = function() {
            len--;
            if (!len) {
                // All scripts have been loaded, proceed to next lvl.
                nextSeq.run();
                callback && callback();
            }
        };

        while (sequence.length) {

            var item = sequence.pop();

            lmpost.loadScript(item, null, onload);
        }

        return nextSeq;
    }

    lmpost.loadScripts = lmpost.loader = loadScripts;
})();
lmpost.pdcore = function (instrumented) {


    var mobile = (/iphone|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()));
    var getReturningLoader = function () {
        var loader = new lmpost.loader();

        return loader.then([lmpost.options.form + '/retform.js'])
            .then(function () { lmpost.setupReturning(); })
            .then(['../scripts/searching.lenders.js']);
    };

    // Test funcs.
    function getDigits(value) {
        return (!value || value.length == 0) ? "" : value.replace(/\D/g, '');
    };

    var loadRetUserStep = function () {
        var $ = jQuery;
        $('form.b2cform').remove();
        lmpost.cleanupFormUI();
        $('div.b2cloader').show();
        lmpost.formState = 'wait';
        var ldr = getReturningLoader();
        h = ldr.head;
        ldr.waitFor(function () {
            return lmpost.formState == 'active'; // Wait while returning user sequence is processed.
        })
            .then(function () {
                $('#leadsb2cformsrc').after(window.lmpost.formData);
                $('div.b2cloader').hide();
            }
            )
            .then(function () { // re-init plugins
                var plugins = lmpost.options.plugins;
                if (plugins) {
                    for (var idx in plugins) {
                        plugins[idx]();
                    }
                }
            })
            .chain(
                function () {
                    return lmpost.options.appOptions && lmpost.options.appOptions.postJQScripts;
                }
            )
            .then(function () { lmpost.customInit(); });

        h.run();
    };

    lmpost.handleReturningUser = function (sesuid) {
        lmpost.formState = 'wait';
        var email = lmpost.email,
            loaderFunc = function (data) {
                if (data && data.Result == 1 && data.RID) {
                    lmpost.RID = data.RID;
                    lmpost.lastDate = data.LastDate;
                    lmpost.retfields = data.Fields;
                }
                getReturningLoader().head.run();
            };

        if (sesuid && email) {
            $.ajax(
            {
                url: window.lmpost.urls.apiUrl + 'misc/?action=leadreturn&responsetype=json&'
                    + 'email=' + escape(lmpost.email)
                    + '&sesuid=' + sesuid + "&c=" + lmpost.options.campaignid,
                timeout: 15000,
                dataType: 'jsonp',
                success: loaderFunc,
                error: loaderFunc
            });
        } else {
            loaderFunc();
        }
    };

    lmpost.isReturning = function (data) {

        if (data.Result == 3) {
            lmpost.returning = true;
            lmpost.dialogPage("../dialogs/retuser.js", 'Welcome Back!', { LoadRetUserSetup: loadRetUserStep, UserEmail: $('form.b2cform').find('#Email').val() });

        }
    };

    lmpost.fillMonthlyIncome = function (elem) {
        var values = [{ k: 700, v: 'Less than $700' }];

        for (var inc = 701; inc < 2500; inc += 100) {
            values.push({ k: inc + 99, v: '$' + inc + ' - $' + (inc + 99) });
        }

        for (var inc = 2501; inc < 5000; inc += 500) {
            values.push({ k: inc + 499, v: '$' + inc + ' - $' + (inc + 499) });
        }

        for (var inc = 5001; inc < 10000; inc += 1000) {
            values.push({ k: inc + 999, v: '$' + inc + ' - $' + (inc + 999) });
        }

        values.push({ k: 10001, v: '$10,000+' });

        for (var i = 0; i < values.length; i++) {
            var incData = values[i];

            elem.append('<option value="' + incData.k + '">' + incData.v + '</option>');
        }
    };

    if (!lmpost.fillRequestedAmount) {
        lmpost.fillRequestedAmount = function (elem) {
            var values = [{ k: 100, v: '$100' }, { k: 200, v: '$200' }, { k: 300, v: '$300' }, { k: 400, v: '$400' }, { k: 500, v: '$500' }, { k: 600, v: '$600' }, { k: 700, v: '$700' }, { k: 800, v: '$800' }, { k: 900, v: '$900' }, { k: 1000, v: '$1000' }];

            elem.find('option:not([value=""])').remove();

            for (var i = 0; i < values.length; i++) {
                var incData = values[i];

                elem.append('<option value="' + incData.k + '">' + incData.v + '</option>');
            }
        }
    }

    function addDays(date, days) {
        var result = new Date(date);
        result.setDate(date.getDate() + days);
        return result;
    }

    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

    lmpost.configurePaydate = function (elem) {
        var dp = jQuery(elem).is("input") && jQuery(elem).datepicker;
        if (dp) {
            dp('option', 'beforeShowDay',
                function (date) {
                    var isHoliday = isHolidayDate(date);
                    if (isHoliday) {
                        return false;
                    };

                    return jQuery.datepicker.noWeekends(date);
                }
            );
        } else {
            var sel = jQuery(elem), placeholder = sel.attr('placeholder');

            if (!sel.find('option').length) {
                elem.append('<option value></option>');
            }

            for (var i = 1; i <= 40; i++) {
                var dt = addDays(new Date(), i);

                if (i == 1 || dt.getDate() == 1) {
                    elem.append('<option disabled>' + months[dt.getMonth()] + ' - ' + dt.getFullYear() + '</option>');
                }

                if (!isHolidayDate(dt)) {
                    var dow = dt.getDay();
                    if (dow > 0 && dow < 6) {
                        elem.append('<option value="' + (dt.getMonth() + 1) + '/' + dt.getDate() + '/' + dt.getFullYear() + '">' + days[dt.getDay()].substr(0, 3) + ' ' + months[dt.getMonth()].substr(0, 3) + '-' + dt.getDate() + '-' + dt.getFullYear() + '</option>');
                    }
                }
            }
        }
    };

    var holidayDates = [

        //2015
        '10/12/2015', '11/11/2015', '11/26/2015',
        '12/25/2015'
    ];

    var isHolidayDate = function (date) {
        for (var i = 0; i < holidayDates.length; i++) {
            if (new Date(holidayDates[i]).toString() == date.toString()) {
                return [true];
            }
        }

        return false;
    };

    // Override std init

    lmpost.customInit = function () {

        var $ = jQuery,
            form = $('form.b2cform');

        if (!(lmpost.returning || lmpost.options.ignoreReturning)) {
            form.find('#Email').on('change', function () {
                var email = this.value;

                if (lmpost.returning || (email && lmpost.email && lmpost.email.toLowerCase() == email.toLowerCase())) {
                    return;
                }

                if (email) {
                    lmpost.email = email;
                    var url = lmpost.actionUrl('campaignstatus',
                    {
                        c: lmpost.options.campaignid,
                        email: email,
                        leadtypeid: lmpost.options.leadtypeid,
                        mailsrc: 'field',
                        callback: 'lmpost.isReturning'
                    });

                    lmpost.loadScript(url);
                }
            });
        }

        if (!lmpost.RID) {
            var checkbox =
                            '<div class="b2c-radio-wrap pd-accept b2c-disclimer">' +
                                '<label><input type="checkbox" name="Optin" id="Optin" value="yes">' +
                                    'By checking this box, I give my written consent to receive SMS/text messages and autodialed or prerecorded calls from you and your marketing partners at the telephone number I provided. I understand my consent is not a condition of obtaining a loan.' +
                                    '<br/> Why do we require this? One reason is that lenders may confirm your application by phone for quick approval and funding. The faster they can reach you, the quicker you get your cash!' +
                                '</label> ' +
                            'By clicking “Get Cash Now!” you affirm that you have read, understand, and agree to the ' +
                            '<a class="b2c-popup-link" href="../documents/_disclaimer.js" target="_blank">Disclaimer</a>, ' +
                            '<a class="b2c-popup-link"  href="../documents/_privacypolicy.js" target="_blank">Privacy Policy</a> and ' +
                            '<a class="b2c-popup-link b2c-popup-terms" href="../documents/_terms.js" target="_blank">Terms of Use</a>, ' +
                            'your click is your electronic signature, and you authorize us to share your information with lenders and other marketing partners that might use autodailers or prerecorded messages to call or text you on your mobile phone or landline. </div>';
            form.find('[class*="b2c-step"]').last().find('.b2c-row').last().after(checkbox);
        }

        form.find('#BankABA').on('change', function () {
            var me = this, lookupfield = form.find('#BankName');
            $.ajax({
                url: lmpost.actionUrl('validatebankaba', { bankaba: me.value }),
                type: "get",
                dataType: 'jsonp',
                timeout: 5000,
                success: function (data) {
                    if (data.Result && data.Result != 4) {
                        var res = { BankName: '' };
                        try {
                            res = eval('(' + data.Conversion + ')');
                        } finally {
                            lookupfield.val(res.BankName);
                            lookupfield.valid();
                        }
                    }
                }
            });
            return true;
        });

        form.find('#FirstName,#LastName,#Email').on('change', function () {
            var fn = $('#FirstName').val(),
                ln = $('#LastName').val(),
                em = $('#Email').val(),
                phone = $('#PhoneHome').val(),
                adr = $('#Address1').val(),
                zip = $('#ZipCode').val();
            if (fn && ln && em) {
                $.ajax(
                {
                    url: lmpost.actionUrl('regvisitor', { fname: fn, lname: ln, email: em, ph: phone, ad: adr, zp: zip }),
                    type: "get",
                    dataType: 'jsonp',
                    timeout: 5000,
                    success: function (data) {

                    }
                });
            }
        });
        form.find('#ZipCode').on('change', function () {
            var me = this, lookupLabel = form.find('span.pd-zip-lookup');
            if (!lookupLabel.length)
                lookupLabel = $(me).after('<span class="pd-zip-lookup"/>').next();
            $.ajax(
                {
                    url: lmpost.actionUrl('validatezipcode', { zipcode: me.value }),
                    type: "get",
                    dataType: 'jsonp',
                    timeout: 5000,
                    success: function (data) {
                        if (data.Result && data.Result != 4) {
                            var res = { StateShort: '', City: '' };
                            try {
                                res = eval('(' + data.Conversion + ')');
                            } finally {
                                if (res.StateShort == 'NY' || res.StateShort == 'VT' || res.StateShort == 'WV') {
                                    window.onbeforeunload = null;
                                    document.location.href = 'https://www.paydaylendersearch.com/api2/Redirect-Prosper/?c=' + lmpost.options.campaignid;
                                }

                                lookupLabel.text(res.City + ', ' + res.StateShort);
                                var dlState = $('#DriversLicenseState');
                                if (!dlState.val() && res.StateShort) {
                                    dlState.val(res.StateShort);
                                    dlState.change();
                                }
                            }
                        } else {
                            form.find('#ZipCode').addClass('b2c-error').val('');
                            lookupLabel.text('');

                            if (data.AlternativeSite) {
                                lmpost.dialogPage("../dialogs/altcountry.js",
                                    "ARE YOU FROM " + data.ResolvedCountryName.toUpperCase() + "?",
                                    data);
                            }
                        }
                    },
                    error: function () {
                        form.find('#ZipCode').addClass('b2c-error').val('');
                        lookupLabel.text('');
                    }
                }
            );
        });

        //Adding Masks
        $('#PhoneHome').mask('000-000-0000');
        $('#PhoneWork').mask('000-000-0000');
        $('#SSN1').not('.b2c-returned #SSN1').mask('000-00-0000');

        // Phone validator.
        $.validator.addMethod(
            "ValidatePhone",
            function (value, element) {
                value = getDigits(value);
                value = value.replace(/^1/, '');

                if (value.length == 0) {
                    element.value = value;
                    return true;
                }
                var reg = /^([2-9][0-9]{2})([2-9][0-9]{2})([0-9]{4})$/;
                var result = this.optional(element) || (reg.test(value));
                if (result) {
                    var pc = value.match(reg);
                    element.value = pc[1] + '-' + pc[2] + '-' + pc[3];
                    return true;
                }
                value = value.substring(0, 3) + '-' + value.substring(3, 6) + '-' + value.substring(6, 10);
                value = value.replace("--", "");

                element.value = value;

                return false;

            },
            "Please provide a valid phone number");

        // Phone maxlenght.
        //form.find('[name*="Phone"]').each(function () {
        //    var mlen = 12,
        //        el = $(this),
        //        pfx = el.closest('div').find('.b2c-phone-prefix');

        //    if (pfx.length) {
        //        mlen = mlen - pfx.text().length;
        //    }

        //    el.attr('maxlength', mlen);
        //}
        //);


        $.validator.addMethod("KnownZipCode", function (value, element, len) {
            var result = (window.ZipCodeValidation) ? ZipCodeValidation.isZipValid(value) : true,
                lookupLabel = $('span.pd-zip-lookup'),
                len = value && value.length;

            if (!lookupLabel.length) {
                lookupLabel = $(element).after('<span class="pd-zip-lookup"/>').next();
            }

            if (!len || !value.match(/[0-9]{5}/)) {
                lookupLabel.text('');
                return false;
            };

            if (!result)
                lookupLabel.text('');

            return result;
        }, '');


        $.validator.addMethod(
            "isValidABA",
            function (value, element) {
                var isValid = false,
                    c = 0,
                    i = 0,
                    n = 0,
                    t = "";
                try {
                    for (i = 0; i < value.length; i++) {
                        c = parseInt(value.charAt(i), 10);
                        t = t + c;
                    }
                    if (t.length == 9) // pass 9 digit check
                    {
                        n = 0;
                        for (i = 0; i < t.length; i += 3) {
                            n += (parseInt(t.charAt(i), 10) * 3) + (parseInt(t.charAt(i + 1), 10) * 7) + parseInt(t.charAt(i + 2), 10);
                        }
                        isValid = (n != 0 && n % 10 == 0);
                    }
                } catch (err) {
                }
                return this.optional(element) || isValid;
            }, ""
        );

        $.validator.addMethod(
            "IsWorkingDay",
            function (value, element) {
                if (!value) {
                    return true;
                }

                var date = new Date(value),
                    day = date.getDay();

                return (day > 0 && day < 6) ? !isHolidayDate(date) : false;
            }
        );

        $.validator.addMethod(
            "validateSSN",
            function (value, element) {
                value = (value.length == 0) ? "" : value.replace(/\D/g, '');

                var reg = /^([0-9]{3})([0-9]{2})([0-9]{4})$/;
                var result = value.length > 0 && reg.test(value);
                if (result) {
                    var pc = value.match(reg);
                    element.value = pc[1] + '-' + pc[2] + '-' + pc[3];
                    return true;

                }

                var len = (value.length > 9) ? 9 : value.length;
                value = value.replace(/\s|-/g, '');
                value = value.substring(0, 3) + '-' + value.substring(3, 5) + '-' + value.substring(5, len);

                element.value = value;

                return false;
            }, ""
        );


        var ui = lmpost.getFormUI();

        // Change months at controls default settings.
        form.find('select#MonthsAtAddress,#MonthsAtBank,#MonthsEmployed').each(function () {
            var el = $(this);
            el.find('option:not([value=""])').remove();
            var opts = [{ v: 6, s: "1 to 6 Months" }, { v: 12, s: "7 to 12 Months" }, { v: 24, s: "1 to 2 Years" }, { v: 36, s: "2 to 3 Years" }, { v: 60, s: "3 to 5 Years" }, { v: 72, s: "5+ Years" }];
            for (var i = 0; i < opts.length; i++) {
                el.append('<option value="' + opts[i].v + '">' + opts[i].s + '</option>')
            }
        });

        // Init mobile-friendly dp
        lmpost.preInitMobileDP(form, true);

        // Copy value from SSN1 field to SSN without optional characters.
        form.find('#SSN1').on('change',
            function () {
                var ssnRaw = $(this).val();

                ui.byID('SSN').val(ssnRaw.replace(/\s|-/g, '').substr(0, 9));
            }
        );


        form.find('#RequestedAmount').on('change',
            function () {
                var val = $(this).val(),
                    ltidCt = $('#leadtypeid'),
                    curLt = parseInt(ltidCt.val(), 10),
                    newLt = 19;

                if (!val) return;

                if (parseInt(val, 10) <= 1000) {
                    newLt = 9;
                } else {
                    newLt = 19;
                }

                if (curLt != newLt) ltidCt.val(newLt);
            }
        );

        //Payday-Installment switcher for hybrid forms
        var creditScore = '<div class="credit-score b2c-row"><label for="credit">What is your credit score?</label><div class="b2c-label-col"><select name="credit" id="Credit"><option value="">Select</option><option value="EXCELLENT">Excellent Credit (760+)</option><option value="VERYGOOD">Very Good Credit (720-759)</option><option value="GOOD">Good Credit (660-719)</option><option value="FAIR">Fair Credit (600-659)</option><option value="POOR">Poor Credit (580-599)</option><option value="VERYPOOR">Very Poor Credit (500+)</option><option value="UNSURE">Not Sure</option></select></div></div>';
        var cresitScoreSwitcher = function () {
            var amountVal = $(".b2c-flex #RequestedAmount").val();
            if (amountVal >= 1500 && $('.b2c-flex .credit-score').length == 0) {
                form.find('[class*="b2c-step"]').last().find('.b2c-disclimer').before(creditScore);
                return false;
            }
            else if (amountVal >= 1500 && $('.b2c-flex .credit-score').length != 0) {
                return false;
            }
            else {
                $('.b2c-flex .credit-score').remove();
                return false;
            }
        };

        $("#RequestedAmount").change(function () {
            cresitScoreSwitcher();
        });
        $(function () {
            cresitScoreSwitcher();
        });

        // Only app specific rules are defined here.
        var validationRules = {
            ZipCode: { KnownZipCode: true },
            SSN1:
            {
                required: true,
                validateSSN: true,
                minlength: 9
            },
            DriversLicense: { required: true, minlength: 5 },
            BankName: { required: true, minlength: 2 },
            BankABA: { required: true, digits: true, minlength: 9, isValidABA: true },
            BankAccountNumber: { required: true, minlength: 5 },
            PayDate1: { IsWorkingDay: true }
        };


        var formNode = $('form.b2cform');
        lmpost.fillMonthlyIncome(formNode.find('#MonthlyIncome'));
        lmpost.fillRequestedAmount(formNode.find('#RequestedAmount'));

        ui.InitForm(validationRules);


        // Configure paydate field.
        lmpost.configurePaydate(ui.byID('PayDate1'));
        lmpost.configurePaydate(ui.byID('PayDate2'));

        var pdOptions = lmpost.options.payday;

        ui.find("a.b2c-popup-link").on('click',
            function () {
                var el = $(this);
                lmpost.dialogPage(el.attr("href"), el.text());
                return false;
            }
        );

        var hideLogo = pdOptions && pdOptions.hideNortonLogo;

        if (hideLogo) {
            ui.find('.b2c-norton-top').hide();
        } else {

            $(document).on('click', '.b2c-norton-top', function () {
                var sdomain = 'www.paydaylendersearch.com';
                if (pdOptions && pdOptions.NSDomain)
                    sdomain = pdOptions.NSDomain;
                var url = 'https://trustsealinfo.verisign.com/splash?form_file=fdf/splash.fdf&dn=' + sdomain + '&lang=en';
                window.open(url, '_blank', 'height=445,width=560');
            });
        }

        // Init from query params.
        var queryParams = $.parseQuery();

        // Merge with defaults if exists.
        if (lmpost.defaultFields) {
            var defFields = lmpost.defaultFields;
            for (var i = 0; i < defFields.length; i++) {
                var defKey = defFields[i].key;
                if (!queryParams[defKey]) queryParams[defKey] = defFields[i].value;
            }
        }

        for (var key in queryParams) {
            var val = queryParams[key],
                target = ui.find('[name="' + key + '"]');

            if (target.length && target.val && val) {
                val = decodeURIComponent(val);

                if (target.data('qs-set')) {
                    target.removeData('qs-val');
                    target.removeData('qs-set');
                    return;
                }

                jQuery.fn.reverse = [].reverse;

                if (!target.find('option[value=' + val + ']').length) {
                    var prevVal;
                    target.find('option').reverse().each(function (index, value) {

                        if (this.value && parseInt(val) >= parseInt(this.value)) {
                            val = this.value;
                            return false;
                        }

                        if (!this.value && prevVal) val = prevVal;
                        else prevVal = this.value;
                    });

                }


                target.val(val);
                target.data('qs-val', val);
                target.change();
            }

        }

        $(function () {
        });

        //Hints for forms with content
        var ssnHint = '<div class="b2c-hint"><strong><em class="b2c-ico-help">?</em> <p>Your social security number is encrypted with a 256-bit SSL.</strong> To protect your identity and privacy we never store your social security number</p></div>';
        var bankAbaHint = '<div class="b2c-hint"><em class="b2c-ico-help">?</em> <p>You data is secured using industry standard for the site information security. Your ABA/Routing number can be found on most bank statements as well as on the bottom of your checks.</p> <em class="b2c-hint-aba"></em></div>';
        var bankNumberHint = '<div class="b2c-hint"><em class="b2c-ico-help">?</em> <p>Your <strong>Checking Account</strong> number can be found on most bank statements as well as on the bottom of your checks.</p> <em class="b2c-hint-account"></em></div>';


        $.fn.hint = function (param) {
            $(this).focusin(function () {
                if ($(this).parent().find('.b2c-hint').length != 0) {
                    $(this).siblings(".b2c-hint").show();
                } else {
                    $(this).parent().append(param);
                }
            });
            $(this).focusout(function () {
                $(this).siblings(".b2c-hint").hide();
                return false;
            });
        };

        $('#BankAccountNumber').hint(bankNumberHint);
        $('#SSN1').hint(ssnHint);
        $('#BankABA').hint(bankAbaHint);

        //Show CarFree field
        $("#OwnCar").change(
            function () {
                if ($(this).val() == 1) {
                    $(".b2c-carfree").show();
                } else {
                    $(".b2c-carfree").hide();
                }
            });

    };
};

lmpost.setupConfig = function () {

    var lmpost = window.lmpost,
        $ = window.jQuery;

    if (lmpost && lmpost.configCoreStarted) return;

    if (!lmpost) {
        if (window.LeadsB2C) {
            window.lmpost = window.LeadsB2C;
            window.lmpost.campaignid = window.LeadsB2C.campaignId;
        } else
            lmpost = window.lmpost = {};
        lmpost.options = {};
    }
    if (!lmpost.urls) lmpost.urls = {};

    if (!lmpost.urls.apiUrl) lmpost.urls.apiUrl = 'https://www.loanmatchingservice.com/';
    if (!lmpost.urls.submitUrl) lmpost.urls.submitUrl = 'post/live.aspx';
    if (!lmpost.urls.supportUrl) lmpost.urls.supportUrl = 'https://www.loanmatchingservice.com/misc/';
    if (!lmpost.urls.hitUrl) lmpost.urls.hitUrl = 'https://www.sparning.com/hit/hit.core.js';

    //if (!lmpost.urls.apiUrl) lmpost.urls.apiUrl = 'http://localhost:51541/';
    //if (!lmpost.urls.submitUrl) lmpost.urls.submitUrl = 'post/live.aspx';
    //if (!lmpost.urls.supportUrl) lmpost.urls.supportUrl = 'http://localhost:51541/misc/';
    //if (!lmpost.urls.hitUrl) lmpost.urls.hitUrl = 'http://localhost:51541/hit/hit.core.js';
    var scripts = ['../scripts/plugins/jquery.poshytip.min.js', '../scripts/searching.lenders.js'];
    var preJqScripts = [];

    if (!lmpost.pdcore) {
        preJqScripts.push('../scripts.core/payday.core.js');
    }

    var appOptions =
    {
        styles: ['/pd-2.css'], // Array of required stylesheets.
        formStartupFile: '/createform.js', // Form source file. 
        preJQScripts: new lmpost.loader().then(preJqScripts), // Sequence of required scripts to load BEFORE jq is loaded.
        postJQScripts: // Sequency of required scripts loaded after jq is loaded.
            new lmpost.loader()
                .then(
                    // Placeholders script is required for older MS IEs.
                    function () {
                        if (jQuery('#b2cOldIE').length) {
                            scripts.push('../scripts/plugins/jquery.placeholder-1.1.9.js');
                        };
                    }
                )
                .then(scripts)
                .then(function () {
                    if (jQuery('#b2cOldIE').length) {
                        jQuery('input[type=text], input[type=password], textarea').placeholder();
                    }
                }
                ),
        disclosures: // App disclosures config
        {
            dialogTitle: 'Terms and Conditions',
            url: '../content/agreement.html',
            template: 'I have read and understand the <a href="#" class="b2c-disclosures" id="disclosures">noproblemcash.com terms and conditions</a>.'
        },
        templates: // app customizable templates
        {
            thankYouTemplate: '', // Shown on success.
            btnFinish: 'Get Cash Now!',
            btnBack: { remove: true } // Btn back is removed.

        }
    };

    lmpost.configCoreStarted = true;
    lmpost.options.appOptions = appOptions;

};

lmpost.setupConfig();

function reportErrors(logMessages, callback) {

    if (!lmpost.errCount) {
        lmpost.errCount = 0;
    }

    lmpost.errCount++;
    var errCount = lmpost.errCount;

    if (errCount == 1 && window.ga)
    {
        ga('lmjsfrm.send', 'event', 'form_error');
    }

};

window.onerror = function (msg, url, linenumber) {
    if (!url && window.JSON) {
        msg = JSON.stringify(msg);
    }

    var errs = [];

    errs.push({ source: url + ' ln.' + linenumber, exception: msg });

    reportErrors(errs);

    return document.location.host.indexOf('localhost') < 0; // supress local errors
};


(


function core(instrumented) {

    var coreVer = '3',
		coreVerLo = '6',
		instance = null,
        coreCheck;

    var fletcher = function (a, b, c, d, e) {
        var x;
        for (b = c = d = 0; e = a.charCodeAt(d++) ; c = (c + b) % 255) b = (b + e) % 255; return c << 8 | b
    };

    coreCheck = coreVer + '.' + coreVerLo + '.' + fletcher(core.toString().replace(RegExp("\\t|\\s|\\r|\\n", "g"), ''));

    lmpost.coreCheck = coreCheck;

    lmpost.setCampStatus = function (p) {
        lmpost.cstatus = (p.Result == 1 || p.Result == 3) ? 1 : 0;
        lmpost.returning = (p.Result == 3);
        lmpost.serverDate = p.ServerDate;
        lmpost.RID = p.RID;
        lmpost.lastDate = p.LastDate;

        if (lmpost.authAPI) {
            lmpost.authAPI.syncAuth();
        }

        if (lmpost.RID) {
            lmpost.returning = true;
        }

        if (p.Fields) {
            lmpost.retfields = p.Fields;
        }

        if (p.Conversion) {
            try {
                lmpost.defaultFields = JSON.parse(p.Conversion);
            }
            catch (ex) { };
         
        }
    }

    lmpost.makeUrl = function (baseUrl, requireApiDomain) {
        if (requireApiDomain || !baseUrl.match(/^(http|https):/)) {
            return lmpost.urls.apiUrl + baseUrl;
        }
        else {
            return baseUrl;
        }
    };

    lmpost.processConversion = function (form, data) {
        if (!data || !data.Conversion) {
            return;
        }

        form.append(data.Conversion);
    };

    lmpost.actionUrl = function (action, params) {
        var url = window.lmpost.urls.apiUrl + '/misc/?responsetype=json&action=' + action,
            proto = url.match(/^(http|https):(\/)+/),
            si = proto ? proto[0].length : 0,
            ps = "";

        params = params || {};
        params.uts = new Date().getTime();
        params.uid = lmpost.options.hituid;

        for (var p in params) {
            var v = params[p];
            if (v) ps += '&' + p + "=" + escape(params[p]);
        }

        return (proto ? proto[0] : "") + url.substr(si).replace(/(\/){2,}/g, '/') + ps;
        };

      lmpost.readCookie = function (name) {
            return (name = new RegExp('(?:^|;\\s*)' + ('' + name).replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&') + '=([^;]*)').exec(document.cookie)) && name[1];
      };

      lmpost.writeCookie = function (c_name, value, exminutes) {
        var exdate = new Date();
        exdate.setTime(exdate.getTime() + exminutes * 60000);
        var c_value = escape(value) + ((exminutes == null) ? "" : "; expires=" + exdate.toUTCString());
        document.cookie = c_name + "=" + c_value + "; path=/";
        return exdate;
    }

    lmpost.getOrCreateTestVar = function (name, variants) {
        if (!name || !variants.length) throw 'Incorrect params';
        var cname = 'lmtestvars', varCookieRaw = lmpost.readCookie(cname),
            varCookie = varCookieRaw ? unescape(varCookieRaw) : '',
            varMatch = new RegExp('(^|&)' + name + '=((.)*?)(&|$)').exec(varCookie);

        if (varMatch && varMatch.length >= 3) {
            return varMatch[2];
        }

        var rndIdx = Math.floor(Math.random(new Date().getMilliseconds()) * variants.length), val = variants[rndIdx],
        nvp = name + '=' + val,
        cookieVal = (varCookie) ? varCookie + '&' + nvp : nvp;

        lmpost.writeCookie(cname, cookieVal, 7 * 24 * 60);
        return val;
      };

      lmpost.getTestPostfix = function () {
          var cname = 'lmtestvars', varCookieRaw = lmpost.readCookie(cname),
               varCookie = varCookieRaw ? unescape(varCookieRaw) : '', result = '';

          var pageVars = lmpost.options.testvars;

          if (pageVars) {
              for (var vname in pageVars) {
                  result += '_' + vname + '_' + pageVars[vname]
              }
          };

          if (varCookie) {
              var pairs = varCookie.split('&');

              for (var i = 0; i < pairs.length; i++) {
                  var pair = pairs[i].split('=');
                  if (pair.length == 2) result += '_' + pair[0] + '_' + pair[1];
              }
          }

          return result;
      };

    var checkDelay = 5;
    var checkCount = 0;

    window.setInterval(
        function () {
            checkCount++;
            if (checkCount < 4 && (!window.$ || !$('form.b2cform').length) && (!lmpost.cstatus || lmpost.cstatus == 1 || lmpost.cstatus == 3)) {
                var errMsgs = [],
                 scripts = document.getElementsByTagName('script'),
                 msg;

                var timoutSec = checkCount * checkDelay;

                ga('lmjsfrm.send', 'event', 'timeout', timoutSec);
                }
        },
        checkDelay * 1000);

    function extendJquery() {
        !$ && (function () { $ = jQuery })();
        // Jq custom extensions.
        $.extend({
            parseQuery: function () {
                var nvpair = {};
                var qs = window.location.search.replace('?', '');
                var pairs = qs.split('&');
                $.each(pairs, function (i, v) {
                    var pair = v.split('=');
                    nvpair[pair[0]] = pair[1];
                });
                return nvpair;
            }
        });

        $.fn.makevisible = function () {
            if (!this.length) return;
            var sch = document.compatMode == 'CSS1Compat' && !window.opera ? document.documentElement.clientHeight : document.body.clientHeight;
            var rect = this[0].getBoundingClientRect();
            if (rect.bottom > rect.height && rect.bottom < sch) return;
            if ($(window).scrollTop() > this.first().offset().top) {
                $('html, body').animate({ scrollTop: this.first().offset().top }, 500)
            }
        };
    }

    // Event helper class.
    function EventHelper(handlers, objToExtend) {
        var me = this,
		eventHandlers = handlers;

        me.fireEvent = function (event, paramsArray) {
            var delegate = eventHandlers[event];

            if (!delegate) return;

            for (var idx = 0; idx < delegate.length; idx++) {
                var handler = delegate[idx];
                handler.func.apply(handler.context || window, paramsArray || []);
            } //for
            
            if (paramsArray && paramsArray.length) {
                var e = paramsArray[0];
                !e.cancelled && e.continuation && e.continuation();
            }
        };

        // Initalizes current events from given options block.
        me.loadEventOptions = function (optionsBlock) {
            var delegate, newHandler;

            if (!optionsBlock) {
                return;
            }

            for (var idx in optionsBlock) {
                newHandler = optionsBlock[idx];

                objToExtend.addListener(idx, newHandler, objToExtend);
            }
        };

        // Priveleged methods.
        // Adds new event handler.
        objToExtend.addListener = function (event, handler, context) {
            var delegate = eventHandlers[event];
            delegate && handler && delegate.push({ func: handler, context: context });
        };

        objToExtend.removeListener = function (event, handler) {
            var delegate = eventHandlers[event],
			newDelegate,
			currentHandler;

            if (delegate) {

                for (idx = 0; idx < delegate.length; idx++) {
                    currentHandler = delegate[idx];
                    currentHandler && currentHandler.func != handler && newDelegate.push(currentHandler);
                }

                eventHandlers[event] = newDelegate;
            }
        };
    }

    if (!window.lmpost) {
        if (window.LeadsB2C) {
            window.lmpost = window.LeadsB2C;
            window.lmpost.campaignid = window.LeadsB2C.campaignid;
        }
        else
            window.lmpost = {};
    }

    // Public API

    // Form layout class
    var FormLayout = lmpost.FormLayout = function () {
        var me = this,
        form,
        validationRules,

        errorUpdateState = function (element, errorClass, validClass, errorState) {
            var elem = $(element),
                type = elem.attr('type'),
			    func = errorState ? 'addClass' : 'removeClass',
                valFunc = errorState ? 'removeClass' : 'addClass';

            if (elem.attr('readonly')) return;

            if (type == 'radio' || type == 'checkbox') {
                var wrapper = $(element).closest('div.b2c-radio-wrap');
                wrapper[func](errorClass);
                wrapper[valFunc](validClass);
            } else {
                elem[func](errorClass);
                elem[valFunc](validClass);
            }

            me.validationStateCallback && me.validationStateCallback(element, !errorState);
        },

	    errorHighlight = function (element, errorClass, validClass) {
	        errorUpdateState(element, errorClass, validClass, true);
	    },

	    errorUnHighlight = function (element, errorClass, validClass) {
	        errorUpdateState(element, errorClass, validClass, false);
	    },

        // Show message placeholder - For desktop.
       showPopup = function (title, data) {
            var dlg = $('<div class="pd-content-frame">Loading...</div>').dialog(
                          {
                              buttons: {
                                  "Close": function () {
                                      $(this).dialog("close");
                                      $(this).dialog("destroy").remove();
                                  }
                              },
                              "modal": true,
                              "width": 560,
                              "height": 420,
                              "dialogClass": 'b2c-dialog-text',
                              "title": title
                          });
           var dlgClose = function () {
                var frm = $('.pd-content-frame');
                frm.dialog('close');
                frm.remove();
            };

           data.appendButton = function (title, id, className, handler) {
                var buttons = dlg.dialog('option', 'buttons');
                buttons[title] = handler;
                dlg.dialog('option', 'buttons', buttons);
            };

           data.setFixedDialog = function () {
              $('.ui-widget-overlay').off('click');
            }

            data.CloseDialog = dlgClose;
            //$('.ui-widget-overlay').on('click', dlgClose);
            $('.pd-content-frame').data(data);
            return false;
        },

        // Show message placeholder -  For mobile platforms.
        showMobileDoc = function (title, data) {
            var current = form.find('div.b2c-form:visible');
            current.hide();
            var docForm = '<div class="b2c-form b2c-step b2c-consent">' +
            '<p class="b2c-form-title">' + title + '</p><div class="pd-content-frame">Loading...</div>';

            docForm += '</div>';
            var locForm = $(docForm).insertAfter(current);
            locForm.makevisible();

            var dlgClose = function () {
                var frm = $('.pd-content-frame');
                current.show().makevisible();
                locForm.remove();
                frm.remove();
            };

            data.CloseDialog = dlgClose;

            data.appendButton = function (title, id, className, handler) {
                var ctr = locForm.find('div.b2c-form-action');
                className = !className ? 'b2c-btn' : 'b2c-btn ' + className;
                var btn = $('<div class="b2c-btn-row b2c-form-action"><div class="b2c-btn-wrap"><a id="' + id + '" href="#' + id + '" class="' + className + '">' + title + '</a></div></div>');
                
                if (ctr.length) { btn.insertBefore(ctr); } else { locForm.append(btn); }
                btn.find('a').on('click', handler);
            };

            data.setFixedDialog = function () {
            };

            data.appendButton('Close', 'dlgclose', 'b2c-btn-close-dlg', dlgClose);
            $('.pd-content-frame').data(data);
            return false;
        },

        initValidationRules = function () {
            var methods =
                {
                    "regex": function (value, element, regexp) {
                        return this.optional(element) || regexp.test(value);
                    },
                    // Checkbox group validator.
                    "AtLeastOneCheckboxSelected": function (value, element) {
                        var root = $(element).closest('.b2c-check-group'),
                        allCheckboxes = root.find(':checkbox').get().length;
                        if (!allCheckboxes) {
                            return true;
                        }
                        var val = root.find('input:checked').get().length;
                        return val != 0;
                    }
                };
            // Always override std maxlength method!
            $.validator.addMethod("maxlength", function (value, element, len) {
                return value == "" || value.length <= len;
            });

            for (var valname in methods) {
                if (!$.validator.methods[valname]) {
                    $.validator.addMethod(valname, methods[valname]);
                }
            }
        },

	    initValidators = function () {

	        initValidationRules();

	        // Inputs and checkboxes inside required groups s/b required too.
	        form.find('div.b2c-required :input').addClass('b2c-required');
	        form.find('div.b2c-required :checkbox').each(function () {
	            var name = $(this).attr('name');
	            validationRules[name] = { "AtLeastOneCheckboxSelected": true };
	        }
            );


	        // Validate only agreement chechbox by default.
	        validationRules['accepted'] = { required: true };


	        // Apply required validation.
	        form.find('input[type!="hidden"][type!="checkbox"],select, textarea').each(
				    function () {
				        var elem = $(this),
						    name = elem.attr('name'),
						    rule = validationRules[name];

				        if (!rule) {
				            rule = {};
				            validationRules[name] = rule;
				        }

				        if (elem.hasClass('b2c-required')) {
				            rule.required = true;
				            elem.on('change',
							    function () {
							        if (!$(this).val()) {
							            $(this).valid();
							        }
							    }
							    );
				        }
				    }
			    );

	        form.find('.b2c-phone').each(
				    function () {
				        var elem = $(this);
				        me.getRule(elem.attr('name')).ValidatePhone = true;
				    }
			    );

	        // Init base shared rules.
	        me.getRule('Email').email = true;

	        // Remove spaces from Email
	        $('#Email').on('change', function () {
	            $(this).val($(this).val().replace(/\s+/g, ""));
	        });

	        // Set validator defaults.
	        $.validator.setDefaults({ onkeyup: false });
	        $.validator.messages.required = "";

	        form.validate({
	            invalidHandler: function (form, validator) { },
	            errorClass: "b2c-error",
	            validClass: "b2c-valid",
	            errorPlacement: function (error, element) { },
	            'highlight': errorHighlight,
	            'unhighlight': errorUnHighlight,
	            rules: validationRules

	        });
	    };
 
        lmpost.dialogPage = me.dialogPage = function (targetUrl, title, data) {
         data = data || {};//lmpost.mobile = true;
            (lmpost.mobile ? showMobileDoc : showPopup)(title, data);
        
            $.ajax(
            {
                url: targetUrl.replace('../', lmpost.options.domain),
                crossDomain: true,
                dataType: "script"
            }
            );
        };

        me.getRule = function (name) {
            var rule = validationRules[name];

            if (!rule) {
                rule = {};
                validationRules[name] = rule;
            }

            return rule;
        };

        me.find = function (selector) {
            if (!form) {
                form = $('form.b2cform').first();
            }

            return form.find(selector);
        };

        me.byID = function (id) {
            return me.find('#' + id);
        };

        me.blinkErrorElements = function () {
            var colorArray = new Array("#fff", "#fde3e3", "#FFFFFF", "#fde3e3", "#FFFFFF", "#ffe9e9"),
				chain = $(':visible.b2c-error'),
				delays = [0, 150, 100, 100, 100, 100];

            for (var i = 1; i < 6; i++) {
                chain = chain.animate({ backgroundColor: colorArray[i] }, delays[i]);
            }

            chain = chain.animate({ backgroundColor: colorArray[0] }, 0,
				function () {
				    $(this).removeAttr('style');
				}
			);

            form.find('.b2c-error:first').parent().makevisible();
        };

        me.validateSubform = function (subform) {
            var valid = true;
            subform.find('input[type!="hidden"]:not("[disabled]"),select:not("[disabled]"),textarea:not("[disabled]")').each(
						function () {
						    var elem = $(this);
						 
						    valid = (valid & (!elem.rules || $.isEmptyObject(elem.rules()) || elem.valid()));
						}
					);

            return valid;
        };

        me.Init = function (elem, rules) {
            form = elem;
            form.data('FormUI', me);
            form.show();


            // Init required selectors.
            var selectors = form.find('select.b2c-required option:first-child[value!=""]');
            selectors.removeAttr('selected');
            selectors.before('<option value="" selected>- Select -</option>');
            form.find('select.b2c-required option:first-child').attr('selected', 'selected');
            form.find('select.b2c-required').on('change',
				    function () {
				        $(this).valid();
				    }
			    );

            // Init PIE - CSS3 support for oldier IEs.
            if (window.PIE) {
                form.find('.b2c-btn, .b2c-progress-bar-line').each(function () {
                    PIE.attach(this);
                });
            }

            // Init custom parameters.
            if (lmpost.options) {
                $.each(['campaignid', 'campaignkey', 'leadtypeid', 'TestResult', 'declinedURL', 'acceptedURL'],
				    function (intIndex, objValue) {
				        lmpost.options[objValue] && $('<input>').attr({
				            type: 'hidden',
				            id: objValue,
				            name: objValue,
				            value: lmpost.options[objValue]
				        }).appendTo(form);
				    });
            }

            form.find('input.b2c-required[type=text], select.b2c-required, div.b2c-required, textarea.b2c-required').after('<span class="b2c-req-marker">*</span>');


            // Init validation and behavior.
            validationRules = rules;
            initValidators();

            // Init datepickers.
            form.find('input.b2c-date').each(
			            function () {
			                var dp = $(this),
                                name = dp.length && dp.attr('name'),
			                    fmt = dp.attr('formatdate') || lmpost.options.appOptions.formatdate || 'mm\/dd\/yy';
                         
			                dp.length && dp.datepicker({
			                    dateFormat: fmt,
			                    yearRange: '-100:+1',
			                    minDate: dp.attr('minDate'),
			                    maxDate: dp.attr('maxDate'),
                                defaultDate: -6935,
			                    changeMonth: true,
			                    changeYear: true,
			                    onSelect:
						            function () {
						                $(this).valid();
						            }
			                }
					            );
			            }
		    );

        }

        me.getTemplate = function (tplName, defValue) {
            var
				appOptions = lmpost.options && lmpost.options.appOptions,
				tplData;

            if (!appOptions) {
                return null;
            }

            if (!appOptions.templates) {
                return null;
            }

            tplData = appOptions.templates[tplName];

            if (tplData && tplData.remove) {
                return null;
            }

            return tplData || defValue;
        };
    };

    var FormUI = function () {
        var me = this,
		validationRules,
		form,
        layout,
		validationTimeout = 2000,
		prefix = '', //'b2c' + (new Date()).getTime() + '_', - disabled functionality
		urls,
		events = new EventHelper(
			{
			    acquireValidationState: [],
			    formReady: [],
			    formBeforeSubmit: [],
			    formStepChanging: [],
			    formSubmitError: []
			}, me),
		logMessages = [],


        // Private methods
		showStatus = function (title, message, closeButton) {
		    var id = prefix + 'msgDiv',
				elem = form.find('#' + id),
				msgHtml,
				lastForm = form.find('.b2c-form:not(.b2c-msg):last');

		    if (!elem.length) {
		        msgHtml = '<div class=\"b2c-form b2c-msg\" id=\"' + id + '\" style=\"display:none\">';
		        msgHtml += '<p class=\"b2c-form-title\"><\/p>';
		        msgHtml += '<div class=\"b2c-row b2c-form-thank\">';
		        msgHtml += '<p><\/p>';
		        msgHtml += '<\/div>';
		        msgHtml += '<div class=\"b2c-row b2c-form-action\"><div class=\"b2c-btn-wrap\"><a  class=\"b2c-btn\">Retry<\/a></div><\/div>';
		        lastForm.after(msgHtml);
		        elem = lastForm.next();


		        elem.find('.b2c-btn').on('click',
					function () {
					    submitFormStd();
					    return false;
					}
				);
		    }

		    elem.find('p.b2c-form-title').text(title).makevisible();
		    elem.find('div.b2c-form-thank > p').html(message);
		    var action = closeButton ? 'show' : 'hide';
		    elem.find('.b2c-form-action')[action]();

		    lastForm.hide();
		    elem.show();
		},




	submitForm = function (success, error) {
	    var customSuccess = lmpost.options.onSuccess,
				customError = lmpost.options.onError,
				beforeSendData = lmpost.options.onBeforeSend,
                options,
                lmopts = lmpost.options,
                hituidStr = lmpost.options.hituid ? '&uid=' + escape(lmopts.hituid) : '',
				startHandler = beforeSendData ? beforeSendData : function () { return {} }, // use dummy default start handler
				successHandler = function (data) {
				    if (lmopts.appOptions.noRedirect && data.Result == 0) // If still processing, just repeat the query until accepted or rejected.
                     { 
                            var lid = data.LeadID;

				        options.url = lmpost.makeUrl(urls.submitUrl, true) + '?LeadID=' + lid + '&c=' + lmopts.campaignid + hituidStr + '&responsetype=json&ResponseAsync=1';
                            
				        setTimeout(function () {
                                 $.ajax(options);
                            }, 200);
                        }
				    else {
				        customSuccess ? customSuccess(data, success) : success(data);
                        }
                },
				errorHandler = customError ? function (result) { customError(result, error); } : error,
				
				query = form.find(':not(.b2c-dont-send)').serialize().replace(new RegExp(prefix, "g"), '');


				options = {
				    // warning: api url must be always used for submission.
				    url: lmpost.makeUrl(urls.submitUrl, true) + '?' + query + hituidStr + '&ClientUid=' + lmpost.clientId + '&responsetype=json&ResponseAsync=1&clienturl=' + escape(location.href),
				    success: successHandler,
				    error: errorHandler,
				    timeout: 300000,
				    type: 'get',
				    dataType: 'jsonp'
				};
	    var
			appOptions = lmpost.options.appOptions,
			submitTemplate = layout.getTemplate("processingTemplate", 'The form is processed...');

	    var e = {
	        continuation: function (evtArgs) {
	            showStatus(' ', submitTemplate, false);
	            startHandler();
	            $.ajax(options);
	        }
	    };
	    events.fireEvent('formBeforeSubmit', [e]);

	},

        submitFormStd = function () {
            var
				appOptions = lmpost.options.appOptions;

            submitForm(
								function (data) {
                                    
								    if (data && data.Result != 4) {

								        lmpost.collect("submit", "", "form");
                                      
								        processConversion(data);

								        if (data.RedirectURL && !lmpost.options.appOptions.noRedirect) {
								            var redirUrl = data.RedirectURL, va = window._va;
								            regFormEvent('form', 10, redirUrl);
								            if (va && va.createThirdPartyParameter) {
								                var vaKey = va.createThirdPartyParameter(), sym = redirUrl.indexOf("?") ? "&" : "?";
								                redirUrl += sym + vaKey;
								            }
								            window.setTimeout(function () { window.location = redirUrl; }, 250);
								        }
								        else {
                                            regFormEvent('form', 10, '');
								            var thankYouTemplate = layout.getTemplate("thankYouTemplate", 'Your application has been submitted.');
								            showStatus('Thank you!', thankYouTemplate);
								        }
								    }
								    else {
                                        regFormEvent('form', 11, 0);
								        errorCallback(data);
								    }

                                    return 0;
								}
								, errorCallback);
        },


		setError = function (field) {
		    var target = me.byID(field);
            if (!target.length) {
		        target = form.find('[name$=_' + field + ']');
		        if (!target.length) {
		            return false; // no target element.
		        }
            }

		    // For hidden inputs try to find visible elements.
		   // if (target.length)// && target.attr('type') == 'hidden') {
		    var displayFieldRef = target.attr('b2c-display-field'),
                dispFieldSet = displayFieldRef && target.closest('div').find(displayFieldRef),
				displayField = (dispFieldSet && dispFieldSet.length) ? dispFieldSet : me.byID(displayFieldRef);

		    if (displayField && displayField.length) {
		        target = displayField; // display field s/b highlighted instead of hidden data field.
		    }
			
            if (!target.length) {
		         return false; // no target element.
		    }

            var type = target.attr('type');
		    if (type == 'radio' || type == 'checkbox') {
		        target = target.closest('div.b2c-radio-wrap');
		    }

		    target.addClass('b2c-error').removeClass('b2c-valid');

		    target.each(function () {
                     acquireValidationState(this, false);
                }
            );
		    
		    return true;
		},

        // Adds conversion element the DOM if returned.
		processConversion = function (data) {
		    lmpost.processConversion(form, data);
		},

        // Std error callback.
	errorCallback = function (result) {
	    showStatus('Sorry', 'Please try again.', true);
	    result && processConversion(result);

	    events.fireEvent('formSubmitError');

	    if (result && result.Errors && result.Errors.length) {
	        $.each(result.Errors, function (index, value) {
	            value.Field && setError(value.Field);
	        }
				);

	        // Find first step with error and show it. Otherwise only sorry message is shown.
	        var invalidStep = form.find('.b2c-error:first').closest('.b2c-form');
	        if (invalidStep.length) {
	            form.find('.b2c-form:visible').hide();
	            invalidStep.show();
	            me.blinkErrorElements();
	        }
	    }
	},

    regFormEvent = function (s, evt, value) {
        var evts = {
            0: 'register',
            1: 'continue',
            10: 'step',
            11: 'submit_error',
            100: 'focus',
            101: 'blur',
            102: 'validation_error',
            103: 'validation_ok'
        };
        $.ajax({ url: lmpost.actionUrl('regevent', { sender: s, event: evt, value: value }), type: 'get', dataType: 'jsonp' });
        if (!ga || evt == 1000) return;
        if (evt == 0 ) s = value;
        ga('lmjsfrm.send', 'event', s, evts[evt]);
        switch (evt) {
            case 100:
                lmpost.collect('interaction', 'focus', s);
                break;
            case 101:
                lmpost.collect('display', '', 'form');
                break;
            case 1:
                if (value === '1') {
                    lmpost.collect('transition', s, 'form');
                    lmpost.collect('display', '', 'form');
                }
                break;
        }
    },

		goNext = function () {
		    delete me.isSubmitting;
		    if (!form.valid()) return;

		    var e = {
		        continuation: function (data) {

		            if (data && data.Result != 1) {
                        errorCallback(data);
                        return;
                    }

		            var current, next;
		            form.find('.b2c-form').each(
                        function () {
                            var frm = $(this);

                            if (current) { // if current is already set it's next form.
                                if (!next) {
                                    next = frm;
                                    next.data('PrevForm', current);
                                }

                                return;
                            }

                            if (frm[0].style['display'] != 'none') {
                                current = frm;
                            }
                        }
                    );

		            current.hide();
		            if (next && next.length && !next.hasClass('b2c-msg')) // has other forms to fill
		            {
                        regFormEvent(next.attr('class'), 2);
		                next.show();
		                if (!next.find('.b2c-error').length) {
		                    $('.b2c-form:visible').makevisible();
		                }
		                // Whether the form has invalid elements from external source, highlight them.
		                me.blinkErrorElements();
		            }
		            else {
		                // There are no forms left... begin submission.
		                submitFormStd();
		            }
		        }
		    };
		    events.fireEvent('formStepChanging', [e]);
            

		    //if (e.cancelled) { return; };

		    //e.continuation();
		},

		onContinueButtonClick = function (e) {
		    var sender = (e && $(e.target).is('a')) ? $(e.target) : form.find('.b2c-form:visible a.b2c-btn:not(.b2c-btn-back)'),
					subform = sender.closest('.b2c-form'),
					valid = true,
					nextSubform = subform.next(':not(.b2c-msg)'),
					prevSubform;

		    // Process back button click.
		    if (sender.hasClass('b2c-btn-back')) {
		        delete me.isSubmitting; // cancel submit flag
		        form.find('span.b2c-btn-loader').hide();
		        prevSubform = subform.data('PrevForm');
		        subform.hide();
		        prevSubform.show();
		        return;
		    }

		    if (me.isSubmitting) {
		        return;
		    }

		    // Next button click.
		    valid = layout.validateSubform(subform);
            var senderTag = subform.attr('class');

		    if (!valid) {
		        var validator = form.validate();
		        if (validator.pendingRequest) {
		            me.isSubmitting = true;
		            var loader = form.find('span.b2c-btn-loader');
		            // isSubmitting flag MUST be removed in any scenario after a while.
		            window.setTimeout(
							function () {
							    if (me.isSubmitting) {
							        delete me.isSubmitting;
							        loader.hide();

							        goNext();
							    }
							}, validationTimeout + 1000);
		            loader.show();
		        }
		        me.blinkErrorElements();
                regFormEvent(senderTag, 1, '0');
		    }
		    else {
                regFormEvent(senderTag, 1, '1');
		        goNext();
		    }
		    return false;
		},

		initCustomSettings = function () {
		    var config = window.LMEmbeddedFormConfig,
						formSelector = form.find('.b2c-form'),
						cfgButton;

		    if (config) {
		        config.formWidth && formSelector.css('width', config.formWidth);
		        config.backgroundColor && formSelector.css('background', config.backgroundColor);
		        cfgButton = config.continueButton;
		        if (cfgButton) {
		            cfgButton.backgroundColor && $('.b2c-btn').css('background', cfgButton.backgroundColor);
		        }
		        if (config.fontSize) {
		            form.css('font-size', config.fontSize);
		        }
		    }
		},

	acquireValidationState = function (element, isValid) {
	    events.fireEvent('acquireValidationState', [element, isValid]);
      
            regFormEvent($(element).attr('name'), (!isValid) ? 102 : 103, $(element).val());
	};

        lmpost.regFormEvent = regFormEvent;

        me.getUrl = function (key) {
            return urls[key];
        };
        //

        me.find = function (selector) {
            return layout.find(selector);
        };

        me.byID = function (id) {
            return layout.byID(id);
        };

        me.getTemplate = function (tplName, defValue) {
            return layout.getTemplate(tplName, defValue);
        }

        me.InitForm = function (rules) {
            // Init required urls from config.
            urls = lmpost.urls;


            form = $('form.b2cform').first();
            layout = new FormLayout();
            layout.Init(form, rules);
            layout.validationStateCallback = acquireValidationState;
            $('div.b2cloader').hide();

            var fldar = [];
            form.find('[name]').each(function (ind, el) { if (el.name) { fldar[el.name.toLowerCase()] = el.name; } });
            window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
                form.find('input[name=' + fldar[key.toLowerCase()] + ']').val(unescape(value));
                form.find('select[name=' + fldar[key.toLowerCase()] + ']').val(unescape(value));
            });


            // Attach continue buttons.
            form.find('.b2c-form').not(':last').append('<div class=\"b2c-row b2c-form-action\"><span style=\"display:none\" class=\"b2c-btn-loader\"><\/span><div class=\"b2c-btn-wrap\"><a class=\"b2c-btn\" tabindex=\"0\">Continue<\/a><\/div><\/div>');
            form.find('.b2c-form:last').append('<div class=\"b2c-row b2c-form-action\"><span style=\"display:none\" class=\"b2c-btn-loader\"><\/span><div class=\"b2c-btn-wrap\"><a  class=\"b2c-btn\" tabindex=\"0\">' +
			layout.getTemplate('btnFinish', 'Submit') +
			 '<\/a><\/div><\/div>');

            var backBtn = layout.getTemplate('btnBack');
            if (backBtn) {
                form.find('.b2c-form div.b2c-form-action').not(':first').prepend('<div class=\"b2c-btn-wrap\"><a  class=\"b2c-btn b2c-btn-back\">' + backBtn + '<\/a><\/div>');
            }

            // Form init
            form.find('.b2c-btn').on('click', onContinueButtonClick);
            $(document).on('keydown',
			function (e) {
			    if (e.keyCode == 13) {
			        onContinueButtonClick(e);
			        e.stopPropagation();
			        return false;
			    }
			}
		);


            initCustomSettings();

            form.find('input,select,textarea')
                .on('focus', function () { regFormEvent($(this).attr('name'), 100) })
                .on('blur', function () { regFormEvent($(this).attr('name'), 101) });

            events.fireEvent('formReady');

        };

        me.blinkErrorElements = function () {
            layout.blinkErrorElements();
        };

    };

    window.lmpost.getFormUI = function () {
        if (!instance) {
            instance = new FormUI();
        }

        return instance;
    };

    window.lmpost.cleanupFormUI = function () {
        instance = null;
    };

    window.lmpost.newFormUI = function () {
        return new FormUI();
    };

    // Module initialization.
    (function () {

        function loadStyle(url) {
            var script = document.createElement('link');
            script.setAttribute('type', 'text/css');
            script.setAttribute('rel', 'stylesheet');
            url = url.replace('../', lmpost.options.domain);
            script.setAttribute('href', url);
            document.getElementsByTagName("head")[0].appendChild(script);
        }

        lmpost.loadStyle = loadStyle;

        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-45594311-2', 'auto', { 'name': 'lmjsfrm' });
        var form_location = window.location.hostname +
            window.location.pathname +
            window.location.search;
        ga('lmjsfrm.send', 'pageview', form_location);
        ga('lmjsfrm.send', 'event', 'init', lmpost.options.leadtypeid);

        lmpost.collect('loading', 'load-main', 'form');

        function checkScript(scriptAliases, condition) {
            var url, referenced;

            if (condition && condition()) {
                return true;
            }

            // Search for defined script.
            var scripts = document.getElementsByTagName('script');

            for (var i = 0; i < scripts.length; i++) {
                var srcElem = scripts[i],
                        src = srcElem.src;

                for (var j = 0; j < scriptAliases.length; j++) {
                    if (src && (endsWith(src, scriptAliases[j]))) {
                        referenced = true;
                        break;
                    }
                }

                if (referenced) return true;
            }

            return false;
        };

        lmpost.checkScript = checkScript;

        function defaultInit() {
            if (lmpost.customInit) {
                lmpost.customInit();
            }
            else {
                instance.InitForm({});
            }
        }


        var requiredStyles = 
        {
            main: '../content/themes/general/b2c-css-core.min.css'
        };

        var mobile = (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()));
        lmpost.mobile = mobile;
        if (mobile) {
            requiredStyles.mobile = '../content/themes/general/b2c-styles-mobile.css';
        }
        if (navigator.appVersion.indexOf("MSIE 7.") != -1) {
            requiredStyles.msie = '../content/themes/general/b2c-styles-ie7.css';   
        }

        var preScripts = [];
        if (!checkScript(['jquery.js', 'jquery.min.js'], function () { return window.jQuery; })) {
            preScripts.push('https://ajax.aspnetcdn.com/ajax/jquery/jquery-1.7.min.js');
        }

        if(preScripts.length) lmpost.loadScripts(preScripts);


        var requiredScripts = ["../scripts/plugins/plugins-bundle.min.js"];
        var jq = []; // required jquery script
        var jqReferenced = false,
            startTime = new Date();

        function endsWith(str, suffix) {
            return str.indexOf(suffix, str.length - suffix.length) !== -1;
        }

        function execHandlers(key) {
            var handlers = lmpost[key];
            if (handlers && handlers.length) {
                for (var i in handlers) {
                    handlers[i]();
                }
            }
        }

        lmpost.boot = { styles: requiredStyles };

            var objLoader = lmpost.loadScripts(),
                loadScripts = lmpost.loadScripts;

            var injectForm = function() {
                var src = document.getElementById('leadsb2cformsrc');
                var div = document.createElement('DIV');
                div.innerHTML = window.lmpost.formData;
                src.parentNode.insertBefore(div.children[0], src.nextSibling);
            };
         
        objLoader
           .chain(function() {
               if (!lmpost.configCoreStarted) {
                   return loadScripts().then(["../scripts/config.core.js"]);
               }
            })
		//.then(["../scripts/config.core.js"])
        .then(function() { lmpost.setupConfig(); })
		.chain(
			function () {
                return lmpost.options && lmpost.options.runLocalConfig && loadScripts().then([lmpost.options.localConfigName || 'site.config.js']);
			}
		)
        .then(function () {
            if (!checkScript(['hit.core.js'])) {
                loadScripts([window.lmpost.urls.hitUrl]);
            }
          }
        )
		.chain(
			function () {
			    lmpost.collect('loading', 'load-layout', 'form');
			    if (!lmpost.createForm) return lmpost.loadScripts().then([lmpost.options.form + lmpost.options.appOptions.formStartupFile]);
			}
		)
        .then(function () { lmpost.createForm && lmpost.createForm(); })
        .then(function () {
                injectForm();
                lmpost.collect('html', '', 'form');
            })
        .then(function () // Load required css
        {
            lmpost.collect('loading', 'load-css', 'form');
            if (lmpost.options.theme) {
                requiredStyles.theme = '../content/themes/general/b2c-styles-' + lmpost.options.theme + '.css';
            }

            var styles = lmpost.boot.styles,
                seq = ['jqui', 'main', 'theme'];
            for (var i = 0; i < seq.length; i++) {
                var s = seq[i];
                if (styles[s]) {
                    var path = styles[s];
                     loadStyle(path + '?v=' + coreCheck);
                     styles[s] = null;
                }
            }

            for (var idx in lmpost.boot.styles) {
                var path = lmpost.boot.styles[idx];
                path && loadStyle(path + '?v=' + coreCheck);
            }
        })
	.chain(
		function () {
			lmpost.collect('loading', 'load-prejq', 'form');

			return lmpost.options.appOptions && lmpost.options.appOptions.preJQScripts;
		}
	)
    .then(function() { lmpost.pdcore(); })
    .then(function () { lmpost.collect('loading', 'load-jq1', 'form');})
    .waitFor(function () { return window.jQuery; })
    .then(function () { execHandlers('onConfigLoaded'); })
    .then(function () { lmpost.collect('loading', 'load-jq-ext', 'form'); })
    .then(extendJquery)
    .then(function () {
        var ieChecker = $('<!--[if IE]><span id="b2cIE" style="display:none"></span><![endif]--><!--[if lt IE 10]><span id="b2cOldIE" style="display:none"></span><![endif]-->').appendTo('body');
        if ($('#b2cOldIE').length)
        { requiredScripts.push('../Scripts/plugins/PIE.js'); };
    }
    )
    .then(function () { lmpost.collect('loading', 'load-req', 'form'); })
    .then(requiredScripts)
    .then(function () { lmpost.collect('loading', 'load-hit', 'form'); })
    .waitFor( function () { return window.hitcorejsalreadyfired == 1 && lmpost.options.hituid })
    .chain(
        function () {
            var queryParams = $.parseQuery(),
                rawEmail = queryParams['Email'] || queryParams['email'],
                returning = queryParams['returning'],
                msgsid = queryParams['msgsid'] || '',
                msguid = queryParams['msguid'] || '',
                sesuid = queryParams['sesuid'] || '',
            email = rawEmail ? unescape(rawEmail) : '';

            lmpost.email = email;
            lmpost.collect('loading', 'load-checkstatus', 'form');

            return loadScripts()
                .then(
                 [lmpost.actionUrl('checkstatus',
                                  {
                                      c: lmpost.options.campaignid, email: email,
                                      leadtypeid: lmpost.options.leadtypeid,
                                      callback: 'lmpost.setCampStatus',
                                      mailsrc: 'query',
                                      sesuid: sesuid,
                                      msgsid: msgsid,
                                      msguid: msguid
                                  })])
                .then(function () { // Validate if campaign is corrrect and we have returning user here.


                    if (lmpost.cstatus != 1) { lmpost.formState = 'denied'; /* Denied, block loading. */ };
                    if (returning) lmpost.returning = true;
                    if (lmpost.returning && lmpost.handleReturningUser && !lmpost.options.ignoreReturning) {
                        lmpost.collect('loading', 'returning', 'form');
                        ga('lmjsfrm.send', 'event', 'display_returning', lmpost.options.leadtypeid);
                        lmpost.handleReturningUser(sesuid);
                    }
                    else {
                        lmpost.formState = 'active'; // New user, load main form asap.
                    }
                }
                )
                .waitFor(function () {
                    return lmpost.formState == 'active'; // Wait while returning user sequence is processed.
                }
                );
        }
    )
	.then(function () {

	    if (lmpost.cstatus == 1) {

            if ($('form.b2cform').length == 0) {
                injectForm();
            }

	        var tag = lmpost.options.form.replace('../', ''),
                pfx = lmpost.getTestPostfix(),
                delay = new Date() - startTime;

	        if (pfx) tag += pfx;
	       
	        $.ajax(
           {
               url: lmpost.actionUrl('regform', { tag: tag, host: location.hostname, tagval: delay }),
               type: 'get', dataType: 'jsonp'
           });
	    }
	    else {
	        lmpost.collect('blocked', '', 'form');
	        $('.b2cform').remove();
	        $('div.b2cloader').hide(); $('#leadsb2cformsrc').after('<div>The campaign is pending or blocked.</div>');
	    }
	})
    .then(function () { $('div.b2cloader').hide(); $('form.b2cform:first').show();
                if (!lmpost.ready) {
                    lmpost.collect('ready', '', 'form');
                    lmpost.ready = true;
                }
                ga('lmjsfrm.send', 'event', 'display', lmpost.options.leadtypeid);} )
	.chain(
		function () {
		    return lmpost.options.appOptions && lmpost.options.appOptions.postJQScripts;
		}
	)
        // Load specific form after initialization.
    .then(
		defaultInit
		);
        objLoader.run();
    }()
);

})();

lmpost.setupReturning = function() {

    // Cleanup form
    if ($('form.b2cform').length > 1) {
        $('form.b2cform').last().remove();
    }

    // Init YOB
    var form = $('form.b2cform').first();
    var yobSelect = form.find('#YOB');
    var curYear = new Date().getFullYear();


    for (var yr = curYear - 18; yr > curYear - 100; yr--) {
        yobSelect.append('<option value=' + yr + ' >' + yr + '</option>');
    }

    var queryParams = $.parseQuery(),
        zipCode = unescape(queryParams['ZipCode'] || queryParams['zipcode']);


    if (zipCode && zipCode != 'undefined') {
        form.find('#ZipCode').val(zipCode);
    }


    var ui = new lmpost.FormLayout();
    var rid;
    var amount, income, freq, paydate;


    var rules =
    {
        SSN: { required: true, digits: true, minlength: 4, maxlength: 4 }
    };

    ui.Init(form, rules);


    // Add hidden hit identifier field.

    form.find('[type=hidden]').last().after('<input type="hidden" id="uid" name="uid" value="' + lmpost.options.hituid + '"/>');

    lmpost.fillRequestedAmount(ui.byID('RequestedAmount'));
    lmpost.fillMonthlyIncome(ui.byID('MonthlyIncome'));
    lmpost.configurePaydate(ui.byID('PayDate1'));

    if (!lmpost.ready) {
        lmpost.collect('ready', 'returning', 'form');
        lmpost.ready = true;
    }

    var firstimeflag = true;
    var goStep1Error = function(data) {

        var step1Form = ui.find('.b2c-step1-returned');
        if (data.Result == 4) {
            if (data.Errors && data.Errors.length) {
                $.each(data.Errors, function(index, value) {
                        value.Field && step1Form.find('[name=' + value.Field + ']').addClass('b2c-error');
                    }
                );
            }
        }

        if (data.Result == 2) {
            if (firstimeflag) {
                firstimeflag = false;
                step1Form.find('#YOB,#ZipCode,#SSN1').addClass('b2c-error');
            } else {
                skipLogin();
                return;
            }
        }

        form.find('.b2c-step-load').hide();
        step1Form.show();
        ui.blinkErrorElements();
    };

    var isInt = function(value) {
        return !isNaN(parseInt(value, 10)) && (parseFloat(value, 10) == parseInt(value, 10));
    }

    var setSelectValue = function(jqRoot, key, fieldValue) {
        var options = jqRoot.find('[name=' + key + '] option'),
            len = options.length;
        $.each(options, function(index, option) {
            var rawVal = $(option).attr('value');
            var optionVal = isInt(rawVal) ? parseInt(rawVal) : rawVal;
            if (fieldValue <= optionVal || (fieldValue > optionVal && index == (len - 1))) {
                $(option).attr('selected', true);
                return false;
            }
        });
    };

    var setIncomeValue = function(jqRoot, fieldValue) {
        setSelectValue(jqRoot, 'MonthlyIncome', fieldValue);
    };

    var showStep2 = function(data) {
        if (!lmpost.retfields) {
            lmpost.retfields = data.fields;
        }
        var step2Form = form.find('.b2c-step2-returned');
        if (data.Result == 1) {
            var retDate = step2Form.find('.b2c-submit-date');

            if (retDate.find('.b2c-day').length) {
                var monthShort = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                    parts = data.LastDate.split('/');
                retDate.find('.b2c-month').text(monthShort[parts[0] - 1]);
                retDate.find('.b2c-day').text(parts[1]);
                retDate.find('.b2c-year').text(parts[2]);
            } else {
                retDate.html(data.LastDate);
            }

            if (data && data.Fields) {
                $.each(data.Fields, function(index, value) {
                    switch (value.FieldName) {
                    case 'FirstName':
                        step2Form.find('#FirstName').html(value.FieldValue + "!");
                        break;
                    case 'MonthlyIncome':
                        setIncomeValue(step2Form, value.FieldValue);
                        break;
                    default:
                        step2Form.find('[name=' + value.FieldName + ']').val(value.FieldValue);
                    }
                });
            }

            rid = data.RID;
            form.append('<input type="hidden" id="RID" name="RID" value="' + data.RID + '"/>');
            form.find('.b2c-step-load').hide();
            form.find('.b2c-step1-returned').hide();
            step2Form.show();
        } else {
            goStep1Error(data);
            return;
        }
    };

    var goFormUpdate = function(data) {
        //
        lmpost.options.appOptions.postJQScripts = lmpost.options.appOptions.postJQScripts.then(
            function() {
                lmpost.getFormUI().addListener('formReady',
                    function() {
                        prepareForm(data);
                    });
            });
        skipLogin();
    };

    $('a.b2c-btn-verify').on('click',
        function() {
            var firstForm = ui.find('.b2c-step1-returned');
            if (ui.validateSubform(firstForm)) {
                firstForm.hide();
                $('.b2c-step-load').show();
                $.ajax(
                {
                    url: window.lmpost.urls.apiUrl + 'misc/?action=leadreturn&responsetype=json&'
                        + 'email=' + escape(lmpost.email)
                        + '&' + form.find('div.b2c-step1-returned input,div.b2c-step1-returned select,[type=hidden]').serialize(),
                    timeout: 15000,
                    dataType: 'jsonp',
                    success: showStep2,
                    error: goStep1Error
                });
            } else {
                ui.blinkErrorElements();
            }
        }
    );

    var setOrigVal = function(elem, val) {
        elem.val(val);
        elem.data('orig-value', val);
        elem.addClass('b2c-ignore');
        elem.addClass('b2c-dont-send');
    };

    var prepareForm = function(data) {
        if (data.Result != 1) {
            return;
        };

        var mainForm = lmpost.getFormUI(),
            ridLoc = rid || lmpost.RID;

        mainForm.find('[type=hidden]').last().after('<input type="hidden" id="RID" name="RID" value="' + ridLoc + '"/>');

        $.each(data.Fields, function(index, value) {
                switch (value.FieldName) {
                case 'RequestedAmount':
                    if (amount) {
                        mainForm.find('#RequestedAmount').val(amount);
                    }
                    break;
                case 'SSN':
                    var ssnVal = '***-**-' + value.FieldValue.substring(5);
                    mainForm.find('#SSN1').val(ssnVal);
                    break;
                case 'PayFrequency':
                    var freqVal = value.FieldValue;
                    if (freq) {
                        freqVal = freq;
                    }
                    mainForm.find('#PayFrequency').val(freqVal);
                    break;
                case 'PayDate1':
                    var pdVal = value.FieldValue;
                    if (paydate) {
                        pdVal = paydate;
                    }
                    mainForm.find('#PayDate1').val(paydate);
                    break;
                case 'Address1':
                    mainForm.find('#Address1').val(value.FieldValue);
                    break;
                case 'IncomeType':
                    var val = (value.FieldValue == 'EMPLOYMENT') ? 'Employment' : 'Benefits';
                    mainForm.find('#IncomeType').val(val);
                    break;
                case 'MonthlyIncome':

                    var incomeValue = value.FieldValue;
                    if (income) {
                        incomeValue = income;
                    }
                    setIncomeValue(mainForm.find('#' + value.FieldName).parent(), incomeValue);
                    break;
                case 'BankAccountNumber':
                    setOrigVal(mainForm.find('#BankAccountNumber'), value.FieldValue);
                    break;
                case 'DriversLicense':
                    setOrigVal(mainForm.find('#DriversLicense'), value.FieldValue);
                    break;
                default:

                    var control = mainForm.find('#' + value.FieldName);
                    if (control.length) {

                        var prevVal = value.FieldValue;
                        if (prevVal.toLowerCase() == 'true' || prevVal.toLowerCase() == 'false') {
                            prevVal = (prevVal.toLowerCase() == 'true') ? '1' : '0';
                        }

                        if (control.is('select')) {
                            setSelectValue(mainForm, value.FieldName, prevVal);
                        } else {

                            control.val(prevVal);
                            control.change && control.change();
                        }
                    }
                }
            }
        );

        $('#LastName,#FirstName,#SSN1,#Email,#DOB').attr('disabled', 'disabled');

        var wrapValidator = function(validator) {
            return function(value, element, p1, p2) {
                var isAutoFilled = $(element).hasClass('b2c-ignore');

                if (isAutoFilled) {
                    var origData = $(element).data('orig-value');
                    if (value == origData) {
                        return true;
                    } else {
                        $(element).removeClass('b2c-ignore');
                        $(element).removeClass('b2c-dont-send');
                    }
                }

                return validator.apply(this, [value, element, p1, p2]);
            };
        };

        for (var key in $.validator.methods) {
            var original = $.validator.methods[key];
            var wrapperData = { orig: original };
            $.validator.methods[key] = null;
            $.validator.addMethod(key, wrapValidator(original));
        }

        var origAddMethod = $.validator.addMethod,
            handler = wrapValidator(original);
        $.validator.addMethod = function(key, method) {
            origAddMethod.apply(this, [key, handler]);
        };

        $('.b2c-required:not([disabled])').valid();
    };

    //validateSubform 
    var skipLogin = function() {
        $('form.b2cform').hide();
        $('div.b2cloader').show();

        $('form.b2cform').remove();
        lmpost.formState = 'active';
    };

    var applicationError = function(result) {
        var step2Form = ui.find('.b2c-step2-returned');
        ui.find('.b2c-step-searching').hide();
        step2Form.show();

        result && lmpost.processConversion($('form.b2cform'), result);

        if (result && result.Errors && result.Errors.length) {
            $.each(result.Errors, function(index, value) {
                    value.Field && step2Form.find('[name=' + value.Field + ']').addClass('b2c-error');
                }
            );
        }

        ui.blinkErrorElements();
    };

    var applicationSuccess = function(data) {
        if (data && data.Result != 4) {
            lmpost.processConversion($('form.b2cform'), data);

            if (data.RedirectURL && !lmpost.options.appOptions.noRedirect) {
                window.location = data.RedirectURL;
            }
        } else {
            applicationError(data);
        }
    };

    var transferFormUpdate = function() {
        // Request leadreturnedit details.
        var secondForm = ui.find('.b2c-step2-returned');

        // Save field values.
        amount = secondForm.find('#RequestedAmount').val();
        income = secondForm.find('#MonthlyIncome').val();
        freq = secondForm.find('#PayFrequency').val();
        paydate = secondForm.find('#PayDate1').val();

        if (lmpost.RID && !form.find('#RID').length) {
            form.append('<input type="hidden" id="RID" name="RID" value="' + lmpost.RID + '"/>');
        }


        secondForm.hide();
        form.find('.b2c-step-load').show();
        $.ajax(
        {
            url: window.lmpost.urls.apiUrl + 'misc/?action=leadreturnedit&responsetype=json&' + form.find('[type=hidden]').serialize(),
            timeout: 15000,
            dataType: 'jsonp',
            success: goFormUpdate,
            error: goStep1Error
        });
    };

    if (!lmpost.RID) {
        ui.find('.b2c-step1-returned').show();
    } else {
        var dta = { Result: 1, RID: lmpost.RID, LastDate: lmpost.lastDate, Fields: lmpost.retfields },
            updateMode = (unescape(queryParams['updateapp']) == 'true');
        if (updateMode) {
            transferFormUpdate();
            return;
        }

        showStep2(dta);
    }

    $('a.b2c-btn-submit').on('click',
        function() {
            var secondForm = ui.find('.b2c-step2-returned');
            var searchingForm = ui.find('.b2c-step-searching');
            if (ui.validateSubform(secondForm)) {
                secondForm.hide();

                var submitTemplate = ui.getTemplate("processingTemplate", 'The form is processed...'),
                    titleElem = searchingForm.find('.b2c-title-returned'),
                    query = ui.find('div.b2c-step2-returned input,div.b2c-step2-returned select,[type=hidden]').serialize();

                titleElem.nextAll().remove();
                titleElem.after(submitTemplate);
                searchingForm.show();

                lmpost.options.onBeforeSend();

                $.ajax(
                {
                    url: lmpost.makeUrl(lmpost.urls.submitUrl, true) + '?' + query + '&responsetype=json&ResponseAsync=1&clienturl=' + escape(location.href),
                    timeout: 15000,
                    dataType: 'jsonp',
                    success: function(data) { lmpost.options.onSuccess(data, applicationSuccess) },
                    error: lmpost.options.onError ? function(result) { lmpost.options.onError(result, applicationError); } : applicationError
                });
            } else {
                ui.blinkErrorElements();
            }

            return false;
        }
    );

    $('.b2c-btn-submit-new').on('click', skipLogin);


    $('.b2c-btn-update').on('click', transferFormUpdate);
};