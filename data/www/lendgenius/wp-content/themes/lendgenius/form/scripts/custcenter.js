﻿(function CustomerCenter(state) {

    // Wait for initialization

    if (!window.$ || !(lmpost && lmpost.options && lmpost.options.domain)) {
        window.setTimeout(function () { CustomerCenter() }, 2000);
        return;
    }

    lmpost.urls.portalUrl = ' https://www.paydaylendersearch.com/cserv/api/';

    // Check prerequisites

    //Script checking section.
    var scripts = document.getElementsByTagName('script');

    function endsWith(str, suffix) {
        return str.indexOf(suffix, str.length - suffix.length) !== -1;
    }

    var isScriptReferenced = function (scriptAliases) {
        var referenced = false;
        for (var i = 0; i < scripts.length; i++) {
            var srcElem = scripts[i],
			src = srcElem.src;

            for (var j = 0; j < scriptAliases.length; j++) {
                if (src && (endsWith(src, scriptAliases[j]))) {
                    referenced = true;
                    break;
                }
            }

            if (referenced) break;
        }

        return referenced;
    }


    if (!(state && state.jqval) && !isScriptReferenced(['jquery.validate.min.js'])) {
        $.getScript("../scripts/jquery.validate.min.js").done(function () { CustomerCenter({ jqval: true }) });
        return;
    }

    var loadScript = function (url, onload) {

        showLoader('Please wait...');
        if (url.indexOf('../') == 0) {
            url = url.replace('../', lmpost.options.domain + 'customer_center/');
        }

        var processOnload = function () {
            hideLoader();
            if (onload) onload();
        }

        $.getScript(url, processOnload);
    };

    lmpost.loadCustCenterScript = loadScript;

    var loadedScripts = [];

    var showDynaForm = function (name) {
        if (loadedScripts[name]) {
            return;
        }
        loadedScripts[name] = true;
        var container = $('div.custcenter-container'),
            custForm;

        if (container.length) {
            custForm = container.find('#clform');
            if (!custForm.length) {
                loadScript(name);
            }
        }
    }

    var container = $('div.custcenter-container');

    var showLoader = function (msg) {
        var loader;

        if (!container.length) return;
        loader = container.find('div.custcenter-loader');

        if (!loader.length) {
            loader = $('<div class="custcenter-loader"></div>').prependTo(container);
        }

        var h = container.height();

        container.find(":visible").attr('data-visible', 'true');
        container.find(':not(.custcenter-loader)').hide();

        loader.html(msg).height(h).show();

    };

    var hideLoader = function () {
        var 
            loader;

        if (!container.length) return;
        loader = container.find('div.custcenter-loader');

        loader.length && loader.hide() && container.find(':not(.custcenter-loader)[data-visible]').show();
    };


    var showLoginForm = function () {
        showDynaForm('../login.js');
    };

    var showPortalForm = function () {
        showDynaForm('../portal.js');
    };

    var setCookie = function (c_name, value, exminutes) {
        var exdate = new Date();
        exdate.setTime(exdate.getTime() + exminutes * 60000);
        var c_value = escape(value) + ((exminutes == null) ? "" : "; expires=" + exdate.toUTCString());
        document.cookie = c_name + "=" + c_value + "; path=/";
        return exdate;
    }

    var deleteCookie = function (c_name) {
        setCookie(c_name, '', -1);
    }

    var getCookie = function (c_name, c_subname) {
        var i, x, y, ARRcookies = document.cookie.split(";");
        for (i = 0; i < ARRcookies.length; i++) {
            x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
            y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
            x = x.replace(/^\s+|\s+$/g, "");
            if (x == c_name) {
                if (c_subname) {
                    ARRcookies = y.split("&");
                    for (i = 0; i < ARRcookies.length; i++) {
                        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
                        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
                        x = x.replace(/^\s+|\s+$/g, "");
                        if (x == c_subname) {
                            return unescape(y);
                        }
                    }
                } else {
                    return unescape(y);
                }
            }
        }
    }


    var getRID = function () {
        return lmpost.RID;
    };

    // Init auth api object.
    var authAPI = {},
        expiresHandled = false;

    var authExpired = function () {
        alert('Your session has ended. Please log in again to renew your session.');
        authAPI.logout();
    };

    authAPI.showLoader = showLoader;
    authAPI.hideLoader = hideLoader;

    var stylesLoaded = false;

    authAPI.loadReqStyles = function () {
        if (stylesLoaded) return;
        stylesLoaded = true;
        var href = lmpost.options.domain + "content/themes/general/b2c-auth.min.css";
        $('<link rel="stylesheet" type="text/css" href="' + href + '" />').appendTo("head");
    };

    authAPI.syncAuth = function () {
        var rid = getCookie('auth_RID'),
            fieldsEncoded = getCookie('auth_UserData'),
            expirationDate = getCookie('auth_Expires');

        if (!lmpost.RID) {
            lmpost.RID = rid;
            lmpost.retfields = fieldsEncoded && eval(fieldsEncoded);
        }

        lmpost.usrData = {};
        if (lmpost.retfields) {
            for (var i = 0; i < lmpost.retfields.length; i++) {
                var f = lmpost.retfields[i];
                lmpost.usrData[f.FieldName] = f.FieldValue;
            }
        }

        if (lmpost.RID && !expiresHandled && expirationDate) {
            setTimeout(authExpired, new Date(expirationDate) - new Date());
            expiresHandled = true;
        }
    };

    authAPI.logout = function (newloc) {
        lmpost.RID = '';
        lmpost.retfields = null;
        deleteCookie('auth_RID');
        deleteCookie('auth_UserData');
        deleteCookie('auth_Expires');

        if (!newloc) location.reload(); else window.location = newloc;
    };

    authAPI.updateAuthControls = function (local) {
        // Check if already has RID.
        authAPI.syncAuth();
        var rid = getRID();
        if (!rid) {
            $('.auth-visible').hide();
            $('.guest-visible').show();
            !local && showLoginForm();
        }
        else {
            $('.auth-visible').show();
            $('.guest-visible').hide();
            !local && showPortalForm();
        }

        if (lmpost.retfields) {
            $.each(lmpost.retfields, function (index, value) {
                switch (value.FieldName) {
                    case 'FirstName':
                        $('.auth-user-fname').html(value.FieldValue);
                        break;
                    default:
                        break;
                };
            });
        }
    };

    authAPI.authorize = function (rid, fields) {
        lmpost.RID = rid;
        lmpost.retfields = fields;

        var exdate = setCookie('auth_RID', rid, 30);
        setCookie('auth_UserData', JSON.stringify(fields), 30);
        setCookie('auth_Expires', exdate);
    }

    lmpost.authAPI = authAPI;

    authAPI.updateAuthControls();
})()