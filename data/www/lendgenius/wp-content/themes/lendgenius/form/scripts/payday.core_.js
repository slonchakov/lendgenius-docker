﻿(function pdcore(instrumented) {


    var mobile = (/iphone|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()));
    var getReturningLoader = function () {
        var loader = new lmpost.loader();

        return loader.then([lmpost.options.form + '/retform.js'])
        .then(['../scripts/returning.js', '../scripts/searching.lenders.js']);
    };

    // Test funcs.
    function getDigits(value) {
        return (!value || value.length == 0) ? "" : value.replace(/\D/g, '');
    };

    var loadRetUserStep = function () {
        var $ = jQuery;
        $('form.b2cform').remove();
        lmpost.cleanupFormUI();
        $('div.b2cloader').show();
        lmpost.formState = 'wait';
        var ldr = getReturningLoader();
        h = ldr.head;
        ldr.waitFor(function () {
            return lmpost.formState == 'active'; // Wait while returning user sequence is processed.
        })
                         .then(function () { $('#leadsb2cformsrc').after(window.lmpost.formData); $('div.b2cloader').hide(); }
                        )
                        .then(function () { // re-init plugins
                            var plugins = lmpost.options.plugins;
                            if (plugins) {
                                for (var idx in plugins) {
                                    plugins[idx]();
                                }
                            }
                        })
                        .chain(
		                        function () {
		                            return lmpost.options.appOptions && lmpost.options.appOptions.postJQScripts;
		                        }
	                        )
                        .then(function () { lmpost.customInit(); });

        h.run();
    };

    lmpost.handleReturningUser = function () {
        lmpost.formState = 'wait';

        getReturningLoader().head.run();
    };

    lmpost.isReturning = function (data) {

        if (data.Result == 3) {
            lmpost.returning = true;

            lmpost.dialogPage("../dialogs/retuser.js", 'Welcome Back!', { LoadRetUserSetup: loadRetUserStep, UserEmail: $('form.b2cform').find('#Email').val() });

        }
    };

    lmpost.fillMonthlyIncome = function (elem) {
        var values = [{ k: 700, v: 'Less than $700' }];

        for (var inc = 701; inc < 2500; inc += 100) {
            values.push({ k: inc + 99, v: '$' + inc + ' - $' + (inc + 99) });
        }

        for (var inc = 2501; inc < 5000; inc += 500) {
            values.push({ k: inc + 499, v: '$' + inc + ' - $' + (inc + 499) });
        }

        for (var inc = 5001; inc < 10000; inc += 1000) {
            values.push({ k: inc + 999, v: '$' + inc + ' - $' + (inc + 999) });
        }

        values.push({ k: 10001, v: '$10,000+' });

        for (var i = 0; i < values.length; i++) {
            var incData = values[i];

            elem.append('<option value="' + incData.k + '">' + incData.v + '</option>');
        }
    };

    function addDays(date, days) {
        var result = new Date(date);
        result.setDate(date.getDate() + days);
        return result;
    }

    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

    lmpost.configurePaydate = function (elem) {
        var dp = jQuery(elem).is("input") && jQuery(elem).datepicker;
        if (dp) {
            dp('option', 'beforeShowDay',
			function (date) {
			    var isHoliday = isHolidayDate(date);
			    if (isHoliday) {
			        return false;
			    };

			    return jQuery.datepicker.noWeekends(date);
			}
		    );
        }
        else {
            var sel = jQuery(elem), placeholder = sel.attr('placeholder');

            if (!sel.find('option').length) {
                elem.append('<option value></option>');
            }

            for (var i = 1; i <= 40; i++) {
                var dt = addDays(new Date(), i);

                if (i == 1 || dt.getDate() == 1) {
                    elem.append('<option disabled>' + months[dt.getMonth()] + ' - ' + dt.getFullYear() + '</option>');
                }

                if (!isHolidayDate(dt)) {
                    var dow = dt.getDay();
                    if (dow > 0 && dow < 6) {
                        elem.append('<option value="' + (dt.getMonth() + 1) + '/' + dt.getDate() + '/' + dt.getFullYear() + '">' + days[dt.getDay()].substr(0, 3) + ' ' + months[dt.getMonth()].substr(0, 3) + '-' + dt.getDate() + '-' + dt.getFullYear() + '</option>');
                    }
                }
            }
        }
    };

    var holidayDates = [

    //2015
                '1/1/2015', '1/19/2015', '2/16/2015',
                '5/25/2015', '7/3/2015', '9/7/2015',
                '10/12/2015', '11/11/2015', '11/26/2015',
                '12/25/2015'
    ];

    var isHolidayDate = function (date) {
        for (var i = 0; i < holidayDates.length; i++) {
            if (new Date(holidayDates[i]).toString() == date.toString()) {
                return [true];
            }
        }

        return false;
    };

    // Override std init

    lmpost.customInit = function () {

        var $ = jQuery,
            form = $('form.b2cform');

        if (!(lmpost.returning || lmpost.options.ignoreReturning)) {
            form.find('#Email').on('change', function () {
                var email = this.value;

                if (lmpost.returning || (email && lmpost.email && lmpost.email.toLowerCase() == email.toLowerCase())) {
                    return;
                }

                if (email) {
                    lmpost.email = email;
                    var url = lmpost.actionUrl('campaignstatus',
                    {
                        c: lmpost.options.campaignid,
                        email: email,
                        leadtypeid: lmpost.options.leadtypeid,
                        mailsrc: 'field',
                        callback: 'lmpost.isReturning'
                    });

                    lmpost.loadScript(url);
                }
            });
        }

        if (!lmpost.RID) {
            var checkbox =
                '<div class="b2c-radio-wrap pd-accept b2c-disclimer">' +
                    '<label><input type="checkbox" name="Optin" id="Optin" value="yes">' +
                        'By checking this box, I give my written consent to receive SMS/text messages and autodialed or prerecorded calls from you and your marketing partners at the telephone number I provided. I understand my consent is not a condition of obtaining a loan.' +
                        '<br/> Why do we require this? One reason is that lenders may confirm your application by phone for quick approval and funding. The faster they can reach you, the quicker you get your cash!' +
                    '</label> ' +
                'By clicking “Get Cash Now!” you affirm that you have read, understand, and agree to the ' +
                '<a class="b2c-popup-link" href="../documents/_disclaimer.js" target="_blank">Disclaimer</a>, ' +
                '<a class="b2c-popup-link"  href="../documents/_privacypolicy.js" target="_blank">Privacy Policy</a> and ' +
                '<a class="b2c-popup-link b2c-popup-terms" href="../documents/_terms.js" target="_blank">Terms of Use</a>, ' +
                'your click is your electronic signature, and you authorize us to share your information with lenders and other marketing partners that might use autodailers or prerecorded messages to call or text you on your mobile phone or landline. </div>';
            form.find('[class*="b2c-step"]').last().find('.b2c-row').last().after(checkbox);
        }

        form.find('#BankABA').on('change', function () {
            var me = this, lookupfield = form.find('#BankName');
            $.ajax({
                url: lmpost.actionUrl('validatebankaba', { bankaba: me.value }),
                type: "get",
                dataType: 'jsonp',
                timeout: 5000,
                success: function (data) {
                    if (data.Result && data.Result != 4) {
                        var res = { BankName: '' };
                        try {
                            res = eval('(' + data.Conversion + ')');
                        } finally {
                            lookupfield.val(res.BankName);
                            lookupfield.valid();
                        }
                    }
                }
            });
            return true;
        });


        form.find('#ZipCode').on('change', function () {
            var me = this, lookupLabel = form.find('span.pd-zip-lookup');
            if (!lookupLabel.length)
                lookupLabel = $(me).after('<span class="pd-zip-lookup"/>').next();
            $.ajax(
                {
                    url: lmpost.actionUrl('validatezipcode', { zipcode: me.value }),
                    type: "get",
                    dataType: 'jsonp',
                    timeout: 5000,
                    success: function (data) {
                        if (data.Result && data.Result != 4) {
                            var res = { StateShort: '', City: '' };
                            try {
                                res = eval('(' + data.Conversion + ')');
                            } finally {
                                if (res.StateShort == 'NY' || res.StateShort == 'AR' || res.StateShort == 'VT' || res.StateShort == 'WV') {
                                    window.onbeforeunload = null;
                                    document.location.href = 'https://www.paydaylendersearch.com/api2/Redirect-Prosper/?c=' + lmpost.options.campaignid;
                                }

                                lookupLabel.text(res.City + ', ' + res.StateShort);
                                var dlState = $('#DriversLicenseState');
                                if (!dlState.val() && res.StateShort) {
                                    dlState.val(res.StateShort);
                                    dlState.change();
                                }
                            }
                        } else {
                            form.find('#ZipCode').addClass('b2c-error').val('');
                            lookupLabel.text('');

                            if (data.AlternativeSite) {
                                lmpost.dialogPage("../dialogs/altcountry.js",
                                    "ARE YOU FROM " + data.ResolvedCountryName.toUpperCase() + "?",
                                    data);
                            }
                        }
                    },
                    error: function () {
                        form.find('#ZipCode').addClass('b2c-error').val('');
                        lookupLabel.text('');
                    }
                }
            );
        });

        //Adding Masks
        $('#RequestedAmount').mask('00000000', { reverse: true, maxlength: true });
        $('#GrossSales').mask('0000000', { reverse: true });
        $('#PhoneHome').mask('000-000-0000');
        $('#ZipCodeBusiness').mask('00000');
        $('#ZipCode').mask('00000');
        $('#PhoneWork').mask('000-000-0000');
        $('#BusinessStartDate').mask('00/0000');
        $('#PercentageOwnership').mask('000');
        $('#SSN1:not(".b2c-returned #SSN1")').mask('000-00-0000');


        // Phone validator.
        $.validator.addMethod(
            "ValidatePhone",
            function (value, element) {
                value = getDigits(value);
                value = value.replace(/^1/, '');

                if (value.length == 0) {
                    element.value = value;
                    return true;
                }
                var reg = /^([2-9][0-9]{2})([2-9][0-9]{2})([0-9]{4})$/;
                var result = this.optional(element) || (reg.test(value));
                if (result) {
                    var pc = value.match(reg);
                    element.value = pc[1] + '-' + pc[2] + '-' + pc[3];
                    return true;
                }
                value = value.substring(0, 3) + '-' + value.substring(3, 6) + '-' + value.substring(6, value.length);
                value = value.replace("--", "");

                element.value = value;

                return false;

            },
            "Please provide a valid phone number");

        //

        $.validator.addMethod("KnownZipCode", function (value, element, len) {
            var result = (window.ZipCodeValidation) ? ZipCodeValidation.isZipValid(value) : true,
                lookupLabel = $('span.pd-zip-lookup'),
                len = value && value.length;

            if (!lookupLabel.length) {
                lookupLabel = $(element).after('<span class="pd-zip-lookup"/>').next();
            }

            if (!len || !value.match(/[0-9]{5}/)) {
                lookupLabel.text('');
                return false;
            };

            if (!result)
                lookupLabel.text('');

            return result;
        }, '');


        $.validator.addMethod(
            "isValidABA",
            function (value, element) {
                var isValid = false,
                    c = 0,
                    i = 0,
                    n = 0,
                    t = "";
                try {
                    for (i = 0; i < value.length; i++) {
                        c = parseInt(value.charAt(i), 10);
                        t = t + c;
                    }
                    if (t.length == 9) // pass 9 digit check
                    {
                        n = 0;
                        for (i = 0; i < t.length; i += 3) {
                            n += (parseInt(t.charAt(i), 10) * 3) + (parseInt(t.charAt(i + 1), 10) * 7) + parseInt(t.charAt(i + 2), 10);
                        }
                        isValid = (n != 0 && n % 10 == 0);
                    }
                } catch (err) {
                }
                return this.optional(element) || isValid;
            }, ""
        );

        $.validator.addMethod(
            "IsWorkingDay",
            function (value, element) {
                if (!value) {
                    return true;
                }

                var date = new Date(value),
                    day = date.getDay();

                return (day > 0 && day < 6) ? !isHolidayDate(date) : false;
            }
        );

        $.validator.addMethod(
            "validateSSN",
            function (value, element) {
                value = (value.length == 0) ? "" : value.replace(/\D/g, '');

                var reg = /^([0-9]{3})([0-9]{2})([0-9]{4})$/;
                var result = value.length > 0 && reg.test(value);
                if (result) {
                    var pc = value.match(reg);
                    element.value = pc[1] + '-' + pc[2] + '-' + pc[3];
                    return true;

                }

                var len = (value.length > 9) ? 9 : value.length;
                value = value.replace(/\s|-/g, '');
                value = value.substring(0, 3) + '-' + value.substring(3, 5) + '-' + value.substring(5, len);

                element.value = value;

                return false;
            }, ""
        );

        var ui = lmpost.getFormUI();

        // Init mobile-friendly dp
        lmpost.preInitMobileDP(form, true);

        // Copy value from SSN1 field to SSN without optional characters.
        form.find('#SSN1').on('change',
            function () {
                var ssnRaw = $(this).val();

                ui.byID('SSN').val(ssnRaw.replace(/\s|-/g, '').substr(0, 9));
            }
        );



        //Payday-Installment switcher for hybrid forms
        var creditScore = '<div class="credit-score b2c-row"><label for="credit">What is your credit score?</label><div class="b2c-label-col"><select name="credit" id="Credit"><option value="">Select</option><option value="EXCELLENT">Excellent Credit (760+)</option><option value="VERYGOOD">Very Good Credit (720-759)</option><option value="GOOD">Good Credit (660-719)</option><option value="FAIR">Fair Credit (600-659)</option><option value="POOR">Poor Credit (580-599)</option><option value="VERYPOOR">Very Poor Credit (500+)</option><option value="UNSURE">Not Sure</option></select></div></div>';
        var cresitScoreSwitcher = function () {
            var amountVal = $(".b2c-flex #RequestedAmount").val();
            if (amountVal >= 1500 && $('.b2c-flex .credit-score').length == 0) {
                form.find('[class*="b2c-step"]').last().find('.b2c-disclimer').before(creditScore);
                return false;
            }
            else if (amountVal >= 1500 && $('.b2c-flex .credit-score').length != 0) {
                return false;
            }
            else {
                $('.b2c-flex .credit-score').remove();
                return false;
            }
        };

        $("#RequestedAmount").change(function () {
            cresitScoreSwitcher();
        });
        $(function () {
            cresitScoreSwitcher();
        });

        // Only app specific rules are defined here.
        var validationRules = {
            ZipCode: { KnownZipCode: true },
            SSN1:
            {
                required: true,
                validateSSN: true,
                minlength: 9
            },
            DriversLicense: { required: true, minlength: 5 },
            BankName: { required: true, minlength: 2 },
            BankABA: { required: true, digits: true, minlength: 9, isValidABA: true },
            BankAccountNumber: { required: true, minlength: 5 },
            PayDate1: { IsWorkingDay: true }
        };


        lmpost.fillMonthlyIncome($('form.b2cform').find('#MonthlyIncome'));

        ui.InitForm(validationRules);

        // Configure paydate field.
        lmpost.configurePaydate(ui.byID('PayDate1'));
        lmpost.configurePaydate(ui.byID('PayDate2'));

        var pdOptions = lmpost.options.payday;

        ui.find("a.b2c-popup-link").on('click',
            function () {
                var el = $(this);
                lmpost.dialogPage(el.attr("href"), el.text());
                return false;
            }
        );

        var hideLogo = pdOptions && pdOptions.hideNortonLogo;

        if (hideLogo) {
            ui.find('.b2c-norton-top').hide();
        } else {

            $(document).on('click', '.b2c-norton-top', function () {
                var sdomain = 'www.paydaylendersearch.com';
                if (pdOptions && pdOptions.NSDomain)
                    sdomain = pdOptions.NSDomain;
                var url = 'https://trustsealinfo.verisign.com/splash?form_file=fdf/splash.fdf&dn=' + sdomain + '&lang=en';
                window.open(url, '_blank', 'height=445,width=560');
            });
        }


        // Init from query params.
        var queryParams = $.parseQuery();
        for (var key in queryParams) {
            var val = queryParams[key],
                target = ui.find('[name=' + key + ']');

            if (target.length && target.val && val) {
                val = decodeURIComponent(val);

                if (target.data('qs-set')) {
                    target.removeData('qs-val');
                    target.removeData('qs-set');
                    return;
                }
                target.val(val);
                target.data('qs-val', val);
                target.change();
            }

        }

        //Init Multiple select
        if (!mobile) {
            $('select.b2c-multiple').multipleSelect();
        }

        //Hints for forms with content
        var ssnHint = '<div class="b2c-hint"><strong><em class="b2c-ico-help">?</em> <p>Your social security number is encrypted with a 256-bit SSL.</strong> To protect your identity and privacy we never store your social security number</p></div>';
        var bankAbaHint = '<div class="b2c-hint"><em class="b2c-ico-help">?</em> <p>You data is secured using industry standard for the site information security. Your ABA/Routing number can be found on most bank statements as well as on the bottom of your checks.</p> <em class="b2c-hint-aba"></em></div>';
        var bankNumberHint = '<div class="b2c-hint"><em class="b2c-ico-help">?</em> <p>Your <strong>Checking Account</strong> number can be found on most bank statements as well as on the bottom of your checks.</p> <em class="b2c-hint-account"></em></div>';


        $.fn.hint = function (param) {
            $(this).focusin(function () {
                if ($(this).parent().find('.b2c-hint').length != 0) {
                    $(this).siblings(".b2c-hint").show();
                }
                else {
                    $(this).parent().append(param);
                }
            });
            $(this).focusout(function () {
                $(this).siblings(".b2c-hint").hide();
                return false;
            });
        };

        $('#BankAccountNumber').hint(bankNumberHint);
        $('#SSN1').hint(ssnHint);
        $('#BankABA').hint(bankAbaHint);


    };

})();
