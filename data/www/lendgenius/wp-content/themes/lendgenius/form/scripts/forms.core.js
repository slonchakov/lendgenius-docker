﻿(function() {

    function createLoader() {
        var div, img, container;
        el = document.getElementById('leadsb2cformsrc');
        if (el != null) {
            container = el.parentNode;
            div = document.createElement("DIV");
            div.className = 'b2cloader';
            div.style.position = 'relative';
            div.style.maxWidth = '740px';
            div.style.minHeight = '525px';
            div.style.border = '1px solid #eeeeee';
            div.style.margin = '0 auto';
            div.style.background = '#fff';
            div.style.display = 'none';
            div.id = "pd_loader";
            container.appendChild(div);

            img = document.createElement('IMG');
            img.src = '../content/themes/images/loader.gif'.replace('../', lmpost.options.domain);
            img.width = '100';
            img.height = '100';
            img.style.position = 'absolute';
            img.style.top = '50%';
            img.style.left = '50%';
            img.style.margin = '-50px 0 0 -50px';
            div.appendChild(img);
        }
    }


    createLoader();

    function getGuid() { var x = (new Date).getTime(), e = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (e) { var r = (x + 16 * Math.random()) % 16 | 0; return x = Math.floor(x / 16), ("x" == e ? r : 3 & r | 8).toString(16) }); return e }

    var formState = {
        'state': '',
        'substate': '',
        'target': '',
        'formType': '',
        'clientId': getGuid(),
        'time': 0
    }, cachedState = {
        'state': '.',
        'substate': '.',
        'target': '.',
        'formType': '.'
    };

    lmpost.clientId = formState.clientId;
    var stateQueue = [];
    function submitState() {
        var img = document.createElement('img'),
            baseUrl = 'https://loanmtchsvtracking.com/',
            hitUid = lmpost.options.hituid || '', queueItem, withHistory = false,
            stateArray = [], substateArray = [], targetArray = [], formTypeArray = [], timeArray = [];
        function parseQueueItem(queueItem) {
            stateArray.push(queueItem.state);
            substateArray.push(queueItem.substate);
            targetArray.push(queueItem.target);
            formTypeArray.push(queueItem.formType);
            timeArray.push(queueItem.time);
        }
        while (stateQueue.length > 1) {
            queueItem = stateQueue.shift();
            parseQueueItem(queueItem);
            withHistory = true;
        }
        queueItem = stateQueue[0];
        if (withHistory) {
            parseQueueItem(queueItem);
        }
        queueItem.time = (new Date()).getTime();
        parseQueueItem(queueItem);

        img.setAttribute('src', baseUrl + '?h=' + hitUid +
            '&c=' + formState.clientId +
            '&a=' + stateArray.join() +
            '&s=' + substateArray.join() +
            '&o=' + targetArray.join() +
            '&f=' + formTypeArray.join() +
            '&t=' + timeArray.join());
        document.getElementsByTagName('body')[0].appendChild(img);
        img.parentElement.removeChild(img);
    }

    setInterval(submitState, 1000);

    lmpost.collect = function (state, substate, target) {
        if (state !== cachedState.state || substate !== cachedState.substate || target !== cachedState.target) {
            formState.state = cachedState.state === state ? '.' : state;
            formState.substate = cachedState.substate === substate ? '.' : substate;
            formState.target = cachedState.target === target ? '.' : target;
            formState.time = (new Date()).getTime();
            var formType = lmpost.options.form.replace('../', '') + (lmpost.getTestPostfix?lmpost.getTestPostfix() : '');
            formState.formType = cachedState.formType === formType ? '.' : formType;
            cachedState.state = state;
            cachedState.substate = substate;
            cachedState.target = target;
            cachedState.formType = formType;
            stateQueue.push(JSON.parse(JSON.stringify(formState)));
        }
    };

    function loadScript(url, id, onload) {
        var script = document.createElement('script'),
            stime = new Date();
        script.setAttribute('type', 'text/javascript');
        if (url.indexOf('../') == 0) {
            url = url.replace('../', lmpost.options.domain);
            url += (url.indexOf('?') > -1) ? '&' : '?';
            if (lmpost.coreCheck) url += 'fcv=' + lmpost.coreCheck;
        }

        script.setAttribute('src', url);
        script.setAttribute('async', 'async');

        if (id != undefined)
            script.setAttribute('id', id);
        if (onload != undefined) {
            script.onreadystatechange = function() {
                var i;
                if (this.readyState == 'complete' || this.readyState == 'loaded') {
                    script.onreadystatechange = script.onload = script.onerror = null;
                    script.setAttribute('elapsed', new Date() - stime);
                    onload();
                }
            };
            script.onload = script.onerror = function() {
                script.onreadystatechange = script.onload = script.onerror = null;
                script.setAttribute('elapsed', new Date() - stime);
                onload();
            };
        }


        document.getElementsByTagName('head')[0].appendChild(script);
    }

    lmpost.loadScript = loadScript;

    loadScript('../scripts/forms-bundle.min.js');

    lmpost.collect('loading', 'init', 'form');

})();