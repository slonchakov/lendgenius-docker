﻿(function slendres(instrumented) {
    if (!instrumented && window.runInstrumented) {
        runInstrumented(slendres, 'slendres');
        return;
    }

    var opts = lmpost.options;

    opts.appOptions.templates.processingTemplate = "<div class=\"b2c-search\">"
                         + "<h1 class=\"b2c-searching-title\">Please wait while we search our panel of lenders and authorised brokers for you.<\/h1>"
                         + "<h2 class=\"b2c-search-subtitle\">Check your email for loan confirmation and exclusive offers.<\/h2>"
                         + "<h3 class=\"b2c-search-subtitle\">Do not close this window until you are presented with an offer from a lender.<\/h3>"
                         + " <p class=\"b2c-search-subtitle\">Form processing<\/p>"
                         + " <p class=\"b2c-search-percent\" id=\"search-percent\">0%<\/p>"
                         + " <div class=\"b2c-search-loading-wrap\">"
                         + " <div class=\"b2c-search-loading\" id=\"search-loading\" style=\"width: 0%\">"
                         + " <\/div><\/div>"
                         + " <p>Once you are matched with an authorised Lender or Broker,<\/p>"
                         + " <p>you will be redirected to the electronic signature page.<\/p>"
                         + "<\/div>";

    var lendersFound = false,
		startTime;

    var responseReceived = false;
    var loaded = 0;

    //Init callbacks and deffered elemennts.
    var beforeSendData = function () {
        startTime = new Date().getTime();
        window.scrollTo(0, 0);
        ChangeLoaded();
    };

    opts.onBeforeSend = beforeSendData;
    var origSuccess = opts.onSuccess;
    opts.onSuccess = opts.onError = function (data, defaultHandler) {
        var result = 0,
        finalize = function () {
            responseReceived = true;
            FastFillProgressBar();
            if (origSuccess)
            { result = origSuccess(data, defaultHandler); }
            else {
                if (defaultHandler) { result = defaultHandler(data); }
            }
        };

        if (!data) {
            return;
        }

        if (data.Result == 0 && lmpost.options.appOptions.noRedirect) {
            return -1; // If is still processing just ignore custom actions.
        };

        if ( data.Result == 4) {
            finalize();
            return 0;
        }
        else {
            window.setTimeout(finalize, 10000);
        }

        return result;
    };

    function redirect(url) {
        window.location = url;
    }


    function ChangeLoaded() {
        if (!responseReceived) {
            loaded = loaded + Math.floor(Math.random() * 5);
            if (loaded > 100) {
                loaded = 0;
            }
            $('#search-loading').attr('style', 'width:' + loaded + '%');
            $('#search-percent').text(loaded + '%');
            window.setTimeout(function () {
                ChangeLoaded(loaded);
            }, 300 + Math.floor(Math.random() * 2000));
        }
    };

    function FastFillProgressBar(redirectUrl) {
        if (loaded < 100) {
            loaded++;
            $('#search-loading').attr('style', 'width:' + loaded + '%');
            $('#search-percent').text(loaded + '%');
        }
    };

})()

