﻿lmpost.setupReturning = function() {

    // Cleanup form
    if ($('form.b2cform').length > 1) {
        $('form.b2cform').last().remove();
    }

    // Init YOB
    var form = $('form.b2cform').first();
    var yobSelect = form.find('#YOB');
    var curYear = new Date().getFullYear();


    for (var yr = curYear - 18; yr > curYear - 100; yr--) {
        yobSelect.append('<option value=' + yr + ' >' + yr + '</option>');
    }

    var queryParams = $.parseQuery(),
        zipCode = unescape(queryParams['ZipCode'] || queryParams['zipcode']);


    if (zipCode && zipCode != 'undefined') {
        form.find('#ZipCode').val(zipCode);
    }


    var ui = new lmpost.FormLayout();
    var rid;
    var amount, income, freq, paydate;


    var rules =
    {
        SSN: { required: true, digits: true, minlength: 4, maxlength: 4 }
    };

    ui.Init(form, rules);


    // Add hidden hit identifier field.

    form.find('[type=hidden]').last().after('<input type="hidden" id="uid" name="uid" value="' + lmpost.options.hituid + '"/>');

    lmpost.fillRequestedAmount(ui.byID('RequestedAmount'));
    lmpost.fillMonthlyIncome(ui.byID('MonthlyIncome'));
    lmpost.configurePaydate(ui.byID('PayDate1'));

    if (!lmpost.ready) {
        lmpost.collect('ready', 'returning', 'form');
        lmpost.ready = true;
    }

    var firstimeflag = true;
    var goStep1Error = function(data) {

        var step1Form = ui.find('.b2c-step1-returned');
        if (data.Result == 4) {
            if (data.Errors && data.Errors.length) {
                $.each(data.Errors, function(index, value) {
                        value.Field && step1Form.find('[name=' + value.Field + ']').addClass('b2c-error');
                    }
                );
            }
        }

        if (data.Result == 2) {
            if (firstimeflag) {
                firstimeflag = false;
                step1Form.find('#YOB,#ZipCode,#SSN1').addClass('b2c-error');
            } else {
                skipLogin();
                return;
            }
        }

        form.find('.b2c-step-load').hide();
        step1Form.show();
        ui.blinkErrorElements();
    };

    var isInt = function(value) {
        return !isNaN(parseInt(value, 10)) && (parseFloat(value, 10) == parseInt(value, 10));
    }

    var setSelectValue = function(jqRoot, key, fieldValue) {
        var options = jqRoot.find('[name=' + key + '] option'),
            len = options.length;
        $.each(options, function(index, option) {
            var rawVal = $(option).attr('value');
            var optionVal = isInt(rawVal) ? parseInt(rawVal) : rawVal;
            if (fieldValue <= optionVal || (fieldValue > optionVal && index == (len - 1))) {
                $(option).attr('selected', true);
                return false;
            }
        });
    };

    var setIncomeValue = function(jqRoot, fieldValue) {
        setSelectValue(jqRoot, 'MonthlyIncome', fieldValue);
    };

    var showStep2 = function(data) {
        if (!lmpost.retfields) {
            lmpost.retfields = data.fields;
        }
        var step2Form = form.find('.b2c-step2-returned');
        if (data.Result == 1) {
            var retDate = step2Form.find('.b2c-submit-date');

            if (retDate.find('.b2c-day').length) {
                var monthShort = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                    parts = data.LastDate.split('/');
                retDate.find('.b2c-month').text(monthShort[parts[0] - 1]);
                retDate.find('.b2c-day').text(parts[1]);
                retDate.find('.b2c-year').text(parts[2]);
            } else {
                retDate.html(data.LastDate);
            }

            if (data && data.Fields) {
                $.each(data.Fields, function(index, value) {
                    switch (value.FieldName) {
                    case 'FirstName':
                        step2Form.find('#FirstName').html(value.FieldValue + "!");
                        break;
                    case 'MonthlyIncome':
                        setIncomeValue(step2Form, value.FieldValue);
                        break;
                    default:
                        step2Form.find('[name=' + value.FieldName + ']').val(value.FieldValue);
                    }
                });
            }

            rid = data.RID;
            form.append('<input type="hidden" id="RID" name="RID" value="' + data.RID + '"/>');
            form.find('.b2c-step-load').hide();
            form.find('.b2c-step1-returned').hide();
            step2Form.show();
        } else {
            goStep1Error(data);
            return;
        }
    };

    var goFormUpdate = function(data) {
        //
        lmpost.options.appOptions.postJQScripts = lmpost.options.appOptions.postJQScripts.then(
            function() {
                lmpost.getFormUI().addListener('formReady',
                    function() {
                        prepareForm(data);
                    });
            });
        skipLogin();
    };

    $('a.b2c-btn-verify').on('click',
        function() {
            var firstForm = ui.find('.b2c-step1-returned');
            if (ui.validateSubform(firstForm)) {
                firstForm.hide();
                $('.b2c-step-load').show();
                $.ajax(
                {
                    url: window.lmpost.urls.apiUrl + 'misc/?action=leadreturn&responsetype=json&'
                        + 'email=' + escape(lmpost.email)
                        + '&' + form.find('div.b2c-step1-returned input,div.b2c-step1-returned select,[type=hidden]').serialize(),
                    timeout: 15000,
                    dataType: 'jsonp',
                    success: showStep2,
                    error: goStep1Error
                });
            } else {
                ui.blinkErrorElements();
            }
        }
    );

    var setOrigVal = function(elem, val) {
        elem.val(val);
        elem.data('orig-value', val);
        elem.addClass('b2c-ignore');
        elem.addClass('b2c-dont-send');
    };

    var prepareForm = function(data) {
        if (data.Result != 1) {
            return;
        };

        var mainForm = lmpost.getFormUI(),
            ridLoc = rid || lmpost.RID;

        mainForm.find('[type=hidden]').last().after('<input type="hidden" id="RID" name="RID" value="' + ridLoc + '"/>');

        $.each(data.Fields, function(index, value) {
                switch (value.FieldName) {
                case 'RequestedAmount':
                    if (amount) {
                        mainForm.find('#RequestedAmount').val(amount);
                    }
                    break;
                case 'SSN':
                    var ssnVal = '***-**-' + value.FieldValue.substring(5);
                    mainForm.find('#SSN1').val(ssnVal);
                    break;
                case 'PayFrequency':
                    var freqVal = value.FieldValue;
                    if (freq) {
                        freqVal = freq;
                    }
                    mainForm.find('#PayFrequency').val(freqVal);
                    break;
                case 'PayDate1':
                    var pdVal = value.FieldValue;
                    if (paydate) {
                        pdVal = paydate;
                    }
                    mainForm.find('#PayDate1').val(paydate);
                    break;
                case 'Address1':
                    mainForm.find('#Address1').val(value.FieldValue);
                    break;
                case 'IncomeType':
                    var val = (value.FieldValue == 'EMPLOYMENT') ? 'Employment' : 'Benefits';
                    mainForm.find('#IncomeType').val(val);
                    break;
                case 'MonthlyIncome':

                    var incomeValue = value.FieldValue;
                    if (income) {
                        incomeValue = income;
                    }
                    setIncomeValue(mainForm.find('#' + value.FieldName).parent(), incomeValue);
                    break;
                case 'BankAccountNumber':
                    setOrigVal(mainForm.find('#BankAccountNumber'), value.FieldValue);
                    break;
                case 'DriversLicense':
                    setOrigVal(mainForm.find('#DriversLicense'), value.FieldValue);
                    break;
                default:

                    var control = mainForm.find('#' + value.FieldName);
                    if (control.length) {

                        var prevVal = value.FieldValue;
                        if (prevVal.toLowerCase() == 'true' || prevVal.toLowerCase() == 'false') {
                            prevVal = (prevVal.toLowerCase() == 'true') ? '1' : '0';
                        }

                        if (control.is('select')) {
                            setSelectValue(mainForm, value.FieldName, prevVal);
                        } else {

                            control.val(prevVal);
                            control.change && control.change();
                        }
                    }
                }
            }
        );

        $('#LastName,#FirstName,#SSN1,#Email,#DOB').attr('disabled', 'disabled');

        var wrapValidator = function(validator) {
            return function(value, element, p1, p2) {
                var isAutoFilled = $(element).hasClass('b2c-ignore');

                if (isAutoFilled) {
                    var origData = $(element).data('orig-value');
                    if (value == origData) {
                        return true;
                    } else {
                        $(element).removeClass('b2c-ignore');
                        $(element).removeClass('b2c-dont-send');
                    }
                }

                return validator.apply(this, [value, element, p1, p2]);
            };
        };

        for (var key in $.validator.methods) {
            var original = $.validator.methods[key];
            var wrapperData = { orig: original };
            $.validator.methods[key] = null;
            $.validator.addMethod(key, wrapValidator(original));
        }

        var origAddMethod = $.validator.addMethod,
            handler = wrapValidator(original);
        $.validator.addMethod = function(key, method) {
            origAddMethod.apply(this, [key, handler]);
        };

        $('.b2c-required:not([disabled])').valid();
    };

    //validateSubform 
    var skipLogin = function() {
        $('form.b2cform').hide();
        $('div.b2cloader').show();

        $('form.b2cform').remove();
        lmpost.formState = 'active';
    };

    var applicationError = function(result) {
        var step2Form = ui.find('.b2c-step2-returned');
        ui.find('.b2c-step-searching').hide();
        step2Form.show();

        result && lmpost.processConversion($('form.b2cform'), result);

        if (result && result.Errors && result.Errors.length) {
            $.each(result.Errors, function(index, value) {
                    value.Field && step2Form.find('[name=' + value.Field + ']').addClass('b2c-error');
                }
            );
        }

        ui.blinkErrorElements();
    };

    var applicationSuccess = function(data) {
        if (data && data.Result != 4) {
            lmpost.processConversion($('form.b2cform'), data);

            if (data.RedirectURL && !lmpost.options.appOptions.noRedirect) {
                window.location = data.RedirectURL;
            }
        } else {
            applicationError(data);
        }
    };

    var transferFormUpdate = function() {
        // Request leadreturnedit details.
        var secondForm = ui.find('.b2c-step2-returned');

        // Save field values.
        amount = secondForm.find('#RequestedAmount').val();
        income = secondForm.find('#MonthlyIncome').val();
        freq = secondForm.find('#PayFrequency').val();
        paydate = secondForm.find('#PayDate1').val();

        if (lmpost.RID && !form.find('#RID').length) {
            form.append('<input type="hidden" id="RID" name="RID" value="' + lmpost.RID + '"/>');
        }


        secondForm.hide();
        form.find('.b2c-step-load').show();
        $.ajax(
        {
            url: window.lmpost.urls.apiUrl + 'misc/?action=leadreturnedit&responsetype=json&' + form.find('[type=hidden]').serialize(),
            timeout: 15000,
            dataType: 'jsonp',
            success: goFormUpdate,
            error: goStep1Error
        });
    };

    if (!lmpost.RID) {
        ui.find('.b2c-step1-returned').show();
    } else {
        var dta = { Result: 1, RID: lmpost.RID, LastDate: lmpost.lastDate, Fields: lmpost.retfields },
            updateMode = (unescape(queryParams['updateapp']) == 'true');
        if (updateMode) {
            transferFormUpdate();
            return;
        }

        showStep2(dta);
    }

    $('a.b2c-btn-submit').on('click',
        function() {
            var secondForm = ui.find('.b2c-step2-returned');
            var searchingForm = ui.find('.b2c-step-searching');
            if (ui.validateSubform(secondForm)) {
                secondForm.hide();

                var submitTemplate = ui.getTemplate("processingTemplate", 'The form is processed...'),
                    titleElem = searchingForm.find('.b2c-title-returned'),
                    query = ui.find('div.b2c-step2-returned input,div.b2c-step2-returned select,[type=hidden]').serialize();

                titleElem.nextAll().remove();
                titleElem.after(submitTemplate);
                searchingForm.show();

                lmpost.options.onBeforeSend();

                $.ajax(
                {
                    url: lmpost.makeUrl(lmpost.urls.submitUrl, true) + '?' + query + '&responsetype=json&ResponseAsync=1&clienturl=' + escape(location.href),
                    timeout: 15000,
                    dataType: 'jsonp',
                    success: function(data) { lmpost.options.onSuccess(data, applicationSuccess) },
                    error: lmpost.options.onError ? function(result) { lmpost.options.onError(result, applicationError); } : applicationError
                });
            } else {
                ui.blinkErrorElements();
            }

            return false;
        }
    );

    $('.b2c-btn-submit-new').on('click', skipLogin);


    $('.b2c-btn-update').on('click', transferFormUpdate);
};