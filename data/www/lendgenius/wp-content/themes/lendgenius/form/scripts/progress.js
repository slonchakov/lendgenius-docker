﻿(function plugin(instrumented) {
    if (!instrumented && window.runInstrumented) {
        runInstrumented(plugin, 'plugin');
        return;
    }
    if (!lmpost.options.plugins) {
        lmpost.options.plugins = {};
    }

    var plugins = lmpost.options.plugins;
    if (!plugins['progress']) {
        plugins['progress'] = plugin;
    }

    var pluginOptions = lmpost.options.progressPlugin;

    var hideProgress = pluginOptions && pluginOptions.remove,
		initialPerc = (pluginOptions && pluginOptions.initialPercentage) || 0;

    var ui = lmpost.getFormUI(),
		insertionElement = $('.b2c-form-container'),
		requiredCount = 0,
		validCount = 0,
		valids = {},

	setProgress = function (perc) {
	    var pbar = $('.b2c-progress-bar-line').first();

	    if (perc > 100) perc = 100;
	    $('.b2c-progress-num').text(perc + '%');
	    pbar.css('width', perc + '%');
	},

		onValidate = function (element, isValid) {
		    var el = $(element),
				id = el.attr('id');

		    requiredCount = $('.b2c-required:not([disabled])').length;

		    if (!el.hasClass('b2c-required')) {
		        return;
		    }

		    if (isValid) {
		        valids[id] = true;
		    }
		    else {
		        delete valids[id];
		    }

		    if (hideProgress) {
		        return;
		    }

		    validCount = 0;
		    for (var prop in valids) {
		        validCount++;
		    }

		    var percentage = Math.round((100 - initialPerc) * validCount / requiredCount),
				percDisplay = percentage + initialPerc;
		    setProgress(percDisplay);
		};




    if (!hideProgress) {
        insertionElement.prepend(' <div class="b2c-progress-bar-wrap"><div class="b2c-progress-text"><span class="b2c-progress-num">0%</span> Complete</div><div class="b2c-progress-bar"><div class="b2c-progress-bar-line"></div></div>'
		);
        setProgress(initialPerc);
    }

    var container = $('.b2c-progress-bar-wrap');


    ui.addListener('formReady',
		function () {

		    ui.addListener('acquireValidationState', onValidate);
		}
	);

    ui.addListener('formBeforeSubmit',
		function () {
		    if (!hideProgress) {
		        container.hide();
		    }
		}
		);

    ui.addListener('formSubmitError',
		function () {
		    if (!hideProgress) {
		        container.show();
		    }
		}
		);
})();
