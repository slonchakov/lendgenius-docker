﻿(function blcore(instrumented) {

    var mobile = (/iphone|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()));

    // Test funcs.
    function getDigits(value) {
        return (!value || value.length == 0) ? "" : value.replace(/\D/g, '');
    };

    lmpost.handleReturningUser = function () {
    };

    lmpost.isReturning = function (data) {
        return false
    };



    // Override std init

    lmpost.customInit = function () {

        var form = $('form.b2cform');

        if (!lmpost.RID) {
            var checkbox =
              '<div class="b2c-row b2c-radio-wrap pd-accept"><label><input type="checkbox" id="accepted" name="accepted" style="margin-right:10px;">By clicking “Apply Now!” you affirm that you have read, understand, and agree to our  <a class="b2c-popup-link" href="../documents/_disclaimer.js" target="_blank">Disclaimer</a>, <a class="b2c-popup-link"  href="../documents/_privacypolicy.js" target="_blank">Privacy Policy</a>  and <a class="b2c-popup-link b2c-popup-terms" href="../documents/_terms.js" target="_blank">Terms of Use</a> this acts as your electronic signature, and you authorize us to share your information with lenders.</label></div>';

            form.find('[class*="b2c-step"]').last().find('.b2c-row').last().after(checkbox);
        }


        // Phone validator.
        $.validator.addMethod(
                "ValidatePhone",
            function (value, element) {
                value = getDigits(value);
                value = value.replace(/^1/, '');

                if (value.length == 0) {
                    element.value = value;
                    return true;
                }
                var reg = /^([2-9][0-9]{2})([2-9][0-9]{2})([0-9]{4})$/;
                var result = this.optional(element) || (reg.test(value));
                if (result) {
                    var pc = value.match(reg);
                    element.value = pc[1] + '-' + pc[2] + '-' + pc[3];
                    return true;
                }
                value = value.substring(0, 3) + '-' + value.substring(3, 6) + '-' + value.substring(6, value.length);
                value = value.replace("--", "");

                element.value = value;

                return false;

            },
            "Please provide a valid phone number");

   
        var ui = lmpost.getFormUI();

     
        // Only app specific rules are defined here.
        var validationRules = {
            PrimaryPhone : { ValidatePhone : true }
        };

    
        ui.InitForm(validationRules);

        ui.find("a.b2c-popup-link").on('click',
            function () {
                var el = $(this);
                lmpost.dialogPage(el.attr("href"), el.text());
                return false;
            }
        );


        // Init from query params.
        var queryParams = $.parseQuery();
        for (var key in queryParams) {
            var val = queryParams[key],
			target = ui.find('[name=' + key + ']');

            if (target.length && target.val && val) {
                val = decodeURIComponent(val);

                if (target.data('qs-set')) {
                    target.removeData('qs-val');
                    target.removeData('qs-set');
                    return;
                }
                target.val(val);
                target.data('qs-val', val);
                target.change();
            }

        }

    }
})();
