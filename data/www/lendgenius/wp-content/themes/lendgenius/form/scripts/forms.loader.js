﻿(function() {
    function loadScripts(sequence, callback) {

        var next = function(headNode) {
            var me = this,
                seq,
                nextLvl;

            me.head = (headNode) ? headNode : me;

            me.run = function() {
                var onNext = function() {
                    nextLvl && nextLvl.run();
                };

                if (seq instanceof Function) {
                    seq();
                    nextLvl && nextLvl.run();

                    return;
                }

                if (seq) {
                    if (!seq.length) {
                        onNext(); // skip empty seqs
                    } else {
                        loadScripts(seq, onNext);
                    }
                }
            };

            me.then = function(list) {
                seq = list;
                nextLvl = new next(me.head);
                return nextLvl;
            };

            me.nowait = function(list) {
                seq = list;
                list.noblock = true;
                nextLvl = new next(me.head);
                return nextLvl;
            };

            me.chain = function(continuation) {

                if (continuation) {
                    me.run = function() {
                        // Continuation can be defined statically or dynamically.
                        if (continuation instanceof Function) {
                            continuation = continuation();
                        }

                        if (!continuation) {
                            continuation = new lmpost.loader(); // Create a fake continuatuion element if not found.
                        }

                        continuation.then(
                            function() {
                                nextLvl && nextLvl.run(); // After executing the continuation chain, pass control to the following action in current chain.
                            });
                        continuation.head.run();
                    };
                }

                nextLvl = new next();
                return nextLvl;
            };

            me.waitFor = function(condition) {
                nextLvl = new next(me.head);

                var wait = function() {
                    if (condition()) {
                        nextLvl && nextLvl.run();
                    } else {
                        window.setTimeout(wait, 50); // May be we're lucky next time.
                    }
                };

                me.run = wait; // Here is the magic, the wait func s/b triggered by run in the chain.

                return nextLvl;
            };

            me.waitForScript = function(scriptAliases, scriptUrl, condition) {
                var toRun = [],
                    url,
                    referenced;

                if (!condition || condition()) {
                    return me;
                }


                var testFun = function() {
                    url = (scriptUrl instanceof Function) ? scriptUrl() : scriptUrl;


                    // Search for defined script.
                    var scripts = document.getElementsByTagName('script');

                    for (var i = 0; i < scripts.length; i++) {
                        var srcElem = scripts[i],
                            src = srcElem.src;

                        for (var j = 0; j < scriptAliases.length; j++) {
                            if (src && (endsWith(src, scriptAliases[j]))) {
                                referenced = true;
                                break;
                            }
                        }

                        if (referenced) break;
                    }

                    if (!referenced && !condition()) {
                        // Include jq script in the loading sequence if not already referenced by teh page.
                        toRun.push(url.replace(/^http:|^https:|^file:/gi, getResourcesProtocol()));
                    }
                };

                return me.then(testFun).then(toRun).waitFor(condition);
            };
        };

        var nextSeq = new next();
        if (!sequence) return nextSeq;

        sequence.reverse();

        var len = sequence.length;

        var onload = function() {
            len--;
            if (!len) {
                // All scripts have been loaded, proceed to next lvl.
                nextSeq.run();
                callback && callback();
            }
        };

        while (sequence.length) {

            var item = sequence.pop();

            lmpost.loadScript(item, null, onload);
        }

        return nextSeq;
    }

    lmpost.loadScripts = lmpost.loader = loadScripts;
})();