﻿
lmpost.readCookie = function (name) {
    return (name = new RegExp('(?:^|;\\s*)' + ('' + name).replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&') + '=([^;]*)').exec(document.cookie)) && name[1];
};

lmpost.writeCookie = function (c_name, value, exminutes) {
    var exdate = new Date();
    exdate.setTime(exdate.getTime() + exminutes * 60000);
    var c_value = escape(value) + ((exminutes == null) ? "" : "; expires=" + exdate.toUTCString());
    document.cookie = c_name + "=" + c_value + "; path=/";
    return exdate;
}

lmpost.getOrCreateTestVar = function (name, variants) {
    if (!name || !variants.length) throw 'Incorrect params';
    var cname = 'lmtestvars', varCookieRaw = lmpost.readCookie(cname),
        varCookie = varCookieRaw ? unescape(varCookieRaw) : '',
        varMatch = new RegExp('(^|&)' + name + '=((.)*?)(&|$)').exec(varCookie);

    if (varMatch && varMatch.length >= 3) {
        return varMatch[2];
    }

    var rndIdx = Math.floor(Math.random(new Date().getMilliseconds()) * variants.length), val = variants[rndIdx],
    nvp = name + '=' + val,
    cookieVal = (varCookie) ? varCookie + '&' + nvp : nvp;

    lmpost.writeCookie(cname, cookieVal, 7 * 24 * 60);
    return val;
};

lmpost.getTestPostfix = function () {
    var cname = 'lmtestvars', varCookieRaw = lmpost.readCookie(cname),
         varCookie = varCookieRaw ? unescape(varCookieRaw) : '';

    if (varCookie) {
        var pairs = varCookie.split('&'),
         result = '';
        for (var i = 0; i < pairs.length; i++) {
            var pair = pairs[i].split('=');
            if (pair.length == 2) result += '_' + pair[0] + '_' + pair[1];
        }

        return result;
    }
    return '';
};

// Event helper class.
function EventHelper(handlers, objToExtend) {
    var me = this,
	eventHandlers = handlers;

    me.fireEvent = function (event, paramsArray) {
        var delegate = eventHandlers[event];

        if (!delegate) return;

        for (var idx = 0; idx < delegate.length; idx++) {
            var handler = delegate[idx];
            handler.func.apply(handler.context || window, paramsArray || []);
        } //for

        if (paramsArray && paramsArray.length) {
            var e = paramsArray[0];
            !e.cancelled && e.continuation && e.continuation();
        }
    };

    // Initalizes current events from given options block.
    me.loadEventOptions = function (optionsBlock) {
        var delegate, newHandler;

        if (!optionsBlock) {
            return;
        }

        for (var idx in optionsBlock) {
            newHandler = optionsBlock[idx];

            objToExtend.addListener(idx, newHandler, objToExtend);
        }
    };

    // Priveleged methods.
    // Adds new event handler.
    objToExtend.addListener = function (event, handler, context) {
        var delegate = eventHandlers[event];
        delegate && handler && delegate.push({ func: handler, context: context });
    };

    objToExtend.removeListener = function (event, handler) {
        var delegate = eventHandlers[event],
		newDelegate,
		currentHandler;

        if (delegate) {

            for (idx = 0; idx < delegate.length; idx++) {
                currentHandler = delegate[idx];
                currentHandler && currentHandler.func != handler && newDelegate.push(currentHandler);
            }

            eventHandlers[event] = newDelegate;
        }
    };
}

var instance = null;

// Form layout class
var FormLayout = lmpost.FormLayout = function () {
    var me = this,
	form,
	validationRules,

	errorUpdateState = function (element, errorClass, validClass, errorState) {
	    var elem = $(element),
			type = elem.attr('type'),
			func = errorState ? 'addClass' : 'removeClass',
			valFunc = errorState ? 'removeClass' : 'addClass';

	    if (elem.attr('readonly')) return;

	    if (type == 'radio' || type == 'checkbox') {
	        var wrapper = $(element).closest('div.b2c-radio-wrap');
	        wrapper[func](errorClass);
	        wrapper[valFunc](validClass);
	    } else {
	        elem[func](errorClass);
	        elem[valFunc](validClass);
	    }

	    me.validationStateCallback && me.validationStateCallback(element, !errorState);
	},

	errorHighlight = function (element, errorClass, validClass) {
	    errorUpdateState(element, errorClass, validClass, true);
	},

	errorUnHighlight = function (element, errorClass, validClass) {
	    errorUpdateState(element, errorClass, validClass, false);
	},

		 // Show message placeholder - For desktop.
   showPopup = function (title, data) {
       var dlg = $('<div class="pd-content-frame">Loading...</div>').dialog(
                     {
                         buttons: {
                             "Close": function () {
                                 $(this).dialog("close");
                                 $(this).dialog("destroy").remove();
                             }
                         },
                         "modal": true,
                         "width": 560,
                         "height": 420,
                         "dialogClass": 'b2c-dialog-text',
                         "title": title
                     });
       var dlgClose = function () {
           var frm = $('.pd-content-frame');
           frm.dialog('close');
           frm.remove();
       };

       data.appendButton = function (title, id, className, handler) {
           var buttons = dlg.dialog('option', 'buttons');
           buttons[title] = handler;
           dlg.dialog('option', 'buttons', buttons);
       };

       data.setFixedDialog = function () {
           $('.ui-widget-overlay').off('click');
       }

       data.CloseDialog = dlgClose;
       //$('.ui-widget-overlay').on('click', dlgClose);
       $('.pd-content-frame').data(data);
       return false;
   },

	// Show message placeholder -  For mobile platforms.
	showMobileDoc = function (title, data) {
	    var current = form.find('div.b2c-form:visible');
	    current.hide();
	    var docForm = '<div class="b2c-form b2c-step b2c-consent">' +
		'<p class="b2c-form-title">' + title + '</p><div class="pd-content-frame">Loading...</div>';

	    docForm += '</div>';
	    var locForm = $(docForm).insertAfter(current);
	    locForm.makevisible();

	    var dlgClose = function () {
	        var frm = $('.pd-content-frame');
	        current.show().makevisible();
	        locForm.remove();
	        frm.remove();
	    };

	    data.CloseDialog = dlgClose;

	    data.appendButton = function (title, id, className, handler) {
	        var ctr = locForm.find('div.b2c-form-action');
	        className = !className ? 'b2c-btn' : 'b2c-btn ' + className;
	        var btn = $('<div class="b2c-btn-row b2c-form-action"><div class="b2c-btn-wrap"><a id="' + id + '" href="#' + id + '" class="' + className + '">' + title + '</a></div></div>');

	        if (ctr.length) { btn.insertBefore(ctr); } else { locForm.append(btn); }
	        btn.find('a').on('click', handler);
	    };

	    data.setFixedDialog = function () {
	    };

	    data.appendButton('Close', 'dlgclose', 'b2c-btn-close-dlg', dlgClose);
	    $('.pd-content-frame').data(data);
	    return false;
	},

	initValidationRules = function () {
	    var methods =
			{
			    "regex": function (value, element, regexp) {
			        return this.optional(element) || regexp.test(value);
			    },
			    // Checkbox group validator.
			    "AtLeastOneCheckboxSelected": function (value, element) {
			        var root = $(element).closest('.b2c-check-group'),
					allCheckboxes = root.find(':checkbox').get().length;
			        if (!allCheckboxes) {
			            return true;
			        }
			        var val = root.find('input:checked').get().length;
			        return val != 0;
			    }
			};
	    // Always override std maxlength method!
	    $.validator.addMethod("maxlength", function (value, element, len) {
	        return value == "" || value.length <= len;
	    });

	    for (var valname in methods) {
	        if (!$.validator.methods[valname]) {
	            $.validator.addMethod(valname, methods[valname]);
	        }
	    }
	},

	initValidators = function () {

	    initValidationRules();

	    // Inputs and checkboxes inside required groups s/b required too.
	    form.find('div.b2c-required :input').addClass('b2c-required');
	    form.find('div.b2c-required :checkbox').each(function () {
	        var name = $(this).attr('name');
	        validationRules[name] = { "AtLeastOneCheckboxSelected": true };
	    }
		);


	    // Validate only agreement chechbox by default.
	    validationRules['accepted'] = { required: true };


	    // Apply required validation.
	    form.find('input[type!="hidden"][type!="checkbox"],select, textarea').each(
				function () {
				    var elem = $(this),
						name = elem.attr('name'),
						rule = validationRules[name];

				    if (!rule) {
				        rule = {};
				        validationRules[name] = rule;
				    }

				    if (elem.hasClass('b2c-required')) {
				        rule.required = true;
				        elem.on('change',
							function () {
							    if (!$(this).val()) {
							        $(this).valid();
							    }
							}
							);
				    }
				}
			);

	    form.find('.b2c-phone').each(
				function () {
				    var elem = $(this);
				    me.getRule(elem.attr('name')).ValidatePhone = true;
				}
			);

	    // Init base shared rules.
	    me.getRule('Email').email = true;


	    // Set validator defaults.
	    $.validator.setDefaults({ onkeyup: false });
	    $.validator.messages.required = "";

	    form.validate({
	        invalidHandler: function (form, validator) { },
	        errorClass: "b2c-error",
	        validClass: "b2c-valid",
	        errorPlacement: function (error, element) { },
	        'highlight': errorHighlight,
	        'unhighlight': errorUnHighlight,
	        rules: validationRules

	    });
	};

    lmpost.dialogPage = me.dialogPage = function (targetUrl, title, data) {
        data = data || {};//lmpost.mobile = true;
        (lmpost.mobile ? showMobileDoc : showPopup)(title, data);

        $.ajax(
		{
		    url: targetUrl.replace('../', lmpost.options.domain),
		    crossDomain: true,
		    dataType: "script"
		}
		);
    };

    me.getRule = function (name) {
        var rule = validationRules[name];

        if (!rule) {
            rule = {};
            validationRules[name] = rule;
        }

        return rule;
    };

    me.find = function (selector) {
        if (!form) {
            form = $('form.b2cform').first();
        }

        return form.find(selector);
    };

    me.byID = function (id) {
        return me.find('#' + id);
    };

    me.blinkErrorElements = function () {
        var colorArray = new Array("#fff", "#fde3e3", "#FFFFFF", "#fde3e3", "#FFFFFF", "#ffe9e9"),
			chain = $(':visible.b2c-error'),
			delays = [0, 150, 100, 100, 100, 100];

        for (var i = 1; i < 6; i++) {
            chain = chain.animate({ backgroundColor: colorArray[i] }, delays[i]);
        }

        chain = chain.animate({ backgroundColor: colorArray[0] }, 0,
			function () {
			    $(this).removeAttr('style');
			}
		);

        form.find('.b2c-error:first').parent().makevisible();
    };

    me.validateSubform = function (subform) {
        var valid = true;
        subform.find('input[type!="hidden"]:not("[disabled]"),select:not("[disabled]"),textarea:not("[disabled]")').each(
					function () {
					    var elem = $(this);

					    valid = (valid & (!elem.rules || $.isEmptyObject(elem.rules()) || elem.valid()));
					}
				);

        return valid;
    };

    me.Init = function (elem, rules) {
        form = elem;
        form.data('FormUI', me);
        form.show();


        // Init required selectors.
        var selectors = form.find('select.b2c-required option:first-child[value!=""]');
        selectors.removeAttr('selected');
        selectors.before('<option value="" selected>- Select -</option>');
        form.find('select.b2c-required option:first-child').attr('selected', 'selected');
        form.find('select.b2c-required').on('change',
				function () {
				    $(this).valid();
				}
			);

        // Init PIE - CSS3 support for oldier IEs.
        if (window.PIE) {
            form.find('.b2c-btn, .b2c-progress-bar-line').each(function () {
                PIE.attach(this);
            });
        }

        // Init custom parameters.
        if (lmpost.options) {
            $.each(['campaignid', 'campaignkey', 'leadtypeid', 'TestResult'],
				function (intIndex, objValue) {
				    lmpost.options[objValue] && $('<input>').attr({
				        type: 'hidden',
				        id: objValue,
				        name: objValue,
				        value: lmpost.options[objValue]
				    }).appendTo(form);
				});
        }

        form.find('input.b2c-required[type=text], select.b2c-required, div.b2c-required, textarea.b2c-required').after('<span class="b2c-req-marker">*</span>');

        // Check and init html 5 features.
        !($('#b2cOldIE').length) && form.find('.b2c-phone').each(function () {
            var orig = $(this);
            var upd = orig.clone();

            upd.attr("type", "tel");
            upd.insertBefore(orig);
            orig.remove();
        }
		);


        // Init validation and behavior.
        validationRules = rules;
        initValidators();

        // Init datepickers.
        form.find('input.b2c-date').each(
					function () {
					    var dp = $(this),
							name = dp.length && dp.attr('name'),
							fmt = dp.attr('formatdate') || lmpost.options.appOptions.formatdate || 'mm\/dd\/yy';

					    dp.length && dp.datepicker({
					        dateFormat: fmt,
					        yearRange: '-100:+1',
					        minDate: dp.attr('minDate'),
					        maxDate: dp.attr('maxDate'),
					        changeMonth: true,
					        changeYear: true,
					        onSelect:
								function () {
								    $(this).valid();
								}
					    }
							);
					}
		);

    }

    me.getTemplate = function (tplName, defValue) {
        var
			appOptions = lmpost.options && lmpost.options.appOptions,
			tplData;

        if (!appOptions) {
            return null;
        }

        if (!appOptions.templates) {
            return null;
        }

        tplData = appOptions.templates[tplName];

        if (tplData && tplData.remove) {
            return null;
        }

        return tplData || defValue;
    };
};

var FormUI = function () {
    var me = this,
	validationRules,
	form,
	layout,
	validationTimeout = 2000,
	prefix = '', //'b2c' + (new Date()).getTime() + '_', - disabled functionality
	urls,
	events = new EventHelper(
		{
		    acquireValidationState: [],
		    formReady: [],
		    formBeforeSubmit: [],
		    formStepChanging: [],
		    formSubmitError: []
		}, me),
	logMessages = [],


	// Private methods
	showStatus = function (title, message, closeButton) {
	    var id = prefix + 'msgDiv',
			elem = form.find('#' + id),
			msgHtml,
			lastForm = form.find('.b2c-form:not(.b2c-msg):last');

	    if (!elem.length) {
	        msgHtml = '<div class=\"b2c-form b2c-msg\" id=\"' + id + '\" style=\"display:none\">';
	        msgHtml += '<p class=\"b2c-form-title\"><\/p>';
	        msgHtml += '<div class=\"b2c-row b2c-form-thank\">';
	        msgHtml += '<p><\/p>';
	        msgHtml += '<\/div>';
	        msgHtml += '<div class=\"b2c-row b2c-form-action\"><div class=\"b2c-btn-wrap\"><a  class=\"b2c-btn\">Retry<\/a></div><\/div>';
	        lastForm.after(msgHtml);
	        elem = lastForm.next();


	        elem.find('.b2c-btn').on('click',
				function () {
				    submitFormStd();
				    return false;
				}
			);
	    }

	    elem.find('p.b2c-form-title').text(title).makevisible();
	    elem.find('div.b2c-form-thank > p').html(message);
	    var action = closeButton ? 'show' : 'hide';
	    elem.find('.b2c-form-action')[action]();

	    lastForm.hide();
	    elem.show();
	},




submitForm = function (success, error) {
    var customSuccess = lmpost.options.onSuccess,
			customError = lmpost.options.onError,
			beforeSendData = lmpost.options.onBeforeSend,
			options,
			lmopts = lmpost.options,
			hituidStr = lmpost.options.hituid ? '&uid=' + escape(lmopts.hituid) : '',
			startHandler = beforeSendData ? beforeSendData : function () { return {} }, // use dummy default start handler
			successHandler = function (data) {
			    if (lmopts.appOptions.noRedirect && data.Result == 0) // If still processing, just repeat the query until accepted or rejected.
			    {
			        var lid = data.LeadID;

			        options.url = lmpost.makeUrl(urls.submitUrl, true) + '?LeadID=' + lid + '&c=' + lmopts.campaignid + hituidStr + '&responsetype=json&ResponseAsync=1';

			        setTimeout(function () {
			            $.ajax(options);
			        }, 200);
			    }
			    else {
			        customSuccess ? customSuccess(data, success) : success(data);
			    }
			},
			errorHandler = customError ? function (result) { customError(result, error); } : error,

			query = form.find(':not(.b2c-dont-send)').serialize().replace(new RegExp(prefix, "g"), '');

    options = {
        // warning: api url must be always used for submission.
        url: lmpost.makeUrl(urls.submitUrl, true) + '?' + query + hituidStr + '&responsetype=json&ResponseAsync=1&clienturl=' + escape(location.href),
        success: successHandler,
        error: errorHandler,
        timeout: 300000,
        type: 'get',
        dataType: 'jsonp'
    };
    var
		appOptions = lmpost.options.appOptions,
		submitTemplate = layout.getTemplate("processingTemplate", 'The form is processed...');

    var e = {
        continuation: function (evtArgs) {
            showStatus('Please wait', submitTemplate, false);
            startHandler();
            $.ajax(options);
        }
    };
    events.fireEvent('formBeforeSubmit', [e]);

},

	submitFormStd = function () {
	    var
			appOptions = lmpost.options.appOptions;

	    submitForm(
							function (data) {

							    if (data && data.Result != 4) {

							        processConversion(data);

							        if (data.RedirectURL && !lmpost.options.appOptions.noRedirect) {
							            regFormEvent('form', 10, data.RedirectURL);
							            window.setTimeout(function () { window.location = data.RedirectURL }, 500);
							        }
							        else {
							            regFormEvent('form', 10, '');
							            var thankYouTemplate = layout.getTemplate("thankYouTemplate", 'Your application has been submitted.');
							            showStatus('Thank you!', thankYouTemplate);
							        }
							    }
							    else {
							        regFormEvent('form', 11, 0);
							        errorCallback(data);
							    }

							    return 0;
							}
							, errorCallback);
	},


	setError = function (field) {
	    var target = me.byID(field);
	    if (!target.length) {
	        target = form.find('[name$=_' + field + ']');
	        if (!target.length) {
	            return false; // no target element.
	        }
	    }

	    // For hidden inputs try to find visible elements.
	    // if (target.length)// && target.attr('type') == 'hidden') {
	    var displayFieldRef = target.attr('b2c-display-field'),
			dispFieldSet = displayFieldRef && target.closest('div').find(displayFieldRef),
			displayField = (dispFieldSet && dispFieldSet.length) ? dispFieldSet : me.byID(displayFieldRef);

	    if (displayField && displayField.length) {
	        target = displayField; // display field s/b highlighted instead of hidden data field.
	    }

	    if (!target.length) {
	        return false; // no target element.
	    }

	    var type = target.attr('type');
	    if (type == 'radio' || type == 'checkbox') {
	        target = target.closest('div.b2c-radio-wrap');
	    }

	    target.addClass('b2c-error').removeClass('b2c-valid');

	    target.each(function () {
	        acquireValidationState(this, false);
	    }
		);

	    return true;
	},

	// Adds conversion element the DOM if returned.
	processConversion = function (data) {
	    lmpost.processConversion(form, data);
	},

	// Std error callback.
errorCallback = function (result) {
    showStatus('Sorry', 'Please try again.', true);
    result && processConversion(result);

    events.fireEvent('formSubmitError');

    if (result && result.Errors && result.Errors.length) {
        $.each(result.Errors, function (index, value) {
            value.Field && setError(value.Field);
        }
			);

        // Find first step with error and show it. Otherwise only sorry message is shown.
        var invalidStep = form.find('.b2c-error:first').closest('.b2c-form');
        if (invalidStep.length) {
            form.find('.b2c-form:visible').hide();
            invalidStep.show();
            me.blinkErrorElements();
        }
    }
},

regFormEvent = function (s, evt, value) {
    $.ajax({ url: lmpost.actionUrl('regevent', { sender: s, event: evt, value: value }), type: 'get', dataType: 'jsonp' });
},

	goNext = function () {
	    delete me.isSubmitting;
	    if (!form.valid()) return;

	    var e = {
	        continuation: function (data) {

	            if (data && data.Result != 1) {
	                errorCallback(data);
	                return;
	            }

	            var current, next;
	            form.find('.b2c-form').each(
					function () {
					    var frm = $(this);

					    if (current) { // if current is already set it's next form.
					        if (!next) {
					            next = frm;
					            next.data('PrevForm', current);
					        }

					        return;
					    }

					    if (frm[0].style['display'] != 'none') {
					        current = frm;
					    }
					}
				);

	            current.hide();
	            if (next && next.length && !next.hasClass('b2c-msg')) // has other forms to fill
	            {
	                regFormEvent(next.attr('class'), 2);
	                next.show();
	                if (!next.find('.b2c-error').length) {
	                    $('.b2c-form:visible').makevisible();
	                }
	                // Whether the form has invalid elements from external source, highlight them.
	                me.blinkErrorElements();
	            }
	            else {
	                // There are no forms left... begin submission.
	                submitFormStd();
	            }
	        }
	    };
	    events.fireEvent('formStepChanging', [e]);


	    //if (e.cancelled) { return; };

	    //e.continuation();
	},

	onContinueButtonClick = function (e) {
	    var sender = (e && $(e.target).is('a')) ? $(e.target) : form.find('.b2c-form:visible a.b2c-btn:not(.b2c-btn-back)'),
				subform = sender.closest('.b2c-form'),
				valid = true,
				nextSubform = subform.next(':not(.b2c-msg)'),
				prevSubform;

	    // Process back button click.
	    if (sender.hasClass('b2c-btn-back')) {
	        delete me.isSubmitting; // cancel submit flag
	        form.find('span.b2c-btn-loader').hide();
	        prevSubform = subform.data('PrevForm');
	        subform.hide();
	        prevSubform.show();
	        return;
	    }

	    if (me.isSubmitting) {
	        return;
	    }

	    // Next button click.
	    valid = layout.validateSubform(subform);
	    var senderTag = subform.attr('class');

	    if (!valid) {
	        var validator = form.validate();
	        if (validator.pendingRequest) {
	            me.isSubmitting = true;
	            var loader = form.find('span.b2c-btn-loader');
	            // isSubmitting flag MUST be removed in any scenario after a while.
	            window.setTimeout(
						function () {
						    if (me.isSubmitting) {
						        delete me.isSubmitting;
						        loader.hide();

						        goNext();
						    }
						}, validationTimeout + 1000);
	            loader.show();
	        }
	        me.blinkErrorElements();
	        regFormEvent(senderTag, 1, '0');
	    }
	    else {
	        regFormEvent(senderTag, 1, '1');
	        goNext();
	    }
	    return false;
	},

	initCustomSettings = function () {
	    var config = window.LMEmbeddedFormConfig,
					formSelector = form.find('.b2c-form'),
					cfgButton;

	    if (config) {
	        config.formWidth && formSelector.css('width', config.formWidth);
	        config.backgroundColor && formSelector.css('background', config.backgroundColor);
	        cfgButton = config.continueButton;
	        if (cfgButton) {
	            cfgButton.backgroundColor && $('.b2c-btn').css('background', cfgButton.backgroundColor);
	        }
	        if (config.fontSize) {
	            form.css('font-size', config.fontSize);
	        }
	    }
	},

acquireValidationState = function (element, isValid) {
    events.fireEvent('acquireValidationState', [element, isValid]);
    if (!isValid) {
        regFormEvent($(element).attr('name'), 102, $(element).val());
    }
};

    lmpost.regFormEvent = regFormEvent;

    me.getUrl = function (key) {
        return urls[key];
    };
    //

    me.find = function (selector) {
        return layout.find(selector);
    };

    me.byID = function (id) {
        return layout.byID(id);
    };

    me.getTemplate = function (tplName, defValue) {
        return layout.getTemplate(tplName, defValue);
    }

    me.InitForm = function (rules) {
        // Init required urls from config.
        urls = lmpost.urls;


        form = $('form.b2cform').first();
        layout = new FormLayout();
        layout.Init(form, rules);
        layout.validationStateCallback = acquireValidationState;
        $('div.b2cloader').hide();

        var fldar = [];
        form.find('[name]').each(function (ind, el) { if (el.name) { fldar[el.name.toLowerCase()] = el.name; } });
        window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
            form.find('input[name=' + fldar[key.toLowerCase()] + ']').val(unescape(value));
            form.find('select[name=' + fldar[key.toLowerCase()] + ']').val(unescape(value));
        });


        // Attach continue buttons.
        form.find('.b2c-form').not(':last').append('<div class=\"b2c-row b2c-form-action\"><span style=\"display:none\" class=\"b2c-btn-loader\"><\/span><div class=\"b2c-btn-wrap\"><a class=\"b2c-btn\" tabindex=\"0\">Continue<\/a><\/div><\/div>');
        form.find('.b2c-form:last').append('<div class=\"b2c-row b2c-form-action\"><span style=\"display:none\" class=\"b2c-btn-loader\"><\/span><div class=\"b2c-btn-wrap\"><a  class=\"b2c-btn\" tabindex=\"0\">' +
		layout.getTemplate('btnFinish', 'Submit') +
		 '<\/a><\/div><\/div>');

        var backBtn = layout.getTemplate('btnBack');
        if (backBtn) {
            form.find('.b2c-form div.b2c-form-action').not(':first').prepend('<div class=\"b2c-btn-wrap\"><a  class=\"b2c-btn b2c-btn-back\">' + backBtn + '<\/a><\/div>');
        }

        // Form init
        form.find('.b2c-btn').on('click', onContinueButtonClick);
        $(document).on('keydown',
		function (e) {
		    if (e.keyCode == 13) {
		        onContinueButtonClick(e);
		        e.stopPropagation();
		        return false;
		    }
		}
	);


        initCustomSettings();

        form.find('input,select,textarea')
			.on('focus', function () { regFormEvent($(this).attr('name'), 100) })
			.on('blur', function () { regFormEvent($(this).attr('name'), 101) });

        events.fireEvent('formReady');

    };

    me.blinkErrorElements = function () {
        layout.blinkErrorElements();
    };

};

window.lmpost.getFormUI = function () {
    if (!instance) {
        instance = new FormUI();
    }

    return instance;
};

window.lmpost.cleanupFormUI = function () {
    instance = null;
};

window.lmpost.newFormUI = function () {
    return new FormUI();
};
