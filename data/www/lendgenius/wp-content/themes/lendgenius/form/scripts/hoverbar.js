﻿(function () {
	
	var ui = lmpost.getFormUI(),
		defaultMsg = 'Mouse over any field for hints to appear here',
		tipContainer,
		tips =  {
            RequestedAmount:'The amount you want deposited.',
            Email: 'A valid email address we can reach you at.',
            FirstName: 'Please enter your first name.',
            LastName: 'Please enter your last name.',
            PhoneHome: 'Your primary phone number.',
            PhoneAlt1: 'Your alternate phone number.',
            Address1: 'The address you currently live at.',
            ZipCode: 'The zip code you reside in.',
            SSN1: 'Your social security number.',
            DOB: 'The date you were born in.',
            DriversLicense: 'Your license or state ID number.',
            DriversLicenseState: 'The issuing state of your license or ID.',
            MonthsAtAddress: 'The amount of time you have lived at your residence.',
            OwnHome: 'Do you rent or own your residence?',
            ActiveMilitary: 'Are you or your dependent employed by the US Armed Forces?',
            
            IncomeType: 'What type of income do you receive?',
            MonthlyIncome: 'Your monthly take home after taxes.',
            EmployerName: 'What is the name of your employer?',
            MonthsEmployed: 'How long have you been with your current employer?',
            PhoneWork: 'Your work phone number.',
            DirectDeposit: 'I Receive Paycheck',
            PayFrequency: 'How often you get paid.',
            PayDate1: 'Next Payday',
            
            BankABA: 'Your ABA/Routing number.',
            BankName: 'The name of your current bank.',
            BankAccountNumber: 'Your bank account number.',
            BankAccountType: 'Type of bank account you have.',
            MonthsAtBank: 'How long have you been with your current bank?',
        };

	$('div.b2c-form>p.b2c-form-title:first-child').after(' <div class="b2c-message">Mouse over any field for hints to appear here.</div>  ');

	tipContainer = $('div.b2c-message');
		
	ui.addListener('formReady',
		function () {
			for(var idx in tips)
			{
				var msg = tips[idx],
					el = $('#'+idx);

				el.on('mouseover',
					function()
					{
						tipContainer.text(tips[$(this).attr('id')]);
					}
				);

				el.on('mouseout',
					function()
					{
						tipContainer.text(defaultMsg);
					}
				);
			}
		}
	);

	
	ui.addListener('formBeforeSubmit',
		function () {
			tipContainer.hide();
		}
		);

	ui.addListener('formSubmitError',
		function () {
			tipContainer.show();
		}
		);
})();
