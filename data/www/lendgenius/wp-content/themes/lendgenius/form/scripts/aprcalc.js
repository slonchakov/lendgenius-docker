﻿$(function () {

	var isNumeric = function (n) {
		return !isNaN(parseFloat(n)) && isFinite(n);
	};

	$('#r-calculate .btn-popup').on('click',
	function () {
		var container = $('#r-calculate'),
			amount = container.find('#op1').val(),
			fee = container.find('#op2').val(),
			days = container.find('#op3').val(),
			apr = lmpost.calculateAPR(amount, fee, days),
			result = '';

		if (apr > -1) {
			result = apr.toFixed(2);
		}
		else {
			$('#r-calculate [name*=op]').change();
			lmpost.blinkErrorElements();
		}

		container.find('#result').val(result);
		return false;
	}
	);

	$('#r-calculate [name*=op]').on('change',
	function () {
		var elem = $(this);

		elem.toggleClass('error', !isNumeric(elem.val()));
	}
);
});