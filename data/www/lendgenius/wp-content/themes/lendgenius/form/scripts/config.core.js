﻿lmpost.setupConfig = function () {

    var lmpost = window.lmpost,
        $ = window.jQuery;

    if (lmpost && lmpost.configCoreStarted) return;

    if (!lmpost) {
        if (window.LeadsB2C) {
            window.lmpost = window.LeadsB2C;
            window.lmpost.campaignid = window.LeadsB2C.campaignId;
        } else
            lmpost = window.lmpost = {};
        lmpost.options = {};
    }
    if (!lmpost.urls) lmpost.urls = {};

    if (!lmpost.urls.apiUrl) lmpost.urls.apiUrl = 'https://www.loanmatchingservice.com/';
    if (!lmpost.urls.submitUrl) lmpost.urls.submitUrl = 'post/live.aspx';
    if (!lmpost.urls.supportUrl) lmpost.urls.supportUrl = 'https://www.loanmatchingservice.com/misc/';
    if (!lmpost.urls.hitUrl) lmpost.urls.hitUrl = 'https://www.sparning.com/hit/hit.core.js';

    //if (!lmpost.urls.apiUrl) lmpost.urls.apiUrl = 'http://localhost:51541/';
    //if (!lmpost.urls.submitUrl) lmpost.urls.submitUrl = 'post/live.aspx';
    //if (!lmpost.urls.supportUrl) lmpost.urls.supportUrl = 'http://localhost:51541/misc/';
    //if (!lmpost.urls.hitUrl) lmpost.urls.hitUrl = 'http://localhost:51541/hit/hit.core.js';
    var scripts = ['../scripts/plugins/jquery.poshytip.min.js', '../scripts/searching.lenders.js'];
    var preJqScripts = [];

    if (!lmpost.pdcore) {
        preJqScripts.push('../scripts.core/payday.core.js');
    }

    var appOptions =
    {
        styles: ['/pd-2.css'], // Array of required stylesheets.
        formStartupFile: '/createform.js', // Form source file. 
        preJQScripts: new lmpost.loader().then(preJqScripts), // Sequence of required scripts to load BEFORE jq is loaded.
        postJQScripts: // Sequency of required scripts loaded after jq is loaded.
            new lmpost.loader()
                .then(
                    // Placeholders script is required for older MS IEs.
                    function () {
                        if (jQuery('#b2cOldIE').length) {
                            scripts.push('../scripts/plugins/jquery.placeholder-1.1.9.js');
                        };
                    }
                )
                .then(scripts)
                .then(function () {
                    if (jQuery('#b2cOldIE').length) {
                        jQuery('input[type=text], input[type=password], textarea').placeholder();
                    }
                }
                ),
        disclosures: // App disclosures config
        {
            dialogTitle: 'Terms and Conditions',
            url: '../content/agreement.html',
            template: 'I have read and understand the <a href="#" class="b2c-disclosures" id="disclosures">noproblemcash.com terms and conditions</a>.'
        },
        templates: // app customizable templates
        {
            thankYouTemplate: '', // Shown on success.
            btnFinish: 'Get Cash Now!',
            btnBack: { remove: true } // Btn back is removed.

        }
    };

    lmpost.configCoreStarted = true;
    lmpost.options.appOptions = appOptions;

};

lmpost.setupConfig();