/**
 * Created by slan on 04.11.16.
 */


var ppp = 10; // Post per page
var pppBlog = 15; // Post per page
var pageNumber = 1;
var pageNumberBlog = 1;


function load_posts(author){
    pageNumber++;
    var str = '&pageNumber=' + pageNumber + '&ppp=' + ppp + '&author=' + author + '&action=more_post_ajax';
    jQuery.ajax({
        type: "POST",
        dataType: "html",
        url: '/wp-admin/admin-ajax.php',
        data: str,
        success: function(data){
            var $data = jQuery.parseJSON(data);
            if ($data['content'].length) {
                jQuery("#more_posts").attr("disabled", false);
                jQuery("#ajax-posts").append($data['content']);
                //animationLoadedPost();
                jQuery('.matchheight').matchHeight();
                if($data['more'] == false){
                    jQuery("#more_posts").hide();
                }
                //.masonry('appended', $data, true);
                // if (jQuery('.list-posts-wrapper.masonry').length) {
                //     jQuery('.list-posts-wrapper.masonry').masonry({
                //          itemSelector: '.blog-item-sub',
                //          columnWidth: 20,
                //          layoutInstant: false,
                //          gutter: 30
                //     });
                //     jQuery('.matchheight').matchHeight();
                // }
            } else{
                jQuery("#more_posts").hide();
            }
        },
        error : function(jqXHR, textStatus, errorThrown) {
            $loader.html(jqXHR + " :: " + textStatus + " :: " + errorThrown);
        }

    });
    return false;
}

function load_posts_blog(){
    pageNumberBlog++;
    var str = '&pageNumber=' + pageNumberBlog + '&ppp=' + pppBlog + '&action=more_post_ajax_blog';
    jQuery.ajax({
        type: "POST",
        dataType: "html",
        url: '/wp-admin/admin-ajax.php',
        data: str,
        success: function(data){
            var $data = jQuery.parseJSON(data);
            if ($data['content'].length) {
                jQuery("#more_posts_blog").attr("disabled", false);
                jQuery("#ajax-posts").append($data['content']);
                //animationLoadedPost();
                jQuery('.matchheight').matchHeight();
                if($data['more'] == false){
                    jQuery("#more_posts_blog").hide();
                }
            } else{
                jQuery("#more_posts_blog").hide();
            }
        },
        error : function(jqXHR, textStatus, errorThrown) {
            $loader.html(jqXHR + " :: " + textStatus + " :: " + errorThrown);
        }

    });
    return false;
}

function animationLoadedPost() {
    wow = new WOW({
        boxClass: 'wow', 
        animateClass: 'animated', 
        offset: 130, 
        mobile: true, 
        live: true 
    });
    wow.init();
}

animationLoadedPost();
jQuery("#more_posts").on("click",function(event){ // When btn is pressed.
    event.preventDefault();
    jQuery("#more_posts").attr("disabled",true); // Disable the button, temp.
    load_posts(jQuery("#more_posts").data( "author" ));
});

jQuery("#more_posts_blog").on("click",function(event){ // When btn is pressed.
    event.preventDefault();
    jQuery("#more_posts_blog").attr("disabled",true); // Disable the button, temp.
    load_posts_blog();
});