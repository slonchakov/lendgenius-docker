import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { CurrencyMaskModule } from 'ng2-currency-mask';


import { GetCategoriesService } from './services/get-categories.service';
import { LoadingService } from './services/loading.service';

import { AppComponent } from './app.component';
import { LoanGridComponent } from './components/loan-grid/loan-grid.component';
import { FiltersComponent } from './components/filters/filters.component';
import { LoanAmountComponent } from './components/filters/filters/loan-amount.component';
import { AgeOfBusinessComponent } from './components/filters/filters/age-of-business.component';
import { AnnualRevenueComponent } from './components/filters/filters/annual-revenue.component';
import { CreditScoreComponent } from './components/filters/filters/credit-score.component';
import { IsProfitableComponent } from './components/filters/filters/is-profitable.component';

@NgModule({
  imports:      [ BrowserModule, FormsModule, HttpModule, CurrencyMaskModule ],
  declarations: [ AppComponent, LoanGridComponent, FiltersComponent, LoanAmountComponent, AgeOfBusinessComponent, AnnualRevenueComponent, CreditScoreComponent, IsProfitableComponent ],
  providers: [ GetCategoriesService, LoadingService ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
