function fixModal() {
    var mediaSm = 756,
        $wrapperCm = $('.wrapper-cm'),
        windowSize = $(window).width(),
        $body = $('body'),
        $heightHeaderOnMobile = $('.header-panel').height();

    onOpen = function () {
        if (windowSize > mediaSm) {
            return;
        }
        $body.addClass('fix-modal-ios');
        var ycoord = $(window).scrollTop();
        $wrapperCm.data('ycoord', ycoord);
        ycoord = ycoord * -1;
        $wrapperCm.css({
            'position': 'fixed',
            'left': '0',
            'right': '0',
            'top': ycoord + $heightHeaderOnMobile + 'px'

        });
    };
    onClosed = function () {
        if (windowSize > mediaSm) {
            return;
        }
        $body.removeClass('fix-modal-ios');
        $wrapperCm.css({
            'position': 'static',
            'top': 'auto',
            'left': 'auto',
            'right': 'auto',
            'bottom': 'auto'
        });
        $(window).scrollTop($wrapperCm.data('ycoord'));

    }
    $(document).on('click', 'form-app first-step button', function () {
        if ($('body').hasClass('modal-open')) {
            onOpen();
        }
    });
    $(document).on('click', 'popup-step', function () {
        setTimeout(function () {
            if (!$('body').hasClass('modal-open')) {
                onClosed();
            }
        }, 600);
    });
}
fixModal();