import { Component, Input,  AfterContentInit } from '@angular/core';


@Component({
    selector: 'credit-score',
    template: `<label>Credit Score</label>
               <select #CreditScore (change)="updateValue($event)" name="creditScore" class="form-control">
                    <option [value]="1">Excellent (720-850)</option>
                    <option [value]="2">Good (690-719)</option>
                    <option [value]="3">Average (630-689)</option>
                    <option [value]="4">Poor (350-629)</option>
               </select>`
})
export class CreditScoreComponent  {

    @Input() filtersValues;

    constructor () { }


    updateValue(event) {
        this.filtersValues[event.target.name] = event.target.value;
    }


    ngDoCheck() {  }


}
