import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import { CategoryItem } from '../components/loan-grid/abstract/category-item';
import { apiConfig } from '../configs/api-config';
import { LoadingService } from './loading.service';

import { Observable, Observer } from  'rxjs/Rx';
import { ReplaySubject } from 'rxjs';
import 'rxjs/add/operator/map';


@Injectable()
export class GetCategoriesService {


    categoriesObservable: ReplaySubject<any> = new ReplaySubject(1);


    constructor(private http: Http,
                private loadingService: LoadingService) { }


    getCategories(filtersValues?: Object) {
        this.loadingService.loading = true;
        let params, request;
        if (filtersValues !== undefined) {
            params = this.objectToParams(filtersValues);
            request = `${apiConfig.categories}?${params}`;
        } else {
            request = `${apiConfig.categories}`;
        }
        this.http.get(request).subscribe(response => {
            this.categoriesObservable.next(response);
        });
    }


    objectToParams(object) {
        return Object.keys(object).map((value) => {
            var objectValue =  object[value];
            return `${value}=${objectValue}`;
        }).join('&');
    }


}
