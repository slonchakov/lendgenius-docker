import { Component, Input,  AfterContentInit } from '@angular/core';


@Component({
    selector: 'is-profitable',
    template: `<label>Is Profitable?</label>
               <select #IsProfitable (change)="updateValue($event)" name="isProfitable" class="form-control">
                    <option [value]="1">Yes, for 2 years</option>
                    <option [value]="2">Yes, for 1 year</option>
                    <option [value]="3">No</option>
               </select>`
})
export class IsProfitableComponent  {

    @Input() filtersValues;

    constructor () { }


    updateValue(event) {
        this.filtersValues[event.target.name] = event.target.value;
    }


    ngDoCheck() {  }


}
