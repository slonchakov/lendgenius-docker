/**
 * Created by sergey on 14.08.17.
 */
jQuery(document).ready(function($){
    var $iframe = $('.content-blog p iframe, .loan-content p iframe');
    if (! $iframe.length || !$iframe.attr('src').match('youtube')) return;
    $iframe.each(function(){
        $(this).parent('p').addClass('video-container');
    })
});

// Carousel Table
jQuery(document).ready(function($){

    var tableComplex = $('.table-complex ');

    if ( !tableComplex.length ) return;

    var carouselContainerTpl = '<div class="table-carousel-wrapper"><div class="table-carousel-mobile">{carousel_slides}</div></div>',
        carouselSlideTpl     = '<div class="table-carousel-tr"><div class="col-lg-12">{carousel_slide}</div></div>',
        carouselThTpl        = '<div class="table-carousel-th">{content}</div>';



    tableComplex.addClass('table-sm').each(function(){
        var carouselSlides = '',
            thisTable      = $(this),
            tableHeaders   = thisTable.find('th'),
            tableBody      = thisTable.find('tbody');


        tableBody.find('tr').each(function(){
            var thisRow = $(this), carouselSlideContent = '';


            thisRow.find('td').each(function(index){
                var thisCell = $(this),
                    carouselTh = carouselThTpl;

                carouselSlideContent += carouselTh.replace('{content}', tableHeaders.eq(index).text());
                carouselSlideContent += thisCell.html();
            });

            var carouselSlide = carouselSlideTpl;
            carouselSlides += carouselSlide.replace('{carousel_slide}',carouselSlideContent);

        });

        var carouselContainer = carouselContainerTpl;
        carouselContainer = carouselContainer.replace('{carousel_slides}', carouselSlides);
        thisTable.after(carouselContainer);

    });

    jQuery('.table-carousel-mobile').each(function(){

        $(this).slick({
            dots: true,
            dotsClass: 'list-unstyled slick-dots-mobile',
            arrows: false
        });
    });
});

var CheckForm = {
    form: null,
    fields: [],
    hasError: false,
    checker: function()
    {
        var obj = this;
        obj.hasError = false;
        this.fields.forEach(function(item){
            $item = jQuery(item);
            if ($item.length && $item.val() == '' ) obj.hasError = true;
        });
        return this.hasError;
    }
};

jQuery(document).ready(function(){

    var formToCheck = jQuery('#comment_form');

    if (!formToCheck.length) return false;

    var CheckCommentForm = Object.create(CheckForm, {
        form: { value:formToCheck},
        fields: { value:['textarea','[name="author"]','[name="email"]']}
    });

    CheckCommentForm.form.find(CheckCommentForm.fields.join(",")).focus(function(){
        jQuery(this).removeClass('has-error');
    });

    CheckCommentForm.form.submit(function(e){

        if (CheckCommentForm.checker()) {
            CheckCommentForm.form.find(CheckCommentForm.fields.join(",")).addClass('has-error');
            e.preventDefault();
        }

    });
});