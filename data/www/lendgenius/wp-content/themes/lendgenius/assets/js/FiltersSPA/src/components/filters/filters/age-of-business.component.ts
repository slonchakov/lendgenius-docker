import { Component, Input,  AfterContentInit } from '@angular/core';


@Component({
    selector: 'age-of-business',
    template: `
               <label>Time In Business</label>
               <select #AgeOfBusiness (change)="updateValue($event)" name="ageOfBusiness" class="form-control">
                    <option [value]="1">Starting a business</option>
                    <option [value]="2">Less than 1 year</option>
                    <option [value]="3">1-2 years</option>
                    <option [value]="4">2+ years</option>
               </select>
               <span class="arrow-select"></span>`
})
export class AgeOfBusinessComponent  {

    @Input() filtersValues;

    constructor () { }


    updateValue(event) {
        this.filtersValues[event.target.name] = event.target.value;
    }


    ngDoCheck() {  }


}
