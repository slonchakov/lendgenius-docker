const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ENV = process.env.ENV = process.env.NODE_ENV;

const webpackConfig = {
    entry: {
        'filters': path.join(__dirname, '../src/main.ts')
    },
    resolve: {
        extensions: ['.ts', '.js', '.scss', '.less', '.html'],
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                enforce: "pre",
                exclude: /node_modules/,
                use: 'tslint-loader'
            },
            {
                test: /\.ts$/,
                use: [
                    'ts-loader',
                    'angular2-template-loader'
                ]
            },
            {
                test: /\.(png|gif|jpe?g|svg)$/i,
                use: 'file?hash=sha512&digest=hex&name=/img/[name]-[hash].[ext]'
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2)$/,
                use: 'file?name=/fonts/[name].[ext]'
            },
            {
                test: /\.ejs$/,
                use: 'ejs-loader'
            },
            {test: /\.html$/, loader: 'raw-loader'},
            {
                test: /\.(sass|scss)/,
                exclude: /node_modules/,
                use: [
                    'raw-loader',
                    'sass-loader',
                ]
            },
            {
                test: /\.less$/,
                use: [
                    'raw-loader',
                    'less-loader'
                ],
                exclude: /node_modules/
            }
        ]
    },
    plugins: [
        new webpack.LoaderOptionsPlugin({
            options: {
                configFileName: path.resolve(__dirname, '../src') + 'tsconfig.json'
            }
        }),
        new webpack.LoaderOptionsPlugin({
            options: {
                tslint: {
                    configFile: path.join(__dirname, '../tslint.json'),
                    emitErrors: true,
                    failOnHint: true,
                    typeCheck: false,
                    tsConfigFile: path.join(__dirname, '../tsconfig.json')
                },
            }
        }),
        new webpack.ContextReplacementPlugin(
            /angular(\\|\/)core(\\|\/)@angular/,
            path.resolve(__dirname, '../src'),
            {}
        ),
        new webpack.optimize.OccurrenceOrderPlugin(true),
        new HtmlWebpackPlugin({
            title: 'Filters SPA',
            baseUrl: '/filters.js',
            template: path.join(__dirname, '../common/index.ejs'),
            filename: path.join(__dirname, '../dist/index.html'),
            inject: false
        }),
        new webpack.DefinePlugin({
            'ENV': JSON.stringify(ENV),
            'process.env': {
                'ENV': JSON.stringify(ENV)
            }
        })
    ]
};


module.exports = webpackConfig;
