import { LoanItem } from './loan-item';


export class CategoryItem {
    category: string;
    description: string;
    items: Array<LoanItem>;
}
