const path = require('path');
const webpack = require('webpack');

module.exports = {
    devtool: 'eval-source-map',
    output: {
        path: path.resolve(__dirname, '../dist'),
        publicPath: '/',
        filename: '[name].js',
        sourceMapFilename: '[name].map'
    },
    devServer: {
        contentBase: path.join(__dirname, "../dist"),
        compress: true,
        historyApiFallback: true,
        stats: {
            colors: true
        },
        host: 'localhost',
        port: 9045,
        watchOptions: {
            aggregateTimeout: 300,
            poll: 1000
        }
    }
}
