export class LoanItem {
    name: string; 
    logo: string;
    apr: Array<number>;
    creditScore: number;
    redirectLink: string;
    qualifications: Array<string>;
    pros: Array<string>;
    cons: Array<string>;
}
