import { Component, Input, OnInit } from '@angular/core';
import { CategoryItem } from './abstract/category-item';
import { GetCategoriesService } from '../../services/get-categories.service';
import { LoadingService } from '../../services/loading.service';
import { mapCategories } from './helpers/categories-mapper';


@Component({
    selector: 'loan-grid',
    templateUrl: './loan-grid.component.html'
})
export class LoanGridComponent  {

    categories: Array<CategoryItem>;

    itemsState: Array<any>;

    emptyCollection = false;


    constructor (private getCategoriesService: GetCategoriesService,
                 private loadingService: LoadingService) {
        this.getCategoriesService.getCategories();
    }


    ngOnInit() {
        this.getCategoriesService.categoriesObservable.subscribe( data => {
            this.loadingService.loading = false;
            this.categories = data.json();
            this.isEmptyCollection();
            if (!this.emptyCollection) {
                this.categories = mapCategories(this.categories);
                this.mapToItemState(this.categories);
            }
        });
    }


    showHideItems(catIndex, itemIndex) {
        this.itemsState[catIndex][itemIndex] = ! this.itemsState[catIndex][itemIndex];
    }


    mapToItemState(categories) {
        if (categories.length > 0) {
            this.itemsState = categories.map(cat => {
                return cat.items.map((item, index) => {
                    return false;
                });
            });
            this.itemsState[0][0] = true;
        }
    }


    isEmptyCollection() {
        if (this.categories.length === 0) {
            this.emptyCollection = true;
        } else {
            this.emptyCollection = false;
        }
    }


    trackRedirect(eventLabel) {
        window['dataLayer'] = window['dataLayer'] || [];
        window['dataLayer'].push({
            'event' : 'FiltersSPARedirect',
            'eventCategory' : 'FiltersSPA',
            'eventAction' : eventLabel
        });
    }
}
