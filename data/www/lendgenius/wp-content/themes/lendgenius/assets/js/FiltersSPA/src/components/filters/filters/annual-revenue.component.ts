import { Component, Input,  AfterContentInit } from '@angular/core';


@Component({
    selector: 'annual-revenue',
    template: `<label>Annual Revenue</label>
               <select #AgeOfBusiness (change)="updateValue($event)" name="annualRevenue" class="form-control">
                    <option [value]="1">Less than $25,000</option>
                    <option [value]="2">$25,000 - $49,999</option>
                    <option [value]="3">$50,000 - $74,999</option>
                    <option [value]="4">$75,000 - $99,999</option>
                    <option [value]="5">$100,000 or more</option>
               </select>`
})
export class AnnualRevenueComponent  {

    @Input() filtersValues;

    constructor () { }


    updateValue(event) {
        this.filtersValues[event.target.name] = event.target.value;
    }


    ngDoCheck() {  }


}
