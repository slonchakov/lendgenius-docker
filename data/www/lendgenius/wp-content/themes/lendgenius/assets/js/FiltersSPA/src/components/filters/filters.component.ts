import { Component, Input, KeyValueDiffers, DoCheck, AfterContentInit } from '@angular/core';
import { GetCategoriesService } from '../../services/get-categories.service';
import { FiltersValues } from './abstract/filters';

@Component({
    selector: 'filters',
    templateUrl: './filters.component.html'
})
export class FiltersComponent  {

    differ: any;
    firstInit: boolean;
    filtersValues = FiltersValues;

    constructor (private differs: KeyValueDiffers,
                 private getCategoriesService: GetCategoriesService) {
        this.differ = differs.find(this.filtersValues).create(null);
        this.firstInit = true;
    }


    ngDoCheck() {
        const change = this.differ.diff(this.filtersValues);
        if (change && !this.firstInit) {
            this.getCategoriesService.getCategories(this.filtersValues);
        }
    }

    ngAfterContentInit() {
        this.firstInit = false;
    }


    getAllOptions() {
        this.getCategoriesService.getCategories();
    }


}
