/**
 * Created by sergey on 14.08.17.
 */
jQuery(document).ready(function(){
    var $iframe = $('.content-blog p iframe, .loan-content p iframe');
    if (! $iframe.length || !$iframe.attr('src').match('youtube')) return;
    $iframe.each(function(){
        $(this).parent('p').addClass('video-container');
    })
});