const merge = require('webpack-merge');

const configCommon = require('./webpack-config/webpack.config.common');
const configDev = require('./webpack-config/webpack.config.dev');
const configProd = require('./webpack-config/webpack.config.prod');
const configStage = require('./webpack-config/webpack.config.stage');

let config;


switch (process.env.NODE_ENV) {
    case 'prod':
    case 'production':
        config = merge(configCommon, configProd);
        break;
    case 'dev':
    case 'development':
    default:
        config = merge(configCommon, configDev);
        break;
    case 'stage':
        config = merge(configCommon, configStage);
        break;
}


module.exports = config;
