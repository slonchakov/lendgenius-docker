// script for loan-sub-page for additional sticky menu
if (document.querySelector('.subpage_menu') !== null) {
	(function () {
		var a = document.querySelector('#sticky_menu'), b = null, P = 0;
		window.addEventListener('scroll', Ascroll, false);
		document.body.addEventListener('scroll', Ascroll, false);
		function Ascroll() {
			if (b == null) {
				var Sa = getComputedStyle(a, ''), s = '';
				for (var i = 0; i < Sa.length; i++) {
					if (Sa[i].indexOf('overflow') == 0 || Sa[i].indexOf('padding') == 0 || Sa[i].indexOf('border') == 0 || Sa[i].indexOf('outline') == 0 || Sa[i].indexOf('box-shadow') == 0 || Sa[i].indexOf('background') == 0) {
						s += Sa[i] + ': ' + Sa.getPropertyValue(Sa[i]) + '; '
					}
				}
				b = document.createElement('div');
				b.style.cssText = s + ' box-sizing: border-box; width: ' + a.offsetWidth + 'px;';
				a.insertBefore(b, a.firstChild);
				var l = a.childNodes.length;
				for (var i = 1; i < l; i++) {
					b.appendChild(a.childNodes[1]);
				}
				a.style.height = b.getBoundingClientRect().height + 'px';
				a.style.padding = '0';
				a.style.border = '0';
			}
			var Ra = a.getBoundingClientRect(),
				R = Math.round(Ra.top + b.getBoundingClientRect().height - document.querySelector('#sticky_articles').getBoundingClientRect().bottom);  // Selector unit, when the lower edge of which you want to detach the adhesive element
			if ((Ra.top - P) <= 0) {
				if ((Ra.top - P) <= R) {
					b.className = 'stop';
					b.style.top = - R + 'px';
				} else {
					b.className = 'sticky';
					b.style.top = P + 64 + 'px';
				}
			} else {
				b.className = '';
				b.style.top = '';
			}
			window.addEventListener('resize', function () {
				a.children[0].style.width = getComputedStyle(a, '').width
			}, false);
		}
	})();

	jQuery(document).ready(function () {
		jQuery("#sticky_menu ul li a").on("click", "a", function (event) {
			event.preventDefault();
			var offsetTop = 120;
			var id = jQuery(this).attr('href'),
				top = jQuery(id).offset().top - offsetTop;
			jQuery('body,html').animate({ scrollTop: top }, 1500);
		});
	});

	jQuery(document).ready(function () {
		jQuery(window).scroll(function () {
			var y = jQuery(this).scrollTop();
			jQuery('#sticky_menu ul li a').each(function (event) {
				if (jQuery(jQuery(this).attr('href')).length) {
					if (y >= jQuery(jQuery(this).attr('href')).offset().top - 170) {
						jQuery('#sticky_menu ul li a').not(this).removeClass('active');
						jQuery(this).addClass('active');
					}
				}
			});
		});
	});

	// SMOOTH SCROLLING (

	jQuery(function () {
		jQuery('a[href*=#]:not([href=#])').click(function () {
			if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
				var target = jQuery(this.hash);
				var offsetTop = 150; //400 this fix
				var mq = window.matchMedia("(max-width: 767px)");
				target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');



				if (target.length) {
					if (mq.matches) {
						console.log('if');
						jQuery('html,body').animate({
							scrollTop: (target.offset().top - 380)
						}, 850);
						return false;
					} else {
						console.log('else');
						jQuery('html,body').animate({
							scrollTop: (target.offset().top - 140)
						}, 850);
						return false;
					}

				}
			}
		});
	});

} // end if
// END script

if (jQuery('.arrow-to-top').length)
	jQuery('.arrow-to-top').click(function (e) {
		e.preventDefault();
		jQuery("html, body").animate({ scrollTop: jQuery('body').offset().top }, "slow");
	});

//HOW IT WORKS
if (jQuery('.content-how-much, .how-much-form').length) {
	if (jQuery('#RequestedAmount').length)
		MoneyMask('#RequestedAmount', true);
	if (jQuery('#RequestedAmount2').length)
		MoneyMask('#RequestedAmount2', true);
	if (jQuery('#GrossSales').length)
		MoneyMask('#GrossSales', true);
	if (jQuery('#GrossSales2').length)
		MoneyMask('#GrossSales2', true);
}

// SWIPE
//jQuery(function(){
//  if (matchMedia) {
//    var mq = window.matchMedia( "(max-width: 768px)" );
//    mq.addListener(WidthChange);
//    WidthChange(mq);
//  }
//  function WidthChange(mq) {
//    if (mq.matches) {
//      var swipeArea = document.querySelector('.swipe-area');

//      Hammer(swipeArea).on("swipeleft", function() {
//            jQuery(swipeArea).addClass("menu-opened");
//      });

//      Hammer(swipeArea).on("swiperight", function() {
//            jQuery(swipeArea).removeClass("menu-opened");
//      });
//    }
//  }
//})

jQuery(document).ready(function () {
	var $document = jQuery(document),
		$element = jQuery('header.header-panel'),
		className = 'hasScrolled';

	$document.scroll(function () {
		if ($document.scrollTop() >= 30) {
			$element.addClass(className);

		} else {
			$element.removeClass(className);
		}
	});
});


jQuery(document).ready(function () {
	jQuery('.share-quote').hover(function () {
		$this = jQuery(this).find('.share-quote-resource');
		var a = $this.position().left;
		if (a <= 15) {
			$this.attr('data-placement', 'bottom');
		} else {
			$this.attr('data-placement', 'right');
		}
	});
});


// page init
jQuery(function () {
	initCycleCarousel();
	initSameHeight();

	jQuery('[type="reset"]').click(function (e) {
		setTimeout(function () {
			//jQuery('select.field-styler-option').trigger('refresh');
		}, 1);
		e.preventDefault();
	});

	//jQuery('select.field-styler-option').styler().trigger('refresh');

	jQuery('.header-panel-wraper .btn-link').click(function () {
		jQuery('body').toggleClass('menu-opened');
		closeDropMenu();
		jQuery('.ovarlay').click(function () {
			jQuery('body').removeClass('menu-opened');
		});

	});

	function closeMainMenu() {
		jQuery('.header-panel').find('.main-menu').slideUp();
		jQuery('body').removeClass('menu-opened');
	}


	jQuery('.header-panel .btn-burger').click(function (e) {
		e.preventDefault();
		e.stopImmediatePropagation();
		jQuery('.header-panel').find('.main-menu').slideToggle();
	});



	if (jQuery('body').hasClass("menu-opened")) {
		jQuery('html').css('overflow-y', 'hidden');
	}
	else {
		jQuery('html').css('overflow-y', 'auto');
	};

	jQuery('.header-panel-wraper .main-menu .close').click(function () {
		jQuery('body').removeClass('menu-opened');
		jQuery('.ovarlay').hide();
	});


	var fadeTime = 550,
		holderSubmenu = jQuery('.header-panel-wraper ul li.menu-item-has-children');

	function closeDropMenu() {
		var mq = window.matchMedia("(max-width: 767px)");
		if (mq.matches) {
			holderSubmenu.find('.sub-menu').slideUp(fadeTime);
			holderSubmenu.removeClass('opened');
			return;
		}

		holderSubmenu.find('.sub-menu').fadeOut(fadeTime);
		holderSubmenu.removeClass('opened');
	}


	jQuery(document).click(function () {
		closeMainMenu();
		closeDropMenu();
	});

	jQuery('.main-menu ul li.menu-item-has-children>a').click(function (e) {
		e.preventDefault(); //fix js S.N
		e.stopImmediatePropagation();

		var holderSubmenuThis = jQuery(this).parent();
		var mq = window.matchMedia("(max-width: 767px)");
		if (mq.matches) {
			//mobile animations menu
			if (holderSubmenuThis.is('.opened')) {
				holderSubmenuThis.removeClass('opened');
				holderSubmenuThis.find('.sub-menu').slideUp(fadeTime);
				return;
			}

			holderSubmenu.removeClass('opened');
			holderSubmenu.find('.sub-menu').slideUp(fadeTime);
			holderSubmenuThis.addClass('opened');

			if (holderSubmenuThis.is('.opened')) {
				holderSubmenuThis.find('.sub-menu').slideDown(fadeTime);
			}
			return;
		}
		//desktop animations menu
		fadeTime = 350;
		if (holderSubmenuThis.is('.opened')) {
			holderSubmenuThis.removeClass('opened');
			holderSubmenuThis.find('.sub-menu').fadeOut(fadeTime);
			return;
		}

		holderSubmenu.removeClass('opened');
		holderSubmenu.find('.sub-menu').fadeOut(fadeTime);
		holderSubmenuThis.addClass('opened');

		if (holderSubmenuThis.is('.opened')) {
			holderSubmenuThis.find('.sub-menu').fadeIn(fadeTime);
		}

	});

	jQuery('.menu-main-menu-container .sub-menu,.menu-header-menu-container .sub-menu,.menu-main-menu-container .menu').click(function (e) {
		e.stopImmediatePropagation();
	});
	jQuery('.menu-header-menu-container .menu-item-has-children>a').click(function (e) {
		e.preventDefault();
		e.stopImmediatePropagation();
		closeMainMenu();
		var holderSubmenuThis = jQuery(this).parent();
		fadeTime = 350;
		if (holderSubmenuThis.is('.opened')) {
			holderSubmenuThis.removeClass('opened');
			holderSubmenuThis.find('.sub-menu').fadeOut(fadeTime);
			return;
		}

		holderSubmenu.removeClass('opened');
		holderSubmenu.find('.sub-menu').fadeOut(fadeTime);
		holderSubmenuThis.addClass('opened');

		if (holderSubmenuThis.is('.opened')) {
			holderSubmenuThis.find('.sub-menu').fadeIn(fadeTime);
		}


	});

	jQuery('.subpage_menu .mobile_menu_icon').click(function () {
		jQuery('.subpage_menu .menu').slideToggle("slow");
	});
	jQuery('.subpage_menu .menu li a').click(function () {
		var mq = window.matchMedia("(max-width: 767px)");
		if (mq.matches) {
			jQuery('.subpage_menu .menu').slideToggle('slow');
		}

	});

	jQuery('.sub-menu.col-2 .col-holder').each(function () {
		$this = jQuery(this);
		if (!$this.children('.col').length > 0) {
			$this.parent().removeClass('col-2');
		}
	});
	jQuery('.matchheight').matchHeight();
	jQuery('.matchheight-row').matchHeight({
		byRow : false
	});


	function simpleAccordion() {
		jQuery('.accordion-header').click(function () {
			var $this = jQuery(this),
				accordionHolder = $this.parent(),
				accordionBody = accordionHolder.find('.accordion-body');
			accordionHolder.toggleClass('open');
			accordionBody.slideToggle();
		});
	}

	function cardPanelFix() {

		if (!jQuery('.b-cards-description').length) {
			return;
		}

		var panelPosition, heightPanel,
			windowPosition, panelItem,
			panelHolderHeight, ifValue,
			headerHeight, cardsDeskHolder;
		//clone = jQuery('.panel-cards_clone');

		panelItem = jQuery('.panel-cards');

		function refreshVar() {

			panelPosition = jQuery('.b-cards').offset().top;
			heightPanel = jQuery('.panel-cards').outerHeight(true);
			panelHolderHeight = jQuery('.b-cards').outerHeight(true);
			headerHeight = jQuery('.header-panel').outerHeight(true);

			ifValue = panelPosition + panelHolderHeight;

		}

		refreshVar();

		jQuery('<div class="panel-cards_clone" />').insertBefore('.panel-cards').css('height', heightPanel).hide();

		jQuery(window).resize(function () {

			refreshVar();

		});



		jQuery(window).scroll(function () {

			windowPosition = jQuery(window).scrollTop();

			if (windowPosition >= ifValue) {

				panelItem.addClass('panel-cards_fixed');

				jQuery('.panel-cards_toggle').addClass('show');

				jQuery('.panel-cards_clone').show();


			} else {

				panelItem.removeClass('panel-cards_fixed');

				jQuery('.panel-cards_toggle').removeClass('show');

				jQuery('.panel-cards_clone').hide();

			}

		});



		jQuery('.panel-cards a[data-scroll]').on('click', function (e) {

			if (jQuery('.panel-cards').is('.panel-cards_fixed')) {

				jQuery('.panel-cards').removeClass('show');
				jQuery('.panel-cards_toggle').find('span').addClass('on').html('Browse Cards');

				var scrollAnchor = jQuery(this).attr('data-scroll'),
					scrollPoint = jQuery('article[data-anchor="' + scrollAnchor + '"]').offset().top - (headerHeight + 91 + 40);

				jQuery('body,html').animate({
					scrollTop: scrollPoint
				}, 500);

				return false;

			} else {
				e.preventDefault();
			}


		})

	}
	function hightlightCard() {
		jQuery(window).scroll(function () {
			var windscroll = jQuery(window).scrollTop(),
				cardsDeskHolder = jQuery('.b-cards-description').offset().top;

			if (windscroll >= cardsDeskHolder) {

				jQuery('.card-article').each(function (i) {
					if (jQuery(this).position().top <= windscroll + 200) {
						jQuery('.b-card a[data-scroll]').parent().removeClass('active');
						jQuery('.b-card a[data-scroll]').eq(i).parent().addClass('active');
					}
				});

			}

		}).scroll();
	}

	function commentBoxToggle(){
		slideDuration = 350; //ms

		jQuery('.comment-form-toggle').click(function(){
			$this = jQuery(this);
			$this.fadeOut(slideDuration - 30);

			$this.siblings('.comment-form-holder').slideDown(slideDuration, function(){
				jQuery(this).find('textarea').focus();
			});
			
		});
	}
	commentBoxToggle();

	simpleAccordion();
	cardPanelFix();


	jQuery('.panel-cards_toggle').click(function () {
		$this = jQuery(this);
		txtContent = $this.find('span');

		jQuery('.panel-cards.panel-cards_fixed').toggleClass('show');

		if (jQuery('.panel-cards.panel-cards_fixed').is('.show')) {

			txtContent.html('Close');
			txtContent.removeClass('on');

		} else {

			txtContent.html('Browse Cards');
			txtContent.addClass('on');

		}

	});

	jQuery(window).resize(function () {

		jQuery('.panel-cards_clone').remove();
		jQuery('.panel-cards').removeClass('panel-cards_fixed');

		cardPanelFix();

	});

	if (jQuery('.b-cards-description').length) {
		hightlightCard();
	}
});

// align blocks height
function initSameHeight() {
	jQuery('.info-row').sameHeight({
		elements: 'h3',
		flexible: true,
		multiLine: true,
		biggestHeight: true
	});
}

function initSameHeight() {
	jQuery('.content-our-values .items-list').sameHeight({
		elements: '.item',
		flexible: true,
		multiLine: true,
		biggestHeight: true
	});
}

// cycle scroll gallery init
function initCycleCarousel() {
	jQuery('div.cycle-gallery.gallery').scrollAbsoluteGallery({
		mask: 'div.mask',
		slider: 'div.slideset',
		slides: 'div.slide',
		generatePagination: '.pagination',
		stretchSlideToMask: true,
		pauseOnHover: true,
		maskAutoSize: true,
		autoRotation: false,
		switchTime: 3000,
		animSpeed: 500
	});
}


function initSlickCarousel() {
	jQuery('.finance_products-slick').slick({
		mobileFirst: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: true,
		arrows: false,
		dotsClass: 'list-unstyled slick-dots-mobile',
		prevArrow: jQuery(".slick-prev"),
		nextArrow: jQuery(".slick-next"),

		responsive: [{
			breakpoint: 510,
			settings: {
				arrows: true,
				dotsClass: 'slick-counter',
				customPaging: function (slider, i) {
					//console.log(slider);
					return 'PAGE ' + (i + 1) + ' / ' + slider.slideCount;
				}
			}
		}]
	});
}

initSlickCarousel();

function initSlickCarouselTable() {
	jQuery('.table-carousel-mobile').slick({
		dots: true,
		dotsClass: 'list-unstyled slick-dots-mobile',
		arrows: false
	});

};

initSlickCarouselTable();

/*
 * jQuery SameHeight plugin
 */
; (function ($) {


	/* ----------------------------------------------
	padding-top for body element == header height
	----------------------------------------------- */

	var bumpIt = function () {
		jQuery('body').css('padding-top', jQuery('.header-panel').outerHeight(true));
	},
		didResize = false;

	jQuery(window).resize(function () {
		didResize = true;
	});
	setInterval(function () {
		if (didResize) {
			didResize = false;
			bumpIt();
		}
	}, 250);

	jQuery(document).ready(function () {
		bumpIt();
	});


	/* ----------------------------------------------
	END script for body padding
	----------------------------------------------- */

	$.fn.sameHeight = function (opt) {
		var options = $.extend({
			skipClass: 'same-height-ignore',
			leftEdgeClass: 'same-height-left',
			rightEdgeClass: 'same-height-right',
			elements: '>*',
			flexible: false,
			multiLine: false,
			useMinHeight: false,
			biggestHeight: false
		}, opt);
		return this.each(function () {
			var holder = jQuery(this), postResizeTimer, ignoreResize;
			var elements = holder.find(options.elements).not('.' + options.skipClass);
			if (!elements.length) return;

			// resize handler
			function doResize() {
				elements.css(options.useMinHeight && supportMinHeight ? 'minHeight' : 'height', '');
				if (options.multiLine) {
					// resize elements row by row
					resizeElementsByRows(elements, options);
				} else {
					// resize elements by holder
					resizeElements(elements, holder, options);
				}
			}
			doResize();

			// handle flexible layout / font resize
			var delayedResizeHandler = function () {
				if (!ignoreResize) {
					ignoreResize = true;
					doResize();
					clearTimeout(postResizeTimer);
					postResizeTimer = setTimeout(function () {
						doResize();
						setTimeout(function () {
							ignoreResize = false;
						}, 10);
					}, 100);
				}
			};

			// handle flexible/responsive layout
			if (options.flexible) {
				jQuery(window).bind('resize orientationchange fontresize', delayedResizeHandler);
			}

			// handle complete page load including images and fonts
			jQuery(window).bind('load', delayedResizeHandler);
		});
	};

	// detect css min-height support
	var supportMinHeight = typeof document.documentElement.style.maxHeight !== 'undefined';

	// get elements by rows
	function resizeElementsByRows(boxes, options) {
		var currentRow = jQuery(), maxHeight, maxCalcHeight = 0, firstOffset = boxes.eq(0).offset().top;
		boxes.each(function (ind) {
			var curItem = jQuery(this);
			if (curItem.offset().top === firstOffset) {
				currentRow = currentRow.add(this);
			} else {
				maxHeight = getMaxHeight(currentRow);
				maxCalcHeight = Math.max(maxCalcHeight, resizeElements(currentRow, maxHeight, options));
				currentRow = curItem;
				firstOffset = curItem.offset().top;
			}
		});
		if (currentRow.length) {
			maxHeight = getMaxHeight(currentRow);
			maxCalcHeight = Math.max(maxCalcHeight, resizeElements(currentRow, maxHeight, options));
		}
		if (options.biggestHeight) {
			boxes.css(options.useMinHeight && supportMinHeight ? 'minHeight' : 'height', maxCalcHeight);
		}
	}

	// calculate max element height
	function getMaxHeight(boxes) {
		var maxHeight = 0;
		boxes.each(function () {
			maxHeight = Math.max(maxHeight, jQuery(this).outerHeight());
		});
		return maxHeight;
	}

	// resize helper function
	function resizeElements(boxes, parent, options) {
		var calcHeight;
		var parentHeight = typeof parent === 'number' ? parent : parent.height();
		boxes.removeClass(options.leftEdgeClass).removeClass(options.rightEdgeClass).each(function (i) {
			var element = jQuery(this);
			var depthDiffHeight = 0;
			var isBorderBox = element.css('boxSizing') === 'border-box' || element.css('-moz-box-sizing') === 'border-box' || element.css('-webkit-box-sizing') === 'border-box';

			if (typeof parent !== 'number') {
				element.parents().each(function () {
					var tmpParent = jQuery(this);
					if (parent.is(this)) {
						return false;
					} else {
						depthDiffHeight += tmpParent.outerHeight() - tmpParent.height();
					}
				});
			}
			calcHeight = parentHeight - depthDiffHeight;
			calcHeight -= isBorderBox ? 0 : element.outerHeight() - element.height();

			if (calcHeight > 0) {
				element.css(options.useMinHeight && supportMinHeight ? 'minHeight' : 'height', calcHeight);
			}
		});
		boxes.filter(':first').addClass(options.leftEdgeClass);
		boxes.filter(':last').addClass(options.rightEdgeClass);
		return calcHeight;
	}
}(jQuery));

/*
 * jQuery FontResize Event
 */
jQuery.onFontResize = (function ($) {
	jQuery(function () {
		var randomID = 'font-resize-frame-' + Math.floor(Math.random() * 1000);
		var resizeFrame = jQuery('<iframe>').attr('id', randomID).addClass('font-resize-helper');

		// required styles
		resizeFrame.css({
			width: '100em',
			height: '10px',
			position: 'absolute',
			borderWidth: 0,
			top: '-9999px',
			left: '-9999px'
		}).appendTo('body');

		// use native IE resize event if possible
		if (window.attachEvent && !window.addEventListener) {
			resizeFrame.bind('resize', function () {
				$.onFontResize.trigger(resizeFrame[0].offsetWidth / 100);
			});
		}
		// use script inside the iframe to detect resize for other browsers
		else {
			var doc = resizeFrame[0].contentWindow.document;
			doc.open();
			doc.write('<scri' + 'pt>window.onload = function(){var em = parent.jQuery("#' + randomID + '")[0];window.onresize = function(){if(parent.jQuery.onFontResize){parent.jQuery.onFontResize.trigger(em.offsetWidth / 100);}}};</scri' + 'pt>');
			doc.close();
		}
		jQuery.onFontResize.initialSize = resizeFrame[0].offsetWidth / 100;
	});
	return {
		// public method, so it can be called from within the iframe
		trigger: function (em) {
			jQuery(window).trigger("fontresize", [em]);
		}
	};
}(jQuery));

/*
 * jQuery Cycle Carousel plugin
 */
; (function ($) {
	function ScrollAbsoluteGallery(options) {
		this.options = $.extend({
			activeClass: 'active',
			mask: 'div.slides-mask',
			slider: '>ul',
			slides: '>li',
			btnPrev: '.btn-prev',
			btnNext: '.btn-next',
			pagerLinks: 'ul.pager > li',
			generatePagination: false,
			pagerList: '<ul>',
			pagerListItem: '<li><a href="#"></a></li>',
			pagerListItemText: 'a',
			galleryReadyClass: 'gallery-js-ready',
			currentNumber: 'span.current-num',
			totalNumber: 'span.total-num',
			maskAutoSize: false,
			autoRotation: false,
			pauseOnHover: false,
			stretchSlideToMask: false,
			switchTime: 3000,
			animSpeed: 500,
			handleTouch: true,
			swipeThreshold: 15,
			vertical: false
		}, options);
		this.init();
	}
	ScrollAbsoluteGallery.prototype = {
		init: function () {
			if (this.options.holder) {
				this.findElements();
				this.attachEvents();
				this.makeCallback('onInit', this);
			}
		},
		findElements: function () {
			// find structure elements
			this.holder = jQuery(this.options.holder).addClass(this.options.galleryReadyClass);
			this.mask = this.holder.find(this.options.mask);
			this.slider = this.mask.find(this.options.slider);
			this.slides = this.slider.find(this.options.slides);
			this.btnPrev = this.holder.find(this.options.btnPrev);
			this.btnNext = this.holder.find(this.options.btnNext);

			// slide count display
			this.currentNumber = this.holder.find(this.options.currentNumber);
			this.totalNumber = this.holder.find(this.options.totalNumber);

			// create gallery pagination
			if (typeof this.options.generatePagination === 'string') {
				this.pagerLinks = this.buildPagination();
			} else {
				this.pagerLinks = this.holder.find(this.options.pagerLinks);
			}

			// define index variables
			this.sizeProperty = this.options.vertical ? 'height' : 'width';
			this.positionProperty = this.options.vertical ? 'top' : 'left';
			this.animProperty = this.options.vertical ? 'marginTop' : 'marginLeft';

			this.slideSize = this.slides[this.sizeProperty]();
			this.currentIndex = 0;
			this.prevIndex = 0;

			// reposition elements
			this.options.maskAutoSize = this.options.vertical ? false : this.options.maskAutoSize;
			if (this.options.vertical) {
				this.mask.css({
					height: this.slides.innerHeight()
				});
			}
			if (this.options.maskAutoSize) {
				this.mask.css({
					height: this.slider.height()
				});
			}
			this.slider.css({
				position: 'relative',
				height: this.options.vertical ? this.slideSize * this.slides.length : '100%'
			});
			this.slides.css({
				position: 'absolute'
			}).css(this.positionProperty, -9999).eq(this.currentIndex).css(this.positionProperty, 0);
			this.refreshState();
		},
		buildPagination: function () {
			var pagerLinks = jQuery();
			if (!this.pagerHolder) {
				this.pagerHolder = this.holder.find(this.options.generatePagination);
			}
			if (this.pagerHolder.length) {
				this.pagerHolder.empty();
				this.pagerList = jQuery(this.options.pagerList).appendTo(this.pagerHolder);
				for (var i = 0; i < this.slides.length; i++) {
					jQuery(this.options.pagerListItem).appendTo(this.pagerList).find(this.options.pagerListItemText).text(i + 1);
				}
				pagerLinks = this.pagerList.children();
			}
			return pagerLinks;
		},
		attachEvents: function () {
			// attach handlers
			var self = this;
			if (this.btnPrev.length) {
				this.btnPrevHandler = function (e) {
					e.preventDefault();
					self.prevSlide();
				};
				this.btnPrev.click(this.btnPrevHandler);
			}
			if (this.btnNext.length) {
				this.btnNextHandler = function (e) {
					e.preventDefault();
					self.nextSlide();
				};
				this.btnNext.click(this.btnNextHandler);
			}
			if (this.pagerLinks.length) {
				this.pagerLinksHandler = function (e) {
					e.preventDefault();
					self.numSlide(self.pagerLinks.index(e.currentTarget));
				};
				this.pagerLinks.click(this.pagerLinksHandler);
			}

			// handle autorotation pause on hover
			if (this.options.pauseOnHover) {
				this.hoverHandler = function () {
					clearTimeout(self.timer);
				};
				this.leaveHandler = function () {
					self.autoRotate();
				};
				this.holder.bind({ mouseenter: this.hoverHandler, mouseleave: this.leaveHandler });
			}

			// handle holder and slides dimensions
			this.resizeHandler = function () {
				if (!self.animating) {
					if (self.options.stretchSlideToMask) {
						self.resizeSlides();
					}
					self.resizeHolder();
					self.setSlidesPosition(self.currentIndex);
				}
			};
			jQuery(window).bind('load resize orientationchange', this.resizeHandler);
			if (self.options.stretchSlideToMask) {
				self.resizeSlides();
			}

			// handle swipe on mobile devices
			if (this.options.handleTouch && window.Hammer && this.mask.length && this.slides.length > 1 && isTouchDevice) {
				this.swipeHandler = new Hammer.Manager(this.mask[0]);
				this.swipeHandler.add(new Hammer.Pan({
					direction: self.options.vertical ? Hammer.DIRECTION_VERTICAL : Hammer.DIRECTION_HORIZONTAL,
					threshold: self.options.swipeThreshold
				}));

				this.swipeHandler.on('panstart', function () {
					if (self.animating) {
						self.swipeHandler.stop();
					} else {
						clearTimeout(self.timer);
					}
				}).on('panmove', function (e) {
					self.swipeOffset = -self.slideSize + e[self.options.vertical ? 'deltaY' : 'deltaX'];
					self.slider.css(self.animProperty, self.swipeOffset);
					clearTimeout(self.timer);
				}).on('panend', function (e) {
					if (e.distance > self.options.swipeThreshold) {
						if (e.offsetDirection === Hammer.DIRECTION_RIGHT || e.offsetDirection === Hammer.DIRECTION_DOWN) {
							self.nextSlide();
						} else {
							self.prevSlide();
						}
					} else {
						var tmpObj = {};
						tmpObj[self.animProperty] = -self.slideSize;
						self.slider.animate(tmpObj, { duration: self.options.animSpeed });
						self.autoRotate();
					}
					self.swipeOffset = 0;
				});
			}

			// start autorotation
			this.autoRotate();
			this.resizeHolder();
			this.setSlidesPosition(this.currentIndex);
		},
		resizeSlides: function () {
			this.slideSize = this.mask[this.options.vertical ? 'height' : 'width']();
			this.slides.css(this.sizeProperty, this.slideSize);
		},
		resizeHolder: function () {
			if (this.options.maskAutoSize) {
				this.mask.css({
					height: this.slides.eq(this.currentIndex).outerHeight(true)
				});
			}
		},
		prevSlide: function () {
			if (!this.animating && this.slides.length > 1) {
				this.direction = -1;
				this.prevIndex = this.currentIndex;
				if (this.currentIndex > 0) this.currentIndex--;
				else this.currentIndex = this.slides.length - 1;
				this.switchSlide();
			}
		},
		nextSlide: function (fromAutoRotation) {
			if (!this.animating && this.slides.length > 1) {
				this.direction = 1;
				this.prevIndex = this.currentIndex;
				if (this.currentIndex < this.slides.length - 1) this.currentIndex++;
				else this.currentIndex = 0;
				this.switchSlide();
			}
		},
		numSlide: function (c) {
			if (!this.animating && this.currentIndex !== c && this.slides.length > 1) {
				this.direction = c > this.currentIndex ? 1 : -1;
				this.prevIndex = this.currentIndex;
				this.currentIndex = c;
				this.switchSlide();
			}
		},
		preparePosition: function () {
			// prepare slides position before animation
			this.setSlidesPosition(this.prevIndex, this.direction < 0 ? this.currentIndex : null, this.direction > 0 ? this.currentIndex : null, this.direction);
		},
		setSlidesPosition: function (index, slideLeft, slideRight, direction) {
			// reposition holder and nearest slides
			if (this.slides.length > 1) {
				var prevIndex = (typeof slideLeft === 'number' ? slideLeft : index > 0 ? index - 1 : this.slides.length - 1);
				var nextIndex = (typeof slideRight === 'number' ? slideRight : index < this.slides.length - 1 ? index + 1 : 0);

				this.slider.css(this.animProperty, this.swipeOffset ? this.swipeOffset : -this.slideSize);
				this.slides.css(this.positionProperty, -9999).eq(index).css(this.positionProperty, this.slideSize);
				if (prevIndex === nextIndex && typeof direction === 'number') {
					var calcOffset = direction > 0 ? this.slideSize * 2 : 0;
					this.slides.eq(nextIndex).css(this.positionProperty, calcOffset);
				} else {
					this.slides.eq(prevIndex).css(this.positionProperty, 0);
					this.slides.eq(nextIndex).css(this.positionProperty, this.slideSize * 2);
				}
			}
		},
		switchSlide: function () {
			// prepare positions and calculate offset
			var self = this;
			var oldSlide = this.slides.eq(this.prevIndex);
			var newSlide = this.slides.eq(this.currentIndex);
			this.animating = true;

			// resize mask to fit slide
			if (this.options.maskAutoSize) {
				this.mask.animate({
					height: newSlide.outerHeight(true)
				}, {
						duration: this.options.animSpeed
					});
			}

			// start animation
			var animProps = {};
			animProps[this.animProperty] = this.direction > 0 ? -this.slideSize * 2 : 0;
			this.preparePosition();
			this.slider.animate(animProps, {
				duration: this.options.animSpeed, complete: function () {
					self.setSlidesPosition(self.currentIndex);

					// start autorotation
					self.animating = false;
					self.autoRotate();

					// onchange callback
					self.makeCallback('onChange', self);
				}
			});

			// refresh classes
			this.refreshState();

			// onchange callback
			this.makeCallback('onBeforeChange', this);
		},
		refreshState: function (initial) {
			// slide change function
			this.slides.removeClass(this.options.activeClass).eq(this.currentIndex).addClass(this.options.activeClass);
			this.pagerLinks.removeClass(this.options.activeClass).eq(this.currentIndex).addClass(this.options.activeClass);

			// display current slide number
			this.currentNumber.html(this.currentIndex + 1);
			this.totalNumber.html(this.slides.length);

			// add class if not enough slides
			this.holder.toggleClass('not-enough-slides', this.slides.length === 1);
		},
		autoRotate: function () {
			var self = this;
			clearTimeout(this.timer);
			if (this.options.autoRotation) {
				this.timer = setTimeout(function () {
					self.nextSlide();
				}, this.options.switchTime);
			}
		},
		makeCallback: function (name) {
			if (typeof this.options[name] === 'function') {
				var args = Array.prototype.slice.call(arguments);
				args.shift();
				this.options[name].apply(this, args);
			}
		},
		destroy: function () {
			// destroy handler
			this.btnPrev.unbind('click', this.btnPrevHandler);
			this.btnNext.unbind('click', this.btnNextHandler);
			this.pagerLinks.unbind('click', this.pagerLinksHandler);
			this.holder.unbind('mouseenter', this.hoverHandler);
			this.holder.unbind('mouseleave', this.leaveHandler);
			jQuery(window).unbind('load resize orientationchange', this.resizeHandler);
			clearTimeout(this.timer);

			// destroy swipe handler
			if (this.swipeHandler) {
				this.swipeHandler.destroy();
			}

			// remove inline styles, classes and pagination
			this.holder.removeClass(this.options.galleryReadyClass);
			this.slider.add(this.slides).removeAttr('style');
			if (typeof this.options.generatePagination === 'string') {
				this.pagerHolder.empty();
			}
		}
	};

	// detect device type
	var isTouchDevice = /Windows Phone/.test(navigator.userAgent) || ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch;

	// jquery plugin
	$.fn.scrollAbsoluteGallery = function (opt) {
		return this.each(function () {
			jQuery(this).data('ScrollAbsoluteGallery', new ScrollAbsoluteGallery($.extend(opt, { holder: this })));
		});
	};
}(jQuery));

/*
 * jQuery Form Styler v1.7.6
 * https://github.com/Dimox/jQueryFormStyler
 *
 * Copyright 2012-2016 Dimox (http://dimox.name/)
 * Released under the MIT license.
 *
 * Date: 2016.06.05
 *
 */

; (function (factory) {
	if (typeof define === 'function' && define.amd) {
		// AMD
		define(['jquery'], factory);
	} else if (typeof exports === 'object') {
		// CommonJS
		module.exports = factory(require('jquery'));
	} else {
		factory(jQuery);
	}
}(function ($) {

	'use strict';

	var pluginName = 'styler',
		defaults = {
			idSuffix: '-styler',
			filePlaceholder: 'Файл не выбран',
			fileBrowse: 'Обзор...',
			fileNumber: 'Выбрано файлов: %s',
			selectPlaceholder: 'Выберите...',
			selectSearch: false,
			selectSearchLimit: 10,
			selectSearchNotFound: 'Совпадений не найдено',
			selectSearchPlaceholder: 'Поиск...',
			selectVisibleOptions: 0,
			singleSelectzIndex: '100',
			selectSmartPositioning: true,
			onSelectOpened: function () { },
			onSelectClosed: function () { },
			onFormStyled: function () { }
		};

	function Plugin(element, options) {
		this.element = element;
		this.options = $.extend({}, defaults, options);
		this.init();
	}

	Plugin.prototype = {

		// init
		init: function () {

			var el = jQuery(this.element);
			var opt = this.options;

			var iOS = (navigator.userAgent.match(/(iPad|iPhone|iPod)/i) && !navigator.userAgent.match(/(Windows\sPhone)/i)) ? true : false;
			var Android = (navigator.userAgent.match(/Android/i) && !navigator.userAgent.match(/(Windows\sPhone)/i)) ? true : false;

			function Attributes() {
				if (el.attr('id') !== undefined && el.attr('id') !== '') {
					this.id = el.attr('id') + opt.idSuffix;
				}
				this.title = el.attr('title');
				this.classes = el.attr('class');
				this.data = el.data();
			}

			// checkbox
			if (el.is(':checkbox')) {

				var checkboxOutput = function () {

					var att = new Attributes();
					var checkbox = jQuery('<div class="jq-checkbox"><div class="jq-checkbox__div"></div></div>')
						.attr({
							id: att.id,
							title: att.title
						})
						.addClass(att.classes)
						.data(att.data)
						;

					// hide dafault checkbox
					el.css({
						position: 'absolute',
						zIndex: '-1',
						opacity: 0,
						margin: 0,
						padding: 0
					}).after(checkbox).prependTo(checkbox);

					checkbox.attr('unselectable', 'on').css({
						'-webkit-user-select': 'none',
						'-moz-user-select': 'none',
						'-ms-user-select': 'none',
						'-o-user-select': 'none',
						'user-select': 'none',
						display: 'inline-block',
						position: 'relative',
						overflow: 'hidden'
					});

					if (el.is(':checked')) checkbox.addClass('checked');
					if (el.is(':disabled')) checkbox.addClass('disabled');

					// click on custom ceckbox
					checkbox.click(function (e) {
						e.preventDefault();
						if (!checkbox.is('.disabled')) {
							if (el.is(':checked')) {
								el.prop('checked', false);
								checkbox.removeClass('checked');
							} else {
								el.prop('checked', true);
								checkbox.addClass('checked');
							}
							el.focus().change();
						}
					});
					// click on label
					el.closest('label').add('label[for="' + el.attr('id') + '"]').on('click.styler', function (e) {
						if (!jQuery(e.target).is('a') && !jQuery(e.target).closest(checkbox).length) {
							checkbox.triggerHandler('click');
							e.preventDefault();
						}
					});
					// typing Space or Enter
					el.on('change.styler', function () {
						if (el.is(':checked')) checkbox.addClass('checked');
						else checkbox.removeClass('checked');
					})
						// check checkbox wrapped in label
						.on('keydown.styler', function (e) {
							if (e.which == 32) checkbox.click();
						})
						.on('focus.styler', function () {
							if (!checkbox.is('.disabled')) checkbox.addClass('focused');
						})
						.on('blur.styler', function () {
							checkbox.removeClass('focused');
						});

				}; // end checkboxOutput()

				checkboxOutput();

				// updating
				el.on('refresh', function () {
					el.closest('label').add('label[for="' + el.attr('id') + '"]').off('.styler');
					el.off('.styler').parent().before(el).remove();
					checkboxOutput();
				});

				// end checkbox

				// radio
			} else if (el.is(':radio')) {

				var radioOutput = function () {

					var att = new Attributes();
					var radio = jQuery('<div class="jq-radio"><div class="jq-radio__div"></div></div>')
						.attr({
							id: att.id,
							title: att.title
						})
						.addClass(att.classes)
						.data(att.data)
						;

					// hide default radio
					el.css({
						position: 'absolute',
						zIndex: '-1',
						opacity: 0,
						margin: 0,
						padding: 0
					}).after(radio).prependTo(radio);

					radio.attr('unselectable', 'on').css({
						'-webkit-user-select': 'none',
						'-moz-user-select': 'none',
						'-ms-user-select': 'none',
						'-o-user-select': 'none',
						'user-select': 'none',
						display: 'inline-block',
						position: 'relative'
					});

					if (el.is(':checked')) radio.addClass('checked');
					if (el.is(':disabled')) radio.addClass('disabled');

					// get parent for radio with the same names
					// http://stackoverflow.com/a/27733847
					$.fn.commonParents = function () {
						var cachedThis = this;
						return cachedThis.first().parents().filter(function () {
							return jQuery(this).find(cachedThis).length === cachedThis.length;
						});
					};
					$.fn.commonParent = function () {
						return jQuery(this).commonParents().first();
					};

					// click on custom radio
					radio.click(function (e) {
						e.preventDefault();
						if (!radio.is('.disabled')) {
							var inputName = jQuery('input[name="' + el.attr('name') + '"]');
							inputName.commonParent().find(inputName).prop('checked', false).parent().removeClass('checked');
							el.prop('checked', true).parent().addClass('checked');
							el.focus().change();
						}
					});
					// клик на label
					el.closest('label').add('label[for="' + el.attr('id') + '"]').on('click.styler', function (e) {
						if (!jQuery(e.target).is('a') && !jQuery(e.target).closest(radio).length) {
							radio.triggerHandler('click');
							e.preventDefault();
						}
					});
					// arrows
					el.on('change.styler', function () {
						el.parent().addClass('checked');
					})
						.on('focus.styler', function () {
							if (!radio.is('.disabled')) radio.addClass('focused');
						})
						.on('blur.styler', function () {
							radio.removeClass('focused');
						});

				}; // end radioOutput()

				radioOutput();

				// changeing
				el.on('refresh', function () {
					el.closest('label').add('label[for="' + el.attr('id') + '"]').off('.styler');
					el.off('.styler').parent().before(el).remove();
					radioOutput();
				});

				// end radio

				// file
			} else if (el.is(':file')) {

				// hide default field
				el.css({
					position: 'absolute',
					top: 0,
					right: 0,
					margin: 0,
					padding: 0,
					opacity: 0,
					fontSize: '100px'
				});

				var fileOutput = function () {

					var att = new Attributes();
					var placeholder = el.data('placeholder');
					if (placeholder === undefined) placeholder = opt.filePlaceholder;
					var browse = el.data('browse');
					if (browse === undefined || browse === '') browse = opt.fileBrowse;

					var file =
						jQuery('<div class="jq-file">' +
							'<div class="jq-file__name">' + placeholder + '</div>' +
							'<div class="jq-file__browse">' + browse + '</div>' +
							'</div>')
							.css({
								display: 'inline-block',
								position: 'relative',
								overflow: 'hidden'
							})
							.attr({
								id: att.id,
								title: att.title
							})
							.addClass(att.classes)
							.data(att.data)
						;

					el.after(file).appendTo(file);
					if (el.is(':disabled')) file.addClass('disabled');

					el.on('change.styler', function () {
						var value = el.val();
						var name = jQuery('div.jq-file__name', file);
						if (el.is('[multiple]')) {
							value = '';
							var files = el[0].files.length;
							if (files > 0) {
								var number = el.data('number');
								if (number === undefined) number = opt.fileNumber;
								number = number.replace('%s', files);
								value = number;
							}
						}
						name.text(value.replace(/.+[\\\/]/, ''));
						if (value === '') {
							name.text(placeholder);
							file.removeClass('changed');
						} else {
							file.addClass('changed');
						}
					})
						.on('focus.styler', function () {
							file.addClass('focused');
						})
						.on('blur.styler', function () {
							file.removeClass('focused');
						})
						.on('click.styler', function () {
							file.removeClass('focused');
						});

				}; // end fileOutput()

				fileOutput();

				// changing
				el.on('refresh', function () {
					el.off('.styler').parent().before(el).remove();
					fileOutput();
				});

				// end file

			} else if (el.is('input[type="number"]')) {

				var numberOutput = function () {

					var att = new Attributes();
					var number =
						jQuery('<div class="jq-number">' +
							'<div class="jq-number__spin minus"></div>' +
							'<div class="jq-number__spin plus"></div>' +
							'</div>')
							.attr({
								id: att.id,
								title: att.title
							})
							.addClass(att.classes)
							.data(att.data)
						;

					el.after(number).prependTo(number).wrap('<div class="jq-number__field"></div>');
					if (el.is(':disabled')) number.addClass('disabled');

					var min,
						max,
						step,
						timeout = null,
						interval = null;
					if (el.attr('min') !== undefined) min = el.attr('min');
					if (el.attr('max') !== undefined) max = el.attr('max');
					if (el.attr('step') !== undefined && $.isNumeric(el.attr('step')))
						step = Number(el.attr('step'));
					else
						step = Number(1);

					var changeValue = function (spin) {
						var value = el.val(),
							newValue;

						if (!$.isNumeric(value)) {
							value = 0;
							el.val('0');
						}

						if (spin.is('.minus')) {
							newValue = Number(value) - step;
						} else if (spin.is('.plus')) {
							newValue = Number(value) + step;
						}

						// get count of signs after coma step
						var decimals = (step.toString().split('.')[1] || []).length;
						if (decimals > 0) {
							var multiplier = '1';
							while (multiplier.length <= decimals) multiplier = multiplier + '0';
							// deleting signs after coma
							newValue = Math.round(newValue * multiplier) / multiplier;
						}

						if ($.isNumeric(min) && $.isNumeric(max)) {
							if (newValue >= min && newValue <= max) el.val(newValue);
						} else if ($.isNumeric(min) && !$.isNumeric(max)) {
							if (newValue >= min) el.val(newValue);
						} else if (!$.isNumeric(min) && $.isNumeric(max)) {
							if (newValue <= max) el.val(newValue);
						} else {
							el.val(newValue);
						}
					};

					if (!number.is('.disabled')) {
						number.on('mousedown', 'div.jq-number__spin', function () {
							var spin = jQuery(this);
							changeValue(spin);
							timeout = setTimeout(function () {
								interval = setInterval(function () { changeValue(spin); }, 40);
							}, 350);
						}).on('mouseup mouseout', 'div.jq-number__spin', function () {
							clearTimeout(timeout);
							clearInterval(interval);
						}).on('mouseup', 'div.jq-number__spin', function () {
							el.change();
						});
						el.on('focus.styler', function () {
							number.addClass('focused');
						})
							.on('blur.styler', function () {
								number.removeClass('focused');
							});
					}

				}; // end numberOutput()

				numberOutput();

				// updating
				el.on('refresh', function () {
					el.off('.styler').closest('.jq-number').before(el).remove();
					numberOutput();
				});

				// end number

				// select
			} else if (el.is('select')) {

				var selectboxOutput = function () {

					// disable page scroll on scrilling custom scorll
					function preventScrolling(selector) {
						selector.off('mousewheel DOMMouseScroll').on('mousewheel DOMMouseScroll', function (e) {
							var scrollTo = null;
							if (e.type == 'mousewheel') { scrollTo = (e.originalEvent.wheelDelta * -1); }
							else if (e.type == 'DOMMouseScroll') { scrollTo = 40 * e.originalEvent.detail; }
							if (scrollTo) {
								e.stopPropagation();
								e.preventDefault();
								jQuery(this).scrollTop(scrollTo + jQuery(this).scrollTop());
							}
						});
					}

					var option = jQuery('option', el);
					var list = '';
					// select
					function makeList() {
						for (var i = 0; i < option.length; i++) {
							var op = option.eq(i);
							var li = '',
								liClass = '',
								liClasses = '',
								id = '',
								title = '',
								dataList = '',
								optionClass = '',
								optgroupClass = '',
								dataJqfsClass = '';
							var disabled = 'disabled';
							var selDis = 'selected sel disabled';
							if (op.prop('selected')) liClass = 'selected sel';
							if (op.is(':disabled')) liClass = disabled;
							if (op.is(':selected:disabled')) liClass = selDis;
							if (op.attr('id') !== undefined && op.attr('id') !== '') id = ' id="' + op.attr('id') + opt.idSuffix + '"';
							if (op.attr('title') !== undefined && option.attr('title') !== '') title = ' title="' + op.attr('title') + '"';
							if (op.attr('class') !== undefined) {
								optionClass = ' ' + op.attr('class');
								dataJqfsClass = ' data-jqfs-class="' + op.attr('class') + '"';
							}

							var data = op.data();
							for (var k in data) {
								if (data[k] !== '') dataList += ' data-' + k + '="' + data[k] + '"';
							}

							if ((liClass + optionClass) !== '') liClasses = ' class="' + liClass + optionClass + '"';
							li = '<li' + dataJqfsClass + dataList + liClasses + title + id + '>' + op.html() + '</li>';

							// if optgroup
							if (op.parent().is('optgroup')) {
								if (op.parent().attr('class') !== undefined) optgroupClass = ' ' + op.parent().attr('class');
								li = '<li' + dataJqfsClass + dataList + ' class="' + liClass + optionClass + ' option' + optgroupClass + '"' + title + id + '>' + op.html() + '</li>';
								if (op.is(':first-child')) {
									li = '<li class="optgroup' + optgroupClass + '">' + op.parent().attr('label') + '</li>' + li;
								}
							}

							list += li;
						}
					} // end makeList()

					// single select
					function doSelect() {

						var att = new Attributes();
						var searchHTML = '';
						var selectPlaceholder = el.data('placeholder');
						var selectSearch = el.data('search');
						var selectSearchLimit = el.data('search-limit');
						var selectSearchNotFound = el.data('search-not-found');
						var selectSearchPlaceholder = el.data('search-placeholder');
						var singleSelectzIndex = el.data('z-index');
						var selectSmartPositioning = el.data('smart-positioning');

						if (selectPlaceholder === undefined) selectPlaceholder = opt.selectPlaceholder;
						if (selectSearch === undefined || selectSearch === '') selectSearch = opt.selectSearch;
						if (selectSearchLimit === undefined || selectSearchLimit === '') selectSearchLimit = opt.selectSearchLimit;
						if (selectSearchNotFound === undefined || selectSearchNotFound === '') selectSearchNotFound = opt.selectSearchNotFound;
						if (selectSearchPlaceholder === undefined) selectSearchPlaceholder = opt.selectSearchPlaceholder;
						if (singleSelectzIndex === undefined || singleSelectzIndex === '') singleSelectzIndex = opt.singleSelectzIndex;
						if (selectSmartPositioning === undefined || selectSmartPositioning === '') selectSmartPositioning = opt.selectSmartPositioning;

						var selectbox =
							jQuery('<div class="jq-selectbox jqselect">' +
								'<div class="jq-selectbox__select" style="position: relative">' +
								'<div class="jq-selectbox__select-text"></div>' +
								'<div class="jq-selectbox__trigger">' +
								'<div class="jq-selectbox__trigger-arrow"></div></div>' +
								'</div>' +
								'</div>')
								.css({
									display: 'inline-block',
									position: 'relative',
									zIndex: singleSelectzIndex
								})
								.attr({
									id: att.id,
									title: att.title
								})
								.addClass(att.classes)
								.data(att.data)
							;

						el.css({ margin: 0, padding: 0 }).after(selectbox).prependTo(selectbox);

						var divSelect = jQuery('div.jq-selectbox__select', selectbox);
						var divText = jQuery('div.jq-selectbox__select-text', selectbox);
						var optionSelected = option.filter(':selected');

						makeList();

						if (selectSearch) searchHTML =
							'<div class="jq-selectbox__search"><input type="search" autocomplete="off" placeholder="' + selectSearchPlaceholder + '"></div>' +
							'<div class="jq-selectbox__not-found">' + selectSearchNotFound + '</div>';
						var dropdown =
							jQuery('<div class="jq-selectbox__dropdown" style="position: absolute">' +
								searchHTML +
								'<ul style="position: relative; list-style: none; overflow: auto; overflow-x: hidden">' + list + '</ul>' +
								'</div>');
						selectbox.append(dropdown);
						var ul = jQuery('ul', dropdown);
						var li = jQuery('li', dropdown);
						var search = jQuery('input', dropdown);
						var notFound = jQuery('div.jq-selectbox__not-found', dropdown).hide();
						if (li.length < selectSearchLimit) search.parent().hide();

						// display default option
						// or display placeholder
						if (option.first().text() === '' && option.first().is(':selected')) {
							divText.text(selectPlaceholder).addClass('placeholder');
						} else {
							divText.text(optionSelected.text());
						}

						// get widthest select option
						var liWidthInner = 0,
							liWidth = 0;
						li.css({ 'display': 'inline-block' });
						li.each(function () {
							var l = jQuery(this);
							if (l.innerWidth() > liWidthInner) {
								liWidthInner = l.innerWidth();
								liWidth = l.width();
							}
						});
						li.css({ 'display': '' });

						// set select width
						if (divText.is('.placeholder') && (divText.width() > liWidthInner)) {
							divText.width(divText.width());
						} else {
							var selClone = selectbox.clone().appendTo('body').width('auto');
							var selCloneWidth = selClone.outerWidth();
							selClone.remove();
							if (selCloneWidth == selectbox.outerWidth()) {
								divText.width(liWidth);
							}
						}

						// dropdown
						if (liWidthInner > selectbox.width()) dropdown.width(liWidthInner);

						// set placeholder or first item
						if (option.first().text() === '' && el.data('placeholder') !== '') {
							li.first().hide();
						}

						// hide default select
						el.css({
							position: 'absolute',
							left: 0,
							top: 0,
							width: '100%',
							height: '100%',
							opacity: 0
						});

						var selectHeight = selectbox.outerHeight(true);
						var searchHeight = search.parent().outerHeight(true);
						var isMaxHeight = ul.css('max-height');
						var liSelected = li.filter('.selected');
						if (liSelected.length < 1) li.first().addClass('selected sel');
						if (li.data('li-height') === undefined) li.data('li-height', li.outerHeight());
						var position = dropdown.css('top');
						if (dropdown.css('left') == 'auto') dropdown.css({ left: 0 });
						if (dropdown.css('top') == 'auto') {
							dropdown.css({ top: selectHeight });
							position = selectHeight;
						}
						dropdown.hide();

						// ont default item
						if (liSelected.length) {
							// add class
							if (option.first().text() != optionSelected.text()) {
								selectbox.addClass('changed');
							}
							selectbox.data('jqfs-class', liSelected.data('jqfs-class'));
							selectbox.addClass(liSelected.data('jqfs-class'));
						}

						// unactive select
						if (el.is(':disabled')) {
							selectbox.addClass('disabled');
							return false;
						}

						// click on custom select
						divSelect.click(function () {

							// cack on close select
							if (jQuery('div.jq-selectbox').filter('.opened').length) {
								opt.onSelectClosed.call(jQuery('div.jq-selectbox').filter('.opened'));
							}

							el.focus();

							// if iOS
							if (iOS) return;

							// position
							var win = jQuery(window);
							var liHeight = li.data('li-height');
							var topOffset = selectbox.offset().top;
							var bottomOffset = win.height() - selectHeight - (topOffset - win.scrollTop());
							var visible = el.data('visible-options');
							if (visible === undefined || visible === '') visible = opt.selectVisibleOptions;
							var minHeight = liHeight * 5;
							var newHeight = liHeight * visible;
							if (visible > 0 && visible < 6) minHeight = newHeight;
							if (visible === 0) newHeight = 'auto';

							var dropDown = function () {
								dropdown.height('auto').css({ bottom: 'auto', top: position });
								var maxHeightBottom = function () {
									ul.css('max-height', Math.floor((bottomOffset - 20 - searchHeight) / liHeight) * liHeight);
								};
								maxHeightBottom();
								ul.css('max-height', newHeight);
								if (isMaxHeight != 'none') {
									ul.css('max-height', isMaxHeight);
								}
								if (bottomOffset < (dropdown.outerHeight() + 20)) {
									maxHeightBottom();
								}
							};

							var dropUp = function () {
								dropdown.height('auto').css({ top: 'auto', bottom: position });
								var maxHeightTop = function () {
									ul.css('max-height', Math.floor((topOffset - win.scrollTop() - 20 - searchHeight) / liHeight) * liHeight);
								};
								maxHeightTop();
								ul.css('max-height', newHeight);
								if (isMaxHeight != 'none') {
									ul.css('max-height', isMaxHeight);
								}
								if ((topOffset - win.scrollTop() - 20) < (dropdown.outerHeight() + 20)) {
									maxHeightTop();
								}
							};

							if (selectSmartPositioning === true || selectSmartPositioning === 1) {
								// drop down
								if (bottomOffset > (minHeight + searchHeight + 20)) {
									dropDown();
									selectbox.removeClass('dropup').addClass('dropdown');
									// drop up
								} else {
									dropUp();
									selectbox.removeClass('dropdown').addClass('dropup');
								}
							} else if (selectSmartPositioning === false || selectSmartPositioning === 0) {
								// drop down
								if (bottomOffset > (minHeight + searchHeight + 20)) {
									dropDown();
									selectbox.removeClass('dropup').addClass('dropdown');
								}
							}

							// checking window width
							if (selectbox.offset().left + dropdown.outerWidth() > win.width()) {
								dropdown.css({ left: 'auto', right: 0 });
							}
							// position end

							jQuery('div.jqselect').css({ zIndex: (singleSelectzIndex - 1) }).removeClass('opened');
							selectbox.css({ zIndex: singleSelectzIndex });
							if (dropdown.is(':hidden')) {
								jQuery('div.jq-selectbox__dropdown:visible').hide();
								dropdown.show();
								selectbox.addClass('opened focused');
								// callback on open
								opt.onSelectOpened.call(selectbox);
							} else {
								dropdown.hide();
								selectbox.removeClass('opened dropup dropdown');
								// callback on close
								if (jQuery('div.jq-selectbox').filter('.opened').length) {
									opt.onSelectClosed.call(selectbox);
								}
							}

							// search
							if (search.length) {
								search.val('').keyup();
								notFound.hide();
								search.keyup(function () {
									var query = jQuery(this).val();
									li.each(function () {
										if (!jQuery(this).html().match(new RegExp('.*?' + query + '.*?', 'i'))) {
											jQuery(this).hide();
										} else {
											jQuery(this).show();
										}
									});
									// hide first option
									if (option.first().text() === '' && el.data('placeholder') !== '') {
										li.first().hide();
									}
									if (li.filter(':visible').length < 1) {
										notFound.show();
									} else {
										notFound.hide();
									}
								});
							}

							// scroll to selected
							if (li.filter('.selected').length) {
								if (el.val() === '') {
									ul.scrollTop(0);
								} else {
									// height fix
									if ((ul.innerHeight() / liHeight) % 2 !== 0) liHeight = liHeight / 2;
									ul.scrollTop(ul.scrollTop() + li.filter('.selected').position().top - ul.innerHeight() / 2 + liHeight);
								}
							}

							preventScrolling(ul);

						}); // end divSelect.click()

						// on hover
						li.hover(function () {
							jQuery(this).siblings().removeClass('selected');
						});
						var selectedText = li.filter('.selected').text();

						// on click
						li.filter(':not(.disabled):not(.optgroup)').click(function () {
							el.focus();
							var t = jQuery(this);
							var liText = t.text();
							if (!t.is('.selected')) {
								var index = t.index();
								index -= t.prevAll('.optgroup').length;
								t.addClass('selected sel').siblings().removeClass('selected sel');
								option.prop('selected', false).eq(index).prop('selected', true);
								selectedText = liText;
								divText.text(liText);

								// add class
								if (selectbox.data('jqfs-class')) selectbox.removeClass(selectbox.data('jqfs-class'));
								selectbox.data('jqfs-class', t.data('jqfs-class'));
								selectbox.addClass(t.data('jqfs-class'));

								el.change();
							}
							dropdown.hide();
							selectbox.removeClass('opened dropup dropdown');
							// callback on close
							opt.onSelectClosed.call(selectbox);

						});
						dropdown.mouseout(function () {
							jQuery('li.sel', dropdown).addClass('selected');
						});

						// select change
						el.on('change.styler', function () {
							divText.text(option.filter(':selected').text()).removeClass('placeholder');
							li.removeClass('selected sel').not('.optgroup').eq(el[0].selectedIndex).addClass('selected sel');
							// add class
							if (option.first().text() != li.filter('.selected').text()) {
								selectbox.addClass('changed');
							} else {
								selectbox.removeClass('changed');
							}
						})
							.on('focus.styler', function () {
								selectbox.addClass('focused');
								jQuery('div.jqselect').not('.focused').removeClass('opened dropup dropdown').find('div.jq-selectbox__dropdown').hide();
							})
							.on('blur.styler', function () {
								selectbox.removeClass('focused');
							})
							// change by keyboard
							.on('keydown.styler keyup.styler', function (e) {
								var liHeight = li.data('li-height');
								if (el.val() === '') {
									divText.text(selectPlaceholder).addClass('placeholder');
								} else {
									divText.text(option.filter(':selected').text());
								}
								li.removeClass('selected sel').not('.optgroup').eq(el[0].selectedIndex).addClass('selected sel');
								// Up, Left, Page Up, Home
								if (e.which == 38 || e.which == 37 || e.which == 33 || e.which == 36) {
									if (el.val() === '') {
										ul.scrollTop(0);
									} else {
										ul.scrollTop(ul.scrollTop() + li.filter('.selected').position().top);
									}
								}
								// Down, Right, Page Down, End
								if (e.which == 40 || e.which == 39 || e.which == 34 || e.which == 35) {
									ul.scrollTop(ul.scrollTop() + li.filter('.selected').position().top - ul.innerHeight() + liHeight);
								}
								// close on enter
								if (e.which == 13) {
									e.preventDefault();
									dropdown.hide();
									selectbox.removeClass('opened dropup dropdown');
									// callback on select
									opt.onSelectClosed.call(selectbox);
								}
							}).on('keydown.styler', function (e) {
								// open select
								if (e.which == 32) {
									e.preventDefault();
									divSelect.click();
								}
							});

						// close select out of select
						if (!onDocumentClick.registered) {
							jQuery(document).on('click', onDocumentClick);
							onDocumentClick.registered = true;
						}

					} // end doSelect()

					// multiselect
					function doMultipleSelect() {

						var att = new Attributes();
						var selectbox =
							jQuery('<div class="jq-select-multiple jqselect"></div>')
								.css({
									display: 'inline-block',
									position: 'relative'
								})
								.attr({
									id: att.id,
									title: att.title
								})
								.addClass(att.classes)
								.data(att.data)
							;

						el.css({ margin: 0, padding: 0 }).after(selectbox);

						makeList();
						selectbox.append('<ul>' + list + '</ul>');
						var ul = jQuery('ul', selectbox).css({
							'position': 'relative',
							'overflow-x': 'hidden',
							'-webkit-overflow-scrolling': 'touch'
						});
						var li = jQuery('li', selectbox).attr('unselectable', 'on');
						var size = el.attr('size');
						var ulHeight = ul.outerHeight();
						var liHeight = li.outerHeight();
						if (size !== undefined && size > 0) {
							ul.css({ 'height': liHeight * size });
						} else {
							ul.css({ 'height': liHeight * 4 });
						}
						if (ulHeight > selectbox.height()) {
							ul.css('overflowY', 'scroll');
							preventScrolling(ul);
							// scrolling
							if (li.filter('.selected').length) {
								ul.scrollTop(ul.scrollTop() + li.filter('.selected').position().top);
							}
						}

						// hide default select
						el.prependTo(selectbox).css({
							position: 'absolute',
							left: 0,
							top: 0,
							width: '100%',
							height: '100%',
							opacity: 0
						});

						// if note  select
						if (el.is(':disabled')) {
							selectbox.addClass('disabled');
							option.each(function () {
								if (jQuery(this).is(':selected')) li.eq(jQuery(this).index()).addClass('selected');
							});

							// if actecit
						} else {

							// click on list i
							li.filter(':not(.disabled):not(.optgroup)').click(function (e) {
								el.focus();
								var clkd = jQuery(this);
								if (!e.ctrlKey && !e.metaKey) clkd.addClass('selected');
								if (!e.shiftKey) clkd.addClass('first');
								if (!e.ctrlKey && !e.metaKey && !e.shiftKey) clkd.siblings().removeClass('selected first');

								// select with Ctrl
								if (e.ctrlKey || e.metaKey) {
									if (clkd.is('.selected')) clkd.removeClass('selected first');
									else clkd.addClass('selected first');
									clkd.siblings().removeClass('first');
								}

								// selecting with Shift
								if (e.shiftKey) {
									var prev = false,
										next = false;
									clkd.siblings().removeClass('selected').siblings('.first').addClass('selected');
									clkd.prevAll().each(function () {
										if (jQuery(this).is('.first')) prev = true;
									});
									clkd.nextAll().each(function () {
										if (jQuery(this).is('.first')) next = true;
									});
									if (prev) {
										clkd.prevAll().each(function () {
											if (jQuery(this).is('.selected')) return false;
											else jQuery(this).not('.disabled, .optgroup').addClass('selected');
										});
									}
									if (next) {
										clkd.nextAll().each(function () {
											if (jQuery(this).is('.selected')) return false;
											else jQuery(this).not('.disabled, .optgroup').addClass('selected');
										});
									}
									if (li.filter('.selected').length == 1) clkd.addClass('first');
								}

								// selecting by mouse
								option.prop('selected', false);
								li.filter('.selected').each(function () {
									var t = jQuery(this);
									var index = t.index();
									if (t.is('.option')) index -= t.prevAll('.optgroup').length;
									option.eq(index).prop('selected', true);
								});
								el.change();

							});

							// select with keyboard
							option.each(function (i) {
								jQuery(this).data('optionIndex', i);
							});
							el.on('change.styler', function () {
								li.removeClass('selected');
								var arrIndexes = [];
								option.filter(':selected').each(function () {
									arrIndexes.push(jQuery(this).data('optionIndex'));
								});
								li.not('.optgroup').filter(function (i) {
									return $.inArray(i, arrIndexes) > -1;
								}).addClass('selected');
							})
								.on('focus.styler', function () {
									selectbox.addClass('focused');
								})
								.on('blur.styler', function () {
									selectbox.removeClass('focused');
								});

							// scroll with keyboard
							if (ulHeight > selectbox.height()) {
								el.on('keydown.styler', function (e) {
									// Up, left, PageUp
									if (e.which == 38 || e.which == 37 || e.which == 33) {
										ul.scrollTop(ul.scrollTop() + li.filter('.selected').position().top - liHeight);
									}
									// down, right, PageDown
									if (e.which == 40 || e.which == 39 || e.which == 34) {
										ul.scrollTop(ul.scrollTop() + li.filter('.selected:last').position().top - ul.innerHeight() + liHeight * 2);
									}
								});
							}

						}
					} // end doMultipleSelect()

					if (el.is('[multiple]')) {

						// if Android или iOS dn'ot use custom select
						if (Android || iOS) return;

						doMultipleSelect();
					} else {
						doSelect();
					}

				}; // end selectboxOutput()

				selectboxOutput();

				// updating
				el.on('refresh', function () {
					el.off('.styler').parent().before(el).remove();
					selectboxOutput();
				});

				// end select

				// reset
			} else if (el.is(':reset')) {
				el.on('click', function () {
					setTimeout(function () {
						el.closest('form').find('input, select').trigger('refresh');
					}, 1);
				});
			} // end reset

		}, // init: function()

		// deructor
		destroy: function () {

			var el = jQuery(this.element);

			if (el.is(':checkbox') || el.is(':radio')) {
				el.removeData('_' + pluginName).off('.styler refresh').removeAttr('style').parent().before(el).remove();
				el.closest('label').add('label[for="' + el.attr('id') + '"]').off('.styler');
			} else if (el.is('input[type="number"]')) {
				el.removeData('_' + pluginName).off('.styler refresh').closest('.jq-number').before(el).remove();
			} else if (el.is(':file') || el.is('select')) {
				el.removeData('_' + pluginName).off('.styler refresh').removeAttr('style').parent().before(el).remove();
			}

		} // destroy: function()

	}; // Plugin.prototype

	$.fn[pluginName] = function (options) {
		var args = arguments;
		if (options === undefined || typeof options === 'object') {
			this.each(function () {
				if (!$.data(this, '_' + pluginName)) {
					$.data(this, '_' + pluginName, new Plugin(this, options));
				}
			})
				// callback after plugin called
				.promise()
				.done(function () {
					var opt = jQuery(this[0]).data('_' + pluginName);
					if (opt) opt.options.onFormStyled.call();
				});
			return this;
		} else if (typeof options === 'string' && options[0] !== '_' && options !== 'init') {
			var returns;
			this.each(function () {
				var instance = $.data(this, '_' + pluginName);
				if (instance instanceof Plugin && typeof instance[options] === 'function') {
					returns = instance[options].apply(instance, Array.prototype.slice.call(args, 1));
				}
			});
			return returns !== undefined ? returns : this;
		}
	};

	// hide drop on click ouft select
	function onDocumentClick(e) {
		// e.target.nodeName != 'OPTION' - Opera fix
		if (!jQuery(e.target).parents().hasClass('jq-selectbox') && e.target.nodeName != 'OPTION') {
			if (jQuery('div.jq-selectbox.opened').length) {
				var selectbox = jQuery('div.jq-selectbox.opened'),
					search = jQuery('div.jq-selectbox__search input', selectbox),
					dropdown = jQuery('div.jq-selectbox__dropdown', selectbox),
					opt = selectbox.find('select').data('_' + pluginName).options;

				// callback on closed
				opt.onSelectClosed.call(selectbox);

				if (search.length) search.val('').keyup();
				dropdown.hide().find('li.sel').addClass('selected');
				selectbox.removeClass('focused opened dropup dropdown');
			}
		}
	}
	onDocumentClick.registered = false;

}));

//Contact Us
if (jQuery('.wpcf7-form').length && jQuery('[name="your-phone"]').length) {
	jQuery('[name="your-phone"]').mask('000-000-0000');
}

//Survey CTA
if (jQuery(".survey-cta").length) {
	$leftScroll = jQuery('.wrapper-section_select').width();
	$leftPosition = $leftScroll;
	var n = jQuery(".wrapper-section_select").length;

	jQuery('.wrapper-section').css('width', $leftPosition);


	jQuery('.steps-section_fullwidth').css('width', n * $leftPosition);
	var $countsteps = 1;
	var $progress = 50;

	jQuery(".stepNext").click(function () {
		if ($countsteps <= 3) {
			jQuery('.steps-section_fullwidth').css('left', -$leftScroll);
			if ($countsteps == 3) {
				jQuery('.stepNext').text('Search').addClass('search').removeClass('stepNext');
				jQuery('.progress-wrapper_color').css('width', $progress + "%");
				$countsteps++;
				jQuery('.steps').text($countsteps + "/4");
				jQuery('.search').click(function () {
					jQuery('.steps-wrapper h3').text('Great, we found 14 lenders for you');
					jQuery('.steps-section_middle').hide();
				});
			} else {
				$leftScroll = $leftScroll + $leftPosition;
				$countsteps++;
				jQuery('.steps').text($countsteps + "/4");
				jQuery('.progress-wrapper_color').css('width', $progress + "%");
				$progress = $progress + 25;
			}
		}
	});
}

var $closebutton = jQuery("<span class='closeLink'></span>");
jQuery(".searchform div").append($closebutton);

jQuery(".searchform label").click(function () {
	jQuery("header .searchform").addClass("openSearch");
	if (jQuery(window).width() < 769) {
		jQuery('body').addClass('search-start');
	}
});
jQuery(".closeLink").click(function () {
	jQuery("header .searchform").removeClass("openSearch");
	if (jQuery(window).width() < 769) {
		jQuery('body').removeClass('search-start');
	}
});
jQuery(".searchform #s").attr('placeholder', 'Search LendGenius');

//   function shareList() {
//     var mq = window.matchMedia("(max-width: 767px)");

//     var lastScrollTop = 0;
//     var listHolder = jQuery('.share-list-holder');

//     if (mq.matches) {

//         var downScrollAnchor = 0;
//         var upScrollAnchor = 0;

//         $(window).scroll(function(event) {
//             var st = $(this).scrollTop();
//             if (st > lastScrollTop) {
//                 //console.log('downscroll');
//                 downScrollAnchor++;
//                 //console.log(downScrollAnchor);

//                 if (downScrollAnchor == 1) {
//                     listHolder.removeClass('animate-show').addClass('animate-hide');
//                     upScrollAnchor = 0;
//                     //console.log(upScrollAnchor);
//                 }

//             } else {
//                 //console.log('upscroll');
//                 upScrollAnchor++;
//                 //console.log(upScrollAnchor);
//                 if (upScrollAnchor == 1) {
//                     listHolder.removeClass('animate-hide').addClass('animate-show');
//                     downScrollAnchor = 0;
//                     //console.log(downScrollAnchor);
//                 }

//             }
//             lastScrollTop = st;
//         });
//     }
// }

function shareList() {
	var mq = window.matchMedia("(max-width: 767px)"),
		listHolder = jQuery('.share-list-holder'),
		didScroll,
		lastScrollTop = 0,
		delta = 5,
		navbarHeight = jQuery('header').outerHeight();

	jQuery(window).scroll(function () {
		didScroll = true;
	});

	setInterval(function () {

		if (didScroll) {
			if (!mq.matches) {
				return false;
			}
			//listHolder.removeClass('pre-state');
			hasScrolled();
			didScroll = false;
		}
	}, 250);

	function hasScrolled() {
		var st = jQuery(this).scrollTop();

		if (Math.abs(lastScrollTop - st) <= delta)
			return;

		if (st > lastScrollTop && st > navbarHeight) {
			// listHolder.removeClass('animate-show').addClass('animate-hide');
			listHolder.removeClass('pre-state').removeClass('animate-hide').addClass('animate-show');
			//console.log('+');
		} else {
			if (st + jQuery(window).height() < jQuery(document).height()) {
				listHolder.removeClass('pre-state').removeClass('animate-show').addClass('animate-hide');
				//console.log('-');
			}
		}

		lastScrollTop = st;
	}
}

jQuery(document).ready(function () {
	var heightHeader = 62;
	shareList();

	jQuery('.fixed-side-item').fixer({
		gap: heightHeader + 10
	});
});
jQuery(window).resize(function () {
	shareList();
});

jQuery(document).ready(function($){

	$('.table-funding [data-toggle="modal"]').on('click', function(){

			var modal      = $('#FundingModal'),
				additional = $(this).parents('tr').find('.additional');

			$('#modal_qualifications', modal).empty().append($('.qualifications li', additional).clone());
			$('#modal_pros', modal).empty().append($('.pros li', additional).clone());
			$('#modal_cons', modal).empty().append($('.cons li', additional).clone());
			$('.modal-funding-logo', modal).empty().empty().append($(this).siblings('img').clone());

		//$('.modal-funding-logo').empty().html('<img src="'+ $(this).siblings('img').attr('src') + ' />')
	})
});


jQuery(document).ready(function ($) {
	var carouselComparison = $('#carousel-сomparison');

	if (!carouselComparison.length) return;
	
	carouselComparison.carousel({
		interval: 7000
	});
	carouselComparison.carousel()
	carouselComparison.on("touchstart", function (event) {
		var xClick = event.originalEvent.touches[0].pageX;
		$(this).one("touchmove", function (event) {
			var xMove = event.originalEvent.touches[0].pageX;
			if (Math.floor(xClick - xMove) > 5) {
				carouselComparison.carousel('next');
			}
			else if (Math.floor(xClick - xMove) < -5) {
				carouselCompgitarison.carousel('prev');
			}
		});
		carouselComparison.on("touchend", function () {
			$(this).off("touchmove");
		});
	});
});