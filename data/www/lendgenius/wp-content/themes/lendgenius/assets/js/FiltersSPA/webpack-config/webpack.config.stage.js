const path = require('path');
const webpack = require('webpack');


module.exports = {
    devtool: 'nosources-source-map',
    output: {
        path: path.resolve(__dirname, '../dist'),
        publicPath: '/',
        filename: '[name].js',
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin({
            comments: false,
            compress: {warnings: false}
        }),
        new webpack.LoaderOptionsPlugin({
            options:{
                sassLoader: {
                    outputStyle: 'compressed'
                }
            }
        })
    ]
};
