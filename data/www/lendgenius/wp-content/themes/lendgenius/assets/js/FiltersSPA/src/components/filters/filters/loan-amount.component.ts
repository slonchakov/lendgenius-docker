import { Component, Input,  AfterContentInit } from '@angular/core';


@Component({
    selector: 'loan-amount',
    template: `<label>Amount seeking</label>
               <input currencyMask #LoanAmount (ngModelChange)="updateValue($event)" name="loanAmount" class="form-control"
               [(ngModel)]="innerValue"
               [options]="{ prefix: '$ ', thousands: ',', decimal: ',', align: 'left', precision: '0' }"
               [ngClass]="inputClass">
               <div class="validation-message" *ngIf="!validateCurrency(innerValue)">Requested amount should be greater than $1.000 and less than $1.000.000 </div>`
})
export class LoanAmountComponent  {


    innerValue = 1000;
    inputClass = 'valid';

    @Input() filtersValues;


    constructor () { }


    updateValue(event) {
        if (this.validateCurrency(event)) {
            this.filtersValues.loanAmount = event;
        } else {
            console.log(event);
        }
    }


    validateCurrency(value) {
        if ( isNaN(value) ) {
            return false;
        } else if ( value < 1000 || value > 1000000) {
            this.inputClass = 'invalid-component';
            return false;
        } else {
            this.inputClass = 'valid';
            return true;
        }
    }


}
