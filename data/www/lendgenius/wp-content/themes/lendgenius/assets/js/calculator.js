/**
 * Created by slan on 27.10.16.
 */

//Money Mask
function MoneyMask(elem, useDollarSign) {

    jQuery(elem).removeAttr('min').removeAttr('max').removeAttr('maxlength');
    var clonedElem = jQuery(elem).clone().insertAfter(jQuery(elem));
    jQuery(elem).attr('type', 'hidden');
    clonedElem.attr('id', clonedElem.attr('id') + 'Masked');
    clonedElem.attr('name', clonedElem.attr('id'));

    var maskUseCurrencySign = useDollarSign ? true : false;
    var maskCurrencySign = maskUseCurrencySign ? '$' : '';
    var SPMaskBehavior = function (val) {
            var compare = val.replace(/\D/g, '').length;
            if (compare === 0) { return '000000' }
            else if (compare < 4) { return maskCurrencySign + '000000' }
            else if (compare === 4) { return maskCurrencySign + '0,0000' }
            else if (compare === 5) { return maskCurrencySign + '00,0000' }
            else if (compare === 6) { return maskCurrencySign + '000,0000' }
            else if (compare > 6) { return maskCurrencySign + '0,000,000' }
        },
        spOptions = { onKeyPress: function (val, e, field, options) { field.mask(SPMaskBehavior.apply({}, arguments), options); } };

    clonedElem.mask(SPMaskBehavior, spOptions);

    clonedElem.on('change', function () {
        var value = jQuery(this).val();
        var intValue = value.replace(/[^0-9]/g, '');
        jQuery(elem).val(intValue);
    })
}

//Business Lendth Calculator
function changeFx (loan, percent, revenue) {
    var months = (loan / ((revenue * percent) / (1200))).toFixed(0);
    months = Math.max(1, months);
    var monthly = (loan / months).toFixed(0);
    monthly = Math.min(monthly, loan);
    if ((!isNaN(months)) && (!isNaN(monthly))) {
        months = months;
        monthly = monthly;
    }
    return [months, monthly];
}

function update_loan_length_calculator(months, monthly_payment){
    if(!months && !monthly_payment){
        jQuery('#res_revenue').html(0);
        jQuery('#res_amount').html(0);
        jQuery('#res_payback').html(0);
        jQuery('#res_months').html(0);
        jQuery('#res_monthly_payment').html(0);
    }else{
        jQuery('#res_revenue').html(jQuery('#revenue').val());
        jQuery('#res_amount').html(jQuery('#amount').val());
        jQuery('#res_payback').html(jQuery('#payback').val());
        jQuery('#res_months').html(months);
        jQuery('#res_monthly_payment').html(monthly_payment);
        $('.result_value').unmask().mask("#,##0.00", {reverse: true});
    }
    jQuery('.calc_result').show();
    jQuery('.cta_b').show();
    jQuery('#calc').hide();
}

function calculate_short_term(){
    var loanAmount = jQuery('#loan_amount').val(),
    loanTerm = jQuery('#payment_terms').val() * jQuery('#payment').val(),
    interestRate = jQuery('#interest_rate').val() / 1200,
    monthlyPayment = (loanAmount * (interestRate * Math.pow(1 + interestRate, loanTerm)) / (Math.pow(1 + interestRate, loanTerm) - 1)),
    totalInterest = (monthlyPayment * loanTerm - loanAmount),
    TotalRepayment = (monthlyPayment * loanTerm);
    return [monthlyPayment, totalInterest, TotalRepayment];
}

function calculate_short_term_2(){
    var loanAmount = jQuery('#loan_amount2').val(),
        loanTerm = jQuery('#payment_terms2').val() * jQuery('#payment2').val(),
        interestRate = jQuery('#interest_rate2').val() / 1200,
        interestRate2 = jQuery('#interest_rate2').val(),
        monthlyInstallment = Math.round(ExcelFormulas.PMT(interestRate, loanTerm, loanAmount*(-1))),
        effective_cost_of_loan = ExcelFormulas.EFFECT(interestRate2, loanTerm),
        TotalRepayment = monthlyInstallment * loanTerm;
    return [monthlyInstallment, effective_cost_of_loan,TotalRepayment];
}

//Invoice Factoring
function calculate_invoice_factoring(){
    var invoice_amount = parseFloat(jQuery('#invoice_amount').val()),
        advance_percentage = parseFloat(jQuery('#advance_percentage').val()/100),
        factoring_fee = parseFloat(jQuery('#factoring_fee').val())/100,
        weeks = parseFloat(jQuery('#cnt_w').val() * jQuery('#t_period').val()),
        advance_amount = invoice_amount * advance_percentage,
        ral_fee = (invoice_amount - advance_amount) - ((factoring_fee * invoice_amount * weeks) /*- 50 - 25*/),
        total_cost = invoice_amount - (advance_amount + ral_fee),
        effective_apr = (((total_cost / invoice_amount) * 52) / weeks) * 100;
    return [parseFloat(advance_amount).toFixed(1), parseFloat(ral_fee).toFixed(1), parseFloat(total_cost).toFixed(1), parseFloat(effective_apr).toFixed(1)];
}

function update_invoice_factoring_calculator(set_params){
    jQuery('#advanced_amount').html(set_params[0]);
    jQuery('#ral_fee').html(set_params[1]);
    jQuery('#total_cost').html(set_params[2]);
    jQuery('#effective_apr').html(set_params[3] + '%');
    jQuery('.calc_result').show();
    jQuery('.cta_b').show();
    jQuery('#calc').hide();
}

//Calculate Merchant Cash Advance
function calculate_merchant_cash_advance(){
    var total_advance = parseInt(jQuery('#total_advance').val()),
        factoring_rate = parseFloat(jQuery('#factoring_rate').val()),
        monthly_ccs = parseInt(jQuery('#monthly_ccs').val()),
        percent_ccs = parseFloat(jQuery('#percent_ccs').val() / 100),
        other_fees = parseFloat(jQuery('#other_fees').val()),
        daily_payment_amount = (monthly_ccs * percent_ccs) / 30,
        total_cost = ((total_advance * factoring_rate) + other_fees) - total_advance,
        total_repayment_period = (total_cost + total_advance) / daily_payment_amount,
        effective_apr = (((daily_payment_amount / total_repayment_period) * 365) / total_repayment_period) * 100;
    return [parseFloat(total_repayment_period).toFixed(1), parseFloat(total_cost).toFixed(1), parseFloat(effective_apr).toFixed(1)];
}

function update_merchant_cash_advance(set_params){
    jQuery('#total_rp').html(set_params[0]);
    jQuery('#total_cost').html(set_params[1]);
    jQuery('#effective_apr').html(set_params[2] + '%');
    jQuery('.calc_result').show();
    jQuery('.cta_b').show();
    jQuery('#calc').hide();
}

function update_short_term_calculator2(set_params){
    if(!set_params){
        jQuery('#res_loan_amount2').html(0);
        jQuery('#res_interest_paid2').html(0);
        jQuery('#res_monthly_payment2').html(0);
        jQuery('#res_total_repayment2').html(0);
    }else{
        jQuery('#res_loan_amount2').html(parseInt(jQuery('#loan_amount2').val())+'.00');
        jQuery('#res_interest_paid2').html(set_params[1].toFixed(2));
        jQuery('#res_monthly_payment2').html(set_params[0].toFixed(2));
        jQuery('#res_total_repayment2').html(set_params[2].toFixed(2));
        $('.result_value').unmask().mask("#,##0.00", {reverse: true});
    }
    jQuery('.calc_result').show();
    jQuery('.cta_b').show();
    jQuery('#calc').hide();
}

function update_short_term_calculator(set_params){
    if(!set_params){
        jQuery('#res_loan_amount').html(0);
        jQuery('#res_interest_paid').html(0);
        jQuery('#res_monthly_payment').html(0);
        jQuery('#res_total_repayment').html(0);
    }else{
        jQuery('#res_loan_amount').html(parseInt(jQuery('#loan_amount').val())+'.00');
        jQuery('#res_interest_paid').html(set_params[1].toFixed(2));
        jQuery('#res_monthly_payment').html(set_params[0].toFixed(2));
        jQuery('#res_total_repayment').html(set_params[2].toFixed(2));
        $('.result_value').unmask().mask("#,##0.00", {reverse: true});
    }
    jQuery('.calc_result').show();
    jQuery('.cta_b').show();
    jQuery('#calc').hide();
}

function format_factoring(elem){
    var value = $(elem).val();
    var intValue = value.replace(/[^0-9\.]/g, '');
    $(elem).val(intValue);
}

jQuery(document).ready(function($) {
    MoneyMask('#how_much', true);
    MoneyMask('#revenue', true);
    MoneyMask('#amount', true);
    MoneyMask('#loan_amount', true);
    MoneyMask('#loan_amount2', true);
    MoneyMask('#gross_sales', true);
    MoneyMask('#invoice_amount', true);
    MoneyMask('#total_advance', true);
    MoneyMask('#monthly_ccs', true);
    MoneyMask('#GrossRevenueMasked', true);
    MoneyMask('#BorrowAmountMasked', true);
    //MoneyMask('#factoring_fee', true);
    jQuery('#factoring_fee').on('keyup', function () {
        format_factoring(this);
    });
    jQuery('#factoring_fee').on('change', function () {
        format_factoring(this);
    });
});