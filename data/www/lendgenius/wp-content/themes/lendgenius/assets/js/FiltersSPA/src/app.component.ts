import { Component, ViewEncapsulation } from '@angular/core';
import { LoadingService } from './services/loading.service';

@Component({
  selector: 'filters-app',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.scss' ],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent  {
  name = 'Angular 4';
}
