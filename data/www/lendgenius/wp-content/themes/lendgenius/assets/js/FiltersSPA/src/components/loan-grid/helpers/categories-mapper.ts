function urlMapper () {
    return {
        'start-up' : 'Start Up',
        'term-loans' : 'Term Loans',
        'sba-loans' : 'SBA Loans',
        'short-term' : 'Short Term Business Loans',
        'personal-loans-business': 'Personal Loans for Business',
        'working-capital-loans': 'Working Capital Loans',
        'acquisition-loan': 'Business Acquisition Loan',
        'line-of-credit' : 'Business Line of Credit',
        'merchant-cash-advance' : 'Merchant Cash Advance',
        'invoice-financing' : 'Invoice Financing',
        'equipment-financing' : 'Equipment Financing',
        'best-business-credit-cards' : 'Business Credit Cards'
    };
}


export function mapCategories (categories: Array<any>) {
    let catUrl = document.location.href.split('/')[4];
    let currentCatIndex;
    if (catUrl !== undefined) {
        let currentCat = urlMapper()[catUrl];
        categories.forEach( (ele, index, array) => {
            if (ele.category === currentCat) {
                currentCatIndex = index;
            }
        });
    }
    if (currentCatIndex !== undefined) {
        let tempCat = categories[currentCatIndex];
        categories = [tempCat];
    }
    return categories;
}
