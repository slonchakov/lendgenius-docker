<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 3/22/17
 * Time: 11:09 AM
 */

?>

<div class="post">
    <h2><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h2>
    <div class="entry-content">
        <?php echo wp_trim_words(strip_shortcodes(get_the_content())); ?>
    </div>
</div>
