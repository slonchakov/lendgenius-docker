<?php
/*
    Template Name: Publisher Referral V2
*/
get_header();

$apply_now = get_post_meta( get_the_ID(), 'apply_now', true );
$sub_title = get_post_meta( get_the_ID(), 'sub_title', true );
$pre_title = get_post_meta( get_the_ID(), 'pre_title', true );
$pre_header = get_post_meta( get_the_ID(), 'pre_header', true );

$step1_title = get_post_meta( get_the_ID(), 'step1_title', true );
$step1_description = get_post_meta( get_the_ID(), 'step1_description', true );
$step2_title = get_post_meta( get_the_ID(), 'step2_title', true );
$step2_description = get_post_meta( get_the_ID(), 'step2_description', true );
$step3_title = get_post_meta( get_the_ID(), 'step3_title', true );
$step3_description = get_post_meta( get_the_ID(), 'step3_description', true );

$second_title = get_post_meta( get_the_ID(), 'second_title', true );
$second_sub_title = get_post_meta( get_the_ID(), 'second_sub_title', true );

$why_join1_title = get_post_meta( get_the_ID(), 'why_join1_title', true );
$why_join1_description = get_post_meta( get_the_ID(), 'why_join1_description', true );
$why_join2_title = get_post_meta( get_the_ID(), 'why_join2_title', true );
$why_join2_description = get_post_meta( get_the_ID(), 'why_join2_description', true );
$why_join3_title = get_post_meta( get_the_ID(), 'why_join3_title', true );
$why_join3_description = get_post_meta( get_the_ID(), 'why_join3_description', true );
$why_join4_title = get_post_meta( get_the_ID(), 'why_join4_title', true );
$why_join4_description = get_post_meta( get_the_ID(), 'why_join4_description', true );
$why_join5_title = get_post_meta( get_the_ID(), 'why_join5_title', true );
$why_join5_description = get_post_meta( get_the_ID(), 'why_join5_description', true );
?>
    <section class="bg-pattern">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="slogan-txt"><?php the_title(); ?></h1>
                    <span class="pre-slogan"><?php echo $sub_title; ?></span>
                    <div>
                        <a href="<?php echo $apply_now; ?>" class="btn btn-success btn-fixed">Apply Now</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="default-section_large">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h2>
                        <?php echo $pre_title; ?>
                    </h2>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="pre-header">
                                <?php echo $pre_header; ?>
                            </div>
                        </div>
                    </div>
                    <div class="step-description-holder">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="step-holder">
                                    <div class="step-number">
                                        <span>1</span>
                                    </div>
                                    <div class="name-step match-height">
                                        <?php echo $step1_title; ?>
                                    </div>
                                    <p>
                                        <?php echo $step1_description; ?>
                                    </p>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="step-holder">
                                    <div class="step-number">
                                        <span>2</span>
                                    </div>
                                    <div class="name-step match-height">
                                        <?php echo $step2_title; ?>
                                    </div>
                                    <p>
                                        <?php echo $step2_description; ?>
                                    </p>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="step-holder">
                                    <div class="step-number">
                                        <span>3</span>
                                    </div>
                                    <div class="name-step match-height">
                                        <?php echo $step3_title; ?>
                                    </div>
                                    <p>
                                        <?php echo $step3_description; ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="default-section_large lightgray-bg">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h2>
                        <?php echo $second_title; ?>
                    </h2>
                    <div class="pre-header">
                        <?php echo $second_sub_title; ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="bg-white join-frame">
                        <div class="join-frame-header">
                            <h3>Why Join Our Small Business Loans Referral Program?</h3>
                        </div>
                        <div class="join-frame-body">
                            <div class="join-frame-item match-height-always">
                                <div class="flex-row flex-row_columns">
                                    <i class="ref-sprite ref-sprite-dollar"></i>
                                    <div class="frame-item-name">
                                        <?php echo $why_join1_title; ?>
                                    </div>
                                    <p>
                                        <?php echo $why_join1_description; ?>
                                    </p>
                                </div>
                            </div>
                            <div class="join-frame-item match-height-always">
                                <div class="flex-row flex-row_columns">
                                    <i class="ref-sprite ref-sprite-elegance"></i>
                                    <div class="frame-item-name">
                                        <?php echo $why_join2_title; ?>
                                    </div>
                                    <p>
                                        <?php echo $why_join2_description; ?>
                                    </p>
                                </div>
                            </div>
                            <div class="join-frame-item match-height-always">
                                <div class="flex-row flex-row_columns">
                                    <i class="ref-sprite ref-sprite-speed"></i>
                                    <div class="frame-item-name">
                                        <?php echo $why_join3_title; ?>
                                    </div>
                                    <p>
                                        <?php echo $why_join3_description; ?>
                                    </p>
                                </div>
                            </div>
                            <div class="join-frame-item match-height-always">
                                <div class="flex-row flex-row_columns">
                                    <i class="ref-sprite ref-sprite-idea-money"></i>
                                    <div class="frame-item-name">
                                        <?php echo $why_join4_title; ?>
                                    </div>
                                    <p>
                                        <?php echo $why_join4_description; ?>
                                    </p>
                                </div>
                            </div>
                            <div class="join-frame-item match-height-always">
                                <div class="flex-row flex-row_columns">
                                    <i class="ref-sprite ref-sprite-phone"></i>
                                    <div class="frame-item-name">
                                        <?php echo $why_join5_title; ?>
                                    </div>
                                    <p>
                                        <?php echo $why_join5_description; ?>
                                    </p>
                                </div>
                            </div>
                            <div class="join-frame-item match-height-always">
                                <div class="flex-row flex-row_columns">
                                    <div class="frame-item-name">
                                        Start Earning
                                    </div>
                                    <a href="<?php echo $apply_now; ?>" class="btn btn-success btn-fixed">Apply Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
get_footer();
?>