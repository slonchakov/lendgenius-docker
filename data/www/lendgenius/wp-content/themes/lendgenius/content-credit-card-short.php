<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 17.10.16
 * Time: 11:40
 */

$sub_title = get_post_meta( get_the_ID(), 'sub_title', true );
$apply_url = get_post_meta( get_the_ID(), 'apply_url', true );
?>

<div class="col-xs-12 col-sm-6 col-md-4">
    <div class="card-block text-center">
        <strong class="info-title"><?php the_title(); ?></strong>
        <h3><?php echo $sub_title; ?></h3>
        <a href="<?php echo $apply_url; ?>" class="btn btn-success" target="_blank">Apply Now</a>
        <span class="info-text">on Lendgenius secure website</span>
    </div>
</div>
