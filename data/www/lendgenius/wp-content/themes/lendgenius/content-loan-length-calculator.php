<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 27.10.16
 * Time: 19:27
 */

?>
<script src="<?php echo get_stylesheet_directory_uri() ?>/js/rangeSlider/nouislider.min.js"></script>
<form id="calculator" class="l_calc-form">
    <div class="l_calc">
        <div class="l_calc__header">Loan Length Calculator</div>
        <div class="l_calc__body">
            <div class="l_calc__row">
                <div class="l_calc__cell l_calc__cell--25 l_calc__cell--label_container">
                    <label for="GrossRevenue">Your Annual
                        Gross Revenue</label>
                </div>
                <div class="l_calc__cell l_calc__cell--75">
                    <div class="input_wrapper input_wrapper--left-icon">
                        <span class="input-icon">$</span>
                        <input type="text" id="GrossRevenue" autocomplete="off"/>
                    </div>
                    <div class="error_msg">This field can not be empty</div>
                </div>
            </div>
            <div class="l_calc__row">
                <div class="l_calc__cell l_calc__cell--25 l_calc__cell--label_container">
                    <label for="BorrowAmount">Amount you wish to borrow</label>
                </div>
                <div class="l_calc__cell l_calc__cell--75">
                    <div class="input_wrapper input_wrapper--left-icon">
                        <span class="input-icon">$</span>
                        <input type="text" id="BorrowAmount" autocomplete="off" placeholder="Borrow Amount"/>
                    </div>
                    <div class="error_msg">This field can not be empty</div>
                </div>
            </div>
            <div class="l_calc_btn"><input class="culc_btn" type="submit" value="Calculate"></div>
        </div>
        <div class="l_calc__body l_calc__body--slider">
            <p class="ranger_title">Percent of Gross Revenue to Dedicate to Payback</p>
            <div class="gross_revenue"><span class="slider_value"></span>%</div>
            <div id="slider"></div>
            <input type="hidden" id="Payback"/>
            <div class="l_calc__row">
                <div class="l_calc__cell l_calc__cell--50 l_calc__cell--border-r result">
                    <div class="result_name">Length of Loan in Months</div>
                    <div class="result_value" data-calc="resultTotalMonths">0</div>
                </div>
                <div class="l_calc__cell l_calc__cell--50 result">
                    <div class="result_name">Average Monthly Payment</div>
                    <div class="result_value"><span data-calc="resultMonthlyPayment">$0</span></div>
                </div>
            </div>
        </div>
        <span class="calc-logo">Powered by<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/calc-logo.png" alt="logo"></span>
    </div>
</form>
<script type="text/javascript" >
    jQuery(document).ready(function($) {
        update_loan_length_calculator(false, false);
        jQuery('#calc_form').on( "submit", function( event ) {
            event.preventDefault();
            var revenue = jQuery('#revenue').val(),
                amount = jQuery('#amount').val(),
                payback = jQuery('#payback').val();
            calc_data = changeFx (amount, payback, revenue);
            update_loan_length_calculator(calc_data[0], calc_data[1]);
        });
    });
</script>