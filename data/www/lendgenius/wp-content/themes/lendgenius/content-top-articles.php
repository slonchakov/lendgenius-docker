<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 12.10.16
 * Time: 20:48
 */
?>

<div class="content-sub-item">
    <h2 class="item-title-primary">Top Articles</h2>
    <div class="content-blog-items in-columns">
        <?php
        $popularpost = new WP_Query( array(
            'post_type' => 'post',
            'posts_per_page' => 4,
            'meta_key' => 'wpb_post_views_count',
            'orderby' => 'meta_value_num',
            'order' => 'DESC'  ) );
        while ( $popularpost->have_posts() ) : $popularpost->the_post();

            get_template_part( 'content-popular-related', get_post_format() );

        endwhile;
        wp_reset_postdata();
        ?>
    </div>
</div>
