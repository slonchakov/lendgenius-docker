<?php
/*
    Template Name: Resources Videos
*/
get_header();
?>
<div class="content-funnel funnel-1">
    <section class="visual" style="background-image:url(<?php echo get_stylesheet_directory_uri() ?>/assets/images/bg-visual-block3.jpg");>
        <div class="container">
            <div class="cycle-gallery">
                <div class="mask">
                    <div class="slideset">
                        <div class="slide">
                            <div class="imac-holder">
                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/img-mac.jpg" alt="imac">
                                <span class="glare"></span>
                                <a href="#" class="play">play</a>
                            </div>
                            <div class="text-holder">
                                <h2>Watch’n Learn</h2>
                                <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum...</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="search-results">
        <div class="container">
            <form action="#" class="search-form">
                <input type="search" placeholder="Search...">
                <button type="submit" class="submit btn btn-primary"><i class="icon-search"></i></button>
            </form>
            <div class="block-holder">
                <div class="row">
                    <?php
                    // The Query
                    $args = array(
                        'post_type' => 'resources',
                        'posts_per_page' => 6,
                        'meta_key'     => 'resource_type',
                        'meta_value'   => 'video',
                        'orderby' => 'ID',
                        'order' => 'DESC'
                    );
                    $query = new WP_Query( $args );
                    $post_count = $query->post_count;
                    if ( $query->have_posts() ) {
                        // The Loop
                        while ( $query->have_posts() ) : $query->the_post();

                            get_template_part( 'content-video-tool', get_post_format() );

                        endwhile;
                        wp_reset_postdata();
                    }
                    ?>
                </div>
            </div>
            <!--a class="btn btn-default btn-full btn-lg font-icon-arrow-down" href="#"></a-->
        </div>
    </section>
</div>

<?php
//How much Block
get_template_part( 'content-how-much', get_post_format() );

//Features Block
get_template_part( 'content-features-block', get_post_format() );

get_footer();
