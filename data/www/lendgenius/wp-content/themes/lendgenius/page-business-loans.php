<?php
/*
    Template Name: Business Loans
*/
get_header();
?>
<<div class="lg_loans_banner">
    <h1>We Can Find The Best Financing Available For Your Business</h1>
    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/lg_loans_bg.jpg" alt="banner">
</div>

<section class="lg_loans find_loan">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2 class="main_title"><?php the_title(); ?></h2>
                <?php while ( have_posts() ) : the_post();
                    echo wpautop(get_the_content());
                endwhile;
                ?>
                <a href="<?php echo cta_button_link(); ?>" class="btn btn-success">See Loan Options</a>
            </div>
            <div class="col-md-6">


                <div class="finance_products">
                    <div class="grid">
                        <div class="product_line">
                            <?php
                            // The Query
                            $args = array(
                                'post_type' => 'loans',
                                'posts_per_page' => 9,
                                'orderby' => 'ID',
                                'order' => 'ASC'
                            );
                            $query = new WP_Query( $args );
                            if ( $query->have_posts() ) {
                                // The Loop
                                while ( $query->have_posts() ) : $query->the_post();

                                    get_template_part( 'content-business-loan-icons', get_post_format() );

                                endwhile;
                                wp_reset_postdata();
                            }
                            ?>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</section>


<section class="lg_loans browse_loan">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="main_title">Browse Our Loan Products</h2>
            </div>
        </div>

        <div class="row browse_grid">
            <?php
            // The Query
            $args = array(
                'post_type' => 'loans',
                'orderby' => 'ID',
                'order' => 'ASC'
            );
            $query = new WP_Query( $args );
            if ( $query->have_posts() ) {
                // The Loop
                while ( $query->have_posts() ) : $query->the_post();

                    get_template_part( 'content-business-loan', get_post_format() );

                endwhile;
                wp_reset_postdata();
            }
            ?>
        </div>
    </div>
</section>

<?php
//Features Block
get_template_part( 'content-features-block', get_post_format() );

//How much Block
//get_template_part( 'content-how-much', get_post_format() );

get_footer();
?>
