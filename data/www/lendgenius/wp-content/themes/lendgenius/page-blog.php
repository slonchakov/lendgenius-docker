<?php
/*
Template Name: Blog Page
*/
get_header();
?>
<div class="page-blog">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-lg-8">
            <div class="content-blog">
              <div class="content-blog-items masonry" id="ajax-posts">
                <?php
                // The Query
                $args = array(
                    'post_type' => 'post',
                    'post_status' => 'publish',
                    'posts_per_page' => 10
                );
                $query = new WP_Query( $args );
                $post_count = $query->post_count;
                if ( $query->have_posts() ) {
                  // The Loop
                  while ( $query->have_posts() ) : $query->the_post();

                    get_template_part( 'content-blog', get_post_format() );

                  endwhile;
                  wp_reset_postdata();
                }
                ?>
              </div>
              <?php if($post_count >= 10): ?>
              <div class="content-blog-reamore">
                  <a class="btn btn-default btn-full btn-lg font-icon-arrow-down" href="#" id="more_posts"></a>
              </div>
              <?php endif; ?>
            </div>
          </div>
          <div class="col-xs-12 col-lg-4">
            <?php get_sidebar('blog'); ?>
          </div>
        </div>
      </div>
    </div>
<?php
get_template_part( 'content-how-much', get_post_format() );
?>
<?php

get_footer();