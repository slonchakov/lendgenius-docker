<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 31.12.16
 * Time: 11:30
 */

?>

<div class="container">
    <div class="category-buttons-wrapper">
        <div class="mobile-only">
            <div class="item-field-select">
                <select class="form-control">
                    <option value="">Select category</option>
                    <option value="MonthlyBest">Best Offers</option>
                    <option value="balanceTransfer">Balance Transfer</option>
                    <option value="cashBack">Cash Back</option>
                    <option value="noForeignTrans">Travel</option>
                    <option value="rewards">Rewards</option>
                    <option value="lowInterest">Low Interest</option>
                    <option value="smallBusiness">Business</option>
                    <option value="averagecredit">Average Credit</option>
                    <option value="noCredit">Building Credit</option>
                    <option value="college">Student</option>
                </select>
            </div>
        </div>
        <div class="desktop-tablet-only">
            <nav class="category-buttons">
                <a class="category-buttons__jump-link" href="#">
                    <div class="category-buttons__inner-wrapper"><span class="icon MonthlyBest-credit-cards-icon"></span><span class="category-buttons__jump-link-text">Best Offers</span></div>
                </a>
                <a class="category-buttons__jump-link" href="#">
                    <div class="category-buttons__inner-wrapper"><span class="icon balanceTransfer-credit-cards-icon"></span><span class="category-buttons__jump-link-text">Balance Transfer</span></div>
                </a>
                <a class="category-buttons__jump-link" href="#">
                    <div class="category-buttons__inner-wrapper"><span class="icon cashBack-credit-cards-icon"></span><span class="category-buttons__jump-link-text">Cash Back</span></div>
                </a>
                <a class="category-buttons__jump-link" href="#">
                    <div class="category-buttons__inner-wrapper"><span class="icon noForeignTrans-credit-cards-icon"></span><span class="category-buttons__jump-link-text">Travel</span></div>
                </a>
                <a class="category-buttons__jump-link" href="#">
                    <div class="category-buttons__inner-wrapper"><span class="icon rewards-credit-cards-icon"></span><span class="category-buttons__jump-link-text">Rewards</span></div>
                </a>
                <a class="category-buttons__jump-link" href="#">
                    <div class="category-buttons__inner-wrapper"><span class="icon lowInterest-credit-cards-icon"></span><span class="category-buttons__jump-link-text">Low Interest</span></div>
                </a>
                <a class="category-buttons__jump-link" href="#">
                    <div class="category-buttons__inner-wrapper"><span class="icon smallBusiness-credit-cards-icon"></span><span class="category-buttons__jump-link-text">Business</span></div>
                </a>
                <a class="category-buttons__jump-link" href="#">
                    <div class="category-buttons__inner-wrapper"><span class="icon averagecredit-credit-cards-icon"></span><span class="category-buttons__jump-link-text">Average Credit</span></div>
                </a>
                <a class="category-buttons__jump-link" href="#">
                    <div class="category-buttons__inner-wrapper"><span class="icon noCredit-credit-cards-icon"></span><span class="category-buttons__jump-link-text">Building Credit</span></div>
                </a>
                <a class="category-buttons__jump-link" href="#">
                    <div class="category-buttons__inner-wrapper"><span class="icon college-credit-cards-icon"></span><span class="category-buttons__jump-link-text">Student</span></div>
                </a>
            </nav>
        </div>
        <div class="bcc-summary-label">
            <a class="bcc-summary-label__link" href="#">Summary<span class="icon ss-gizmo ss-plus bcc-summary-label__toggle-icon">+</span></a>
        </div>
        <section class="best-credit-card-summary">
            <h2 class="best-credit-card-summary__header">NerdWallet's Best Credit Cards</h2>
            <p>Updated: December 19, 2016</p>
            <div class="best-credit-card-summary__table-wrapper">
                <table class="best-credit-card-summary__table table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Best For...</th>
                        <th>Credit Card</th>
                        <th>Card Highlight</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="best-credit-card-summary__rewards-highlight">Rewards</td>
                        <td class="best-credit-card-summary__card-name"><a href="#">Chase Sapphire Preferred® Card</a></td>
                        <td class="best-credit-card-summary__rewards-highlight">A $625 rewards bonus for travel</td>
                    </tr>
                    <tr>
                        <td class="best-credit-card-summary__rewards-highlight">Cash Back</td>
                        <td class="best-credit-card-summary__card-name"><a href="#">Discover it® - Cashback Match™</a></td>
                        <td class="best-credit-card-summary__rewards-highlight">5% back at Amazon and Department Stores in Q4</td>
                    </tr>
                    <tr>
                        <td class="best-credit-card-summary__rewards-highlight">Average Credit</td>
                        <td class="best-credit-card-summary__card-name"><a href="#11720-averagecredit-row">Capital One® QuicksilverOne® Cash Rewards Credit Card</a></td>
                        <td class="best-credit-card-summary__rewards-highlight">1.5% cash back on all purchases</td>
                    </tr>
                    <tr>
                        <td class="best-credit-card-summary__rewards-highlight">Low Interest</td>
                        <td class="best-credit-card-summary__card-name"><a href="#">BankAmericard® Credit Card</a></td>
                        <td class="best-credit-card-summary__rewards-highlight">0% APR for 18 billing cycles and a low ongoing APR</td>
                    </tr>
                    <tr>
                        <td class="best-credit-card-summary__rewards-highlight">Travel</td>
                        <td class="best-credit-card-summary__card-name"><a href="#">Chase Sapphire Preferred® Card</a></td>
                        <td class="best-credit-card-summary__rewards-highlight">A $625 rewards bonus for travel</td>
                    </tr>
                    <tr>
                        <td class="best-credit-card-summary__rewards-highlight">Balance Transfer</td>
                        <td class="best-credit-card-summary__card-name"><a href="#">Discover it®- 18 Month Balance Transfer Offer</a></td>
                        <td class="best-credit-card-summary__rewards-highlight">0% Intro APR on Balance Transfers and Earn Rewards</td>
                    </tr>
                    <tr>
                        <td class="best-credit-card-summary__rewards-highlight">Student</td>
                        <td class="best-credit-card-summary__card-name"><a href="#">Discover it® for Students</a></td>
                        <td class="best-credit-card-summary__rewards-highlight">5% cash back on rotating bonus categories</td>
                    </tr>
                    <tr>
                        <td class="best-credit-card-summary__rewards-highlight">Business</td>
                        <td class="best-credit-card-summary__card-name"><a href="#">Capital One® Spark® Cash for Business</a></td>
                        <td class="best-credit-card-summary__rewards-highlight">2% cash back on all purchases</td>
                    </tr>
                    <tr>
                        <td class="best-credit-card-summary__rewards-highlight">Building Credit</td>
                        <td class="best-credit-card-summary__card-name"><a href="#">Discover it® Secured Card – No Annual Fee</a></td>
                        <td class="best-credit-card-summary__rewards-highlight">Cash back rewards secured card</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </section>
    </div>
</div>
