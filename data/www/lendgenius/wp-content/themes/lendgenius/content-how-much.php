<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 13.10.16
 * Time: 15:51
 */
?>
<div class="content-funnel funnel-1">
<div class="content-how-much">
    <div class="container">
        <div class="how-much-wraper">
            <div class="how-much-title">Ready to Get Down to Business?</div>
            <div class="how-much-form">
                <div class="row">
                    <div class="col-lg-12">
                        <a href="<?php echo cta_button_link(); ?>" class="btn btn-default-success">See Loan Options</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>