<?php
/*
    Template Name: Publisher Partner V2
*/
get_header();

$referral_program = get_post_meta( get_the_ID(), 'referral_program', true );
$referral_program = apply_filters('the_content', $referral_program);
$affiliate_program = get_post_meta( get_the_ID(), 'affiliate_program', true );
$affiliate_program = apply_filters('the_content', $affiliate_program);

$referral_url = get_post_meta( get_the_ID(), 'referral_url', true );
$affiliate_url = get_post_meta( get_the_ID(), 'affiliate_url', true );
$sub_title = get_post_meta( get_the_ID(), 'sub_title', true );
?>
<section class="bg-pattern">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1 class="slogan-txt"><?php the_title(); ?></h1>
                <span class="pre-slogan"><?php echo $sub_title; ?></span>
            </div>
        </div>
    </div>
</section>
<section class="sky-bg default-section">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="program-placeholder">
                    <h4 class="line-success-bottom">Referral Program</h4>
                    <?php echo $referral_program; ?>
                    <a href="<?php echo $referral_url; ?>" class="btn btn-success btn-success-bordered">Learn More <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="program-placeholder">
                    <h4 class="line-success-bottom">Affiliate Program</h4>
                    <?php echo $affiliate_program; ?>
                    <a href="<?php echo $affiliate_url; ?>" class="btn btn-success btn-success-bordered">Learn More <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
get_footer();
?>
