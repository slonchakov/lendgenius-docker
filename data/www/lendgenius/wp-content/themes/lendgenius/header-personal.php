<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="google-site-verification" content="0vHphrtA59XVVa8JNf9MU2PtfnAMp1UfQTKudNEurAc" />
    <link href="https://plus.google.com/114339560599887475172" rel="publisher" />
    <?php if ( is_front_page() ) { ?>
        <meta name="msvalidate.01" content="D07588DA24884897E4C89F3F32048F38" />
    <?php } ?>
    <?php /*elegant_description(); ?>
	<?php elegant_keywords(); ?>
	<?php elegant_canonical(); */?>

    <?php do_action( 'et_head_meta' ); ?>

    <link rel="preconnect" href="https://addshoppers.s3.amazonaws.com">
    <link rel="preconnect" href="https://fl.adpxl.co">
    <link rel="preconnect" href="https://ip.freshmarketer.com">
    <link rel="preconnect" href="https://dx.steelhousemedia.com">
    <link rel="preconnect" href="https://px.steelhousemedia.com">


    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

    <?php $template_directory_uri = get_template_directory_uri(); ?>
    <!--[if lt IE 9]>
    <script src="<?php echo esc_url( $template_directory_uri . '/js/html5.js' ); ?>" type="text/javascript"></script>
    <![endif]-->

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />

    <title><?php wp_title( '|', true, 'right' ); ?></title>

    <?php wp_head(); ?>


    <link rel="SHORTCUT ICON" href="/favicon.ico">

    <link rel="stylesheet" href="/wp-content/themes/lendgenius/style-2018/css/nouislider.min.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/assets/styles/icomoon.min.css?ver=<?php echo get_option( 'sts_script_version' ); ?>">
    <link rel="stylesheet" href="/wp-content/themes/lendgenius/style-2018/css/header-footer.min.css?ver=<?php echo get_option( 'sts_script_version' ); ?>">

    <link rel="stylesheet" href="/wp-content/themes/lendgenius/style-2018/css/style.css?ver=<?php echo get_option( 'sts_script_version' ); ?>">

</head>

<body <?php body_class(); ?>>

<?php
$exclude_header_pages = [
    'page-template-blank.php',
    'page-under-constraction.php',
    'page-funnel-3.php',
    'page-funnel-4.php',
    'page-blank-iframe.php',
    'landing-gmail-1.php'
];


if ( (!is_page_template( $exclude_header_pages ) &&
        //strpos($page_template,'page-landing') === false &&
        !is_404() ) || is_search() ){
    get_template_part('views/headers/header-top-content');
}