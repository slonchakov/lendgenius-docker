;(function(){

    // Scroll to top form
    $('.short_questions__item').on('click', function(e){
        e.preventDefault();
        
        $("body, html").animate({
            scrollTop: 0
        }, 500)
    });


    var nav       = $('.menu-right-menu-container'),
        navButton = $('.nav-button');

    navButton.on('click',function(){
        navButton.toggleClass('active');
        nav.toggleClass('active');
    });

    $('.menu-item-has-children > a').on('click',function(e){
        e.preventDefault();
        var item = $(this).parent();
        toggleItem(item);
    });

    $(window).on('scroll', function(){
        if ($(this).width() < 992 && !nav.hasClass('active')) return;
        closeNav();
    });

    $(document).on('click', function(e){
        var elem = $(e.target);

        if (!elem.closest('.nav-menu').length && nav.hasClass('active')) {
            closeNav();
        }
    });

    function closeNav (){
        navButton.removeClass('active');
        nav.removeClass('active');
    }

    function toggleItem(item){
        item.toggleClass('active').siblings('.menu-item-has-children').removeClass('active').find('.sub-menu').removeClass('active');
        item.find('.sub-menu').toggleClass('active');
    }


    $('.menu-item-has-children > span').on('click',function(e){
        e.preventDefault();
        var item = $(this).parent();
        toggleItem(item);
    });

})();
