<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 4/25/17
 * Time: 9:47 PM
 */

get_header();

?>
    <div class="page-blog">
        <div class="container container-md">
            <div class="row">
                <?php

                // Start the loop.
                while ( have_posts() ) : the_post(); ?>
                    <div class="col-xs-12">
                        <div class="content-blog">
                            <div class="blog-item">
                                <div class="item-title">
                                    <?php
                                    if ( is_single() ) :
                                        the_title();
                                    else :
                                        the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
                                    endif;
                                    ?></div>
                                <?php
                                // Author info.
                                if ( is_single() ) :
                                    get_template_part( 'single-author-info' );
                                endif;
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-lg-8">

                        <div class="content-blog content-rel-blog">
                            <?php
                            ;                         get_template_part( 'content-single', get_post_format() );
                            ?>

                        </div>

                        <?php get_template_part( 'content-custom-articles', get_post_format() ); ?>
                        <?php if ( is_active_sidebar( 'under-post-area' )  ) : ?>
                            <?php dynamic_sidebar( 'under-post-area' ); ?>
                        <?php endif; ?>
                        <?php
                        // If comments are open or we have at least one comment, load up the comment template.
                        if ( comments_open() || get_comments_number() ) :
                            comments_template();
                        endif;
                        ?>
                    </div>

                <?php endwhile; ?>
                <div class="col-xs-12 col-lg-4">
                    <?php
                    //Sidebar
                    get_sidebar('single');
                    ?>
                </div>
            </div>

        </div>

    </div>

<?php
get_footer();
