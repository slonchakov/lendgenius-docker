<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 12.10.16
 * Time: 16:43
 */

if ( is_active_sidebar( 'sidebar-how-it-works' )  ) : ?>
    <div class="content-aside">
        <?php dynamic_sidebar( 'sidebar-how-it-works' ); ?>
    </div>
<?php endif; ?>