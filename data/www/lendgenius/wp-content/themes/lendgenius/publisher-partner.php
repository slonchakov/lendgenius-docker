<?php
/*
    Template Name: Publisher Partner
*/
get_header();

$referral_program = get_post_meta( get_the_ID(), 'referral_program', true );
$referral_program = apply_filters('the_content', $referral_program);
$affiliate_program = get_post_meta( get_the_ID(), 'affiliate_program', true );
$affiliate_program = apply_filters('the_content', $affiliate_program);

$referral_url = get_post_meta( get_the_ID(), 'referral_url', true );
$affiliate_url = get_post_meta( get_the_ID(), 'affiliate_url', true );
?>

<section class="main-programm">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 title-page">
                <h1><?php the_title(); ?></h1>
                <p><?php echo get_the_content(); ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 programm-inflow">
                <div class="programm-holder programm-left">
                    <h2>Referral Program</h2>
                    <div class="programm-desk match-height">
                        <?php echo $referral_program; ?>
                    </div>
                    <a href="<?php echo $referral_url; ?>">Referral Program</a>
                </div>
            </div>
            <div class="col-sm-6 programm-inflow">
                <div class="programm-holder programm-right br-lg">
                    <h2>Affiliate Program</h2>
                    <div class="programm-desk match-height">
                        <?php echo $affiliate_program; ?>
                    </div>
                    <a href="<?php echo $affiliate_url; ?>">Affiliate Program</a>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
get_footer();
?>
