<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>

    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="google-site-verification" content="0vHphrtA59XVVa8JNf9MU2PtfnAMp1UfQTKudNEurAc" />
    <link href="https://plus.google.com/114339560599887475172" rel="publisher" />
    <?php if ( is_front_page() ) { ?>
        <meta name="msvalidate.01" content="D07588DA24884897E4C89F3F32048F38" />
    <?php } ?>

    <?php do_action( 'et_head_meta' ); ?>

    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

    <?php $template_directory_uri = get_template_directory_uri(); ?>

    <!--[if lt IE 9]>
    <script src="<?php echo esc_url( $template_directory_uri . '/js/html5.js"' ); ?>" type="text/javascript"></script>
    <![endif]-->

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />

    <title><?php wp_title( '|', true, 'right' ); ?></title>

    <?php wp_head(); ?>

    <link rel="SHORTCUT ICON" href="/favicon.ico">

    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/assets/styles/icomoon.min.css?ver=<?php echo get_option( 'sts_script_version' ); ?>">
    <link rel="stylesheet" href="/wp-content/themes/lendgenius/style-2018/css/header-footer.css">
    <link rel="stylesheet" href="/wp-content/themes/lendgenius/style-2018/css/unsubscribe.css">

</head>
<body>

<?php get_template_part('views/headers/header-top-content');