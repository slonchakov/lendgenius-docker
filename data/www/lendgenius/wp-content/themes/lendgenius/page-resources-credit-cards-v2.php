<?php
/*
    Template Name: Resources Credit Cards (v2)
*/
get_header();
?>
<section class="loans_subpage_banner" style="background-image:url('<?php echo get_stylesheet_directory_uri() ?>/assets/images/publesher-portal/affilate-pattern.jpg')">
    <div class="darken-lining">
        <div class="container"> <!--.container-fluid (if full-screen)-->
            <div class="row flex flex-align-stretch flex-column-xs">

                <div class="col-sm-12 flex flex-align-center"> <!-- col-sm-5 col-md-7 -->
                    <div class="text-center center-block">
                        <h1><?php the_title(); ?></h1>
                        <p><?php the_excerpt(); ?></p>
                    </div>
                </div>

                <!-- <div class="col-sm-5 col-md-7 flex flex-align-center">
                    <div class="">
                        <h1><?php the_title(); ?></h1>
                        <?php the_excerpt(); ?>
                    </div>
                </div>
                <div class="col-sm-7 col-md-5 banner-form-wrapper">
                    <form-app>
                        <div class="loading-form-ng">
                            <div class="spinner-ng">Loading...</div>
                        </div>
                    </form-app>
                </div> -->
            </div>
        </div>
    </div>
</section>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <?php
            breadcrumbs('<div id="breadcrumbs" class="breadcrumbs">','</div>');
            ?>
        </div>
    </div>
</div>
<section class="at_a_glance" id="glance">
    <div class="container">
        <div class="row">
            <div id="at_glance" class="col-md-8 col-sm-7">
                <?php the_content(); ?>
            </div>
            <div class="col-md-4 col-sm-5">
                <div class="header-list">
                    Quick jump to
                </div>
                <ul class="jump-list">

                </ul>
            </div>
        </div>
    </div>
</section>
<section class="b-cards lightgray-bg text-center">
    <div class="container">
       <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <h2 id="top-headline" class="h3">
                    Here are our top picks for the best credit cards available<br> to small businesses
                </h2>
            </div>
       </div>
       <div class="row">
           <div class="col-lg-12 panel-cards">
               <?php
               // The Query
               $args = array(
                   'post_type' => 'resources',
                   'meta_key'     => 'resource_type',
                   'meta_value'   => 'ccard',
                   'orderby' => 'ID',
                   'order' => 'DESC'
               );
               $query = new WP_Query( $args );
               $post_count = $query->post_count;
               if ( $query->have_posts() ) {
                   // The Loop
                   while ( $query->have_posts() ) : $query->the_post();

                       get_template_part( 'views/parts/credit-cards-grid', get_post_format() );

                   endwhile;
                   wp_reset_postdata();
               }
               ?>
           </div>
       </div>
    </div>
</section>
<div class="b-cards-description">
    <div class="container">

        <?php
        // The Query
        $args = array(
            'post_type' => 'resources',
            'meta_key'     => 'resource_type',
            'meta_value'   => 'ccard',
            'orderby' => 'ID',
            'order' => 'DESC'
        );
        $query = new WP_Query( $args );
        $post_count = $query->post_count;
        if ( $query->have_posts() ) {
            // The Loop
            while ( $query->have_posts() ) : $query->the_post();

                get_template_part( 'views/parts/credit-cards-list', get_post_format() );

            endwhile;
            wp_reset_postdata();
        }
        ?>
    </div>
</div>
<div class="panel-cards_toggle">
    <span class="browse on">Browse Cards</span>
</div>
<?php

get_footer();
?>
<script>
jQuery(document).ready(function($){

    var list = "", $jumpList = $('.jump-list');

    $('h2').each(function(){
        var $this = $(this),
            href  = ($this.attr('id')) ? $this.attr('id') : $this.parent().attr('id'),
            text  = $this.text();

        list += '<li><a href="#'+ href +'">'+ text +'</a></li>';
    });

    $jumpList.empty().append(list);

    $jumpList.on('click', 'a', function(e){
        e.preventDefault();

        var goTo = $(this).attr('href');

        $('html,body').animate({scrollTop: $(goTo).offset().top - 165 },1000)
    })

});
</script>