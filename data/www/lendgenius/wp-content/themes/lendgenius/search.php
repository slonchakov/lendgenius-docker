<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 3/22/17
 * Time: 10:44 AM
 */

$excluded_posts_result = new WP_Query([
    'meta_key' => 'exclude_from_search',
    'meta_value_num' => 'true',
    'meta_compare' => '==',
    'fields' => 'ids',
    'post_type' => ['post','page'],
    'post_per_page' => -1
]);

$excluded_posts = [];
if ( $excluded_posts_result->have_posts() ) {
    while ( $excluded_posts_result->have_posts() ) : $excluded_posts_result->the_post();
        $excluded_posts[] =  $post;
    endwhile;
}

get_header();
?>
<div class="search-results-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="subsectiontitle">Search results for</h2>
                <?php get_search_form(); ?>
            </div>
        </div>
    </div>
</div>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <?php
                /* Search Count */
                $args = [
                    's' => $s,
                    'showposts' => -1,
                    'post__not_in' => $excluded_posts,
                ];
                $allsearch = new WP_Query($args);
                ?>
                <?php if ( have_posts() ) : ?>
                    <div class="countOfWrapper">
                        <p><?php echo $allsearch->post_count; ?> results</p>
                    </div>
                    <?php while ( $allsearch->have_posts() ) : $allsearch->the_post();
                        get_template_part( 'content-search-post', get_post_format() );
                    endwhile; ?>
                    <!--ul class="page-numbers">
                        <li>
                            <span class="page-numbers current">1</span>
                        </li>
                        <li>
                            <a class="page-numbers" href="">2</a>
                        </li>
                        <li>
                            <a class="page-numbers" href="#">3</a>
                        </li>
                        <li>
                            <span class="page-numbers dots">...</span>
                        </li>
                        <li>
                            <a class="page-numbers" href="#">26</a>
                        </li>
                        <li>
                            <a class="page-numbers" href="#">27</a>
                        </li>
                        <li>
                            <a class="page-numbers" href="#">28</a>
                        </li>
                        <li>
                            <a class="next page-numbers" href="#">Next</a>
                        </li>
                    </ul-->
                <?php endif; ?>
            </div>
            <div class="col-md-4">

            </div>
        </div>
    </div>
<?php get_footer(); ?>