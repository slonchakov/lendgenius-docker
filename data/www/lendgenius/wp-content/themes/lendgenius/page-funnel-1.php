<?php
/*
    Template Name: Funnel-1
*/
get_header();
?>
<div class="content-funnel funnel-1">
    <div class="content-how-much">
        <div class="content-how-much-wraper">
            <div class="container">
                <div class="how-much-wraper">
                    <div class="how-much-title">How Much Does Your Business need?</div>
                    <div class="how-much-search-wraper">
                        <form action="<?php echo cta_button_link(); ?>">
                            <div class="how-much-search">
                                <input class="field-input field-input-big field-input-clear" name="how_much" id="how_much" type="tel" placeholder="How Much Does Your Business Need?">
                            </div>
                            <div class="how-much-button">
                                <button class="field-button btn btn-success" type="submit">See Loan Options</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="steps-list">
        <div class="steps-list-wraper">
            <div class="container">
                <div class="steps-list-container">
                    <div class="steps-items row">
                        <div class="item col-lg-4 col-sm-6">
                            <div class="item-image">
                                <div class="item-image-src font-icon-online_form"></div>
                            </div>
                            <div class="item-description">Fill out</div>
                            <div class="item-title">Online Form</div>
                        </div>
                        <div class="item col-lg-4 col-sm-6">
                            <div class="item-image">
                                <div class="item-image-src font-icon-loan_options"></div>
                            </div>
                            <div class="item-description">Know your</div>
                            <div class="item-title">Loan Options</div>
                        </div>
                        <div class="item col-lg-4 col-sm-6">
                            <div class="item-image">
                                <div class="item-image-src font-icon-money_aproved"></div>
                            </div>
                            <div class="item-description">Sometimes in less than 24 hr</div>
                            <div class="item-title">Get Funds</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="our-customers-happy-stories">
        <div class="our-customers-happy-stories-wraper">
            <div class="container">
                <div class="our-customers-happy-stories-container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="item-title">Our Customer’s Happy Stories</div>
                            <div class="item-text">Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit.</div>
                            <div class="item-author"><strong>Roberta Scott</strong>, Bakery House</div>
                            <div class="item-readmore"><a href="#">See more stories</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();
