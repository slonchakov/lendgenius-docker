<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 16.10.16
 * Time: 22:02
 */
?>

<div class="col-xs-12 col-sm-6 col-md-4">
    <?php
    // Post thumbnail.
    single_post_thumbnail();
    ?>
    <div class="text-container">
        <h2><?php the_title(); ?></h2>
        <p><?php the_content(); ?></p>
    </div>
</div>
