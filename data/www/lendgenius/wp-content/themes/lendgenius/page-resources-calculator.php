<?php
/*
    Template Name: Resources Calculator
*/
get_header();

$count_of_calculators = get_post_meta( get_the_ID(), 'count_of_calculators', true );
$calculator_type = get_post_meta( get_the_ID(), 'calculator_type', true );
$content_part_2 = get_post_meta( get_the_ID(), 'content_part_2', true );
?>

<section class="calc2_page">
    <div class="calc_page_title">
        <div class="container">
            <div class="row">
                <i class="icon-calculator"></i>
                <h1><?php the_title(); ?></h1>
            </div>
        </div>
    </div>

    <?php if($count_of_calculators == 'two'): ?>
    <div class="how_to_calculate">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-center main_title">
                        Choose How You Want To Calculate
                    </h2>
                    <div class="calculate_buttons">
                        <a href="#apr">Calculator by APR</a>
                        <a href="#installment">Calculator by Installment</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>

    <div class="main_content">
        <div class="container">
            <div class="row">
                <div class="col-md-12" id="apr">
                    <?php
                    if($calculator_type == 'loan_length_calc') {
                        get_template_part('content-loan-length-calculator', get_post_format());
                    }elseif($calculator_type == 'short_term_calc'){
                        get_template_part('views/content/short-term-calculator', get_post_format());
                    }elseif($calculator_type == 'invoice_calc'){
                        get_template_part('views/content/invoice-factoring-calculator', get_post_format());
                    }elseif($calculator_type == 'merchant_cash_advance'){
                        get_template_part('views/content/merchant-cash-advance', get_post_format());
                    }
                    ?>
                    <div class="content">
                    <?php
                    // Start the loop.
                    while ( have_posts() ) : the_post();
                        echo the_content();
                    endwhile; ?>

                        <div class="more_calculators">
                            <h3>more calculators</h3>
                            <ul>
                                <?php
                                // The Query
                                $args_c = array(
                                    'post_type' => 'page',
                                    'post_parent' => wp_get_post_parent_id(get_the_ID()),
                                    'post__not_in' => array(get_the_ID()),
                                    'posts_per_page' => 2,
                                    'orderby' => 'rand'
                                );
                                $query_c = new WP_Query( $args_c );
                                if ( $query_c->have_posts() ) {
                                    // The Loop
                                    while ( $query_c->have_posts() ) : $query_c->the_post(); ?>
                                        <li>
                                            <i class="icon-calculator"></i>
                                            <a href="<?php echo get_permalink(); ?>"><?php the_title();?></a>
                                        </li>
                                    <?php endwhile;
                                    wp_reset_postdata();
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if($count_of_calculators == 'two'): ?>
    <div class="main_content">
        <div class="container">
            <div class="row">
                <div class="col-md-12" id="installment">
                    <?php
                    if($calculator_type == 'short_term_calc'){
                        get_template_part('views/content/short-term-calculator2', get_post_format());
                    }
                    ?>
                    <div class="content">
                        <?php echo $content_part_2; ?>
                        <div class="more_calculators">
                            <h3>more calculators</h3>
                            <ul>
                                <?php
                                // The Query
                                $args_c = array(
                                    'post_type' => 'page',
                                    'post_parent' => wp_get_post_parent_id(get_the_ID()),
                                    'post__not_in' => array(get_the_ID()),
                                    'posts_per_page' => 2,
                                    'orderby' => 'rand'
                                );
                                $query_c = new WP_Query( $args_c );
                                if ( $query_c->have_posts() ) {
                                    // The Loop
                                    while ( $query_c->have_posts() ) : $query_c->the_post(); ?>
                                        <li>
                                            <i class="icon-calculator"></i>
                                            <a href="<?php echo get_permalink(); ?>"><?php the_title();?></a>
                                        </li>
                                    <?php endwhile;
                                    wp_reset_postdata();
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <?php
    //Get Finance Products
    get_template_part( 'content-finance-products', get_post_format() );
    ?>
</section>
<?php
//How much Block
get_template_part( 'content-how-much', get_post_format() );

get_footer();
