﻿<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 17.10.16
 * Time: 23:10
 * OLD Sinle LOans
 */

get_header();
$the_ID = get_the_ID();
$loan_icon = get_post_meta( $the_ID, 'loan_icon', true );
$item_1_title = get_post_meta( $the_ID, 'item_1_title', true );
$item_1_description = get_post_meta( $the_ID, 'item_1_description', true );
$item_2_title = get_post_meta( $the_ID, 'item_2_title', true );
$item_2_description = get_post_meta( $the_ID, 'item_2_description', true );
$item_3_title = get_post_meta( $the_ID, 'item_3_title', true );
$item_3_description = get_post_meta( $the_ID, 'item_3_description', true );
$item_4_title = get_post_meta( $the_ID, 'item_4_title', true );
$item_4_description = get_post_meta( $the_ID, 'item_4_description', true );
$features_pros = get_post_meta( $the_ID, 'features_pros', true );
$features_cons = get_post_meta( $the_ID, 'features_cons', true );
$header_description = get_post_meta( $the_ID, 'header_description', true );

?>
    <div class="loans_subpage_banner" style="background-image:url('<?php echo get_stylesheet_directory_uri() ?>/assets/images/loans_subpage.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 white-txt">
                    <h1><?php the_title(); ?></h1>
                    <p>
                        <?php echo $header_description; ?>
                    </p>
                    <div class="margin-top-30">
                        <a href="<?php echo cta_button_link(); ?>" class="btn btn-success btn-x2">See Loan Options</a>
                    </div>
                </div>

                <div class="col-sm-6" style="display:none">

                    <form-app>
                        <div class="loading-form-ng">
                            <div class="spinner-ng">Loading...</div>
                        </div>
                    </form-app>

                </div>
            </div>
        </div>
    </div>
    <div id="sticky_articles">
        <div class="subpage_menu" id="sticky_menu">
            <div class="container">
      <span class="mobile_menu_icon">
        <i class="fa fa-bars" aria-hidden="true"></i> <span>Menu</span>
      </span>
                <ul class="menu">
                    <li class="active">
                        <a href="#glance">at a glance</a>
                    </li>
                    <li>
                        <a href="#features">Features</a>
                    </li>
                    <li>
                        <a href="#how_works">how it works</a>
                    </li>
                    <li>
                        <a href="#cost">cost</a>
                    </li>
                    <li>
                        <a href="#qualifies">who qualifies</a>
                    </li>
                    <li>
                        <a href="#how_to_apply">how to apply</a>
                    </li>
                </ul>
            </div>
        </div>

        <section class="at_a_glance" id="glance">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="main_title">At a Glance</h2>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <?php the_excerpt(); ?>
                    </div>
                    <div class="col-md-3 col-xs-6 small_grid">
                        <div class="col">
                            <div class="image">
                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/amount.png" alt="amount">
                            </div>
                            <div class="content">
                                <b><?php echo $item_1_title; ?></b>
                                <span><?php echo $item_1_description; ?></span>
                            </div>
                        </div>
                        <div class="col">
                            <div class="image">
                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/term.png" alt="term">
                            </div>
                            <div class="content">
                                <b><?php echo $item_3_title; ?></b>
                                <span><?php echo $item_3_description; ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-6 small_grid">
                        <div class="col">
                            <div class="image">
                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/rates.png" alt="rates">
                            </div>
                            <div class="content">
                                <b><?php echo $item_2_title; ?></b>
                                <span><?php echo $item_2_description; ?></span>
                            </div>
                        </div>
                        <div class="col">
                            <div class="image">
                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/speed.png" alt="speed">
                            </div>
                            <div class="content">
                                <b><?php echo $item_4_title; ?></b>
                                <span><?php echo $item_4_description; ?></span>
                            </div>
                        </div>
                    </div>

                    <!--div class="col-md-12 next_btn">
                        <a href="#" class="">
                            <span>Next</span>
                        </a>
                        <i class="item-src icomoon font-icon font-icon-equipment"></i>
                        <a href="#" class="">
                            <h6>Equipment Financing</h6>
                        </a>
                    </div-->
                </div>
            </div>
        </section>

        <?php if($features_pros || $features_cons): ?>
            <section class="sba_loans_features" id="features">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <h2 class="main_title">Advantages & Disadvantages</h2>
                        </div>
                        <div class="col-sm-6 success">
                            <?php echo $features_pros; ?>
                        </div>
                        <div class="col-sm-6 error">
                            <?php echo $features_cons; ?>
                        </div>
                    </div>
                </div>
            </section>
        <?php endif; ?>

        <section class="loans_subpage simple_content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 loan-content">
                        <?php
                        // Start the loop.
                        while ( have_posts() ) : the_post();
                            the_content();
                        endwhile; ?>
                    </div>
                    <div class="col-sm-4">
                        <?php
                        //Sidebar
                        get_sidebar('single');
                        ?>
                    </div>
                </div>
            </div>
        </section>
    </div>

<?php
//Get Finance Products
get_template_part( 'content-finance-products.php', get_post_format() );
?>


    <section class="top_articles">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <?php get_template_part( 'content-custom-articles', get_post_format() ); ?>
                </div>

                <div class="col-md-4">
                    <?php
                    //Sidebar
                    get_sidebar('single');
                    ?>
                </div> <!-- END col-md-4 -->

            </div>
        </div>
    </section>

<?php
//Features Block
get_template_part( 'content-features-block', get_post_format() );

get_footer();