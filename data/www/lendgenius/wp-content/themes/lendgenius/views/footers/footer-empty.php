<?php
$template_directory_uri   = get_template_directory_uri();
?>
<script src="<?php echo $template_directory_uri; ?>/assets/js/jquery-1.11.2.min.js"></script>
<script src="<?php echo $template_directory_uri; ?>/js/jquery.mask.min.js"></script>
<script src="<?php echo $template_directory_uri; ?>/assets/js/bootstrap.min.js"></script>
<?php
wp_footer();
if(is_page_template( 'landing-gmail-1.php' )){
    if(get_option( 'sts_enable_form' ) == 'on'){ ?>
        <script type="text/javascript">
            window.applicationOptions = {
                sendShortForm: '<?php echo get_option( 'sts_endpoint_url' ); ?>',
                leadsource: '<?php echo get_option( 'sts_leadSource' ); ?>',
                c: <?php echo get_option( 'sts_campaignId' ); ?>,
                zipCode: '<?php echo get_option( 'sts_zipcode' ); ?>',
                emailValidateUrl: '<?php echo get_option( 'sts_email_v_url' ); ?>',
                phoneValidateUrl: '<?php echo get_option( 'sts_phone_v_url' ); ?>',
                RequestedAmount: '<?php echo $_POST['RequestedAmount']; ?>',
                CreditScore: '<?php echo $_POST['CreditScore']; ?>',
                MonthlySales:'<?php echo $_POST['MonthlySales']; ?>',
                MonthsInBusiness:'<?php echo $_POST['MonthsInBusiness']; ?>'
            }
        </script>
        <!--            <script src="--><?php //echo get_option( 'sts_script_url' ); ?><!--/form_3/form-loader.js?ver=--><?php //echo get_option( 'sts_script_version' ); ?><!--"></script>-->
    <?php } ?>
    <?php
}
echo '</body></html>';