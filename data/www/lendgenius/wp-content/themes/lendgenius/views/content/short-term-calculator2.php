<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 31.10.16
 * Time: 14:02
 */

?>

<div class="calculator calc-flow right">
    <div class="title_block">
        <h4>Term Loan Calculator</h4>
        <span class="subtitle">By interest rate</span>
    </div>
    <div class="calc_container">
        <form action="#" method="post" id="calc_form2">
            <label for="loan_amount2">Loan Amount, $</label>
            <input type="text" id="loan_amount2">

            <label for="monthly_payment2">Payment Terms</label>
            <span class="calc_term">
                <select id="payment_terms2" class="field-styler-option field-input-biger">
                    <option value="1">Months</option>
                    <option value="12">Years</option>
                </select>
                <input type="text" id="payment2">
            </span>

            <label for="interest_rate2">Interest Rate, %</label>
            <input type="text" id="interest_rate2">

            <input type="submit" value="Calculate" id="calc">
            <a class="cta_b culc_btn" href="<?php echo cta_button_link(); ?>" style="display: none;">See Loan Options</a>
        </form>
    </div>
    <div class="calc_result" style="display: none;">
        <h5>Calculation Result</h5>
        <div class="result_line">
            <span>Loan Amount, $</span>
            <strong id="res_loan_amount2" class="result_value">-</strong>
        </div>

        <div class="result_line">
            <span>Effective Cost of Loan, %</span>
            <strong id="res_interest_paid2" class="result_value">-</strong>
        </div>

        <div class="result_line">
            <span>Monthly Payment, $</span>
            <strong id="res_monthly_payment2" class="result_value">-</strong>
        </div>

        <div class="result_line">
            <span>Total Repayment, $</span>
            <strong id="res_total_repayment2" class="result_value">-</strong>
        </div>
    </div>
    <span class="calc-logo">Powered by<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/calc-logo.png" alt="logo"></span>
</div>
<script type="text/javascript" >
    jQuery(document).ready(function($) {
        jQuery('#calc_form2').on( "submit", function( event ) {
            event.preventDefault();
            calc_data = calculate_short_term_2();
            update_short_term_calculator2(calc_data);
        });
    });
</script>
