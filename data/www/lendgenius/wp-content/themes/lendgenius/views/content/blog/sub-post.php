<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 04.01.18
 *
 */
?>
<div class="blog-item-sub">
    <div class="blog-item-sub-img">
        <a href="<?php echo get_permalink(); ?>">
            <?php
            // Post thumbnail.
            //imgpath(single_post_thumbnail('author-thumb-posts', ''));
            ?>

            <img class="img-lazy" src="<?= imgpath(get_stylesheet_directory_uri() . '/style-2018/img/empty.png?v=3') ?>" data-src="<?= imgpath(get_the_post_thumbnail_url($post, 'author-thumb-posts')) ?>" alt="">
        </a>
    </div>
    <h4 class="blog-item-sub-title matchheight">
        <a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a>
    </h4>
    <div class="blog-item-sub-author"><span class="medium"><?php echo do_shortcode('[est_time]'); ?> read</span> &nbsp; <span><a href="<?php echo get_permalink(get_post_meta( get_the_ID() , 'author' , true )); ?>"><?php echo get_the_title(get_post_meta( get_the_ID() , 'author' , true )); ?></a></span> <i class="fa fa-circle" aria-hidden="true"></i> <span><?php echo get_the_date() ?></span></div>
</div>