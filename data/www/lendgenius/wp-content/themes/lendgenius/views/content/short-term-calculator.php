<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 28.10.16
 * Time: 17:39
 */

?>
<div class="calculator calc-flow left">
    <div class="title_block">
        <h4><?php the_title(); ?></h4>
        <span class="subtitle">By interest rate</span>
    </div>
    <div class="calc_container">
        <form id="calc_form" method="post">
            <label for="loan_amount">Loan Amount, $</label>
            <input type="text" id="loan_amount">

            <label for="monthly_payment">Payment Terms</label>
            <span class="calc_term">
                <select id="payment_terms" class="field-styler-option field-input-biger">
                    <option value="1">Months</option>
                    <option value="12">Years</option>
                </select>
                <input type="text" id="payment">
            </span>

            <label for="interest_rate">Interest Rate, %</label>
            <input type="text" id="interest_rate">

            <input type="submit" value="Calculate" id="calc">
            <a class="cta_b culc_btn" href="<?php echo cta_button_link(); ?>" style="display: none;">See Loan Options</a>
        </form>
    </div>
    <div class="calc_result" style="display: none;">
        <h5>Calculation Result</h5>
        <div class="result_line">
            <span>Loan Amount, $</span>
            <strong id="res_loan_amount" class="result_value">-</strong>
        </div>

        <div class="result_line">
            <span>Interest Paid, %</span>
            <strong id="res_interest_paid" class="result_value">-</strong>
        </div>

        <div class="result_line">
            <span>Monthly Payment, $</span>
            <strong id="res_monthly_payment" class="result_value">-</strong>
        </div>

        <div class="result_line">
            <span>Total Repayment, $</span>
            <strong id="res_total_repayment" class="result_value">-</strong>
        </div>
    </div>
    <span class="calc-logo">Powered by<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/calc-logo.png" alt="logo"></span>
</div>
<script type="text/javascript" >
    jQuery(document).ready(function($) {
        jQuery('#calc_form').on( "submit", function( event ) {
            event.preventDefault();
            calc_data = calculate_short_term();
            update_short_term_calculator(calc_data);
        });
    });
</script>