<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 04.01.18
 *
 */
?>
<article class="blog-item-general">
    <a href="<?php echo get_permalink(); ?>">
        <div class="fix-ie-flex negative-margin-row-xs">
            <div class="blog-item-general-img">
                <div class="general-img-path bg-lazy" data-style="background-image: url(<?php echo imgpath(get_the_post_thumbnail_url(get_the_ID(), 'post-thumb-general')) ?>);"></div>
                <div class="pre-title text-uppercase"><?php /*echo $post_category_name;*/ ?></div>
                <h2 class="blog-item-general-title"><?php the_title(); ?></h2>
            </div>
        </div>

        <div class="blog-item-general-content">
            <!--            <div class="sub-title">--><?php //the_title(); ?><!--</div>-->
            <p><?php echo strip_shortcodes(wp_trim_words(get_the_content())); ?></p>
        </div>
    </a>
    <div class="blog-item-sub-author"><span class="medium"><?php echo do_shortcode('[est_time]'); ?> read</span><span><a href="<?php echo get_permalink(get_post_meta( get_the_ID() , 'author' , true )); ?>"><?php echo get_the_title(get_post_meta( get_the_ID() , 'author' , true )); ?></a></span> <i class="fa fa-circle" aria-hidden="true"></i> <span><?php echo get_the_date() ?></span></div>
</article>

