<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 16.11.16
 * Time: 18:13
 */
?>
<div class="calculator calc-flow left">
    <div class="title_block">
        <h4>INVOICE FACTORING</h4>
    </div>
    <div class="calc_container">
        <form id="calc_form" method="post">
            <label for="invoice_amount">Invoice Amount, $</label>
            <input type="text" id="invoice_amount">

            <label for="advance_percentage">Advance Percentage, %</label>
            <input type="text" id="advance_percentage">

            <label for="factoring_fee">Factoring Fee, %</label>
            <input type="text" id="factoring_fee">

            <label for="t_period">Invoice Due In</label>
            <span class="calc_term">
                    <select class="field-styler-option field-input-biger" name="select-0" id="t_period">
                        <option value="1" selected>Weeks</option>
                        <option value="4">Month</option>
                        <option value="52">Years</option>
                    </select>

                    <input type="text" id="cnt_w">
                </span>
            <input type="submit" value="Calculate" id="calc">
            <a class="cta_b culc_btn" href="<?php echo cta_button_link(); ?>" style="display: none;">See Loan Options</a>

        </form>
    </div>
    <div class="calc_result" style="display: none;">
        <h5>Calculation Result</h5>
        <div class="result_line">
            <span>Advanced Amount, $</span>
            <strong id="advanced_amount"></strong>
        </div>

        <div class="result_line">
            <span>Reserve Anount Less Fee, $</span>
            <strong id="ral_fee"></strong>
        </div>

        <div class="result_line">
            <span>Cost of Financing, $</span>
            <strong id="total_cost"></strong>
        </div>

        <div class="result_line">
            <span>Effective APR</span>
            <strong id="effective_apr"></strong>
        </div>

    </div>
    <span class="calc-logo">Powered by<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/calc-logo.png" alt="logo"></span>
</div>
<script type="text/javascript" >
    jQuery(document).ready(function($) {
        jQuery('#calc_form').on( "submit", function( event ) {
            event.preventDefault();
            calc_data = calculate_invoice_factoring();
            update_invoice_factoring_calculator(calc_data);
        });
    });
</script>