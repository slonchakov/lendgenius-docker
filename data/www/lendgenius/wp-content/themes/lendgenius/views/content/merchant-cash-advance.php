<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 16.11.16
 * Time: 21:41
 */
?>
<div class="calculator calc-flow left">
    <div class="title_block">
        <h4>MERCHANT CASH ADVANCE</h4>
    </div>
    <div class="calc_container">
        <form id="calc_form" method="post">
            <label for="total_advance">Total Advance, $</label>
            <input type="text" id="total_advance">

            <label for="factoring_rate">Factoring Rate</label>
            <input type="text" id="factoring_rate">

            <label for="monthly_ccs">Monthly Credit Card Sales, $</label>
            <input type="text" id="monthly_ccs">

            <label for="percent_ccs">% of Credit Card Sales</label>
            <input type="text" id="percent_ccs">

            <label for="other_fees">Other Fees, $(optional)</label>
            <input type="text" id="other_fees">

            <input type="submit" value="Calculate" id="calc">
            <a class="cta_b culc_btn" href="<?php echo cta_button_link(); ?>" style="display: none;">See Loan Options</a>

        </form>
    </div>
    <div class="calc_result" style="display: none;">
        <h5>Calculation Result</h5>
        <div class="result_line">
            <span>Total Repayment Period</span>
            <strong id="total_rp"></strong>
        </div>

        <div class="result_line">
            <span>Total Cost of Financing, $</span>
            <strong id="total_cost"></strong>
        </div>

        <div class="result_line">
            <span>Effective APR</span>
            <strong id="effective_apr"></strong>
        </div>

    </div>
    <span class="calc-logo">Powered by<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/calc-logo.png" alt="logo"></span>
</div>
<script type="text/javascript" >
    jQuery(document).ready(function($) {
        jQuery('#calc_form').on( "submit", function( event ) {
            event.preventDefault();
            calc_data = calculate_merchant_cash_advance();
            update_merchant_cash_advance(calc_data);
        });
    });
</script>
