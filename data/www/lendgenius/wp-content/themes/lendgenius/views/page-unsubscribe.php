<?php
/*
 * Template Name: Unsubscribe
 *
 */

get_header('unsubscribe');

echo get_the_content();

get_footer('unsubscribe');