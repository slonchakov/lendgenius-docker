<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 22.11.17
 *
 */

ob_start();
?>
[
    {
    “category”: string,
    “description:” string,
    “items”:[
            {
            “name”: string,
            “logo”: string,
            “apr”: Array<number>,
            “creditScore”: number,
            “redirectLink”: string,
            “qualifications”: Array<string>,
            “pros”: Array<string>,
            “cons”: Array<string>
            },
            {
            “name”: string,
            “logo”: string,
            “apr”: Array<number>,
            “creditScore”: number,
            “redirectLink”: string,
            “qualifications”: Array<string>,
            “pros”: Array<string>,
            “cons”: Array<string>
            }
        ]
    },

]
<?php
ob_end_flush();