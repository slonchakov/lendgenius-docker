<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 02.11.17
 *
 */

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Lendgenius - Redirecting to <?= $wp->redirectName ;?></title>
    <!-- Bootstrap -->
    <link href="/wp-content/themes/lendgenius/assets/styles/redirect/redirect.min.css" rel="stylesheet" />
    <?php
    wp_head();
    ?>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                })(window,document,'script','dataLayer','GTM-P9HP45');</script>
    <!-- End Google Tag Manager -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-84489985-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-84489985-3');
    </script>

</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P9HP45"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div class="wrapper">
    <div class="align-block">
        <div class="logo">
            <img src="/wp-content/themes/lendgenius/images/logo2.png" class="center-block" />
        </div>
        <div class="wrapper-content">
            <div class="h2">
                One moment, please
            </div>
            <div class="line-success_bottom">
                <p>Redirecting you to <?= $wp->redirectName ;?> secure website to complete your loan applicaton.</p>
            </div>
            <div class="logo-for-redirect">
                <img src="<?= $wp->redirectLogo ;?>" class="img-responsive center-block" />
            </div>
        </div>
    </div>
</div>
</body>
</html>
<?php
    get_footer();
?>
<script>
    jQuery(document).ready(function(){
        var options = {
            timeout: 3000,
            url: '<?= $wp->redirectUrl; ?>'
        };

        $('title').text('Lendgenius - Redirecting to <?= $wp->redirectName ;?>');

        setTimeout(function(){
            location.href = options.url
        }, options.timeout);
    });
</script>