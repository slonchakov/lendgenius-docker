<?php
/*
    Template Name: Loan type choice
*/

get_header();
?>
<style>

    .page-loan-choice {
        min-height: 600px;
        text-align: center;
        font-family: "Work Sans";
    }

    .page-loan-choice .box-content{
        margin: auto;
        max-width: 600px;
        padding-left: 15px;
        padding-right: 15px;
    }

    .page-loan-choice h1 {
        font-size: 38px;
        font-weight: 300;
        margin-bottom: 60px;

    }

    .page-loan-choice .link-box i{
        display: block;
        margin: auto;
        text-align: center;
        font-size: 60px;
        margin-bottom: 15px;

    }
    .page-loan-choice .link-box a {
        font-size: 28px;
        font-weight: 300;
    }
</style>

<div class="page-loan-choice">
    <div class="box-content">
        <h1>
            What are you looking for?
        </h1>
        <div class="col-xs-6">
            <div class="link-box">

                <a href="/business-loans" class="loan-link">
                    <i class="icomoon-shop ico-gradient"></i>
                    Business loans</a>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="link-box">

                <a href="/personal-loans" class="loan-link">
                    <i class="icomoon-user-star ico-gradient"></i>
                    Personal loans</a>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();
?>
