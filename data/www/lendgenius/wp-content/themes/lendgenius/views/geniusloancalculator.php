<?php
/*
    Template Name: Genius Loan Calculator
*/

addStyle('calculator', '/style-2018/css/calculator.css');
addScript('calculator', '/style-2018/js/calculator.js');

get_header();
?>

<div id="genius-calculator">
    <div class="container">
        <h1><?= the_title();?></h1>

        <div class="hint">
            <?= the_content(); ?>
        </div>

        <div id="calculator-body">
            <div class="header">
                Simple Loan Calculator
            </div>
            
            <div class="content">
                <div class="items">
                    <div class="item">
                        <div class="title">
                            <div class="step">1</div>
                            <span>Loan Amount</span>
                        </div>
                        <div class="input dollar-sign"><input type="text" maxlength="15" name="loan_amount" value="50000"></div>
                    </div>
                    <div class="item">
                        <div class="title">
                            <div class="step">2</div>
                            <span>Interest Rate</span>
                        </div>
                        <div class="input percent-sign"><input type="text" maxlength="6" name="interest_rate" value="6"></div>
                    </div>
                    <div class="item">
                        <div class="title">
                            <div class="step">3</div>
                            <span>Number of Months</span>
                        </div>
                        <div class="input"><input type="text" maxlength="2" name="number_of_months" value="60"></div>
                    </div>
                </div>
                <div class="bottom">
                    <button id="btn-calculate">Calculate</button>
                </div>
            </div>
        </div>

        <div id="calculator-result">
            <div class="header">
                Your Payments
            </div>

            <div class="content">
                <div class="sub-title">Base Calculations</div>

                <div class="table-payments clearfix">
                    <div class="table-cell first">
                        <div class="text">
                            <div class="title">Loan Amount</div>
                            <div class="value">$<span id="loan-amount">50,000.00</span></div>
                        </div>
                    </div>
                    <div class="table-cell">
                        <div class="text">
                            <div class="title">Interest Rate</div>
                            <div class="value"><span id="interest-rate">6</span>%</div>
                        </div>
                    </div>
                    <div class="table-cell">
                        <div class="text">
                            <div class="title">Number of Months</div>
                            <div class="value"><span id="number-of-months">60</span></div>
                        </div>
                    </div>
                    <div class="table-cell last">
                        <div class="text">
                            <div class="title">Monthly Payments</div>
                            <div class="value">$<span id="monthly-payments">967</span></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div id="calculator-embed-form">
            <?= do_shortcode('[embed-form]'); ?>
        </div>
    </div>
</div>


<?php

get_footer();
