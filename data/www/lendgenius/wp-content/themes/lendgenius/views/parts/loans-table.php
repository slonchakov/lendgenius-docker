<div class="container-fluid container-max-width-fixed">
    <div class="row">
        <div class="col-xs-12">
            <div class="table-funding table-responsive">
                <table>
                    <thead>
                        <tr>
                            <th>Funding Options</th>
                            <th>APR</th>
                            <th> Do you qualify?</th>
                            <th>Time in Business</th>
                            <th>Annual Revenue</th>
                            <th></th>
                        </tr>
                    </thead>    
                    <tbody>
                    <?php foreach ($wp->fundingOptions as $funding) : ?>
                    <?php if(!empty($funding['link'])): ?>
                        <tr>
                            <td>
                                <div class="visible-xs cell-headline">
                                    Funding Option
                                </div>
                                <div class="additional hidden">
                                    <div class="qualifications">
                                        <ul>
                                        <?php while( have_rows('qualifications', $funding['ID']) ): the_row(); ?>
                                            <li>
                                                <?= get_sub_field('item'); ?>
                                            </li>
                                        <?php endwhile; ?>
                                        </ul>
                                    </div>

                                    <div class="pros">
                                        <ul>
                                            <?php while( have_rows('pros', $funding['ID']) ): the_row(); ?>
                                                <li>
                                                    <?= get_sub_field('item'); ?>
                                                </li>
                                            <?php endwhile; ?>
                                        </ul>
                                    </div>

                                    <div class="cons">
                                        <ul>
                                            <?php while( have_rows('cons', $funding['ID']) ): the_row(); ?>
                                                <li>
                                                    <?= get_sub_field('item'); ?>
                                                </li>
                                            <?php endwhile; ?>
                                        </ul>
                                    </div>

                                </div>
                                <?= (get_the_post_thumbnail($funding['ID']) != '') ? get_the_post_thumbnail($funding['ID']) : "<p>". get_the_title($funding['ID']) ."</p>"; ?>
                                <?php if (have_rows('pros', $funding['ID']) || have_rows('pros', $funding['ID']) || have_rows('qualifications', $funding['ID']) ) : ?>
                                <button class="btn btn-default btn-small btn-primary btn-lg" type="button" data-toggle="modal" data-target="#FundingModal">View details</button>
<!--                                <span>at foundation work modal</span>-->
                                <?php endif; ?>
                            </td>
                            <td>
                                <div class="visible-xs cell-headline">
                                    APR
                                </div>
                                <dl>
                                    <dd><?php
                                        $interest = get_post_meta( $funding['ID'], 'estimated_apr', true);
                                        $interestText = 'Estimated Apr';

                                        if ($interest == '') {
                                            $interest = get_post_meta( $funding['ID'], 'interest', true);
                                            $interestText = get_post_meta( $funding['ID'], 'interest_text', true);
                                        }

                                        echo $interest;

                                        ?></dd>
                                    <dt>
                                        <?= $interestText; ?>
                                    </dt>
                                </dl>
                            </td>
                            <td>
                                <div class="visible-xs cell-headline">
                                    Do you qualify?
                                </div>
                                <dl>
                                    <dd><?= get_post_meta( $funding['ID'], 'min_credit', true);?></dd>
                                    <dt>MIN CREDIT SCORE</dt>
                                </dl>    
                            </td>
                            <td>
                                <div class="visible-xs cell-headline">
                                    Time in Business
                                </div>
                                <dl>
                                    <dd><?= get_post_meta( $funding['ID'], 'time_in_business', true);?></dd>

                                </dl>
                            </td>
                            <td>
                                <div class="visible-xs cell-headline">
                                    Annual Revenue
                                </div>
                                <dl>
                                    <dd><?= get_post_meta( $funding['ID'], 'annual_revenue', true);?></dd>

                                </dl>
                            </td>
                            <td>
                                <a href="<?= $funding['link'];?>" class="btn btn-success" target="_blank"><?= $funding['button']; ?></a>
                                <!--                                <div class="btn-span">Be the first to review this offer.</div>-->
                            </td>
                        </tr>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>    
        </div>
    </div>
</div>

<?php
get_template_part( 'views/parts/loans-modal' );