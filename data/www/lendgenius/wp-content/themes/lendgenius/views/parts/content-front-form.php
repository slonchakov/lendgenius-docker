<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 2/27/17
 * Time: 4:20 PM
 */

$theId = get_the_ID();

$form_title = get_post_meta( $theId, 'form_title', true );
$sub_form_title = get_post_meta( $theId, 'sub_form_title', true );
?>
<div class="another-view-holder">
    <div class="another-mobile-view">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="general-slogan"><?php echo $form_title; ?> <b class="text-uppercase"><? echo $sub_form_title; ?></b></h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <!-- <div class="business-loan-form">
            <form-app>
                <div class="loading-form-ng">
                    <div class="spinner-ng">Loading...</div>
                </div>
            </form-app>
        </div> -->
        <div class="loan-form-sq">
            <?php do_shortcode('[lead form="4"]'); ?>
        </div> 
    </div>
</div>