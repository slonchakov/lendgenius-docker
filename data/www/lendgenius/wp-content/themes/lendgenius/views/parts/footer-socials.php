<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 03.10.17
 *
 */

    $twitter_url   = get_option( 'sts_twitter_url' );
    $facebook_url  = get_option( 'sts_facebook_url' );
    $gplus_url     = get_option( 'sts_gplus_url' );
    $instagram_url = get_option( 'sts_instagram_url' );
    $linkedin_url  = get_option( 'sts_linkedin_url' );
    $youtube_url   = get_option( 'sts_youtube_url' );

    if (!empty($twitter_url) || !empty($facebook_url) || !empty($gplus_url) || !empty($instagram_url) || !empty($linkedin_url)) {
        ?>
        <div class="social-list">
            <ul class="list-inline">
                <?php
                if (!empty($twitter_url)) {
                    ?>
                    <li>
                        <a target="_blank" href="<?php echo $twitter_url; ?>" rel=”nofollow”>
                            <i class="fa fa-twitter" aria-hidden="true"></i>
                        </a>
                    </li>
                    <?php
                }
                if (!empty($facebook_url)) {?>
                    <li>
                        <a target="_blank" href="<?php echo $facebook_url; ?>" rel=”nofollow”>
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                        </a>
                    </li>
                    <?php
                }
                if (!empty($gplus_url)) {?>
                    <li>
                        <a target="_blank" href="<?php echo $gplus_url; ?>" rel=”nofollow”>
                            <i class="fa fa-google-plus" aria-hidden="true"></i>
                        </a>
                    </li>
                    <?php
                }
                if (!empty($instagram_url)) {?>
                    <li>
                        <a target="_blank" href="<?php echo $instagram_url; ?>" rel=”nofollow”>
                            <i class="fa fa-instagram" aria-hidden="true"></i>
                        </a>
                    </li>
                    <?php
                }
                if (!empty($linkedin_url)) {?>
                    <li>
                        <a target="_blank" href="<?php echo $linkedin_url; ?>" rel=”nofollow”>
                            <i class="fa fa-linkedin" aria-hidden="true"></i>
                        </a>
                    </li>
                    <?php
                }
                if (!empty($youtube_url)) { ?>
                    <li>
                        <a target="_blank" href="<?php echo $youtube_url; ?>" rel=”nofollow”>
                            <i class="fa fa-youtube" aria-hidden="true"></i>
                        </a>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </div>
    <?php } ?>