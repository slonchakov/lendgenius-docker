<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 18.09.17
 *
 */
$the_id = get_the_ID();
$sub_title = get_post_meta( $the_id, 'sub_title', true );
?>
<article id="id-card-<?= $the_id; ?>" class="card-article" data-anchor="id-card-<?= $the_id; ?>">
    <div class="row">
        <div class="col-lg-12">
            <div class="b-description-item">
                <div class="row">
                    <div id="id-card-<?= $the_id; ?>" class="col-sm-4 col-md-3">
                        <?php
                        // Post thumbnail.
                        the_post_thumbnail('author-thumb-posts', array('class'=> 'img-responsive b-card_img'));
                        ?>

                        <div class="b-card_pre-title medium">
                            <?php the_title() ;?>
                        </div>

                        <h2 class="b-card_title">
                            <?= $sub_title ; ?>
                        </h2>

                    </div>
                    <div class="col-sm-8 col-md-9 content_rd">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</article>
