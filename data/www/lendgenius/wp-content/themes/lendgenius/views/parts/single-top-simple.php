<style>
    .pattern-title-third{
        background-image: url('<?php echo imgpath(get_the_post_thumbnail_url(get_the_ID(), 'hero-image-400-342')) ?>');
    }
    @media(min-width: 401px){
        .pattern-title-third{
            background-image: url('<?php echo imgpath(get_the_post_thumbnail_url(get_the_ID(), 'hero-image-770-350')); ?>');
        }
    }
    @media(min-width: 769px){
        .pattern-title-third{
            background-image: url('<?php echo imgpath(get_the_post_thumbnail_url(get_the_ID(), 'hero-image-1280-350')); ?>');
        }
    }
    @media(min-width: 1201px ){
        .pattern-title-third{
            background-image: url('<?php echo imgpath(get_the_post_thumbnail_url(get_the_ID(), 'hero-image-2560-667')); ?>');
        }
    }
</style>
<div class="pattern-title-third">
    <div class="matchheight-row"></div>
    <div class="matchheight-row"></div>
    <div class='container matchheight-row'>
        <div class='row'>
            <div class='col-lg-12'>
                <h1 class="item-title">
                    <?php
                        the_title();
                    ?>
                </h1>
            </div>
        </div>
    </div>
</div>