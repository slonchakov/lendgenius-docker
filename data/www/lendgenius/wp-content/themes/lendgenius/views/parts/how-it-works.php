<?php

// How it works content
$how_it_works = 104;
$sub_title2   = get_post_meta( $how_it_works, 'sub_title2', true );
$sub_title3   = get_post_meta( $how_it_works, 'sub_title3', true );
$sub_title4 = get_post_meta( $how_it_works, 'sub_title4', true );
// How it works content

?>
<style>
    a:hover {
        text-decoration: none;
    }
</style>
<!-- How it works section -->
<div class="work-sans content-rd">
    <?php
    $args = array(
        'page_id' => $how_it_works
    );
    $query = new WP_Query( $args );
    ?>
    <section class="various-sided-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="light-font">
                        <?php echo $sub_title2; ?>
                    </h2>
                </div>
            </div>
        </div>
        <div class="various-sided">
<!--            --><?php //if( have_rows('how_we_make_money', $query->post->ID) ): ?>
<!--                --><?php //while( have_rows('how_we_make_money', $query->post->ID) ): the_row();
//
//                    // vars
//                    $icon = get_sub_field('icon');
//                    $title = get_sub_field('title');
//                    $description = get_sub_field('description');
//                    ?>
<!--                    <div class="various-sided-item">-->
<!--                        <div class="container">-->
<!--                            <div class="row">-->
<!--                                <div class="col-md-8 col-md-offset-2 flex">-->
<!--                                    <div class="sided-item-logo">-->
<!--                                        <i class="--><?php //echo $icon; ?><!--"></i>-->
<!--                                    </div>-->
<!--                                    <div class="sided-item-content">-->
<!--                                        <div class="sided-item-title">--><?php //echo $title; ?><!--</div>-->
<!--                                        --><?php //echo $description; ?>
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                --><?php //endwhile; ?>
<!--            --><?php //endif; ?>

            <div class="various-sided">
                <div class="various-sided-item">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2 flex">
                                <div class="sided-item-logo">
                                    <i class="icomoon-doc-time ico-gradient"></i>
                                </div>
                                <div class="sided-item-content">
                                    <div class="sided-item-title">Fill out a short survey.</div>
                                    <p>Answer some questions about yourself to see what kind of financing you qualify for - You’ll have options in a matter of minutes.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="various-sided-item">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2 flex">
                                <div class="sided-item-logo">
                                    <i class="icomoon-compare-options ico-gradient"></i>
                                </div>
                                <div class="sided-item-content">
                                    <div class="sided-item-title">Browse your matches...</div>
                                    <p>If you want, we'll walk you through the products you matched with. Your Loan Genius is well-versed in the nuances of financing.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="various-sided-item">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2 flex">
                                <div class="sided-item-logo">
                                    <i class="icomoon-multiple-offers ico-gradient"></i>
                                </div>
                                <div class="sided-item-content">
                                    <div class="sided-item-title">Apply once to browse multiple offers.</div>
                                    <p>You’ll discover financing options and apply with as many lenders as you want. We don’t waste your time requesting the same information over and over again.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="various-sided-item">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2 flex">
                                <div class="sided-item-logo">
                                    <i class="icomoon-preferences ico-gradient"></i>
                                </div>
                                <div class="sided-item-content">
                                    <div class="sided-item-title">Compare your offers...</div>
                                    <p>Now the fun part – Shop around for the right product and best rate. When lenders compete, your win big.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="various-sided-item">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2 flex">
                                <div class="sided-item-logo">
                                    <i class="icomoon-dollar-circle ico-gradient"></i>
                                </div>
                                <div class="sided-item-content">
                                    <div class="sided-item-title">Get your funds!</div>
                                    <p>You’re free to snatch up that office space you’ve been eyeing, upgrade your technology, or restock more of that hot inventory. With every loan payment, you’ll set yourself up for even better options with LendGenius in the future.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

    <section class="steps-icon">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>How We Earn Money</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 steps-item">
                    <div class="steps-item-icon">
                        <i class="icomoon-free-circle ico-gradient"></i>
                    </div>
                    <div class="steps-item-title"></div>
                    <p>No catch – Our service is totally free for you, whether you fund or not.</p>
                </div>
                <div class="col-sm-4 steps-item">
                    <div class="steps-item-icon">
                        <i class="icomoon-user-star ico-gradient"></i>
                    </div>
                    <div class="steps-item-title"></div>
                    <p>Choose the lender that meets your needs; we’ll never pressure you to take an offer.</p>
                </div>
                <div class="col-sm-4 steps-item">
                    <div class="steps-item-icon">
                        <i class="icomoon-dollar-circle ico-gradient"></i>
                    </div>
                    <div class="steps-item-title"></div>
                    <p>The lender may pay 2–5% of the total sum for the privilege of having you as a borrower.</p>
                </div>
            </div>
        </div>
    </section>

<!--    <section class="steps-icon">-->
<!--        <div class="container">-->
<!--            <div class="row">-->
<!--                <div class="col-lg-12">-->
<!--                    <h2>--><?php //echo $sub_title3; ?><!--</h2>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="row">-->
<!--                --><?php //if( have_rows('how_we_earn_money', $query->post->ID) ): ?>
<!--                    --><?php //while( have_rows('how_we_earn_money', $query->post->ID) ): the_row();
//
//                        $icon = get_sub_field('icon');
//                        $title = get_sub_field('htitle');
//                        $description = get_sub_field('description');
//                        ?>
<!--                        <div class="col-sm-4 steps-item">-->
<!--                            <div class="steps-item-icon">-->
<!--                                <i class="--><?php //echo $icon; ?><!--"></i>-->
<!--                            </div>-->
<!--                            <div class="steps-item-title">--><?php //echo $title; ?><!--</div>-->
<!--                            --><?php //echo $description; ?>
<!--                        </div>-->
<!--                    --><?php //endwhile; ?>
<!--                --><?php //endif; ?>
<!--            </div>-->
<!--        </div>-->
<!--    </section>-->
<!--    <section class="finance_products">-->
<!--        <div class="container">-->
<!--            <div class="row">-->
<!--                <h2>--><?php //echo $sub_title4; ?><!--</h2>-->
<!--                <div class="grid">-->
<!--                    <div class="product_line">-->
<!--                        --><?php
//                        // The Query
//                        $args = array(
//                            'post_type' => 'loans',
//                            'orderby' => 'ID',
//                            'order' => 'ASC',
//                        );
//                        $slider_count = 1;
//                        $query = new WP_Query( $args );
//                        if ( $query->have_posts() ) {
//                            // The Loop
//                            while ( $query->have_posts() ) : $query->the_post();
//                                $loan_icon = get_post_meta( get_the_ID(), 'loan_icon', true );
//                                ?>
<!--                                <div class="finance_product">-->
<!--                                    <a href="--><?php //echo get_permalink(); ?><!--">-->
<!--                                        <div class="image">-->
<!--                                            <i class="home-sprite --><?php //echo $loan_icon; ?><!--"></i>-->
<!--                                        </div>-->
<!--                                        <h5>--><?php //the_title(); ?><!--</h5>-->
<!--                                    </a>-->
<!--                                </div>-->
<!--                                --><?php
//                            endwhile;
//                            wp_reset_postdata();
//                        }
//                        ?>
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </section>-->

</div>
<!-- How it works section end -->