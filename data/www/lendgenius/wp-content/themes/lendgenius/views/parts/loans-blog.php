<div class="table-funding blog-table">
    <table>
        <thead>
        <tr>
            <th>Funding Options</th>
            <th>APR</th>
            <th>Do you qualify?</th>
            <th>Time in Business</th>
            <th>Annual Revenue</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($wp->fundingOptions as $funding) :  ?>
            <?php if(!empty($funding['link'])): ?>
            <tr>
                <td>
                    <div class="visible-xs cell-headline">
                        Funding Option
                    </div>
                    <?= get_the_post_thumbnail($funding['ID']); ?>
                    <a href="<?= $funding['link'];?>" class="btn btn-success"  target="_blank"><?= $funding['button']; ?></a>
<!--                    <div class="btn-span">Be the first to review this offer.</div>-->
                </td>
                <td>
                    <div class="visible-xs cell-headline">
                        APR
                    </div>
                    <dl>
                        <dd><?php
                            $interest = get_post_meta( $funding['ID'], 'estimated_apr', true);
                            $interestText = 'Estimated Apr';

                            if ($interest == '') {
                                $interest = get_post_meta( $funding['ID'], 'interest', true);
                                $interestText = get_post_meta( $funding['ID'], 'interest_text', true);
                            }

                            echo $interest;

                            ?></dd>
                        <dt>
                            <?= $interestText; ?>
                        </dt>
                    </dl>
                </td>
                <td>
                    <div class="visible-xs cell-headline">
                        Do you qualify?
                    </div>
                    <dl>
                        <dd><?= get_post_meta( $funding['ID'], 'min_credit', true);?></dd>
                        <dt>MIN CREDIT SCORE</dt>
                    </dl>
                </td>
                <td>
                    <div class="visible-xs cell-headline">
                        Time in Business
                    </div>
                    <dl>
                        <dd><?= get_post_meta( $funding['ID'], 'time_in_business', true);?></dd>

                    </dl>
                </td>
                <td>
                    <div class="visible-xs cell-headline">
                        Annual Revenue
                    </div>
                    <dl>
                        <dd><?= get_post_meta( $funding['ID'], 'annual_revenue', true);?></dd>

                    </dl>
                </td>
            </tr>
            <?php endif; ?>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>