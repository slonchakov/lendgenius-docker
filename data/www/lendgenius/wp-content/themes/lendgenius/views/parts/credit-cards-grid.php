<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 18.09.17
 *
 */
$the_id    = get_the_ID();
$sub_title = get_post_meta( $the_id, 'sub_title', true );
$apply_url = get_post_meta( $the_id, 'apply_url', true );
?>
<div class="b-card">
    <a href="#id-card-<?= $the_id; ?>" data-scroll="id-card-<?= $the_id; ?>">
        <?php
        // Post thumbnail.
        the_post_thumbnail('credit-card-grid', array('class'=> 'img-responsive center-block b-card_img'));
        ?>
    </a>
    <div class="b-card_pre-title medium">
        <?php the_title() ;?>
    </div>
    <h3 class="b-card_title">
        <?= $sub_title ; ?>
    </h3>
    <div class="b-card_controls">
        <div class="b-card_controls-content">
            <a href="<?= $apply_url; ?>" class="btn btn-success btn-block">Learn More</a>
            <div class="btn-subscribe">on Lendgenius secure website</div>
        </div>
    </div>
</div>