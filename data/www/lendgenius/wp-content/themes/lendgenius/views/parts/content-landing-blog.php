<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 4/19/17
 * Time: 10:09 AM
 */

    $args_lending_blog = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'posts_per_page' => 3,
        'meta_key' => 'publish_landing_page',
        'meta_value' => '1',
        'orderby' => 'ID',
        'order' => 'DESC'
    );
    $query_lending_blog = new WP_Query( $args_lending_blog );
    if ( $query_lending_blog->have_posts() ) {
?>

<div class="row">
    <div class="col-xs-12">
        <h2>
            Blog Updates
        </h2>
    </div>
</div>
<div class="row">
        <?php
        // The Loop
        while ( $query_lending_blog->have_posts() ) : $query_lending_blog->the_post();
        ?>
    <div class="col-sm-4">
        <div class="blog-items-prev">
            <?php
            // Post thumbnail.
            single_post_thumbnail('top-article-thumb');
            ?>
            <a href="<?php echo get_permalink(); ?>" class="title-blog"><?php the_title(); ?></a>
            <div class="info-blog">
                <?php echo get_the_title(get_post_meta( get_the_ID() , 'author' , true )); ?> <span>◦ <?php the_date() ?></span>
            </div>
        </div>
    </div>
    <?php
        endwhile;
        wp_reset_postdata();
    ?>
</div>
<?php } ?>
