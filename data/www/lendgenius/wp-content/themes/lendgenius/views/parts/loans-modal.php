<div class="modal fade here" id="FundingModal" tabindex="-1" role="dialog" aria-labelledby="FundingModalLabel">
    <div class="modal-dialog modal-funding" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="modal-funding-logo">
                </div>
                <h4 class="modal-title line-success-bottom" id="FundingModalLabel">Loan Details</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="h4">Qualifications</div>
                        <ul id="modal_qualifications" class="list-success-circle">
                        </ul>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="h4">Pros</div>
                        <ul id="modal_pros" class="list-success-circle">
                        </ul>
                    </div>
                    <div class="col-sm-6">
                        <div class="h4">Cons</div>
                        <ul id="modal_cons" class="list-danger-circle">
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>