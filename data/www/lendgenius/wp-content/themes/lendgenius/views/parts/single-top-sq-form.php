<style>
    .loans_subpage_banner{
        background-image: url('<?php echo get_the_post_thumbnail_url(get_the_ID(), 'hero-image-400-342') ?>');
    }
    @media(min-width: 401px){
        .loans_subpage_banner{
            background-image: url('<?php echo get_the_post_thumbnail_url(get_the_ID(), 'hero-image-770-350'); ?>');
        }
    }
    @media(min-width: 769px){
        .loans_subpage_banner{
            background-image: url('<?php echo get_the_post_thumbnail_url(get_the_ID(), 'hero-image-1280-350'); ?>');
        }
    }
    @media(min-width: 1201px ){
        .loans_subpage_banner{
            background-image: url('<?php echo get_the_post_thumbnail_url(get_the_ID(), 'hero-image-2560-667'); ?>');
        }
    }
</style>
<section class="loans_subpage_banner">
    <div class="darken-lining">
        <div class="container"> <!--.container-fluid (if full-screen)-->
            <div class="row flex flex-align-stretch flex-column-xs">
                <div class="col-sm-5 col-md-7 flex flex-align-center">
                    <div class="">
                        <h1><?php the_title(); ?></h1>
                    </div>
                </div>
                <div class="col-sm-7 col-md-5 banner-form-wrapper">
                    <?= do_shortcode('[lead form="7"]'); ?>
                </div>
            </div>
        </div>
    </div>
</section>