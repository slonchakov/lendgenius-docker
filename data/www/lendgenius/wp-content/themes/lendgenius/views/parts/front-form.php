<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 2/27/17
 * Time: 4:20 PM
 */

$theId = get_the_ID();

$form_title         = get_post_meta( $theId, 'form_title', true );
$sub_form_title     = get_post_meta( $theId, 'sub_form_title', true );
$input_orange_text  = get_post_meta( $theId, 'orange_button_text', true );
$input_orange_url   = get_post_meta( $theId, 'orange_button_url', true );
$orange_button_text = !empty($input_orange_text) ?  $input_orange_text : 'See Loan Options';
$orange_button_url  = !empty($input_orange_url) ? $input_orange_url : cta_button_link();

?>
<div class="another-view-holder">
    <div class="another-mobile-view">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="general-slogan"><?= $form_title; ?> <b class="text-uppercase"><?= $sub_form_title; ?></b></h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <!-- Form 2 -->
        <!-- <div class="business-loan-form">
            <form-app>
                <div class="loading-form-ng">
                    <div class="spinner-ng">Loading...</div>
                </div>
            </form-app>
        </div> -->
            <!--  Form 4 -->
<!--        <div class="loan-form-sq">-->
<!--            <form-app>-->
<!--                <div class="loading-form-ng">-->
<!--                    <div class="spinner-ng">Loading...</div>-->
<!--                </div>-->
<!--            </form-app>-->
<!--        </div>-->
        <!-- <?= do_shortcode('[lead form="4"]'); ?> -->

        <div class="row">
            <div class="col-xs-12 margin-top-xs-30">
                <a class="btn btn-x2 btn-warning-custom" href="<?= $orange_button_url; ?>"><?= $orange_button_text; ?></a>
            </div>
        </div>
    </div>
</div>
