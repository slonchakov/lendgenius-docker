<div class="comments-box">
    <div class="medium fs-24 margin-bottom-25"><?php
        $comments_count = get_comments_number();
        if ($comments_count > 0)
            echo  $comments_count ,' ', ($comments_count > 1) ? 'Comments' : 'Comment';
        ?></div>

    <div class="comment-control-holder">
        <button type="button" class='btn btn-default_white btn-block comment-form-toggle'>Add comment...</button>
        <div class="comment-form-holder">
            <form id="comment_form" action="/wp-comments-post.php" method="post">
                <div class="comment-form-fields">
                    <textarea name="comment" placeholder="Add comment..."></textarea>

                    <?php if (!is_user_logged_in()) : ?>
                    <div class="field-row">
                        <div>
                            <input type="text" placeholder="Name" name="author" />
                        </div>
                        <div>
                            <input type="email" placeholder="Email" name="email" />
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
                <div class="comment-form_controls flex">
                    <div class="form_captcha">
                        <?php do_action( 'comment_form_after_fields' ); ?>
                    </div>
                    <div>
                        <input type="hidden" name="comment_post_ID" value="<?= get_the_ID(); ?>" id="comment_post_ID">
                        <input type="hidden" name="comment_parent" id="comment_parent" value="0">
                        <button type="submit" class="btn btn btn-success btn-post">Post</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="comments-box-holder">
        <?php
        $arguments = [
            'post_id' => get_the_ID(),
            'order'   => 'DESC',
            'status'  => 'approve',
        ];

        $comments = get_comments($arguments);

        foreach ($comments as $comment) {
            ?>
            <div class="comment-item">
                <div class="flex flex-justify-sb flex-align-center flex-wrap">
                    <div class="author-comment medium">
                        <?= $comment->comment_author; ?>
                    </div>
                    <div class="date-comment nowrap"><?= print_date_ago($comment->comment_date_gmt); ?></div>
                </div>

                <div class="comment-content">
                    <?= $comment->comment_content; ?>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>