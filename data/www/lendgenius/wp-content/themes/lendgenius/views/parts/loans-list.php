<div class="container-fluid container-max-width-fixed">
    <div class="row">
        <div class="col-xs-12">
           <div class="list-option-funding">

               <?php foreach ($wp->fundingOptions as $funding) : ?>
                   <?php if(!empty($funding['link'])): ?>
                    <section class="row-option-funding">
                        <div class="option-funding-logo">
                            <?= get_the_post_thumbnail($funding['ID']); ?>
                        </div>
                        <div class="option-funding-desk">
                            <h4>
                                <?= get_the_title($funding['ID']); ?>
                            </h4>
                            <div class="option-funding-content">
                                <div class="option-funding-content-content">
                                    <?= get_the_content($funding['ID']); ?>
                                </div>
                                <div class="option-funding-control">
                                    <a href="<?= $funding['link'];?>" class="btn btn-block btn-success">Apply Now</a>
    <!--                                <span>Be the first to review this offer.</span>-->
                                </div>
                            </div>
                        </div>
                    </section>
               <?php endif; ?>
               <?php endforeach; ?>
           </div>
        </div>
    </div>
</div>