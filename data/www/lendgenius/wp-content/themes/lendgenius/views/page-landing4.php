<?php
/*
 * Template Name: Lending4
 */
$template_directory_uri = get_template_directory_uri();

$post_id             = get_the_ID();
$rates_starting      = get_post_meta( $post_id, 'rates_starting', true );
$loans_from          = get_post_meta( $post_id, 'loans_from', true );
$loans_from_interval = get_post_meta( $post_id, 'loans_from_interval', true );
$funding             = get_post_meta( $post_id, 'funding', true );
$heading_title       = get_post_meta( $post_id, 'heading_title', true );
$sub_title           = get_post_meta( $post_id, 'sub_title', true );

$input_orange_text  = get_post_meta( $post_id, 'orange_button_text', true );
$input_orange_url   = get_post_meta( $post_id, 'orange_button_url', true );
$orange_button_text = !empty($input_orange_text) ?  $input_orange_text : 'See Loan Options';
$orange_button_url  = !empty($input_orange_url) ? $input_orange_url : cta_button_link();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <script src='//cdn.freshmarketer.com/182106/488542.js'></script>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>LendGenius - Personal and short term financing. Connect with a lender in as little as 5 minutes | LendGenius.com</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/assets/styles/icomoon.min.css?ver=<?php echo get_option( 'sts_script_version' ); ?>">
    <link href="<?php bloginfo('template_directory'); ?>/getstarted/css/style.css" rel="stylesheet">


    <style type="text/css">

        .header {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            width: 100%;
            padding: 25px 0;
            z-index: 12;
            background-color: #fff;
        }

        .form-page.form-loader jsf-form{
            padding-bottom: 30px !important;
        }

        @media (min-width: 992px) {
            .header__menu {
                display: flex;
            }

            .header .logo {
                background: url(<?php bloginfo('template_directory'); ?>/assets/images/logo-lendgenius.png) no-repeat center;
                background-size: contain;
            }

            .header {
                position: absolute;
                background-color: transparent;
                box-shadow: none;
            }

            .l-page-nav {
                display: none;
            }

            .footer .l-page-nav {
                padding: 40px 30px 0;
                display: block;
            }

        }
        }

    </style>

    <script>
        dataLayer = [
            { 'c': 'l', 'v': 'ust', 'st': 'p' }
        ];
    </script>

    <?= stripcslashes(get_option('google_tag_manager')); ?>

    <script type='application/ld+json'>
        {
            "@context": "http://schema.org/",
            "@type": "Organization",
            "name": "LendGenius",
            "description": "LendGenius is an online personal loan matching platform connecting borrowers with multiple lenders.",
            "email": "support@lendgenius.com",
            "logo": "https://lendgeniuslandingstorage.azureedge.net/wp-uploads/2017/04/lendgenius-thumbnail.png",
            "telephone": "+18003480997",
        "url": "https://www.lendgenius.com/",
            "address": {
                "@type": "PostalAddress",
                "addressCountry": "United States",
                "addressLocality": "Woodland Hills",
                "addressRegion": "California",
                "postalCode": "91367",
                "streetAddress": "21600 Oxnard St. #400",
                "email": "support@lendgenius.com",
                "telephone": "+18003480997",
                "name": "LendGenius"
            },
        "sameAs" :  [ "https://www.facebook.com/lendgenius",
        "https://twitter.com/lendgenius",
        "https://plus.google.com/114339560599887475172",
        "https://www.linkedin.com/company/lendgenius/",
        "https://www.instagram.com/lendgenius/",
        "https://www.youtube.com/channel/UCCv7gNH7RQLQB0MoaHSIlCw" ]}
        </script>
</head>

<body>
<?= stripcslashes(get_option('google_tag_manager_noscript')); ?>

<header class="header">
    <div class="l-container-fluid l-content">
        <div class="logo-container">
            <span class="logo"></span>
        </div>
    </div>
</header>
<div class="l-wrapper">



    <section class="home-main" style="background: url('<?= get_the_post_thumbnail_url(null, 'hero-image-2560-667'); ?>') top center no-repeat; background-size: cover ;">
        <div class="home-main__body">
            <h1 class="title">
                <?= $sub_title; ?>
            </h1>
            <div class="form-page form-loader">
                <?php get_template_part('views/parts/jsf-form')?>
            </div>
        </div>
        <div class="home-main__footer">
            <ul class="home-main__info">
                <li class="item">
                    <?= $rates_starting; ?>
                </li>
                <li class="item">
                    <?= $loans_from; ?>
                </li>
                <li class="item">
                    <?= $loans_from_interval; ?>
                </li>
            </ul>
        </div>

    </section>

    <section id="howitworks" class="how-works">
        <div class="l-container-fluid">
            <div class="how-works__head">
                <h2 class="how-works__title">How <span>LendGenius&trade;</span> works</h2>
                <h4 class="how-works__subtitle">One or more loan offers in as fast as five minutes</h4>
            </div>
            <div id="slider" class="how-works__body">
                <div class="how-works__image-wr">
                    <img class="how-works__image" src="<?php bloginfo('template_directory'); ?>/getstarted/images/form1.svg" alt="" />
                    <img hidden class="how-works__image" src="<?php bloginfo('template_directory'); ?>/getstarted/images/form2.svg" alt="" />
                    <img hidden class="how-works__image" src="<?php bloginfo('template_directory'); ?>/getstarted/images/form3.svg" alt="" />
                </div>
                <ol class="how-works__list" is-active="false">
                    <li data-item-id="0" class="how-works__item">
                        <h3 class="how-works__item-head">
                            Fill out our form
                        </h3>
                        <div class="how-works__item-body">
                            Save time by filling out one simple form and our network of lenders will review your information.
                        </div>
                    </li>
                    <li data-item-id="1" class="how-works__item">
                        <h3 class="how-works__item-head">
                            One or more loan offers
                        </h3>
                        <div class="how-works__item-body">
                            We are committed to helping you get connected with a lender. You could receive up to five loan offers.
                        </div>
                    </li>
                    <li data-item-id="2" class="how-works__item">
                        <h3 class="how-works__item-head">
                            Connect with a lender
                        </h3>
                        <div class="how-works__item-body">
                            We connect you with a lender who can deposit the money directly to your bank account, sometimes in as fast as 24 hours.
                        </div>
                    </li>
                </ol>
            </div>
            <div id="mobile-slider" class="swipe mobile-slider how-works__body">
                <ul class="how-works__list swipe-wrap">
                    <li class="how-works__item">
                        <img class="how-works__image" src="<?php bloginfo('template_directory'); ?>/getstarted/images/m-form1.svg" alt="" />
                        <h3 class="how-works__item-head">
                            Fill out our form
                        </h3>
                        <div class="how-works__item-body">
                            Save time by filling out one simple form and our network of lenders will review your information.
                        </div>
                    </li>
                    <li class="how-works__item">
                        <img class="how-works__image" src="<?php bloginfo('template_directory'); ?>/getstarted/images/m-form2.svg" alt="" />
                        <h3 class="how-works__item-head">
                            One or more loan offers
                        </h3>
                        <div class="how-works__item-body">
                            We are committed to helping you get connected with a lender. You could receive up to five loan offers.
                        </div>
                    </li>
                    <li class="how-works__item">
                        <img class="how-works__image" src="<?php bloginfo('template_directory'); ?>/getstarted/images/m-form3.svg" alt="" />
                        <h3 class="how-works__item-head">
                            Connect with a lender
                        </h3>
                        <div class="how-works__item-body">
                            We connect you with a lender who can deposit the money directly to your bank account, sometimes in as fast as 24 hours.
                        </div>
                    </li>
                </ul>
                <ul class="dots-show-state">
                    <li class="dots-show-state__item active">
                    </li>
                    <li class="dots-show-state__item">
                    </li>
                    <li class="dots-show-state__item">
                    </li>
                </ul>
            </div>
        </div>
    </section>
</div>
<!-- Footer-->

<footer class="footer">
    <div class="l-page-nav">
        <!--        <nav class="navigation">-->
        <!--            <a href="../#howitworks">How It Works</a>-->
        <!--            <a href="/Questions.html">Questions</a>-->
        <!--            <a href="/Unsubscribe.html">Unsubscribe</a>-->
        <!--            <a href="/PrivacyPolicy.html">Privacy Policy</a>-->
        <!--            <a href="/TermsOfUse.html">Terms Of Use</a>-->
        <!--            <a href="/Disclaimer.html">Disclaimer</a>-->
        <!--            <a href="/Contact.html">Contact</a>-->
        <!--            <a href="/EConsent.html">E-Consent</a>-->
        <!--        </nav>-->
    </div>
    <div class="l-container">
        <div class="disclosure">
            <?= strip_shortcodes($post->post_content); ?>

            <?php  get_template_part('views/parts/how-it-works'); ?>

            <div class="button-section lightgray-bg">
                <div class="container text-center">
                    <a href="<?= cta_button_link(); ?>" class=" btn-warning-custom btn-x2">See Loan Options</a>
                </div>
            </div>
        </div>

        <div class="copyright">

            <div class="footer-nav text-center">
                <a href="/privacy-policy/">Privacy Policy</a>
                <a href="/terms-of-use/">Terms of Use</a>
            </div>
            
            Copyright &copy; <?= date("Y"); ?> LendGenius.com. All Rights Reserved
        </div>
    </div>
</footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://www.sparning.com/hit/hit.core.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/getstarted/js/general.static.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/getstarted/js/common.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/getstarted/js/custom.js"></script>


<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/getstarted/js/swipe.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/getstarted/js/slider.js"></script>

<script>
    $('a[href*="#howitworks"]').click(function () {
        $('html, body').animate({
            scrollTop: $("#howitworks").offset().top
        }, 1000);

    });
</script>

</body>
</html>
