<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 6/29/17
 * Time: 10:22 PM
 */
?>

<div class="blog-item-sub wow fadeIn">
    <div class="blog-item-sub-img">
        <a href="<?php echo get_permalink(); ?>">
            <?php
            // Post thumbnail.
            single_post_thumbnail('author-thumb-posts', 'img-responsive center-block');
            ?>
        </a>
    </div>
    <h4 class="blog-item-sub-title matchheight">
        <a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a>
    </h4>
    <?php
    get_template_part( 'content-author-info' );
    ?>
</div>