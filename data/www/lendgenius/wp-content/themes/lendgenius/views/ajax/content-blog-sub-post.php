<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 7/14/17
 * Time: 3:58 PM
 */

?>

<div class="blog-item-sub wow fadeIn">
    <div class="blog-item-sub-img">
        <a href="<?php echo get_permalink(); ?>">
            <?php
            // Post thumbnail.
            single_post_thumbnail('author-thumb-posts', '');
            ?>
        </a>
    </div>
    <h4 class="blog-item-sub-title matchheight">
        <a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a>
    </h4>
    <div class="blog-item-sub-author"><span><a href="<?php echo get_permalink(get_post_meta( get_the_ID() , 'author' , true )); ?>"><?php echo get_the_title(get_post_meta( get_the_ID() , 'author' , true )); ?></a></span> <i class="fa fa-circle" aria-hidden="true"></i> <span><?php echo get_the_date() ?></span></div>
</div>
