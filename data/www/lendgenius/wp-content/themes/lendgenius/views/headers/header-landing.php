<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/assets/styles/icomoon.min.css?ver=<?php echo get_option( 'sts_script_version' ); ?>">
<link href="<?php echo get_template_directory_uri(); ?>/landing-pages/css/site.css?ver=<?php echo get_option( 'sts_script_version' ); ?>" rel="stylesheet">

<?php if(!is_page_template( 'landing-gmail-1.php' )) { ?>
    <style>
        <?php

        $media_backgrounds = [
            'ID' => null,
            'backgrounds' => [
                '.slogan-section' => [
                    ['hero-image-400-342'],
                    ['hero-image-770-350', 'min-width: 401px'],
                    ['hero-image-1280-563', 'min-width: 769px'],
                    ['hero-image-2560-667', 'min-width: 1201px']
                ],
                '.another-mobile-view' => [
                    ['hero-image-400-342','max-width: 400px'],
                    ['hero-image-770-350','max-width: 768px'],
                ],
                '.loans_subpage_banner' => [
                    ['hero-image-400-342'],
                    ['hero-image-770-350', 'min-width: 401px'],
                    ['hero-image-2560-667', 'min-width: 769px']
                ]
            ]
        ];

        if (preg_match('/publisher-\w{1,10}-v2/', get_post_meta( $post->ID, '_wp_page_template', true ))) {

            $media_backgrounds['backgrounds']['.bg-pattern'] = [

                    ['hero-image-2560-667'],
                    ['hero-image-2560-667', 'min-width: 768px']

            ];

        } elseif (is_page_template( 'page-see-loan-options.php' )) {


            $media_backgrounds['backgrounds']['.col-bg_img'] = [

                ['post-thumb'],
                ['get-started-general', 'min-width: 768px']

            ];
        }

        setBackgroundImages($media_backgrounds);

        ?>

        .slogan-section {
            background-repeat: no-repeat;
            background-size: cover;
            background-position: top center;
        }

    </style>
    <?php
}