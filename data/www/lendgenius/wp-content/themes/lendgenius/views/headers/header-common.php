<?php
/**
 * Set background images for headers depend on page type and image size
 * @var $media_backgrounds - array() of backgrounds
 * 'ID': post id if not need use default
 * 'backgrounds': array of backgrounds witch will create
 *      key = class name for which we create set of backgrounds
 *      value = array of datas for creating background
 *          [0] = thumbnail slug
 *          [1] = media query for which we create background, if background for all resolutions we don't need it
 */
if(is_front_page() || is_page_template('home.php')){

        $media_backgrounds = [
            'ID' => null,
            'backgrounds' => [
                '.another-mobile-view' => [
                    ['hero-image-400-342', 'max-width: 400px'],
                    ['hero-image-770-350', 'max-width: 767px']
                ],
                '.another-view-holder' => [
                    ['hero-image-1280-563', 'min-width: 768px'],
                    ['hero-image-2560-667', 'min-width: 1201px']
                ]
            ]
        ];

} elseif (is_post_type_archive( 'loans' )) {
    $loan_page = get_page_by_path('business-loans');

        $media_backgrounds = [
            'ID' => $loan_page->ID,
            'backgrounds' => [
                '.slogan-section' => [
                    ['hero-image-400-342'],
                    ['hero-image-770-350', 'min-width: 401px'],
                    ['hero-image-1280-563', 'min-width: 769px'],
                    ['hero-image-2560-667', 'min-width: 1201px']
                ]
            ]
        ];

}
?>
<style>
    <?php

    if (!empty($media_backgrounds )) {
        setBackgroundImages($media_backgrounds);
    }
    ?>
</style>
