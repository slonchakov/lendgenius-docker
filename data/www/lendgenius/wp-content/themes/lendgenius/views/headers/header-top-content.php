<header id="header" class="clearfix">
    <div class="container">
        <div id="logo" class="pull-left"><a href="/"><span>Lend</span><span>Genius</span><sup>TM</sup></a> <img src="<?php echo get_stylesheet_directory_uri() ?>/styles/style-installment/logo-tv.svg" alt="as seen on TV"></div>
        <div class="pull-right">
            <div class="nav-menu">
                <div class="nav-button">
                    <span>Menu</span>
                    <div class="humburger">
                        <div class="line"></div>
                        <div class="line"></div>
                        <div class="line"></div>
                    </div>
                </div>

                <?php wp_nav_menu(['menu' => 'Right menu']); ?>

            </div>
        </div>
    </div>
</header>