<?php
/*
    Template Name: Get started
*/
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <script src='//cdn.freshmarketer.com/182106/488542.js'></script>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>LendGenius - Personal and short term financing. Connect with a lender in as little as 5 minutes | LendGenius.com</title>

    <link href="<?php bloginfo('template_directory'); ?>/getstarted/css/style.css" rel="stylesheet">

    <style type="text/css">

        .header {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            width: 100%;
            padding: 31px 0;
            z-index: 12;
            background-color: #fff;
        }
        
        .form-page.form-loader jsf-form{
            padding-bottom: 30px !important;
        }

        @media (min-width: 992px) {
            .header__menu {
                display: flex;
            }

            .header .logo {
                background: url(<?php bloginfo('template_directory'); ?>/assets/images/logo-lendgenius.png) no-repeat center;
                background-size: contain;
            }

            .header {
                position: absolute;
                background-color: transparent;
                box-shadow: none;
                padding: 25px 0;
            }

            .l-page-nav {
                display: none;
            }

            .footer .l-page-nav {
                padding: 40px 30px 0;
                display: block;
            }

        }
        }

    </style>

    <script>
        dataLayer = [
            { 'c': 'l', 'v': 'ust', 'st': 'p' }
        ];
    </script>

    <?= stripcslashes(get_option('google_tag_manager')); ?>

    <script type='application/ld+json'>
        {
            "@context": "http://schema.org/",
            "@type": "Organization",
            "name": "LendGenius",
            "description": "LendGenius is an online personal loan matching platform connecting borrowers with multiple lenders.",
            "email": "support@lendgenius.com",
            "logo": "https://lendgeniuslandingstorage.azureedge.net/wp-uploads/2017/04/lendgenius-thumbnail.png",
            "telephone": "+18003480997",
        "url": "https://www.lendgenius.com/",
            "address": {
                "@type": "PostalAddress",
                "addressCountry": "United States",
                "addressLocality": "Woodland Hills",
                "addressRegion": "California",
                "postalCode": "91367",
                "streetAddress": "21600 Oxnard St. #400",
                "email": "support@lendgenius.com",
                "telephone": "+18003480997",
                "name": "LendGenius"
            },
        "sameAs" :  [ "https://www.facebook.com/lendgenius",
        "https://twitter.com/lendgenius",
        "https://plus.google.com/114339560599887475172",
        "https://www.linkedin.com/company/lendgenius/",
        "https://www.instagram.com/lendgenius/",
        "https://www.youtube.com/channel/UCCv7gNH7RQLQB0MoaHSIlCw" ]}
        </script>
</head>

<body>
<?= stripcslashes(get_option('google_tag_manager_noscript')); ?>

<header class="header">
    <div class="l-container-fluid l-content">
        <div class="logo-container">
            <div id="logo" ><a href="/"><span>Lend</span><span>Genius</span><sup>TM</sup></a></div>
        </div>

        <div class="pull-right">
            <div class="nav-menu">
                <div class="nav-button">
                    <span>Menu</span>
                    <div class="humburger">
                        <div class="line"></div>
                        <div class="line"></div>
                        <div class="line"></div>
                    </div>
                </div>

                <?php wp_nav_menu([
                    'menu' => 'Right menu'
                ]); ?>

            </div>
        </div>

    </div>
</header>
<div class="l-wrapper">

    <section class="home-main" style="background-image: url(<?= imgpath( get_template_directory_uri() .'/getstarted/images/bg-main.jpg') ?>);">
        <div class="home-main__body">
            <h1 class="title">
                When you need money, we'll help you find the lender.
            </h1>
            <div class="form-page form-loader">
                <?php get_template_part('views/parts/jsf-form')?>
            </div>
        </div>
        <div class="home-main__footer">
            <ul class="home-main__info">
                <li class="item">
                    Competitive Interest Rates
                </li>
                <li class="item">
                    $2,500 - $35,000
                </li>
                <li class="item">
                    Funding in as fast as 1 day
                </li>
            </ul>
        </div>

    </section>

    <section id="howitworks" class="how-works">
        <div class="l-container-fluid">
            <div class="how-works__head">
                <h2 class="how-works__title">How <span>LendGenius<sup>TM</sup></span> works</h2>
                <h4 class="how-works__subtitle">One or more loan offers in as fast as five minutes</h4>
            </div>
            <div id="slider" class="how-works__body">
                <div class="how-works__image-wr">
                    <img class="how-works__image" src="<?php bloginfo('template_directory'); ?>/getstarted/images/form1.svg" alt="" />
                    <img hidden class="how-works__image" src="<?php bloginfo('template_directory'); ?>/getstarted/images/form2.svg" alt="" />
                    <img hidden class="how-works__image" src="<?php bloginfo('template_directory'); ?>/getstarted/images/form3.svg" alt="" />
                </div>
                <ol class="how-works__list" is-active="false">
                    <li data-item-id="0" class="how-works__item">
                        <h3 class="how-works__item-head">
                            Fill out our form
                        </h3>
                        <div class="how-works__item-body">
                            Save time by filling out one simple form and our network of lenders will review your information.
                        </div>
                    </li>
                    <li data-item-id="1" class="how-works__item">
                        <h3 class="how-works__item-head">
                            One or more loan offers
                        </h3>
                        <div class="how-works__item-body">
                            We are committed to helping you get connected with a lender. You could receive up to five loan offers.
                        </div>
                    </li>
                    <li data-item-id="2" class="how-works__item">
                        <h3 class="how-works__item-head">
                            Connect with a lender
                        </h3>
                        <div class="how-works__item-body">
                            We connect you with a lender who can deposit the money directly to your bank account, sometimes in as fast as 24 hours.
                        </div>
                    </li>
                </ol>
            </div>
            <div id="mobile-slider" class="swipe mobile-slider how-works__body">
                <ul class="how-works__list swipe-wrap">
                    <li class="how-works__item">
                        <img class="how-works__image" src="<?php bloginfo('template_directory'); ?>/getstarted/images/m-form1.svg" alt="" />
                        <h3 class="how-works__item-head">
                            Fill out our form
                        </h3>
                        <div class="how-works__item-body">
                            Save time by filling out one simple form and our network of lenders will review your information.
                        </div>
                    </li>
                    <li class="how-works__item">
                        <img class="how-works__image" src="<?php bloginfo('template_directory'); ?>/getstarted/images/m-form2.svg" alt="" />
                        <h3 class="how-works__item-head">
                            One or more loan offers
                        </h3>
                        <div class="how-works__item-body">
                            We are committed to helping you get connected with a lender. You could receive up to five loan offers.
                        </div>
                    </li>
                    <li class="how-works__item">
                        <img class="how-works__image" src="<?php bloginfo('template_directory'); ?>/getstarted/images/m-form3.svg" alt="" />
                        <h3 class="how-works__item-head">
                            Connect with a lender
                        </h3>
                        <div class="how-works__item-body">
                            We connect you with a lender who can deposit the money directly to your bank account, sometimes in as fast as 24 hours.
                        </div>
                    </li>
                </ul>
                <ul class="dots-show-state">
                    <li class="dots-show-state__item active">
                    </li>
                    <li class="dots-show-state__item">
                    </li>
                    <li class="dots-show-state__item">
                    </li>
                </ul>
            </div>
        </div>
    </section>
</div>
<!-- Footer-->

<footer class="footer">
    <div class="l-page-nav">
<!--        <nav class="navigation">-->
<!--            <a href="../#howitworks">How It Works</a>-->
<!--            <a href="/Questions.html">Questions</a>-->
<!--            <a href="/Unsubscribe.html">Unsubscribe</a>-->
<!--            <a href="/PrivacyPolicy.html">Privacy Policy</a>-->
<!--            <a href="/TermsOfUse.html">Terms Of Use</a>-->
<!--            <a href="/Disclaimer.html">Disclaimer</a>-->
<!--            <a href="/Contact.html">Contact</a>-->
<!--            <a href="/EConsent.html">E-Consent</a>-->
<!--        </nav>-->
    </div>
    <div class="l-container">
        <div class="disclaimer">
            <p><strong>Important Disclosures. Please Read Carefully.</strong></p>
            <p>
                Persons facing serious financial difficulties should consider other alternatives or should seek out professional financial advice.
            </p>
            <p>
                This website is not an offer to lend.
                Lendgenius.com is not a lender or lending partner and does not make loan or credit decisions.
                Lendgenius.com connects interested persons with a lender or lending partner from its network of approved lenders and lending partners.
                Lendgenius.com does not control and is not responsible for the actions or inactions of any lender or lending partner, is not an agent, representative or broker of any lender or lending partner, and does not endorse any lender or lending partner.
                Lendgenius.com receives compensation from its lenders and lending partners, often based on a ping-tree model similar to Google AdWords where the highest available bidder is connected to the consumer.
                Regardless, Lendgenius.com’s service is always free to you.
            </p>
            <p>
                This service is not available in all states.
                If you request to connect with a lender or lending partner in a particular state where such loans are prohibited, or in a location where Lendgenius.com does not have an available lender or lending partner, you will not be connected to a lender or lending partner.
                You are urged to read and understand the terms of any loan offered by any lender or lending partner, and to reject any particular loan offer that you cannot afford to repay or that includes terms that are not acceptable to you.
            </p>
            <p>
                By submitting your information via this website, you are authorizing Lendgenius.com
                and/or lenders and lending partners in its network or other intermediaries to do a credit check,
                which may include verifying your social security number, driver license number or other identification,
                and a review of your creditworthiness. Credit checks are usually performed by one of the major credit bureaus such as Experian,
                Equifax and Trans Union, but also may include alternative credit bureaus such as Teletrack, DP Bureau or others. You also authorize
                Lendgenius.com to share your information and credit history with its network of approved lenders and lending partners.
            </p>
            <p>Our lenders offer loans with an Annual Percentage Rate (APR) of 35.99% and below. For qualified consumers, the maximum APR (including the interest rates plus fees and other costs) is 35.99%. All loans are subject to the lender’s approval based on its own unique underwriting criteria.</p>
            <p>Example: Loan Amount: $4,300.00, Annual Percentage Rate: 35.99%. Number of Monthly Payments: 30. Monthly Payment Amount: $219.36. Total Amount Payable: $6,581.78</p>
            <p>Loans include a minimum repayment plan of 12 months and a maximum repayment plan of 30 months.</p>
            <p>
                In some cases, you may be given the option of obtaining a loan from a tribal lender. Tribal lenders are subject to tribal and certain federal laws while being immune from state law including usury caps. If you are connected to a tribal lender, please understand that the tribal lender’s rates and fees may be higher than state-licensed lenders. Additionally, tribal lenders may require you to agree to resolve any disputes in a tribal jurisdiction. You are urged to read and understand the terms of any loan offered by any lender, whether tribal or state-licensed, and to reject any particular loan offer that you cannot afford to repay or that includes terms that are not acceptable to you.
            </p>
            <br>
            <p><strong>Lender’s or Lending Partner’s Disclosure of Terms.</strong></p>
            <p>The lenders and lending partners you are connected to will provide documents that contain all fees and rate information pertaining to the loan being offered, including any potential fees for late-payments and the rules under which you may be allowed (if permitted by applicable law) to refinance, renew or rollover your loan. Loan fees and interest rates are determined solely by the lender or lending partner based on the lender’s or lending partner’s internal policies, underwriting criteria and applicable law. Lendgenius.com has no knowledge of or control over the loan terms offered by a
                lender and lending partner. You are urged to read and understand the terms of any loan offered by any lenders and lending partners and to reject any particular loan offer that you cannot afford to repay or that includes terms that are not acceptable to you.</p>
            <br>
            <p><strong>Late Payments Hurt Your Credit Score</strong></p>
            <p>
                Please be aware that missing a payment or making a late payment can negatively impact your credit score.
                To protect yourself and your credit history, make sure you only accept loan terms that you can afford to repay.
                If you cannot make a payment on time, you should contact your lenders and lending partners immediately and discuss how to handle late payments.
            </p>                       <br><br>
            </div>
        <div class="copyright">

            <div class="footer-nav text-center">
                <a href="/privacy-policy/">Privacy Policy</a>
                <a href="/terms-of-use/">Terms of Use</a>
            </div>
            Copyright &copy; <?= date("Y"); ?> LendGenius.com. All Rights Reserved
        </div>
    </div>
</footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://www.sparning.com/hit/hit.core.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/getstarted/js/general.static.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/getstarted/js/common.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/getstarted/js/custom.js"></script>


<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/getstarted/js/swipe.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/getstarted/js/slider.js"></script>

<script>
    $('a[href*="#howitworks"]').click(function () {
        $('html, body').animate({
            scrollTop: $("#howitworks").offset().top
        }, 1000);

    });
</script>

</body>
</html>
