<?php
/*
 * Template name: 1 Question From
 *
 *
 */

$campaignId = get_option('sts_company_id');

if (!empty($_GET['c'])) {
    $campaignId = $_GET['c'];
} else if (!empty($_COOKIE['CID'])) {
    $campaignId = $_COOKIE['CID'];
}

?>

<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="google-site-verification" content="0vHphrtA59XVVa8JNf9MU2PtfnAMp1UfQTKudNEurAc" />
    <link href="https://plus.google.com/114339560599887475172" rel="publisher" />


    <?php do_action( 'et_head_meta' ); ?>

    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

    <script src='//cdn.freshmarketer.com/182106/488542.js'></script>

    <?php $template_directory_uri = get_template_directory_uri(); ?>
    <!--[if lt IE 9]>
    <script src="<?php echo esc_url( $template_directory_uri . '/js/html5.js"' ); ?>" type="text/javascript"></script>
    <![endif]-->

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />

    <title><?php wp_title( '|', true, 'right' ); ?></title>

    <?php wp_head(); ?>

    <link rel="SHORTCUT ICON" href="/favicon.ico">

    <link rel="stylesheet" href="/wp-content/themes/lendgenius/style-2018/css/nouislider.min.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/assets/styles/icomoon.min.css?ver=<?php echo get_option( 'sts_script_version' ); ?>">
    <link rel="stylesheet" href="/wp-content/themes/lendgenius/style-2018/css/header-footer.css?ver=<?php echo get_option( 'sts_script_version' ); ?>">
    <link rel="stylesheet" href="/wp-content/themes/lendgenius/style-2018/css/inner.css?ver=<?php echo get_option( 'sts_script_version' ); ?>">

    <script>
        window['_fs_debug'] = false;
        window['_fs_host'] = 'fullstory.com';
        window['_fs_org'] = 'F70MC';
        window['_fs_namespace'] = 'FS';
        (function(m,n,e,t,l,o,g,y){
            if (e in m) {if(m.console && m.console.log) { m.console.log('FullStory namespace conflict. Please set window["_fs_namespace"].');} return;}
            g=m[e]=function(a,b,s){g.q?g.q.push([a,b,s]):g._api(a,b,s);};g.q=[];
            o=n.createElement(t);o.async=1;o.src='https://'+_fs_host+'/s/fs.js';
            y=n.getElementsByTagName(t)[0];y.parentNode.insertBefore(o,y);
            g.identify=function(i,v,s){g(l,{uid:i},s);if(v)g(l,v,s)};g.setUserVars=function(v,s){g(l,v,s)};g.event=function(i,v,s){g('event',{n:i,p:v},s)};
            g.shutdown=function(){g("rec",!1)};g.restart=function(){g("rec",!0)};
            g.consent=function(a){g("consent",!arguments.length||a)};
            g.identifyAccount=function(i,v){o='account';v=v||{};v.acctId=i;g(o,v)};
            g.clearUserCookie=function(){};
        })(window,document,window['_fs_namespace'],'script','user');
    </script>
</head>

<body <?php body_class(); ?>>
<?php
    get_template_part('views/headers/header-top-content');
?>


<jsf-form></jsf-form>
<script>
    window.lmpost = window.lmpost || {};
    window.lmpost.options = {
        leadtypeid: 44,
        campaignid: '<?= $campaignId ?>',
        campaignKey: '8A20B784-A3E2-4D32-8ACE-58D1F821C099',
        checkBlacklistedABA: 1
    };
</script>
<script async src="https://formrequests.com/personal/1q_h_v1/form-loader.js"></script>

    <style>
        .b2cform {
            z-index: 1;
        }

        .b2c-form .b2c-btn-submit,
        .b2c-form .b2c-btn-verify,
        .b2c-form a.b2c-btn:not(.b2c-btn-back) {
            background: #0065B3;
            color: #ffffff;
        }

        .b2c-form .b2c-btn-submit:hover,
        .b2c-form .b2c-btn-verify:hover {
            background: #003a67;
            color: #ffffff;
        }

        .b2c-progress-bar-wrap .b2c-progress-bar .b2c-progress-bar-line,
        .b2c-step .b2c-row .b2c-radio-row label span {
            background: #0065B3;
            color: #ffffff;
        }

        .b2c-returned select,
        .b2c-returned .b2c-row input,
        .b2c-returned .b2c-title-returned,
        .b2c-step .b2c-row .b2c-hint,
        .b2c-step .b2c-row input, .b2c-step .b2c-row select{
            border-color: #0065B3 !important;
        }

        .b2c-returned select.b2c-valid,
        .b2c-step .b2c-row .b2c-valid:not(.b2c-radio-wrap):not(.b2c-dp-element):not(select):not(.b2c-aba-loader),
        .b2c-returned .b2c-row input.b2c-valid,
        .b2c-step .b2c-row select.b2c-valid {
            border-color: #003a67 !important;
        }

        .b2c-step .b2c-row .b2c-radio-row label :checked+span {
            color: #fff;
            background-color: #003a67;
            border-color: #003a67;
        }

        .b2c-step .b2c-row .b2c-radio-row label span:after {
            background: none;
        }
    </style>

<?php
the_content();
?>

<footer id="footer">

    <div class="container">
        <?php wp_nav_menu([
            'menu' => 'New Footer Menu',
            'container' => '',
            'items_wrap' => '<ul class="footer-nav">%3$s</ul>'
        ]); ?>

        <div class="social-buttons">
            <ul>
                <li><a href="<?= get_option('sts_twitter_url');?>"><i class="icomoon-twitter"></i></a></li>
                <li><a href="<?= get_option('sts_facebook_url');?>"><i class="icomoon-facebook"></i></a></li>
            </ul>
        </div>

        <div class="copyright">Copyright © <?= date('Y');?> LendGenius.com. All Rights Reserved</div>
    </div>

    <?php
    if( strpos($page_template,'page-landing') !== false  && !is_search() ) {
        ?>
        <style>
            footer{
                position: relative;
            }
        </style>
        <div class="secureLogo" style="position:absolute;right: 30px;bottom:19px;">
            <script type="text/javascript"> //<![CDATA[
                var tlJsHost = ((window.location.protocol == "https:") ? "https://secure.comodo.com/" : "http://www.trustlogo.com/");
                document.write(unescape("%3Cscript src='" + tlJsHost + "trustlogo/javascript/trustlogo.js' type='text/javascript'%3E%3C/script%3E"));
                //]]>
            </script>
            <script language="JavaScript" type="text/javascript">
                if (typeof TrustLogo === 'function') {
                    TrustLogo("https://lendgeniuslandingstorage.azureedge.net/wp-uploads/2017/04/comodo_secure_seal_113x59_transp.png", "CL1", "none");
                }
            </script>
            <a  class="hide hidden" href="https://www.instantssl.com/" id="comodoTL">Essential SSL</a>

        </div>
    <?php } ?>
</footer>
</div>

<link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,700&amp;subset=latin-ext" rel="stylesheet">

<script type="text/javascript" src="/wp-content/themes/lendgenius/style-2018/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/wp-content/themes/lendgenius/style-2018/js/jquery.validate.unobtrusive.min.js"></script>
<script type="text/javascript" src="/wp-content/themes/lendgenius/style-2018/js/jquery.mask.min.js"></script>

<script src="/wp-content/themes/lendgenius/style-2018/js/main.js"></script>

<?php wp_footer();  ?>

</body>
</html>
