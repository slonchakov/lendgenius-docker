<form action="/login-submit" class="form form-login" id="LoginForm" method="post">
    <?php if(isset($_GET['noCustomer'])):?>
        <div class="form-error">
            <span class="field-validation-valid" data-valmsg-for="FormError" data-valmsg-replace="true">Invalid customer credentials.</span>
        </div>
    <?php endif;?>
    <fieldset class="a-center">
        <p class="form-apply-title">Secure Login</p>
        <div class="form-row">
            <label for="Email">Enter Your Email Address</label>
            <div class="input-wrap">
                <input class="required" data-val="true" data-val-required="The Email field is required." id="Email" name="Email" placeholder="Email" type="email" value=""/>
                <span class="form-error field-validation-valid" data-valmsg-for="Email" data-valmsg-replace="true"></span>
            </div>
        </div>
        <div class="form-row">
            <label for="SSN">Enter the Last 4 of Your SSN</label>
            <div class="input-wrap"><input autocomplete="off" class="required" data-val="true" data-val-length="The field Last 4 Digits of SSN must be a string with a maximum length of 4." data-val-length-max="4" data-val-regex="Must be 4 digits" data-val-regex-pattern="[0-9]{4}" data-val-required="The Last 4 Digits of SSN field is required." id="SsnLast4" maxlength="4" name="SsnLast4" placeholder="XXXX" type="text" value=""/>
                <span class="form-error field-validation-valid" data-valmsg-for="SsnLast4" data-valmsg-replace="true"></span>

            </div>
        </div>
        <div class="form-row action-row">
            <button type="submit" class="btn btn-login" href="#" id='Login'>Login</button>
        </div>
        <div class="new-request">
            <p>
                <span>Can’t Sign In?</span><br>That’s because your information has expired.<br>You can still request a loan below:</p>
            <a href="/loan-request">Submit New Request</a>
        </div>
    </fieldset>
</form>