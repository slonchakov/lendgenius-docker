<?php
/*
 * Template Name: Account Page
 */

get_header();
?>
<div class="account-page-body">
    <div class="container">
        <h1>Log In To Your Account</h1>

        <p class="login-description">
            Re-submitting a loan request is easy.<br>
            We saved the information from your last request.<br>
            You just have to log in, confirm that your information is still correct and click re-submit!
        </p>

        <?php get_template_part('views/forms/'. get_post_meta(get_the_ID(), 'form', true)); ?>
    </div>
</div>

<div class="page-account-bottom">
    <div class="container">
        <?php the_content(); ?>
    </div>
</div>
<?php
get_footer();