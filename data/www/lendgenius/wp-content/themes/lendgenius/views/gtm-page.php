<?php
/*
 * Template Name: GTM Page
 */
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="robots" content="noindex, nofollow">
    <title><?php wp_title( '|', true, 'right' ); ?></title>

    <?= stripcslashes(get_option('google_tag_manager')); ?>

</head>
<body>
    <?= stripcslashes(get_option('google_tag_manager_noscript')); ?>
</body>
</html>
