<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 28.10.16
 * Time: 13:50
 */

?>
<div class="blog-item-sub-author"><span><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(get_post_meta( get_the_ID() , 'author' , true )); ?></a></span> <i class="fa fa-circle" aria-hidden="true"></i> <span><?php the_date() ?></span></div>
