<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 10.10.16
 * Time: 12:45
 */
?>
<div class="item-author">
    <div class="item-author-avatar" style="background-image: url(<?php echo get_the_post_thumbnail_url(get_post_meta( get_the_ID() , 'author' , true ), '70'); ?>)"></div>
    <!--<div class="item-author-avatar" style="background-image: url(http://www.piczme.com/piczme/88/main/52a8b689f217d.jpg)"></div>-->
    <div class="flx-gr-2">
        <a class="item-author_link dark-link" href="<?php echo get_permalink(get_post_meta( get_the_ID() , 'author' , true )); ?>">
            <?php echo get_the_title(get_post_meta( get_the_ID() , 'author' , true )); ?>
        </a> <br />
        <span class='light-font'>on <?php echo get_the_date() ?></span>
    </div>
    <div class='blog-read-time'>
        Read in <?php echo do_shortcode('[est_time]'); ?>
    </div>
    <!--div class="item-author-info"><?php the_author_meta( 'description' ); ?></div-->
</div>
