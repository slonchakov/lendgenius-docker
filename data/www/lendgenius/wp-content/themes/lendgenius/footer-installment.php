<footer id="footer">

    <div class="container">
        <?php wp_nav_menu([
            'menu' => 'New Footer Menu',
            'container' => '',
            'items_wrap' => '<ul class="footer-nav">%3$s</ul>'
        ]); ?>


    </div>

</footer>
</div>

<div id="blog-disclosure-box" class="disclosure-box">
    <div class="container">
        <?= strip_shortcodes(get_page_by_path('personal')->post_content); ?>
    </div>
</div>

<div class="container">
    <div class="social-buttons">
        <ul>
            <li><a aria-label="twitter" href="<?= get_option('sts_twitter_url');?>"><i class="icomoon-twitter"></i></a></li>
            <li><a aria-label="facebook" href="<?= get_option('sts_facebook_url');?>"><i class="icomoon-facebook"></i></a></li>
        </ul>
    </div>

    <div class="copyright">Copyright © <?= date('Y');?> LendGenius.com. All Rights Reserved</div>
</div>

<?php

wp_footer();


?>

<script defer async src='//cdn.freshmarketer.com/182106/488542.js'></script>

</body>
</html>
