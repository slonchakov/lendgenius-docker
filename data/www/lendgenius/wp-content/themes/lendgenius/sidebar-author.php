<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 24.10.16
 * Time: 12:28
 */

if ( is_active_sidebar( 'sidebar-author' )  ) : ?>
    <div class="content-aside">
        <?php dynamic_sidebar( 'sidebar-author' ); ?>
    </div>
<?php endif; ?>