<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 12.10.16
 * Time: 16:57
 */
?>

<div class="blog-item">
    <div class="blog-item-wraper">
        <div class="item-image item-image-opacity">
            <a href="<?php echo get_permalink(); ?>">
                <?php
                // Post thumbnail.
                single_post_thumbnail('top-article-thumb');
                ?>
            </a>
        </div>
        <h4 class="blog-item-title"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h4>
        <h6><?php echo get_the_title(get_post_meta( get_the_ID() , 'author' , true )); ?> / <?php the_date() ?></h6>
    </div>
</div>
