<?php
/*
    Template Name: Resources Calculators
*/
get_header();
?>

<section class="title_banner">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-push-6">
                <div class="calculator">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/calculator.png" alt="calculator">
                </div>
            </div>
            <div class="col-sm-6 col-sm-pull-6">
                <h1>Easy as 1, 2, 3!</h1>
                <p>
                    Make simple arithmetic out of business finance with loan calculators that help you fill in the blank.
                </p>
            </div>
        </div>
    </div>
</section>
<section class="contntent_after_title_banner">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-sm-6">
                <h2>Plug & Play</h2>
            </div>

                <?php while ( have_posts() ) : the_post();
                    echo the_content();
                endwhile;
                ?>

        </div>
    </div>
</section>

<section class="bussines_loan">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2>Business Loan Calculators by Loan Type </h2>
            </div>
        </div>
        <div class="row bussines_loan--group">
            <?php
            // The Query
            $args_c = array(
                'post_type' => 'page',
                'post_parent' => get_the_ID(),
                'posts_per_page' => 6
            );
            $query_c = new WP_Query( $args_c );
            if ( $query_c->have_posts() ) {
                // The Loop
                while ( $query_c->have_posts() ) : $query_c->the_post(); ?>

                    <div class="col-md-4 col-sm-6 bussines_loan--button">
                        <a href="<?php echo get_permalink(); ?>"><?php the_title();?></a>
                    </div>

                <?php endwhile;
                wp_reset_postdata();
            }
            ?>
        </div>
    </div>
</section>

<section class="calc_table">
    <div class="main_row green_line">
        <div class="container">
            <div class="grid">
                <div class="column column_1">
                    <h5>Loan Product</h5>
                </div>
                <div class="column column_2">
                    <h5>Unique Value Proposition</h5>
                </div>
                <div class="column column_3">
                    <h5>Repayment</h5>
                </div>
                <div class="column column_4">
                    <h5>Rates</h5>
                </div>
            </div>
        </div>
    </div>
    <div class="main_row secondary">
        <div class="container">
            <div class="grid">
                <?php
                // The Query
                $args = array(
                    'post_type' => 'loans',
                    'orderby' => 'ID',
                    'order' => 'ASC'
                );
                $query = new WP_Query( $args );
                $post_count = $query->post_count;
                if ( $query->have_posts() ) {
                    // The Loop
                    while ( $query->have_posts() ) : $query->the_post();

                        get_template_part( 'content-resources-calc', get_post_format() );

                    endwhile;
                    wp_reset_postdata();
                }
                ?>
            </div>
        </div>
    </div>
</section>

<?php
//How much Block
get_template_part( 'content-how-much', get_post_format() );

//Features Block
get_template_part( 'content-features-block', get_post_format() );

get_footer();
