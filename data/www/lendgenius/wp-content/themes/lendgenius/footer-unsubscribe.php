<footer id="footer">

    <div class="container">
        <?php wp_nav_menu([
            'menu' => 'New Footer Menu',
            'container' => '',
            'items_wrap' => '<ul class="footer-nav">%3$s</ul>'
        ]); ?>

        <div class="social-buttons">
            <ul>
                <li><a href="<?= get_option('sts_twitter_url');?>"><i class="icomoon-twitter"></i></a></li>
                <li><a href="<?= get_option('sts_facebook_url');?>"><i class="icomoon-facebook"></i></a></li>
            </ul>
        </div>

        <div class="copyright">Copyright © 2018 LendGenius.com. All Rights Reserved</div>
    </div>

    <div class="disclosure-box">
        <div class="container">
            <?= strip_shortcodes(get_page_by_path('new-front-page')->post_content); ?>
        </div>
    </div>

    <?php
    if( strpos($page_template,'page-landing') !== false  && !is_search() ) {
        ?>
        <style>
            footer{
                position: relative;
            }
        </style>
        <div class="secureLogo" style="position:absolute;right: 30px;bottom:19px;">
            <script type="text/javascript"> //<![CDATA[
                var tlJsHost = ((window.location.protocol == "https:") ? "https://secure.comodo.com/" : "http://www.trustlogo.com/");
                document.write(unescape("%3Cscript src='" + tlJsHost + "trustlogo/javascript/trustlogo.js' type='text/javascript'%3E%3C/script%3E"));
                //]]>
            </script>
            <script language="JavaScript" type="text/javascript">
                if (typeof TrustLogo === 'function') {
                    TrustLogo("https://lendgeniuslandingstorage.azureedge.net/wp-uploads/2017/04/comodo_secure_seal_113x59_transp.png", "CL1", "none");
                }
            </script>
            <a  class="hide hidden" href="https://www.instantssl.com/" id="comodoTL">Essential SSL</a>

        </div>
    <?php } ?>
</footer>

<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,700&amp;subset=latin-ext" rel="stylesheet">
<!-- inject:js -->
<script src="/wp-content/themes/lendgenius/assets/js/bundle/assets-bundle.min-5fc39a089ac7d6e99ec10991bc5231c4.js"></script>
<!-- endinject -->
<script src="/wp-content/themes/lendgenius/style-2018/js/main.js"></script>
<?php wp_footer();  ?>
<!--<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js?v=22"></script>-->
<script src="/wp-content/themes/lendgenius/assets/js/jquery-ui-autocomplete.min.js?ver=<?= $sts_script_version; ?>"></script>

<script src="/wp-content/themes/lendgenius/style-2018/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://www.sparning.com/hit/hit.core.js"></script>

<script src="/wp-content/themes/lendgenius/style-2018/js/unsubscribe.js"></script>



</body>
</html>
