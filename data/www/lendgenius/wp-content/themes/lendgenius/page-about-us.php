<?php
/*
    Template Name: About Us
*/
get_header();
//Custom fields init
$our_mission_1 = get_post_meta( get_the_ID(), 'our_mission_1', true );
$our_mission_2 = get_post_meta( get_the_ID(), 'our_mission_2', true );
$service_1 = get_post_meta( get_the_ID(), 'service_1', true );
$service_2 = get_post_meta( get_the_ID(), 'service_2', true );
$service_3 = get_post_meta( get_the_ID(), 'service_3', true );
$service_4 = get_post_meta( get_the_ID(), 'service_4', true );
$our_culture_label_1 = get_post_meta( get_the_ID(), 'our_culture_label_1', true );
$our_culture_label_2 = get_post_meta( get_the_ID(), 'our_culture_label_2', true );
$our_culture_label_3 = get_post_meta( get_the_ID(), 'our_culture_label_3', true );
$our_subculture_1 = get_post_meta( get_the_ID(), 'our_subculture_1', true );
$our_subculture_2 = get_post_meta( get_the_ID(), 'our_subculture_2', true );
$our_subculture_3 = get_post_meta( get_the_ID(), 'our_subculture_3', true );
$our_culture_1 = get_post_meta( get_the_ID(), 'our_culture_1', true );
$our_culture_2 = get_post_meta( get_the_ID(), 'our_culture_2', true );
$our_culture_3 = get_post_meta( get_the_ID(), 'our_culture_3', true );
$our_values_label_1 = get_post_meta( get_the_ID(), 'our_values_label_1', true );
$our_values_label_2 = get_post_meta( get_the_ID(), 'our_values_label_2', true );
$our_values_label_3 = get_post_meta( get_the_ID(), 'our_values_label_3', true );
$our_values_label_4 = get_post_meta( get_the_ID(), 'our_values_label_4', true );
$our_values_label_5 = get_post_meta( get_the_ID(), 'our_values_label_5', true );
$our_values_label_6 = get_post_meta( get_the_ID(), 'our_values_label_6', true );
$our_values_1 = get_post_meta( get_the_ID(), 'our_values_1', true );
$our_values_2 = get_post_meta( get_the_ID(), 'our_values_2', true );
$our_values_3 = get_post_meta( get_the_ID(), 'our_values_3', true );
$our_values_4 = get_post_meta( get_the_ID(), 'our_values_4', true );
$our_values_5 = get_post_meta( get_the_ID(), 'our_values_5', true );
$our_values_6 = get_post_meta( get_the_ID(), 'our_values_6', true );
$more_about_culture = get_post_meta( get_the_ID(), 'more_about_culture', true );

?>
<div class="page-about-us">
    <div class="page-wraper">
        <div class="page-head">
            <div class="container">
                <h1 class="item-title-primary"><?php the_title(); ?></h1>
            </div>
            <div class="content-ourmission">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-md-4">
                            <h2 class="item-title-primary">Our Mission</h2>
                        </div>
                        <div class="col-xs-12 col-md-4">
                            <p><?php echo $our_mission_1; ?></p>
                        </div>
                        <div class="col-xs-12 col-md-4">
                            <p><?php echo $our_mission_2; ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-body">
            <div class="content-our-services">
                <div class="container">
                    <h2 class="item-title-primary">Our Services</h2>
                    <div class="items-services">
                        <div class="row">
                            <div class="col-xs-6 col-md-3">
                                <div class="item">
                                    <div class="item-image">
                                        <div class="font-icon font-icon-handshake"></div>
                                    </div>
                                    <div class="item-text"><?php echo $service_1; ?></div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-md-3">
                                <div class="item">
                                    <div class="item-image">
                                        <div class="font-icon font-icon-handshake"></div>
                                    </div>
                                    <div class="item-text"><?php echo $service_2; ?></div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-md-3">
                                <div class="item">
                                    <div class="item-image">
                                        <div class="font-icon font-icon-handshake"></div>
                                    </div>
                                    <div class="item-text"><?php echo $service_3; ?></div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-md-3">
                                <div class="item">
                                    <div class="item-image">
                                        <div class="font-icon font-icon-handshake"></div>
                                    </div>
                                    <div class="item-text"><?php echo $service_4; ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-our-culture">
                <div class="container">
                    <ul class="nav nav-justified nav-tabs" role="tablist">
                        <li class="active"><a href="#culture_1" data-toggle="tab"><?php echo $our_culture_label_1; ?></a></li>
                        <li><a href="#culture_2" data-toggle="tab"><?php echo $our_culture_label_2; ?></a></li>
                        <li><a href="#culture_3" data-toggle="tab"><?php echo $our_culture_label_3; ?></a></li>
                    </ul>
                </div>
                <div class="tab-content-wrap">
                    <div class="container">
                        <div class="tab-content">
                            <div class="tab-pane active" id="culture_1">
                                <div class="item">
                                    <div class="col-xs-12 col-md-4">
                                        <div class="item-title"><?php echo $our_culture_label_1; ?></div>
                                        <div class="item-sub-title"><?php echo $our_subculture_1; ?></div>
                                    </div>
                                    <div class="col-xs-12 col-md-8">
                                        <div class="item-text"><?php echo $our_culture_1; ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="culture_2">
                                <div class="item">
                                    <div class="col-xs-12 col-md-4">
                                        <div class="item-title"><?php echo $our_culture_label_2; ?></div>
                                        <div class="item-sub-title"><?php echo $our_subculture_2; ?></div>
                                    </div>
                                    <div class="col-xs-12 col-md-8">
                                        <div class="item-text"><?php echo $our_culture_2; ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="culture_3">
                                <div class="item">
                                    <div class="col-xs-12 col-md-4">
                                        <div class="item-title"><?php echo $our_culture_label_3; ?></div>
                                        <div class="item-sub-title"><?php echo $our_subculture_3; ?></div>
                                    </div>
                                    <div class="col-xs-12 col-md-8">
                                        <div class="item-text"><?php echo $our_culture_3; ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-our-values">
                <div class="container">
                    <div class="item-title-primary">Our values</div>
                    <div class="items-list">
                        <div class="row">
                            <div class="col-xs-12 col-md-6 col-lg-4 item">
                                <div class="item-title"><?php echo $our_values_label_1; ?></div>
                                <div class="item-text"><?php echo $our_values_1; ?></div>
                            </div>
                            <div class="col-xs-12 col-md-6 col-lg-4 item">
                                <div class="item-title"><?php echo $our_values_label_2; ?></div>
                                <div class="item-text"><?php echo $our_values_2; ?></div>
                            </div>
                            <div class="col-xs-12 col-md-6 col-lg-4 item">
                                <div class="item-title"><?php echo $our_values_label_3; ?></div>
                                <div class="item-text"><?php echo $our_values_3; ?></div>
                            </div>
                            <div class="col-xs-12 col-md-6 col-lg-4 item">
                                <div class="item-title"><?php echo $our_values_label_4; ?></div>
                                <div class="item-text"><?php echo $our_values_4; ?></div>
                            </div>
                            <div class="col-xs-12 col-md-6 col-lg-4 item">
                                <div class="item-title"><?php echo $our_values_label_5; ?></div>
                                <div class="item-text"><?php echo $our_values_5; ?></div>
                            </div>
                            <div class="col-xs-12 col-md-6 col-lg-4 item">
                                <div class="item-title"><?php echo $our_values_label_6; ?></div>
                                <div class="item-text"><?php echo $our_values_6; ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-working-at">
                <div class="container">
                    <div class="item-title-primary">Working at LenGenius</div>
                    <div class="item-content">
                        <div class="item-content-title">More Than a Culture</div>
                        <div class="item-content-text"><?php echo $more_about_culture; ?></div>
                    </div>
                    <div class="item-button"><a class="btn btn-success btn-big" href="#">Apply to work at LendGenius</a></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();
?>
