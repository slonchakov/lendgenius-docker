<?php
/*
Template Name: Without Header and Footer
*/
$iframe_url = get_post_meta( get_the_ID(), 'iframe_url', true );
if(!empty($iframe_url)){
    include_once $_SERVER["DOCUMENT_ROOT"].$iframe_url;
}
?>
