<?php
/*
    Template Name: How It Works
*/
get_header();

$tools = get_post_meta( get_the_ID(), 'tools', true );
$calculators = get_post_meta( get_the_ID(), 'calculators', true );
$videos = get_post_meta( get_the_ID(), 'videos', true );
$credit_cards = get_post_meta( get_the_ID(), 'credit_cards', true );
?>
    <div class="page-how-it-works tpl-2">
        <div class="page-wraper">
            <div class="how-it-works-banner">
                <div class="banner-wraper">
                    <div class="table">
                        <div class="table-row">
                            <div class="table-cell-middle side-left">
                                <div class="banner-content">
                                    <div class="baner-title-thin">How It Works Is</div>
                                    <div class="baner-title-bold">A piece of cake</div>
                                    <div class="how-much-search-wraper">
                                        <form action="<?php echo cta_button_link(); ?>">
                                            <div class="how-much-search">
                                                <input class="field-input field-input-big field-input-clear" name="how_much" id="how_much" type="tel" placeholder="How Much Does Your Business Need?">
                                            </div>
                                            <div class="how-much-button">
                                                <button class="field-button btn btn-success" type="submit">See Loan Options</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="table-cell-middle side-right">
                                <div class="banner-content">
                                    <div class="steps-list tpl-vertical">
                                        <div class="steps-list-wraper">
                                            <div class="steps-items">
                                                <div class="item">
                                                    <div class="item-image">
                                                        <div class="item-image-src font-icon-online_form"></div>
                                                    </div>
                                                    <div class="item-description">Fill out</div>
                                                    <div class="item-title">Online Form</div>
                                                </div>
                                                <div class="item">
                                                    <div class="item-image">
                                                        <div class="item-image-src font-icon-loan_options"></div>
                                                    </div>
                                                    <div class="item-description">Know your</div>
                                                    <div class="item-title">Loan Options</div>
                                                </div>
                                                <div class="item">
                                                    <div class="item-image">
                                                        <div class="item-image-src font-icon-money_aproved"></div>
                                                    </div>
                                                    <div class="item-description">Sometimes in less than 24 Hours</div>
                                                    <div class="item-title">Get Funds</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="prt-from-article">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-lg-8">
                            <div class="content-blog content-sub">
                                <?php get_template_part( 'content-top-articles', get_post_format() ); ?>
                            </div><span class="lendgenius-banner"><a class="btn btn-success" href="<?php echo cta_button_link(); ?>">See Loan Options</a></span>
                        </div>
                        <div class="col-xs-12 col-lg-4">
                            <?php
                            //Sidebar
                            get_sidebar('how-it-works');
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="prt-from-article">
                <div class="container">
                    <div class="prt-learn-more">
                        <h2 class="item-title-primary">Learn More About Business Loans & Finance Products</h2>
                        <ul class="learn-more-list">
                            <li><a href="#">
                                    <div class="icon font-icon-small_business"></div>
                                    <div class="title">Small Business Loans</div></a></li>
                            <li><a href="#">
                                    <div class="icon font-icon-sba"></div>
                                    <div class="title">SBA</div></a></li>
                            <li><a href="#">
                                    <div class="icon font-icon-equipment"></div>
                                    <div class="title">Equipment Financing</div></a></li>
                            <li><a href="#">
                                    <div class="icon font-icon-terms"></div>
                                    <div class="title">Term Business Loan</div></a></li>
                            <li><a href="#">
                                    <div class="icon font-icon-creditcards"></div>
                                    <div class="title">Credit Cards</div></a></li>
                            <li><a href="#">
                                    <div class="icon font-icon-startup_loan"></div>
                                    <div class="title">Startup Loan</div></a></li>
                            <li><a href="#">
                                    <div class="icon font-icon-short_term"></div>
                                    <div class="title">Short-Term Business Loan</div></a></li>
                            <li><a href="#">
                                    <div class="icon font-icon-bag"></div>
                                    <div class="title">Personal Loan For Business</div></a></li>
                            <li><a href="#">
                                    <div class="icon font-icon-invoice"></div>
                                    <div class="title">Invoice Financing</div></a></li>
                            <li><a href="#">
                                    <div class="icon font-icon-business-line-credit"></div>
                                    <div class="title">Business LineOf Credit</div></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="prt-from-article">
                <div class="resources-block">
                    <div class="container">
                        <h2 class="item-title-primary">Find Free Useful Recources</h2>
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <div class="image-block"><img class="align-left" src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/img-resources1.png" alt="tools"><strong class="title-text text-uppercase text-center">tools</strong></div>
                                <div class="text-block">
                                    <p><?php echo $tools; ?></p><a class="more" href="#">Learn more</a>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="image-block"><img class="align-left" src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/img-resources2.png" alt="calculators"><strong class="title-text text-uppercase text-center">calculators</strong></div>
                                <div class="text-block">
                                    <p><?php echo $calculators; ?></p><a class="more" href="#">Learn more</a>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="image-block"><img class="align-left" src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/img-resources3.png" alt="videos"><strong class="title-text text-uppercase text-center">videos</strong></div>
                                <div class="text-block">
                                    <p><?php echo $videos; ?></p><a class="more" href="#">Learn more</a>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="image-block"><img class="align-left" src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/img-resources4.png" alt="credit cards"><strong class="title-text text-uppercase text-center">credit cards</strong></div>
                                <div class="text-block">
                                    <p><?php echo $credit_cards; ?></p><a class="more" href="#">Learn more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer();