<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 13.10.16
 * Time: 16:53
 */
?>

<section class="resources-block">
    <div class="container">
        <h2>Find Free Useful Resources</h2>
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <div class="image-block">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/img-resources1.png" alt="tools" class="align-left">
                    <strong class="title-text text-uppercase text-center">tools</strong>
                </div>
                <div class="text-block">
                    <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.</p>
                    <a href="<?php echo get_cus_link('resources-tools'); ?>" class="more">Learn more</a>
                </div>
            </div>
            <div class="col-xs-12 col-md-6">
                <div class="image-block">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/img-resources2.png" alt="calculators" class="align-left">
                    <strong class="title-text text-uppercase text-center">calculators</strong>
                </div>
                <div class="text-block">
                    <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.</p>
                    <a href="<?php echo get_cus_link('resources-calculators'); ?>" class="more">Learn more</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <div class="image-block">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/img-resources3.png" alt="videos" class="align-left">
                    <strong class="title-text text-uppercase text-center">videos</strong>
                </div>
                <div class="text-block">
                    <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.</p>
                    <a href="<?php echo get_cus_link('resources-videos'); ?>" class="more">Learn more</a>
                </div>
            </div>
            <div class="col-xs-12 col-md-6">
                <div class="image-block">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/img-resources4.png" alt="credit cards" class="align-left">
                    <strong class="title-text text-uppercase text-center">credit cards</strong>
                </div>
                <div class="text-block">
                    <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.</p>
                    <a href="<?php echo get_cus_link('resources-credit-cards'); ?>" class="more">Learn more</a>
                </div>
            </div>
        </div>
    </div>
</section>
