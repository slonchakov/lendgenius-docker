<?php
/*
 * Template Name: Home
 */
get_header();
$front_form = get_option( 'sts_enable_form' );

$theId = get_the_ID();
$first_step = get_post_meta( $theId, 'first_step', true );
$second_step = get_post_meta( $theId, 'second_step', true );
$third_step = get_post_meta( $theId, 'third_step', true );

$h2_title_1 = get_post_meta( $theId, 'h2_title_1', true );
$h2_title_2 = get_post_meta( $theId, 'h2_title_2', true );
$title_3 = get_post_meta( $theId, 'title_3', true );

$cta_button_link = cta_button_link();
?>
    <div class="content-funnel funnel-1">
        <?php

        $theId = get_the_ID();

        $form_title = get_post_meta( $theId, 'form_title', true );
        $sub_form_title = get_post_meta( $theId, 'sub_form_title', true );
        ?>
        <div class="another-view-holder">
            <div class="another-mobile-view">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="general-slogan"><?php echo $form_title; ?> <b class="text-uppercase"><? echo $sub_form_title; ?></b></h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <!-- <div class="business-loan-form">
                    <form-app>
                        <div class="loading-form-ng">
                            <div class="spinner-ng">Loading...</div>
                        </div>
                    </form-app>
                </div> -->
                <?= do_shortcode('[lead form="2_home"]'); ?>

            </div>
        </div>

        <div class="steps-list">
            <div class="steps-list-wraper">
                <div class="container">
                    <div class="steps-list-container">
                        <div class="steps-items row br-lg">
                            <div class="item col-sm-4">
                                <div class="item-image">
                                    <i class="icomoon-doc-time ico-gradient"></i>
                                </div>
                                <div class="item-title">Complete Form</div>
                                <div class="item-description"><?php echo $first_step; ?></div>
                            </div>
                            <div class="item col-sm-4">
                                <div class="item-image">
                                    <i class="icomoon-preferences ico-gradient"></i>
                                </div>
                                <div class="item-title">Browse Loans</div>
                                <div class="item-description"><?php echo $second_step; ?></div>
                            </div>
                            <div class="item col-sm-4">
                                <div class="item-image">
                                    <i class="icomoon-dollar-circle ico-gradient"></i>
                                </div>
                                <div class="item-title">Get Funded</div>
                                <div class="item-description"><?php echo $third_step; ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php

        $query = new DBQ('posts');
        $feedback = $query->setFields('ID, post_title, post_content')->order_by('ID')->where("post_type = 'feedbacks' AND post_status='publish'")->limit(1)->query();

        if ($feedback !== null && isset($feedback[0])) {
            ?>
            <section class="user-feedback lightgray-bg">
                <div class="container">
                    <div class="row">
                        <?php
                        $author_url = get_post_meta($feedback[0]->ID, 'author_url', true);
                        ?>
                        <div class="col-sm-7">
                            <h3 class="line-primary">
                                &#10077;<?php echo $feedback[0]->post_content; ?>&#10078;
                            </h3>
                            <div class="user-feedback_name">
                                <?= $feedback[0]->post_title; ?>
                            </div>
                            <?php if (!empty($author_url)) { ?>
                                <a href="<?php echo $author_url; ?>">Read story &rarr;</a>
                            <?php } ?>
                        </div>
                        <div class="col-xs-12 col-sm-5">
                            <div class="user-feedback_photo">
                                <?php
                                // Post thumbnail.
                                echo get_the_post_thumbnail($feedback[0]->ID,'feedback-post', array('class'=> 'img-responsive center-block'));
                                ?>
                            </div>
                        </div>
                        <?php

                        ?>
                    </div>
                </div>

            </section>
            <?php
        }
        ?>
        <section class="finance_products">
            <div class="container">
                <div class="row">
                    <h2 class="text-center"><?php echo $h2_title_1; ?></h2>
                    <div class="grid">
                        <div class="product_line">
                            <?php

                            $query = new DBQ('posts');
                            $loans = $query->setFields('ID, post_title')->order_by('ID')->where("post_type = 'loans'")->limit(10)->query();
                            foreach ($loans as $loan) {
                                include( TEMPLATEPATH .'/content-business-loan-icons.php');
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="options-block">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <div class="text-holder">
                            <?php
                            echo wpautop(get_the_content());
                            ?>
                            <a href="<?php echo $cta_button_link; ?>" class="btn btn-default-success">See Loan Options</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="benefits-wrap lightgray-bg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="text-center"><?php echo $h2_title_2; ?></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="benefits-holder text-center">
                            <?php if( have_rows('why_choose_lg') ): ?>
                                <?php while( have_rows('why_choose_lg') ): the_row();

                                    // vars
                                    $icon        = get_sub_field('why_choose_lg_icon');
                                    $title       = get_sub_field('why_choose_lg_title');
                                    $description = get_sub_field('why_choose_lg_description');
                                    ?>
                                    <div class="benefits-item">
                                        <div class="benefits-item-icon">
                                            <i class="home-sprite <?php echo $icon; ?>"></i>
                                        </div>
                                        <div class="item-name">
                                            <?php echo $title; ?>
                                        </div>
                                        <?php echo $description; ?>
                                    </div>
                                <?php endwhile; ?>

                                <?php $icon = $title = $description = null; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="content-how-much">
            <div class="container">
                <div class="how-much-wraper">
                    <div class="how-much-title"><?php echo $title_3; ?></div>
                    <div class="how-much-form">
                        <div class="row">
                            <div class="col-lg-12">
                                <a href="<?php echo $cta_button_link; ?>" class="btn btn-default-success">See Loan Options</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--
    <?php
        if($front_form == 'on'){
            // get_template_part( 'content-front-get-money', get_post_format() );
        }else{
            // get_template_part( 'content-how-much-home2', get_post_format() );
        }
        ?>
    -->
    </div>
<?php
get_footer();
