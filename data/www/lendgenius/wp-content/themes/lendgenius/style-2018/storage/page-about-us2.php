<?php
/*
    Template Name: About Us(v2)
*/
get_header();
//Custom fields init
$our_mission_1 = get_post_meta( get_the_ID(), 'our_mission_1', true );
$our_culture_1 = get_post_meta( get_the_ID(), 'our_culture_1', true );
$our_values_label_1 = get_post_meta( get_the_ID(), 'our_values_label_1', true );
$our_values_label_2 = get_post_meta( get_the_ID(), 'our_values_label_2', true );
$our_values_label_3 = get_post_meta( get_the_ID(), 'our_values_label_3', true );
$our_values_label_4 = get_post_meta( get_the_ID(), 'our_values_label_4', true );
$our_values_1 = get_post_meta( get_the_ID(), 'our_values_1', true );
$our_values_2 = get_post_meta( get_the_ID(), 'our_values_2', true );
$our_values_3 = get_post_meta( get_the_ID(), 'our_values_3', true );
$our_values_4 = get_post_meta( get_the_ID(), 'our_values_4', true );
$work_at_lendgenius_url = get_post_meta( get_the_ID(), 'work_at_lendgenius_url', true );
$more_about_culture = get_post_meta( get_the_ID(), 'more_about_culture', true );

?>
<div class="ovarlay"></div>
<div class="page-about-us">
    <div class="page-wraper">
        <div class="page-head">
            <div class="container">
                <h1 class="item-title-primary"><?php the_title(); ?></h1>
                <p><?php echo $our_mission_1; ?></p>
            </div>
        </div>
        <div class="page-body">
            <div class="content-culture">
                <div class="container">
                    <div class="item-title-primary">Culture</div>
                    <p><?php echo $our_culture_1; ?></p>
                </div>
            </div>
            <div class="content-our-values">
                <div class="container">
                    <div class="item-title-primary">Values</div>
                    <div class="items-list">
                        <div class="row">
                            <div class="col-xs-12 col-md-6 col-lg-6">
                                <div class="item">
                                    <div class="item-title"><?php echo $our_values_label_1; ?></div>
                                    <div class="item-text"><?php echo $our_values_1; ?></div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6 col-lg-6">
                                <div class="item">
                                    <div class="item-title"><?php echo $our_values_label_2; ?></div>
                                    <div class="item-text"><?php echo $our_values_2; ?></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-6 col-lg-6">
                                <div class="item">
                                    <div class="item-title"><?php echo $our_values_label_3; ?></div>
                                    <div class="item-text"><?php echo $our_values_3; ?></div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6 col-lg-6">
                                <div class="item">
                                    <div class="item-title"><?php echo $our_values_label_4; ?></div>
                                    <div class="item-text"><?php echo $our_values_4; ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-working-at">
                <div class="container">
                    <div class="item-title-primary">Careers at LendGenius</div>
                    <div class="item-content">
                        <div class="item-content-title">More Than a Culture</div>
                        <div class="item-content-text"><?php echo $more_about_culture; ?></div>
                    </div>
                    <div class="item-button"><a class="btn btn-success btn-big" href="<?php echo $work_at_lendgenius_url; ?>">Work at LendGenius</a></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();
?>
