<?php
$template_directory_uri   = get_template_directory_uri();
$stylesheet_directory_uri = get_stylesheet_directory_uri();
$sts_script_version       = get_option( 'sts_script_version' );
$page_template            = get_post_meta( $post->ID, '_wp_page_template', true );

if ( is_page_template( 'page-under-constraction.php' ) ||
    is_page_template( 'page-blank-iframe.php' ) ||
    is_page_template( 'landing-gmail-1.php' ) ||
    is_404() )
{
    ?>
    <script src="<?php echo $template_directory_uri; ?>/assets/js/jquery-1.11.2.min.js"></script>
    <script src="<?php echo $template_directory_uri; ?>/js/jquery.mask.min.js"></script>
    <script src="<?php echo $template_directory_uri; ?>/assets/js/bootstrap.min.js"></script>
    <?php
    wp_footer();
    if(is_page_template( 'landing-gmail-1.php' )){
        if(get_option( 'sts_enable_form' ) == 'on'){ ?>
            <script type="text/javascript">
                window.applicationOptions = {
                    sendShortForm: '<?php echo get_option( 'sts_endpoint_url' ); ?>',
                    leadsource: '<?php echo get_option( 'sts_leadSource' ); ?>',
                    c: <?php echo get_option( 'sts_campaignId' ); ?>,
                    zipCode: '<?php echo get_option( 'sts_zipcode' ); ?>',
                    emailValidateUrl: '<?php echo get_option( 'sts_email_v_url' ); ?>',
                    phoneValidateUrl: '<?php echo get_option( 'sts_phone_v_url' ); ?>',
                    RequestedAmount: '<?php echo $_POST['RequestedAmount']; ?>',
                    CreditScore: '<?php echo $_POST['CreditScore']; ?>',
                    MonthlySales:'<?php echo $_POST['MonthlySales']; ?>',
                    MonthsInBusiness:'<?php echo $_POST['MonthsInBusiness']; ?>'
                }
            </script>
            <!--            <script src="--><?php //echo get_option( 'sts_script_url' ); ?><!--/form_3/form-loader.js?ver=--><?php //echo get_option( 'sts_script_version' ); ?><!--"></script>-->
        <?php } ?>
        <?php
    }
    echo '</body></html>';
    return;
}
?>

<footer id="footer">
    <!-- <div class="footer-info">
        <div class="footer-info-wraper">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3 col-md-2 info-item"><h5>Explore</h5></div>
                    <div class="col-sm-3 col-md-2 info-item"><h5>Need Help ?</h5></div>
                    <div class="col-sm-3 col-md-2 info-item"><a href="/resources/">Resources</a></div>
                    <div class="col-sm-3 col-md-2 info-item to-top">
                        <a class="font-icon font-icon-stop-arrow" href="#"></a>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <div class="footer-nav">
        <div class="footer-nav-wraper">
            <div class="container">
                <!--<a class="font-icon font-icon-arrow-down arrow-to-top" href="#"></a>-->
                <div class="row">
                    <div class="nav-item col-sm-6 col-md-3">
                        <div class='nav-item_title'>
                            Resources
                        </div>
                        <?php if ( is_active_sidebar( 'sidebar-2' )  ) : ?>
                            <?php dynamic_sidebar( 'sidebar-2' ); ?>
                        <?php endif; ?>
                    </div>
                    <div class="nav-item item-help col-sm-6 col-md-3">
                        <div class='nav-item_title'>Need Help ?</div>
                        <?php if ( is_active_sidebar( 'sidebar-3' )  ) : ?>
                            <?php dynamic_sidebar( 'sidebar-3' ); ?>
                        <?php endif; ?>
                    </div>
                    <div class="nav-item col-md-6">
                        <div class="pull-right-md">
                            <div class="item-protect-iconts">
                                <ul class="list-inline">
                                    <li>
                                        <img src='<?= $stylesheet_directory_uri; ?>/assets/images/comodo_badge.svg?ver=<?= $sts_script_version; ?>' alt='Comodo'>
                                    </li>
                                    <li>
                                        <img src='<?= $stylesheet_directory_uri; ?>/assets/images/SC_badge.svg?ver=<?= $sts_script_version; ?>' alt='Secure Cloud'>
                                    </li>
                                </ul>
                                <!--span>We are NOT a lead-generation company.</span-->
                            </div>

                            <?php get_template_part('views/parts/footer-socials'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="footer-copyright-wraper">
            <div class="container">
                <div class="item-terms">
                    <span class="link"><a href="/privacy-policy/">Privacy Policy</a></span>
                    <span class="link"><a href="/terms-of-use/">Terms of Use</a></span>
                    <span class="link"><a href="/disclaimer/">Disclaimer</a></span>
                </div>
                <div class="item-copyright"><span>©<?php echo date('Y'); ?> Lendgenius. All Rights Reserved.</span></div>
            </div>
        </div>
    </div>
    <?php
    if( strpos($page_template,'page-landing') !== false  && !is_search() ) {
        ?>
        <style>
            footer{
                position: relative;
            }
        </style>
        <div class="secureLogo" style="position:absolute;right: 30px;bottom:19px;">
            <script type="text/javascript"> //<![CDATA[
                var tlJsHost = ((window.location.protocol == "https:") ? "https://secure.comodo.com/" : "http://www.trustlogo.com/");
                document.write(unescape("%3Cscript src='" + tlJsHost + "trustlogo/javascript/trustlogo.js' type='text/javascript'%3E%3C/script%3E"));
                //]]>
            </script>
            <script language="JavaScript" type="text/javascript">
                if (typeof TrustLogo === 'function') {
                    TrustLogo("https://lendgeniuslandingstorage.azureedge.net/wp-uploads/2017/04/comodo_secure_seal_113x59_transp.png", "CL1", "none");
                }
            </script>
            <a  class="hide hidden" href="https://www.instantssl.com/" id="comodoTL">Essential SSL</a>

        </div>
    <?php } ?>
</footer>
</div>

<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,700&amp;subset=latin-ext" rel="stylesheet">
<!-- inject:js -->
<script src="/wp-content/themes/lendgenius/assets/js/bundle/assets-bundle.min-5fc39a089ac7d6e99ec10991bc5231c4.js"></script>
<!-- endinject -->
<script src="/wp-content/themes/lendgenius/style-2018/js/main.js"></script>
<?php wp_footer();  ?>
<!--<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js?v=22"></script>-->
<script src="/wp-content/themes/lendgenius/assets/js/jquery-ui-autocomplete.min.js?ver=<?= $sts_script_version; ?>"></script>

<script>
    (function( $ ) {
        $(function() {
            $( "#s" ).autocomplete({
                maxResults: 2,
                source: "/wp-admin/admin-ajax.php?action=my_search",
                minLength: 3,
                appendTo: ".header-panel-wraper .panel-nav",
                classes: {
                    "ui-autocomplete": "autocomplete-search autocomplete-header"
                },
                select: function (event, ui) {
                    window.location = ui.item.link;
                }
            });
            <?php
            if(is_search()){ ?>
            $( ".search-results-section input[name=\"s\"]" ).autocomplete({
                appendTo: ".search-results-section .col-md-12",
                open: function(){
                    var width = $('.search-results-section .ui-autocomplete-input').width();
                    $('.search-results-section .col-md-12 > ul').css('width', width + 30 + 'px');
                },
                maxResults: 2,
                source: "/wp-admin/admin-ajax.php?action=my_search",
                minLength: 3,
                classes: {
                    "ui-autocomplete": "autocomplete-search autocomplete-search_search-page"
                },
                select: function (event, ui) {
                    window.location = ui.item.link;
                }
            });
            <?php } ?>
        });

    })( jQuery );
</script>

<?php if (is_page_template('page-funnel-3a.php')) { ?>
    <script src="<?= $stylesheet_directory_uri; ?>/LG_LP_UPD4/assets/js/owl.carousel.min.js"></script>
    <script src="<?= $stylesheet_directory_uri; ?>/LG_LP_UPD4/assets/js/main.js"></script>
<?php }

if(get_option( 'sts_enable_form' ) == 'on' && !is_front_page() && !is_post_type_archive( 'loans' ) && 1==10){ ?>
    <script type="text/javascript">
        window.applicationOptions = {
            sendShortForm:    '<?= get_option( 'sts_endpoint_url' ); ?>',
            c: <?php echo get_option( 'sts_campaignId' ); ?>,
            leadsource: '<?php echo get_option( 'sts_leadSource' ); ?>'
        }
    </script>
    <script src="<?php echo get_option( 'sts_script_url' ); ?>/hit_id/hit_id.js?ver=<?= $sts_script_version; ?>"></script>
<?php }

if ( is_page_template( 'publisher-partner.php' ) ||
    is_page_template( 'publisher-referral.php' ) ||
    is_page_template( 'publisher-affiliate-v2.php' ) ||
    is_page_template( 'publisher-referral-v2.php' ) ||
    is_page_template( 'publisher-affiliate.php' ) ) {
    ?>
    <script src="<?php echo get_stylesheet_directory_uri() ?>/assets/js/jquery.matchHeight.js"></script>
    <script>
        <?php if(is_page_template( 'publisher-affiliate-v2.php' )) {
        ?>
        function simpleAccordion() {
            $('.accordion-header').click(function() {
                var $this = $(this),
                    accordionHolder = $this.parent(),
                    accordionBody = accordionHolder.find('.accordion-body');
                accordionHolder.toggleClass('open');
                accordionBody.slideToggle();
            });
        }
        jQuery(document).ready(function () {
            $('.match-height').matchHeight();
            $('.match-height-always').matchHeight({
                byRow: false
            });

            simpleAccordion();
        });
        <?php
        } elseif (is_page_template( 'publisher-referral-v2.php' )) {
        ?>
        jQuery(document).ready(function ($) {
            $('.match-height').matchHeight();
            $('.match-height-always').matchHeight({
                byRow: false
            });
        });
        <?php
        } else { ?>
        jQuery(document).ready(function ($) {
            $('.match-height').matchHeight();
        });
        <?php } ?>
    </script>
    <?php
} elseif (is_page_template( 'page-see-loan-options.php' )) { ?>
    <script src="<?php echo $template_directory_uri; ?>/assets/js/bootstrap.min.js"></script>
    <script>
        var
            holderTitleCol = $('.col-bg_img .title-col'),
            holderTitleColWithoutBg = $('.col-without-bg_img .title-col'),
            contentTitleCol = holderTitleColWithoutBg.html(),
            contentTitleForm = $('.col-bg_img .title-col').html();
        function changePlaceTitle() {
            if ($(window).width() < 768) {
                $('.col-without-bg_img .title-col').find('h2').remove();
                holderTitleCol.find('h2').remove();
                holderTitleCol.append(contentTitleCol);

            } else {
                holderTitleCol.find('h2').remove();
                holderTitleCol.append(contentTitleForm);
                $('.col-without-bg_img .title-col').find('h2').remove();
                $('.col-without-bg_img .title-col').append(contentTitleCol);
            }

        }

        function accordionMobileQuestions() {
            jQuery('.btn-accordion').click(function() {
                $this = $(this),
                    accordionItem = $(this).closest('.accordion-holder').find('.accordion-body');
                $this.closest('.accordion-controls').slideUp();
                accordionItem.slideToggle();
            });
        }


        jQuery(document).ready(function($) {
            changePlaceTitle();
            accordionMobileQuestions();
            $(window).resize(function () {
                changePlaceTitle();
            });
        });
    </script>
    <?php
} elseif (strpos($page_template,'page-landing') !== false) {
    ?>
    <script>
        //        jQuery(document).ready(function ($) {
        //            var carouselComparison = $('#carousel-сomparison');
        //            carouselComparison.carousel({
        //                interval: 7000
        //            });
        //            carouselComparison.carousel()
        //            carouselComparison.on("touchstart", function (event) {
        //                var xClick = event.originalEvent.touches[0].pageX;
        //                $(this).one("touchmove", function (event) {
        //                    var xMove = event.originalEvent.touches[0].pageX;
        //                    if (Math.floor(xClick - xMove) > 5) {
        //                        carouselComparison.carousel('next');
        //                    }
        //                    else if (Math.floor(xClick - xMove) < -5) {
        //                        carouselCompgitarison.carousel('prev');
        //                    }
        //                });
        //                carouselComparison.on("touchend", function () {
        //                    $(this).off("touchmove");
        //                });
        //            });
        //        });
    </script>
    <?php

}

if (is_single()) {
    ?>
    <script src="<?= get_stylesheet_directory_uri() ?>/assets/js/additional_scripts.js?ver=<?= $sts_script_version; ?>"></script>
    <?php
}
?>
<?php
$active = 0;
if (is_front_page() && $active == 1) { ?>
    <script type='application/ld+json'>
    {
        "@context": "http://schema.org/",
        "@type": "Organization",
        "name": "LendGenius",
        "description": "LendGenius is an online business loan matching platform connecting small business owners with multiple lenders.",
        "email": "support@lendgenius.com",
        "logo": "https://www.lendgenius.com/wp-content/uploads/2017/04/lendgenius-thumbnail.png",
        "telephone": "18003480997",
    "url": "https://www.lendgenius.com",
        "address": {
            "@type": "PostalAddress",
            "addressCountry": "United States",
            "addressLocality": "Woodland Hills",
            "addressRegion": "California",
            "postalCode": "91367",
            "streetAddress": "21600 Oxnard St. #400",
            "email": "support@lendgenius.com",
            "telephone": "+18003480997",
            "name": "LendGenius"
        },
    "sameAs" :  [ "https://www.facebook.com/lendgenius",
    "https://twitter.com/lendgenius",
    "https://plus.google.com/114339560599887475172",
    "https://www.linkedin.com/company/lendgenius",
    "https://www.instagram.com/lendgenius/",
    "https://www.youtube.com/channel/UCCv7gNH7RQLQB0MoaHSIlCw/" ]}
    </script>
    <?php
}
?>

<?php
global $form_number;
if ($form_number) {
    ?>

    <script type="text/javascript">
        window.applicationOptions = {
            sendShortForm:    '<?= get_option( 'sts_endpoint_url' ); ?>',
            c:                 <?= get_option( 'sts_campaignId' ); ?>,
            leadsource:       '<?= get_option( 'sts_leadSource' ); ?>',
            zipCode:          '<?= get_option( 'sts_zipcode' ); ?>',
            emailValidateUrl: '<?= get_option( 'sts_email_v_url' ); ?>',
            phoneValidateUrl: '<?= get_option( 'sts_phone_v_url' ); ?>'
        }
    </script>
    <!--<script type="text/javascript" src="--><?//= get_option( 'sts_script_url' ); ?><!--/form_--><?//= $form_number;?><!--/form-loader.js?ver=--><?//= get_option( 'sts_script_version' ); ?><!--"></script>-->
<?php } ?>
</body>
</html>
