<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 17.10.16
 * Time: 23:10
 */
/*
Template Name: CustomizableLoanPage
*/

get_header();
$the_ID = get_the_ID();
$loan_icon           = get_post_meta( $the_ID, 'loan_icon', true );
$item_1_title        = get_post_meta( $the_ID, 'item_1_title', true );
$item_1_description  = get_post_meta( $the_ID, 'item_1_description', true );
$item_2_title        = get_post_meta( $the_ID, 'item_2_title', true );
$item_2_description  = get_post_meta( $the_ID, 'item_2_description', true );
$item_3_title        = get_post_meta( $the_ID, 'item_3_title', true );
$item_3_description  = get_post_meta( $the_ID, 'item_3_description', true );
$item_4_title        = get_post_meta( $the_ID, 'item_4_title', true );
$item_4_description  = get_post_meta( $the_ID, 'item_4_description', true );
$features_pros       = get_post_meta( $the_ID, 'features_pros', true );
$features_cons       = get_post_meta( $the_ID, 'features_cons', true );
$header_description  = get_post_meta( $the_ID, 'header_description', true );

$loan_options_title  = get_post_meta( $the_ID, 'loans_options_title', true );
$about_business      = get_post_meta( $the_ID, 'about_business', true );
$about_business_text = get_post_meta( $the_ID, 'about_business_text', true );
$how_it_works_title  = get_post_meta( $the_ID, 'how_it_works_title', true );
$how_it_works_text   = get_post_meta( $the_ID, 'how_it_works_text', true );

$first_accordion_title            = get_post_meta( $the_ID, 'first_accordion_title', true);
$second_accordion_title           = get_post_meta( $the_ID, 'second_accordion_title', true);
$loans_advantages_description     = get_post_meta( $the_ID, 'loans_advantages_description', true );
$loans_disadvantages_description  = get_post_meta( $the_ID, 'loans_disadvantages_description', true );
$how_to_qualify_title             = (!empty(get_post_meta( $the_ID, 'how_to_qualify_title', true ))) ? get_post_meta( $the_ID, 'how_to_qualify_title', true ) : 'How To Qualify';
$how_to_qualify_description       = get_post_meta( $the_ID, 'how_to_qualify_description', true );
$how_to_apply_description         = get_post_meta( $the_ID, 'how_to_apply_description', true );
$how_to_apply_title               = get_post_meta( $the_ID, 'how_to_apply_title', true );
$how_to_apply_content             = get_post_meta( $the_ID, 'how_to_apply_content', true );
?>
<!--source/components/startUp-page.less-->
<section class="loans_subpage_banner" style="background-image:url('<?php echo get_stylesheet_directory_uri() ?>/assets/images/publesher-portal/affilate-pattern.jpg')">
    <div class="darken-lining">
        <div class="container"> <!--.container-fluid (if full-screen)-->
            <div class="row flex flex-align-stretch flex-column-xs">
                <div class="col-sm-12 flex flex-align-center"> <!-- col-sm-5 col-md-7 -->
                    <div class="text-center center-block">
                        <h1><?php the_title(); ?></h1>
                        <p>
                            <?php echo $header_description; ?>
                        </p>
                    </div>
                </div>
                <!-- <div class="col-sm-7 col-md-5 banner-form-wrapper">
                    <?= do_shortcode('[lead form="7"]'); ?>
                </div> -->
            </div>
        </div>
    </div>
</section>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <?php
            breadcrumbs('<div id="breadcrumbs" class="breadcrumbs">','</div>');
            ?>
        </div>
    </div>
</div>
<section class="at_a_glance" id="glance">
    <div class="container">
        <!--div class="row filters-app-holder filters-sub">
          <filters-app>
              <div class="spinner-holder" >
                  <div class="spinner">
                      <div class="double-bounce1"></div>
                      <div class="double-bounce2"></div>
                  </div>
                  <div class="spinner-text">Loading Loan Options...</div>
              </div>
          </filters-app>
          <script async src="/wp-content/themes/lendgenius/assets/js/FiltersSPA/dist/filters.js?ver=<?= get_option( 'sts_script_version' ); ?>"></script>
        </div-->
        <div class="row">
            <div id="at_glance" class="col-md-7 col-sm-6">
                <h2>At a Glance</h2>
                <?php the_excerpt(); ?>
                <div class="row margin-top-30">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="small_grid">
                                <div class="col-xs-6 col">
                                    <div class="image">
                                        <i class="ico-gradient icomoon-dollar-small"></i>
                                    </div>
                                    <div class="content">
                                        <b class="b"><?php echo $item_1_title; ?></b>
                                        <span><?php echo $item_1_description; ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-6 col">
                                    <div class="image">
                                        <i class="ico-gradient icomoon-calendar-small"></i>
                                    </div>
                                    <div class="content">
                                        <b class="b"><?php echo $item_2_title; ?></b>
                                        <span><?php echo $item_2_description; ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-6 col">
                                    <div class="image">
                                        <i class="ico-gradient icomoon-percent-small"></i>
                                    </div>
                                    <div class="content">
                                        <b class="b"><?php echo $item_3_title; ?></b>
                                        <span><?php echo $item_3_description; ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-6 col">
                                    <div class="image">
                                        <i class="ico-gradient icomoon-clock-small"></i>
                                    </div>
                                    <div class="content">
                                        <b class="b"><?php echo $item_4_title; ?></b>
                                        <span><?php echo $item_4_description; ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-md-offset-1 col-sm-5 col-sm-offset-1">
                <div class="header-list">
                    Quick jump to
                </div>
                <ul class="jump-list">
                    <li>
                        <a href="#at_glance">
                            At a Glance
                        </a>
                    </li>
                    <li>
                        <a href="#features">
                            Advantages & Disadvantages
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Business Line of Credit
                        </a>
                    </li>
                    <li>
                        <a href="#how_qualify">
                            How to Qualify
                        </a>
                    </li>
                    <li>
                        <a href="#how_apply">
                            How to Apply
                        </a>
                    </li>
                </ul>
            </div>

            <!-- Block with funding options -->
            <div class="clearfix"></div>
            <div class="col-xs-12">
                <br />
                <?= do_shortcode('[funding]'); ?>
            </div>
            <!-- Block with funding options -->

            <!--div class="col-md-12 next_btn">
                <a href="#" class="">
                    <span>Next</span>
                </a>
                <i class="item-src icomoon font-icon font-icon-equipment"></i>
                <a href="#" class="">
                    <h6>Equipment Financing</h6>
                </a>
            </div-->
        </div>
    </div>
</section>
<?php if($features_pros || $features_cons): ?>
    <section class="sba_loans_features" id="features">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 title-section">
                    <h2 id="cons-pros" class="h2">Advantages & Disadvantages</h2>
                </div>
                <div class="col-sm-6 success">
                    <?php echo $features_pros; ?>
                </div>
                <div class="col-sm-6 error">
                    <?php echo $features_cons; ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
<?php if (get_post_meta( $the_ID, 'cta_1_enable', true )) :
    $cta = get_post_meta( $the_ID, 'cta_1_shortcode', true );
    echo "<br />", do_shortcode($cta);
endif; ?>
<?php if (!empty($about_business) && !empty($about_business_text)) { ?>
    <section class="about-business-txt content_rd">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-lg-9">
                    <div class="page-blog-rd">
                        <div class="item-article">
                            <div class="h2">
                                <h2 id="about-business"><?= $about_business; ?></h2>
                            </div>
                            <?= wpautop(do_shortcode($about_business_text)); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php } ?>
<?php if( have_rows('loans_options_content') ): ?>
    <section class="loans_options lightgray-bg section-padding-x2">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-lg-9">
                    <h2 id="loan-options" class="h2">
                        <?= $loan_options_title; ?>
                    </h2>
                </div>
            </div>
            <div class="row flex flex-wrap flex-align-stretch">

                <?php while( have_rows('loans_options_content') ): the_row();
                    // vars
                    $icon   = get_sub_field('item_icon');
                    $title  = get_sub_field('item_title');
                    $amount = get_sub_field('item_amount');
                    $cost   = get_sub_field('item_cost');
                    $terms  = get_sub_field('item_terms');
                    $time_to_funding  = get_sub_field('time_to_funding');
                    ?>
                    <div class="col-xs-12 col-sm-4 flex flex-align-stretch">
                        <div class="options_item">
                            <div class="options_item-icon">
                                <i class="<?= $icon; ?> ico-gradient"></i>
                            </div>
                            <div class="options_item-title medium matchheight">
                                <?= $title; ?>
                            </div>
                            <div>
                                <dl>
                                    <dt>Amount</dt>
                                    <dd><?= $amount; ?></dd>
                                </dl>
                                <dl>
                                    <dt>Cost</dt>
                                    <dd><?= $cost; ?></dd>
                                </dl>
                                <dl>
                                    <dt>Terms</dt>
                                    <dd><?= $terms; ?></dd>
                                </dl>
                                <dl>
                                    <dt>Time to Funding</dt>
                                    <dd><?= $time_to_funding; ?></dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </section>
<?php endif; ?>
<?php if (!empty($how_it_works_text) || have_rows('business_loans_comparing_table')) : ?>
    <div class="how-works-section section-padding content_rd">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-lg-9">
                    <h2 id="how-it-works"><?= $how_it_works_title; ?></h2>

                    <?= wpautop(do_shortcode($how_it_works_text)); ?>

                    <?php if( have_rows('business_loans_comparing_table') ): ?>
                        <div class="table-compare-holder">
                            <div class="h3">
                                Compared to Other Loan Types...
                            </div>
                            <div class="table-responsive">
                                <table class="table-compare">
                                    <thead>
                                    <tr>
                                        <th>Credit Type</th>
                                        <th>Amount</th>
                                        <th>Cost</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php while( have_rows('business_loans_comparing_table') ): the_row();
                                        // vars
                                        $icon   = get_sub_field('icon');
                                        $title  = get_sub_field('credit_type');
                                        $amount = get_sub_field('amount');
                                        $cost   = get_sub_field('cost');
                                        $link   = get_sub_field('link');
                                        ?>
                                        <tr>
                                            <td>
                                                <div>
                                                    <a href="<?= $link; ?>"> <i class="<?=$icon; ?> ico-gradient"></i> </a>
                                                    <div class="medium success-text"><a href="<?= $link; ?>" class="link-success"><?= $title; ?></a></div>
                                                </div>
                                            </td>
                                            <td>
                                                <?= $amount; ?>
                                            </td>
                                            <td>
                                                <?= $cost; ?>
                                            </td>
                                        </tr>
                                    <?php endwhile; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (get_post_meta( $the_ID, 'cta_2_enable', true )) :
    $cta = get_post_meta( $the_ID, 'cta_2_shortcode', true );
    echo do_shortcode($cta);
endif; ?>
</div>
<div class="steps-list content_rd">
    <div class="steps-list-wraper">
        <div class="container">

            <div class="steps-list-container padding-btm-30">
                <div class="row">
                    <div class="col-md-10 col-lg-9">
                        <div class="steps-items row br-lg">
                            <div class="item col-sm-4">
                                <div class="item-image">
                                    <i class="icomoon-doc-time ico-gradient"></i>
                                </div>
                                <div class="item-step-number">
                                    1
                                </div>
                                <div class="item-title">Complete Form</div>
                                <div class="item-description">We use this information to match you<br> with lenders that want to work with you.</div>
                            </div>
                            <div class="item col-sm-4">
                                <div class="item-image">
                                    <i class="icomoon-preferences ico-gradient"></i>
                                </div>
                                <div class="item-step-number">
                                    2
                                </div>
                                <div class="item-title">Browse Loans</div>
                                <div class="item-description">Compare loan offers &amp; communicate with lenders using our convenient platform.</div>
                            </div>
                            <div class="item col-sm-4">
                                <div class="item-image">
                                    <i class="icomoon-dollar-circle ico-gradient"></i>
                                </div>
                                <div class="item-step-number">
                                    3
                                </div>
                                <div class="item-title">Get Funded</div>
                                <div class="item-description">Consult with an expert to decide, or<br> choose your own financing at any time!</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10 col-lg-9">
                    <?php if( have_rows('loans_steps_accordion') ): ?>
                        <div class="accordion-holder">
                            <?php while( have_rows('loans_steps_accordion') ): the_row();

                                // vars
                                $title  = get_sub_field('title');
                                $text = get_sub_field('text');
                                ?>
                                <div class="accordion-group">
                                    <div class="accordion-header">
                                        <?= $title; ?>
                                    </div>
                                    <div class="accordion-body">
                                        <?= wpautop(do_shortcode($text)); ?>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    <?php endif; ?>
                    <?php if( have_rows('loans_advantages') ): ?>
                        <h2 id="first-accordion"><?= $first_accordion_title; ?></h2>
                        <?= wpautop(do_shortcode($loans_advantages_description)); ?>
                        <div class="accordion-holder">
                            <?php while( have_rows('loans_advantages') ): the_row();

                                // vars
                                $title  = get_sub_field('title');
                                $text = get_sub_field('text');
                                ?>
                                <div class="accordion-group">
                                    <div class="accordion-header">
                                        <?= $title; ?>
                                    </div>
                                    <div class="accordion-body">
                                        <?= wpautop(do_shortcode($text)); ?>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    <?php endif; ?>
                    <?php if( have_rows('loans_disadvantages') ): ?>
                        <h2 id="second-accordion"><?= $second_accordion_title;?></h2>
                        <?= wpautop(do_shortcode($loans_disadvantages_description)); ?>
                        <div class="accordion-holder">
                            <?php while( have_rows('loans_disadvantages') ): the_row();

                                // vars
                                $title  = get_sub_field('title');
                                $text = get_sub_field('text');
                                ?>
                                <div class="accordion-group">
                                    <div class="accordion-header">
                                        <?= $title; ?>
                                    </div>
                                    <div class="accordion-body">
                                        <?= wpautop(do_shortcode($text)); ?>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if (get_post_meta( $the_ID, 'cta_3_enable', true )) :
    $cta = get_post_meta( $the_ID, 'cta_3_shortcode', true );
    echo do_shortcode($cta);
endif; ?>
<?php if (!empty($how_to_qualify_description) || have_rows('loans_instructions')) : ?>
    <section id="how_qualify" class="instruction-qualify">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-lg-9 content_rd">
                    <div class="header_title">
                        <h2 id="how-to-qualify"><?= $how_to_qualify_title; ?></h2>
                        <?= wpautop(do_shortcode($how_to_qualify_description)); ?>
                    </div>
                    <?php if ( have_rows('loans_instructions') ) : ?>
                        <div>
                            <?php $counter = 1;
                            while( have_rows('loans_instructions') ): the_row();
                                // vars
                                $title  = get_sub_field('title');
                                $text = get_sub_field('text');
                                ?>
                                <div class="instruction-item">
                                    <div class="number-holder">
                                        <div class="number">
                                            <?= $counter; ?>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <div class="title bold">
                                            <?= $title; ?>
                                        </div>
                                        <?= wpautop(do_shortcode($text)); ?>
                                    </div>
                                </div>
                                <?php $counter++; ?>
                            <?php endwhile; ?>
                        </div>
                    <?php endif; ?>
                    <?php if( have_rows('lender_choosing') ): ?>
                        <?php while( have_rows('lender_choosing') ): the_row();
                            // vars
                            $title  = get_sub_field('title');
                            $description = get_sub_field('description');
                            ?>
                            <h2 id="lender-choosing"><?= $title; ?></h2>
                            <?= wpautop(do_shortcode($description)); ?>
                            <?php if( have_rows('lender_accordion') ): ?>
                                <?php while( have_rows('lender_accordion') ): the_row();
                                    $title  = get_sub_field('title');
                                    $text = get_sub_field('text');
                                    ?>
                                    <div class="accordion-group">
                                        <div class="accordion-header">
                                            <?= $title; ?>
                                        </div>
                                        <div class="accordion-body">
                                            <?= wpautop(do_shortcode($text)); ?>
                                        </div>
                                    </div>
                                <?php endwhile;
                            endif;
                        endwhile;
                    endif; ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
<?php if(!empty($how_to_apply_description) || have_rows('testimonial_content')) { ?>
    <section id="how_apply" class="section-padding">
        <div class="container content_rd">
            <div class="row">
                <div class="col-lg-12">
                    <h2 id="how-to-apply"><?= $how_to_apply_title; ?></h2>
                    <?= wpautop(do_shortcode($how_to_apply_description)); ?>
                    <div class="row">
                        <div class="col-sm-6">
                            <ul>
                                <?php if( have_rows('how_to_apply_content') ): ?>
                                    <?php while( have_rows('how_to_apply_content') ): the_row();
                                        $item  = get_sub_field('item');
                                        ?>
                                        <li><?= $item; ?></li>
                                    <?php endwhile; endif; ?>
                            </ul>
                        </div>
                        <?php if( have_rows('testimonial_content') ): ?>
                            <div class="col-sm-6">
                                <div class="user-feedback-business">
                                    <?php while( have_rows('testimonial_content') ): the_row();
                                        $avatar  = get_sub_field('avatar');
                                        $author  = get_sub_field('author');
                                        $company = get_sub_field('company');
                                        $text    = get_sub_field('text_of_testimonial');
                                        ?>
                                        <div class="photo">
                                            <img src="<?= $avatar['url']; ?>">
                                        </div>
                                        <div class="quotes">
                                            ❝<?= $text; ?>❞
                                        </div>
                                        <div class="info">
                                            <div class="name">
                                                <?= $author; ?>
                                            </div>
                                            <div>
                                                <?= $company; ?>
                                            </div>
                                        </div>
                                    <?php endwhile; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="col-xs-12">
                <br />
                <?= do_shortcode('[funding]'); ?>
            </div>
        </div>
    </section>

<?php } ?>
<?php
//Features Block
//get_template_part( 'content-features-block', get_post_format() );

get_footer();

?>
<script>
    jQuery(document).ready(function($){

        var list = "", $jumpList = $('.jump-list');

        $('h2').each(function(){
            var $this = $(this),
                href  = ($this.attr('id')) ? $this.attr('id') : $this.parent().attr('id'),
                text  = $this.text();

            if (!href) return;

            list += '<li><a href="#'+ href +'">'+ text +'</a></li>';
        });

        $jumpList.empty().append(list);

        $jumpList.on('click', 'a', function(e){
            e.preventDefault();

            var goTo = $(this).attr('href');

            $('html,body').animate({scrollTop: $(goTo).offset().top - 62},1000)
        })

    });
</script>
