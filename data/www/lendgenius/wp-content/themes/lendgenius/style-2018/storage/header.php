<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="google-site-verification" content="0vHphrtA59XVVa8JNf9MU2PtfnAMp1UfQTKudNEurAc" />
    <link href="https://plus.google.com/114339560599887475172" rel="publisher" />
    <?php if ( is_front_page() ) { ?>
        <meta name="msvalidate.01" content="D07588DA24884897E4C89F3F32048F38" />
    <?php } ?>
    <?php /*elegant_description(); ?>
	<?php elegant_keywords(); ?>
	<?php elegant_canonical(); */?>

    <?php do_action( 'et_head_meta' ); ?>


    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

    <?php $template_directory_uri = get_template_directory_uri(); ?>
    <!--[if lt IE 9]>
    <script src="<?php echo esc_url( $template_directory_uri . '/js/html5.js"' ); ?>" type="text/javascript"></script>
    <![endif]-->

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />

    <title><?php wp_title( '|', true, 'right' ); ?></title>

    <?php wp_head(); ?>

    <?php if ( is_single() ) { ?>
        <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() ?>/assets/styles/additional_styles.css?ver=<?php echo get_option( 'sts_script_version' ); ?>">
    <?php } ?>

    <link rel="SHORTCUT ICON" href="/favicon.ico">
    <?php
    $page_template = get_post_meta( $post->ID, '_wp_page_template', true );
    if ( is_page_template(array('publisher-partner.php', 'publisher-referral.php', 'publisher-affiliate.php')) ) {

        get_template_part('views/headers/header-publisher');

    } else if (is_page_template( array('landing-gmail-1.php', 'page-see-loan-options.php') ) ||
        strpos($page_template,'page-landing') !== false  && !is_search() || // for all landing pages
        preg_match('/publisher-\w{1,10}-v2/', $page_template) // for all pages witch names contain words publisher and v2
    ) {

        get_template_part('views/headers/header-landing');

    } else {

        if (!is_front_page() && !is_page_template('home.php')) { ?>
            <!-- inject:css -->
        <link rel="stylesheet" href="/wp-content/themes/lendgenius/assets/styles/assets-styles-bb75ed2e5ae0ad6d77f4c0a40343cc9e.css">
            <!-- endinject -->
        <?php } else { ?>
            <!-- home-styles:css -->
                    <link rel="stylesheet" href="/wp-content/themes/lendgenius/assets/styles/styles-home-353481c59db9716df786330729c1d51b.css">
            <!-- endinject -->
            <script type='application/ld+json'>
        {
            "@context": "http://schema.org/",
            "@type": "Organization",
            "name": "LendGenius",
            "description": "LendGenius is an online personal loan matching platform connecting borrowers with multiple lenders.",
            "email": "support@lendgenius.com",
            "logo": "https://lendgeniuslandingstorage.azureedge.net/wp-uploads/2017/04/lendgenius-thumbnail.png",
            "telephone": "+18003480997",
        "url": "https://www.lendgenius.com/",
            "address": {
                "@type": "PostalAddress",
                "addressCountry": "United States",
                "addressLocality": "Woodland Hills",
                "addressRegion": "California",
                "postalCode": "91367",
                "streetAddress": "21600 Oxnard St. #400",
                "email": "support@lendgenius.com",
                "telephone": "+18003480997",
                "name": "LendGenius"
            },
        "sameAs" :  [ "https://www.facebook.com/lendgenius",
        "https://twitter.com/lendgenius",
        "https://plus.google.com/114339560599887475172",
        "https://www.linkedin.com/company/lendgenius/",
        "https://www.instagram.com/lendgenius/",
        "https://www.youtube.com/channel/UCCv7gNH7RQLQB0MoaHSIlCw" ]}
        </script>
        <?php }

        get_template_part('views/headers/header-common');
    }

    if (!is_front_page()) { ?>
        <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/assets/styles/icomoon.min.css?ver=<?php echo get_option( 'sts_script_version' ); ?>">
        <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/assets/styles/header-footer.min.css?ver=<?php echo get_option( 'sts_script_version' ); ?>">
    <?php } ?>
    <?= stripcslashes(get_option('google_tag_manager')); ?>
</head>

<body <?php body_class(); ?>>
<?= stripcslashes(get_option('google_tag_manager_noscript')); ?>
<?php
$exclude_header_pages = [
    'page-template-blank.php',
    'page-under-constraction.php',
    'page-funnel-3.php',
    'page-funnel-4.php',
    'page-blank-iframe.php',
    'landing-gmail-1.php'
];



if ( (!is_page_template( $exclude_header_pages ) &&
        //strpos($page_template,'page-landing') === false &&
        !is_404() ) || is_search() ){
    get_template_part('views/headers/header-top-content');
}