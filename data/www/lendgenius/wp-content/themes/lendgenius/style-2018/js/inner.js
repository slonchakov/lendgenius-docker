/**
 * Created by sergey on 05.09.18.
 */
jQuery(document).ready(function ($) {

    var form = $('form.form-login');

    var setError = function (field) {
        var target;
        target = (field == 'Email') ? form.find('#LoginEmail') : form.find('#' + field);

        target.addClass('error');
    };

    // Init redirection from main form.

    if (form.length) {
        var submitBtn = form.find('a.btn'),
            submitHandler = function () {
                var targetUrl = lmpost.urls.supportUrl,
                    options = lmpost.options,
                    project = options.projectName,
                    hitID = options.hituid,
                    query = form.serialize().replace('LoginEmail', 'Email');

                if (form.valid()) {
                    form.submit();
                }
                else {
                    lmpost.blinkErrorElements();
                }

                return false;
            };

        submitBtn.on('click', submitHandler);
    }


    // lmpost.registerHit();

    form.validate({
        rules: {
            Name: {
                required: true
            },
            PhoneHome: {
                required: true,
                minlength: 12
            },
            Message: {
                required: true
            },
            Email: {
                required: true,
                email: true
            }
        },
        messages: {
            Email: {
                required: 'Please enter email address',
                email: "Please enter a valid email address"
            },
            PhoneHome: {
                minlength: "Must be 10 digits"
            }
        }
    });

    $("#PhoneHome").mask("000-000-0000");
    $("#SsnLast4").mask("0000");

    $("#PhoneHome").bind("paste", function () {
        var $this = $(this);

        $this.removeAttr("maxlength");
        $this.unmask();

        // Short pause to wait for paste to complete
        setTimeout(function () {
            var valPhone = $this.val();
            valPhone = valPhone.replace(/-/g, '');

            if (valPhone.substring(0, 2) === '+1') {
                valPhone = valPhone.substring(2)
            } else if (valPhone.substring(0, 1) === '1') {
                valPhone = valPhone.substring(1)
            }

            $this.val(valPhone);
            $this.mask("000-000-0000");
        }, 10);
    });

});

