/**
 * Created by sergey on 17.10.18.
 */
$(document).ready(function(){

    var elements = {
            inputs: {
                loanAmount: $('[name="loan_amount"]'),
                interestRate: $('[name="interest_rate"]'),
                numberOfMonths: $('[name="number_of_months"]')
            },
            results: {
                loanAmount: $('#loan-amount'),
                interestRate: $('#interest-rate'),
                numberOfMonths: $('#number-of-months'),
                monthlyPayments: $('#monthly-payments')
            },
            button: $('#btn-calculate'),
            resultContainer: $('#calculator-result')
        };

    function countMonthlyPayment()
    {

        interest = parseFloat(elements.inputs.interestRate.val()) / 100 / 12;

        var payments = parseInt(elements.inputs.numberOfMonths.val()),
            x        = Math.pow(1 + interest, payments),
            monthly  = (parseFloat(elements.inputs.loanAmount.val())*x*interest)/(x-1);

        if (payments < 1 || payments > 60 ) {
            elements.inputs.numberOfMonths.addClass('invalid');
            elements.inputs.numberOfMonths.on('focus', function(){
                $(this).removeClass('invalid')
            });
            return false;
        }

        return (isFinite(monthly) && monthly > 1) ? monthly.toFixed() : false;
    }

    elements.inputs.loanAmount
        .add(elements.inputs.interestRate)
        .add(elements.inputs.numberOfMonths)
        .on('focus', function(){
            $(this).val("");
        });

    elements.inputs.loanAmount
        .add(elements.inputs.interestRate)
        .add(elements.inputs.numberOfMonths)
        .on('keydown',function(e){

            if (e.key == 0 && $(this).val() === '') {
                e.preventDefault();
            }
            // only numbers dots and commas
            if (e.which !== 8 && !(48 <= e.which && e.which <= 57) && !(96 <= e.which && e.which <= 105) && e.which !== 110 && e.which !== 188 && e.which !== 190 && !(37 <= e.which && e.which <= 40)) {
                e.preventDefault();
            }

            // for number of months just numbers
            if ($(this).attr('name') == 'number_of_months') {
                if (e.which == 190 || e.which == 110 || e.which == 188) {
                    e.preventDefault();
                }
            }
        });

    elements.button.on('click', function(){

        var monthlyPayment = countMonthlyPayment();

        if (!monthlyPayment) return;

        var suffix        = (elements.inputs.loanAmount.val().match(/\./)) ? '' : '.00',
            loanAmount    = parseFloat(elements.inputs.loanAmount.val()).toLocaleString(),
            interestRate  = elements.inputs.interestRate.val(),
            numberOfMonths = elements.inputs.numberOfMonths.val();

        if (interestRate.match(/[,.]/)) {
            interestRate = parseFloat(interestRate).toFixed(2);
        }

        if (numberOfMonths.match(/[,.]/)) {
            numberOfMonths = parseInt(numberOfMonths);
        }

        elements.results.loanAmount.text(loanAmount + suffix);
        elements.results.interestRate.text(interestRate);
        elements.results.numberOfMonths.text(numberOfMonths);
        elements.results.monthlyPayments.text(monthlyPayment);

        $('html, body').animate({scrollTop: elements.resultContainer.offset().top - 20});
    });

    elements.inputs.loanAmount
        .add(elements.inputs.interestRate)
        .add(elements.inputs.numberOfMonths)
        .on('keyup',function(e){
           if (e.keyCode == 13 && $(this).val() !== '') {
               elements.button.click()
           }
        });
});