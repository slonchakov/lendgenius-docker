/**
 * Created by sergey.s on 10.08.18.
 */


jQuery(document).ready(function($){

    // Top nav
    var nav       = $('.menu-right-menu-container'),
        navButton = $('.nav-button');

    navButton.on('click',function(){
        navButton.toggleClass('active');
        nav.toggleClass('active');
    });

    $('.menu-item-has-children > span').on('click',function(e){
        e.preventDefault();
        var item = $(this).parent();
        toggleItem(item);
    });

    $(window).on('scroll', function(){
        if ($(this).width() < 992 && !nav.hasClass('active')) return;
        closeNav();
    });

    $(document).on('click', function(e){
        var elem = $(e.target);

        if (!elem.closest('.nav-menu').length && nav.hasClass('active')) {
            closeNav();
        }
    });

//    Auto-rotate
    var howItWorksBlock = $('.how-it-works-box'),
        rotateItems = howItWorksBlock.find('li'),
        current = first = rotateItems.first(),
        mobile = $(window).width() < 992;


    $('li', howItWorksBlock).on('click',function(){
        var item = $(this);

        howItWorksBlock.addClass('hovered');
        toggleHowItWorks(item);
        current = item;
    });

    $(window).resize(function(){
        mobile = $(window).width() < 992;
    });

    progressBarStart(first);

    setInterval(function(){
        // if (howItWorksBlock.hasClass('hovered') || mobile) return;
        if (howItWorksBlock.hasClass('hovered')) return;
        rotateHowItWorks();
    },3000);

    function rotateHowItWorks(){
        var next = current.next();
        current = (next.length) ? next : first;
        toggleHowItWorks(current);
    }

    function toggleHowItWorks(item){
        item.addClass('active').siblings().removeClass('active');
        $('#'+ item.attr('id') + '-container').addClass('active').siblings().removeClass('active');
        progressBarStart(item);
    }

    function progressBarStart(item){
        item.find('.progressbar').animate({'width':'100%'}, 2999);
        item.siblings().find('.progressbar').stop().width(0);
    }

    function closeNav (){
        navButton.removeClass('active');
        nav.removeClass('active');
    }

    function toggleItem(item){
        item.toggleClass('active').siblings('.menu-item-has-children').removeClass('active').find('.sub-menu').removeClass('active');
        item.find('.sub-menu').toggleClass('active');
    }

    function getURLVar(key) {
        var value = [];

        var query = String(document.location).split('?');

        if (query[1]) {
            var part = query[1].split('&');

            for (i = 0; i < part.length; i++) {
                var data = part[i].split('=');

                if (data[0] && data[1]) {
                    value[data[0]] = data[1];
                }
            }

            if (value[key]) {
                return value[key];
            } else {
                return '';
            }
        }
    }

    var loanAmountSlider = $("#loan-emount-slider-line"),
        preSelectedAmount = parseInt(getURLVar('psamount')),
        startPoint = ( preSelectedAmount) ? preSelectedAmount : 5000;

    if (loanAmountSlider.length) {

        var nonLinearSlider = document.getElementById('slider-body');

        noUiSlider.create(nonLinearSlider, {
            start: [ startPoint ],
            range: {
                'min': 1000,
                'max': 35000
            },
            step: 1000,
            connect: [true, false]
        });

        $('#get-started-button').on('click', function(){
            var popunderLink = $(this).data('popunder');

            setTimeout(function(){
                window.location.href = popunderLink;
            },2000);
        });

        nonLinearSlider.noUiSlider.on('update', function ( values, handle, unencoded, isTap, positions ) {

            var point = parseInt(unencoded[handle]);
            $('.loan-amount-slider .sum span').text(point.toLocaleString());
            $('#get-started-button').attr('href', '/loan-request?RequestedAmount='+ point );
        });
    }

    if ($('body.home').length) {
        $('a[href*="how-it-works"]').on('click', function(e){
            e.preventDefault();
            var howItWorksPosition = $('.how-it-works-box h2').offset().top;

            $('body,html').animate({scrollTop: howItWorksPosition});
        })
    }

    function loadBgImages() {

        jQuery('.bg-lazy').each(function(){

            var elem = jQuery(this);
            if (jQuery(this).data('style')) {
                if (jQuery(window).width() > 768 && !elem.hasClass('bg-lazy-mobile')) {
                    this.style = elem.data('style');
                } else if (jQuery(window).width() < 768 && !elem.hasClass('bg-lazy-desktop')) {
                    this.style = elem.data('style');
                }
            }
        })
    }

    function loadImages() {
        jQuery('.img-lazy').each(function(){

            var elem = jQuery(this);
            if (jQuery(this).data('src')) {
                if (jQuery(window).width() > 768 && !elem.hasClass('img-lazy-mobile')) {
                    elem.attr('src', elem.data('src')) ;
                } else if (jQuery(window).width() < 768 && !elem.hasClass('img-lazy-desktop')) {
                    elem.attr('src',elem.data('src'));
                }
            }
        })
    }
    setTimeout(function(){
        loadBgImages();
        loadImages();
    },500);

    jQuery(window).on('load resize', function(){
        setTimeout(function(){
            loadBgImages();
            loadImages();
        },500);
    });

});