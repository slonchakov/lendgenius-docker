jQuery(document).ready(function($){


    if (!window.lmpost) {
        window.lmpost = {};
    }

    var baseApiUrl = "https://www.loanmatchingservice.com";

    if (!lmpost.urls) lmpost.urls = {};
    if (!lmpost.urls.portalUrl) lmpost.urls.portalUrl = baseApiUrl + '/cserv';
    if (!lmpost.urls.apiUrl) lmpost.urls.apiUrl = 'https://www.loanmatchingservice.com/';
    if (!lmpost.urls.submitUrl) lmpost.urls.submitUrl = 'post/live.aspx';
    if (!lmpost.urls.supportUrl) lmpost.urls.supportUrl = 'https://www.loanmatchingservice.com/misc/';
    if (!lmpost.urls.hitUrl) lmpost.urls.hitUrl = 'https://www.sparning.com/hit/hit.core.js';

    //if (!lmpost.urls.apiUrl) lmpost.urls.apiUrl = baseApiUrl + '/api2/';
    //if (!lmpost.urls.submitUrl) lmpost.urls.submitUrl = 'post/live.aspx';
    //if (!lmpost.urls.supportUrl) lmpost.urls.supportUrl = baseApiUrl + '/api2/misc/';
    //if (!lmpost.urls.hitUrl) lmpost.urls.hitUrl = baseApiUrl + '/api2/hit/hit.core.js';

    lmpost.registerHit = function () {
        var script = document.createElement('script');
        script.setAttribute('type', 'text/javascript');
        script.setAttribute('src', lmpost.urls.hitUrl);
        document.getElementsByTagName('head')[0].appendChild(script);
    };

    lmpost.actionUrl = function (action, params) {
        var url = window.lmpost.urls.apiUrl + '/misc/?responsetype=json&action=' + action,
            proto = url.match(/^(http|https):(\/)+/),
            si = proto ? proto[0].length : 0,
            ps = "";

        params = params || {};
        params.uts = new Date().getTime();
        params.uid = lmpost.options.hituid;

        for (var p in params) {
            var v = params[p];
            if (v) ps += '&' + p + "=" + escape(params[p]);
        }

        return (proto ? proto[0] : "") + url.substr(si).replace(/(\/){2,}/g, '/') + ps;
    };

    lmpost.makeUrl = function (baseUrl) {
        if (!baseUrl.match(/^(http|https):/)) {
            return lmpost.urls.apiUrl + baseUrl;
        }
        else {
            return baseUrl;
        }
    };

    lmpost.scriptUrl = function (scriptPath) {
        var url = lmpost.options.domain + scriptPath,
            proto = url.match(/^(http|https):(\/)+/),
            si = proto ? proto[0].length : 0;

        return (proto ? proto[0] : "") + url.substr(si).replace(/(\/){2,}/g, '/');
    };

    lmpost.blinkErrorElements = function (root) {
        var colorArray = new Array("#fff", "#F94722", "#FFFFFF", "#F94722", "#FFFFFF", "#FEE0DA");
        if (!root) root = jQuery;
        if (root.find(':visible.error').length) {
            root.find(':visible.error').animate({
                backgroundColor: colorArray[1]
            }, 150)
                .animate({
                    backgroundColor: colorArray[2]
                }, 100)
                .animate({
                    backgroundColor: colorArray[3]
                }, 100)
                .animate({
                    backgroundColor: colorArray[4]
                }, 100)
                .animate({
                    backgroundColor: colorArray[5]
                }, 100, function () {
                    $(this).removeAttr('style');
                });
        }

    };

    var isNumeric = function (n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    };

    lmpost.calculateAPR = function (amount, totalFee, numberOfDays) {
        var apr = 0,
            inputReady = isNumeric(amount) && isNumeric(totalFee) && isNumeric(numberOfDays) && amount > 0 && numberOfDays > 0;

        if (!inputReady) {
            return -1;
        }

        apr = 36500 * (totalFee / amount / numberOfDays);

        return apr;
    };

    $(function () {
        var forms = $('form.form-unsubscribe');

        var setError = function (field) {
            var target;
            target = (field == "Email") ? $('#SubscriberEmail') : form.find('#' + field);

            target.addClass('error');
        };

        //Capture incorrect data
        $('#SubscriberPhone').mask('9999999999');
        $('#ZipCode').mask('99999');
        $('#StreetAddress, #City').mask('A', { translation: { 'A': { pattern: /[A-Za-z0-9\s-\\/"']/, recursive: true } } });
        $('#State').mask('A', { translation: { 'A': { pattern: /[A-Za-z\s]/, recursive: true } } });
        $('#SubscriberEmail').mask("A", {
            translation: {
                "A": { pattern: /[\w@\-.+]/, recursive: true }
            }
        });

        // Init redirection from main form.

        for (var i = 0; i < forms.length; i++) {
            var form = $(forms[i]);

            var submitBtn = form.find('a.btn'),
                submitHandler = function () {
                    var form = $(this).closest('form'),
                        targetUrl = lmpost.urls.supportUrl,
                        options = lmpost.options,
                        hitID = options.hituid;

                    if (form.valid()) {
                        $('<div class="processmsg"><p>Please wait. Your data is being processed...</p></div>').insertBefore(form);
                        form.hide();
                        $('div.svcerrormsg').hide();

                        var inputCtrl = form.find('input'),
                            name = inputCtrl.attr('name'),
                            val = inputCtrl.val(),
                            name = name.replace('Subscriber', ''),
                            formValues = '&field=' + name + '&value=' + val;

                        if (inputCtrl.attr('name') == 'StreetAddress') {
                            formValues = '&' + form.serialize() + "&field=address&value=1";
                        }

                        if (form.attr('id') == 'UnsubscribeFormPhone') {
                            targetUrl = 'https://flow.lmnext.io/endpoint/lm-websites-unsubscribe-phone';
                            formValues = '&phone=' + val;
                        }

                        $.ajax(
                            {
                                url: targetUrl + '?action=unsubscribe1&responsetype=json&uid=' + hitID + '&clienturl=' + escape(location.href) + formValues,
                                type: 'get',
                                dataType: 'jsonp',
                                success: function (data) {
                                    if (data && data.Result < 2) {
                                        $('div.thakyoumsg').show();
                                        $('div.svcerrormsg').hide();
                                    }
                                    else {
                                        form.show();
                                        $('div.svcerrormsg').show();
                                        if (data && data.Errors && data.Errors.length) {
                                            $.each(data.Errors, function (index, value) {
                                                    value.Field && setError(value.Field);
                                                }
                                            );
                                        } //if
                                        lmpost.blinkErrorElements(form);
                                    }

                                    $('div.processmsg').remove();
                                },

                                error: function () {
                                    form.show();
                                    $('div.svcerrormsg').show();
                                    $('div.processmsg').remove();
                                }
                            }
                        );
                    }
                    else {
                        lmpost.blinkErrorElements(form);
                    }

                    return false;
                };

            submitBtn.on('click', submitHandler);
            function getParameterByName(name) {
                name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
                var regexS = "[\\?&]" + name + "=([^&#]*)";
                var regex = new RegExp(regexS, "i");
                var results = regex.exec(window.location.search);
                if (results != null) {
                    return decodeURIComponent(results[1].replace(/\+/g, " "));
                }
                return null;
            }

            var email = getParameterByName('email'),
                muid = getParameterByName('msguid');
            if (email) {
                $('#SubscriberEmail').val(email);
                function autoSubmit() {
                    if (lmpost.options && lmpost.options.hituid)
                        submitBtn.click();
                    else
                        window.setTimeout(autoSubmit, 1500);
                }
                if (muid)
                    autoSubmit();
            }
        }


        lmpost.registerHit();

        $("#UnsubscribeForm").validate({
            rules: {
                SubscriberEmail: {
                    required: true,
                    email: true
                }
            },
            messages: {
                SubscriberEmail: "Please enter a valid email address"
            }
        });

        jQuery.validator.setDefaults({
            debug: true,
            success: "valid"
        });

        $("#UnsubscribeFormPhone").validate({
            rules: {
                SubscriberPhone: {
                    required: true,
                    minlength: 10
                }
            }
        });

    });});