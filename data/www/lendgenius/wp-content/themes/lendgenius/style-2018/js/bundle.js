! function() {
    var a = window.MutationObserver || window.WebKitMutationObserver,
        b = "ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch,
        c = void 0 !== document.documentElement.style["touch-action"] || document.documentElement.style["-ms-touch-action"];
    if (!c && b && a) {
        window.Hammer = window.Hammer || {};
        var d = /touch-action[:][\s]*(none)[^;'"]*/,
            e = /touch-action[:][\s]*(manipulation)[^;'"]*/,
            f = /touch-action/,
            g = navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? !0 : !1,
            h = function() {
                try {
                    var a = document.createElement("canvas");
                    return !(!window.WebGLRenderingContext || !a.getContext("webgl") && !a.getContext("experimental-webgl"))
                } catch (b) {
                    return !1
                }
            }(),
            i = h && g;
        window.Hammer.time = {
            getTouchAction: function(a) {
                return this.checkStyleString(a.getAttribute("style"))
            },
            checkStyleString: function(a) {
                return f.test(a) ? d.test(a) ? "none" : e.test(a) ? "manipulation" : !0 : void 0
            },
            shouldHammer: function(a) {
                var b = this.hasParent(a.target);
                return b && (!i || Date.now() - a.target.lastStart < 125) ? b : !1
            },
            touchHandler: function(a) {
                var b = a.target.getBoundingClientRect(),
                    c = b.top !== this.pos.top || b.left !== this.pos.left,
                    d = this.shouldHammer(a);
                ("none" === d || c === !1 && "manipulation" === d) && ("touchend" === a.type && (a.target.focus(), setTimeout(function() {
                    a.target.click()
                }, 0)), a.preventDefault()), this.scrolled = !1, delete a.target.lastStart
            },
            touchStart: function(a) {
                this.pos = a.target.getBoundingClientRect(), i && this.hasParent(a.target) && (a.target.lastStart = Date.now())
            },
            styleWatcher: function(a) {
                a.forEach(this.styleUpdater, this)
            },
            styleUpdater: function(a) {
                if (a.target.updateNext) return void(a.target.updateNext = !1);
                var b = this.getTouchAction(a.target);
                return b ? void("none" !== b && (a.target.hadTouchNone = !1)) : void(!b && (a.oldValue && this.checkStyleString(a.oldValue) || a.target.hadTouchNone) && (a.target.hadTouchNone = !0, a.target.updateNext = !1, a.target.setAttribute("style", a.target.getAttribute("style") + " touch-action: none;")))
            },
            hasParent: function(a) {
                for (var b, c = a; c && c.parentNode; c = c.parentNode)
                    if (b = this.getTouchAction(c)) return b;
                return !1
            },
            installStartEvents: function() {
                document.addEventListener("touchstart", this.touchStart.bind(this)), document.addEventListener("mousedown", this.touchStart.bind(this))
            },
            installEndEvents: function() {
                document.addEventListener("touchend", this.touchHandler.bind(this), !0), document.addEventListener("mouseup", this.touchHandler.bind(this), !0)
            },
            installObserver: function() {
                this.observer = new a(this.styleWatcher.bind(this)).observe(document, {
                    subtree: !0,
                    attributes: !0,
                    attributeOldValue: !0,
                    attributeFilter: ["style"]
                })
            },
            install: function() {
                this.installEndEvents(), this.installStartEvents(), this.installObserver()
            }
        }, window.Hammer.time.install()
    }
}();

"use strict";
! function(a, b, c) {
    "function" == typeof define && define.amd ? define(["jquery"], a) : "object" == typeof exports ? module.exports = a(require("jquery")) : a(b || c)
}(function(a) {
    var b = function(b, c, d) {
        var e = {
            invalid: [],
            getCaret: function() {
                try {
                    var a, c = 0,
                        d = b.get(0),
                        f = document.selection,
                        g = d.selectionStart;
                    return f && navigator.appVersion.indexOf("MSIE 10") === -1 ? (a = f.createRange(), a.moveStart("character", -e.val().length), c = a.text.length) : (g || "0" === g) && (c = g), c
                } catch (a) {}
            },
            setCaret: function(a) {
                try {
                    if (b.is(":focus")) {
                        var c, d = b.get(0);
                        a += 1, d.setSelectionRange ? d.setSelectionRange(a, a) : (c = d.createTextRange(), c.collapse(!0), c.moveEnd("character", a), c.moveStart("character", a), c.select())
                    }
                } catch (a) {}
            },
            events: function() {
                b.on("keydown.mask", function(a) {
                    b.data("mask-keycode", a.keyCode || a.which)
                }).on(a.jMaskGlobals.useInput ? "input.mask" : "keyup.mask", e.behaviour).on("paste.mask drop.mask", function() {
                    setTimeout(function() {
                        b.keydown().keyup()
                    }, 100)
                }).on("change.mask", function() {
                    b.data("changed", !0)
                }).on("blur.mask", function() {
                    g === e.val() || b.data("changed") || b.trigger("change"), b.data("changed", !1)
                }).on("blur.mask", function() {
                    g = e.val()
                }).on("focus.mask", function(b) {
                    d.selectOnFocus === !0 && a(b.target).select()
                }).on("focusout.mask", function() {
                    d.clearIfNotMatch && !h.test(e.val()) && e.val("")
                })
            },
            getRegexMask: function() {
                for (var b, d, e, g, h, i, a = [], j = 0; j < c.length; j++) b = f.translation[c.charAt(j)], b ? (d = b.pattern.toString().replace(/.{1}$|^.{1}/g, ""), e = b.optional, g = b.recursive, g ? (a.push(c.charAt(j)), h = {
                            digit: c.charAt(j),
                            pattern: d
                        }) : a.push(e || g ? d + "?" : d)) : a.push(c.charAt(j).replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&"));
                return i = a.join(""), h && (i = i.replace(new RegExp("(" + h.digit + "(.*" + h.digit + ")?)"), "($1)?").replace(new RegExp(h.digit, "g"), h.pattern)), new RegExp(i)
            },
            destroyEvents: function() {
                b.off(["input", "keydown", "keyup", "paste", "drop", "blur", "focusout", ""].join(".mask "))
            },
            val: function(a) {
                var e, c = b.is("input"),
                    d = c ? "val" : "text";
                return arguments.length > 0 ? (b[d]() !== a && b[d](a), e = b) : e = b[d](), e
            },
            getMCharsBeforeCount: function(a, b) {
                for (var d = 0, e = 0, g = c.length; e < g && e < a; e++) f.translation[c.charAt(e)] || (a = b ? a + 1 : a, d++);
                return d
            },
            caretPos: function(a, b, d, g) {
                var h = f.translation[c.charAt(Math.min(a - 1, c.length - 1))];
                return h ? Math.min(a + d - b - g, d) : e.caretPos(a + 1, b, d, g)
            },
            behaviour: function(c) {
                c = c || window.event, e.invalid = [];
                var d = b.data("mask-keycode");
                if (a.inArray(d, f.byPassKeys) === -1) {
                    var g = e.getCaret(),
                        h = e.val(),
                        i = h.length,
                        j = e.getMasked(),
                        k = j.length,
                        l = e.getMCharsBeforeCount(k - 1) - e.getMCharsBeforeCount(i - 1),
                        m = g < i && j !== h;
                    return e.val(j), m && (8 !== d && 46 !== d ? g = e.caretPos(g, i, k, l) : g -= 1, e.setCaret(g)), e.callbacks(c)
                }
            },
            getMasked: function(a, b) {
                var p, q, g = [],
                    h = void 0 === b ? e.val() : b + "",
                    i = 0,
                    j = c.length,
                    k = 0,
                    l = h.length,
                    m = 1,
                    n = "push",
                    o = -1;
                d.reverse ? (n = "unshift", m = -1, p = 0, i = j - 1, k = l - 1, q = function() {
                        return i > -1 && k > -1
                    }) : (p = j - 1, q = function() {
                        return i < j && k < l
                    });
                for (var r; q();) {
                    var s = c.charAt(i),
                        t = h.charAt(k),
                        u = f.translation[s];
                    u ? (t.match(u.pattern) ? (g[n](t), u.recursive && (o === -1 ? o = i : i === p && (i = o - m), p === o && (i -= m)), i += m) : t === r ? r = void 0 : u.optional ? (i += m, k -= m) : u.fallback ? (g[n](u.fallback), i += m, k -= m) : e.invalid.push({
                                            p: k,
                                            v: t,
                                            e: u.pattern
                                        }), k += m) : (a || g[n](s), t === s ? k += m : r = s, i += m)
                }
                var v = c.charAt(p);
                return j !== l + 1 || f.translation[v] || g.push(v), g.join("")
            },
            callbacks: function(a) {
                var f = e.val(),
                    h = f !== g,
                    i = [f, a, b, d],
                    j = function(a, b, c) {
                        "function" == typeof d[a] && b && d[a].apply(this, c)
                    };
                j("onChange", h === !0, i), j("onKeyPress", h === !0, i), j("onComplete", f.length === c.length, i), j("onInvalid", e.invalid.length > 0, [f, a, b, e.invalid, d])
            }
        };
        b = a(b);
        var h, f = this,
            g = e.val();
        c = "function" == typeof c ? c(e.val(), void 0, b, d) : c, f.mask = c, f.options = d, f.remove = function() {
            var a = e.getCaret();
            return e.destroyEvents(), e.val(f.getCleanVal()), e.setCaret(a - e.getMCharsBeforeCount(a)), b
        }, f.getCleanVal = function() {
            return e.getMasked(!0)
        }, f.getMaskedVal = function(a) {
            return e.getMasked(!1, a)
        }, f.init = function(g) {
            if (g = g || !1, d = d || {}, f.clearIfNotMatch = a.jMaskGlobals.clearIfNotMatch, f.byPassKeys = a.jMaskGlobals.byPassKeys, f.translation = a.extend({}, a.jMaskGlobals.translation, d.translation), f = a.extend(!0, {}, f, d), h = e.getRegexMask(), g) e.events(), e.val(e.getMasked());
            else {
                d.placeholder && b.attr("placeholder", d.placeholder), b.data("mask") && b.attr("autocomplete", "off");
                for (var i = 0, j = !0; i < c.length; i++) {
                    var k = f.translation[c.charAt(i)];
                    if (k && k.recursive) {
                        j = !1;
                        break
                    }
                }
                j && b.attr("maxlength", c.length), e.destroyEvents(), e.events();
                var l = e.getCaret();
                e.val(e.getMasked()), e.setCaret(l + e.getMCharsBeforeCount(l, !0))
            }
        }, f.init(!b.is("input"))
    };
    a.maskWatchers = {};
    var c = function() {
            var c = a(this),
                e = {},
                f = "data-mask-",
                g = c.attr("data-mask");
            if (c.attr(f + "reverse") && (e.reverse = !0), c.attr(f + "clearifnotmatch") && (e.clearIfNotMatch = !0), "true" === c.attr(f + "selectonfocus") && (e.selectOnFocus = !0), d(c, g, e)) return c.data("mask", new b(this, g, e))
        },
        d = function(b, c, d) {
            d = d || {};
            var e = a(b).data("mask"),
                f = JSON.stringify,
                g = a(b).val() || a(b).text();
            try {
                return "function" == typeof c && (c = c(g)), "object" != typeof e || f(e.options) !== f(d) || e.mask !== c
            } catch (a) {}
        },
        e = function(a) {
            var c, b = document.createElement("div");
            return a = "on" + a, c = a in b, c || (b.setAttribute(a, "return;"), c = "function" == typeof b[a]), b = null, c
        };
    a.fn.mask = function(c, e) {
        e = e || {};
        var f = this.selector,
            g = a.jMaskGlobals,
            h = g.watchInterval,
            i = e.watchInputs || g.watchInputs,
            j = function() {
                if (d(this, c, e)) return a(this).data("mask", new b(this, c, e))
            };
        return a(this).each(j), f && "" !== f && i && (clearInterval(a.maskWatchers[f]), a.maskWatchers[f] = setInterval(function() {
            a(document).find(f).each(j)
        }, h)), this
    }, a.fn.masked = function(a) {
        return this.data("mask").getMaskedVal(a)
    }, a.fn.unmask = function() {
        return clearInterval(a.maskWatchers[this.selector]), delete a.maskWatchers[this.selector], this.each(function() {
            var b = a(this).data("mask");
            b && b.remove().removeData("mask")
        })
    }, a.fn.cleanVal = function() {
        return this.data("mask").getCleanVal()
    }, a.applyDataMask = function(b) {
        b = b || a.jMaskGlobals.maskElements;
        var d = b instanceof a ? b : a(b);
        d.filter(a.jMaskGlobals.dataMaskAttr).each(c)
    };
    var f = {
        maskElements: "input,td,span,div",
        dataMaskAttr: "*[data-mask]",
        dataMask: !0,
        watchInterval: 300,
        watchInputs: !0,
        useInput: e("input"),
        watchDataMask: !1,
        byPassKeys: [9, 16, 17, 18, 36, 37, 38, 39, 40, 91],
        translation: {
            0: {
                pattern: /\d/
            },
            9: {
                pattern: /\d/,
                optional: !0
            },
            "#": {
                pattern: /\d/,
                recursive: !0
            },
            A: {
                pattern: /[a-zA-Z0-9]/
            },
            S: {
                pattern: /[a-zA-Z]/
            }
        }
    };
    a.jMaskGlobals = a.jMaskGlobals || {}, f = a.jMaskGlobals = a.extend(!0, {}, f, a.jMaskGlobals), f.dataMask && a.applyDataMask(), setInterval(function() {
        a.jMaskGlobals.watchDataMask && a.applyDataMask()
    }, f.watchInterval)
}, window.jQuery, window.Zepto);

/*! Hammer.JS - v2.0.7 - 2016-04-22
 * http://hammerjs.github.io/
 *
 * Copyright (c) 2016 Jorik Tangelder;
 * Licensed under the MIT license */
! function(a, b, c, d) {
    "use strict";

    function e(a, b, c) {
        return setTimeout(j(a, c), b)
    }

    function f(a, b, c) {
        return Array.isArray(a) ? (g(a, c[b], c), !0) : !1
    }

    function g(a, b, c) {
        var e;
        if (a)
            if (a.forEach) a.forEach(b, c);
            else if (a.length !== d)
                for (e = 0; e < a.length;) b.call(c, a[e], e, a), e++;
            else
                for (e in a) a.hasOwnProperty(e) && b.call(c, a[e], e, a)
    }

    function h(b, c, d) {
        var e = "DEPRECATED METHOD: " + c + "\n" + d + " AT \n";
        return function() {
            var c = new Error("get-stack-trace"),
                d = c && c.stack ? c.stack.replace(/^[^\(]+?[\n$]/gm, "").replace(/^\s+at\s+/gm, "").replace(/^Object.<anonymous>\s*\(/gm, "{anonymous}()@") : "Unknown Stack Trace",
                f = a.console && (a.console.warn || a.console.log);
            return f && f.call(a.console, e, d), b.apply(this, arguments)
        }
    }

    function i(a, b, c) {
        var d, e = b.prototype;
        d = a.prototype = Object.create(e), d.constructor = a, d._super = e, c && la(d, c)
    }

    function j(a, b) {
        return function() {
            return a.apply(b, arguments)
        }
    }

    function k(a, b) {
        return typeof a == oa ? a.apply(b ? b[0] || d : d, b) : a
    }

    function l(a, b) {
        return a === d ? b : a
    }

    function m(a, b, c) {
        g(q(b), function(b) {
            a.addEventListener(b, c, !1)
        })
    }

    function n(a, b, c) {
        g(q(b), function(b) {
            a.removeEventListener(b, c, !1)
        })
    }

    function o(a, b) {
        for (; a;) {
            if (a == b) return !0;
            a = a.parentNode
        }
        return !1
    }

    function p(a, b) {
        return a.indexOf(b) > -1
    }

    function q(a) {
        return a.trim().split(/\s+/g)
    }

    function r(a, b, c) {
        if (a.indexOf && !c) return a.indexOf(b);
        for (var d = 0; d < a.length;) {
            if (c && a[d][c] == b || !c && a[d] === b) return d;
            d++
        }
        return -1
    }

    function s(a) {
        return Array.prototype.slice.call(a, 0)
    }

    function t(a, b, c) {
        for (var d = [], e = [], f = 0; f < a.length;) {
            var g = b ? a[f][b] : a[f];
            r(e, g) < 0 && d.push(a[f]), e[f] = g, f++
        }
        return c && (d = b ? d.sort(function(a, c) {
                return a[b] > c[b]
            }) : d.sort()), d
    }

    function u(a, b) {
        for (var c, e, f = b[0].toUpperCase() + b.slice(1), g = 0; g < ma.length;) {
            if (c = ma[g], e = c ? c + f : b, e in a) return e;
            g++
        }
        return d
    }

    function v() {
        return ua++
    }

    function w(b) {
        var c = b.ownerDocument || b;
        return c.defaultView || c.parentWindow || a
    }

    function x(a, b) {
        var c = this;
        this.manager = a, this.callback = b, this.element = a.element, this.target = a.options.inputTarget, this.domHandler = function(b) {
            k(a.options.enable, [a]) && c.handler(b)
        }, this.init()
    }

    function y(a) {
        var b, c = a.options.inputClass;
        return new(b = c ? c : xa ? M : ya ? P : wa ? R : L)(a, z)
    }

    function z(a, b, c) {
        var d = c.pointers.length,
            e = c.changedPointers.length,
            f = b & Ea && d - e === 0,
            g = b & (Ga | Ha) && d - e === 0;
        c.isFirst = !!f, c.isFinal = !!g, f && (a.session = {}), c.eventType = b, A(a, c), a.emit("hammer.input", c), a.recognize(c), a.session.prevInput = c
    }

    function A(a, b) {
        var c = a.session,
            d = b.pointers,
            e = d.length;
        c.firstInput || (c.firstInput = D(b)), e > 1 && !c.firstMultiple ? c.firstMultiple = D(b) : 1 === e && (c.firstMultiple = !1);
        var f = c.firstInput,
            g = c.firstMultiple,
            h = g ? g.center : f.center,
            i = b.center = E(d);
        b.timeStamp = ra(), b.deltaTime = b.timeStamp - f.timeStamp, b.angle = I(h, i), b.distance = H(h, i), B(c, b), b.offsetDirection = G(b.deltaX, b.deltaY);
        var j = F(b.deltaTime, b.deltaX, b.deltaY);
        b.overallVelocityX = j.x, b.overallVelocityY = j.y, b.overallVelocity = qa(j.x) > qa(j.y) ? j.x : j.y, b.scale = g ? K(g.pointers, d) : 1, b.rotation = g ? J(g.pointers, d) : 0, b.maxPointers = c.prevInput ? b.pointers.length > c.prevInput.maxPointers ? b.pointers.length : c.prevInput.maxPointers : b.pointers.length, C(c, b);
        var k = a.element;
        o(b.srcEvent.target, k) && (k = b.srcEvent.target), b.target = k
    }

    function B(a, b) {
        var c = b.center,
            d = a.offsetDelta || {},
            e = a.prevDelta || {},
            f = a.prevInput || {};
        b.eventType !== Ea && f.eventType !== Ga || (e = a.prevDelta = {
            x: f.deltaX || 0,
            y: f.deltaY || 0
        }, d = a.offsetDelta = {
            x: c.x,
            y: c.y
        }), b.deltaX = e.x + (c.x - d.x), b.deltaY = e.y + (c.y - d.y)
    }

    function C(a, b) {
        var c, e, f, g, h = a.lastInterval || b,
            i = b.timeStamp - h.timeStamp;
        if (b.eventType != Ha && (i > Da || h.velocity === d)) {
            var j = b.deltaX - h.deltaX,
                k = b.deltaY - h.deltaY,
                l = F(i, j, k);
            e = l.x, f = l.y, c = qa(l.x) > qa(l.y) ? l.x : l.y, g = G(j, k), a.lastInterval = b
        } else c = h.velocity, e = h.velocityX, f = h.velocityY, g = h.direction;
        b.velocity = c, b.velocityX = e, b.velocityY = f, b.direction = g
    }

    function D(a) {
        for (var b = [], c = 0; c < a.pointers.length;) b[c] = {
            clientX: pa(a.pointers[c].clientX),
            clientY: pa(a.pointers[c].clientY)
        }, c++;
        return {
            timeStamp: ra(),
            pointers: b,
            center: E(b),
            deltaX: a.deltaX,
            deltaY: a.deltaY
        }
    }

    function E(a) {
        var b = a.length;
        if (1 === b) return {
            x: pa(a[0].clientX),
            y: pa(a[0].clientY)
        };
        for (var c = 0, d = 0, e = 0; b > e;) c += a[e].clientX, d += a[e].clientY, e++;
        return {
            x: pa(c / b),
            y: pa(d / b)
        }
    }

    function F(a, b, c) {
        return {
            x: b / a || 0,
            y: c / a || 0
        }
    }

    function G(a, b) {
        return a === b ? Ia : qa(a) >= qa(b) ? 0 > a ? Ja : Ka : 0 > b ? La : Ma
    }

    function H(a, b, c) {
        c || (c = Qa);
        var d = b[c[0]] - a[c[0]],
            e = b[c[1]] - a[c[1]];
        return Math.sqrt(d * d + e * e)
    }

    function I(a, b, c) {
        c || (c = Qa);
        var d = b[c[0]] - a[c[0]],
            e = b[c[1]] - a[c[1]];
        return 180 * Math.atan2(e, d) / Math.PI
    }

    function J(a, b) {
        return I(b[1], b[0], Ra) + I(a[1], a[0], Ra)
    }

    function K(a, b) {
        return H(b[0], b[1], Ra) / H(a[0], a[1], Ra)
    }

    function L() {
        this.evEl = Ta, this.evWin = Ua, this.pressed = !1, x.apply(this, arguments)
    }

    function M() {
        this.evEl = Xa, this.evWin = Ya, x.apply(this, arguments), this.store = this.manager.session.pointerEvents = []
    }

    function N() {
        this.evTarget = $a, this.evWin = _a, this.started = !1, x.apply(this, arguments)
    }

    function O(a, b) {
        var c = s(a.touches),
            d = s(a.changedTouches);
        return b & (Ga | Ha) && (c = t(c.concat(d), "identifier", !0)), [c, d]
    }

    function P() {
        this.evTarget = bb, this.targetIds = {}, x.apply(this, arguments)
    }

    function Q(a, b) {
        var c = s(a.touches),
            d = this.targetIds;
        if (b & (Ea | Fa) && 1 === c.length) return d[c[0].identifier] = !0, [c, c];
        var e, f, g = s(a.changedTouches),
            h = [],
            i = this.target;
        if (f = c.filter(function(a) {
                return o(a.target, i)
            }), b === Ea)
            for (e = 0; e < f.length;) d[f[e].identifier] = !0, e++;
        for (e = 0; e < g.length;) d[g[e].identifier] && h.push(g[e]), b & (Ga | Ha) && delete d[g[e].identifier], e++;
        return h.length ? [t(f.concat(h), "identifier", !0), h] : void 0
    }

    function R() {
        x.apply(this, arguments);
        var a = j(this.handler, this);
        this.touch = new P(this.manager, a), this.mouse = new L(this.manager, a), this.primaryTouch = null, this.lastTouches = []
    }

    function S(a, b) {
        a & Ea ? (this.primaryTouch = b.changedPointers[0].identifier, T.call(this, b)) : a & (Ga | Ha) && T.call(this, b)
    }

    function T(a) {
        var b = a.changedPointers[0];
        if (b.identifier === this.primaryTouch) {
            var c = {
                x: b.clientX,
                y: b.clientY
            };
            this.lastTouches.push(c);
            var d = this.lastTouches,
                e = function() {
                    var a = d.indexOf(c);
                    a > -1 && d.splice(a, 1)
                };
            setTimeout(e, cb)
        }
    }

    function U(a) {
        for (var b = a.srcEvent.clientX, c = a.srcEvent.clientY, d = 0; d < this.lastTouches.length; d++) {
            var e = this.lastTouches[d],
                f = Math.abs(b - e.x),
                g = Math.abs(c - e.y);
            if (db >= f && db >= g) return !0
        }
        return !1
    }

    function V(a, b) {
        this.manager = a, this.set(b)
    }

    function W(a) {
        if (p(a, jb)) return jb;
        var b = p(a, kb),
            c = p(a, lb);
        return b && c ? jb : b || c ? b ? kb : lb : p(a, ib) ? ib : hb
    }

    function X() {
        if (!fb) return !1;
        var b = {},
            c = a.CSS && a.CSS.supports;
        return ["auto", "manipulation", "pan-y", "pan-x", "pan-x pan-y", "none"].forEach(function(d) {
            b[d] = c ? a.CSS.supports("touch-action", d) : !0
        }), b
    }

    function Y(a) {
        this.options = la({}, this.defaults, a || {}), this.id = v(), this.manager = null, this.options.enable = l(this.options.enable, !0), this.state = nb, this.simultaneous = {}, this.requireFail = []
    }

    function Z(a) {
        return a & sb ? "cancel" : a & qb ? "end" : a & pb ? "move" : a & ob ? "start" : ""
    }

    function $(a) {
        return a == Ma ? "down" : a == La ? "up" : a == Ja ? "left" : a == Ka ? "right" : ""
    }

    function _(a, b) {
        var c = b.manager;
        return c ? c.get(a) : a
    }

    function aa() {
        Y.apply(this, arguments)
    }

    function ba() {
        aa.apply(this, arguments), this.pX = null, this.pY = null
    }

    function ca() {
        aa.apply(this, arguments)
    }

    function da() {
        Y.apply(this, arguments), this._timer = null, this._input = null
    }

    function ea() {
        aa.apply(this, arguments)
    }

    function fa() {
        aa.apply(this, arguments)
    }

    function ga() {
        Y.apply(this, arguments), this.pTime = !1, this.pCenter = !1, this._timer = null, this._input = null, this.count = 0
    }

    function ha(a, b) {
        return b = b || {}, b.recognizers = l(b.recognizers, ha.defaults.preset), new ia(a, b)
    }

    function ia(a, b) {
        this.options = la({}, ha.defaults, b || {}), this.options.inputTarget = this.options.inputTarget || a, this.handlers = {}, this.session = {}, this.recognizers = [], this.oldCssProps = {}, this.element = a, this.input = y(this), this.touchAction = new V(this, this.options.touchAction), ja(this, !0), g(this.options.recognizers, function(a) {
            var b = this.add(new a[0](a[1]));
            a[2] && b.recognizeWith(a[2]), a[3] && b.requireFailure(a[3])
        }, this)
    }

    function ja(a, b) {
        var c = a.element;
        if (c.style) {
            var d;
            g(a.options.cssProps, function(e, f) {
                d = u(c.style, f), b ? (a.oldCssProps[d] = c.style[d], c.style[d] = e) : c.style[d] = a.oldCssProps[d] || ""
            }), b || (a.oldCssProps = {})
        }
    }

    function ka(a, c) {
        var d = b.createEvent("Event");
        d.initEvent(a, !0, !0), d.gesture = c, c.target.dispatchEvent(d)
    }
    var la, ma = ["", "webkit", "Moz", "MS", "ms", "o"],
        na = b.createElement("div"),
        oa = "function",
        pa = Math.round,
        qa = Math.abs,
        ra = Date.now;
    la = "function" != typeof Object.assign ? function(a) {
            if (a === d || null === a) throw new TypeError("Cannot convert undefined or null to object");
            for (var b = Object(a), c = 1; c < arguments.length; c++) {
                var e = arguments[c];
                if (e !== d && null !== e)
                    for (var f in e) e.hasOwnProperty(f) && (b[f] = e[f])
            }
            return b
        } : Object.assign;
    var sa = h(function(a, b, c) {
            for (var e = Object.keys(b), f = 0; f < e.length;)(!c || c && a[e[f]] === d) && (a[e[f]] = b[e[f]]), f++;
            return a
        }, "extend", "Use `assign`."),
        ta = h(function(a, b) {
            return sa(a, b, !0)
        }, "merge", "Use `assign`."),
        ua = 1,
        va = /mobile|tablet|ip(ad|hone|od)|android/i,
        wa = "ontouchstart" in a,
        xa = u(a, "PointerEvent") !== d,
        ya = wa && va.test(navigator.userAgent),
        za = "touch",
        Aa = "pen",
        Ba = "mouse",
        Ca = "kinect",
        Da = 25,
        Ea = 1,
        Fa = 2,
        Ga = 4,
        Ha = 8,
        Ia = 1,
        Ja = 2,
        Ka = 4,
        La = 8,
        Ma = 16,
        Na = Ja | Ka,
        Oa = La | Ma,
        Pa = Na | Oa,
        Qa = ["x", "y"],
        Ra = ["clientX", "clientY"];
    x.prototype = {
        handler: function() {},
        init: function() {
            this.evEl && m(this.element, this.evEl, this.domHandler), this.evTarget && m(this.target, this.evTarget, this.domHandler), this.evWin && m(w(this.element), this.evWin, this.domHandler)
        },
        destroy: function() {
            this.evEl && n(this.element, this.evEl, this.domHandler), this.evTarget && n(this.target, this.evTarget, this.domHandler), this.evWin && n(w(this.element), this.evWin, this.domHandler)
        }
    };
    var Sa = {
            mousedown: Ea,
            mousemove: Fa,
            mouseup: Ga
        },
        Ta = "mousedown",
        Ua = "mousemove mouseup";
    i(L, x, {
        handler: function(a) {
            var b = Sa[a.type];
            b & Ea && 0 === a.button && (this.pressed = !0), b & Fa && 1 !== a.which && (b = Ga), this.pressed && (b & Ga && (this.pressed = !1), this.callback(this.manager, b, {
                pointers: [a],
                changedPointers: [a],
                pointerType: Ba,
                srcEvent: a
            }))
        }
    });
    var Va = {
            pointerdown: Ea,
            pointermove: Fa,
            pointerup: Ga,
            pointercancel: Ha,
            pointerout: Ha
        },
        Wa = {
            2: za,
            3: Aa,
            4: Ba,
            5: Ca
        },
        Xa = "pointerdown",
        Ya = "pointermove pointerup pointercancel";
    a.MSPointerEvent && !a.PointerEvent && (Xa = "MSPointerDown", Ya = "MSPointerMove MSPointerUp MSPointerCancel"), i(M, x, {
        handler: function(a) {
            var b = this.store,
                c = !1,
                d = a.type.toLowerCase().replace("ms", ""),
                e = Va[d],
                f = Wa[a.pointerType] || a.pointerType,
                g = f == za,
                h = r(b, a.pointerId, "pointerId");
            e & Ea && (0 === a.button || g) ? 0 > h && (b.push(a), h = b.length - 1) : e & (Ga | Ha) && (c = !0), 0 > h || (b[h] = a, this.callback(this.manager, e, {
                pointers: b,
                changedPointers: [a],
                pointerType: f,
                srcEvent: a
            }), c && b.splice(h, 1))
        }
    });
    var Za = {
            touchstart: Ea,
            touchmove: Fa,
            touchend: Ga,
            touchcancel: Ha
        },
        $a = "touchstart",
        _a = "touchstart touchmove touchend touchcancel";
    i(N, x, {
        handler: function(a) {
            var b = Za[a.type];
            if (b === Ea && (this.started = !0), this.started) {
                var c = O.call(this, a, b);
                b & (Ga | Ha) && c[0].length - c[1].length === 0 && (this.started = !1), this.callback(this.manager, b, {
                    pointers: c[0],
                    changedPointers: c[1],
                    pointerType: za,
                    srcEvent: a
                })
            }
        }
    });
    var ab = {
            touchstart: Ea,
            touchmove: Fa,
            touchend: Ga,
            touchcancel: Ha
        },
        bb = "touchstart touchmove touchend touchcancel";
    i(P, x, {
        handler: function(a) {
            var b = ab[a.type],
                c = Q.call(this, a, b);
            c && this.callback(this.manager, b, {
                pointers: c[0],
                changedPointers: c[1],
                pointerType: za,
                srcEvent: a
            })
        }
    });
    var cb = 2500,
        db = 25;
    i(R, x, {
        handler: function(a, b, c) {
            var d = c.pointerType == za,
                e = c.pointerType == Ba;
            if (!(e && c.sourceCapabilities && c.sourceCapabilities.firesTouchEvents)) {
                if (d) S.call(this, b, c);
                else if (e && U.call(this, c)) return;
                this.callback(a, b, c)
            }
        },
        destroy: function() {
            this.touch.destroy(), this.mouse.destroy()
        }
    });
    var eb = u(na.style, "touchAction"),
        fb = eb !== d,
        gb = "compute",
        hb = "auto",
        ib = "manipulation",
        jb = "none",
        kb = "pan-x",
        lb = "pan-y",
        mb = X();
    V.prototype = {
        set: function(a) {
            a == gb && (a = this.compute()), fb && this.manager.element.style && mb[a] && (this.manager.element.style[eb] = a), this.actions = a.toLowerCase().trim()
        },
        update: function() {
            this.set(this.manager.options.touchAction)
        },
        compute: function() {
            var a = [];
            return g(this.manager.recognizers, function(b) {
                k(b.options.enable, [b]) && (a = a.concat(b.getTouchAction()))
            }), W(a.join(" "))
        },
        preventDefaults: function(a) {
            var b = a.srcEvent,
                c = a.offsetDirection;
            if (this.manager.session.prevented) return void b.preventDefault();
            var d = this.actions,
                e = p(d, jb) && !mb[jb],
                f = p(d, lb) && !mb[lb],
                g = p(d, kb) && !mb[kb];
            if (e) {
                var h = 1 === a.pointers.length,
                    i = a.distance < 2,
                    j = a.deltaTime < 250;
                if (h && i && j) return
            }
            return g && f ? void 0 : e || f && c & Na || g && c & Oa ? this.preventSrc(b) : void 0
        },
        preventSrc: function(a) {
            this.manager.session.prevented = !0, a.preventDefault()
        }
    };
    var nb = 1,
        ob = 2,
        pb = 4,
        qb = 8,
        rb = qb,
        sb = 16,
        tb = 32;
    Y.prototype = {
        defaults: {},
        set: function(a) {
            return la(this.options, a), this.manager && this.manager.touchAction.update(), this
        },
        recognizeWith: function(a) {
            if (f(a, "recognizeWith", this)) return this;
            var b = this.simultaneous;
            return a = _(a, this), b[a.id] || (b[a.id] = a, a.recognizeWith(this)), this
        },
        dropRecognizeWith: function(a) {
            return f(a, "dropRecognizeWith", this) ? this : (a = _(a, this), delete this.simultaneous[a.id], this)
        },
        requireFailure: function(a) {
            if (f(a, "requireFailure", this)) return this;
            var b = this.requireFail;
            return a = _(a, this), -1 === r(b, a) && (b.push(a), a.requireFailure(this)), this
        },
        dropRequireFailure: function(a) {
            if (f(a, "dropRequireFailure", this)) return this;
            a = _(a, this);
            var b = r(this.requireFail, a);
            return b > -1 && this.requireFail.splice(b, 1), this
        },
        hasRequireFailures: function() {
            return this.requireFail.length > 0
        },
        canRecognizeWith: function(a) {
            return !!this.simultaneous[a.id]
        },
        emit: function(a) {
            function b(b) {
                c.manager.emit(b, a)
            }
            var c = this,
                d = this.state;
            qb > d && b(c.options.event + Z(d)), b(c.options.event), a.additionalEvent && b(a.additionalEvent), d >= qb && b(c.options.event + Z(d))
        },
        tryEmit: function(a) {
            return this.canEmit() ? this.emit(a) : void(this.state = tb)
        },
        canEmit: function() {
            for (var a = 0; a < this.requireFail.length;) {
                if (!(this.requireFail[a].state & (tb | nb))) return !1;
                a++
            }
            return !0
        },
        recognize: function(a) {
            var b = la({}, a);
            return k(this.options.enable, [this, b]) ? (this.state & (rb | sb | tb) && (this.state = nb), this.state = this.process(b), void(this.state & (ob | pb | qb | sb) && this.tryEmit(b))) : (this.reset(), void(this.state = tb))
        },
        process: function(a) {},
        getTouchAction: function() {},
        reset: function() {}
    }, i(aa, Y, {
        defaults: {
            pointers: 1
        },
        attrTest: function(a) {
            var b = this.options.pointers;
            return 0 === b || a.pointers.length === b
        },
        process: function(a) {
            var b = this.state,
                c = a.eventType,
                d = b & (ob | pb),
                e = this.attrTest(a);
            return d && (c & Ha || !e) ? b | sb : d || e ? c & Ga ? b | qb : b & ob ? b | pb : ob : tb
        }
    }), i(ba, aa, {
        defaults: {
            event: "pan",
            threshold: 10,
            pointers: 1,
            direction: Pa
        },
        getTouchAction: function() {
            var a = this.options.direction,
                b = [];
            return a & Na && b.push(lb), a & Oa && b.push(kb), b
        },
        directionTest: function(a) {
            var b = this.options,
                c = !0,
                d = a.distance,
                e = a.direction,
                f = a.deltaX,
                g = a.deltaY;
            return e & b.direction || (b.direction & Na ? (e = 0 === f ? Ia : 0 > f ? Ja : Ka, c = f != this.pX, d = Math.abs(a.deltaX)) : (e = 0 === g ? Ia : 0 > g ? La : Ma, c = g != this.pY, d = Math.abs(a.deltaY))), a.direction = e, c && d > b.threshold && e & b.direction
        },
        attrTest: function(a) {
            return aa.prototype.attrTest.call(this, a) && (this.state & ob || !(this.state & ob) && this.directionTest(a))
        },
        emit: function(a) {
            this.pX = a.deltaX, this.pY = a.deltaY;
            var b = jQuery(a.direction);
            b && (a.additionalEvent = this.options.event + b), this._super.emit.call(this, a)
        }
    }), i(ca, aa, {
        defaults: {
            event: "pinch",
            threshold: 0,
            pointers: 2
        },
        getTouchAction: function() {
            return [jb]
        },
        attrTest: function(a) {
            return this._super.attrTest.call(this, a) && (Math.abs(a.scale - 1) > this.options.threshold || this.state & ob)
        },
        emit: function(a) {
            if (1 !== a.scale) {
                var b = a.scale < 1 ? "in" : "out";
                a.additionalEvent = this.options.event + b
            }
            this._super.emit.call(this, a)
        }
    }), i(da, Y, {
        defaults: {
            event: "press",
            pointers: 1,
            time: 251,
            threshold: 9
        },
        getTouchAction: function() {
            return [hb]
        },
        process: function(a) {
            var b = this.options,
                c = a.pointers.length === b.pointers,
                d = a.distance < b.threshold,
                f = a.deltaTime > b.time;
            if (this._input = a, !d || !c || a.eventType & (Ga | Ha) && !f) this.reset();
            else if (a.eventType & Ea) this.reset(), this._timer = e(function() {
                this.state = rb, this.tryEmit()
            }, b.time, this);
            else if (a.eventType & Ga) return rb;
            return tb
        },
        reset: function() {
            clearTimeout(this._timer)
        },
        emit: function(a) {
            this.state === rb && (a && a.eventType & Ga ? this.manager.emit(this.options.event + "up", a) : (this._input.timeStamp = ra(), this.manager.emit(this.options.event, this._input)))
        }
    }), i(ea, aa, {
        defaults: {
            event: "rotate",
            threshold: 0,
            pointers: 2
        },
        getTouchAction: function() {
            return [jb]
        },
        attrTest: function(a) {
            return this._super.attrTest.call(this, a) && (Math.abs(a.rotation) > this.options.threshold || this.state & ob)
        }
    }), i(fa, aa, {
        defaults: {
            event: "swipe",
            threshold: 10,
            velocity: .3,
            direction: Na | Oa,
            pointers: 1
        },
        getTouchAction: function() {
            return ba.prototype.getTouchAction.call(this)
        },
        attrTest: function(a) {
            var b, c = this.options.direction;
            return c & (Na | Oa) ? b = a.overallVelocity : c & Na ? b = a.overallVelocityX : c & Oa && (b = a.overallVelocityY), this._super.attrTest.call(this, a) && c & a.offsetDirection && a.distance > this.options.threshold && a.maxPointers == this.options.pointers && qa(b) > this.options.velocity && a.eventType & Ga
        },
        emit: function(a) {
            var b = jQuery(a.offsetDirection);
            b && this.manager.emit(this.options.event + b, a), this.manager.emit(this.options.event, a)
        }
    }), i(ga, Y, {
        defaults: {
            event: "tap",
            pointers: 1,
            taps: 1,
            interval: 300,
            time: 250,
            threshold: 9,
            posThreshold: 10
        },
        getTouchAction: function() {
            return [ib]
        },
        process: function(a) {
            var b = this.options,
                c = a.pointers.length === b.pointers,
                d = a.distance < b.threshold,
                f = a.deltaTime < b.time;
            if (this.reset(), a.eventType & Ea && 0 === this.count) return this.failTimeout();
            if (d && f && c) {
                if (a.eventType != Ga) return this.failTimeout();
                var g = this.pTime ? a.timeStamp - this.pTime < b.interval : !0,
                    h = !this.pCenter || H(this.pCenter, a.center) < b.posThreshold;
                this.pTime = a.timeStamp, this.pCenter = a.center, h && g ? this.count += 1 : this.count = 1, this._input = a;
                var i = this.count % b.taps;
                if (0 === i) return this.hasRequireFailures() ? (this._timer = e(function() {
                        this.state = rb, this.tryEmit()
                    }, b.interval, this), ob) : rb
            }
            return tb
        },
        failTimeout: function() {
            return this._timer = e(function() {
                this.state = tb
            }, this.options.interval, this), tb
        },
        reset: function() {
            clearTimeout(this._timer)
        },
        emit: function() {
            this.state == rb && (this._input.tapCount = this.count, this.manager.emit(this.options.event, this._input))
        }
    }), ha.VERSION = "2.0.7", ha.defaults = {
        domEvents: !1,
        touchAction: gb,
        enable: !0,
        inputTarget: null,
        inputClass: null,
        preset: [
            [ea, {
                enable: !1
            }],
            [ca, {
                enable: !1
            },
                ["rotate"]
            ],
            [fa, {
                direction: Na
            }],
            [ba, {
                direction: Na
            },
                ["swipe"]
            ],
            [ga],
            [ga, {
                event: "doubletap",
                taps: 2
            },
                ["tap"]
            ],
            [da]
        ],
        cssProps: {
            userSelect: "none",
            touchSelect: "none",
            touchCallout: "none",
            contentZooming: "none",
            userDrag: "none",
            tapHighlightColor: "rgba(0,0,0,0)"
        }
    };
    var ub = 1,
        vb = 2;
    ia.prototype = {
        set: function(a) {
            return la(this.options, a), a.touchAction && this.touchAction.update(), a.inputTarget && (this.input.destroy(), this.input.target = a.inputTarget, this.input.init()), this
        },
        stop: function(a) {
            this.session.stopped = a ? vb : ub
        },
        recognize: function(a) {
            var b = this.session;
            if (!b.stopped) {
                this.touchAction.preventDefaults(a);
                var c, d = this.recognizers,
                    e = b.curRecognizer;
                (!e || e && e.state & rb) && (e = b.curRecognizer = null);
                for (var f = 0; f < d.length;) c = d[f], b.stopped === vb || e && c != e && !c.canRecognizeWith(e) ? c.reset() : c.recognize(a), !e && c.state & (ob | pb | qb) && (e = b.curRecognizer = c), f++
            }
        },
        get: function(a) {
            if (a instanceof Y) return a;
            for (var b = this.recognizers, c = 0; c < b.length; c++)
                if (b[c].options.event == a) return b[c];
            return null
        },
        add: function(a) {
            if (f(a, "add", this)) return this;
            var b = this.get(a.options.event);
            return b && this.remove(b), this.recognizers.push(a), a.manager = this, this.touchAction.update(), a
        },
        remove: function(a) {
            if (f(a, "remove", this)) return this;
            if (a = this.get(a)) {
                var b = this.recognizers,
                    c = r(b, a); - 1 !== c && (b.splice(c, 1), this.touchAction.update())
            }
            return this
        },
        on: function(a, b) {
            if (a !== d && b !== d) {
                var c = this.handlers;
                return g(q(a), function(a) {
                    c[a] = c[a] || [], c[a].push(b)
                }), this
            }
        },
        off: function(a, b) {
            if (a !== d) {
                var c = this.handlers;
                return g(q(a), function(a) {
                    b ? c[a] && c[a].splice(r(c[a], b), 1) : delete c[a]
                }), this
            }
        },
        emit: function(a, b) {
            this.options.domEvents && ka(a, b);
            var c = this.handlers[a] && this.handlers[a].slice();
            if (c && c.length) {
                b.type = a, b.preventDefault = function() {
                    b.srcEvent.preventDefault()
                };
                for (var d = 0; d < c.length;) c[d](b), d++
            }
        },
        destroy: function() {
            this.element && ja(this, !1), this.handlers = {}, this.session = {}, this.input.destroy(), this.element = null
        }
    }, la(ha, {
        INPUT_START: Ea,
        INPUT_MOVE: Fa,
        INPUT_END: Ga,
        INPUT_CANCEL: Ha,
        STATE_POSSIBLE: nb,
        STATE_BEGAN: ob,
        STATE_CHANGED: pb,
        STATE_ENDED: qb,
        STATE_RECOGNIZED: rb,
        STATE_CANCELLED: sb,
        STATE_FAILED: tb,
        DIRECTION_NONE: Ia,
        DIRECTION_LEFT: Ja,
        DIRECTION_RIGHT: Ka,
        DIRECTION_UP: La,
        DIRECTION_DOWN: Ma,
        DIRECTION_HORIZONTAL: Na,
        DIRECTION_VERTICAL: Oa,
        DIRECTION_ALL: Pa,
        Manager: ia,
        Input: x,
        TouchAction: V,
        TouchInput: P,
        MouseInput: L,
        PointerEventInput: M,
        TouchMouseInput: R,
        SingleTouchInput: N,
        Recognizer: Y,
        AttrRecognizer: aa,
        Tap: ga,
        Pan: ba,
        Swipe: fa,
        Pinch: ca,
        Rotate: ea,
        Press: da,
        on: m,
        off: n,
        each: g,
        merge: ta,
        extend: sa,
        assign: la,
        inherit: i,
        bindFn: j,
        prefixed: u
    });
    var wb = "undefined" != typeof a ? a : "undefined" != typeof self ? self : {};
    wb.Hammer = ha, "function" == typeof define && define.amd ? define(function() {
            return ha
        }) : "undefined" != typeof module && module.exports ? module.exports = ha : a[c] = ha
}(window, document, "Hammer");
//# sourceMappingURL=hammer.min.js.map
/*
 * jquery-match-height 0.7.2 by @liabru
 * http://brm.io/jquery-match-height/
 * License MIT
 */
! function(t) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery"], t) : "undefined" != typeof module && module.exports ? module.exports = t(require("jquery")) : t(jQuery)
}(function(t) {
    var e = -1,
        o = -1,
        n = function(t) {
            return parseFloat(t) || 0
        },
        a = function(e) {
            var o = 1,
                a = t(e),
                i = null,
                r = [];
            return a.each(function() {
                var e = t(this),
                    a = e.offset().top - n(e.css("margin-top")),
                    s = r.length > 0 ? r[r.length - 1] : null;
                null === s ? r.push(e) : Math.floor(Math.abs(i - a)) <= o ? r[r.length - 1] = s.add(e) : r.push(e), i = a
            }), r
        },
        i = function(e) {
            var o = {
                byRow: !0,
                property: "height",
                target: null,
                remove: !1
            };
            return "object" == typeof e ? t.extend(o, e) : ("boolean" == typeof e ? o.byRow = e : "remove" === e && (o.remove = !0), o)
        },
        r = t.fn.matchHeight = function(e) {
            var o = i(e);
            if (o.remove) {
                var n = this;
                return this.css(o.property, ""), t.each(r._groups, function(t, e) {
                    e.elements = e.elements.not(n)
                }), this
            }
            return this.length <= 1 && !o.target ? this : (r._groups.push({
                    elements: this,
                    options: o
                }), r._apply(this, o), this)
        };
    r.version = "0.7.2", r._groups = [], r._throttle = 80, r._maintainScroll = !1, r._beforeUpdate = null,
        r._afterUpdate = null, r._rows = a, r._parse = n, r._parseOptions = i, r._apply = function(e, o) {
        var s = i(o),
            h = t(e),
            l = [h],
            c = t(window).scrollTop(),
            p = t("html").outerHeight(!0),
            u = h.parents().filter(":hidden");
        return u.each(function() {
            var e = t(this);
            e.data("style-cache", e.attr("style"))
        }), u.css("display", "block"), s.byRow && !s.target && (h.each(function() {
            var e = t(this),
                o = e.css("display");
            "inline-block" !== o && "flex" !== o && "inline-flex" !== o && (o = "block"), e.data("style-cache", e.attr("style")), e.css({
                display: o,
                "padding-top": "0",
                "padding-bottom": "0",
                "margin-top": "0",
                "margin-bottom": "0",
                "border-top-width": "0",
                "border-bottom-width": "0",
                height: "100px",
                overflow: "hidden"
            })
        }), l = a(h), h.each(function() {
            var e = t(this);
            e.attr("style", e.data("style-cache") || "")
        })), t.each(l, function(e, o) {
            var a = t(o),
                i = 0;
            if (s.target) i = s.target.outerHeight(!1);
            else {
                if (s.byRow && a.length <= 1) return void a.css(s.property, "");
                a.each(function() {
                    var e = t(this),
                        o = e.attr("style"),
                        n = e.css("display");
                    "inline-block" !== n && "flex" !== n && "inline-flex" !== n && (n = "block");
                    var a = {
                        display: n
                    };
                    a[s.property] = "", e.css(a), e.outerHeight(!1) > i && (i = e.outerHeight(!1)), o ? e.attr("style", o) : e.css("display", "")
                })
            }
            a.each(function() {
                var e = t(this),
                    o = 0;
                s.target && e.is(s.target) || ("border-box" !== e.css("box-sizing") && (o += n(e.css("border-top-width")) + n(e.css("border-bottom-width")), o += n(e.css("padding-top")) + n(e.css("padding-bottom"))), e.css(s.property, i - o + "px"))
            })
        }), u.each(function() {
            var e = t(this);
            e.attr("style", e.data("style-cache") || null)
        }), r._maintainScroll && t(window).scrollTop(c / p * t("html").outerHeight(!0)),
            this
    }, r._applyDataApi = function() {
        var e = {};
        t("[data-match-height], [data-mh]").each(function() {
            var o = t(this),
                n = o.attr("data-mh") || o.attr("data-match-height");
            n in e ? e[n] = e[n].add(o) : e[n] = o
        }), t.each(e, function() {
            this.matchHeight(!0)
        })
    };
    var s = function(e) {
        r._beforeUpdate && r._beforeUpdate(e, r._groups), t.each(r._groups, function() {
            r._apply(this.elements, this.options)
        }), r._afterUpdate && r._afterUpdate(e, r._groups)
    };
    r._update = function(n, a) {
        if (a && "resize" === a.type) {
            var i = t(window).width();
            if (i === e) return;
            e = i;
        }
        n ? o === -1 && (o = setTimeout(function() {
                s(a), o = -1
            }, r._throttle)) : s(a)
    }, t(r._applyDataApi);
    var h = t.fn.on ? "on" : "bind";
    t(window)[h]("load", function(t) {
        r._update(!1, t)
    }), t(window)[h]("resize orientationchange", function(t) {
        r._update(!0, t)
    })
});
/*! WOW - v1.1.3 - 2016-05-06
 * Copyright (c) 2016 Matthieu Aussaguel;*/
(function() {
    var a, b, c, d, e, f = function(a, b) {
            return function() {
                return a.apply(b, arguments)
            }
        },
        g = [].indexOf || function(a) {
                for (var b = 0, c = this.length; c > b; b++)
                    if (b in this && this[b] === a) return b;
                return -1
            };
    b = function() {
        function a() {}
        return a.prototype.extend = function(a, b) {
            var c, d;
            for (c in b) d = b[c], null == a[c] && (a[c] = d);
            return a
        }, a.prototype.isMobile = function(a) {
            return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(a)
        }, a.prototype.createEvent = function(a, b, c, d) {
            var e;
            return null == b && (b = !1), null == c && (c = !1), null == d && (d = null), null != document.createEvent ? (e = document.createEvent("CustomEvent"), e.initCustomEvent(a, b, c, d)) : null != document.createEventObject ? (e = document.createEventObject(), e.eventType = a) : e.eventName = a, e
        }, a.prototype.emitEvent = function(a, b) {
            return null != a.dispatchEvent ? a.dispatchEvent(b) : b in (null != a) ? a[b]() : "on" + b in (null != a) ? a["on" + b]() : void 0
        }, a.prototype.addEvent = function(a, b, c) {
            return null != a.addEventListener ? a.addEventListener(b, c, !1) : null != a.attachEvent ? a.attachEvent("on" + b, c) : a[b] = c
        }, a.prototype.removeEvent = function(a, b, c) {
            return null != a.removeEventListener ? a.removeEventListener(b, c, !1) : null != a.detachEvent ? a.detachEvent("on" + b, c) : delete a[b]
        }, a.prototype.innerHeight = function() {
            return "innerHeight" in window ? window.innerHeight : document.documentElement.clientHeight
        }, a
    }(), c = this.WeakMap || this.MozWeakMap || (c = function() {
            function a() {
                this.keys = [], this.values = []
            }
            return a.prototype.get = function(a) {
                var b, c, d, e, f;
                for (f = this.keys, b = d = 0, e = f.length; e > d; b = ++d)
                    if (c = f[b], c === a) return this.values[b]
            }, a.prototype.set = function(a, b) {
                var c, d, e, f, g;
                for (g = this.keys, c = e = 0, f = g.length; f > e; c = ++e)
                    if (d = g[c], d === a) return void(this.values[c] = b);
                return this.keys.push(a), this.values.push(b)
            }, a
        }()), a = this.MutationObserver || this.WebkitMutationObserver || this.MozMutationObserver || (a = function() {
            function a() {
                "undefined" != typeof console && null !== console && console.warn("MutationObserver is not supported by your browser."), "undefined" != typeof console && null !== console && console.warn("WOW.js cannot detect dom mutations, please call .sync() after loading new content.")
            }
            return a.notSupported = !0, a.prototype.observe = function() {}, a
        }()), d = this.getComputedStyle || function(a, b) {
            return this.getPropertyValue = function(b) {
                var c;
                return "float" === b && (b = "styleFloat"), e.test(b) && b.replace(e, function(a, b) {
                    return b.toUpperCase()
                }), (null != (c = a.currentStyle) ? c[b] : void 0) || null
            }, this
        }, e = /(\-([a-z]){1})/g, this.WOW = function() {
        function e(a) {
            null == a && (a = {}), this.scrollCallback = f(this.scrollCallback, this), this.scrollHandler = f(this.scrollHandler, this), this.resetAnimation = f(this.resetAnimation, this), this.start = f(this.start, this), this.scrolled = !0, this.config = this.util().extend(a, this.defaults), null != a.scrollContainer && (this.config.scrollContainer = document.querySelector(a.scrollContainer)), this.animationNameCache = new c, this.wowEvent = this.util().createEvent(this.config.boxClass)
        }
        return e.prototype.defaults = {
            boxClass: "wow",
            animateClass: "animated",
            offset: 0,
            mobile: !0,
            live: !0,
            callback: null,
            scrollContainer: null
        }, e.prototype.init = function() {
            var a;
            return this.element = window.document.documentElement, "interactive" === (a = document.readyState) || "complete" === a ? this.start() : this.util().addEvent(document, "DOMContentLoaded", this.start), this.finished = []
        }, e.prototype.start = function() {
            var b, c, d, e;
            if (this.stopped = !1, this.boxes = function() {
                    var a, c, d, e;
                    for (d = this.element.querySelectorAll("." + this.config.boxClass), e = [], a = 0, c = d.length; c > a; a++) b = d[a], e.push(b);
                    return e
                }.call(this), this.all = function() {
                    var a, c, d, e;
                    for (d = this.boxes, e = [], a = 0, c = d.length; c > a; a++) b = d[a], e.push(b);
                    return e
                }.call(this), this.boxes.length)
                if (this.disabled()) this.resetStyle();
                else
                    for (e = this.boxes, c = 0, d = e.length; d > c; c++) b = e[c], this.applyStyle(b, !0);
            return this.disabled() || (this.util().addEvent(this.config.scrollContainer || window, "scroll", this.scrollHandler), this.util().addEvent(window, "resize", this.scrollHandler), this.interval = setInterval(this.scrollCallback, 50)), this.config.live ? new a(function(a) {
                    return function(b) {
                        var c, d, e, f, g;
                        for (g = [], c = 0, d = b.length; d > c; c++) f = b[c], g.push(function() {
                            var a, b, c, d;
                            for (c = f.addedNodes || [], d = [], a = 0, b = c.length; b > a; a++) e = c[a], d.push(this.doSync(e));
                            return d
                        }.call(a));
                        return g
                    }
                }(this)).observe(document.body, {
                    childList: !0,
                    subtree: !0
                }) : void 0
        }, e.prototype.stop = function() {
            return this.stopped = !0, this.util().removeEvent(this.config.scrollContainer || window, "scroll", this.scrollHandler), this.util().removeEvent(window, "resize", this.scrollHandler), null != this.interval ? clearInterval(this.interval) : void 0
        }, e.prototype.sync = function(b) {
            return a.notSupported ? this.doSync(this.element) : void 0
        }, e.prototype.doSync = function(a) {
            var b, c, d, e, f;
            if (null == a && (a = this.element), 1 === a.nodeType) {
                for (a = a.parentNode || a, e = a.querySelectorAll("." + this.config.boxClass), f = [], c = 0, d = e.length; d > c; c++) b = e[c], g.call(this.all, b) < 0 ? (this.boxes.push(b), this.all.push(b), this.stopped || this.disabled() ? this.resetStyle() : this.applyStyle(b, !0), f.push(this.scrolled = !0)) : f.push(void 0);
                return f
            }
        }, e.prototype.show = function(a) {
            return this.applyStyle(a), a.className = a.className + " " + this.config.animateClass, null != this.config.callback && this.config.callback(a), this.util().emitEvent(a, this.wowEvent), this.util().addEvent(a, "animationend", this.resetAnimation), this.util().addEvent(a, "oanimationend", this.resetAnimation), this.util().addEvent(a, "webkitAnimationEnd", this.resetAnimation), this.util().addEvent(a, "MSAnimationEnd", this.resetAnimation), a
        }, e.prototype.applyStyle = function(a, b) {
            var c, d, e;
            return d = a.getAttribute("data-wow-duration"), c = a.getAttribute("data-wow-delay"), e = a.getAttribute("data-wow-iteration"), this.animate(function(f) {
                return function() {
                    return f.customStyle(a, b, d, c, e)
                }
            }(this))
        }, e.prototype.animate = function() {
            return "requestAnimationFrame" in window ? function(a) {
                    return window.requestAnimationFrame(a)
                } : function(a) {
                    return a()
                }
        }(), e.prototype.resetStyle = function() {
            var a, b, c, d, e;
            for (d = this.boxes, e = [], b = 0, c = d.length; c > b; b++) a = d[b], e.push(a.style.visibility = "visible");
            return e
        }, e.prototype.resetAnimation = function(a) {
            var b;
            return a.type.toLowerCase().indexOf("animationend") >= 0 ? (b = a.target || a.srcElement, b.className = b.className.replace(this.config.animateClass, "").trim()) : void 0
        }, e.prototype.customStyle = function(a, b, c, d, e) {
            return b && this.cacheAnimationName(a), a.style.visibility = b ? "hidden" : "visible", c && this.vendorSet(a.style, {
                animationDuration: c
            }), d && this.vendorSet(a.style, {
                animationDelay: d
            }), e && this.vendorSet(a.style, {
                animationIterationCount: e
            }), this.vendorSet(a.style, {
                animationName: b ? "none" : this.cachedAnimationName(a)
            }), a
        }, e.prototype.vendors = ["moz", "webkit"], e.prototype.vendorSet = function(a, b) {
            var c, d, e, f;
            d = [];
            for (c in b) e = b[c], a["" + c] = e, d.push(function() {
                var b, d, g, h;
                for (g = this.vendors, h = [], b = 0, d = g.length; d > b; b++) f = g[b], h.push(a["" + f + c.charAt(0).toUpperCase() + c.substr(1)] = e);
                return h
            }.call(this));
            return d
        }, e.prototype.vendorCSS = function(a, b) {
            var c, e, f, g, h, i;
            for (h = d(a), g = h.getPropertyCSSValue(b), f = this.vendors, c = 0, e = f.length; e > c; c++) i = f[c], g = g || h.getPropertyCSSValue("-" + i + "-" + b);
            return g
        }, e.prototype.animationName = function(a) {
            var b;
            try {
                b = this.vendorCSS(a, "animation-name").cssText
            } catch (c) {
                b = d(a).getPropertyValue("animation-name")
            }
            return "none" === b ? "" : b
        }, e.prototype.cacheAnimationName = function(a) {
            return this.animationNameCache.set(a, this.animationName(a))
        }, e.prototype.cachedAnimationName = function(a) {
            return this.animationNameCache.get(a)
        }, e.prototype.scrollHandler = function() {
            return this.scrolled = !0
        }, e.prototype.scrollCallback = function() {
            var a;
            return !this.scrolled || (this.scrolled = !1, this.boxes = function() {
                var b, c, d, e;
                for (d = this.boxes, e = [], b = 0, c = d.length; c > b; b++) a = d[b], a && (this.isVisible(a) ? this.show(a) : e.push(a));
                return e
            }.call(this), this.boxes.length || this.config.live) ? void 0 : this.stop()
        }, e.prototype.offsetTop = function(a) {
            for (var b; void 0 === a.offsetTop;) a = a.parentNode;
            for (b = a.offsetTop; a = a.offsetParent;) b += a.offsetTop;
            return b
        }, e.prototype.isVisible = function(a) {
            var b, c, d, e, f;
            return c = a.getAttribute("data-wow-offset") || this.config.offset, f = this.config.scrollContainer && this.config.scrollContainer.scrollTop || window.pageYOffset, e = f + Math.min(this.element.clientHeight, this.util().innerHeight()) - c, d = this.offsetTop(a), b = d + a.clientHeight, e >= d && b >= f
        }, e.prototype.util = function() {
            return null != this._util ? this._util : this._util = new b
        }, e.prototype.disabled = function() {
            return !this.config.mobile && this.util().isMobile(navigator.userAgent)
        }, e
    }()
}).call(this);
/*
 _ _      _       _
 ___| (_) ___| | __  (_)___
 / __| | |/ __| |/ /  | / __|
 \__ \ | | (__|   < _ | \__ \
 |___/_|_|\___|_|\_(_)/ |___/
 |__/

 Version: 1.6.0
 Author: Ken Wheeler
 Website: http://kenwheeler.github.io
 Docs: http://kenwheeler.github.io/slick
 Repo: http://github.com/kenwheeler/slick
 Issues: http://github.com/kenwheeler/slick/issues

 */
! function(a) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery"], a) : "undefined" != typeof exports ? module.exports = a(require("jquery")) : a(jQuery)
}(function(a) {
    "use strict";
    var b = window.Slick || {};
    b = function() {
        function c(c, d) {
            var f, e = this;
            e.defaults = {
                accessibility: !0,
                adaptiveHeight: !1,
                appendArrows: a(c),
                appendDots: a(c),
                arrows: !0,
                asNavFor: null,
                prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">Previous</button>',
                nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button">Next</button>',
                autoplay: !1,
                autoplaySpeed: 3e3,
                centerMode: !1,
                centerPadding: "50px",
                cssEase: "ease",
                customPaging: function(b, c) {
                    return a('<button type="button" data-role="none" role="button" tabindex="0" />').text(c + 1)
                },
                dots: !1,
                dotsClass: "slick-dots",
                draggable: !0,
                easing: "linear",
                edgeFriction: .35,
                fade: !1,
                focusOnSelect: !1,
                infinite: !0,
                initialSlide: 0,
                lazyLoad: "ondemand",
                mobileFirst: !1,
                pauseOnHover: !0,
                pauseOnFocus: !0,
                pauseOnDotsHover: !1,
                respondTo: "window",
                responsive: null,
                rows: 1,
                rtl: !1,
                slide: "",
                slidesPerRow: 1,
                slidesToShow: 1,
                slidesToScroll: 1,
                speed: 500,
                swipe: !0,
                swipeToSlide: !1,
                touchMove: !0,
                touchThreshold: 5,
                useCSS: !0,
                useTransform: !0,
                variableWidth: !1,
                vertical: !1,
                verticalSwiping: !1,
                waitForAnimate: !0,
                zIndex: 1e3
            }, e.initials = {
                animating: !1,
                dragging: !1,
                autoPlayTimer: null,
                currentDirection: 0,
                currentLeft: null,
                currentSlide: 0,
                direction: 1,
                $dots: null,
                listWidth: null,
                listHeight: null,
                loadIndex: 0,
                $nextArrow: null,
                $prevArrow: null,
                slideCount: null,
                slideWidth: null,
                $slideTrack: null,
                $slides: null,
                sliding: !1,
                slideOffset: 0,
                swipeLeft: null,
                $list: null,
                touchObject: {},
                transformsEnabled: !1,
                unslicked: !1
            }, a.extend(e, e.initials), e.activeBreakpoint = null, e.animType = null, e.animProp = null, e.breakpoints = [], e.breakpointSettings = [], e.cssTransitions = !1, e.focussed = !1, e.interrupted = !1, e.hidden = "hidden", e.paused = !0, e.positionProp = null, e.respondTo = null, e.rowCount = 1, e.shouldClick = !0, e.$slider = a(c), e.$slidesCache = null, e.transformType = null, e.transitionType = null, e.visibilityChange = "visibilitychange", e.windowWidth = 0, e.windowTimer = null, f = a(c).data("slick") || {}, e.options = a.extend({}, e.defaults, d, f), e.currentSlide = e.options.initialSlide, e.originalSettings = e.options, "undefined" != typeof document.mozHidden ? (e.hidden = "mozHidden", e.visibilityChange = "mozvisibilitychange") : "undefined" != typeof document.webkitHidden && (e.hidden = "webkitHidden", e.visibilityChange = "webkitvisibilitychange"), e.autoPlay = a.proxy(e.autoPlay, e), e.autoPlayClear = a.proxy(e.autoPlayClear, e), e.autoPlayIterator = a.proxy(e.autoPlayIterator, e), e.changeSlide = a.proxy(e.changeSlide, e), e.clickHandler = a.proxy(e.clickHandler, e), e.selectHandler = a.proxy(e.selectHandler, e), e.setPosition = a.proxy(e.setPosition, e), e.swipeHandler = a.proxy(e.swipeHandler, e), e.dragHandler = a.proxy(e.dragHandler, e), e.keyHandler = a.proxy(e.keyHandler, e), e.instanceUid = b++, e.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/, e.registerBreakpoints(), e.init(!0)
        }
        var b = 0;
        return c
    }(), b.prototype.activateADA = function() {
        var a = this;
        a.$slideTrack.find(".slick-active").attr({
            "aria-hidden": "false"
        }).find("a, input, button, select").attr({
            tabindex: "0"
        })
    }, b.prototype.addSlide = b.prototype.slickAdd = function(b, c, d) {
        var e = this;
        if ("boolean" == typeof c) d = c, c = null;
        else if (0 > c || c >= e.slideCount) return !1;
        e.unload(), "number" == typeof c ? 0 === c && 0 === e.$slides.length ? a(b).appendTo(e.$slideTrack) : d ? a(b).insertBefore(e.$slides.eq(c)) : a(b).insertAfter(e.$slides.eq(c)) : d === !0 ? a(b).prependTo(e.$slideTrack) : a(b).appendTo(e.$slideTrack), e.$slides = e.$slideTrack.children(this.options.slide), e.$slideTrack.children(this.options.slide).detach(), e.$slideTrack.append(e.$slides), e.$slides.each(function(b, c) {
            a(c).attr("data-slick-index", b)
        }), e.$slidesCache = e.$slides, e.reinit()
    }, b.prototype.animateHeight = function() {
        var a = this;
        if (1 === a.options.slidesToShow && a.options.adaptiveHeight === !0 && a.options.vertical === !1) {
            var b = a.$slides.eq(a.currentSlide).outerHeight(!0);
            a.$list.animate({
                height: b
            }, a.options.speed)
        }
    }, b.prototype.animateSlide = function(b, c) {
        var d = {},
            e = this;
        e.animateHeight(), e.options.rtl === !0 && e.options.vertical === !1 && (b = -b), e.transformsEnabled === !1 ? e.options.vertical === !1 ? e.$slideTrack.animate({
                    left: b
                }, e.options.speed, e.options.easing, c) : e.$slideTrack.animate({
                    top: b
                }, e.options.speed, e.options.easing, c) : e.cssTransitions === !1 ? (e.options.rtl === !0 && (e.currentLeft = -e.currentLeft), a({
                    animStart: e.currentLeft
                }).animate({
                    animStart: b
                }, {
                    duration: e.options.speed,
                    easing: e.options.easing,
                    step: function(a) {
                        a = Math.ceil(a), e.options.vertical === !1 ? (d[e.animType] = "translate(" + a + "px, 0px)", e.$slideTrack.css(d)) : (d[e.animType] = "translate(0px," + a + "px)", e.$slideTrack.css(d))
                    },
                    complete: function() {
                        c && c.call()
                    }
                })) : (e.applyTransition(), b = Math.ceil(b), e.options.vertical === !1 ? d[e.animType] = "translate3d(" + b + "px, 0px, 0px)" : d[e.animType] = "translate3d(0px," + b + "px, 0px)", e.$slideTrack.css(d), c && setTimeout(function() {
                    e.disableTransition(), c.call()
                }, e.options.speed))
    }, b.prototype.getNavTarget = function() {
        var b = this,
            c = b.options.asNavFor;
        return c && null !== c && (c = a(c).not(b.$slider)), c
    }, b.prototype.asNavFor = function(b) {
        var c = this,
            d = c.getNavTarget();
        null !== d && "object" == typeof d && d.each(function() {
            var c = a(this).slick("getSlick");
            c.unslicked || c.slideHandler(b, !0)
        })
    }, b.prototype.applyTransition = function(a) {
        var b = this,
            c = {};
        b.options.fade === !1 ? c[b.transitionType] = b.transformType + " " + b.options.speed + "ms " + b.options.cssEase : c[b.transitionType] = "opacity " + b.options.speed + "ms " + b.options.cssEase, b.options.fade === !1 ? b.$slideTrack.css(c) : b.$slides.eq(a).css(c)
    }, b.prototype.autoPlay = function() {
        var a = this;
        a.autoPlayClear(), a.slideCount > a.options.slidesToShow && (a.autoPlayTimer = setInterval(a.autoPlayIterator, a.options.autoplaySpeed))
    }, b.prototype.autoPlayClear = function() {
        var a = this;
        a.autoPlayTimer && clearInterval(a.autoPlayTimer)
    }, b.prototype.autoPlayIterator = function() {
        var a = this,
            b = a.currentSlide + a.options.slidesToScroll;
        a.paused || a.interrupted || a.focussed || (a.options.infinite === !1 && (1 === a.direction && a.currentSlide + 1 === a.slideCount - 1 ? a.direction = 0 : 0 === a.direction && (b = a.currentSlide - a.options.slidesToScroll, a.currentSlide - 1 === 0 && (a.direction = 1))), a.slideHandler(b))
    }, b.prototype.buildArrows = function() {
        var b = this;
        b.options.arrows === !0 && (b.$prevArrow = a(b.options.prevArrow).addClass("slick-arrow"), b.$nextArrow = a(b.options.nextArrow).addClass("slick-arrow"), b.slideCount > b.options.slidesToShow ? (b.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), b.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), b.htmlExpr.test(b.options.prevArrow) && b.$prevArrow.prependTo(b.options.appendArrows), b.htmlExpr.test(b.options.nextArrow) && b.$nextArrow.appendTo(b.options.appendArrows), b.options.infinite !== !0 && b.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true")) : b.$prevArrow.add(b.$nextArrow).addClass("slick-hidden").attr({
                "aria-disabled": "true",
                tabindex: "-1"
            }))
    }, b.prototype.buildDots = function() {
        var c, d, b = this;
        if (b.options.dots === !0 && b.slideCount > b.options.slidesToShow) {
            for (b.$slider.addClass("slick-dotted"), d = a("<ul />").addClass(b.options.dotsClass), c = 0; c <= b.getDotCount(); c += 1) d.append(a("<li />").append(b.options.customPaging.call(this, b, c)));
            b.$dots = d.appendTo(b.options.appendDots), b.$dots.find("li").first().addClass("slick-active").attr("aria-hidden", "false")
        }
    }, b.prototype.buildOut = function() {
        var b = this;
        b.$slides = b.$slider.children(b.options.slide + ":not(.slick-cloned)").addClass("slick-slide"), b.slideCount = b.$slides.length, b.$slides.each(function(b, c) {
            a(c).attr("data-slick-index", b).data("originalStyling", a(c).attr("style") || "")
        }), b.$slider.addClass("slick-slider"), b.$slideTrack = 0 === b.slideCount ? a('<div class="slick-track"/>').appendTo(b.$slider) : b.$slides.wrapAll('<div class="slick-track"/>').parent(), b.$list = b.$slideTrack.wrap('<div aria-live="polite" class="slick-list"/>').parent(), b.$slideTrack.css("opacity", 0), (b.options.centerMode === !0 || b.options.swipeToSlide === !0) && (b.options.slidesToScroll = 1), a("img[data-lazy]", b.$slider).not("[src]").addClass("slick-loading"), b.setupInfinite(), b.buildArrows(), b.buildDots(), b.updateDots(), b.setSlideClasses("number" == typeof b.currentSlide ? b.currentSlide : 0), b.options.draggable === !0 && b.$list.addClass("draggable")
    }, b.prototype.buildRows = function() {
        var b, c, d, e, f, g, h, a = this;
        if (e = document.createDocumentFragment(), g = a.$slider.children(), a.options.rows > 1) {
            for (h = a.options.slidesPerRow * a.options.rows, f = Math.ceil(g.length / h), b = 0; f > b; b++) {
                var i = document.createElement("div");
                for (c = 0; c < a.options.rows; c++) {
                    var j = document.createElement("div");
                    for (d = 0; d < a.options.slidesPerRow; d++) {
                        var k = b * h + (c * a.options.slidesPerRow + d);
                        g.get(k) && j.appendChild(g.get(k))
                    }
                    i.appendChild(j)
                }
                e.appendChild(i)
            }
            a.$slider.empty().append(e), a.$slider.children().children().children().css({
                width: 100 / a.options.slidesPerRow + "%",
                display: "inline-block"
            })
        }
    }, b.prototype.checkResponsive = function(b, c) {
        var e, f, g, d = this,
            h = !1,
            i = d.$slider.width(),
            j = window.innerWidth || a(window).width();
        if ("window" === d.respondTo ? g = j : "slider" === d.respondTo ? g = i : "min" === d.respondTo && (g = Math.min(j, i)), d.options.responsive && d.options.responsive.length && null !== d.options.responsive) {
            f = null;
            for (e in d.breakpoints) d.breakpoints.hasOwnProperty(e) && (d.originalSettings.mobileFirst === !1 ? g < d.breakpoints[e] && (f = d.breakpoints[e]) : g > d.breakpoints[e] && (f = d.breakpoints[e]));
            null !== f ? null !== d.activeBreakpoint ? (f !== d.activeBreakpoint || c) && (d.activeBreakpoint = f, "unslick" === d.breakpointSettings[f] ? d.unslick(f) : (d.options = a.extend({}, d.originalSettings, d.breakpointSettings[f]), b === !0 && (d.currentSlide = d.options.initialSlide), d.refresh(b)), h = f) : (d.activeBreakpoint = f, "unslick" === d.breakpointSettings[f] ? d.unslick(f) : (d.options = a.extend({}, d.originalSettings, d.breakpointSettings[f]), b === !0 && (d.currentSlide = d.options.initialSlide), d.refresh(b)), h = f) : null !== d.activeBreakpoint && (d.activeBreakpoint = null, d.options = d.originalSettings, b === !0 && (d.currentSlide = d.options.initialSlide), d.refresh(b), h = f), b || h === !1 || d.$slider.trigger("breakpoint", [d, h])
        }
    }, b.prototype.changeSlide = function(b, c) {
        var f, g, h, d = this,
            e = a(b.currentTarget);
        switch (e.is("a") && b.preventDefault(), e.is("li") || (e = e.closest("li")), h = d.slideCount % d.options.slidesToScroll !== 0, f = h ? 0 : (d.slideCount - d.currentSlide) % d.options.slidesToScroll, b.data.message) {
            case "previous":
                g = 0 === f ? d.options.slidesToScroll : d.options.slidesToShow - f, d.slideCount > d.options.slidesToShow && d.slideHandler(d.currentSlide - g, !1, c);
                break;
            case "next":
                g = 0 === f ? d.options.slidesToScroll : f, d.slideCount > d.options.slidesToShow && d.slideHandler(d.currentSlide + g, !1, c);
                break;
            case "index":
                var i = 0 === b.data.index ? 0 : b.data.index || e.index() * d.options.slidesToScroll;
                d.slideHandler(d.checkNavigable(i), !1, c), e.children().trigger("focus");
                break;
            default:
                return
        }
    }, b.prototype.checkNavigable = function(a) {
        var c, d, b = this;
        if (c = b.getNavigableIndexes(), d = 0, a > c[c.length - 1]) a = c[c.length - 1];
        else
            for (var e in c) {
                if (a < c[e]) {
                    a = d;
                    break
                }
                d = c[e]
            }
        return a
    }, b.prototype.cleanUpEvents = function() {
        var b = this;
        b.options.dots && null !== b.$dots && a("li", b.$dots).off("click.slick", b.changeSlide).off("mouseenter.slick", a.proxy(b.interrupt, b, !0)).off("mouseleave.slick", a.proxy(b.interrupt, b, !1)), b.$slider.off("focus.slick blur.slick"), b.options.arrows === !0 && b.slideCount > b.options.slidesToShow && (b.$prevArrow && b.$prevArrow.off("click.slick", b.changeSlide), b.$nextArrow && b.$nextArrow.off("click.slick", b.changeSlide)), b.$list.off("touchstart.slick mousedown.slick", b.swipeHandler), b.$list.off("touchmove.slick mousemove.slick", b.swipeHandler), b.$list.off("touchend.slick mouseup.slick", b.swipeHandler), b.$list.off("touchcancel.slick mouseleave.slick", b.swipeHandler), b.$list.off("click.slick", b.clickHandler), a(document).off(b.visibilityChange, b.visibility), b.cleanUpSlideEvents(), b.options.accessibility === !0 && b.$list.off("keydown.slick", b.keyHandler), b.options.focusOnSelect === !0 && a(b.$slideTrack).children().off("click.slick", b.selectHandler), a(window).off("orientationchange.slick.slick-" + b.instanceUid, b.orientationChange), a(window).off("resize.slick.slick-" + b.instanceUid, b.resize), a("[draggable!=true]", b.$slideTrack).off("dragstart", b.preventDefault), a(window).off("load.slick.slick-" + b.instanceUid, b.setPosition), a(document).off("ready.slick.slick-" + b.instanceUid, b.setPosition)
    }, b.prototype.cleanUpSlideEvents = function() {
        var b = this;
        b.$list.off("mouseenter.slick", a.proxy(b.interrupt, b, !0)), b.$list.off("mouseleave.slick", a.proxy(b.interrupt, b, !1))
    }, b.prototype.cleanUpRows = function() {
        var b, a = this;
        a.options.rows > 1 && (b = a.$slides.children().children(), b.removeAttr("style"), a.$slider.empty().append(b))
    }, b.prototype.clickHandler = function(a) {
        var b = this;
        b.shouldClick === !1 && (a.stopImmediatePropagation(), a.stopPropagation(), a.preventDefault())
    }, b.prototype.destroy = function(b) {
        var c = this;
        c.autoPlayClear(), c.touchObject = {}, c.cleanUpEvents(), a(".slick-cloned", c.$slider).detach(), c.$dots && c.$dots.remove(), c.$prevArrow && c.$prevArrow.length && (c.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), c.htmlExpr.test(c.options.prevArrow) && c.$prevArrow.remove()), c.$nextArrow && c.$nextArrow.length && (c.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), c.htmlExpr.test(c.options.nextArrow) && c.$nextArrow.remove()), c.$slides && (c.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each(function() {
            a(this).attr("style", a(this).data("originalStyling"))
        }), c.$slideTrack.children(this.options.slide).detach(), c.$slideTrack.detach(), c.$list.detach(), c.$slider.append(c.$slides)), c.cleanUpRows(), c.$slider.removeClass("slick-slider"), c.$slider.removeClass("slick-initialized"), c.$slider.removeClass("slick-dotted"), c.unslicked = !0, b || c.$slider.trigger("destroy", [c])
    }, b.prototype.disableTransition = function(a) {
        var b = this,
            c = {};
        c[b.transitionType] = "", b.options.fade === !1 ? b.$slideTrack.css(c) : b.$slides.eq(a).css(c)
    }, b.prototype.fadeSlide = function(a, b) {
        var c = this;
        c.cssTransitions === !1 ? (c.$slides.eq(a).css({
                zIndex: c.options.zIndex
            }), c.$slides.eq(a).animate({
                opacity: 1
            }, c.options.speed, c.options.easing, b)) : (c.applyTransition(a), c.$slides.eq(a).css({
                opacity: 1,
                zIndex: c.options.zIndex
            }), b && setTimeout(function() {
                c.disableTransition(a), b.call()
            }, c.options.speed))
    }, b.prototype.fadeSlideOut = function(a) {
        var b = this;
        b.cssTransitions === !1 ? b.$slides.eq(a).animate({
                opacity: 0,
                zIndex: b.options.zIndex - 2
            }, b.options.speed, b.options.easing) : (b.applyTransition(a), b.$slides.eq(a).css({
                opacity: 0,
                zIndex: b.options.zIndex - 2
            }))
    }, b.prototype.filterSlides = b.prototype.slickFilter = function(a) {
        var b = this;
        null !== a && (b.$slidesCache = b.$slides, b.unload(), b.$slideTrack.children(this.options.slide).detach(), b.$slidesCache.filter(a).appendTo(b.$slideTrack), b.reinit())
    }, b.prototype.focusHandler = function() {
        var b = this;
        b.$slider.off("focus.slick blur.slick").on("focus.slick blur.slick", "*:not(.slick-arrow)", function(c) {
            c.stopImmediatePropagation();
            var d = a(this);
            setTimeout(function() {
                b.options.pauseOnFocus && (b.focussed = d.is(":focus"), b.autoPlay())
            }, 0)
        })
    }, b.prototype.getCurrent = b.prototype.slickCurrentSlide = function() {
        var a = this;
        return a.currentSlide
    }, b.prototype.getDotCount = function() {
        var a = this,
            b = 0,
            c = 0,
            d = 0;
        if (a.options.infinite === !0)
            for (; b < a.slideCount;) ++d, b = c + a.options.slidesToScroll, c += a.options.slidesToScroll <= a.options.slidesToShow ? a.options.slidesToScroll : a.options.slidesToShow;
        else if (a.options.centerMode === !0) d = a.slideCount;
        else if (a.options.asNavFor)
            for (; b < a.slideCount;) ++d, b = c + a.options.slidesToScroll, c += a.options.slidesToScroll <= a.options.slidesToShow ? a.options.slidesToScroll : a.options.slidesToShow;
        else d = 1 + Math.ceil((a.slideCount - a.options.slidesToShow) / a.options.slidesToScroll);
        return d - 1
    }, b.prototype.getLeft = function(a) {
        var c, d, f, b = this,
            e = 0;
        return b.slideOffset = 0, d = b.$slides.first().outerHeight(!0), b.options.infinite === !0 ? (b.slideCount > b.options.slidesToShow && (b.slideOffset = b.slideWidth * b.options.slidesToShow * -1, e = d * b.options.slidesToShow * -1), b.slideCount % b.options.slidesToScroll !== 0 && a + b.options.slidesToScroll > b.slideCount && b.slideCount > b.options.slidesToShow && (a > b.slideCount ? (b.slideOffset = (b.options.slidesToShow - (a - b.slideCount)) * b.slideWidth * -1, e = (b.options.slidesToShow - (a - b.slideCount)) * d * -1) : (b.slideOffset = b.slideCount % b.options.slidesToScroll * b.slideWidth * -1, e = b.slideCount % b.options.slidesToScroll * d * -1))) : a + b.options.slidesToShow > b.slideCount && (b.slideOffset = (a + b.options.slidesToShow - b.slideCount) * b.slideWidth, e = (a + b.options.slidesToShow - b.slideCount) * d), b.slideCount <= b.options.slidesToShow && (b.slideOffset = 0, e = 0), b.options.centerMode === !0 && b.options.infinite === !0 ? b.slideOffset += b.slideWidth * Math.floor(b.options.slidesToShow / 2) - b.slideWidth : b.options.centerMode === !0 && (b.slideOffset = 0, b.slideOffset += b.slideWidth * Math.floor(b.options.slidesToShow / 2)), c = b.options.vertical === !1 ? a * b.slideWidth * -1 + b.slideOffset : a * d * -1 + e, b.options.variableWidth === !0 && (f = b.slideCount <= b.options.slidesToShow || b.options.infinite === !1 ? b.$slideTrack.children(".slick-slide").eq(a) : b.$slideTrack.children(".slick-slide").eq(a + b.options.slidesToShow), c = b.options.rtl === !0 ? f[0] ? -1 * (b.$slideTrack.width() - f[0].offsetLeft - f.width()) : 0 : f[0] ? -1 * f[0].offsetLeft : 0, b.options.centerMode === !0 && (f = b.slideCount <= b.options.slidesToShow || b.options.infinite === !1 ? b.$slideTrack.children(".slick-slide").eq(a) : b.$slideTrack.children(".slick-slide").eq(a + b.options.slidesToShow + 1), c = b.options.rtl === !0 ? f[0] ? -1 * (b.$slideTrack.width() - f[0].offsetLeft - f.width()) : 0 : f[0] ? -1 * f[0].offsetLeft : 0, c += (b.$list.width() - f.outerWidth()) / 2)), c
    }, b.prototype.getOption = b.prototype.slickGetOption = function(a) {
        var b = this;
        return b.options[a]
    }, b.prototype.getNavigableIndexes = function() {
        var e, a = this,
            b = 0,
            c = 0,
            d = [];
        for (a.options.infinite === !1 ? e = a.slideCount : (b = -1 * a.options.slidesToScroll, c = -1 * a.options.slidesToScroll, e = 2 * a.slideCount); e > b;) d.push(b), b = c + a.options.slidesToScroll, c += a.options.slidesToScroll <= a.options.slidesToShow ? a.options.slidesToScroll : a.options.slidesToShow;
        return d
    }, b.prototype.getSlick = function() {
        return this
    }, b.prototype.getSlideCount = function() {
        var c, d, e, b = this;
        return e = b.options.centerMode === !0 ? b.slideWidth * Math.floor(b.options.slidesToShow / 2) : 0, b.options.swipeToSlide === !0 ? (b.$slideTrack.find(".slick-slide").each(function(c, f) {
                return f.offsetLeft - e + a(f).outerWidth() / 2 > -1 * b.swipeLeft ? (d = f, !1) : void 0
            }), c = Math.abs(a(d).attr("data-slick-index") - b.currentSlide) || 1) : b.options.slidesToScroll
    }, b.prototype.goTo = b.prototype.slickGoTo = function(a, b) {
        var c = this;
        c.changeSlide({
            data: {
                message: "index",
                index: parseInt(a)
            }
        }, b)
    }, b.prototype.init = function(b) {
        var c = this;
        a(c.$slider).hasClass("slick-initialized") || (a(c.$slider).addClass("slick-initialized"), c.buildRows(), c.buildOut(), c.setProps(), c.startLoad(), c.loadSlider(), c.initializeEvents(), c.updateArrows(), c.updateDots(), c.checkResponsive(!0), c.focusHandler()), b && c.$slider.trigger("init", [c]), c.options.accessibility === !0 && c.initADA(), c.options.autoplay && (c.paused = !1, c.autoPlay())
    }, b.prototype.initADA = function() {
        var b = this;
        b.$slides.add(b.$slideTrack.find(".slick-cloned")).attr({
            "aria-hidden": "true",
            tabindex: "-1"
        }).find("a, input, button, select").attr({
            tabindex: "-1"
        }), b.$slideTrack.attr("role", "listbox"), b.$slides.not(b.$slideTrack.find(".slick-cloned")).each(function(c) {
            a(this).attr({
                role: "option",
                "aria-describedby": "slick-slide" + b.instanceUid + c
            })
        }), null !== b.$dots && b.$dots.attr("role", "tablist").find("li").each(function(c) {
            a(this).attr({
                role: "presentation",
                "aria-selected": "false",
                "aria-controls": "navigation" + b.instanceUid + c,
                id: "slick-slide" + b.instanceUid + c
            })
        }).first().attr("aria-selected", "true").end().find("button").attr("role", "button").end().closest("div").attr("role", "toolbar"), b.activateADA()
    }, b.prototype.initArrowEvents = function() {
        var a = this;
        a.options.arrows === !0 && a.slideCount > a.options.slidesToShow && (a.$prevArrow.off("click.slick").on("click.slick", {
            message: "previous"
        }, a.changeSlide), a.$nextArrow.off("click.slick").on("click.slick", {
            message: "next"
        }, a.changeSlide))
    }, b.prototype.initDotEvents = function() {
        var b = this;
        b.options.dots === !0 && b.slideCount > b.options.slidesToShow && a("li", b.$dots).on("click.slick", {
            message: "index"
        }, b.changeSlide), b.options.dots === !0 && b.options.pauseOnDotsHover === !0 && a("li", b.$dots).on("mouseenter.slick", a.proxy(b.interrupt, b, !0)).on("mouseleave.slick", a.proxy(b.interrupt, b, !1))
    }, b.prototype.initSlideEvents = function() {
        var b = this;
        b.options.pauseOnHover && (b.$list.on("mouseenter.slick", a.proxy(b.interrupt, b, !0)), b.$list.on("mouseleave.slick", a.proxy(b.interrupt, b, !1)))
    }, b.prototype.initializeEvents = function() {
        var b = this;
        b.initArrowEvents(), b.initDotEvents(), b.initSlideEvents(), b.$list.on("touchstart.slick mousedown.slick", {
            action: "start"
        }, b.swipeHandler), b.$list.on("touchmove.slick mousemove.slick", {
            action: "move"
        }, b.swipeHandler), b.$list.on("touchend.slick mouseup.slick", {
            action: "end"
        }, b.swipeHandler), b.$list.on("touchcancel.slick mouseleave.slick", {
            action: "end"
        }, b.swipeHandler), b.$list.on("click.slick", b.clickHandler), a(document).on(b.visibilityChange, a.proxy(b.visibility, b)), b.options.accessibility === !0 && b.$list.on("keydown.slick", b.keyHandler), b.options.focusOnSelect === !0 && a(b.$slideTrack).children().on("click.slick", b.selectHandler), a(window).on("orientationchange.slick.slick-" + b.instanceUid, a.proxy(b.orientationChange, b)), a(window).on("resize.slick.slick-" + b.instanceUid, a.proxy(b.resize, b)), a("[draggable!=true]", b.$slideTrack).on("dragstart", b.preventDefault), a(window).on("load.slick.slick-" + b.instanceUid, b.setPosition), a(document).on("ready.slick.slick-" + b.instanceUid, b.setPosition)
    }, b.prototype.initUI = function() {
        var a = this;
        a.options.arrows === !0 && a.slideCount > a.options.slidesToShow && (a.$prevArrow.show(), a.$nextArrow.show()), a.options.dots === !0 && a.slideCount > a.options.slidesToShow && a.$dots.show()
    }, b.prototype.keyHandler = function(a) {
        var b = this;
        a.target.tagName.match("TEXTAREA|INPUT|SELECT") || (37 === a.keyCode && b.options.accessibility === !0 ? b.changeSlide({
                data: {
                    message: b.options.rtl === !0 ? "next" : "previous"
                }
            }) : 39 === a.keyCode && b.options.accessibility === !0 && b.changeSlide({
                data: {
                    message: b.options.rtl === !0 ? "previous" : "next"
                }
            }))
    }, b.prototype.lazyLoad = function() {
        function g(c) {
            a("img[data-lazy]", c).each(function() {
                var c = a(this),
                    d = a(this).attr("data-lazy"),
                    e = document.createElement("img");
                e.onload = function() {
                    c.animate({
                        opacity: 0
                    }, 100, function() {
                        c.attr("src", d).animate({
                            opacity: 1
                        }, 200, function() {
                            c.removeAttr("data-lazy").removeClass("slick-loading")
                        }), b.$slider.trigger("lazyLoaded", [b, c, d])
                    })
                }, e.onerror = function() {
                    c.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), b.$slider.trigger("lazyLoadError", [b, c, d])
                }, e.src = d
            })
        }
        var c, d, e, f, b = this;
        b.options.centerMode === !0 ? b.options.infinite === !0 ? (e = b.currentSlide + (b.options.slidesToShow / 2 + 1), f = e + b.options.slidesToShow + 2) : (e = Math.max(0, b.currentSlide - (b.options.slidesToShow / 2 + 1)), f = 2 + (b.options.slidesToShow / 2 + 1) + b.currentSlide) : (e = b.options.infinite ? b.options.slidesToShow + b.currentSlide : b.currentSlide, f = Math.ceil(e + b.options.slidesToShow), b.options.fade === !0 && (e > 0 && e--, f <= b.slideCount && f++)), c = b.$slider.find(".slick-slide").slice(e, f), g(c), b.slideCount <= b.options.slidesToShow ? (d = b.$slider.find(".slick-slide"), g(d)) : b.currentSlide >= b.slideCount - b.options.slidesToShow ? (d = b.$slider.find(".slick-cloned").slice(0, b.options.slidesToShow), g(d)) : 0 === b.currentSlide && (d = b.$slider.find(".slick-cloned").slice(-1 * b.options.slidesToShow), g(d))
    }, b.prototype.loadSlider = function() {
        var a = this;
        a.setPosition(), a.$slideTrack.css({
            opacity: 1
        }), a.$slider.removeClass("slick-loading"), a.initUI(), "progressive" === a.options.lazyLoad && a.progressiveLazyLoad()
    }, b.prototype.next = b.prototype.slickNext = function() {
        var a = this;
        a.changeSlide({
            data: {
                message: "next"
            }
        })
    }, b.prototype.orientationChange = function() {
        var a = this;
        a.checkResponsive(), a.setPosition()
    }, b.prototype.pause = b.prototype.slickPause = function() {
        var a = this;
        a.autoPlayClear(), a.paused = !0
    }, b.prototype.play = b.prototype.slickPlay = function() {
        var a = this;
        a.autoPlay(), a.options.autoplay = !0, a.paused = !1, a.focussed = !1, a.interrupted = !1
    }, b.prototype.postSlide = function(a) {
        var b = this;
        b.unslicked || (b.$slider.trigger("afterChange", [b, a]), b.animating = !1, b.setPosition(), b.swipeLeft = null, b.options.autoplay && b.autoPlay(), b.options.accessibility === !0 && b.initADA())
    }, b.prototype.prev = b.prototype.slickPrev = function() {
        var a = this;
        a.changeSlide({
            data: {
                message: "previous"
            }
        })
    }, b.prototype.preventDefault = function(a) {
        a.preventDefault()
    }, b.prototype.progressiveLazyLoad = function(b) {
        b = b || 1;
        var e, f, g, c = this,
            d = a("img[data-lazy]", c.$slider);
        d.length ? (e = d.first(), f = e.attr("data-lazy"), g = document.createElement("img"), g.onload = function() {
                e.attr("src", f).removeAttr("data-lazy").removeClass("slick-loading"), c.options.adaptiveHeight === !0 && c.setPosition(), c.$slider.trigger("lazyLoaded", [c, e, f]), c.progressiveLazyLoad()
            }, g.onerror = function() {
                3 > b ? setTimeout(function() {
                        c.progressiveLazyLoad(b + 1)
                    }, 500) : (e.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), c.$slider.trigger("lazyLoadError", [c, e, f]), c.progressiveLazyLoad())
            }, g.src = f) : c.$slider.trigger("allImagesLoaded", [c])
    }, b.prototype.refresh = function(b) {
        var d, e, c = this;
        e = c.slideCount - c.options.slidesToShow, !c.options.infinite && c.currentSlide > e && (c.currentSlide = e), c.slideCount <= c.options.slidesToShow && (c.currentSlide = 0), d = c.currentSlide, c.destroy(!0), a.extend(c, c.initials, {
            currentSlide: d
        }), c.init(), b || c.changeSlide({
            data: {
                message: "index",
                index: d
            }
        }, !1)
    }, b.prototype.registerBreakpoints = function() {
        var c, d, e, b = this,
            f = b.options.responsive || null;
        if ("array" === a.type(f) && f.length) {
            b.respondTo = b.options.respondTo || "window";
            for (c in f)
                if (e = b.breakpoints.length - 1, d = f[c].breakpoint, f.hasOwnProperty(c)) {
                    for (; e >= 0;) b.breakpoints[e] && b.breakpoints[e] === d && b.breakpoints.splice(e, 1), e--;
                    b.breakpoints.push(d), b.breakpointSettings[d] = f[c].settings
                }
            b.breakpoints.sort(function(a, c) {
                return b.options.mobileFirst ? a - c : c - a
            })
        }
    }, b.prototype.reinit = function() {
        var b = this;
        b.$slides = b.$slideTrack.children(b.options.slide).addClass("slick-slide"), b.slideCount = b.$slides.length, b.currentSlide >= b.slideCount && 0 !== b.currentSlide && (b.currentSlide = b.currentSlide - b.options.slidesToScroll), b.slideCount <= b.options.slidesToShow && (b.currentSlide = 0), b.registerBreakpoints(), b.setProps(), b.setupInfinite(), b.buildArrows(), b.updateArrows(), b.initArrowEvents(), b.buildDots(), b.updateDots(), b.initDotEvents(), b.cleanUpSlideEvents(), b.initSlideEvents(), b.checkResponsive(!1, !0), b.options.focusOnSelect === !0 && a(b.$slideTrack).children().on("click.slick", b.selectHandler), b.setSlideClasses("number" == typeof b.currentSlide ? b.currentSlide : 0), b.setPosition(), b.focusHandler(), b.paused = !b.options.autoplay, b.autoPlay(), b.$slider.trigger("reInit", [b])
    }, b.prototype.resize = function() {
        var b = this;
        a(window).width() !== b.windowWidth && (clearTimeout(b.windowDelay), b.windowDelay = window.setTimeout(function() {
            b.windowWidth = a(window).width(), b.checkResponsive(), b.unslicked || b.setPosition()
        }, 50))
    }, b.prototype.removeSlide = b.prototype.slickRemove = function(a, b, c) {
        var d = this;
        return "boolean" == typeof a ? (b = a, a = b === !0 ? 0 : d.slideCount - 1) : a = b === !0 ? --a : a, d.slideCount < 1 || 0 > a || a > d.slideCount - 1 ? !1 : (d.unload(), c === !0 ? d.$slideTrack.children().remove() : d.$slideTrack.children(this.options.slide).eq(a).remove(), d.$slides = d.$slideTrack.children(this.options.slide), d.$slideTrack.children(this.options.slide).detach(), d.$slideTrack.append(d.$slides), d.$slidesCache = d.$slides, void d.reinit())
    }, b.prototype.setCSS = function(a) {
        var d, e, b = this,
            c = {};
        b.options.rtl === !0 && (a = -a), d = "left" == b.positionProp ? Math.ceil(a) + "px" : "0px", e = "top" == b.positionProp ? Math.ceil(a) + "px" : "0px", c[b.positionProp] = a, b.transformsEnabled === !1 ? b.$slideTrack.css(c) : (c = {}, b.cssTransitions === !1 ? (c[b.animType] = "translate(" + d + ", " + e + ")", b.$slideTrack.css(c)) : (c[b.animType] = "translate3d(" + d + ", " + e + ", 0px)", b.$slideTrack.css(c)))
    }, b.prototype.setDimensions = function() {
        var a = this;
        a.options.vertical === !1 ? a.options.centerMode === !0 && a.$list.css({
                padding: "0px " + a.options.centerPadding
            }) : (a.$list.height(a.$slides.first().outerHeight(!0) * a.options.slidesToShow), a.options.centerMode === !0 && a.$list.css({
                padding: a.options.centerPadding + " 0px"
            })), a.listWidth = a.$list.width(), a.listHeight = a.$list.height(), a.options.vertical === !1 && a.options.variableWidth === !1 ? (a.slideWidth = Math.ceil(a.listWidth / a.options.slidesToShow), a.$slideTrack.width(Math.ceil(a.slideWidth * a.$slideTrack.children(".slick-slide").length))) : a.options.variableWidth === !0 ? a.$slideTrack.width(5e3 * a.slideCount) : (a.slideWidth = Math.ceil(a.listWidth), a.$slideTrack.height(Math.ceil(a.$slides.first().outerHeight(!0) * a.$slideTrack.children(".slick-slide").length)));
        var b = a.$slides.first().outerWidth(!0) - a.$slides.first().width();
        a.options.variableWidth === !1 && a.$slideTrack.children(".slick-slide").width(a.slideWidth - b)
    }, b.prototype.setFade = function() {
        var c, b = this;
        b.$slides.each(function(d, e) {
            c = b.slideWidth * d * -1, b.options.rtl === !0 ? a(e).css({
                    position: "relative",
                    right: c,
                    top: 0,
                    zIndex: b.options.zIndex - 2,
                    opacity: 0
                }) : a(e).css({
                    position: "relative",
                    left: c,
                    top: 0,
                    zIndex: b.options.zIndex - 2,
                    opacity: 0
                })
        }), b.$slides.eq(b.currentSlide).css({
            zIndex: b.options.zIndex - 1,
            opacity: 1
        })
    }, b.prototype.setHeight = function() {
        var a = this;
        if (1 === a.options.slidesToShow && a.options.adaptiveHeight === !0 && a.options.vertical === !1) {
            var b = a.$slides.eq(a.currentSlide).outerHeight(!0);
            a.$list.css("height", b)
        }
    }, b.prototype.setOption = b.prototype.slickSetOption = function() {
        var c, d, e, f, h, b = this,
            g = !1;
        if ("object" === a.type(arguments[0]) ? (e = arguments[0], g = arguments[1], h = "multiple") : "string" === a.type(arguments[0]) && (e = arguments[0], f = arguments[1], g = arguments[2], "responsive" === arguments[0] && "array" === a.type(arguments[1]) ? h = "responsive" : "undefined" != typeof arguments[1] && (h = "single")), "single" === h) b.options[e] = f;
        else if ("multiple" === h) a.each(e, function(a, c) {
            b.options[a] = c
        });
        else if ("responsive" === h)
            for (d in f)
                if ("array" !== a.type(b.options.responsive)) b.options.responsive = [f[d]];
                else {
                    for (c = b.options.responsive.length - 1; c >= 0;) b.options.responsive[c].breakpoint === f[d].breakpoint && b.options.responsive.splice(c, 1), c--;
                    b.options.responsive.push(f[d])
                }
        g && (b.unload(), b.reinit())
    }, b.prototype.setPosition = function() {
        var a = this;
        a.setDimensions(), a.setHeight(), a.options.fade === !1 ? a.setCSS(a.getLeft(a.currentSlide)) : a.setFade(), a.$slider.trigger("setPosition", [a])
    }, b.prototype.setProps = function() {
        var a = this,
            b = document.body.style;
        a.positionProp = a.options.vertical === !0 ? "top" : "left", "top" === a.positionProp ? a.$slider.addClass("slick-vertical") : a.$slider.removeClass("slick-vertical"), (void 0 !== b.WebkitTransition || void 0 !== b.MozTransition || void 0 !== b.msTransition) && a.options.useCSS === !0 && (a.cssTransitions = !0), a.options.fade && ("number" == typeof a.options.zIndex ? a.options.zIndex < 3 && (a.options.zIndex = 3) : a.options.zIndex = a.defaults.zIndex), void 0 !== b.OTransform && (a.animType = "OTransform", a.transformType = "-o-transform", a.transitionType = "OTransition", void 0 === b.perspectiveProperty && void 0 === b.webkitPerspective && (a.animType = !1)), void 0 !== b.MozTransform && (a.animType = "MozTransform", a.transformType = "-moz-transform", a.transitionType = "MozTransition", void 0 === b.perspectiveProperty && void 0 === b.MozPerspective && (a.animType = !1)), void 0 !== b.webkitTransform && (a.animType = "webkitTransform", a.transformType = "-webkit-transform", a.transitionType = "webkitTransition", void 0 === b.perspectiveProperty && void 0 === b.webkitPerspective && (a.animType = !1)), void 0 !== b.msTransform && (a.animType = "msTransform", a.transformType = "-ms-transform", a.transitionType = "msTransition", void 0 === b.msTransform && (a.animType = !1)), void 0 !== b.transform && a.animType !== !1 && (a.animType = "transform", a.transformType = "transform", a.transitionType = "transition"), a.transformsEnabled = a.options.useTransform && null !== a.animType && a.animType !== !1
    }, b.prototype.setSlideClasses = function(a) {
        var c, d, e, f, b = this;
        d = b.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden", "true"), b.$slides.eq(a).addClass("slick-current"), b.options.centerMode === !0 ? (c = Math.floor(b.options.slidesToShow / 2), b.options.infinite === !0 && (a >= c && a <= b.slideCount - 1 - c ? b.$slides.slice(a - c, a + c + 1).addClass("slick-active").attr("aria-hidden", "false") : (e = b.options.slidesToShow + a,
                    d.slice(e - c + 1, e + c + 2).addClass("slick-active").attr("aria-hidden", "false")), 0 === a ? d.eq(d.length - 1 - b.options.slidesToShow).addClass("slick-center") : a === b.slideCount - 1 && d.eq(b.options.slidesToShow).addClass("slick-center")), b.$slides.eq(a).addClass("slick-center")) : a >= 0 && a <= b.slideCount - b.options.slidesToShow ? b.$slides.slice(a, a + b.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false") : d.length <= b.options.slidesToShow ? d.addClass("slick-active").attr("aria-hidden", "false") : (f = b.slideCount % b.options.slidesToShow, e = b.options.infinite === !0 ? b.options.slidesToShow + a : a, b.options.slidesToShow == b.options.slidesToScroll && b.slideCount - a < b.options.slidesToShow ? d.slice(e - (b.options.slidesToShow - f), e + f).addClass("slick-active").attr("aria-hidden", "false") : d.slice(e, e + b.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false")), "ondemand" === b.options.lazyLoad && b.lazyLoad()
    }, b.prototype.setupInfinite = function() {
        var c, d, e, b = this;
        if (b.options.fade === !0 && (b.options.centerMode = !1), b.options.infinite === !0 && b.options.fade === !1 && (d = null, b.slideCount > b.options.slidesToShow)) {
            for (e = b.options.centerMode === !0 ? b.options.slidesToShow + 1 : b.options.slidesToShow, c = b.slideCount; c > b.slideCount - e; c -= 1) d = c - 1, a(b.$slides[d]).clone(!0).attr("id", "").attr("data-slick-index", d - b.slideCount).prependTo(b.$slideTrack).addClass("slick-cloned");
            for (c = 0; e > c; c += 1) d = c, a(b.$slides[d]).clone(!0).attr("id", "").attr("data-slick-index", d + b.slideCount).appendTo(b.$slideTrack).addClass("slick-cloned");
            b.$slideTrack.find(".slick-cloned").find("[id]").each(function() {
                a(this).attr("id", "")
            })
        }
    }, b.prototype.interrupt = function(a) {
        var b = this;
        a || b.autoPlay(), b.interrupted = a
    }, b.prototype.selectHandler = function(b) {
        var c = this,
            d = a(b.target).is(".slick-slide") ? a(b.target) : a(b.target).parents(".slick-slide"),
            e = parseInt(d.attr("data-slick-index"));
        return e || (e = 0), c.slideCount <= c.options.slidesToShow ? (c.setSlideClasses(e), void c.asNavFor(e)) : void c.slideHandler(e)
    }, b.prototype.slideHandler = function(a, b, c) {
        var d, e, f, g, j, h = null,
            i = this;
        return b = b || !1, i.animating === !0 && i.options.waitForAnimate === !0 || i.options.fade === !0 && i.currentSlide === a || i.slideCount <= i.options.slidesToShow ? void 0 : (b === !1 && i.asNavFor(a), d = a, h = i.getLeft(d), g = i.getLeft(i.currentSlide), i.currentLeft = null === i.swipeLeft ? g : i.swipeLeft, i.options.infinite === !1 && i.options.centerMode === !1 && (0 > a || a > i.getDotCount() * i.options.slidesToScroll) ? void(i.options.fade === !1 && (d = i.currentSlide, c !== !0 ? i.animateSlide(g, function() {
                        i.postSlide(d)
                    }) : i.postSlide(d))) : i.options.infinite === !1 && i.options.centerMode === !0 && (0 > a || a > i.slideCount - i.options.slidesToScroll) ? void(i.options.fade === !1 && (d = i.currentSlide, c !== !0 ? i.animateSlide(g, function() {
                            i.postSlide(d)
                        }) : i.postSlide(d))) : (i.options.autoplay && clearInterval(i.autoPlayTimer), e = 0 > d ? i.slideCount % i.options.slidesToScroll !== 0 ? i.slideCount - i.slideCount % i.options.slidesToScroll : i.slideCount + d : d >= i.slideCount ? i.slideCount % i.options.slidesToScroll !== 0 ? 0 : d - i.slideCount : d, i.animating = !0, i.$slider.trigger("beforeChange", [i, i.currentSlide, e]), f = i.currentSlide, i.currentSlide = e, i.setSlideClasses(i.currentSlide), i.options.asNavFor && (j = i.getNavTarget(), j = j.slick("getSlick"), j.slideCount <= j.options.slidesToShow && j.setSlideClasses(i.currentSlide)), i.updateDots(), i.updateArrows(), i.options.fade === !0 ? (c !== !0 ? (i.fadeSlideOut(f), i.fadeSlide(e, function() {
                                i.postSlide(e)
                            })) : i.postSlide(e), void i.animateHeight()) : void(c !== !0 ? i.animateSlide(h, function() {
                                i.postSlide(e)
                            }) : i.postSlide(e))))
    }, b.prototype.startLoad = function() {
        var a = this;
        a.options.arrows === !0 && a.slideCount > a.options.slidesToShow && (a.$prevArrow.hide(), a.$nextArrow.hide()), a.options.dots === !0 && a.slideCount > a.options.slidesToShow && a.$dots.hide(), a.$slider.addClass("slick-loading")
    }, b.prototype.swipeDirection = function() {
        var a, b, c, d, e = this;
        return a = e.touchObject.startX - e.touchObject.curX, b = e.touchObject.startY - e.touchObject.curY, c = Math.atan2(b, a), d = Math.round(180 * c / Math.PI), 0 > d && (d = 360 - Math.abs(d)), 45 >= d && d >= 0 ? e.options.rtl === !1 ? "left" : "right" : 360 >= d && d >= 315 ? e.options.rtl === !1 ? "left" : "right" : d >= 135 && 225 >= d ? e.options.rtl === !1 ? "right" : "left" : e.options.verticalSwiping === !0 ? d >= 35 && 135 >= d ? "down" : "up" : "vertical"
    }, b.prototype.swipeEnd = function(a) {
        var c, d, b = this;
        if (b.dragging = !1, b.interrupted = !1, b.shouldClick = b.touchObject.swipeLength > 10 ? !1 : !0, void 0 === b.touchObject.curX) return !1;
        if (b.touchObject.edgeHit === !0 && b.$slider.trigger("edge", [b, b.swipeDirection()]), b.touchObject.swipeLength >= b.touchObject.minSwipe) {
            switch (d = b.swipeDirection()) {
                case "left":
                case "down":
                    c = b.options.swipeToSlide ? b.checkNavigable(b.currentSlide + b.getSlideCount()) : b.currentSlide + b.getSlideCount(), b.currentDirection = 0;
                    break;
                case "right":
                case "up":
                    c = b.options.swipeToSlide ? b.checkNavigable(b.currentSlide - b.getSlideCount()) : b.currentSlide - b.getSlideCount(), b.currentDirection = 1
            }
            "vertical" != d && (b.slideHandler(c), b.touchObject = {}, b.$slider.trigger("swipe", [b, d]))
        } else b.touchObject.startX !== b.touchObject.curX && (b.slideHandler(b.currentSlide), b.touchObject = {})
    }, b.prototype.swipeHandler = function(a) {
        var b = this;
        if (!(b.options.swipe === !1 || "ontouchend" in document && b.options.swipe === !1 || b.options.draggable === !1 && -1 !== a.type.indexOf("mouse"))) switch (b.touchObject.fingerCount = a.originalEvent && void 0 !== a.originalEvent.touches ? a.originalEvent.touches.length : 1, b.touchObject.minSwipe = b.listWidth / b.options.touchThreshold, b.options.verticalSwiping === !0 && (b.touchObject.minSwipe = b.listHeight / b.options.touchThreshold), a.data.action) {
            case "start":
                b.swipeStart(a);
                break;
            case "move":
                b.swipeMove(a);
                break;
            case "end":
                b.swipeEnd(a)
        }
    }, b.prototype.swipeMove = function(a) {
        var d, e, f, g, h, b = this;
        return h = void 0 !== a.originalEvent ? a.originalEvent.touches : null, !b.dragging || h && 1 !== h.length ? !1 : (d = b.getLeft(b.currentSlide), b.touchObject.curX = void 0 !== h ? h[0].pageX : a.clientX, b.touchObject.curY = void 0 !== h ? h[0].pageY : a.clientY, b.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(b.touchObject.curX - b.touchObject.startX, 2))), b.options.verticalSwiping === !0 && (b.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(b.touchObject.curY - b.touchObject.startY, 2)))), e = b.swipeDirection(), "vertical" !== e ? (void 0 !== a.originalEvent && b.touchObject.swipeLength > 4 && a.preventDefault(), g = (b.options.rtl === !1 ? 1 : -1) * (b.touchObject.curX > b.touchObject.startX ? 1 : -1), b.options.verticalSwiping === !0 && (g = b.touchObject.curY > b.touchObject.startY ? 1 : -1), f = b.touchObject.swipeLength, b.touchObject.edgeHit = !1, b.options.infinite === !1 && (0 === b.currentSlide && "right" === e || b.currentSlide >= b.getDotCount() && "left" === e) && (f = b.touchObject.swipeLength * b.options.edgeFriction, b.touchObject.edgeHit = !0), b.options.vertical === !1 ? b.swipeLeft = d + f * g : b.swipeLeft = d + f * (b.$list.height() / b.listWidth) * g, b.options.verticalSwiping === !0 && (b.swipeLeft = d + f * g), b.options.fade === !0 || b.options.touchMove === !1 ? !1 : b.animating === !0 ? (b.swipeLeft = null, !1) : void b.setCSS(b.swipeLeft)) : void 0)
    }, b.prototype.swipeStart = function(a) {
        var c, b = this;
        return b.interrupted = !0, 1 !== b.touchObject.fingerCount || b.slideCount <= b.options.slidesToShow ? (b.touchObject = {}, !1) : (void 0 !== a.originalEvent && void 0 !== a.originalEvent.touches && (c = a.originalEvent.touches[0]), b.touchObject.startX = b.touchObject.curX = void 0 !== c ? c.pageX : a.clientX, b.touchObject.startY = b.touchObject.curY = void 0 !== c ? c.pageY : a.clientY, void(b.dragging = !0))
    }, b.prototype.unfilterSlides = b.prototype.slickUnfilter = function() {
        var a = this;
        null !== a.$slidesCache && (a.unload(), a.$slideTrack.children(this.options.slide).detach(), a.$slidesCache.appendTo(a.$slideTrack), a.reinit())
    }, b.prototype.unload = function() {
        var b = this;
        a(".slick-cloned", b.$slider).remove(), b.$dots && b.$dots.remove(), b.$prevArrow && b.htmlExpr.test(b.options.prevArrow) && b.$prevArrow.remove(), b.$nextArrow && b.htmlExpr.test(b.options.nextArrow) && b.$nextArrow.remove(), b.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden", "true").css("width", "")
    }, b.prototype.unslick = function(a) {
        var b = this;
        b.$slider.trigger("unslick", [b, a]), b.destroy()
    }, b.prototype.updateArrows = function() {
        var b, a = this;
        b = Math.floor(a.options.slidesToShow / 2), a.options.arrows === !0 && a.slideCount > a.options.slidesToShow && !a.options.infinite && (a.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), a.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), 0 === a.currentSlide ? (a.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true"), a.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : a.currentSlide >= a.slideCount - a.options.slidesToShow && a.options.centerMode === !1 ? (a.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), a.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : a.currentSlide >= a.slideCount - 1 && a.options.centerMode === !0 && (a.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), a.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")))
    }, b.prototype.updateDots = function() {
        var a = this;
        null !== a.$dots && (a.$dots.find("li").removeClass("slick-active").attr("aria-hidden", "true"), a.$dots.find("li").eq(Math.floor(a.currentSlide / a.options.slidesToScroll)).addClass("slick-active").attr("aria-hidden", "false"))
    }, b.prototype.visibility = function() {
        var a = this;
        a.options.autoplay && (document[a.hidden] ? a.interrupted = !0 : a.interrupted = !1)
    }, a.fn.slick = function() {
        var f, g, a = this,
            c = arguments[0],
            d = Array.prototype.slice.call(arguments, 1),
            e = a.length;
        for (f = 0; e > f; f++)
            if ("object" == typeof c || "undefined" == typeof c ? a[f].slick = new b(a[f], c) : g = a[f].slick[c].apply(a[f].slick, d), "undefined" != typeof g) return g;
        return a
    }
});

function load_posts(e) {
    pageNumber++;
    var t = "&pageNumber=" + pageNumber + "&ppp=" + ppp + "&author=" + e + "&action=more_post_ajax";
    return jQuery.ajax({
        type: "POST",
        dataType: "html",
        url: "/wp-admin/admin-ajax.php",
        data: t,
        success: function(e) {
            var t = jQuery.parseJSON(e);
            t.content.length ? (jQuery("#more_posts").attr("disabled", !1), jQuery("#ajax-posts").append(t.content), jQuery(".matchheight").matchHeight(), 0 == t.more && jQuery("#more_posts").hide()) : jQuery("#more_posts").hide()
        },
        error: function(e, t, s) {
            $loader.html(e + " :: " + t + " :: " + s)
        }
    }), !1
}

function load_posts_blog() {
    pageNumberBlog++;
    var e = "&pageNumber=" + pageNumberBlog + "&ppp=" + pppBlog + "&action=more_post_ajax_blog";
    return jQuery.ajax({
        type: "POST",
        dataType: "html",
        url: "/wp-admin/admin-ajax.php",
        data: e,
        success: function(e) {
            var t = jQuery.parseJSON(e);
            t.content.length ? (jQuery("#more_posts_blog").attr("disabled", !1), jQuery("#ajax-posts").append(t.content), jQuery(".matchheight").matchHeight(), 0 == t.more && jQuery("#more_posts_blog").hide()) : jQuery("#more_posts_blog").hide()
        },
        error: function(e, t, s) {
            $loader.html(e + " :: " + t + " :: " + s)
        }
    }), !1
}

function animationLoadedPost() {
    wow = new WOW({
        boxClass: "wow",
        animateClass: "animated",
        offset: 130,
        mobile: !0,
        live: !0
    }), wow.init()
}

function MoneyMask(e, t) {
    jQuery(e).removeAttr("min").removeAttr("max").removeAttr("maxlength");
    var s = jQuery(e).clone().insertAfter(jQuery(e));
    jQuery(e).attr("type", "hidden"), s.attr("id", s.attr("id") + "Masked"), s.attr("name", s.attr("id"));
    var i = !!t,
        n = i ? "$" : "",
        o = function(e) {
            var t = e.replace(/\D/g, "").length;
            return 0 === t ? "000000" : t < 4 ? n + "000000" : 4 === t ? n + "0,0000" : 5 === t ? n + "00,0000" : 6 === t ? n + "000,0000" : t > 6 ? n + "0,000,000" : void 0
        },
        r = {
            onKeyPress: function(e, t, s, i) {
                s.mask(o.apply({}, arguments), i)
            }
        };
    s.mask(o, r), s.on("change", function() {
        var t = jQuery(this).val(),
            s = t.replace(/[^0-9]/g, "");
        jQuery(e).val(s)
    })
}

function changeFx(e, t, s) {
    var i = (e / (s * t / 1200)).toFixed(0);
    i = Math.max(1, i);
    var n = (e / i).toFixed(0);
    return n = Math.min(n, e), isNaN(i) || isNaN(n) || (i = i, n = n), [i, n]
}

function update_loan_length_calculator(e, t) {
    e || t ? (jQuery("#res_revenue").html(jQuery("#revenue").val()), jQuery("#res_amount").html(jQuery("#amount").val()), jQuery("#res_payback").html(jQuery("#payback").val()), jQuery("#res_months").html(e), jQuery("#res_monthly_payment").html(t), jQuery(".result_value").unmask().mask("#,##0.00", {
            reverse: !0
        })) : (jQuery("#res_revenue").html(0), jQuery("#res_amount").html(0), jQuery("#res_payback").html(0), jQuery("#res_months").html(0), jQuery("#res_monthly_payment").html(0)), jQuery(".calc_result").show(), jQuery(".cta_b").show(), jQuery("#calc").hide()
}

function calculate_short_term() {
    var e = jQuery("#loan_amount").val(),
        t = jQuery("#payment_terms").val() * jQuery("#payment").val(),
        s = jQuery("#interest_rate").val() / 1200,
        i = e * (s * Math.pow(1 + s, t)) / (Math.pow(1 + s, t) - 1);
    return [i, i * t - e, i * t]
}

function calculate_short_term_2() {
    var e = jQuery("#loan_amount2").val(),
        t = jQuery("#payment_terms2").val() * jQuery("#payment2").val(),
        s = jQuery("#interest_rate2").val() / 1200,
        i = jQuery("#interest_rate2").val(),
        n = Math.round(ExcelFormulas.PMT(s, t, -1 * e));
    return [n, ExcelFormulas.EFFECT(i, t), n * t]
}

function calculate_invoice_factoring() {
    var e = parseFloat(jQuery("#invoice_amount").val()),
        t = parseFloat(jQuery("#advance_percentage").val() / 100),
        s = parseFloat(jQuery("#factoring_fee").val()) / 100,
        i = parseFloat(jQuery("#cnt_w").val() * jQuery("#t_period").val()),
        n = e * t,
        o = e - n - s * e * i,
        r = e - (n + o),
        a = r / e * 52 / i * 100;
    return [parseFloat(n).toFixed(1), parseFloat(o).toFixed(1), parseFloat(r).toFixed(1), parseFloat(a).toFixed(1)]
}

function update_invoice_factoring_calculator(e) {
    jQuery("#advanced_amount").html(e[0]), jQuery("#ral_fee").html(e[1]), jQuery("#total_cost").html(e[2]), jQuery("#effective_apr").html(e[3] + "%"), jQuery(".calc_result").show(), jQuery(".cta_b").show(), jQuery("#calc").hide()
}

function calculate_merchant_cash_advance() {
    var e = parseInt(jQuery("#total_advance").val()),
        t = parseFloat(jQuery("#factoring_rate").val()),
        s = parseInt(jQuery("#monthly_ccs").val()),
        i = parseFloat(jQuery("#percent_ccs").val() / 100),
        n = parseFloat(jQuery("#other_fees").val()),
        o = s * i / 30,
        r = e * t + n - e,
        a = (r + e) / o,
        l = o / a * 365 / a * 100;
    return [parseFloat(a).toFixed(1), parseFloat(r).toFixed(1), parseFloat(l).toFixed(1)]
}

function update_merchant_cash_advance(e) {
    jQuery("#total_rp").html(e[0]), jQuery("#total_cost").html(e[1]), jQuery("#effective_apr").html(e[2] + "%"), jQuery(".calc_result").show(), jQuery(".cta_b").show(), jQuery("#calc").hide()
}

function update_short_term_calculator2(e) {
    e ? (jQuery("#res_loan_amount2").html(parseInt(jQuery("#loan_amount2").val()) + ".00"), jQuery("#res_interest_paid2").html(e[1].toFixed(2)), jQuery("#res_monthly_payment2").html(e[0].toFixed(2)), jQuery("#res_total_repayment2").html(e[2].toFixed(2)), jQuery(".result_value").unmask().mask("#,##0.00", {
            reverse: !0
        })) : (jQuery("#res_loan_amount2").html(0), jQuery("#res_interest_paid2").html(0), jQuery("#res_monthly_payment2").html(0), jQuery("#res_total_repayment2").html(0)), jQuery(".calc_result").show(), jQuery(".cta_b").show(), jQuery("#calc").hide()
}

function update_short_term_calculator(e) {
    e ? (jQuery("#res_loan_amount").html(parseInt(jQuery("#loan_amount").val()) + ".00"), jQuery("#res_interest_paid").html(e[1].toFixed(2)), jQuery("#res_monthly_payment").html(e[0].toFixed(2)), jQuery("#res_total_repayment").html(e[2].toFixed(2)), jQuery(".result_value").unmask().mask("#,##0.00", {
            reverse: !0
        })) : (jQuery("#res_loan_amount").html(0), jQuery("#res_interest_paid").html(0), jQuery("#res_monthly_payment").html(0), jQuery("#res_total_repayment").html(0)), jQuery(".calc_result").show(), jQuery(".cta_b").show(), jQuery("#calc").hide()
}

function format_factoring(e) {
    var t = jQuery(e).val(),
        s = t.replace(/[^0-9\.]/g, "");
    jQuery(e).val(s)
}

function initSameHeight() {
    jQuery(".info-row").sameHeight({
        elements: "h3",
        flexible: !0,
        multiLine: !0,
        biggestHeight: !0
    })
}

function initSameHeight() {
    jQuery(".content-our-values .items-list").sameHeight({
        elements: ".item",
        flexible: !0,
        multiLine: !0,
        biggestHeight: !0
    })
}

function initCycleCarousel() {
    jQuery("div.cycle-gallery.gallery").scrollAbsoluteGallery({
        mask: "div.mask",
        slider: "div.slideset",
        slides: "div.slide",
        generatePagination: ".pagination",
        stretchSlideToMask: !0,
        pauseOnHover: !0,
        maskAutoSize: !0,
        autoRotation: !1,
        switchTime: 3e3,
        animSpeed: 500
    })
}

function initSlickCarousel() {
    jQuery(".finance_products-slick").slick({
        mobileFirst: !0,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: !0,
        arrows: !1,
        dotsClass: "list-unstyled slick-dots-mobile",
        prevArrow: jQuery(".slick-prev"),
        nextArrow: jQuery(".slick-next"),
        responsive: [{
            breakpoint: 510,
            settings: {
                arrows: !0,
                dotsClass: "slick-counter",
                customPaging: function(e, t) {
                    return "PAGE " + (t + 1) + " / " + e.slideCount
                }
            }
        }]
    })
}

function initSlickCarouselTable() {
    jQuery(".table-carousel-mobile").slick({
        dots: !0,
        dotsClass: "list-unstyled slick-dots-mobile",
        arrows: !1
    })
}

function shareList() {
    function e() {
        var e = jQuery(this).scrollTop();
        Math.abs(n - e) <= o || (e > n && e > r ? i.removeClass("pre-state").removeClass("animate-hide").addClass("animate-show") : e + jQuery(window).height() < jQuery(document).height() && i.removeClass("pre-state").removeClass("animate-show").addClass("animate-hide"), n = e)
    }
    var t, s = window.matchMedia("(max-width: 767px)"),
        i = jQuery(".share-list-holder"),
        n = 0,
        o = 5,
        r = jQuery("header").outerHeight();
    jQuery(window).scroll(function() {
        t = !0
    }), setInterval(function() {
        if (t) {
            if (!s.matches) return !1;
            e(), t = !1
        }
    }, 250)
}

function fixModal() {
    var e = jQuery(".wrapper-cm"),
        t = jQuery(window).width(),
        s = jQuery("body"),
        i = jQuery(".header-panel").height();
    onOpen = function() {
        if (!(t > 756)) {
            s.addClass("fix-modal-ios");
            var n = jQuery(window).scrollTop();
            e.data("ycoord", n), n *= -1, e.css({
                position: "fixed",
                left: "0",
                right: "0",
                top: n + i + "px"
            })
        }
    }, onClosed = function() {
        t > 756 || (s.removeClass("fix-modal-ios"), e.css({
            position: "static",
            top: "auto",
            left: "auto",
            right: "auto",
            bottom: "auto"
        }), jQuery(window).scrollTop(e.data("ycoord")))
    }, jQuery(document).on("click", "form-app first-step button", function() {
        jQuery("body").hasClass("modal-open") && onOpen()
    }), jQuery(document).on("click", "popup-step", function() {
        setTimeout(function() {
            jQuery("body").hasClass("modal-open") || onClosed()
        }, 600)
    })
}
var ppp = 10,
    pppBlog = 15,
    pageNumber = 1,
    pageNumberBlog = 1;
animationLoadedPost(), jQuery("#more_posts").on("click", function(e) {
    e.preventDefault(), jQuery("#more_posts").attr("disabled", !0), load_posts(jQuery("#more_posts").data("author"))
}), jQuery("#more_posts_blog").on("click", function(e) {
    e.preventDefault(), jQuery("#more_posts_blog").attr("disabled", !0), load_posts_blog()
});
var ExcelFormulas = {
    PVIF: function(e, t) {
        return Math.pow(1 + e, t)
    },
    FVIFA: function(e, t) {
        return 0 == e ? t : (this.PVIF(e, t) - 1) / e
    },
    EFFECT: function(e, t) {
        return !(e <= 0 || t < 1) && (t = parseInt(t, 10), Math.pow(1 + e / t, t) - 1)
    },
    PMT: function(e, t, s, i, n) {
        if (i || (i = 0), n || (n = 0), 0 == e) return -(s + i) / t;
        var o = Math.pow(1 + e, t),
            r = e / (o - 1) * -(s * o + i);
        return 1 == n && (r /= 1 + e), r
    },
    IPMT: function(e, t, s, i) {
        var n = Math.pow(1 + s, i);
        return 0 - (e * n * s + t * (n - 1))
    },
    PPMT: function(e, t, s, i, n, o) {
        if (t < 1 || t >= s + 1) return null;
        var r = this.PMT(e, s, i, n, o);
        return r - this.IPMT(i, r, e, t - 1)
    },
    DaysBetween: function(e, t) {
        return Math.round(Math.abs((e.getTime() - t.getTime()) / 864e5))
    },
    XNPV: function(e, t) {
        var s = 0,
            i = new Date(t[0].Date);
        for (var n in t) {
            var o = t[n],
                r = o.Flow,
                a = new Date(o.Date);
            s += r / Math.pow(1 + e, this.DaysBetween(i, a) / 365)
        }
        return s
    },
    XIRR: function(e, t) {
        t || (t = .1);
        for (var s = 0, i = t, n = this.XNPV(s, e), o = this.XNPV(i, e), r = 0; r < 100 && !(n * o < 0); r++) Math.abs(n) < Math.abs(o) ? n = this.XNPV(s += 1.6 * (s - i), e) : o = this.XNPV(i += 1.6 * (i - s), e);
        if (n * o > 0) return null;
        if (this.XNPV(s, e) < 0) var a = s,
            l = i - s;
        else var a = i,
            l = s - i;
        for (var r = 0; r < 100; r++) {
            l *= .5;
            var d = a + l,
                c = this.XNPV(d, e);
            if (c <= 0 && (a = d), Math.abs(c) < 1e-6 || Math.abs(l) < 1e-6) return d
        }
        return null
    }
};
if (jQuery(document).ready(function(e) {
        MoneyMask("#how_much", !0), MoneyMask("#revenue", !0), MoneyMask("#amount", !0), MoneyMask("#loan_amount", !0), MoneyMask("#loan_amount2", !0), MoneyMask("#gross_sales", !0), MoneyMask("#invoice_amount", !0), MoneyMask("#total_advance", !0), MoneyMask("#monthly_ccs", !0), MoneyMask("#GrossRevenueMasked", !0), MoneyMask("#BorrowAmountMasked", !0), jQuery("#factoring_fee").on("keyup", function() {
            format_factoring(this)
        }), jQuery("#factoring_fee").on("change", function() {
            format_factoring(this)
        })
    }), jQuery(document).ready(function() {
        function e(e, t) {
            jQuery(e).removeAttr("min").removeAttr("max").removeAttr("maxlength");
            var s = jQuery(e).clone().insertAfter(jQuery(e));
            jQuery(e).attr("type", "hidden"), s.attr("id", s.attr("id") + "Masked"), s.attr("name", s.attr("id"));
            var i = !!t,
                n = i ? "$" : "",
                o = function(e) {
                    var t = e.replace(/\D/g, "").length;
                    return 0 === t ? "000000" : t < 4 ? n + "000000" : 4 === t ? n + "0,0000" : 5 === t ? n + "00,0000" : 6 === t ? n + "000,0000" : t > 6 ? n + "0,000,000" : void 0
                },
                r = {
                    onKeyPress: function(e, t, s, i) {
                        s.mask(o.apply({}, arguments), i)
                    }
                };
            s.mask(o, r), s.on("change", function() {
                var t = jQuery(this).val(),
                    s = t.replace(/[^0-9]/g, "");
                jQuery(e).val(s)
            })
        }
        var t = {
            $GrossRevenue: jQuery("#GrossRevenue"),
            $BorrowAmount: jQuery("#BorrowAmount"),
            $Payback: jQuery("#Payback"),
            $resultTotalMonths: jQuery('[data-calc="resultTotalMonths"]'),
            $resultMonthlyPayment: jQuery('[data-calc="resultMonthlyPayment"]'),
            init: function() {
                jQuery("#calculator").on("submit", function(e) {
                    return this.doCalc(), e.preventDefault(), !1
                }.bind(this)), jQuery("#calculator").on("keydown", "input", function() {
                    jQuery(this).closest(".has-error").removeClass("has-error")
                });
                var e = document.getElementById("slider");
                if (e.length) {
                    noUiSlider.create(e, {
                        start: 20,
                        step: 1,
                        range: {
                            min: 1,
                            max: 100
                        },
                        pips: {
                            mode: "values",
                            values: [1, 100],
                            density: 100
                        }
                    });
                    var t = !1;
                    e.noUiSlider.on("update", function(e, s) {
                        t || (jQuery(".noUi-handle").append('<span class="noUi-handle-value"></span>'), t = !0), jQuery(".slider_value, .noUi-handle-value").html(+e[s]), this.$Payback.val(e[s])
                    }.bind(this))
                }
                jQuery("#calculator").on("change", "input", function() {}.bind(this))
            },
            doCalc: function() {
                if (this.validation()) {
                    var e = parseInt(this.$GrossRevenue.val()),
                        t = parseInt(this.$BorrowAmount.val()),
                        s = parseInt(this.$Payback.val());
                    console.log(t, s, e);
                    (function(e, t, s) {
                        var i = (e / (s * t / 1200)).toFixed(0);
                        i = Math.max(1, i);
                        var n = (e / i).toFixed(0);
                        n = Math.min(n, e), isNaN(i) || isNaN(n) || (this.months = i, this.monthly = n)
                    }).bind(this)(t, s, e), this.render()
                }
            },
            validation: function() {
                var e = !0;
                return jQuery([this.$GrossRevenue, this.$BorrowAmount, this.$Payback]).each(function() {
                    0 != jQuery(this).val() && "" != jQuery(this).val() || (e = !1)
                }), e
            },
            render: function() {
                this.$resultTotalMonths.html(this.months), this.$resultMonthlyPayment.html("$" + this.monthly)
            }
        };
        jQuery("#calculator").length && t.init(), (jQuery("#GrossRevenue").length || jQuery("#BorrowAmount").length) && (e("#GrossRevenue", !1), e("#BorrowAmount", !1))
    }), null !== document.querySelector(".subpage_menu") && (! function() {
        function e() {
            if (null == s) {
                for (var e = getComputedStyle(t, ""), n = "", o = 0; o < e.length; o++) 0 != e[o].indexOf("overflow") && 0 != e[o].indexOf("padding") && 0 != e[o].indexOf("border") && 0 != e[o].indexOf("outline") && 0 != e[o].indexOf("box-shadow") && 0 != e[o].indexOf("background") || (n += e[o] + ": " + e.getPropertyValue(e[o]) + "; ");
                s = document.createElement("div"), s.style.cssText = n + " box-sizing: border-box; width: " + t.offsetWidth + "px;", t.insertBefore(s, t.firstChild);
                for (var r = t.childNodes.length, o = 1; o < r; o++) s.appendChild(t.childNodes[1]);
                t.style.height = s.getBoundingClientRect().height + "px", t.style.padding = "0", t.style.border = "0"
            }
            var a = t.getBoundingClientRect(),
                l = Math.round(a.top + s.getBoundingClientRect().height - document.querySelector("#sticky_articles").getBoundingClientRect().bottom);
            a.top - i <= 0 ? a.top - i <= l ? (s.className = "stop", s.style.top = -l + "px") : (s.className = "sticky", s.style.top = i + 64 + "px") : (s.className = "", s.style.top = ""), window.addEventListener("resize", function() {
                t.children[0].style.width = getComputedStyle(t, "").width
            }, !1)
        }
        var t = document.querySelector("#sticky_menu"),
            s = null,
            i = 0;
        window.addEventListener("scroll", e, !1), document.body.addEventListener("scroll", e, !1)
    }(), jQuery(document).ready(function() {
        jQuery("#sticky_menu ul li a").on("click", "a", function(e) {
            e.preventDefault();
            var t = jQuery(this).attr("href"),
                s = jQuery(t).offset().top - 120;
            jQuery("body,html").animate({
                scrollTop: s
            }, 1500)
        })
    }), jQuery(document).ready(function() {
        jQuery(window).scroll(function() {
            var e = jQuery(this).scrollTop();
            jQuery("#sticky_menu ul li a").each(function(t) {
                jQuery(jQuery(this).attr("href")).length && e >= jQuery(jQuery(this).attr("href")).offset().top - 170 && (jQuery("#sticky_menu ul li a").not(this).removeClass("active"), jQuery(this).addClass("active"))
            })
        })
    }), jQuery(function() {
        jQuery("a[href*=#]:not([href=#])").click(function() {
            if (location.pathname.replace(/^\//, "") == this.pathname.replace(/^\//, "") && location.hostname == this.hostname) {
                var e = jQuery(this.hash),
                    t = window.matchMedia("(max-width: 767px)");
                if (e = e.length ? e : jQuery("[name=" + this.hash.slice(1) + "]"), e.length) return t.matches ? (console.log("if"), jQuery("html,body").animate({
                        scrollTop: e.offset().top - 380
                    }, 850), !1) : (console.log("else"), jQuery("html,body").animate({
                        scrollTop: e.offset().top - 140
                    }, 850), !1)
            }
        })
    })), jQuery(".arrow-to-top").length && jQuery(".arrow-to-top").click(function(e) {
        e.preventDefault(), jQuery("html, body").animate({
            scrollTop: jQuery("body").offset().top
        }, "slow")
    }), jQuery(".content-how-much, .how-much-form").length && (jQuery("#RequestedAmount").length && MoneyMask("#RequestedAmount", !0), jQuery("#RequestedAmount2").length && MoneyMask("#RequestedAmount2", !0), jQuery("#GrossSales").length && MoneyMask("#GrossSales", !0), jQuery("#GrossSales2").length && MoneyMask("#GrossSales2", !0)), jQuery(document).ready(function() {
        var e = jQuery(document),
            t = jQuery("header.header-panel");
        e.scroll(function() {
            e.scrollTop() >= 30 ? t.addClass("hasScrolled") : t.removeClass("hasScrolled")
        })
    }), jQuery(document).ready(function() {
        jQuery(".share-quote").hover(function() {
            $this = jQuery(this).find(".share-quote-resource"), $this.position().left <= 15 ? $this.attr("data-placement", "bottom") : $this.attr("data-placement", "right")
        })
    }), jQuery(function() {
        function e() {
            jQuery(".header-panel").find(".main-menu").slideUp(), jQuery("body").removeClass("menu-opened")
        }

        function t() {
            if (window.matchMedia("(max-width: 767px)").matches) return n.find(".sub-menu").slideUp(i), void n.removeClass("opened");
            n.find(".sub-menu").fadeOut(i), n.removeClass("opened")
        }

        function s() {
            function e() {
                t = jQuery(".b-cards").offset().top, s = jQuery(".panel-cards").outerHeight(!0), o = jQuery(".b-cards").outerHeight(!0), a = jQuery(".header-panel").outerHeight(!0), r = t + o
            }
            if (jQuery(".b-cards-description").length) {
                var t, s, i, n, o, r, a;
                n = jQuery(".panel-cards"), e(), jQuery('<div class="panel-cards_clone" />').insertBefore(".panel-cards").css("height", s).hide(), jQuery(window).resize(function() {
                    e()
                }), jQuery(window).scroll(function() {
                    i = jQuery(window).scrollTop(), i >= r ? (n.addClass("panel-cards_fixed"), jQuery(".panel-cards_toggle").addClass("show"), jQuery(".panel-cards_clone").show()) : (n.removeClass("panel-cards_fixed"), jQuery(".panel-cards_toggle").removeClass("show"), jQuery(".panel-cards_clone").hide())
                }), jQuery(".panel-cards a[data-scroll]").on("click", function(e) {
                    if (jQuery(".panel-cards").is(".panel-cards_fixed")) {
                        jQuery(".panel-cards").removeClass("show"), jQuery(".panel-cards_toggle").find("span").addClass("on").html("Browse Cards");
                        var t = jQuery(this).attr("data-scroll"),
                            s = jQuery('article[data-anchor="' + t + '"]').offset().top - (a + 91 + 40);
                        return jQuery("body,html").animate({
                            scrollTop: s
                        }, 500), !1
                    }
                    e.preventDefault()
                })
            }
        }
        initCycleCarousel(), initSameHeight(), jQuery('[type="reset"]').click(function(e) {
            setTimeout(function() {}, 1), e.preventDefault()
        }), jQuery(".header-panel-wraper .btn-link").click(function() {
            jQuery("body").toggleClass("menu-opened"), t(), jQuery(".ovarlay").click(function() {
                jQuery("body").removeClass("menu-opened")
            })
        }), jQuery(".header-panel .btn-burger").click(function(e) {
            e.preventDefault(), e.stopImmediatePropagation(), jQuery(".header-panel").find(".main-menu").slideToggle()
        }), jQuery("body").hasClass("menu-opened") ? jQuery("html").css("overflow-y", "hidden") : jQuery("html").css("overflow-y", "auto"), jQuery(".header-panel-wraper .main-menu .close").click(function() {
            jQuery("body").removeClass("menu-opened"), jQuery(".ovarlay").hide()
        });
        var i = 550,
            n = jQuery(".header-panel-wraper ul li.menu-item-has-children");
        jQuery(document).click(function() {
            e(), t()
        }), jQuery(".main-menu ul li.menu-item-has-children>a").click(function(e) {
            e.preventDefault(), e.stopImmediatePropagation();
            var t = jQuery(this).parent();
            return window.matchMedia("(max-width: 767px)").matches ? t.is(".opened") ? (t.removeClass("opened"), void t.find(".sub-menu").slideUp(i)) : (n.removeClass("opened"), n.find(".sub-menu").slideUp(i), t.addClass("opened"), void(t.is(".opened") && t.find(".sub-menu").slideDown(i))) : (i = 350, t.is(".opened") ? (t.removeClass("opened"), void t.find(".sub-menu").fadeOut(i)) : (n.removeClass("opened"), n.find(".sub-menu").fadeOut(i), t.addClass("opened"), void(t.is(".opened") && t.find(".sub-menu").fadeIn(i))))
        }), jQuery(".menu-main-menu-container .sub-menu,.menu-header-menu-container .sub-menu,.menu-main-menu-container .menu").click(function(e) {
            e.stopImmediatePropagation()
        }), jQuery(".menu-header-menu-container .menu-item-has-children>a").click(function(t) {
            t.preventDefault(), t.stopImmediatePropagation(), e();
            var s = jQuery(this).parent();
            if (i = 350, s.is(".opened")) return s.removeClass("opened"), void s.find(".sub-menu").fadeOut(i);
            n.removeClass("opened"), n.find(".sub-menu").fadeOut(i), s.addClass("opened"), s.is(".opened") && s.find(".sub-menu").fadeIn(i)
        }), jQuery(".subpage_menu .mobile_menu_icon").click(function() {
            jQuery(".subpage_menu .menu").slideToggle("slow")
        }), jQuery(".subpage_menu .menu li a").click(function() {
            window.matchMedia("(max-width: 767px)").matches && jQuery(".subpage_menu .menu").slideToggle("slow")
        }), jQuery(".sub-menu.col-2 .col-holder").each(function() {
            $this = jQuery(this), !$this.children(".col").length > 0 && $this.parent().removeClass("col-2")
        }), jQuery(".matchheight").matchHeight(), jQuery(".matchheight-row").matchHeight({
            byRow: !1
        }),
            function() {
                slideDuration = 350, jQuery(".comment-form-toggle").click(function() {
                    $this = jQuery(this), $this.fadeOut(slideDuration - 30), $this.siblings(".comment-form-holder").slideDown(slideDuration, function() {
                        jQuery(this).find("textarea").focus()
                    })
                })
            }(),
            function() {
                jQuery(".accordion-header").click(function() {
                    var e = jQuery(this),
                        t = e.parent(),
                        s = t.find(".accordion-body");
                    t.toggleClass("open"), s.slideToggle()
                })
            }(), s(), jQuery(".panel-cards_toggle").click(function() {
            $this = jQuery(this), txtContent = $this.find("span"), jQuery(".panel-cards.panel-cards_fixed").toggleClass("show"), jQuery(".panel-cards.panel-cards_fixed").is(".show") ? (txtContent.html("Close"), txtContent.removeClass("on")) : (txtContent.html("Browse Cards"), txtContent.addClass("on"))
        }), jQuery(window).resize(function() {
            jQuery(".panel-cards_clone").remove(), jQuery(".panel-cards").removeClass("panel-cards_fixed"), s()
        }), jQuery(".b-cards-description").length && function() {
            jQuery(window).scroll(function() {
                var e = jQuery(window).scrollTop(),
                    t = jQuery(".b-cards-description").offset().top;
                e >= t && jQuery(".card-article").each(function(t) {
                    jQuery(this).position().top <= e + 200 && (jQuery(".b-card a[data-scroll]").parent().removeClass("active"), jQuery(".b-card a[data-scroll]").eq(t).parent().addClass("active"))
                })
            }).scroll()
        }()
    }), initSlickCarousel(), initSlickCarouselTable(), function(e) {
        function t(e, t) {
            var n, o = jQuery(),
                a = 0,
                l = e.eq(0).offset().top;
            e.each(function(e) {
                var r = jQuery(this);
                r.offset().top === l ? o = o.add(this) : (n = s(o), a = Math.max(a, i(o, n, t)), o = r, l = r.offset().top)
            }), o.length && (n = s(o), a = Math.max(a, i(o, n, t))), t.biggestHeight && e.css(t.useMinHeight && r ? "minHeight" : "height", a)
        }

        function s(e) {
            var t = 0;
            return e.each(function() {
                t = Math.max(t, jQuery(this).outerHeight())
            }), t
        }

        function i(e, t, s) {
            var i, n = "number" == typeof t ? t : t.height();
            return e.removeClass(s.leftEdgeClass).removeClass(s.rightEdgeClass).each(function(e) {
                var o = jQuery(this),
                    a = 0,
                    l = "border-box" === o.css("boxSizing") || "border-box" === o.css("-moz-box-sizing") || "border-box" === o.css("-webkit-box-sizing");
                "number" != typeof t && o.parents().each(function() {
                    var e = jQuery(this);
                    if (t.is(this)) return !1;
                    a += e.outerHeight() - e.height()
                }), i = n - a, (i -= l ? 0 : o.outerHeight() - o.height()) > 0 && o.css(s.useMinHeight && r ? "minHeight" : "height", i)
            }), e.filter(":first").addClass(s.leftEdgeClass), e.filter(":last").addClass(s.rightEdgeClass), i
        }
        var n = function() {
                jQuery("body").css("padding-top", jQuery(".header-panel").outerHeight(!0))
            },
            o = !1;
        jQuery(window).resize(function() {
            o = !0
        }), setInterval(function() {
            o && (o = !1, n())
        }, 250), jQuery(document).ready(function() {
            n()
        }), e.fn.sameHeight = function(s) {
            var n = e.extend({
                skipClass: "same-height-ignore",
                leftEdgeClass: "same-height-left",
                rightEdgeClass: "same-height-right",
                elements: ">*",
                flexible: !1,
                multiLine: !1,
                useMinHeight: !1,
                biggestHeight: !1
            }, s);
            return this.each(function() {
                function e() {
                    l.css(n.useMinHeight && r ? "minHeight" : "height", ""), n.multiLine ? t(l, n) : i(l, a, n)
                }
                var s, o, a = jQuery(this),
                    l = a.find(n.elements).not("." + n.skipClass);
                if (l.length) {
                    e();
                    var d = function() {
                        o || (o = !0, e(), clearTimeout(s), s = setTimeout(function() {
                            e(), setTimeout(function() {
                                o = !1
                            }, 10)
                        }, 100))
                    };
                    n.flexible && jQuery(window).bind("resize orientationchange fontresize", d), jQuery(window).bind("load", d)
                }
            })
        };
        var r = void 0 !== document.documentElement.style.maxHeight
    }(jQuery), jQuery.onFontResize = function(e) {
        return jQuery(function() {
            var t = "font-resize-frame-" + Math.floor(1e3 * Math.random()),
                s = jQuery("<iframe>").attr("id", t).addClass("font-resize-helper");
            if (s.css({
                    width: "100em",
                    height: "10px",
                    position: "absolute",
                    borderWidth: 0,
                    top: "-9999px",
                    left: "-9999px"
                }).appendTo("body"), window.attachEvent && !window.addEventListener) s.bind("resize", function() {
                e.onFontResize.trigger(s[0].offsetWidth / 100)
            });
            else {
                var i = s[0].contentWindow.document;
                i.open(), i.write('<script>window.onload = function(){var em = parent.jQuery("#' + t + '")[0];window.onresize = function(){if(parent.jQuery.onFontResize){parent.jQuery.onFontResize.trigger(em.offsetWidth / 100);}}};<\/script>'), i.close()
            }
            jQuery.onFontResize.initialSize = s[0].offsetWidth / 100
        }), {
            trigger: function(e) {
                jQuery(window).trigger("fontresize", [e])
            }
        }
    }(jQuery), function(e) {
        function t(t) {
            this.options = e.extend({
                activeClass: "active",
                mask: "div.slides-mask",
                slider: ">ul",
                slides: ">li",
                btnPrev: ".btn-prev",
                btnNext: ".btn-next",
                pagerLinks: "ul.pager > li",
                generatePagination: !1,
                pagerList: "<ul>",
                pagerListItem: '<li><a href="#"></a></li>',
                pagerListItemText: "a",
                galleryReadyClass: "gallery-js-ready",
                currentNumber: "span.current-num",
                totalNumber: "span.total-num",
                maskAutoSize: !1,
                autoRotation: !1,
                pauseOnHover: !1,
                stretchSlideToMask: !1,
                switchTime: 3e3,
                animSpeed: 500,
                handleTouch: !0,
                swipeThreshold: 15,
                vertical: !1
            }, t), this.init()
        }
        t.prototype = {
            init: function() {
                this.options.holder && (this.findElements(), this.attachEvents(), this.makeCallback("onInit", this))
            },
            findElements: function() {
                this.holder = jQuery(this.options.holder).addClass(this.options.galleryReadyClass), this.mask = this.holder.find(this.options.mask), this.slider = this.mask.find(this.options.slider), this.slides = this.slider.find(this.options.slides), this.btnPrev = this.holder.find(this.options.btnPrev), this.btnNext = this.holder.find(this.options.btnNext), this.currentNumber = this.holder.find(this.options.currentNumber), this.totalNumber = this.holder.find(this.options.totalNumber), "string" == typeof this.options.generatePagination ? this.pagerLinks = this.buildPagination() : this.pagerLinks = this.holder.find(this.options.pagerLinks), this.sizeProperty = this.options.vertical ? "height" : "width", this.positionProperty = this.options.vertical ? "top" : "left", this.animProperty = this.options.vertical ? "marginTop" : "marginLeft", this.slideSize = this.slides[this.sizeProperty](), this.currentIndex = 0, this.prevIndex = 0, this.options.maskAutoSize = !this.options.vertical && this.options.maskAutoSize, this.options.vertical && this.mask.css({
                    height: this.slides.innerHeight()
                }), this.options.maskAutoSize && this.mask.css({
                    height: this.slider.height()
                }), this.slider.css({
                    position: "relative",
                    height: this.options.vertical ? this.slideSize * this.slides.length : "100%"
                }), this.slides.css({
                    position: "absolute"
                }).css(this.positionProperty, -9999).eq(this.currentIndex).css(this.positionProperty, 0), this.refreshState()
            },
            buildPagination: function() {
                var e = jQuery();
                if (this.pagerHolder || (this.pagerHolder = this.holder.find(this.options.generatePagination)), this.pagerHolder.length) {
                    this.pagerHolder.empty(), this.pagerList = jQuery(this.options.pagerList).appendTo(this.pagerHolder);
                    for (var t = 0; t < this.slides.length; t++) jQuery(this.options.pagerListItem).appendTo(this.pagerList).find(this.options.pagerListItemText).text(t + 1);
                    e = this.pagerList.children()
                }
                return e
            },
            attachEvents: function() {
                var e = this;
                this.btnPrev.length && (this.btnPrevHandler = function(t) {
                    t.preventDefault(), e.prevSlide()
                }, this.btnPrev.click(this.btnPrevHandler)), this.btnNext.length && (this.btnNextHandler = function(t) {
                    t.preventDefault(), e.nextSlide()
                }, this.btnNext.click(this.btnNextHandler)), this.pagerLinks.length && (this.pagerLinksHandler = function(t) {
                    t.preventDefault(), e.numSlide(e.pagerLinks.index(t.currentTarget))
                }, this.pagerLinks.click(this.pagerLinksHandler)), this.options.pauseOnHover && (this.hoverHandler = function() {
                    clearTimeout(e.timer)
                }, this.leaveHandler = function() {
                    e.autoRotate()
                }, this.holder.bind({
                    mouseenter: this.hoverHandler,
                    mouseleave: this.leaveHandler
                })), this.resizeHandler = function() {
                    e.animating || (e.options.stretchSlideToMask && e.resizeSlides(), e.resizeHolder(), e.setSlidesPosition(e.currentIndex))
                }, jQuery(window).bind("load resize orientationchange", this.resizeHandler), e.options.stretchSlideToMask && e.resizeSlides(), this.options.handleTouch && window.Hammer && this.mask.length && this.slides.length > 1 && s && (this.swipeHandler = new Hammer.Manager(this.mask[0]), this.swipeHandler.add(new Hammer.Pan({
                    direction: e.options.vertical ? Hammer.DIRECTION_VERTICAL : Hammer.DIRECTION_HORIZONTAL,
                    threshold: e.options.swipeThreshold
                })), this.swipeHandler.on("panstart", function() {
                    e.animating ? e.swipeHandler.stop() : clearTimeout(e.timer)
                }).on("panmove", function(t) {
                    e.swipeOffset = -e.slideSize + t[e.options.vertical ? "deltaY" : "deltaX"], e.slider.css(e.animProperty, e.swipeOffset), clearTimeout(e.timer)
                }).on("panend", function(t) {
                    if (t.distance > e.options.swipeThreshold) t.offsetDirection === Hammer.DIRECTION_RIGHT || t.offsetDirection === Hammer.DIRECTION_DOWN ? e.nextSlide() : e.prevSlide();
                    else {
                        var s = {};
                        s[e.animProperty] = -e.slideSize, e.slider.animate(s, {
                            duration: e.options.animSpeed
                        }), e.autoRotate()
                    }
                    e.swipeOffset = 0
                })), this.autoRotate(), this.resizeHolder(), this.setSlidesPosition(this.currentIndex)
            },
            resizeSlides: function() {
                this.slideSize = this.mask[this.options.vertical ? "height" : "width"](), this.slides.css(this.sizeProperty, this.slideSize)
            },
            resizeHolder: function() {
                this.options.maskAutoSize && this.mask.css({
                    height: this.slides.eq(this.currentIndex).outerHeight(!0)
                })
            },
            prevSlide: function() {
                !this.animating && this.slides.length > 1 && (this.direction = -1, this.prevIndex = this.currentIndex, this.currentIndex > 0 ? this.currentIndex-- : this.currentIndex = this.slides.length - 1, this.switchSlide())
            },
            nextSlide: function(e) {
                !this.animating && this.slides.length > 1 && (this.direction = 1, this.prevIndex = this.currentIndex, this.currentIndex < this.slides.length - 1 ? this.currentIndex++ : this.currentIndex = 0, this.switchSlide())
            },
            numSlide: function(e) {
                !this.animating && this.currentIndex !== e && this.slides.length > 1 && (this.direction = e > this.currentIndex ? 1 : -1, this.prevIndex = this.currentIndex, this.currentIndex = e, this.switchSlide())
            },
            preparePosition: function() {
                this.setSlidesPosition(this.prevIndex, this.direction < 0 ? this.currentIndex : null, this.direction > 0 ? this.currentIndex : null, this.direction)
            },
            setSlidesPosition: function(e, t, s, i) {
                if (this.slides.length > 1) {
                    var n = "number" == typeof t ? t : e > 0 ? e - 1 : this.slides.length - 1,
                        o = "number" == typeof s ? s : e < this.slides.length - 1 ? e + 1 : 0;
                    if (this.slider.css(this.animProperty, this.swipeOffset ? this.swipeOffset : -this.slideSize), this.slides.css(this.positionProperty, -9999).eq(e).css(this.positionProperty, this.slideSize), n === o && "number" == typeof i) {
                        var r = i > 0 ? 2 * this.slideSize : 0;
                        this.slides.eq(o).css(this.positionProperty, r)
                    } else this.slides.eq(n).css(this.positionProperty, 0), this.slides.eq(o).css(this.positionProperty, 2 * this.slideSize)
                }
            },
            switchSlide: function() {
                var e = this,
                    t = (this.slides.eq(this.prevIndex), this.slides.eq(this.currentIndex));
                this.animating = !0, this.options.maskAutoSize && this.mask.animate({
                    height: t.outerHeight(!0)
                }, {
                    duration: this.options.animSpeed
                });
                var s = {};
                s[this.animProperty] = this.direction > 0 ? 2 * -this.slideSize : 0, this.preparePosition(), this.slider.animate(s, {
                    duration: this.options.animSpeed,
                    complete: function() {
                        e.setSlidesPosition(e.currentIndex), e.animating = !1, e.autoRotate(), e.makeCallback("onChange", e)
                    }
                }), this.refreshState(), this.makeCallback("onBeforeChange", this)
            },
            refreshState: function(e) {
                this.slides.removeClass(this.options.activeClass).eq(this.currentIndex).addClass(this.options.activeClass), this.pagerLinks.removeClass(this.options.activeClass).eq(this.currentIndex).addClass(this.options.activeClass), this.currentNumber.html(this.currentIndex + 1), this.totalNumber.html(this.slides.length), this.holder.toggleClass("not-enough-slides", 1 === this.slides.length)
            },
            autoRotate: function() {
                var e = this;
                clearTimeout(this.timer), this.options.autoRotation && (this.timer = setTimeout(function() {
                    e.nextSlide()
                }, this.options.switchTime))
            },
            makeCallback: function(e) {
                if ("function" == typeof this.options[e]) {
                    var t = Array.prototype.slice.call(arguments);
                    t.shift(), this.options[e].apply(this, t)
                }
            },
            destroy: function() {
                this.btnPrev.unbind("click", this.btnPrevHandler), this.btnNext.unbind("click", this.btnNextHandler), this.pagerLinks.unbind("click", this.pagerLinksHandler), this.holder.unbind("mouseenter", this.hoverHandler), this.holder.unbind("mouseleave", this.leaveHandler), jQuery(window).unbind("load resize orientationchange", this.resizeHandler), clearTimeout(this.timer), this.swipeHandler && this.swipeHandler.destroy(), this.holder.removeClass(this.options.galleryReadyClass), this.slider.add(this.slides).removeAttr("style"), "string" == typeof this.options.generatePagination && this.pagerHolder.empty()
            }
        };
        var s = /Windows Phone/.test(navigator.userAgent) || "ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch;
        e.fn.scrollAbsoluteGallery = function(s) {
            return this.each(function() {
                jQuery(this).data("ScrollAbsoluteGallery", new t(e.extend(s, {
                    holder: this
                })))
            })
        }
    }(jQuery), function(e) {
        "function" == typeof define && define.amd ? define(["jquery"], e) : "object" == typeof exports ? module.exports = e(require("jquery")) : e(jQuery)
    }(function(e) {
        "use strict";

        function t(t, s) {
            this.element = t, this.options = e.extend({}, n, s), this.init()
        }

        function s(e) {
            if (!jQuery(e.target).parents().hasClass("jq-selectbox") && "OPTION" != e.target.nodeName && jQuery("div.jq-selectbox.opened").length) {
                var t = jQuery("div.jq-selectbox.opened"),
                    s = jQuery("div.jq-selectbox__search input", t),
                    n = jQuery("div.jq-selectbox__dropdown", t);
                t.find("select").data("_" + i).options.onSelectClosed.call(t), s.length && s.val("").keyup(), n.hide().find("li.sel").addClass("selected"), t.removeClass("focused opened dropup dropdown")
            }
        }
        var i = "styler",
            n = {
                idSuffix: "-styler",
                filePlaceholder: "Файл не выбран",
                fileBrowse: "Обзор...",
                fileNumber: "Выбрано файлов: %s",
                selectPlaceholder: "Выберите...",
                selectSearch: !1,
                selectSearchLimit: 10,
                selectSearchNotFound: "Совпадений не найдено",
                selectSearchPlaceholder: "Поиск...",
                selectVisibleOptions: 0,
                singleSelectzIndex: "100",
                selectSmartPositioning: !0,
                onSelectOpened: function() {},
                onSelectClosed: function() {},
                onFormStyled: function() {}
            };
        t.prototype = {
            init: function() {
                function t() {
                    void 0 !== i.attr("id") && "" !== i.attr("id") && (this.id = i.attr("id") + n.idSuffix), this.title = i.attr("title"), this.classes = i.attr("class"), this.data = i.data()
                }
                var i = jQuery(this.element),
                    n = this.options,
                    o = !(!navigator.userAgent.match(/(iPad|iPhone|iPod)/i) || navigator.userAgent.match(/(Windows\sPhone)/i)),
                    r = !(!navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/(Windows\sPhone)/i));
                if (i.is(":checkbox")) {
                    var a = function() {
                        var e = new t,
                            s = jQuery('<div class="jq-checkbox"><div class="jq-checkbox__div"></div></div>').attr({
                                id: e.id,
                                title: e.title
                            }).addClass(e.classes).data(e.data);
                        i.css({
                            position: "absolute",
                            zIndex: "-1",
                            opacity: 0,
                            margin: 0,
                            padding: 0
                        }).after(s).prependTo(s), s.attr("unselectable", "on").css({
                            "-webkit-user-select": "none",
                            "-moz-user-select": "none",
                            "-ms-user-select": "none",
                            "-o-user-select": "none",
                            "user-select": "none",
                            display: "inline-block",
                            position: "relative",
                            overflow: "hidden"
                        }), i.is(":checked") && s.addClass("checked"), i.is(":disabled") && s.addClass("disabled"), s.click(function(e) {
                            e.preventDefault(), s.is(".disabled") || (i.is(":checked") ? (i.prop("checked", !1), s.removeClass("checked")) : (i.prop("checked", !0), s.addClass("checked")), i.focus().change())
                        }), i.closest("label").add('label[for="' + i.attr("id") + '"]').on("click.styler", function(e) {
                            jQuery(e.target).is("a") || jQuery(e.target).closest(s).length || (s.triggerHandler("click"), e.preventDefault())
                        }), i.on("change.styler", function() {
                            i.is(":checked") ? s.addClass("checked") : s.removeClass("checked")
                        }).on("keydown.styler", function(e) {
                            32 == e.which && s.click()
                        }).on("focus.styler", function() {
                            s.is(".disabled") || s.addClass("focused")
                        }).on("blur.styler", function() {
                            s.removeClass("focused")
                        })
                    };
                    a(), i.on("refresh", function() {
                        i.closest("label").add('label[for="' + i.attr("id") + '"]').off(".styler"), i.off(".styler").parent().before(i).remove(), a()
                    })
                } else if (i.is(":radio")) {
                    var l = function() {
                        var s = new t,
                            n = jQuery('<div class="jq-radio"><div class="jq-radio__div"></div></div>').attr({
                                id: s.id,
                                title: s.title
                            }).addClass(s.classes).data(s.data);
                        i.css({
                            position: "absolute",
                            zIndex: "-1",
                            opacity: 0,
                            margin: 0,
                            padding: 0
                        }).after(n).prependTo(n), n.attr("unselectable", "on").css({
                            "-webkit-user-select": "none",
                            "-moz-user-select": "none",
                            "-ms-user-select": "none",
                            "-o-user-select": "none",
                            "user-select": "none",
                            display: "inline-block",
                            position: "relative"
                        }), i.is(":checked") && n.addClass("checked"), i.is(":disabled") && n.addClass("disabled"), e.fn.commonParents = function() {
                            var e = this;
                            return e.first().parents().filter(function() {
                                return jQuery(this).find(e).length === e.length
                            })
                        }, e.fn.commonParent = function() {
                            return jQuery(this).commonParents().first()
                        }, n.click(function(e) {
                            if (e.preventDefault(), !n.is(".disabled")) {
                                var t = jQuery('input[name="' + i.attr("name") + '"]');
                                t.commonParent().find(t).prop("checked", !1).parent().removeClass("checked"), i.prop("checked", !0).parent().addClass("checked"), i.focus().change()
                            }
                        }), i.closest("label").add('label[for="' + i.attr("id") + '"]').on("click.styler", function(e) {
                            jQuery(e.target).is("a") || jQuery(e.target).closest(n).length || (n.triggerHandler("click"), e.preventDefault())
                        }), i.on("change.styler", function() {
                            i.parent().addClass("checked")
                        }).on("focus.styler", function() {
                            n.is(".disabled") || n.addClass("focused")
                        }).on("blur.styler", function() {
                            n.removeClass("focused")
                        })
                    };
                    l(), i.on("refresh", function() {
                        i.closest("label").add('label[for="' + i.attr("id") + '"]').off(".styler"), i.off(".styler").parent().before(i).remove(), l()
                    })
                } else if (i.is(":file")) {
                    i.css({
                        position: "absolute",
                        top: 0,
                        right: 0,
                        margin: 0,
                        padding: 0,
                        opacity: 0,
                        fontSize: "100px"
                    });
                    var d = function() {
                        var e = new t,
                            s = i.data("placeholder");
                        void 0 === s && (s = n.filePlaceholder);
                        var o = i.data("browse");
                        void 0 !== o && "" !== o || (o = n.fileBrowse);
                        var r = jQuery('<div class="jq-file"><div class="jq-file__name">' + s + '</div><div class="jq-file__browse">' + o + "</div></div>").css({
                            display: "inline-block",
                            position: "relative",
                            overflow: "hidden"
                        }).attr({
                            id: e.id,
                            title: e.title
                        }).addClass(e.classes).data(e.data);
                        i.after(r).appendTo(r), i.is(":disabled") && r.addClass("disabled"), i.on("change.styler", function() {
                            var e = i.val(),
                                t = jQuery("div.jq-file__name", r);
                            if (i.is("[multiple]")) {
                                e = "";
                                var o = i[0].files.length;
                                if (o > 0) {
                                    var a = i.data("number");
                                    void 0 === a && (a = n.fileNumber), a = a.replace("%s", o), e = a
                                }
                            }
                            t.text(e.replace(/.+[\\\/]/, "")), "" === e ? (t.text(s), r.removeClass("changed")) : r.addClass("changed")
                        }).on("focus.styler", function() {
                            r.addClass("focused")
                        }).on("blur.styler", function() {
                            r.removeClass("focused")
                        }).on("click.styler", function() {
                            r.removeClass("focused")
                        })
                    };
                    d(), i.on("refresh", function() {
                        i.off(".styler").parent().before(i).remove(), d()
                    })
                } else if (i.is('input[type="number"]')) {
                    var c = function() {
                        var s = new t,
                            n = jQuery('<div class="jq-number"><div class="jq-number__spin minus"></div><div class="jq-number__spin plus"></div></div>').attr({
                                id: s.id,
                                title: s.title
                            }).addClass(s.classes).data(s.data);
                        i.after(n).prependTo(n).wrap('<div class="jq-number__field"></div>'), i.is(":disabled") && n.addClass("disabled");
                        var o, r, a, l = null,
                            d = null;
                        void 0 !== i.attr("min") && (o = i.attr("min")), void 0 !== i.attr("max") && (r = i.attr("max")), a = void 0 !== i.attr("step") && e.isNumeric(i.attr("step")) ? Number(i.attr("step")) : Number(1);
                        var c = function(t) {
                            var s, n = i.val();
                            e.isNumeric(n) || (n = 0, i.val("0")), t.is(".minus") ? s = Number(n) - a : t.is(".plus") && (s = Number(n) + a);
                            var l = (a.toString().split(".")[1] || []).length;
                            if (l > 0) {
                                for (var d = "1"; d.length <= l;) d += "0";
                                s = Math.round(s * d) / d
                            }
                            e.isNumeric(o) && e.isNumeric(r) ? s >= o && s <= r && i.val(s) : e.isNumeric(o) && !e.isNumeric(r) ? s >= o && i.val(s) : !e.isNumeric(o) && e.isNumeric(r) ? s <= r && i.val(s) : i.val(s)
                        };
                        n.is(".disabled") || (n.on("mousedown", "div.jq-number__spin", function() {
                            var e = jQuery(this);
                            c(e), l = setTimeout(function() {
                                d = setInterval(function() {
                                    c(e)
                                }, 40)
                            }, 350)
                        }).on("mouseup mouseout", "div.jq-number__spin", function() {
                            clearTimeout(l), clearInterval(d)
                        }).on("mouseup", "div.jq-number__spin", function() {
                            i.change()
                        }), i.on("focus.styler", function() {
                            n.addClass("focused")
                        }).on("blur.styler", function() {
                            n.removeClass("focused")
                        }))
                    };
                    c(), i.on("refresh", function() {
                        i.off(".styler").closest(".jq-number").before(i).remove(), c()
                    })
                } else if (i.is("select")) {
                    var u = function() {
                        function a(e) {
                            e.off("mousewheel DOMMouseScroll").on("mousewheel DOMMouseScroll", function(e) {
                                var t = null;
                                "mousewheel" == e.type ? t = -1 * e.originalEvent.wheelDelta : "DOMMouseScroll" == e.type && (t = 40 * e.originalEvent.detail), t && (e.stopPropagation(), e.preventDefault(), jQuery(this).scrollTop(t + jQuery(this).scrollTop()))
                            })
                        }

                        function l() {
                            for (var e = 0; e < d.length; e++) {
                                var t = d.eq(e),
                                    s = "",
                                    i = "",
                                    o = "",
                                    r = "",
                                    a = "",
                                    l = "",
                                    u = "",
                                    h = "",
                                    p = "";
                                t.prop("selected") && (i = "selected sel"), t.is(":disabled") && (i = "disabled"), t.is(":selected:disabled") && (i = "selected sel disabled"), void 0 !== t.attr("id") && "" !== t.attr("id") && (r = ' id="' + t.attr("id") + n.idSuffix + '"'), void 0 !== t.attr("title") && "" !== d.attr("title") && (a = ' title="' + t.attr("title") + '"'), void 0 !== t.attr("class") && (u = " " + t.attr("class"), p = ' data-jqfs-class="' + t.attr("class") + '"');
                                var f = t.data();
                                for (var m in f) "" !== f[m] && (l += " data-" + m + '="' + f[m] + '"');
                                i + u !== "" && (o = ' class="' + i + u + '"'), s = "<li" + p + l + o + a + r + ">" + t.html() + "</li>", t.parent().is("optgroup") && (void 0 !== t.parent().attr("class") && (h = " " + t.parent().attr("class")), s = "<li" + p + l + ' class="' + i + u + " option" + h + '"' + a + r + ">" + t.html() + "</li>", t.is(":first-child") && (s = '<li class="optgroup' + h + '">' + t.parent().attr("label") + "</li>" + s)), c += s
                            }
                        }
                        var d = jQuery("option", i),
                            c = "";
                        if (i.is("[multiple]")) {
                            if (r || o) return;
                            ! function() {
                                var s = new t,
                                    n = jQuery('<div class="jq-select-multiple jqselect"></div>').css({
                                        display: "inline-block",
                                        position: "relative"
                                    }).attr({
                                        id: s.id,
                                        title: s.title
                                    }).addClass(s.classes).data(s.data);
                                i.css({
                                    margin: 0,
                                    padding: 0
                                }).after(n), l(), n.append("<ul>" + c + "</ul>");
                                var o = jQuery("ul", n).css({
                                        position: "relative",
                                        "overflow-x": "hidden",
                                        "-webkit-overflow-scrolling": "touch"
                                    }),
                                    r = jQuery("li", n).attr("unselectable", "on"),
                                    u = i.attr("size"),
                                    h = o.outerHeight(),
                                    p = r.outerHeight();
                                void 0 !== u && u > 0 ? o.css({
                                        height: p * u
                                    }) : o.css({
                                        height: 4 * p
                                    }), h > n.height() && (o.css("overflowY", "scroll"), a(o), r.filter(".selected").length && o.scrollTop(o.scrollTop() + r.filter(".selected").position().top)), i.prependTo(n).css({
                                    position: "absolute",
                                    left: 0,
                                    top: 0,
                                    width: "100%",
                                    height: "100%",
                                    opacity: 0
                                }), i.is(":disabled") ? (n.addClass("disabled"), d.each(function() {
                                        jQuery(this).is(":selected") && r.eq(jQuery(this).index()).addClass("selected")
                                    })) : (r.filter(":not(.disabled):not(.optgroup)").click(function(e) {
                                        i.focus();
                                        var t = jQuery(this);
                                        if (e.ctrlKey || e.metaKey || t.addClass("selected"), e.shiftKey || t.addClass("first"), e.ctrlKey || e.metaKey || e.shiftKey || t.siblings().removeClass("selected first"), (e.ctrlKey || e.metaKey) && (t.is(".selected") ? t.removeClass("selected first") : t.addClass("selected first"), t.siblings().removeClass("first")), e.shiftKey) {
                                            var s = !1,
                                                n = !1;
                                            t.siblings().removeClass("selected").siblings(".first").addClass("selected"), t.prevAll().each(function() {
                                                jQuery(this).is(".first") && (s = !0)
                                            }), t.nextAll().each(function() {
                                                jQuery(this).is(".first") && (n = !0)
                                            }), s && t.prevAll().each(function() {
                                                if (jQuery(this).is(".selected")) return !1;
                                                jQuery(this).not(".disabled, .optgroup").addClass("selected")
                                            }), n && t.nextAll().each(function() {
                                                if (jQuery(this).is(".selected")) return !1;
                                                jQuery(this).not(".disabled, .optgroup").addClass("selected")
                                            }), 1 == r.filter(".selected").length && t.addClass("first")
                                        }
                                        d.prop("selected", !1), r.filter(".selected").each(function() {
                                            var e = jQuery(this),
                                                t = e.index();
                                            e.is(".option") && (t -= e.prevAll(".optgroup").length), d.eq(t).prop("selected", !0)
                                        }), i.change()
                                    }), d.each(function(e) {
                                        jQuery(this).data("optionIndex", e)
                                    }), i.on("change.styler", function() {
                                        r.removeClass("selected");
                                        var t = [];
                                        d.filter(":selected").each(function() {
                                            t.push(jQuery(this).data("optionIndex"))
                                        }), r.not(".optgroup").filter(function(s) {
                                            return e.inArray(s, t) > -1
                                        }).addClass("selected")
                                    }).on("focus.styler", function() {
                                        n.addClass("focused")
                                    }).on("blur.styler", function() {
                                        n.removeClass("focused")
                                    }), h > n.height() && i.on("keydown.styler", function(e) {
                                        38 != e.which && 37 != e.which && 33 != e.which || o.scrollTop(o.scrollTop() + r.filter(".selected").position().top - p), 40 != e.which && 39 != e.which && 34 != e.which || o.scrollTop(o.scrollTop() + r.filter(".selected:last").position().top - o.innerHeight() + 2 * p)
                                    }))
                            }()
                        } else ! function() {
                            var e = new t,
                                r = "",
                                u = i.data("placeholder"),
                                h = i.data("search"),
                                p = i.data("search-limit"),
                                f = i.data("search-not-found"),
                                m = i.data("search-placeholder"),
                                y = i.data("z-index"),
                                v = i.data("smart-positioning");
                            void 0 === u && (u = n.selectPlaceholder), void 0 !== h && "" !== h || (h = n.selectSearch), void 0 !== p && "" !== p || (p = n.selectSearchLimit), void 0 !== f && "" !== f || (f = n.selectSearchNotFound), void 0 === m && (m = n.selectSearchPlaceholder), void 0 !== y && "" !== y || (y = n.singleSelectzIndex), void 0 !== v && "" !== v || (v = n.selectSmartPositioning);
                            var g = jQuery('<div class="jq-selectbox jqselect"><div class="jq-selectbox__select" style="position: relative"><div class="jq-selectbox__select-text"></div><div class="jq-selectbox__trigger"><div class="jq-selectbox__trigger-arrow"></div></div></div></div>').css({
                                display: "inline-block",
                                position: "relative",
                                zIndex: y
                            }).attr({
                                id: e.id,
                                title: e.title
                            }).addClass(e.classes).data(e.data);
                            i.css({
                                margin: 0,
                                padding: 0
                            }).after(g).prependTo(g);
                            var j = jQuery("div.jq-selectbox__select", g),
                                Q = jQuery("div.jq-selectbox__select-text", g),
                                b = d.filter(":selected");
                            l(), h && (r = '<div class="jq-selectbox__search"><input type="search" autocomplete="off" placeholder="' + m + '"></div><div class="jq-selectbox__not-found">' + f + "</div>");
                            var w = jQuery('<div class="jq-selectbox__dropdown" style="position: absolute">' + r + '<ul style="position: relative; list-style: none; overflow: auto; overflow-x: hidden">' + c + "</ul></div>");
                            g.append(w);
                            var x = jQuery("ul", w),
                                _ = jQuery("li", w),
                                C = jQuery("input", w),
                                k = jQuery("div.jq-selectbox__not-found", w).hide();
                            _.length < p && C.parent().hide(), "" === d.first().text() && d.first().is(":selected") ? Q.text(u).addClass("placeholder") : Q.text(b.text());
                            var S = 0,
                                T = 0;
                            if (_.css({
                                    display: "inline-block"
                                }), _.each(function() {
                                    var e = jQuery(this);
                                    e.innerWidth() > S && (S = e.innerWidth(), T = e.width())
                                }), _.css({
                                    display: ""
                                }), Q.is(".placeholder") && Q.width() > S) Q.width(Q.width());
                            else {
                                var M = g.clone().appendTo("body").width("auto"),
                                    P = M.outerWidth();
                                M.remove(), P == g.outerWidth() && Q.width(T)
                            }
                            S > g.width() && w.width(S), "" === d.first().text() && "" !== i.data("placeholder") && _.first().hide(), i.css({
                                position: "absolute",
                                left: 0,
                                top: 0,
                                width: "100%",
                                height: "100%",
                                opacity: 0
                            });
                            var H = g.outerHeight(!0),
                                q = C.parent().outerHeight(!0),
                                I = x.css("max-height"),
                                N = _.filter(".selected");
                            N.length < 1 && _.first().addClass("selected sel"), void 0 === _.data("li-height") && _.data("li-height", _.outerHeight());
                            var z = w.css("top");
                            if ("auto" == w.css("left") && w.css({
                                    left: 0
                                }), "auto" == w.css("top") && (w.css({
                                    top: H
                                }), z = H), w.hide(), N.length && (d.first().text() != b.text() && g.addClass("changed"), g.data("jqfs-class", N.data("jqfs-class")), g.addClass(N.data("jqfs-class"))), i.is(":disabled")) return g.addClass("disabled"), !1;
                            j.click(function() {
                                if (jQuery("div.jq-selectbox").filter(".opened").length && n.onSelectClosed.call(jQuery("div.jq-selectbox").filter(".opened")), i.focus(), !o) {
                                    var e = jQuery(window),
                                        t = _.data("li-height"),
                                        s = g.offset().top,
                                        r = e.height() - H - (s - e.scrollTop()),
                                        l = i.data("visible-options");
                                    void 0 !== l && "" !== l || (l = n.selectVisibleOptions);
                                    var c = 5 * t,
                                        u = t * l;
                                    l > 0 && l < 6 && (c = u), 0 === l && (u = "auto");
                                    var h = function() {
                                        w.height("auto").css({
                                            bottom: "auto",
                                            top: z
                                        });
                                        var e = function() {
                                            x.css("max-height", Math.floor((r - 20 - q) / t) * t)
                                        };
                                        e(), x.css("max-height", u), "none" != I && x.css("max-height", I), r < w.outerHeight() + 20 && e()
                                    };
                                    !0 === v || 1 === v ? r > c + q + 20 ? (h(), g.removeClass("dropup").addClass("dropdown")) : (function() {
                                                w.height("auto").css({
                                                    top: "auto",
                                                    bottom: z
                                                });
                                                var i = function() {
                                                    x.css("max-height", Math.floor((s - e.scrollTop() - 20 - q) / t) * t)
                                                };
                                                i(), x.css("max-height", u), "none" != I && x.css("max-height", I), s - e.scrollTop() - 20 < w.outerHeight() + 20 && i()
                                            }(), g.removeClass("dropdown").addClass("dropup")) : !1 !== v && 0 !== v || r > c + q + 20 && (h(), g.removeClass("dropup").addClass("dropdown")), g.offset().left + w.outerWidth() > e.width() && w.css({
                                        left: "auto",
                                        right: 0
                                    }), jQuery("div.jqselect").css({
                                        zIndex: y - 1
                                    }).removeClass("opened"), g.css({
                                        zIndex: y
                                    }), w.is(":hidden") ? (jQuery("div.jq-selectbox__dropdown:visible").hide(), w.show(), g.addClass("opened focused"), n.onSelectOpened.call(g)) : (w.hide(), g.removeClass("opened dropup dropdown"), jQuery("div.jq-selectbox").filter(".opened").length && n.onSelectClosed.call(g)), C.length && (C.val("").keyup(), k.hide(), C.keyup(function() {
                                        var e = jQuery(this).val();
                                        _.each(function() {
                                            jQuery(this).html().match(new RegExp(".*?" + e + ".*?", "i")) ? jQuery(this).show() : jQuery(this).hide()
                                        }), "" === d.first().text() && "" !== i.data("placeholder") && _.first().hide(), _.filter(":visible").length < 1 ? k.show() : k.hide()
                                    })), _.filter(".selected").length && ("" === i.val() ? x.scrollTop(0) : (x.innerHeight() / t % 2 != 0 && (t /= 2), x.scrollTop(x.scrollTop() + _.filter(".selected").position().top - x.innerHeight() / 2 + t))), a(x)
                                }
                            }), _.hover(function() {
                                jQuery(this).siblings().removeClass("selected")
                            });
                            var $ = _.filter(".selected").text();
                            _.filter(":not(.disabled):not(.optgroup)").click(function() {
                                i.focus();
                                var e = jQuery(this),
                                    t = e.text();
                                if (!e.is(".selected")) {
                                    var s = e.index();
                                    s -= e.prevAll(".optgroup").length, e.addClass("selected sel").siblings().removeClass("selected sel"), d.prop("selected", !1).eq(s).prop("selected", !0), $ = t, Q.text(t), g.data("jqfs-class") && g.removeClass(g.data("jqfs-class")), g.data("jqfs-class", e.data("jqfs-class")), g.addClass(e.data("jqfs-class")), i.change()
                                }
                                w.hide(), g.removeClass("opened dropup dropdown"), n.onSelectClosed.call(g)
                            }), w.mouseout(function() {
                                jQuery("li.sel", w).addClass("selected")
                            }), i.on("change.styler", function() {
                                Q.text(d.filter(":selected").text()).removeClass("placeholder"), _.removeClass("selected sel").not(".optgroup").eq(i[0].selectedIndex).addClass("selected sel"), d.first().text() != _.filter(".selected").text() ? g.addClass("changed") : g.removeClass("changed")
                            }).on("focus.styler", function() {
                                g.addClass("focused"), jQuery("div.jqselect").not(".focused").removeClass("opened dropup dropdown").find("div.jq-selectbox__dropdown").hide()
                            }).on("blur.styler", function() {
                                g.removeClass("focused")
                            }).on("keydown.styler keyup.styler", function(e) {
                                var t = _.data("li-height");
                                "" === i.val() ? Q.text(u).addClass("placeholder") : Q.text(d.filter(":selected").text()), _.removeClass("selected sel").not(".optgroup").eq(i[0].selectedIndex).addClass("selected sel"), 38 != e.which && 37 != e.which && 33 != e.which && 36 != e.which || ("" === i.val() ? x.scrollTop(0) : x.scrollTop(x.scrollTop() + _.filter(".selected").position().top)), 40 != e.which && 39 != e.which && 34 != e.which && 35 != e.which || x.scrollTop(x.scrollTop() + _.filter(".selected").position().top - x.innerHeight() + t), 13 == e.which && (e.preventDefault(), w.hide(), g.removeClass("opened dropup dropdown"), n.onSelectClosed.call(g))
                            }).on("keydown.styler", function(e) {
                                32 == e.which && (e.preventDefault(), j.click())
                            }), s.registered || (jQuery(document).on("click", s), s.registered = !0)
                        }()
                    };
                    u(), i.on("refresh", function() {
                        i.off(".styler").parent().before(i).remove(), u()
                    })
                } else i.is(":reset") && i.on("click", function() {
                    setTimeout(function() {
                        i.closest("form").find("input, select").trigger("refresh")
                    }, 1)
                })
            },
            destroy: function() {
                var e = jQuery(this.element);
                e.is(":checkbox") || e.is(":radio") ? (e.removeData("_" + i).off(".styler refresh").removeAttr("style").parent().before(e).remove(), e.closest("label").add('label[for="' + e.attr("id") + '"]').off(".styler")) : e.is('input[type="number"]') ? e.removeData("_" + i).off(".styler refresh").closest(".jq-number").before(e).remove() : (e.is(":file") || e.is("select")) && e.removeData("_" + i).off(".styler refresh").removeAttr("style").parent().before(e).remove()
            }
        }, e.fn[i] = function(s) {
            var n = arguments;
            if (void 0 === s || "object" == typeof s) return this.each(function() {
                e.data(this, "_" + i) || e.data(this, "_" + i, new t(this, s))
            }).promise().done(function() {
                var e = jQuery(this[0]).data("_" + i);
                e && e.options.onFormStyled.call()
            }), this;
            if ("string" == typeof s && "_" !== s[0] && "init" !== s) {
                var o;
                return this.each(function() {
                    var r = e.data(this, "_" + i);
                    r instanceof t && "function" == typeof r[s] && (o = r[s].apply(r, Array.prototype.slice.call(n, 1)))
                }), void 0 !== o ? o : this
            }
        }, s.registered = !1
    }), jQuery(".wpcf7-form").length && jQuery('[name="your-phone"]').length && jQuery('[name="your-phone"]').mask("000-000-0000"), jQuery(".survey-cta").length) {
    $leftScroll = jQuery(".wrapper-section_select").width(), $leftPosition = $leftScroll;
    var n = jQuery(".wrapper-section_select").length;
    jQuery(".wrapper-section").css("width", $leftPosition), jQuery(".steps-section_fullwidth").css("width", n * $leftPosition);
    var $countsteps = 1,
        $progress = 50;
    jQuery(".stepNext").click(function() {
        $countsteps <= 3 && (jQuery(".steps-section_fullwidth").css("left", -$leftScroll), 3 == $countsteps ? (jQuery(".stepNext").text("Search").addClass("search").removeClass("stepNext"), jQuery(".progress-wrapper_color").css("width", $progress + "%"), $countsteps++, jQuery(".steps").text($countsteps + "/4"), jQuery(".search").click(function() {
                jQuery(".steps-wrapper h3").text("Great, we found 14 lenders for you"), jQuery(".steps-section_middle").hide()
            })) : ($leftScroll += $leftPosition, $countsteps++, jQuery(".steps").text($countsteps + "/4"), jQuery(".progress-wrapper_color").css("width", $progress + "%"), $progress += 25))
    })
}
var $closebutton = jQuery("<span class='closeLink'></span>");
jQuery(".searchform div").append($closebutton), jQuery(".searchform label").click(function() {
    jQuery("header .searchform").addClass("openSearch"), jQuery(window).width() < 769 && jQuery("body").addClass("search-start")
}), jQuery(".closeLink").click(function() {
    jQuery("header .searchform").removeClass("openSearch"), jQuery(window).width() < 769 && jQuery("body").removeClass("search-start")
}), jQuery(".searchform #s").attr("placeholder", "Search LendGenius"), jQuery(document).ready(function() {
    shareList(), jQuery(".fixed-side-item").fixer({
        gap: 72
    })
}), jQuery(window).resize(function() {
    shareList()
}), jQuery(document).ready(function(e) {
    e('.table-funding [data-toggle="modal"]').on("click", function() {
        var t = e("#FundingModal"),
            s = e(this).parents("tr").find(".additional");
        e("#modal_qualifications", t).empty().append(e(".qualifications li", s).clone()), e("#modal_pros", t).empty().append(e(".pros li", s).clone()), e("#modal_cons", t).empty().append(e(".cons li", s).clone()), e(".modal-funding-logo", t).empty().empty().append(e(this).siblings("img").clone())
    })
}), jQuery(document).ready(function(e) {
    var t = e("#carousel-сomparison");
    t.length && (t.carousel({
        interval: 7e3
    }), t.carousel(), t.on("touchstart", function(s) {
        var i = s.originalEvent.touches[0].pageX;
        e(this).one("touchmove", function(e) {
            var s = e.originalEvent.touches[0].pageX;
            Math.floor(i - s) > 5 ? t.carousel("next") : Math.floor(i - s) < -5 && carouselCompgitarison.carousel("prev")
        }), t.on("touchend", function() {
            e(this).off("touchmove")
        })
    }))
}), fixModal(),
    function(e, t) {
        var s = e(t),
            i = {
                gap: 0,
                horizontal: !1,
                isFixed: e.noop
            },
            n = function(e) {
                for (var t, s = ["", "-webkit-", "-moz-", "-ms-", "-o-"]; t = s.pop();)
                    if (e.style.cssText = "position:" + t + "sticky", "" !== e.style.position) return !0;
                return !1
            };
        e.fn.fixer = function(t) {
            t = e.extend({}, i, t);
            var o = t.horizontal,
                r = o ? "left" : "top";
            return this.each(function() {
                var i = this.style,
                    a = e(this),
                    l = a.parent();
                if (n(this)) return void(i[r] = t.gap + "px");
                s.on("scroll", function() {
                    var e = s[o ? "scrollLeft" : "scrollTop"](),
                        n = a[o ? "outerWidth" : "outerHeight"](),
                        d = l.offset()[r],
                        c = l[o ? "outerWidth" : "outerHeight"]();
                    e >= d - t.gap && c + d - t.gap >= e + n ? (i.position = "fixed", i[r] = t.gap + "px", t.isFixed()) : e < d ? (i.position = "absolute", i[r] = 0) : (i.position = "absolute", i[r] = c - n + "px")
                }).resize()
            })
        }
    }(jQuery, this), jQuery(document).ready(function(e) {
    function t(e) {
        e.wrap("<div class='table-wrapper' />");
        var t = e.clone();
        t.find("td:not(:first-child), th:not(:first-child)").css("display", "none"), t.removeClass("responsive"), e.closest(".table-wrapper").append(t), t.wrap("<div class='pinned' />"), e.wrap("<div class='scrollable' />"), i(e, t)
    }

    function s(e) {
        e.closest(".table-wrapper").find(".pinned").remove(), e.unwrap(), e.unwrap()
    }

    function i(t, s) {
        var i = t.find("tr"),
            n = s.find("tr"),
            o = [];
        i.each(function(t) {
            e(this).find("th, td").each(function() {
                var s = e(this).outerHeight(!0);
                o[t] = o[t] || 0, s > o[t] && (o[t] = s)
            })
        }), n.each(function(t) {
            e(this).height(o[t])
        })
    }
    var n = !1,
        o = function() {
            if (e(window).width() < 767 && !n) return n = !0, e("table.responsive").each(function(s, i) {
                t(e(i))
            }), !0;
            n && e(window).width() > 767 && (n = !1, e("table.responsive").each(function(t, i) {
                s(e(i))
            }))
        };
    e(window).load(o), e(window).on("redraw", function() {
        n = !1, o()
    }), e(window).on("resize", o)
});