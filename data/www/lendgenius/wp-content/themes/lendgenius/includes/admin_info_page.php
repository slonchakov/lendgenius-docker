<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 25.10.16
 * Time: 15:49
 */

class lg_info_page {

    function __construct() {
        add_action( 'admin_menu', array( $this, 'info_page_menu' ) );
    }

    function info_page_menu() {
        add_options_page(
            'LG Info',
            'LG Info',
            'manage_options',
            'lg-info',
            array(
                $this,
                'info_page_content'
            )
        );
    }

    function  info_page_content() {
        ?>
        <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/assets/styles/icomoon.min.css?ver=<?php echo get_option( 'sts_script_version' ); ?>">
        <style>
            #show-icon  [class^="icomoon-"]{
                font-size: 4em;
                margin-right: 5px;
            }
            #show-icon  .ico-set-holder{
                display: flex;
                align-items: center;
                flex-wrap: wrap;
                border-bottom: 1px solid rgba(0,0,0,0.3);
                margin-bottom: 20px;
            }
            #show-icon .ico-set-holder > div{
                width: 29%;
                padding: 5px;
                display: flex;
                align-items: center;
            }
            #show-icon .ico-set-holder > div span{
                user-select: none;
                display: inline-block;
                margin: 0 5px;
            }
            .ico-gradient:before {
                background: -webkit-gradient(linear,left top,left bottom,from(#76a924),to(#1496f4));
                -webkit-background-clip: text;
                -webkit-text-fill-color: transparent;
            }
            img{
                max-width: 400px;
            }

        </style>
        <h1>LG Info</h1>
        <h3>Shortcodes</h3>
        <hr />
        <table class="form-table">
            <tbody>
            <tr>
                <th colspan="2"><h4>CTA Shortcodes</h4><hr /></th>
            </tr>
            <tr>
                <th scope="row">[CTA-A]</th>
                <td><img class="img-responsive" src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/horizontal-banner-lg.png" alt="Aside Banner"></td>
            </tr>
            <tr>
                <th scope="row">[CTA-B]</th>
                <td><img class="img-responsive" src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/aside-banner.jpg" alt="Aside Banner"></td>
            </tr>
            <tr>
                <th scope="row">[CTA-C]</th>
                <td><img class="img-responsive" src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/big-button-shortcode.png" alt="Aside Banner"></td>
            </tr>
            <tr>
                <th colspan="2"><h4>Form Shortcodes</h4><hr /></th>
            </tr>
            <tr>
                <th>[lead form="2"]</th>
                <td><img src="/wp-content/themes/lendgenius/images/Form2.jpg" alt=""></td>
            </tr>
            <tr>
                <th>[lead form="4"]</th>
                <td><img src="/wp-content/themes/lendgenius/images/Form4.jpg" alt=""></td>
            </tr>
            <tr>
                <th>[lead form="7"]</th>
                <td><img src="/wp-content/themes/lendgenius/images/Form7.jpg" alt=""></td>
            </tr>
            </tbody>
        </table>
        <h3>Icomoon Icons</h3>
        <hr />
        <table class="form-table">
            <tbody>
            <tr id="show-icon">
                <td>
                    <div id="show-icon">
                     <div style="padding: 10px 15px;" class="ico-set-holder">
                        <div><i class="icomoon-documents"></i> <span>-</span> icomoon-documents</div>
                        <div><i class="icomoon-cards"></i> <span>-</span> icomoon-cards</div>
                        <div><i class="icomoon-cash"></i> <span>-</span> icomoon-cash</div>
                        <div><i class="icomoon-documents-calendar"></i> <span>-</span> icomoon-documents-calendar</div>
                        <div><i class="icomoon-dollar-arrows"></i> <span>-</span> icomoon-dollar-arrows</div>
                        <div><i class="icomoon-dollar-noCircle"></i> <span>-</span> icomoon-dollar-noCircle</div>
                        <div><i class="icomoon-equipment"></i>- icomoon-equipment</div>
                        <div><i class="icomoon-idea-dollar"></i>- icomoon-idea-dollar</div>
                        <div><i class="icomoon-multiple-offers"></i> <span>-</span> icomoon-multiple-offers</div>
                        <div><i class="icomoon-phone"></i> <span>-</span> icomoon-phone</div>
                        <div><i class="icomoon-case"></i> <span>-</span> icomoon-case</div>
                        <div><i class="icomoon-cash-24"></i> <span>-</span> icomoon-cash-24</div>
                        <div><i class="icomoon-compare-options"></i> <span>-</span> icomoon-compare-options</div>
                        <div><i class="icomoon-doc-time"></i> <span>-</span> icomoon-doc-time</div>
                        <div><i class="icomoon-documents-time"></i> <span>-</span> icomoon-documents-time</div>
                        <div><i class="icomoon-government"></i> <span>-</span> icomoon-government</div>
                        <div><i class="icomoon-mobile-dollar"></i> <span>-</span> icomoon-mobile-dollar</div>
                        <div><i class="icomoon-preferences"></i> <span>-</span> icomoon-preferences</div>
                        <div><i class="icomoon-shop"></i> <span>-</span> icomoon-shop</div>
                        <div><i class="icomoon-user-star"></i> <span>-</span> icomoon-user-star</div>
                        <div><i class="icomoon-speed"></i> <span>-</span> icomoon-speed</div>
                        <div><i class="icomoon-arrow-right-circle"></i> <span>-</span> icomoon-arrow-right-circle</div>
                        <div><i class="icomoon-free-circle"></i> <span>-</span> icomoon-free-circle</div>
                        <div><i class="icomoon-dollar-circle"></i> <span>-</span> icomoon-dollar-circle</div>
                        <div><i class="icomoon-simple"></i> <span>-</span> icomoon-simple</div>
                    </div>
                     <div style="padding: 10px 15px;" class="ico-set-holder">
                        <div><i class="icomoon-clock-small"></i> <span>-</span> icomoon-clock-small</div>
                        <div><i class="icomoon-cross-small"></i> <span>-</span> icomoon-cross-small</div>
                        <div><i class="icomoon-percent-small"></i> <span>-</span> icomoon-percent-small</div>
                        <div><i class="icomoon-phone-small"></i> <span>-</span> icomoon-phone-small</div>
                        <div><i class="icomoon-dollar-small"></i> <span>-</span> icomoon-dollar-small</div>
                        <div><i class="icomoon-calendar-small"></i> <span>-</span> icomoon-calendar-small</div>
                        <div><i class="icomoon-menu-small"></i> <span>-</span> icomoon-menu-small</div>
                        <div><i class="icomoon-search-small"></i> <span>-</span> icomoon-search-small</div>
                        <div><i class="icomoon-address-small"></i> <span>-</span> icomoon-address-small</div>
                        <div><i class="icomoon-mail-small"></i> <span>-</span> icomoon-mail-small</div>
                    </div>
                    <div style="padding: 10px 15px;" class="ico-set-holder">
                        <div><i class="icomoon-twitter"></i> <span>-</span> icomoon-twitter</div>
                        <div><i class="icomoon-tumblr"></i> <span>-</span> icomoon-tumblr</div>
                        <div><i class="icomoon-linked-in"></i> <span>-</span> icomoon-linked-in</div>
                        <div><i class="icomoon-pinterest"></i> <span>-</span> icomoon-pinterest</div>
                        <div><i class="icomoon-instagram"></i> <span>-</span> icomoon-instagram</div>
                        <div><i class="icomoon-facebook"></i> <span>-</span> icomoon-facebook</div>
                    </div>
                    <div style="padding: 10px 15px;">
                        <p>If need gradient-color added "ico-gradient".(not supported in ie)</p>
                        <p>Example:</div>
                    <i class="icomoon-user-star ico-gradient"></i> <span>-</span> icomoon-user-star ico-gradient </div>
                     </div>
                </td>
            </tr>
            </tbody>
        </table>

        <?php
    }
}

new lg_info_page;