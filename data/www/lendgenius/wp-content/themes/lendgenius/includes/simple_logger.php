<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 13.12.16
 * Time: 23:48
 */

class simple_logger {
    public $file;

    function __construct($filename){
        $this->file = fopen($filename, "a");
    }

    public function info($mess){
        $before_data = $this->getBeforeData(1);
        $this->saveToFile($before_data.$mess);
    }

    public function warning($mess){
        $before_data = $this->getBeforeData(2);
        $this->saveToFile($before_data.$mess);
    }

    public function error($mess){
        $before_data = $this->getBeforeData(3);
        $this->saveToFile($before_data.$mess);
    }

    public function debug($mess){
        $before_data = $this->getBeforeData(4);
        $this->saveToFile($before_data.$mess);
    }
    private function saveToFile($mess){
        fwrite($this->file, $mess . "\n");
    }

    private function getBeforeData($status){
        $date = date(DATE_ATOM);
        $status_desc = '';
        switch ($status) {
            case 1:
                $status_desc = '[INFO]: ';
                break;
            case 2:
                $status_desc = '[WARNING]: ';
                break;
            case 3:
                $status_desc = '[ERROR]: ';
                break;
            case 4:
                $status_desc = '[DEBUG]: ';
                break;
        }
        return $date.$status_desc;
    }
    function __destruct(){
        fclose($this->file);
    }
}

$simple_logger = new simple_logger(SIMPLE_LOGGER_DIR);