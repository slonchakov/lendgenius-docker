<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 2/1/17
 * Time: 9:46 AM
 */


/*
* Author
*/

function author_post_type() {

// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Authors', 'Post Type General Name', 'lendgenius' ),
        'singular_name'       => _x( 'Author', 'Post Type Singular Name', 'lendgenius' ),
        'menu_name'           => __( 'Authors', 'lendgenius' ),
        'parent_item_colon'   => __( 'Parent Author', 'lendgenius' ),
        'all_items'           => __( 'All Authors', 'lendgenius' ),
        'view_item'           => __( 'View Author', 'lendgenius' ),
        'add_new_item'        => __( 'Add New Author', 'lendgenius' ),
        'add_new'             => __( 'Add New', 'lendgenius' ),
        'edit_item'           => __( 'Edit Author', 'lendgenius' ),
        'update_item'         => __( 'Update Author', 'lendgenius' ),
        'search_items'        => __( 'Search Author', 'lendgenius' ),
        'not_found'           => __( 'Not Found', 'lendgenius' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'lendgenius' ),
    );

// Set other options for Custom Post Type

    $args = array(
        'label'               => __( 'author', 'lendgenius' ),
        'description'         => __( 'Author news and reviews', 'lendgenius' ),
        'labels'              => $labels,
        // Features this CPT(loans) supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-admin-users',
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
        'rewrite' => array('slug' => 'blog/author', 'with_front' => false),
    );

    // Registering  Custom Post Type(loans)
    register_post_type( 'authors', $args );

}

add_action( 'init', 'author_post_type', 0 );

function wpb_change_title_text( $title ){
    $screen = get_current_screen();
    if  ( 'authors' == $screen->post_type ) {
        $title = 'Enter author name';
    }
    return $title;
}

add_filter( 'enter_title_here', 'wpb_change_title_text' );

function rename_featured_image() {
    remove_meta_box( 'postimagediv', 'authors', 'side' );
    add_meta_box('postimagediv', __('Author image', 'lendgenius'), 'post_thumbnail_meta_box', 'authors', 'side', 'low');
}

add_action( 'admin_head', 'rename_featured_image' );

function author_change_featured_image_text( $content ) {
    if ( 'authors' === get_post_type() ) {
        $content = str_replace( 'Set featured image', __( 'Set author image', 'lendgenius' ), $content );
        $content = str_replace( 'Remove featured image', __( 'Remove author image', 'lendgenius' ), $content );
    }
    return $content;
}

add_filter( 'admin_post_thumbnail_html', 'author_change_featured_image_text' );