<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 2/1/17
 * Time: 9:38 AM
 */

/*
* Resources
*/

function resources_post_type() {

// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Resources', 'Post Type General Name', 'Divi-Child' ),
        'singular_name'       => _x( 'Resource', 'Post Type Singular Name', 'Divi-Child' ),
        'menu_name'           => __( 'Resources', 'Divi-Child' ),
        'parent_item_colon'   => __( 'Parent Resource', 'Divi-Child' ),
        'all_items'           => __( 'All Resources', 'Divi-Child' ),
        'view_item'           => __( 'View Resource', 'Divi-Child' ),
        'add_new_item'        => __( 'Add New Resource', 'Divi-Child' ),
        'add_new'             => __( 'Add New', 'Divi-Child' ),
        'edit_item'           => __( 'Edit Resource', 'Divi-Child' ),
        'update_item'         => __( 'Update Resource', 'Divi-Child' ),
        'search_items'        => __( 'Search Resource', 'Divi-Child' ),
        'not_found'           => __( 'Not Found', 'Divi-Child' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'Divi-Child' ),
    );

// Set other options for Custom Post Type

    $args = array(
        'label'               => __( 'resources', 'Divi-Child' ),
        'description'         => __( 'Resource news and reviews', 'Divi-Child' ),
        'labels'              => $labels,
        // Features this CPT(resources) supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        // You can associate this CPT(resources) with a taxonomy or custom taxonomy.
        'taxonomies'          => array( 'types' ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 6,
        //'menu_icon'           => 'dashicons-cart',
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
        'rewrite' => array('slug' => 'resource', 'with_front' => false),
    );

    // Registering  Custom Post Type(loans)
    register_post_type( 'resources', $args );

}


add_action( 'init', 'resources_post_type', 0 );