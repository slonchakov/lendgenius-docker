<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 2/1/17
 * Time: 9:39 AM
 */

/*
* Lender box
*/

function lbox_post_type() {

// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Lender boxes', 'Post Type General Name', 'Divi-Child' ),
        'singular_name'       => _x( 'Lender box', 'Post Type Singular Name', 'Divi-Child' ),
        'menu_name'           => __( 'Lender boxes', 'Divi-Child' ),
        'parent_item_colon'   => __( 'Parent Lender box', 'Divi-Child' ),
        'all_items'           => __( 'All Lender boxes', 'Divi-Child' ),
        'view_item'           => __( 'View Lender box', 'Divi-Child' ),
        'add_new_item'        => __( 'Add New Lender box', 'Divi-Child' ),
        'add_new'             => __( 'Add New', 'Divi-Child' ),
        'edit_item'           => __( 'Edit Lender box', 'Divi-Child' ),
        'update_item'         => __( 'Update Lender box', 'Divi-Child' ),
        'search_items'        => __( 'Search Lender box', 'Divi-Child' ),
        'not_found'           => __( 'Not Found', 'Divi-Child' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'Divi-Child' ),
    );

// Set other options for Custom Post Type

    $args = array(
        'label'               => __( 'lboxes', 'Divi-Child' ),
        'description'         => __( 'Lender box news and reviews', 'Divi-Child' ),
        'labels'              => $labels,
        // Features this CPT(loans) supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-cart',
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
        'rewrite' => array('with_front' => false),
    );

    // Registering  Custom Post Type(loans)
    register_post_type( 'lboxes', $args );

}


add_action( 'init', 'lbox_post_type', 0 );