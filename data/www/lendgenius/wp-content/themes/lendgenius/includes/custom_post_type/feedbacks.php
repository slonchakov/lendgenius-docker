<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 7/4/17
 * Time: 2:43 PM
 */


/*
* Feedbacks
*/

function feedbacks_post_type() {

// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Feedbacks', 'Post Type General Name', 'lendgenius' ),
        'singular_name'       => _x( 'Feedback', 'Post Type Singular Name', 'lendgenius' ),
        'menu_name'           => __( 'Feedbacks', 'lendgenius' ),
        'parent_item_colon'   => __( 'Parent Feedback', 'lendgenius' ),
        'all_items'           => __( 'All Feedbacks', 'lendgenius' ),
        'view_item'           => __( 'View Feedback', 'lendgenius' ),
        'add_new_item'        => __( 'Add New Feedback', 'lendgenius' ),
        'add_new'             => __( 'Add New', 'lendgenius' ),
        'edit_item'           => __( 'Edit Feedback', 'lendgenius' ),
        'update_item'         => __( 'Update Feedback', 'lendgenius' ),
        'search_items'        => __( 'Search Feedback', 'lendgenius' ),
        'not_found'           => __( 'Not Found', 'lendgenius' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'lendgenius' ),
    );

// Set other options for Custom Post Type

    $args = array(
        'label'               => __( 'feedbacks', 'lendgenius' ),
        'description'         => __( 'Feedback about LG', 'lendgenius' ),
        'labels'              => $labels,
        // Features this CPT(resources) supports in Post Editor
        'supports'            => array( 'title', 'editor', 'author', 'thumbnail',  'comments', 'revisions', ),
        // You can associate this CPT(resources) with a taxonomy or custom taxonomy.
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 6,
        'menu_icon'           => 'dashicons-admin-page',
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => false,
        'publicly_queryable'  => false,
        'capability_type'     => 'page'
    );

    // Registering  Custom Post Type(loans)
    register_post_type( 'feedbacks', $args );

}


add_action( 'init', 'feedbacks_post_type', 0 );

function wpb_change_fedbacks_title_text( $title ){
    $screen = get_current_screen();
    if  ( 'feedbacks' == $screen->post_type ) {
        $title = 'Enter author name';
    }
    return $title;
}

add_filter( 'enter_title_here', 'wpb_change_fedbacks_title_text' );