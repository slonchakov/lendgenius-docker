<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 6/15/17
 * Time: 9:05 AM
 */

/*
* FAQs
*/

function faqs_post_type() {

// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Faqs', 'Post Type General Name', 'lendgenius' ),
        'singular_name'       => _x( 'Faq', 'Post Type Singular Name', 'lendgenius' ),
        'menu_name'           => __( 'Faqs', 'lendgenius' ),
        'parent_item_colon'   => __( 'Parent Faq', 'lendgenius' ),
        'all_items'           => __( 'All Faqs', 'lendgenius' ),
        'view_item'           => __( 'View Faq', 'lendgenius' ),
        'add_new_item'        => __( 'Add New Faq', 'lendgenius' ),
        'add_new'             => __( 'Add New', 'lendgenius' ),
        'edit_item'           => __( 'Edit Faq', 'lendgenius' ),
        'update_item'         => __( 'Update Faq', 'lendgenius' ),
        'search_items'        => __( 'Search Faq', 'lendgenius' ),
        'not_found'           => __( 'Not Found', 'lendgenius' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'lendgenius' ),
    );

// Set other options for Custom Post Type

    $args = array(
        'label'               => __( 'faqs', 'lendgenius' ),
        'description'         => __( 'Faq answers on the questions', 'lendgenius' ),
        'labels'              => $labels,
        // Features this CPT(resources) supports in Post Editor
        'supports'            => array( 'title', 'editor', 'author',  'comments', 'revisions', ),
        // You can associate this CPT(resources) with a taxonomy or custom taxonomy.
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 6,
        //'menu_icon'           => 'dashicons-cart',
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
        //'rewrite' => array('slug' => 'faq', 'with_front' => false),
    );

    // Registering  Custom Post Type(loans)
    register_post_type( 'faqs', $args );

}


add_action( 'init', 'faqs_post_type', 0 );