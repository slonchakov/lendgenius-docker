<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 25.10.16
 * Time: 14:48
 */

add_filter( 'gettext', 'wpl_excerpt_gettext', 10, 2 );
function wpl_excerpt_gettext( $translation, $original )
{
    global $typenow;
    if ( 'Excerpt' == $original && $typenow == 'loans') {
        return 'Short Paragraph';
    }elseif($typenow == 'loans'){
        $pos = strpos($original, 'Excerpts are optional hand-crafted summaries of your');
        if ($pos !== false) {
            return  'Short description of loan';
        }
    }
    return $translation;
}

add_action('admin_head', 'admin_loan_styles');

function admin_loan_styles() {
    ?>
    <style>
        .post-type-loans #acf-item_1_title,
        .post-type-loans #acf-item_2_title,
        .post-type-loans #acf-item_3_title,
        .post-type-loans #acf-item_4_title{
            width: 40%;
            float: left;
        }
        .post-type-loans #acf-item_1_description,
        .post-type-loans #acf-item_2_description,
        .post-type-loans #acf-item_3_description,
        .post-type-loans #acf-item_4_description{
            width: 50%;
            float: left;
        }
        .post-type-loans #acf-item_1_title .acf-input-wrap input, .post-type-loans #acf-item_1_description .acf-input-wrap input,
        .post-type-loans #acf-item_2_title .acf-input-wrap input, .post-type-loans #acf-item_2_description .acf-input-wrap input,
        .post-type-loans #acf-item_3_title .acf-input-wrap input, .post-type-loans #acf-item_3_description .acf-input-wrap input,
        .post-type-loans #acf-item_4_title .acf-input-wrap input, .post-type-loans #acf-item_4_description .acf-input-wrap input{
            height: 50px;
        }
        .post-type-loans #acf-item_1_title,
        .post-type-loans #acf-item_2_title,
        .post-type-loans #acf-item_3_title,
        .post-type-loans #acf-item_4_title{
            margin-right: 10%;
        }
        .post-type-loans #acf-item_1_description .acf-input-append,
        .post-type-loans #acf-item_2_description .acf-input-append,
        .post-type-loans #acf-item_3_description .acf-input-append,
        .post-type-loans #acf-item_4_description .acf-input-append{
            border: none;
            margin-left: 30px;
        }
        .post-type-loans #acf-item_1_description .acf-input-append img,
        .post-type-loans #acf-item_2_description .acf-input-append img,
        .post-type-loans #acf-item_3_description .acf-input-append img,
        .post-type-loans #acf-item_4_description .acf-input-append img{
            width: 40px;
        }
        .post-type-loans #acf-features_pros, .post-type-loans #acf-features_cons{
            width: 45%;
            display: inline-block;
        }
        .post-type-loans #acf-features_pros{
            margin-right: 10%;
        }
        .post-type-loans #acf-top_articles, .post-type-loans #acf-top_articles_title{
            clear: both;
        }
  </style>
    <?php
}