<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 26.10.16
 * Time: 22:47
 */

function set_fields_for_loan(){
    $_SESSION['loan_data'] = array();
    $_SESSION['loan_data']['loan_amount'] = isset($_POST['loan_amount']) ? $_POST['loan_amount']:0;
    $_SESSION['loan_data']['gross_sales'] = isset($_POST['gross_sales']) ? $_POST['gross_sales']:0;
    $_SESSION['loan_data']['time_in_business'] = isset($_POST['time_in_business']) ? $_POST['time_in_business']:'';
    $_SESSION['loan_data']['business_name'] = isset($_POST['business_name']) ? $_POST['business_name']:'';
    $_SESSION['loan_data']['industry_description'] = isset($_POST['industry_description']) ? $_POST['industry_description']:'';
    $_SESSION['loan_data']['business_address'] = isset($_POST['business_address']) ? $_POST['business_address']:'';
    $_SESSION['loan_data']['zip_code'] = isset($_POST['zip_code']) ? $_POST['zip_code']:'';
}

add_action('wp_ajax_loan_form', 'send_loan_form');
add_action('wp_ajax_nopriv_loan_form', 'send_loan_form');

function lg_get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
        $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

function send_loan_form()
{
    global $simple_logger;
//Conf data
    $server_url = get_option( 'sts_api_url' );//"https://www.leadsmarket.com/api2/post/data.aspx";
    $company_id = get_option( 'sts_company_id' );//'230575';
    $company_key = get_option( 'sts_company_key' );//'65680307-de95-4788-9c96-0a5ee7e67ebe';
    $test_result = get_option( 'sts_test_result' );
    $test_result_value = get_option( 'sts_test_result_value' );
    $reject_page = get_option( 'sts_reject_page' );
    $client_url = get_site_url().'/get-started/';
//Data
    $uid = $_POST['hituid'];
    $campaignid = $_POST['campaignid'];
    $req_amount = $_POST['loan_amount'];
    $req_amount = intval(str_replace(array('$', ' ', ',', '.'), array('', '', '', ''), $req_amount));
    $reason = "";
    $month_in_business = $_POST['time_in_business'];
    $gross_sales = $_POST['gross_sales'];
    $gross_sales = intval(str_replace(array('$', ' ', ',', '.'), array('', '', '', ''), $gross_sales));
    $credit = $_POST['credit'];
    $business_name = $_POST['business_name'];
    $state_of_incorporation = ''; //---


    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $email = $_POST['email'];
    $zip_code = $_POST['zip_code'];
    $business_address = $_POST['business_address'];

    $industry_description = $_POST['industry_description'];
    $phone_home = $_POST['phone'];

    $is_invoice = isset($_POST['is_invoice']) ? $_POST['is_invoice']:'';


    $vals = array();

//    $vals["CampaignID"] = $company_id;
//    $vals["CampaignKey"] = $company_key;
    $vals["responseType"] = 'xml';
    $vals["uid"] = $uid;
    $vals["campaignid"] = $campaignid;
    if($test_result == 'on'){
        $vals["TestResult"] = $test_result_value;
    }else{
        $vals["TestResult"] = "";
    }
    $vals["SubID"] = "";
    $vals["ClientIP"] = lg_get_client_ip();
    $vals["ClientUserAgent"] = $_SERVER['HTTP_USER_AGENT'];
    $vals["ClientURL"] = $client_url;
    $vals["MinimumPrice"] = '';
    $vals["MaxResponseTime"] = 60;
    $vals["RequestedAmount"] = $req_amount;
    $vals["ReasonUsed"] = $reason;
    $vals["TimeInBusiness"] = $month_in_business;
    $vals["GrossSales"] = $gross_sales;
    $vals["Credit"] = $credit;
    $vals["BusinessName"] = $business_name;
    $vals["Dba"] = "";
    $vals["BusinessType"] = "";
    $vals["StateOfIncorporation"] = $state_of_incorporation;
    $vals["FirstName"] = $first_name;
    $vals["LastName"] = $last_name;
    $vals["Email"] = $email;
    $vals["PhoneWork"] = "";
    $vals["ZipCodeBusiness"] = $zip_code;
    $vals["BusinessAddress"] = $business_address;
    $vals["BusinessConduct"] = "";
    $vals["BusinessStartDate"] = "";
    $vals["IndustryDescription"] = $industry_description;
    $vals["HadBankruptcy"] = "";
    $vals["BusinessOwnerFirstName"] = "";
    $vals["BusinessOwnerLastName"] = "";
    $vals["SSN"] = "";
    $vals["Address1"] = "";
    $vals["City"] = "";
    $vals["State"] = "";
    $vals["ZipCode"] = "";
    $vals["PhoneHome"] = $phone_home;
    $vals["DOB"] = "";
    $vals["OwnershipDisp"] = "";
    $vals["IsInvoice"] = $is_invoice;
    $vals["Optin"] = "";

    $simple_logger->debug(json_encode($vals));

    $postvals = "";
    $resp_data = false;

    foreach ($vals as $var => $val) {
        if (strlen($postvals)) $postvals .= "&";
        $postvals .= $var . "=" . urlencode($val);
    }

    $simple_logger->debug($postvals);

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $server_url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postvals);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Expect:"));
    curl_setopt($ch, CURLOPT_FAILONERROR, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_TIMEOUT, 120);
    curl_setopt($ch, CURLINFO_HEADER_OUT, true);
    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);

    if (curl_errno($ch)) {
        // Error
    } else {
        $response = curl_exec($ch);
        parse_str($response, $resp_data);
        if (curl_errno($ch)) {
            // Error
            $resp_data = curl_errno($ch);
            $simple_logger->error($resp_data);
        } else {
            $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            $header = substr($response, 0, $header_size);
            $body = substr($response, $header_size);
            $simple_logger->debug($body);
            $resp_data = simplexml_load_string($body);
            if(!empty($reject_page) && $resp_data->Result == 'Rejected'){
                $resp_data->RedirectURL = get_permalink($reject_page);
            }
        }
        curl_close($ch);
    }

    echo json_encode($resp_data);
    wp_die();
}
