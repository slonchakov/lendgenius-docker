<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 6/14/17
 * Time: 3:47 PM
 */

    // Widget related posts
    class widget_related_posts extends WP_Widget {

    function __construct() {
        parent::__construct(
        'wp_related_posts_lend',
        __('Related Posts', 'lendgenius'),
        array( 'description' => __( 'Widget for Related Posts', 'lendgenius' ), )
        );
        }

        // Creating widget front-end
        public function widget( $args, $instance ) {
        $title = apply_filters( 'widget_title', $instance['title'] );

        //You May Also Like
        $you_may_also_like = get_post_meta( get_the_ID(), 'top_articles', true );
        if ($you_may_also_like && is_array($you_may_also_like)) {

            ?>
            <div class="related-posts">
            <?php if ( ! empty( $title ) ):
                echo "<div class=\"medium fs-24 margin-bottom-15\">$title</div>";
            endif; ?>
            <div class="list-posts-wrapper">
            <?php

            $args = array(
                'post_type' => 'post',
                'post__in' => $you_may_also_like,
                'posts_per_page' => 10,
                'caller_get_posts' => 1
            );
            $fp_query = new WP_Query($args);
            $post_count = $fp_query->post_count;
            if ($fp_query->have_posts() && $post_count > 0) :
                while ($fp_query->have_posts()) : $fp_query->the_post();
                    ?>

                    <div class="blog-item-sub">
                        <div class="blog-item-sub-img">
                            <a href="<?= get_permalink(); ?>">
                                <?php
                                // Post thumbnail.
                                single_post_thumbnail('author-thumb-posts', '');
                                ?>
                            </a>
                        </div>

                        <h4 class="blog-item-sub-title matchheight" style="height: 69px;">
                            <a href="<?= get_permalink(); ?>">
                                <?= the_title(); ?>
                            </a>
                        </h4>
                        <div class="blog-item-sub-author">
                            <span><a href="<?=get_permalink(get_post_meta( get_the_ID() , 'author' , true )); ?>"><?= get_the_title(get_post_meta( get_the_ID() , 'author' , true )); ?></a></span> <i class="fa fa-circle" aria-hidden="true"></i> <span><?= get_the_date() ?></span>
                        </div>
                    </div>
                    <?php
                endwhile;
                wp_reset_query();

                ?>
                    </div>
                </div>
        <?php
        endif;
        }
    }

    // Widget Backend
    public function form( $instance ) {
        if ( $instance ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = '';
        }
    // Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <?php
    }

    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }
}
