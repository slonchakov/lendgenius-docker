<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 6/26/17
 * Time: 9:43 PM
 */


function breadcrumbs($before = '', $after = '', $display = true, $separator = '<span class="separator"></span>', $last_title= false) {
    global $post;
    $home = 'Home';
    $path = array_filter(explode('/', parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)));
    $base_url = ($_SERVER['HTTPS'] ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
    $breadcrumbs = array("<a href=\"$base_url\">$home</a>");

    $schemaStart = '<script type="application/ld+json">{"@context": "https://schema.org/", "@type": "BreadcrumbList", "itemListElement": [';
    $schemaEnd = ']}</script>';

    $schemaItems = [];

    $schemaItems[] = '{"@type": "ListItem", "position": 1, "name": "Home", "item": "https://www.lendgenius.com" }';

    $breadcrumb_menu = (get_option('breadcrumb_router')) ? get_option('breadcrumb_router') : 'Router';

    $router = wp_get_nav_menu_items($breadcrumb_menu);
    if (isset($router) && $router) {

        $current = array_search($post->ID,array_column($router,'object_id'));
    }

    if (!empty($current) && isset($router[$current]) && !is_archive()) {
        $current_link =  $router[$current]->title;

        $parent_id = $router[$current]->menu_item_parent;

        $parent = array_search($parent_id,array_column($router ,'ID'));

        if (!empty($parent_id) && isset($parent)) {
            $parent_link = '<a href="'. $router[$parent]->url .'/">'. $router[$parent]->title .'</a>';

            $schemaItems[] = '{"@type": "ListItem", "position": 2, "name": "'. $router[$parent]->title .'", "item": "'. $router[$parent]->url .'/" }';
            $breadcrumbs[] = $parent_link;
        }

        $schemaItems[] = '{"@type": "ListItem", "position": 3, "name": "'. $router[$current]->title .'", "item": "'. $router[$current]->url .'" }';

        $breadcrumbs[] = $current_link;

    } else {
        $last = end( array_keys($path) );

        foreach( $path as $x => $crumb ){
            $title = ucwords(str_replace(array('.php', '_'), Array('', ' '), $crumb));
            if( $x != $last ){
                $breadcrumbs[] = '<a href="'.$base_url.$crumb.'/">'.$title.'</a>';

                $schemaItems[] = '{"@type": "ListItem", "position": '. ($x+1) .', "name": "'. $title .'", "item": "'. $base_url.$crumb .'/" }';
            }
            else {
                if($last_title){
                    $breadcrumbs[] = $last_title;

                    $schemaItems[] = '{"@type": "ListItem", "position": '. ($last+1) .', "name": "'. $last_title .'", "item": "'. get_the_permalink($post->ID) .'" }';

                }elseif(is_single($post) || is_page( $post )){
                    $breadcrumbs[] = get_the_title();

                    $schemaItems[] = '{"@type": "ListItem", "position": '. ($last+1) .', "name": "'. get_the_title() .'", "item": "'. get_the_permalink($post->ID) .'" }';

                }else{
                    $breadcrumbs[] = $title;

                    $schemaItems[] = '{"@type": "ListItem", "position": '. ($last+1) .', "name": "'. $title .'", "item": "'. get_the_permalink($post->ID) .'" }';
                }
            }


        }
    }


    if($display){
        echo $schemaStart.implode(',', $schemaItems).$schemaEnd.$before.implode( $separator, $breadcrumbs ).$after;
    }else{
        return implode( $separator, $breadcrumbs );
    }
}