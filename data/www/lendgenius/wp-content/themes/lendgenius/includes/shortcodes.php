<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 24.10.16
 * Time: 16:26
 */

/**
 * TODO refactor all this shit
 */

//[gmail-cta]
function gmail_cta( $atts ){
    $icon = isset($atts['icon']) ? $atts['icon']:'';
    return '<i class="bullet-icon icon-'.$icon.'"></i>';
}
add_shortcode( 'gmail-cta', 'gmail_cta' );

//[lp-as-featured-on]
function lp_as_featured_on( $atts ){
    return '<div class="section-padding">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="partners-logo">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h2>As Seen On</h2>
                                        <br>
                                    </div>
                                </div>
                                <div class="flex-row flex-wrap flex-justify-sa">
                                    <div class="item-logo">
                                        <a href="https://www.entrepreneur.com/article/289771" target="_blank"><img src="'.get_stylesheet_directory_uri().'/landing-pages/img/enterpreteur_logo.png" alt="Entrepreneur"></a>
                                    </div>
                                    <div class="item-logo inc">
                                        <a href="https://www.forbes.com/sites/brianrashid/2017/05/27/how-to-power-through-when-your-startup-is-going-under/#240274333a20" target="_blank"><img src="'.get_stylesheet_directory_uri().'/landing-pages/img/Forbes_logo.png" alt="Forbes"></a>
                                    </div>
                                    <div class="item-logo inc">
                                        <a href="https://www.inc.com/andrew-medal/5-resources-to-help-minority-startup-founders.html" target="_blank"><img src="'.get_stylesheet_directory_uri().'/landing-pages/img/inc_logo.png" alt="Inc."></a>
                                    </div>
                                    <div class="item-logo">
                                        <a href="https://www.desk.com/blog/4-startups-setting-a-high-bar-for-customer-service" target="_blank"><img src="'.get_stylesheet_directory_uri().'/landing-pages/img/sales_force_logo.png" alt="Salesforce."></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
}
add_shortcode( 'lp-as-featured-on', 'lp_as_featured_on' );

//[lp-how-lg-works]
function lp_how_lg_works( $atts ){
    return '<section class="how-works">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>
                    How LendGenius works
                </h2>
                <div class="item-how item-how-3">
                    <div class="icon-holder">
                        <i class="icomoon-user-star"></i>
                    </div>
                    See If You Connect
                </div>
                <div class="item-how item-how-3">
                    <div class="icon-holder">
                        <i class="icomoon-doc-time"></i>
                    </div>
                    Complete Form in Minutes
                </div>
                <div class="item-how item-how-3">
                    <div class="icon-holder">
                        <i class="icomoon-preferences"></i>
                    </div>
                    Check Your Terms
                </div>
                <div class="item-how item-how-3">
                    <div class="icon-holder">
                        <i class="icomoon-dollar-circle"></i>
                    </div>
                    Get Funded
                </div>
            </div>
        </div>
    </div>
</section>';
}
add_shortcode( 'lp-how-lg-works', 'lp_how_lg_works' );

//[lp-how-we-compare]
function lp_how_we_compare( $atts ){
    return '<section class="сomparison">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>How we compare</h2>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th></th>
                                <th>
                                    <div class="brand-logo table-logo">
                                        <a>
                                            Lend<span>Genius</span>
                                        </a>
                                    </div>
                                </th>
                                <th>Traditional Bank<br> Loans</th>
                                <th>SBA Loan</th>
                                <th>Merchant Cash Advance</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    Competitive Rates
                                </td>
                                <td>
                                    <i class="icon-check"></i>
                                </td>
                                <td>
                                    <i class="icon-check"></i>
                                </td>
                                <td>
                                    <i class="icon-check"></i>
                                </td>
                                <td>
                                    <i class="icon-cross"></i>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Multiple Loan Options
                                </td>
                                <td>
                                    <i class="icon-check"></i>
                                </td>
                                <td>
                                    <i class="icon-cross"></i>
                                </td>
                                <td>
                                    <i class="icon-check"></i>
                                </td>
                                <td>
                                    <i class="icon-cross"></i>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    High Approval Rate
                                </td>
                                <td>
                                    <i class="icon-check"></i>
                                </td>
                                <td>
                                    <i class="icon-cross"></i>
                                </td>
                                <td>
                                    <i class="icon-cross"></i>
                                </td>
                                <td>
                                    <i class="icon-cross"></i>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Quick Processing
                                </td>
                                <td>
                                    <i class="icon-check"></i>
                                </td>
                                <td>
                                    <i class="icon-cross"></i>
                                </td>
                                <td>
                                    <i class="icon-cross"></i>
                                </td>
                                <td>
                                    <i class="icon-cross"></i>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Paperless Application
                                </td>
                                <td>
                                    <i class="icon-check"></i>
                                </td>
                                <td>
                                    <i class="icon-cross"></i>
                                </td>
                                <td>
                                    <i class="icon-cross"></i>
                                </td>
                                <td>
                                    <i class="icon-cross"></i>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div id="carousel-сomparison" class="carousel slide carousel-сomparison" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                        <div class="item">
                            <div class="header-carousel">
                                Competitive Rates
                            </div>
                            <div class="item-comparison">
                                <i class="icon-check"></i>
                                <div class="brand-logo table-logo">
                                    <a>
                                        Lend<span>Genius</span>
                                    </a>
                                </div>
                            </div>
                            <div class="item-comparison">
                                <i class="icon-check"></i>
                                <div>
                                    Traditional Bank Loans
                                </div>
                            </div>
                            <div class="item-comparison">
                                <i class="icon-check"></i>
                                <div>
                                    SBA Loan
                                </div>
                            </div>
                            <div class="item-comparison">
                                <i class="icon-cross"></i>
                                <div>
                                    Merchant Cash Advance
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="header-carousel">
                                Multiple Loan Options
                            </div>
                            <div class="item-comparison">
                                <i class="icon-check"></i>
                                <div class="brand-logo table-logo">
                                    <a>
                                        Lend<span>Genius</span>
                                    </a>
                                </div>
                            </div>
                            <div class="item-comparison">
                                <i class="icon-cross"></i>
                                <div>
                                    Traditional Bank Loans
                                </div>
                            </div>
                            <div class="item-comparison">
                                <i class="icon-check"></i>
                                <div>
                                    SBA Loan
                                </div>
                            </div>
                            <div class="item-comparison">
                                <i class="icon-cross"></i>
                                <div>
                                    Merchant Cash Advance
                                </div>
                            </div>
                        </div>
                        <div class="item active">
                            <div class="header-carousel">
                                High Approval Rate
                            </div>
                            <div class="item-comparison">
                                <i class="icon-check"></i>
                                <div class="brand-logo table-logo">
                                    <a>
                                        Lend<span>Genius</span>
                                    </a>
                                </div>
                            </div>
                            <div class="item-comparison">
                                <i class="icon-cross"></i>
                                <div>
                                    Traditional Bank Loans
                                </div>
                            </div>
                            <div class="item-comparison">
                                <i class="icon-cross"></i>
                                <div>
                                    SBA Loan
                                </div>
                            </div>
                            <div class="item-comparison">
                                <i class="icon-cross"></i>
                                <div>
                                    Merchant Cash Advance
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="header-carousel">
                                Quick Processing
                            </div>
                            <div class="item-comparison">
                                <i class="icon-check"></i>
                                <div class="brand-logo table-logo">
                                    <a>
                                        Lend<span>Genius</span>
                                    </a>
                                </div>
                            </div>
                            <div class="item-comparison">
                                <i class="icon-cross"></i>
                                <div>
                                    Traditional Bank Loans
                                </div>
                            </div>
                            <div class="item-comparison">
                                <i class="icon-cross"></i>
                                <div>
                                    SBA Loan
                                </div>
                            </div>
                            <div class="item-comparison">
                                <i class="icon-cross"></i>
                                <div>
                                    Merchant Cash Advance
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="header-carousel">
                                Paperless Application
                            </div>
                            <div class="item-comparison">
                                <i class="icon-check"></i>
                                <div class="brand-logo table-logo">
                                    <a>
                                        Lend<span>Genius</span>
                                    </a>
                                </div>
                            </div>
                            <div class="item-comparison">
                                <i class="icon-cross"></i>
                                <div>
                                    Traditional Bank Loans
                                </div>
                            </div>
                            <div class="item-comparison">
                                <i class="icon-cross"></i>
                                <div>
                                    SBA Loan
                                </div>
                            </div>
                            <div class="item-comparison">
                                <i class="icon-cross"></i>
                                <div>
                                    Merchant Cash Advance
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-сomparison" data-slide-to="0" class=""></li>
                        <li data-target="#carousel-сomparison" data-slide-to="1" class=""></li>
                        <li data-target="#carousel-сomparison" data-slide-to="2" class="active"></li>
                        <li data-target="#carousel-сomparison" data-slide-to="3" class=""></li>
                        <li data-target="#carousel-сomparison" data-slide-to="4" class=""></li>
                    </ol>
                    <!-- Wrapper for slides -->
                </div>
            </div>
        </div>
    </div>
</section>';
}
add_shortcode( 'lp-how-we-compare', 'lp_how_we_compare' );

//[CTA-A]
function cta_a_banner( $atts ){
    return '<span class="lendgenius-banner"><a class="btn btn-success" href="'.cta_button_link().'">See Business Lenders</a></span>';
}
add_shortcode( 'CTA-A', 'cta_a_banner' );

//[CTA-B]
function cta_b_banner( $atts ){
    return '<div class="vert_banner">
                <img src="'.get_stylesheet_directory_uri().'/assets/images/vert_banner_logo.png" alt="See loan Options">
                <a href="'.cta_button_link().'" class="btn btn-success">See Business Lenders</a>
            </div>';
}
add_shortcode( 'CTA-B', 'cta_b_banner' );

//[CTA-C]
function cta_c_banner( $atts ){
    return '<a class="btn btn-success big_btn" href="'.cta_button_link().'">See Business Lenders</a>';
}
add_shortcode( 'CTA-C', 'cta_c_banner' );

//[CTA-D]
function cta_d_banner( $atts ){
    return '<a class="" href="/loan-request?EmbedCampID=246948"><br>
<img src="'.get_stylesheet_directory_uri().'/assets/images/banner_35.png" alt="See Business Lenders">
</a>
';
}
add_shortcode( 'CTA-D', 'cta_d_banner' );

//[how-much]
function how_much_block( $atts ){
    return '<div class="content-how-much">
        <div class="content-how-much-wraper">
            <div class="container">
                <div class="how-much-wraper">
                    <div class="how-much-title">
                        <label for="how_much">How Much Does Your Business need?</label>
                    </div>
                    <div class="how-much-search-wraper">
                        <form action="'.cta_button_link().'">
                            <div class="how-much-search">
                                <input class="field-input field-input-big field-input-clear" id="how_much" type="tel" placeholder="How Much Does Your Business Need?">
                            </div>
                            <div class="how-much-button">
                                <button class="field-button btn btn-success" type="submit">See Loan Options</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>';
}
add_shortcode( 'how-much', 'how_much_block' );

//[markup]
function main_title_block( $atts ){
    $id = isset($atts['id']) ? 'id="'.$atts['id'].'" ':'';
    return '<span" '.$id.'>'.$atts['title'].'</span>';
}
add_shortcode( 'markup', 'main_title_block' );

//[CALC-TERM-APR]
function calc_term_apr_block( $atts ){
    return get_template_part('views/content/short-term-calculator2');
}
add_shortcode( 'CALC-TERM-APR', 'calc_term_apr_block' );

//[CALC-TERM-INST]
function calc_term_inst_block( $atts ){
    return get_template_part('views/content/short-term-calculator2');
}
add_shortcode( 'CALC-TERM-INST', 'calc_term_inst_block' );

//[CALC-SHORT-APR]
function calc_short_apr_block( $atts ){
    return get_template_part('views/content/short-term-calculator2');
}
add_shortcode( 'CALC-SHORT-APR', 'calc_short_apr_block' );

//[CALC-SHORT-INST]
function calc_short_inst_block( $atts ){
    return get_template_part('views/content/short-term-calculator');
}
add_shortcode( 'CALC-SHORT-INST', 'calc_short_inst_block' );

//[CALC-SBA-APR]
function calc_sba_apr_block( $atts ){
    return get_template_part('views/content/short-term-calculator2');
}
add_shortcode( 'CALC-SBA-APR', 'calc_sba_apr_block' );

//[CALC-SBA-INST]
function calc_sba_inst_block( $atts ){
    return get_template_part('views/content/short-term-calculator2');
}
add_shortcode( 'CALC-SBA-INST', 'calc_sba_inst_block' );

//[CALC-INVOICE-FACTORING]
function calc_invoice_factoring_block( $atts ){
    return get_template_part('views/content/invoice-factoring-calculator');
}
add_shortcode( 'CALC-INVOICE-FACTORING', 'calc_invoice_factoring_block' );

//[CALC-MERCHANT-CASH-ADVANCE]
function calc_merchant_cach_block( $atts ){
    return get_template_part('views/content/merchant-cash-advance');
}
add_shortcode( 'CALC-MERCHANT-CASH-ADVANCE', 'calc_merchant_cach_block' );

//[back-to-top]
function back_to_top_block( $atts ){
    return '<a href="#" onclick="jQuery(\'body,html\').animate({scrollTop: 0}, 1500);" class="back-to-top">[back-to-top]</a>';
}
add_shortcode( 'back-to-top', 'back_to_top_block' );

//[lender-box]
function lender_box_block( $atts ){
    $title = isset($atts['title']) ? $atts['title']:'';
    include( locate_template( 'content-lender-box.php', false, false ) );
    return $lender_boxes;
}
add_shortcode( 'lender-box', 'lender_box_block' );

//[navigation-table]
function navigation_table_block( $atts ){
    return get_template_part('content-navigation-table');
}
add_shortcode( 'navigation-table', 'navigation_table_block' );

//[survey-cta]
function survey_cta_block( $atts ){
    return get_template_part('content-survey-cta');
}
add_shortcode( 'survey-cta', 'survey_cta_block' );

//[quote-for-twitter]
function quote_for_twitter( $atts ){
    $quote =  isset($atts['quote']) ? $atts['quote']:'';
    if(!empty($quote)){
        return "<div class=\"share-quote\">
                                <a href=\"https://twitter.com/share?url".get_permalink()."&amp;text=".urlencode($quote)." &amp;hashtags=lendgenius\" target=\"_blank\">
                                 \"".$quote."\"
                                 <span class=\"share-quote-resource\">
                                    <i class=\"fa fa-twitter\" aria-hidden=\"true\"></i>
                                 </span>
                                </a>
                            </div>";
    }
}
add_shortcode( 'quote-for-twitter', 'quote_for_twitter' );

function learn_more_on_loan( $atts )
{
    if (isset($atts['article_id'])) {

        $result = get_post($atts['article_id']);
        return '
        <!--Example blog-preview-->
        <p><b>Learn more:</b></p>
         <div class="fix-ie-flex negative-margin-row-xs">
             <div class="blog-item-general-img">
                 <a style="color:#fff" href="'. get_the_permalink($result->ID) .'">
                 <div class="general-img-path" style="background-image: url('. get_the_post_thumbnail_url($result->ID,'hero-image-770-350') .');"></div>
                 <div class="pre-title text-uppercase"></div>
                 <div class="blog-item-general-title">'. $result->post_title.'</div>
                 </a>
             </div>
         </div>';
    }

}
add_shortcode( 'learn-more', 'learn_more_on_loan' );

function embed_video_loan( $atts )
{
    if (isset($atts['src'])) {

        return '<div class="row video-holder"><div class="col-md-10 col-lg-9"><div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="'.$atts['src'] .'" allowfullscreen=""></iframe>
                </div></div></div>';
    }

}
add_shortcode( 'embed-video', 'embed_video_loan' );


function slide_text($atts, $content = null) {
    extract(shortcode_atts(array(
        'class' => '',
        'align' => 'left'
    ), $atts, 'slide_text'));

    $content = et_content_helper($content);

    $output = do_shortcode('
	[et_pb_text admin_label="Text" background_layout="dark" text_orientation="'.$align.'" use_border_color="off" border_color="#ffffff" border_style="solid" module_class="'.$class.'"]
		'.$content.'
	[/et_pb_text]
	');
    ;

    return $output;
}
add_shortcode('slide_text','slide_text');

//add_shortcode('footer_social','footer_social');
function footer_social($atts, $content = null) {
    extract(shortcode_atts(array(
        'logo' => '',
        'align' => 'center'
    ), $atts, 'footer_social'));
    $image = ( $user_logo = et_get_option( 'divi_logo' ) ) && '' != $user_logo
        ? $user_logo
        : $template_directory_uri . '/images/logo.png';
    $output = '<div class="footer_social et_pb_bg_layout_dark"><img src="'.esc_attr( ($logo=='')?$image:$logo ).'" alt='.esc_attr( get_bloginfo( 'name' ) ).'" id="logo" />';
    if ( false !== et_get_option( 'show_footer_social_icons', true ) ) {
        $output .= '<ul class="et-social-icons">';

        if ( 'on' === et_get_option( 'divi_show_facebook_icon', 'on' ) ) :
            $output .= '<li class="et-social-icon et-social-facebook">
				<a href="'.esc_url( et_get_option( 'divi_facebook_url', '#' ) ).'" class="icon">
					<span>'. esc_html( 'Facebook', 'Divi' ).'</span>
				</a>
			</li>';
        endif;
        if ( 'on' === et_get_option( 'divi_show_twitter_icon', 'on' ) ) :
            $output .= '<li class="et-social-icon et-social-twitter">
				<a href="'.esc_url( et_get_option( 'divi_twitter_url', '#' ) ).'" class="icon">
					<span>'. esc_html( 'Twitter', 'Divi' ).'</span>
				</a>
			</li>';
        endif;
        if ( 'on' === et_get_option( 'divi_show_google_icon', 'on' ) ) :
            $output .= '<li class="et-social-icon et-social-google-plus">
				<a href="'.esc_url( et_get_option( 'divi_google_url', '#' ) ).'" class="icon">
					<span>'. esc_html( 'Google', 'Divi' ).'</span>
				</a>
			</li>';
        endif;
        if ( 'on' === et_get_option( 'divi_show_rss_icon', 'on' ) ) :
            $et_rss_url = '' !== et_get_option( 'divi_rss_url' )
                ? et_get_option( 'divi_rss_url' )
                : get_bloginfo( 'rss2_url' );

            $output .= '<li class="et-social-icon et-social-rss">
				<a href="'.esc_url( $et_rss_url ).'" class="icon">
					<span>'. esc_html( 'RSS', 'Divi' ).'</span>
				</a>
			</li>';
        endif;

        $output .= '</ul>';

    }
    $output .= '</div>';
    return $output;
}

add_shortcode('firstname','rejection_name');

function rejection_name()
{
    $output = (empty($_GET['firstname']) ) ? '' : ucfirst(strip_tags($_GET['firstname'])) .",";
    return $output;
}

// [lead form="0"]
// While we have no forms this shortcode returns just empty string
function leadFormShortcode($atts)
{
    ob_start();
    get_template_part('views/forms/form_'. $atts['form']);
    $output = ob_get_contents();
    ob_end_clean();

    $output = "";
    return $output;
}

add_shortcode('lead', 'leadFormShortcode');

// [redirect url='url.slug' label="Text"]
function redirectLinks(array $atts) : string
{
    if (!empty($atts['url'])) {

        $redirect = \App\Classes\Container::make('\\App\\Classes\\Model\\Redirect');
        $link = $redirect->getLinkByPath($atts['url']);

        if (strpos($link, '/redirect/') !== false) {
            $redirectSlug = end(explode('/',$link));
            $link = $redirect->getLinkByPath($redirectSlug);
        }

        if (strpos($link, 'http') === false && count(explode('/',$link)) == 1) {
            $link = $redirect->getLinkByPath($link);
        }

        $label  = $atts['label'] ?? 'See Loan Options';
        //$output = '<a class="btn btn-x2 btn-warning-custom" href="/redirect/'. $atts['url'] .'">'. $label .'</a>';
        $output = '<a class="btn btn-x2 btn-warning-custom" href="'. $link .'">'. $label .'</a>';

        return $output;
    }

    return '';
}
add_shortcode('redirect', 'redirectLinks');

// funding [funding table/list ]
function funding($atts)
{
    global $wp;

    //$fundingOptions = [];

//    while( have_rows('loan_funding_options') ) {
//        the_row();
//        $fundingOptions[] = get_sub_field('item');
//    }

    $fundingOptions = get_post_meta(get_the_ID(),'funding_options_post', true);

    if (!count($fundingOptions) || $fundingOptions == '') return '';

    $display = $atts['display'] ?? 'table';

    $result = [];
    $sortOrder = [];

    $redirect = \App\Classes\Container::make('\\App\\Classes\\Model\\Redirect');
    foreach ($fundingOptions as $fundingOption) {

        $link = get_post_meta( $fundingOption, 'link', true);

        if (strpos($link, '/redirect/') !== false) {
            $redirectSlug = end(explode('/',$link));
            $link = $redirect->getLinkByPath($redirectSlug);
        }

        if (strpos($link, 'http') === false && count(explode('/',$link)) == 1) {
            $link = $redirect->getLinkByPath($link);
        }

        if ($link == '') {
            continue;
        }

        $sortOrder[$fundingOption] = (int) get_post_meta( $fundingOption, 'sort_order', true);

        $result[] =[
            'ID'     => $fundingOption,
            'button' => (get_post_meta( $fundingOption, 'button_text', true)) ? get_post_meta( $fundingOption, 'button_text', true) : 'Get Started',
            'link'   => $link
        ];
    }

    if (!count($result)) {
        return '';
    }

    array_multisort($sortOrder, SORT_ASC, $result);

    $wp->fundingOptions = $result;

    ob_start();
    get_template_part('views/parts/loans-'. $display);
    $output = ob_get_contents();
    ob_end_clean();
    
    return $output;
}
add_shortcode('funding', 'funding');

function addEmbedFormToPost()
{
    // return '<iframe scrolling="no" class="embed-iframe" src="' .get_stylesheet_directory_uri() .'/embed-form.php" sandbox="allow-scripts allow-same-origin allow-forms allow-top-navigation" style="width:100%; height:600px; border:0;"></iframe>';


    ob_start();
    get_template_part('views/forms/embed-slider-form');
    $output = ob_get_contents();
    ob_end_clean();

    return $output;
}
add_shortcode('embed-form', 'addEmbedFormToPost');

function addAngularForm()
{
    ob_start();
    get_template_part('views/parts/jsf-form');
    $output = ob_get_contents();
    ob_end_clean();

    return $output;
}

add_shortcode('jsf-form', 'addAngularForm');