<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 17.11.16
 * Time: 16:50
 */

class lg_loans_page {

    function __construct() {
        add_action( 'admin_menu', array( $this, 'loan_calc_page_menu' ) );
    }

    function loan_calc_page_menu() {
        add_submenu_page(
            'edit.php?post_type=resources',
            'Loans(Calculators Page)',
            'Loans(Calculators Page)',
            'manage_options',
            'loans',
            array(
                $this,
                'loan_calc_page_content'
            )
        );
    }

    function  loan_calc_page_content() {
        ?>
        <h3>Loans(Calculators Page)</h3>
        <?php
        if(isset($_POST) && isset($_POST['data']) && is_array($_POST['data'])):
            foreach ($_POST['data'] as $lkey => $value){


                if ( ! add_post_meta( $lkey, 'unique_value_calc', $value['unique_value_calc'], true ) ) {
                    update_post_meta( $lkey, 'unique_value_calc', $value['unique_value_calc'] );
                }
                if ( ! add_post_meta( $lkey, 'repayment_calc', $value['repayment_calc'], true ) ) {
                    update_post_meta( $lkey, 'repayment_calc', $value['repayment_calc'] );
                }
                if ( ! add_post_meta( $lkey, 'rates_calc', $value['rates_calc'], true ) ) {
                    update_post_meta( $lkey, 'rates_calc', $value['rates_calc'] );
                }
            }
            ?>
            <div id="message" class="updated notice notice-success is-dismissible">
                <p>Page was updated.</p>
                <button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>
            <?php
        endif;
        // The Query
        $args = array(
            'post_type' => 'loans',
            'orderby' => 'ID',
            'order' => 'ASC'
        );
        $query = new WP_Query( $args );
        $post_count = $query->post_count;
        if ( $query->have_posts() ) {
            // The Loop
            ?>
            <form method="post" action="edit.php?post_type=resources&page=loans">
            <table class="loans">
                <tr>
                    <td><b>LOAN PRODUCT</b></td>
                    <td><b>UNIQUE VALUE PROPOSITION</b></td>
                    <td><b>REPAYMENT</b></td>
                    <td><b>RATES</b></td>
                </tr>
            <?php
            while ( $query->have_posts() ) : $query->the_post();
                $pid = get_the_ID();
                $unique_value = get_post_meta($pid, 'unique_value_calc', true);
                $repayment = get_post_meta($pid, 'repayment_calc', true);
                $rates = get_post_meta($pid, 'rates_calc', true);
            ?>
                <tr>
                    <td><b><?php the_title(); ?></b></td>
                    <td><input type="text" name="data[<?php echo $pid; ?>][unique_value_calc]" value="<?php echo $unique_value; ?>"></td>
                    <td><input type="text" name="data[<?php echo $pid; ?>][repayment_calc]" value="<?php echo $repayment; ?>"></td>
                    <td><input type="text" name="data[<?php echo $pid; ?>][rates_calc]" value="<?php echo $rates; ?>"></td>
                </tr>
            <?php

            endwhile;
            ?></table>
            <input type="submit" value="Save" class="button button-primary button-large">
            </form>
            <style>
                .loans{
                    margin-top: 50px;
                }
                .loans tr td {
                    height: 50px;
                }
            </style>
            <?php
            wp_reset_postdata();
        }
    }
}

new lg_loans_page;