<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 21.12.16
 * Time: 18:03
 */

// Widget featured posts
class widget_category_latest_posts extends WP_Widget {

    function __construct() {
        parent::__construct(
            'wp_category_latest_posts_lend',
            __('Category Latest Posts', 'lendgenius'),
            array( 'description' => __( 'Widget for Category Latest Posts', 'lendgenius' ), )
        );
    }

// Creating widget front-end
    public function widget( $args, $instance ) {
        $title = apply_filters( 'widget_title', $instance['title'] );

        echo $args['before_widget'];
        if ( ! empty( $title ) ):
            echo $args['before_title'];
            echo $title;
            echo $args['after_title'];
        endif;

        $category = get_the_category();
        if (!empty($category)):
            $category= -1;
        endif;
// This is where you run the code and display the output
        $args=array(
            'post_type' => 'post',
            'cat' => $category[0]->cat_ID,
            'posts_per_page' => 8,
            'orderby' => 'ID',
            'order'   => 'DESC'
        );
        $fp_query = new WP_Query($args);
        $post_count = $fp_query->post_count;
        if( $fp_query->have_posts()  && $post_count > 0) :
            while ($fp_query->have_posts()) : $fp_query->the_post();
                ?>
                <div class="aside-item-row">
                    <div class="imate-image">
                        <a href="<?php echo get_permalink(); ?>">
                            <?php
                            // Post thumbnail.
                            single_post_thumbnail('category-latest-post');
                            ?>
                        </a>
                    </div>
                    <h3 class="item-title"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h3>
                </div>
                <?php
            endwhile;
        wp_reset_query();
        endif;
        ?></div><?php
        echo $args['after_widget'];
    }

// Widget Backend
    public function form( $instance ) {
        if ( $instance ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = '';
        }
// Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <?php
    }

// Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }
}