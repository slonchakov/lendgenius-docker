<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 5/11/17
 * Time: 3:05 PM
 */

class sts_settings_page {

    function __construct() {
        add_action( 'admin_menu', array( $this, 'settings_page_menu' ) );
    }

    function settings_page_menu() {
        add_options_page(
            'Settings',
            'Settings',
            'manage_options',
            'sts-settings',
            array(
                $this,
                'settings_page_content'
            )
        );
    }

    function  settings_page_content() {
        ?>
        <h1>Settings page</h1>
        <h3>Social settings</h3>
        <?php
        if(!empty($_POST) && array_key_exists('twitter_url', $_POST)){
            if ( get_option( 'sts_twitter_url' ) !== false ) {

                // The option already exists, so we just update it.
                update_option( 'sts_twitter_url', $_POST['twitter_url'] );

            } else {
                add_option( 'sts_twitter_url', $_POST['twitter_url'] );
            }
        }

        if(!empty($_POST) && array_key_exists('facebook_url', $_POST)){
            if ( get_option( 'sts_facebook_url' ) !== false ) {

                // The option already exists, so we just update it.
                update_option( 'sts_facebook_url', $_POST['facebook_url'] );

            } else {
                add_option( 'sts_facebook_url', $_POST['facebook_url'] );
            }
        }

        if(!empty($_POST) && array_key_exists('gplus_url', $_POST)){
            if ( get_option( 'sts_gplus_url' ) !== false ) {

                // The option already exists, so we just update it.
                update_option( 'sts_gplus_url', $_POST['gplus_url'] );

            } else {
                add_option( 'sts_gplus_url', $_POST['gplus_url'] );
            }
        }

        if(!empty($_POST) && array_key_exists('instagram_url', $_POST)){
            if ( get_option( 'sts_instagram_url' ) !== false ) {

                // The option already exists, so we just update it.
                update_option( 'sts_instagram_url', $_POST['instagram_url'] );

            } else {
                add_option( 'sts_instagram_url', $_POST['instagram_url'] );
            }
        }

        if(!empty($_POST) && array_key_exists('linkedin_url', $_POST)){
            if ( get_option( 'sts_linkedin_url' ) !== false ) {

                // The option already exists, so we just update it.
                update_option( 'sts_linkedin_url', $_POST['linkedin_url'] );

            } else {
                add_option( 'sts_linkedin_url', $_POST['linkedin_url'] );
            }
        }

        if(!empty($_POST) && array_key_exists('youtube_url', $_POST)){
            if ( get_option( 'sts_youtube_url' ) !== false ) {

                // The option already exists, so we just update it.
                update_option( 'sts_youtube_url', $_POST['youtube_url'] );

            } else {
                add_option( 'sts_youtube_url', $_POST['youtube_url'] );
            }
        }

        if(!empty($_POST) && array_key_exists('lgc_phone_number', $_POST)){
            if ( get_option( 'lgc_phone_number' ) !== false ) {

                // The option already exists, so we just update it.
                update_option( 'lgc_phone_number', $_POST['lgc_phone_number'] );

            } else {
                add_option( 'lgc_phone_number', $_POST['lgc_phone_number'] );
            }
        }

        if(!empty($_POST) && array_key_exists('lgc_work_schedule_main', $_POST)){
            if ( get_option( 'lgc_work_schedule_main' ) !== false ) {

                // The option already exists, so we just update it.
                update_option( 'lgc_work_schedule_main', $_POST['lgc_work_schedule_main'] );

            } else {
                add_option( 'lgc_work_schedule_main', $_POST['lgc_work_schedule_main'] );
            }
        }

        if(!empty($_POST) && array_key_exists('lgc_phone_number_main', $_POST)){
            if ( get_option( 'lgc_phone_number_main' ) !== false ) {

                // The option already exists, so we just update it.
                update_option( 'lgc_phone_number_main', $_POST['lgc_phone_number_main'] );

            } else {
                add_option( 'lgc_phone_number_main', $_POST['lgc_phone_number_main'] );
            }
        }

        if(!empty($_POST)){
            $tef = '';
            if(array_key_exists('show_button', $_POST)){
                $tef = $_POST['show_button'];
            }
            if ( get_option( 'lg_show_button' ) !== false ) {

                // The option already exists, so we just update it.
                update_option( 'lg_show_button', $tef );

            } else {
                add_option( 'lg_show_button', $tef );
            }
        }

        if(!empty($_POST) && array_key_exists('lgc_work_schedule', $_POST)){
            if ( get_option( 'lgc_work_schedule' ) !== false ) {

                // The option already exists, so we just update it.
                update_option( 'lgc_work_schedule', $_POST['lgc_work_schedule'] );

            } else {
                add_option( 'lgc_work_schedule', $_POST['lgc_work_schedule'] );
            }
        }


        $twitter_url = get_option( 'sts_twitter_url' );
        $facebook_url = get_option( 'sts_facebook_url' );
        $gplus_url = get_option( 'sts_gplus_url' );
        $instagram_url = get_option( 'sts_instagram_url' );
        $linkedin_url = get_option( 'sts_linkedin_url' );
        $youtube_url = get_option( 'sts_youtube_url' );
        $phone_number  = get_option( 'lgc_phone_number' );
        $work_schedule  = get_option( 'lgc_work_schedule' );
        $phone_number_main  = get_option( 'lgc_phone_number_main' );
        $work_schedule_main  = get_option( 'lgc_work_schedule_main' );
        $show_button  = get_option( 'lg_show_button' );

        ?>
        <form method="POST">
            <table class="form-table">
                <tbody>
                <tr>
                    <th scope="row">Twitter URL</th>
                    <td><input type="text" name="twitter_url" value="<?php echo $twitter_url; ?>" style="width: 50%;"/></td>
                </tr>
                <tr>
                    <th scope="row">Facebook URL</th>
                    <td><input type="text" name="facebook_url" value="<?php echo $facebook_url; ?>" style="width: 50%;"/></td>
                </tr>
                <tr>
                    <th scope="row">Google+ URL</th>
                    <td><input type="text" name="gplus_url" value="<?php echo $gplus_url; ?>" style="width: 50%;"/></td>
                </tr>
                <tr>
                    <th scope="row">Instagram URL</th>
                    <td><input type="text" name="instagram_url" value="<?php echo $instagram_url; ?>" style="width: 50%;"/></td>
                </tr>
                <tr>
                    <th scope="row">Linkedin URL</th>
                    <td><input type="text" name="linkedin_url" value="<?php echo $linkedin_url; ?>" style="width: 50%;"/></td>
                </tr>
                <tr>
                    <th scope="row">Youtube URL</th>
                    <td><input type="text" name="youtube_url" value="<?php echo $youtube_url; ?>" style="width: 50%;"/></td>
                </tr>
                </tbody>
            </table>
            <hr>
            <h3>Header settings(Main)</h3>
            <table class="form-table">
                <tbody>
                <tr>
                    <th scope="row">Show 'See Loan Option' button</th>
                    <td>
                        <input type="checkbox" id="test_result" name="show_button" <?php echo ($show_button == 'on')? 'checked':''; ?> />
                    </td>
                </tr>
                <tr>
                    <th scope="row">Phone number</th>
                    <td><input type="text" name="lgc_phone_number_main" value="<?php echo $phone_number_main; ?>" style="width: 50%;"/></td>
                </tr>
                <tr>
                    <th scope="row">Work schedule</th>
                    <td><input type="text" name="lgc_work_schedule_main" value="<?php echo $work_schedule_main; ?>" style="width: 50%;"/></td>
                </tr>
                </tbody>
            </table>
            <h3>Header settings(Landing)</h3>
            <table class="form-table">
                <tbody>
                <tr>
                    <th scope="row">Phone number</th>
                    <td><input type="text" name="lgc_phone_number" value="<?php echo $phone_number; ?>" style="width: 50%;"/></td>
                </tr>
                <tr>
                    <th scope="row">Work schedule</th>
                    <td><input type="text" name="lgc_work_schedule" value="<?php echo $work_schedule; ?>" style="width: 50%;"/></td>
                </tr>
                </tbody>
            </table>
            <input type="submit" value="Save" class="button button-primary button-large">
        </form>
        <?php
    }
}

new sts_settings_page;