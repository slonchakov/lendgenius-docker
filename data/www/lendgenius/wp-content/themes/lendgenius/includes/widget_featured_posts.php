<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 24.10.16
 * Time: 13:51
 */


// Widget featured posts
class widget_featured_posts extends WP_Widget {

    function __construct() {
        parent::__construct(
            'wp_featured_posts_lend',
            __('Featured Posts', 'Divi-Child'),
            array( 'description' => __( 'Widget for Featured Posts', 'Divi-Child' ), )
        );
    }

// Creating widget front-end
    public function widget( $args, $instance ) {
        $title = apply_filters( 'widget_title', $instance['title'] );
        $include_picture = isset($instance[ 'include_picture' ]) ? $instance[ 'include_picture' ]:0;
        $include_description = isset($instance[ 'include_description' ]) ? $instance[ 'include_description' ]:0;
// before and after widget arguments are defined by themes
        echo $args['before_widget'];
        if ( ! empty( $title ) ):
            echo "<div class=\"item-title-primary\">{$title}</div>";
        endif;

        if (!empty($instance['featured_posts'])):
// This is where you run the code and display the output
            $args=array(
                'post_type' => 'post',
                'post__in' => $instance['featured_posts']
            );
            $fp_query = new WP_Query($args);
            $post_count = $fp_query->post_count;
            if( $fp_query->have_posts()  && $post_count > 0) :
                while ($fp_query->have_posts()) : $fp_query->the_post();
                    ?>
                    <h3 class="item-title"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h3>
                    <div class="item-author">by <strong><?php echo get_the_title(get_post_meta( get_the_ID() , 'author' , true )); ?></strong> on <?php echo get_the_date(); ?></div>
                    <?php if($include_picture): ?>
                        <div class="imate-image">
                            <a href="<?php echo get_permalink(); ?>">

                                <img class="img-lazy" src="<?= imgpath(get_stylesheet_directory_uri() . '/style-2018/img/empty.png?v=3') ?>" data-src="<?= imgpath(get_the_post_thumbnail_url(get_the_ID(), 'sidebar-thumb')) ?>" alt="">

                                <?php
                                // Post thumbnail.
                                // single_post_thumbnail('sidebar-thumb');
                                ?>
                            </a>
                        </div>
                    <?php endif; ?>
                    <?php if($include_description): ?>
                        <div class="item-text"><?php echo wp_strip_all_tags(wp_trim_words(get_the_content(), 20)); ?> <a class="item-readmore" href="<?php echo get_permalink(); ?>">Read more</a></div>
                    <?php endif; ?>
                    <?php
                endwhile;
            endif;
            wp_reset_query();
        endif;
        ?></div><?php
        echo $args['after_widget'];
    }

// Widget Backend
    public function form( $instance ) {
        if ( $instance ) {
            $title = $instance[ 'title' ];
            $featured_posts = $instance[ 'featured_posts' ];
            $include_picture = $instance[ 'include_picture' ];
            $include_description = $instance[ 'include_description' ];
        }
        else {
            $title = '';
            $featured_posts = array();
            $include_picture = 0;
            $include_description = 0;
        }
// Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'featured_posts' ); ?>"><?php _e( 'Featured Posts:' ); ?></label>
            <select class="widefat" name="<?php echo $this->get_field_name( 'featured_posts' ); ?>[]" id="<?php echo $this->get_field_id( 'featured_posts' ); ?>" multiple="multiple" size="10" >
                <?php
                global $post;
                $args = array(
                    'numberposts' => -1,
                    'orderby'   => 'title',
                    'order' => 'ASC'
                );
                $posts = get_posts($args);
                foreach( $posts as $post ) : setup_postdata($post); ?>
                    <option value="<?php echo $post->ID; ?>" <?php echo in_array( $post->ID, $featured_posts) ? 'selected="selected"' : '' ?>><?php the_title(); ?></option>
                <?php endforeach; ?>
            </select>
        </p>
        <p>
            <input class="widefat" id="<?php echo $this->get_field_id( 'include_picture' ); ?>" name="<?php echo $this->get_field_name( 'include_picture' ); ?>" type="checkbox" <?php echo ($include_picture) ? 'checked':'' ?> />
            <label for="<?php echo $this->get_field_id( 'include_picture' ); ?>"><?php _e( 'Include picture' ); ?></label>
        </p>
        <p>
            <input class="widefat" id="<?php echo $this->get_field_id( 'include_description' ); ?>" name="<?php echo $this->get_field_name( 'include_description' ); ?>" type="checkbox" <?php echo ($include_description) ? 'checked':'' ?> />
            <label for="<?php echo $this->get_field_id( 'include_description' ); ?>"><?php _e( 'Include description' ); ?></label>
        </p>
        <?php
    }

// Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['featured_posts'] = ( ! empty( $new_instance['featured_posts'] ) ) ? $new_instance['featured_posts'] : array();
        $instance['include_picture'] = ( isset($new_instance['include_picture']) && ! empty( $new_instance['include_picture'] ) ) ? 1 : 0;
        $instance['include_description'] = ( isset($new_instance['include_description']) && ! empty( $new_instance['include_description'] ) ) ? 1 : 0;
        return $instance;
    }
}