<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 08.05.18
 *
 */

class WidgetJsfForm extends WP_Widget
{

    function __construct() {
        parent::__construct(
            'widget_jsf_form',
            __('Embeded form'),
            array( 'description' => __( 'Widget for Embede Form on Sidebar' ), )
        );
    }

    public function widget($args, $instance)
    {
        get_template_part('views/parts/jsf-form');

    }

    public function form($instance)
    {

    }
}