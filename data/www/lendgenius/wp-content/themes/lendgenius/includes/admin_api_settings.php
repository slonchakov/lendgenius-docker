<?php
/**
 * Created by PhpStorm.
 * User: slan
 * Date: 13.12.16
 * Time: 17:10
 */

class api_settings_page {

    function __construct() {
        add_action( 'admin_menu', array( $this, 'api_settings_page_menu' ) );
    }

    function api_settings_page_menu() {
        add_options_page(
            'API Settings',
            'API Settings',
            'manage_options',
            'sts-api-settings',
            array(
                $this,
                'api_settings_page_content'
            )
        );
    }

    function  api_settings_page_content() {
        ?>
        <h1>API Settings</h1>
        <h3>LeadsMarket</h3>
        <?php
        if(!empty($_POST) && array_key_exists('api_url', $_POST)){
            if ( get_option( 'sts_api_url' ) !== false ) {

                // The option already exists, so we just update it.
                update_option( 'sts_api_url', $_POST['api_url'] );

            } else {
                add_option( 'sts_api_url', $_POST['api_url'] );
            }
        }

        if(!empty($_POST) && array_key_exists('company_id', $_POST)){
            if ( get_option( 'sts_company_id' ) !== false ) {

                // The option already exists, so we just update it.
                update_option( 'sts_company_id', $_POST['company_id'] );

            } else {
                add_option( 'sts_company_id', $_POST['company_id'] );
            }
        }

        if(!empty($_POST) && array_key_exists('company_key', $_POST)){
            if ( get_option( 'sts_company_key' ) !== false ) {

                // The option already exists, so we just update it.
                update_option( 'sts_company_key', $_POST['company_key'] );

            } else {
                add_option( 'sts_company_key', $_POST['company_key'] );
            }
        }

        if(!empty($_POST) && !array_key_exists('reset_cache', $_POST)){
            $tval = '';
            if(array_key_exists('test_result', $_POST)){
                $tval = $_POST['test_result'];
            }
            if ( get_option( 'sts_test_result' ) !== false ) {

                // The option already exists, so we just update it.
                update_option( 'sts_test_result', $tval );

            } else {
                add_option( 'sts_test_result', $tval );
            }
        }

        if(!empty($_POST) && array_key_exists('test_result_value', $_POST)){
            if ( get_option( 'sts_test_result_value' ) !== false ) {

                // The option already exists, so we just update it.
                update_option( 'sts_test_result_value', $_POST['test_result_value'] );

            } else {
                add_option( 'sts_test_result_value', $_POST['test_result_value'] );
            }
        }

        if(!empty($_POST) && array_key_exists('reject_page', $_POST)){
            if ( get_option( 'sts_reject_page' ) !== false ) {

                // The option already exists, so we just update it.
                update_option( 'sts_reject_page', $_POST['reject_page'] );

            } else {
                add_option( 'sts_reject_page', $_POST['reject_page'] );
            }
        }

        if(!empty($_POST) && !array_key_exists('reset_cache', $_POST)){
            $tef = '';
            if(array_key_exists('enable_form', $_POST)){
                $tef = $_POST['enable_form'];
            }
            if ( get_option( 'sts_enable_form' ) !== false ) {

                // The option already exists, so we just update it.
                update_option( 'sts_enable_form', $tef );

            } else {
                add_option( 'sts_enable_form', $tef );
            }
        }

        if(!empty($_POST) && array_key_exists('script_url', $_POST)){
            if ( get_option( 'sts_script_url' ) !== false ) {

                // The option already exists, so we just update it.
                update_option( 'sts_script_url', $_POST['script_url'] );

            } else {
                add_option( 'sts_script_url', $_POST['script_url'] );
            }

            if (get_option( 'sts_script_urls' )) {

                $script_urls_array = json_decode(get_option( 'sts_script_urls' ));

                if(!in_array($_POST['script_url'],$script_urls_array)) {
                    $script_urls_array[] = $_POST['script_url'];
                }

                // The option already exists, so we just update it.
                update_option( 'sts_script_urls', json_encode($script_urls_array));

            } else {

                $script_urls_array = json_decode($_POST[ 'script_urls'] );
                if(!in_array($_POST['script_url'],$script_urls_array)) {
                    $script_urls_array[] = $_POST['script_url'];
                }

                // The option already exists, so we just update it.
                add_option( 'sts_script_urls', json_encode($script_urls_array));

            }


        }

        if(!empty($_POST) && array_key_exists('campaignId', $_POST)){
            if ( get_option( 'sts_campaignId' ) !== false ) {

                // The option already exists, so we just update it.
                update_option( 'sts_campaignId', $_POST['campaignId'] );

            } else {
                add_option( 'sts_campaignId', $_POST['campaignId'] );
            }
        }

        if(!empty($_POST) && array_key_exists('leadSource', $_POST)){
            if ( get_option( 'sts_leadSource' ) !== false ) {

                // The option already exists, so we just update it.
                update_option( 'sts_leadSource', $_POST['leadSource'] );

            } else {
                add_option( 'sts_leadSource', $_POST['leadSource'] );
            }
        }

        if(!empty($_POST) && array_key_exists('endpoint_url', $_POST)){
            if ( get_option( 'sts_endpoint_url' ) !== false ) {

                // The option already exists, so we just update it.
                update_option( 'sts_endpoint_url', $_POST['endpoint_url'] );

            } else {
                add_option( 'sts_endpoint_url', $_POST['endpoint_url'] );
            }

            if (get_option( 'sts_endpoint_urls' )) {

                $endpoint_urls_array = json_decode(get_option( 'sts_endpoint_urls' ));

                if(!in_array($_POST['endpoint_url'],$endpoint_urls_array)) {
                    $endpoint_urls_array[] = $_POST['endpoint_url'];
                }

                // The option already exists, so we just update it.
                update_option( 'sts_endpoint_urls', json_encode($endpoint_urls_array));

            } else {

                $endpoint_urls_array = json_decode($_POST[ 'endpoint_urls'] );
                if(!in_array($_POST['endpoint_url'],$endpoint_urls_array)) {
                    $endpoint_urls_array[] = $_POST['endpoint_url'];
                }

                // The option already exists, so we just update it.
                add_option( 'sts_endpoint_urls', json_encode($endpoint_urls_array));

            }

        }

        if(!empty($_POST) && array_key_exists('zipcode', $_POST)){
            if ( get_option( 'sts_zipcode' ) !== false ) {

                // The option already exists, so we just update it.
                update_option( 'sts_zipcode', $_POST['zipcode'] );

            } else {
                add_option( 'sts_zipcode', $_POST['zipcode'] );
            }
        }

        if(!empty($_POST) && array_key_exists('email_v_url', $_POST)){
            if ( get_option( 'sts_email_v_url' ) !== false ) {

                // The option already exists, so we just update it.
                update_option( 'sts_email_v_url', $_POST['email_v_url'] );

            } else {
                add_option( 'sts_email_v_url', $_POST['email_v_url'] );
            }
        }

        if(!empty($_POST) && array_key_exists('phone_v_url', $_POST)){
            if ( get_option( 'sts_phone_v_url' ) !== false ) {
                // The option already exists, so we just update it.
                update_option( 'sts_phone_v_url', $_POST['phone_v_url'] );

            } else {
                add_option( 'sts_phone_v_url', $_POST['phone_v_url'] );
            }
        }

        if(!empty($_POST) && array_key_exists('reset_cache', $_POST)){
            $script_version = time();
            if ( get_option( 'sts_script_version' ) !== false ) {
                // The option already exists, so we just update it.
                update_option( 'sts_script_version', $script_version );

            } else {
                add_option( 'sts_script_version', $script_version );
            }
        }

        if(!empty($_POST) && array_key_exists('see_loan_option_url', $_POST)){
            if ( get_option( 'see_loan_option_url' ) !== false ) {
                // The option already exists, so we just update it.
                update_option( 'see_loan_option_url', $_POST['see_loan_option_url'] );
            } else {
                add_option( 'see_loan_option_url', $_POST['see_loan_option_url'] );
            }
        }

        if(!empty($_POST) && array_key_exists('google_tag_manager', $_POST)){
            if ( get_option( 'google_tag_manager' ) !== false ) {

                // The option already exists, so we just update it.
                update_option( 'google_tag_manager', $_POST['google_tag_manager'] );

            } else {
                add_option( 'google_tag_manager', $_POST['google_tag_manager'] );
            }
        }

        if(!empty($_POST) && array_key_exists('google_tag_manager_noscript', $_POST)){
            if ( get_option( 'google_tag_manager_noscript' ) !== false ) {

                // The option already exists, so we just update it.
                update_option( 'google_tag_manager_noscript', $_POST['google_tag_manager_noscript'] );

            } else {
                add_option( 'google_tag_manager_noscript', $_POST['google_tag_manager_noscript'] );
            }
        }

        if(!empty($_POST) && array_key_exists('breadcrumb_router', $_POST)){
            if ( get_option( 'breadcrumb_router' ) !== false ) {

                // The option already exists, so we just update it.
                update_option( 'breadcrumb_router', $_POST['breadcrumb_router'] );

            } else {
                add_option( 'breadcrumb_router', $_POST['breadcrumb_router'] );
            }
        }

        $api_url = get_option( 'sts_api_url' );
        $company_id = get_option( 'sts_company_id' );
        $company_key = get_option( 'sts_company_key' );
        $test_result = get_option( 'sts_test_result' );
        $test_result_value = get_option( 'sts_test_result_value' );
        $reject_page = get_option( 'sts_reject_page' );

        $enable_form = get_option( 'sts_enable_form' );
        $script_url = get_option( 'sts_script_url' );
        $campaignId = get_option('sts_campaignId');
        $leadSource = get_option('sts_leadSource');
        $endpoint_url = get_option( 'sts_endpoint_url' );
        $zipcode = get_option( 'sts_zipcode' );
        $email_v_url = get_option( 'sts_email_v_url' );
        $phone_v_url = get_option( 'sts_phone_v_url' );

        $see_loan_option_url = get_option( 'see_loan_option_url' );

        $google_tag_manager = stripcslashes(get_option( 'google_tag_manager' ));
        $google_tag_manager_noscript = stripcslashes(get_option( 'google_tag_manager_noscript' ));
        
        $breadcrumb_router = stripcslashes(get_option( 'breadcrumb_router' ));

        $all_pages = array();

        $p_args = array(
            'post_type' => 'page',
            'post_status' => 'publish',
            'posts_per_page' => -1
        );
        $p_query = new WP_Query( $p_args );
        if ( $p_query->have_posts() ) {
            // The Loop
            while ( $p_query->have_posts() ) : $p_query->the_post();
                $all_pages[get_the_id()] = get_the_title();
            endwhile;
            wp_reset_postdata();
        }

        $script_urls = array(
            'http://lendgeniusfrontenddevelopment.azurewebsites.net',
            'https://stage.jslibrary.io',
            'https://jslibrary.io'
        );

        if (! get_option('sts_script_urls') ) {

            add_option( 'sts_script_urls', json_encode($script_urls));
        } else {

            $script_urls = json_decode(get_option('sts_script_urls'));
        }

        $endpoint_urls = array(
            'http://lendgeniusleadapidevelopment.azurewebsites.net/api/businessloan',
            'https://api-stage.lendgenius.com/api/BusinessLoan',
            'https://api.lendgenius.com/api/BusinessLoan'
        );

        if (! get_option('sts_endpoint_urls') ) {

            add_option( 'sts_endpoint_urls', json_encode($endpoint_urls));
        } else {

            $endpoint_urls = json_decode(get_option('sts_endpoint_urls'));
        }


        ?>
        <form method="POST">
        <table class="form-table">
            <tbody>
            <tr>
                <th scope="row">Api URL</th>
                <td><input type="text" name="api_url" value="<?php echo $api_url; ?>" style="width: 50%;"/></td>
            </tr>
            <tr>
                <th scope="row">Company id</th>
                <td><input type="text" name="company_id" value="<?php echo $company_id; ?>" style="width: 50%;"/></td>
            </tr>
            <tr>
                <th scope="row">Company key</th>
                <td><input type="text" name="company_key" value="<?php echo $company_key; ?>" style="width: 50%;"/></td>
            </tr>
            <tr>
                <th scope="row">TestResult</th>
                <td>
                    <input type="checkbox" id="test_result" name="test_result" <?php echo ($test_result == 'on')? 'checked':''; ?> />
                    <input type="text" name="test_result_value" value="<?php echo $test_result_value; ?>" style="width: 50%; <?php echo ($test_result == 'on')? '':'display: none;'; ?>"/>
                </td>
            </tr>
            <tr>
                <th scope="row">Reject page</th>
                <td>
                    <select name="reject_page" style="width: 50%;">
                        <option disabled <?php echo (empty($reject_page))? 'selected':'' ?>>Please, choose page</option>
                        <?php foreach($all_pages as $key => $value){ ?>
                            <option value="<?php echo $key; ?>" <?php echo ($key == $reject_page)? 'selected':'' ?>><?php echo $value; ?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
            </tbody>
        </table>
            <h1>LG Settings</h1>
            <table class="form-table">
                <tbody>
                <tr>
                    <th scope="row">See Loan Options URL</th>
                    <td>
                        <input type="text" name="see_loan_option_url" value="<?php echo $see_loan_option_url; ?>" style="width: 50%;"/>
                    </td>
                </tr>

                <tr>
                    <th scope="row">Google tag manager</th>
                    <td>
                        <textarea name="google_tag_manager" style="width:50%; height:60%;"><?= $google_tag_manager; ?></textarea>
                    </td>
                </tr>

                <tr>
                    <th scope="row">Google tag manager noiscript</th>
                    <td>
                        <textarea name="google_tag_manager_noscript" style="width:50%; height:60%;"><?= $google_tag_manager_noscript; ?></textarea>
                    </td>
                </tr>

                <tr>
                    <th scope="row">Use menu for breadcrumbs</th>
                    <td>
                        <select name="breadcrumb_router">
                        <?php

                        $nav_menus = wp_get_nav_menus();
                        foreach ($nav_menus as $nav_menu) :
                        ?>
                            <option <?php if($breadcrumb_router == $nav_menu->name) {?>selected="selected" <?php } ?>value="<?= $nav_menu->name ;?>"><?= $nav_menu->name ;?></option>
                        <?php
                        endforeach;
                        ?>
                        </select>
                    </td>
                </tr>

                </tbody>
            </table>
            <h1>Front form</h1>
            <table class="form-table">
                <tbody>
                <tr>
                    <th scope="row">Enable form</th>
                    <td>
                        <input type="checkbox" id="enable_form" name="enable_form" <?php echo ($enable_form == 'on')? 'checked':''; ?> />
                    </td>
                </tr>
                <tr>
                    <th scope="row">Script Url</th>
                    <td>

                        <div class="box-form" style="width: 50%;">

                            <input type="text" name="script_url" value="<?= $script_url; ?>"  style="width: 100%;" />
                            
                            <div class="open-list-button">...</div>

                            <div class="url-list">

                                <input type="hidden" name="script_urls" value='<?= json_encode($script_urls ); ?>'>
                                <ul>
                                    <?php foreach ($script_urls as $place => $url) : ?>
                                    <li data-value="<?= $url; ?>" <?php if ($script_url === $url) { echo 'class="selected"'; } ?>><?= $url; ?></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>

                    </td>
                </tr>
                <tr>
                    <th scope="row">Campaign Id</th>
                    <td>
                        <input type="text" name="campaignId" value="<?php echo $campaignId; ?>" style="width: 50%;"/>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Default Lead Source</th>
                    <td>
                        <input type="text" name="leadSource" value="<?php echo $leadSource; ?>" style="width: 50%;"/>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Endpoint Url</th>
                    <td>

                        <div class="box-form" style="width: 50%;">

                            <input type="text" name="endpoint_url" value="<?= $endpoint_url; ?>"  style="width: 100%;" />

                            <div class="open-list-button">...</div>

                            <div class="url-list">

                                <input type="hidden" name="endpoint_urls" value='<?= json_encode($endpoint_urls ); ?>'>
                                <ul>
                                    <?php foreach ($endpoint_urls as $place => $url) : ?>
                                        <li data-value="<?= $url; ?>" <?php if ($endpoint_urls === $url) { echo 'class="selected"'; } ?>><?= $url; ?></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>


                    </td>
                </tr>
                <tr>
                    <th scope="row">Zip Code</th>
                    <td>
                        <input type="text" name="zipcode" value="<?php echo $zipcode; ?>" style="width: 50%;"/>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Email Validate Url</th>
                    <td>
                        <input type="text" name="email_v_url" value="<?php echo $email_v_url; ?>" style="width: 50%;"/>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Phone Validate Url</th>
                    <td>
                        <input type="text" name="phone_v_url" value="<?php echo $phone_v_url; ?>" style="width: 50%;"/>
                    </td>
                </tr>
                </tbody>
            </table>
            <input type="submit" value="Save" class="button button-primary button-large">
        </form>
        <form method="POST">
            <h1>Form cache</h1>
            <input type="hidden" name="reset_cache" value="1" />
            <input type="submit" value="Reset Form Cache" class="button button-primary button-large">
        </form>
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                jQuery('#test_result').click(function(){
                    if(jQuery(this).prop( "checked" )){
                        jQuery('[name="test_result_value"]').show();
                    }else{
                        jQuery('[name="test_result_value"]').hide();
                    }
                });
            });
        </script>

        <style>
            .box-form {
                position: relative;
                padding-right: 26px;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
            }
            .box-form .open-list-button {
                position: absolute;
                top: 1px;
                right: 0;
                width: 22px;
                height: 22px;
                border: 1px solid #ddd;
                background: #ffffff;
                text-align: center;
                cursor: pointer;
                line-height:24px;

                -webkit-touch-callout: none; /* iOS Safari */
                -webkit-user-select: none; /* Safari */
                -khtml-user-select: none; /* Konqueror HTML */
                -moz-user-select: none; /* Firefox */
                -ms-user-select: none; /* Internet Explorer/Edge */
                user-select: none;
            }
            .url-list {
                width: calc(100% - 30px);
                position: absolute;
                top: 100%;
                left: 0;
                background: #ffffff;
                border: 1px solid #ddd;
                display: none;
            }
            .url-list.active {
                 display: block;
             }


            .url-list ul {
                margin: 0;
            }

            .url-list li {
                padding: 4px 5px;
                cursor: pointer;
                margin-bottom: 1px;
            }
            .url-list li.selected,
            .url-list li:hover {
                color: #ffffff;
                background: #0073aa;
            }

        </style>
        <script>
            (function($){

                function close(elem) {
                    elem.removeClass('active');
                }

                $('.open-list-button').on('click',function(){
                    var $button  = $(this),
                        urlInput = $button.siblings('input'),
                        urlList  = $button.siblings('.url-list');

                    urlList.toggleClass('active');

                    urlInput.focus(function(){close(urlList)});

                    $('li',urlList).off().on('click',function(){
                        var liElem = $(this);

                        liElem.addClass('selected').siblings().removeClass('selected');

                        urlInput.val(liElem.data('value'));
                        close(urlList);
                    });
                });

                $(document).on('click',function(e){
                    if(!$(e.target).closest('.box-form').length) {
                        close($('.url-list'));
                    }
                })

            })(jQuery);

        </script>
        <?php
    }
}

new api_settings_page;