<?php
/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 03.11.17
 *
 */

add_action('admin_enqueue_scripts', function(){
    wp_enqueue_media();
});

add_action( 'admin_menu', 'admin_redirect_settings_menu' );

function admin_redirect_settings_menu() {
    add_options_page(
        'Redirects',
        'Redirect Settings',
        'manage_options',
        'redirect-settings',
        'admin_redirect_settings_content'
    );
}

function  admin_redirect_settings_content() {

    if ($_SERVER['REQUEST_METHOD'] == 'POST' && $_POST['redirect_settings'] === null) delete_option( 'redirect_settings');

    if (!empty($_POST['redirect_settings'])) {

        foreach ($_POST['redirect_settings'] as $key => $redirectItem) {

            if ($redirectItem['name'] == '' || $redirectItem['link'] == '' ) {
                unset($_POST['redirect_settings'][$key]);
            }
        }

        update_option( 'redirect_settings', $_POST['redirect_settings'] );
    }

    $redirects = get_option('redirect_settings');
    $new_item  = (is_array($redirects)) ? end(array_keys($redirects)) : 0;

    ?>
    <style>
        form {
            width: 50%;
        }
        tr {
            border-bottom: 1px solid #d7d7d7;
        }
        input {
            width: 100%;
            display: block;
        }
        .container-fluid {
            padding-top: 15px;
            padding-left: 10px;
        }

        td:first-child {
            width: 25%;
        }

        td:nth-child(2) {
            width: 20%;
        }
        td:nth-child(3) {
            width: 36%;
        }
        td:nth-child(4) {
            width: 26%;
            text-align: center;
        }

        td:last-child {
            width: 4%;
        }

        .row_delete {
            margin-top: 6px;
        }
    </style>
    <h1>Links for redirects</h1>

    <form method="POST" style="width: 70%;">
        <table class="form-table">
            <tbody>
            <?php if (is_array($redirects)) : ?>
                <?php foreach ($redirects as $key => $redirect) : ?>
                    <tr>
                        <td>
                            <label for="">Link name</label>
                            <input type="text" name="redirect_settings[<?= $key;?>][name]" value="<?= $redirect['name'] ?>" />
                        </td>
                        <td>
                            <label for="">Link slug</label>
                            <input type="text" name="redirect_settings[<?= $key;?>][slug]" value="<?= $redirect['slug'] ?>" />
                        </td>
                        <td>
                            <label for="">Link address</label>
                            <input type="text" name="redirect_settings[<?= $key;?>][link]" value="<?= $redirect['link'] ?>" />
                        </td>
                        <td>
                            <br>
                            <input type="hidden" name="redirect_settings[<?= $key;?>][logo]" value="<?= $redirect['logo'] ?>" />
                            <?php if (!empty($redirect['logo'])) : ?>
                                <img style="width:100%" src="<?= $redirect['logo']; ?>" alt="">
                            <?php endif; ?>
                            <button class="button redirect-logo-upload">Attach Logo</button>
                        </td>
                        <td><br><button class="row_delete button-secondary" >X</button></td>
                    </tr>
                <?php endforeach; ?>
            <?php else: ?>
            <tr>
                <td><label for="">Link name</label><input type="text" name="redirect_settings[<?= $new_item; ?>][name]" value="" /></td>
                <td><label for="">Link slug</label><input type="text" name="redirect_settings[<?= $new_item; ?>][slug]" value="" /></td>
                <td><label for="">Link address</label><input type="text" name="redirect_settings[<?= $new_item; ?>][link]" value="" /></td>
                <td><br><input type="hidden" name="redirect_settings[<?= $new_item;?>][logo]" value="" />
                    <button class="button redirect-logo-upload">Attach Logo</button></td>
                <td><br><button disabled class="row_delete button-secondary" >X</button></td>
            </tr>
            <?php endif; ?>
            </tbody>
            <thead>
            <tr>
                <td colspan="5"><button type="submit" class="button button-primary">Save</button></td>
            </tr>
            </thead>
            <tfoot>
                <tr>
                    <td colspan="5"><button class="button button-secondary btn-add-row">Add link</button></td>
                </tr>
            </tfoot>
        </table>
        <div class="container-fluid">
            <button type="submit" class="button button-primary">Save</button>
        </div>
    </form>
    <script>
        jQuery(document).ready(function($){
            var newItem = <?= ++$new_item; ?>,
                rowItem = '<tr>'+
                            '<td><label for="">Link name</label><input type="text" name="redirect_settings[{{itemKey}}][name]" value="" /></td>'+
                            '<td><label for="">Link slug</label><input type="text" name="redirect_settings[{{itemKey}}][slug]" value="" /></td>'+
                            '<td><label for="">Link address</label><input type="text" name="redirect_settings[{{itemKey}}][link]" value="" /></td>'+
                            '<td><br><input type="hidden" name="redirect_settings[{{itemKey}}][logo]" value="" />'+
                            '<button class="button redirect-logo-upload">Attach Logo</button></td>'+
                            '<td><br /><button class="row_delete button-secondary" >X</button></td>'+
                          '</tr>';

            $('.btn-add-row').on('click', function(e){
                e.preventDefault();

                $('tbody').append(rowItem.replace(/\{\{itemKey}}/g, newItem));

                newItem++;
            });

            $('form').on('click', '.row_delete', function(e){
                e.preventDefault();

                $(e.target).parents('tr').remove();
            });
        });

        jQuery(document).ready(function($){

            var custom_uploader;

            $('form').on('click', '.redirect-logo-upload', function(e){
                e.preventDefault();
                var button = $(e.target),
                    input = button.siblings('input');

                if (custom_uploader) {
                    custom_uploader.open();
                    return;
                }
                //Extend the wp.media object
                custom_uploader = wp.media.frames.file_frame = wp.media({
                    title: 'Choose Image',
                    button: {
                        text: 'Choose Image'
                    },
                    multiple: false
                });

                custom_uploader.on('select', function() {
                    attachment = custom_uploader.state().get('selection').first().toJSON();
                    input.val(attachment.url);
                    button.siblings('img').remove();
                    button.before('<img style="width:100%" src="'+ attachment.url +'" />')
                });
                //Open the uploader dialog
                custom_uploader.open();
            });
        });
    </script>
    <?php
}