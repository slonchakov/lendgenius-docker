<?php

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 08.08.17
 *
 */
class DBQ
{

    private $type = 'SELECT';
    private $table;
    private $fields = ' * ';
    private $where = '';
    private $order = 'ASC';
    private $order_by;
    private $query;
    private $limit;

    public $cache_file = WPMU_PLUGIN_DIR.'/DBQ/cache/dbq_cache.json';

    public function __construct($table= null)
    {
        if (isset ($_GET['clear_cache'])) {
            DBQCache::clear();

            die();
        }
        if (null !== $table)
            $this->table = $table;
    }

    public function from($table)
    {
        $this->table = $table;
        return $this;
    }

    public function setFields($fields)
    {
        $this->fields = $fields;

        return $this;
    }

    public function where($where)
    {
        $this->where = $where;

        return $this;
    }

    public function order($order)
    {
        $this->order = $order;

        return $this;
    }

    public function order_by($order_by)
    {
        $this->order_by = $order_by;

        return $this;
    }

    public function limit($limit)
    {
        $this->limit = $limit;

        return $this;
    }

    public function query()
    {
        global $wpdb;

        $query = "SELECT $this->fields FROM ". $wpdb->prefix ."$this->table ";

        if ($this->where != false) {

            $query .= " WHERE $this->where AND post_status='publish' ";
        }
        
        if ($this->order_by != false) {

            $query .= " ORDER BY $this->order_by $this->order ";
        }

        if ($this->limit != false) {

            $query .= " LIMIT $this->limit ";
        }

//        if ( $cache = DBQCache::cache() ) {
//
//            if ($cache->$query)
//                return $cache->$query;
//        }
        $result = $wpdb->get_results($query);
        //$this->toCache($query, $result);

        //var_dump($result);
        return $result;
    }
    
    public function toCache($query, $result)
    {
        $cache = DBQCache::cache();
        
        if ( $cache->$query !== $result ) {

            $cache->$query = $result;
        }
//
//        if(!file_put_contents($this->cache_file,json_encode($cache))) {
//            //echo "not wrote!";
//        }
        
    }
    


}