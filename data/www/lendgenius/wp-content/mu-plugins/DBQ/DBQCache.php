<?php

/**
 *
 * Author: sergey.s
 * Email: sergey.s@lendgenius.com
 * Date: 08.08.17
 *
 */
class DBQCache
{

    public static function cache()
    {
        $cache_file = WPMU_PLUGIN_DIR.'/DBQ/cache/dbq_cache.json';
        $cache = json_decode(file_get_contents($cache_file));

        return $cache;
    }

    public static function clear()
    {
        echo "Here";
        $cache_file = WPMU_PLUGIN_DIR.'/DBQ/cache/dbq_cache.json';
        if (!file_put_contents($cache_file,'')) {
            echo "I can't purge cache";
        }
    }

}