<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<script type='text/javascript'>
    if (!window.lmpost) window.lmpost = {};

    window.lmpost.options = {
        "campaignKey": '8A20B784-A3E2-4D32-8ACE-58D1F821C099',
        "declinedURL": "",
        "errorURL": "",
        "testResult": "",
        "payday": {
            "NSDomain": ""
        },
        "campaignid": <?= $_GET['campid']; ?>,
        "leadtypeid": 19,
        "progressPlugin": {
            "initialPercentage": 0
        },
        "domain": "https://formrequests.com/paydayv3/",
        "form": "../1Question_form_v7_new_amount"
    };
    //        document.write('<script id="leadsb2cformsrc" type="text/javascript" src="https://formrequests.com/paydayv3/scripts/forms.core.min.js"><\/script>');

</script>
<script id="leadsb2cformsrc" type="text/javascript" src="https://formrequests.com/paydayv3/scripts/forms.core.min.js"></script>

<style>
    .b2cform {
        z-index: 1;
    }

    .b2c-form .b2c-btn-submit,
    .b2c-form .b2c-btn-verify,
    .b2c-form a.b2c-btn:not(.b2c-btn-back) {
        background: #0065B3;
        color: #ffffff;
    }

    .b2c-form .b2c-btn-submit:hover,
    .b2c-form .b2c-btn-verify:hover {
        background: #003a67;
        color: #ffffff;
    }

    .b2c-progress-bar-wrap .b2c-progress-bar .b2c-progress-bar-line,
    .b2c-step .b2c-row .b2c-radio-row label span {
        background: #0065B3;
        color: #ffffff;
    }

    .b2c-returned select,
    .b2c-returned .b2c-row input,
    .b2c-returned .b2c-title-returned,
    .b2c-step .b2c-row .b2c-hint,
    .b2c-step .b2c-row input, .b2c-step .b2c-row select{
        border-color: #0065B3 !important;
    }

    .b2c-returned select.b2c-valid,
    .b2c-step .b2c-row .b2c-valid:not(.b2c-radio-wrap):not(.b2c-dp-element):not(select):not(.b2c-aba-loader),
    .b2c-returned .b2c-row input.b2c-valid,
    .b2c-step .b2c-row select.b2c-valid {
        border-color: #003a67 !important;
    }

    .b2c-step .b2c-row .b2c-radio-row label :checked+span {
        color: #fff;
        background-color: #003a67;
        border-color: #003a67;
    }

    .b2c-step .b2c-row .b2c-radio-row label span:after {
        background: none;
    }
</style>

</body>
</html>