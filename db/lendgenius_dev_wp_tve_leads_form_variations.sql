-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: lendgenius_dev
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wp_tve_leads_form_variations`
--

DROP TABLE IF EXISTS `wp_tve_leads_form_variations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_tve_leads_form_variations` (
  `key` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `post_parent` bigint(20) DEFAULT NULL,
  `post_status` varchar(20) DEFAULT 'publish',
  `post_title` text,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `trigger` varchar(64) DEFAULT NULL,
  `trigger_config` text,
  `tcb_fields` longtext,
  `display_frequency` int(11) DEFAULT '0',
  `display_animation` varchar(64) DEFAULT 'instant',
  `position` varchar(32) DEFAULT NULL,
  `form_state` varchar(64) DEFAULT NULL,
  `parent_id` int(11) DEFAULT '0',
  `state_order` int(11) DEFAULT '0',
  `cache_impressions` bigint(20) DEFAULT NULL,
  `cache_conversions` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`key`),
  KEY `post_status` (`post_status`),
  KEY `post_parent` (`post_parent`),
  KEY `parent_id` (`parent_id`),
  KEY `state_order` (`state_order`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_tve_leads_form_variations`
--

LOCK TABLES `wp_tve_leads_form_variations` WRITE;
/*!40000 ALTER TABLE `wp_tve_leads_form_variations` DISABLE KEYS */;
INSERT INTO `wp_tve_leads_form_variations` VALUES (1,'2018-03-27 00:12:12','2018-03-27 00:12:12',2966,'publish','Newsletter','','click','a:0:{}','a:0:{}',0,'instant','bot_right',NULL,0,0,0,0),(2,'2018-03-27 00:14:31','2018-03-27 00:14:31',2968,'publish','Newsletter','','page_load','a:0:{}','a:0:{}',0,'slide_top','top',NULL,0,0,0,0);
/*!40000 ALTER TABLE `wp_tve_leads_form_variations` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-01 18:52:32
