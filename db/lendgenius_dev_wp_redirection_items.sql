-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: lendgenius_dev
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wp_redirection_items`
--

DROP TABLE IF EXISTS `wp_redirection_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_redirection_items` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `url` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `match_url` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `match_data` text COLLATE utf8mb4_unicode_ci,
  `regex` int(11) unsigned NOT NULL DEFAULT '0',
  `position` int(11) unsigned NOT NULL DEFAULT '0',
  `last_count` int(10) unsigned NOT NULL DEFAULT '0',
  `last_access` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `group_id` int(11) NOT NULL DEFAULT '0',
  `status` enum('enabled','disabled') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'enabled',
  `action_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action_code` int(11) unsigned NOT NULL,
  `action_data` mediumtext COLLATE utf8mb4_unicode_ci,
  `match_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `url` (`url`(191)),
  KEY `status` (`status`),
  KEY `regex` (`regex`),
  KEY `group_idpos` (`group_id`,`position`),
  KEY `group` (`group_id`),
  KEY `match_url` (`match_url`(191))
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_redirection_items`
--

LOCK TABLES `wp_redirection_items` WRITE;
/*!40000 ALTER TABLE `wp_redirection_items` DISABLE KEYS */;
INSERT INTO `wp_redirection_items` VALUES (1,'/business-loans/business-credit-cards/','/business-loans/business-credit-cards',NULL,0,0,5871,'2019-08-03 14:18:43',1,'enabled','url',301,'/best-business-credit-cards/','url',NULL),(2,'/business-loans/start-up-business-loans/','/business-loans/start-up-business-loans',NULL,0,1,62,'2018-11-09 20:31:47',1,'enabled','url',301,'/business-loans/start-up/','url',NULL),(3,'/business-loans/business-line-of-credit/','/business-loans/business-line-of-credit',NULL,0,2,701,'2018-08-15 10:38:38',1,'enabled','url',301,'/business-loans/line-of-credit/','url',NULL),(4,'/loans/term-loans-for-business/','/loans/term-loans-for-business',NULL,0,3,341,'2018-08-14 01:45:00',1,'enabled','url',301,'https://www.lendgenius.com/business-loans/term-loans/','url',NULL),(5,'/loans/short-term-business-loans/','/loans/short-term-business-loans',NULL,0,4,66,'2018-08-13 16:03:01',1,'enabled','url',301,'https://www.lendgenius.com/business-loans/short-term/','url',NULL),(6,'/loans/short-term-business-loans/','/loans/short-term-business-loans',NULL,0,5,25,'2017-03-13 14:24:42',1,'enabled','url',301,'https://www.lendgenius.com/business-loans/short-term/','url',NULL),(7,'/loans/sba-loans/','/loans/sba-loans',NULL,0,6,243,'2018-08-15 11:22:21',1,'enabled','url',301,'https://www.lendgenius.com/business-loans/sba-loans/','url',NULL),(8,'/loans/merchant-cash-advance/','/loans/merchant-cash-advance',NULL,0,7,301,'2018-08-14 17:55:59',1,'enabled','url',301,'https://www.lendgenius.com/business-loans/merchant-cash-advance/','url',NULL),(9,'/loans/invoice-financing/','/loans/invoice-financing',NULL,0,8,281,'2018-08-14 00:19:40',1,'enabled','url',301,'https://www.lendgenius.com/business-loans/invoice-financing/','url',NULL),(10,'/loans/business-line-of-credit/','/loans/business-line-of-credit',NULL,0,9,239,'2018-08-15 10:38:37',1,'enabled','url',301,'https://www.lendgenius.com/business-loans/business-line-of-credit/','url',NULL),(11,'/loans/','/loans',NULL,0,10,3143,'2018-08-15 18:47:27',1,'enabled','url',301,'https://www.lendgenius.com/business-loans/','url',NULL),(12,'/home/','/home',NULL,0,11,26,'2018-08-17 17:32:10',1,'enabled','url',301,'https://www.lendgenius.com/','url',NULL),(13,'/business-loans/short-term-business-loans/','/business-loans/short-term-business-loans',NULL,0,12,59,'2018-08-13 04:31:06',1,'enabled','url',301,'https://www.lendgenius.com/business-loans/short-term/','url',NULL),(14,'/blog/loans-for-wholesaler-and-distributor-businesses/','/blog/loans-for-wholesaler-and-distributor-businesses',NULL,0,13,208,'2018-08-15 14:41:49',1,'enabled','url',301,'https://www.lendgenius.com/blog/business-loan-options-wholesale-distribution-companies/','url',NULL),(15,'/blog/10-financial-tools-that-a-small-business-owner-cant-go-without/','/blog/10-financial-tools-that-a-small-business-owner-cant-go-without',NULL,0,14,82,'2018-08-15 06:26:10',1,'enabled','url',301,'https://www.lendgenius.com/blog/10-financial-tools-no-small-business-owner-can-live-without/','url',NULL),(16,'^/get-started/.*','regex',NULL,1,15,5154,'2019-07-06 15:08:31',1,'enabled','url',301,'/','url',''),(17,'/terms-of-service/','/terms-of-service',NULL,0,16,84,'2018-08-14 06:21:21',1,'enabled','url',301,'/terms-of-use/','url',NULL),(18,'/apply/','/apply',NULL,0,17,20,'2018-07-24 03:42:11',1,'enabled','url',301,'https://www.lendgenius.com','url',''),(22,'^/blog/minority-business-loans.*','regex',NULL,1,18,147,'2018-08-15 07:41:49',1,'enabled','url',301,'a:4:{s:8:\"url_from\";s:68:\"https://www.lendgenius.com/blog/small-business-loans-for-minorities/\";s:11:\"url_notfrom\";b:0;s:5:\"regex\";b:1;s:8:\"referrer\";b:0;}','referrer',NULL),(23,'/blog/how-to-get-a-startup-business-loan/','/blog/how-to-get-a-startup-business-loan',NULL,0,19,666,'2018-08-15 19:28:55',1,'enabled','url',301,'https://www.lendgenius.com/blog/how-to-get-a-business-loan/','url',NULL),(24,'/blog/this-is-how-to-get-a-loan-bad-credit/','/blog/this-is-how-to-get-a-loan-bad-credit',NULL,0,20,629,'2018-08-14 22:53:36',1,'enabled','url',301,'https://www.lendgenius.com/blog/small-business-loans-bad-credit','url',NULL),(25,'/blog/alternative-sources-of-finances-for-small-businesses/','/blog/alternative-sources-of-finances-for-small-businesses',NULL,0,21,1159,'2018-08-15 14:03:52',1,'enabled','url',301,'https://www.lendgenius.com/blog/alternative-lending-for-small-business/','url',NULL),(26,'/blog/fastest-way-get-business-loan/','/blog/fastest-way-get-business-loan',NULL,0,22,690,'2018-08-15 14:12:58',1,'enabled','url',301,'https://www.lendgenius.com/blog/fast-business-loans/','url',NULL),(27,'/blog/quick-loans-online/','/blog/quick-loans-online',NULL,0,23,265,'2018-08-13 05:13:47',1,'enabled','url',301,'https://www.lendgenius.com/blog/quick-business-loans/','url',NULL),(28,'/business-loans/page/2/','/business-loans/page/2',NULL,0,24,465,'2019-01-24 15:36:36',1,'enabled','url',301,'https://www.lendgenius.com/business-loans/','url',NULL),(29,'/blog/business-loan-funding-for-restaurants/','/blog/business-loan-funding-for-restaurants',NULL,0,25,258,'2018-08-15 11:33:34',1,'enabled','url',301,'https://www.lendgenius.com/blog/restaurant-business-loans/','url',''),(30,'/blog/guide-to-business-loans-for-funding-a-bar/','/blog/guide-to-business-loans-for-funding-a-bar',NULL,0,26,158,'2018-08-14 03:43:22',1,'enabled','url',301,'https://www.lendgenius.com/blog/loan-for-bar/','url',''),(32,'/how-it-works/','/how-it-works',NULL,0,27,64,'2019-08-03 01:58:00',1,'enabled','url',301,'/#how-it-works-box','url',''),(33,'/getstarted/','/getstarted',NULL,0,28,4,'2019-06-06 09:13:49',1,'disabled','url',301,'/loan-request/','url',''),(34,'/blog/minorities-women-and-veteran-business-owners-heres-how-to-get-extra-funding-for-your-business/','/blog/minorities-women-and-veteran-business-owners-heres-how-to-get-extra-funding-for-your-business',NULL,0,29,11,'2019-07-22 06:24:24',1,'enabled','url',301,'/blog/minorities-women-veterans-business-loans/','url',''),(35,'/blog/capital-one-venture-card/','/blog/capital-one-venture-card',NULL,0,30,1,'2019-07-30 13:12:51',1,'enabled','url',301,'/blog/capital-one-venture-one-card/','url',''),(36,'/blog/jetblue-credit-card/','/blog/jetblue-credit-card',NULL,0,31,2,'2019-08-01 12:14:37',1,'enabled','url',301,'/blog/jet-blue-credit-card/','url','');
/*!40000 ALTER TABLE `wp_redirection_items` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-01 18:49:42
