-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: lendgenius_dev
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wp_spectrom_sync_log`
--

DROP TABLE IF EXISTS `wp_spectrom_sync_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_spectrom_sync_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `push_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `operation` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `source_user` bigint(20) unsigned NOT NULL,
  `source_site` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `source_site_key` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target_user` bigint(20) unsigned NOT NULL,
  `type` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'recv',
  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_spectrom_sync_log`
--

LOCK TABLES `wp_spectrom_sync_log` WRITE;
/*!40000 ALTER TABLE `wp_spectrom_sync_log` DISABLE KEYS */;
INSERT INTO `wp_spectrom_sync_log` VALUES (1,374,'9 Gmail Plugins and Accessories Your Business Needs Now','2016-11-23 14:05:36','push',0,'https://lendgenius.com','e947775ab509bd1da73f0aac3ed7b8f9',0,'recv');
/*!40000 ALTER TABLE `wp_spectrom_sync_log` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-01 18:51:05
