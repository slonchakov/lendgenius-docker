-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: lendgenius_dev
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wp_users`
--

DROP TABLE IF EXISTS `wp_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_users`
--

LOCK TABLES `wp_users` WRITE;
/*!40000 ALTER TABLE `wp_users` DISABLE KEYS */;
INSERT INTO `wp_users` VALUES (2,'lendgenius','$P$Btlah.Lt2ldlU3BHuZcPJ0d3GT2SN4.','ronis-gracie','ronis@leadsmarket.com','http://lendgenius.com','2016-07-25 18:59:14','',0,'Ronis Gracie'),(5,'Eric','$P$B7OF8owr4FuqWr89OJ.DJDZmFGXAgB0','eric','ericgoldschein@gmail.com','http://ericgoldschein.com/','2017-01-11 22:06:26','',0,'Eric Goldschein'),(6,'Amy','$P$B5H2ui8KUuQ0ZNSfJHnJN8e/fJEVwa0','amy','alfontin@gmail.com','http://www.amyfontinelle.com/','2017-01-16 18:48:04','',0,'Amy Fontinelle'),(7,'LaurenWard','$P$B3cBxV.AXub4Yl77wcAyPcbSvVKIm5/','laurenward','laurenalexandra@gmail.com','','2017-01-16 18:56:27','',0,'Lauren Ward'),(11,'DClendenen','$P$BcLJtAQMPE0LW/VeuSNwmthjMaG35e.','dclendenen','dustin.clendenen@gmail.com','','2017-01-30 18:47:25','',0,'Dustin Clendenen'),(12,'Jackie Lam','$P$B2U2wIZ09mgplFRGAf7xmC5TU.fsaR.','jackie-lam','jackie@cheapsters.org','','2017-02-07 00:21:42','',0,'Jackie Lam'),(13,'Brenna Lemieux','$P$BQFdtBQkiT8zTImmob47ZmVsgH/aNy0','brenna-lemieux','brenna.lemieux@gmail.com','','2017-02-07 01:35:24','',0,'Brenna Lemieux'),(16,'andywang','$P$BqCdIqJs7EYXNZpJVDa/N3gJRW8GY3.','andywang','andy@lendgenius.com','','2017-03-15 22:30:08','',0,'Andy Wang'),(19,'sergey.s','$P$BLcvp0P4PDtzgbdGc4LLEf8C3scrWA0','sergey-s','sergey.s@lendgenius.com','','2017-07-03 15:12:07','1504208406:$P$BQHo0x/dl36LOq2h0aTFv1YpSuB6fR1',0,'Sergey Slonchakov'),(23,'marco@leadsmarket.com','$P$B.oobxyXRLpBjjxcNAABucMrEQim/1/','marco-ponce','marco@leadsmarket.com','','2018-02-13 01:24:44','1518485091:$P$BlZ608L9axsd4x7ENgWjSIz6gX28nA0',0,'Marco Ponce'),(24,'casey@leadsmarket.com','$P$BXnLQdxkTmBMnx3wX8iV6NgKcp48Wk1','caseyleadsmarket-com','casey@leadsmarket.com','','2018-02-13 01:27:50','',0,'Casey Rondinella'),(25,'admin','$P$BjlaZx.ETu.4d0m.3Ey0Ov7E/c6uqC.','admin','photo@mikeorlov.com','','2018-12-13 14:22:02','',0,'admin'),(26,'admin2','$P$BXdB13yFG3N3ooTRpFNensyaYFNnQN0','admin2','andy@leadsmarket.com','','2018-12-13 14:22:02','',0,'admin2');
/*!40000 ALTER TABLE `wp_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-01 18:52:27
