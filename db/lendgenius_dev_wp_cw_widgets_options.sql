-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: lendgenius_dev
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wp_cw_widgets_options`
--

DROP TABLE IF EXISTS `wp_cw_widgets_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_cw_widgets_options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `widget` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `show_widget_options` text NOT NULL,
  `hide_widget_options` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_cw_widgets_options`
--

LOCK TABLES `wp_cw_widgets_options` WRITE;
/*!40000 ALTER TABLE `wp_cw_widgets_options` DISABLE KEYS */;
INSERT INTO `wp_cw_widgets_options` VALUES (1,'custom_html-2',NULL,'{\"identifier\":\"show_widget_options\",\"tabs\":[{\"identifier\":\"other_screens\",\"label\":\"Basic Settings\",\"isActive\":false,\"options\":[]},{\"identifier\":\"taxonomy_terms\",\"label\":\"Categories etc.\",\"isActive\":false,\"options\":[]},{\"identifier\":\"posts\",\"label\":\"Posts\",\"isActive\":false,\"options\":[2620,787,1333,1997,1454,2334,2055,368,2613,366,499,706,775,2773,745,793,1308,779,698,737,781,2153,777,1004,729,1376,2619,2211,1941,1281,1077,1742,2349,1370,1365,2780,1309,3145,3143,3202,3200,3144,1992,1940,566,541,2985,733,2237,3197,2978,1528,1314,687,1453,3203,2189,739,1591,1734,534,2827,3190,715,1378,3204,1989,3031,2977,766,3023,1730,1723,581,575,538,572,556,550,553,750,376,1938,748,3021,3032,1507,3027,2544,2965,3033,3034,1744,3159,3194,3205,677,1556,2197,382,2988,704,2991,2749,1635,717,3035,1939,2809,2995,791,2218,3030,3106,1427,3028,4293,722,2336,1437,2347,1637,1990,1995,2052,544,559,578,1508,2335,2853,734,2993,1320,2616,3147,2825,2209,758,1743,854,785,1377,1538,1367,1443,742,724,2734,563,2332,370,768,789,1526,1409,502,755,3022,605,1525,1501,2963,608,700,2543,2962,3142,1809,1942,378,1408,2811,2191,770,569,842,2737,2770,2820,2612,2217,1994,1536,1597,3037,384,1524,1537,372,763,2994,1991,2828,2226,2152,3038,2969,2821,2839,3213,1741,1598,1596,2155,1811,783,547,2617,2986,2017,1993,3222,2829,1505,727,2810,1436,1585,1636,1519,1527,1374,1442,696,1813,3148,2992,1557,2333,3029,1996,3039,1943,2989,772,1523,3158,2614,3160,2976,2615,2287,3041,3146,3019,2817,2964,3036,526,3016,2348,752,3018,396,3214,1319,3161,1782,1379,1375,680,3020,1522,1410,2781,2806,3025,1740,2227,3040,702,3199,1558,3216,1455,2687,3149,998,1456,1271,3150,719]},{\"identifier\":\"pages\",\"label\":\"Pages\",\"isActive\":false,\"options\":[]},{\"identifier\":\"page_templates\",\"label\":\"Page Templates\",\"isActive\":false,\"options\":[]},{\"identifier\":\"post_types\",\"label\":\"Post Types\",\"isActive\":false,\"options\":[]},{\"identifier\":\"taxonomy_archives\",\"label\":\"Archive Pages\",\"isActive\":false,\"options\":[]},{\"identifier\":\"others\",\"label\":\"Other\",\"isActive\":false,\"options\":[]}]}','{\"identifier\":\"hide_widget_options\",\"tabs\":[{\"identifier\":\"other_screens\",\"label\":\"Basic Settings\",\"isActive\":false,\"options\":[]},{\"identifier\":\"taxonomy_terms\",\"label\":\"Categories etc.\",\"isActive\":false,\"options\":[]},{\"identifier\":\"posts\",\"label\":\"Posts\",\"isActive\":false,\"options\":[]},{\"identifier\":\"pages\",\"label\":\"Pages\",\"isActive\":false,\"options\":[]},{\"identifier\":\"page_templates\",\"label\":\"Page Templates\",\"isActive\":false,\"options\":[]},{\"identifier\":\"post_types\",\"label\":\"Post Types\",\"isActive\":false,\"options\":[]},{\"identifier\":\"taxonomy_archives\",\"label\":\"Archive Pages\",\"isActive\":false,\"options\":[]},{\"identifier\":\"others\",\"label\":\"Other\",\"isActive\":false,\"options\":[]}]}'),(2,'custom_html-3',NULL,'{\"identifier\":\"show_widget_options\",\"tabs\":[{\"identifier\":\"other_screens\",\"label\":\"Basic Settings\",\"isActive\":false,\"options\":[]},{\"identifier\":\"taxonomy_terms\",\"label\":\"Categories etc.\",\"isActive\":false,\"options\":[]},{\"identifier\":\"posts\",\"label\":\"Posts\",\"isActive\":false,\"options\":[2051]},{\"identifier\":\"pages\",\"label\":\"Pages\",\"isActive\":false,\"options\":[]},{\"identifier\":\"page_templates\",\"label\":\"Page Templates\",\"isActive\":false,\"options\":[]},{\"identifier\":\"post_types\",\"label\":\"Post Types\",\"isActive\":false,\"options\":[]},{\"identifier\":\"taxonomy_archives\",\"label\":\"Archive Pages\",\"isActive\":false,\"options\":[]},{\"identifier\":\"others\",\"label\":\"Other\",\"isActive\":false,\"options\":[]}]}','{\"identifier\":\"hide_widget_options\",\"tabs\":[{\"identifier\":\"other_screens\",\"label\":\"Basic Settings\",\"isActive\":false,\"options\":[]},{\"identifier\":\"taxonomy_terms\",\"label\":\"Categories etc.\",\"isActive\":false,\"options\":[]},{\"identifier\":\"posts\",\"label\":\"Posts\",\"isActive\":false,\"options\":[]},{\"identifier\":\"pages\",\"label\":\"Pages\",\"isActive\":false,\"options\":[]},{\"identifier\":\"page_templates\",\"label\":\"Page Templates\",\"isActive\":false,\"options\":[]},{\"identifier\":\"post_types\",\"label\":\"Post Types\",\"isActive\":false,\"options\":[]},{\"identifier\":\"taxonomy_archives\",\"label\":\"Archive Pages\",\"isActive\":false,\"options\":[]},{\"identifier\":\"others\",\"label\":\"Other\",\"isActive\":false,\"options\":[]}]}');
/*!40000 ALTER TABLE `wp_cw_widgets_options` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-01 18:52:22
