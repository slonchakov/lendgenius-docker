-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: lendgenius_dev
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wp_srty_links`
--

DROP TABLE IF EXISTS `wp_srty_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_srty_links` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `link_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `destination_url` varchar(2083) COLLATE utf8mb4_unicode_ci NOT NULL,
  `backup_url` varchar(2083) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_url` varchar(2083) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tracking_link` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_redirect_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cloaking_status_enable` tinyint(4) NOT NULL,
  `cloaking_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bar_position` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `frame_content` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `retargeting_code` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `param_tag_forward_param` tinyint(4) NOT NULL DEFAULT '0',
  `param_tag_forward_campaign` tinyint(4) NOT NULL DEFAULT '0',
  `param_tag_affiliate_tracking` tinyint(4) NOT NULL DEFAULT '0',
  `param_tag_affiliate_network` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `param_tag_affiliate_network_custom` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `auto_keyword_linking_enable` tinyint(4) NOT NULL DEFAULT '0',
  `meta_keyword` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `geo_redirect_enable` tinyint(4) NOT NULL DEFAULT '0',
  `geo_redirect_option` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ALL_EXCEPT',
  `geo_redirect_countries` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `geo_redirect_destination_url` varchar(2083) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_expired_enable` tinyint(4) NOT NULL DEFAULT '0',
  `link_expired_date` datetime DEFAULT NULL,
  `link_expired_url` varchar(2083) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reference_tags` varchar(2083) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uptime_monitoring_enabled` tinyint(4) NOT NULL,
  `uptime_is_online` tinyint(4) NOT NULL DEFAULT '1',
  `uptime_last_check` datetime DEFAULT NULL,
  `click_limiter_enable` tinyint(4) NOT NULL,
  `click_limiter_max_clicks` int(11) NOT NULL,
  `click_lifetime_clicks` int(11) NOT NULL DEFAULT '0',
  `click_limiter_url` varchar(2083) COLLATE utf8mb4_unicode_ci NOT NULL,
  `blank_referrer` tinyint(4) NOT NULL DEFAULT '0',
  `retargeting_adwords` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `retargeting_fb` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `retargeting_adroll` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `retargeting_perfect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `retargeter_code` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `param_tag_retargeting` tinyint(4) NOT NULL DEFAULT '0',
  `split_test_enable` tinyint(4) NOT NULL DEFAULT '0',
  `split_test_data` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_srty_links`
--

LOCK TABLES `wp_srty_links` WRITE;
/*!40000 ALTER TABLE `wp_srty_links` DISABLE KEYS */;
INSERT INTO `wp_srty_links` VALUES (1,'balboa','https://www.balboacapital.com/equipment-leasing/lend-genius/','','https://www.balboacapital.com/equipment-leasing/lend-genius/','balboa','',0,'Basic Cloaking','Page Top','NOINDEX','','','','',0,0,0,'aff_sub','',0,'',0,'ALL_EXCEPT','','',0,'0000-00-00 00:00:00','','',0,1,NULL,0,0,358,'',0,'','','','','',0,0,'{\"allocation\":[{\"destination_url\":\"https:\\/\\/www.balboacapital.com\\/equipment-leasing\\/lend-genius\\/\",\"weight\":\"\"}]}'),(2,'balboacapital','https://www.balboacapital.com/business-loan-application/lend-genius/','','https://www.balboacapital.com/business-loan-application/lend-genius/','balboacapital','',0,'Basic Cloaking','Page Top','NOINDEX','','','','',0,0,0,'aff_sub','',0,'',0,'ALL_EXCEPT','','',0,'0000-00-00 00:00:00','','',0,1,NULL,0,0,1261,'',0,'','','','','',0,0,'{\"allocation\":[{\"destination_url\":\"https:\\/\\/www.balboacapital.com\\/business-loan-application\\/lend-genius\\/\",\"weight\":\"\"}]}'),(3,'credibly','https://partner.credibly.com/lend-genius','','https://partner.credibly.com/lend-genius','credibly','',0,'Basic Cloaking','Page Top','NOINDEX','','','','',0,0,0,'aff_sub','',0,'',0,'ALL_EXCEPT','','',0,'0000-00-00 00:00:00','','',0,1,NULL,0,0,1687,'',0,'','','','','',0,0,'{\"allocation\":[{\"destination_url\":\"https:\\/\\/partner.credibly.com\\/lend-genius\",\"weight\":\"\"}]}'),(4,'financefactory','https://lp.thefinancefactory.com/lp/r/1373','','https://lp.thefinancefactory.com/lp/r/1373','financefactory','',0,'Basic Cloaking','Page Top','NOINDEX','','','','',0,0,0,'aff_sub','',0,'',0,'ALL_EXCEPT','','',0,'0000-00-00 00:00:00','','',0,1,NULL,0,0,0,'',0,'','','','','',0,0,'{\"allocation\":[{\"destination_url\":\"https:\\/\\/lp.thefinancefactory.com\\/lp\\/r\\/1373\",\"weight\":\"\"}]}'),(5,'fundbox','https://fbx.go2cloud.org/aff_c?offer_id=10&aff_id=1600','','https://fbx.go2cloud.org/aff_c?offer_id=10&aff_id=1600','fundbox','',0,'Basic Cloaking','Page Top','NOINDEX','','','','',0,0,0,'aff_sub','',0,'',0,'ALL_EXCEPT','','',0,'0000-00-00 00:00:00','','',0,1,NULL,0,0,754,'',0,'','','','','',0,0,'{\"allocation\":[{\"destination_url\":\"https:\\/\\/fbx.go2cloud.org\\/aff_c?offer_id=10&aff_id=1600\",\"weight\":\"\"}]}'),(6,'headway','https://www.headwaycapital.com/apply?source=lend_genius','','https://www.headwaycapital.com/apply?source=lend_genius','headway','',0,'Basic Cloaking','Page Top','NOINDEX','','','','',0,0,0,'aff_sub','',0,'',0,'ALL_EXCEPT','','',0,'0000-00-00 00:00:00','','',0,1,NULL,0,0,142,'',0,'','','','','',0,0,'{\"allocation\":[{\"destination_url\":\"https:\\/\\/www.headwaycapital.com\\/apply?source=lend_genius\",\"weight\":\"\"}]}'),(7,'lendistry','https://lendistry.com/lend-genius/?utm_source=LendGenius&utm_campaign=234&utm_medium=affiliate','','https://lendistry.com/lend-genius/?utm_source=LendGenius&utm_campaign=234&utm_medium=affiliate','lendistry','',0,'Basic Cloaking','Page Top','NOINDEX','','','','',0,0,0,'aff_sub','',0,'',0,'ALL_EXCEPT','','',0,'0000-00-00 00:00:00','','',0,1,NULL,0,0,858,'',0,'','','','','',0,0,'{\"allocation\":[{\"destination_url\":\"https:\\/\\/lendistry.com\\/lend-genius\\/?utm_source=LendGenius&utm_campaign=234&utm_medium=affiliate\",\"weight\":\"\"}]}'),(8,'ondeck','https://www.ondeck.com/lendgenius','','https://www.ondeck.com/lendgenius','ondeck','',0,'Basic Cloaking','Page Top','NOINDEX','','','','',0,0,0,'aff_sub','',0,'',0,'ALL_EXCEPT','','',0,'0000-00-00 00:00:00','','',0,1,NULL,0,0,871,'',0,'','','','','',0,0,'{\"allocation\":[{\"destination_url\":\"https:\\/\\/www.ondeck.com\\/lendgenius\",\"weight\":\"\"}]}'),(9,'quarterspot','https://www.quarterspot.com/Apply?aid=LGEN','','https://www.quarterspot.com/Apply?aid=LGEN','quarterspot','',0,'Basic Cloaking','Page Top','NOINDEX','','','','',0,0,0,'aff_sub','',0,'',0,'ALL_EXCEPT','','',0,'0000-00-00 00:00:00','','',0,1,NULL,0,0,235,'',0,'','','','','',0,0,'{\"allocation\":[{\"destination_url\":\"https:\\/\\/www.quarterspot.com\\/Apply?aid=LGEN\",\"weight\":\"\"}]}'),(10,'seekcapital','https://seekcapital.com/lplg/','','https://seekcapital.com/lplg/','seekcapital','',0,'Basic Cloaking','Page Top','NOINDEX','','','','',0,0,0,'aff_sub','',0,'',0,'ALL_EXCEPT','','',0,'0000-00-00 00:00:00','','',0,1,NULL,0,0,995,'',0,'','','','','',0,0,'{\"allocation\":[{\"destination_url\":\"https:\\/\\/seekcapital.com\\/lplg\\/\",\"weight\":\"\"}]}'),(12,'bluehost','https://www.bluehost.com/track/mapmedia/lg','','https://www.bluehost.com/track/mapmedia/lg','bluehost','',0,'Basic Cloaking','Page Top','NOINDEX','','','','',0,0,0,'aff_sub','',0,'',0,'ALL_EXCEPT','','',0,'0000-00-00 00:00:00','','',0,1,NULL,0,0,224,'',0,'','','','','',0,0,'{\"allocation\":[{\"destination_url\":\"https:\\/\\/www.bluehost.com\\/track\\/mapmedia\\/lg\",\"weight\":\"\"}]}'),(13,'ipage','https://www.ipage.com/join/index.bml?AffID=781365&LinkName=LG','','https://www.ipage.com/join/index.bml?AffID=781365&LinkName=LG','ipage','',0,'Basic Cloaking','Page Top','NOINDEX','','','','',0,0,0,'aff_sub','',0,'',0,'ALL_EXCEPT','','',0,'0000-00-00 00:00:00','','',0,1,NULL,0,0,193,'',0,'','','','','',0,0,'{\"allocation\":[{\"destination_url\":\"https:\\/\\/www.ipage.com\\/join\\/index.bml?AffID=781365&LinkName=LG\",\"weight\":\"\"}]}'),(14,'imh','https://inmotion-hosting.evyy.net/c/192237/260033/4222?subId1=LG&u=https%3A%2F%2Fwww.inmotionhosting.com%2Fbusiness-hosting','','https://inmotion-hosting.evyy.net/c/192237/260033/4222?subId1=LG&u=https%3A%2F%2Fwww.inmotionhosting.com%2Fbusiness-hosting','inmotion-hosting','',0,'Basic Cloaking','Page Top','NOINDEX','','','','',0,0,0,'aff_sub','',0,'',0,'ALL_EXCEPT','','',0,'0000-00-00 00:00:00','','',0,1,NULL,0,0,138,'',0,'','','','','',0,0,'{\"allocation\":[{\"destination_url\":\"https:\\/\\/inmotion-hosting.evyy.net\\/c\\/192237\\/260033\\/4222?subId1=LG&u=https%3A%2F%2Fwww.inmotionhosting.com%2Fbusiness-hosting\",\"weight\":\"\"}]}'),(15,'imh email','https://inmotion-hosting.evyy.net/c/192237/260033/4222?subId1=LGE&u=https%3A%2F%2Fwww.inmotionhosting.com%2Fbusiness-hosting','','https://inmotion-hosting.evyy.net/c/192237/260033/4222?subId1=LGE&u=https%3A%2F%2Fwww.inmotionhosting.com%2Fbusiness-hosting','inmotion-email','',0,'Basic Cloaking','Page Top','NOINDEX','','','','',0,0,0,'aff_sub','',0,'',0,'ALL_EXCEPT','','',0,'0000-00-00 00:00:00','','',0,1,NULL,0,0,508,'',0,'','','','','',0,0,'{\"allocation\":[{\"destination_url\":\"https:\\/\\/inmotion-hosting.evyy.net\\/c\\/192237\\/260033\\/4222?subId1=LGE&u=https%3A%2F%2Fwww.inmotionhosting.com%2Fbusiness-hosting\",\"weight\":\"100\"}]}'),(16,'aweber','http://www.aweber.com/easy-email.htm?id=462433','','http://www.aweber.com/easy-email.htm?id=462433','aweber','',0,'Basic Cloaking','Page Top','NOINDEX','','','','',0,0,0,'aff_sub','',0,'',0,'ALL_EXCEPT','','',0,'0000-00-00 00:00:00','','',0,1,NULL,0,0,107,'',0,'','','','','',0,0,'{\"allocation\":[{\"destination_url\":\"http:\\/\\/www.aweber.com\\/easy-email.htm?id=462433\",\"weight\":\"\"}]}'),(17,'getresponse','https://www.getresponse.com/?a=mapmedia&c=lg','','https://www.getresponse.com/?a=mapmedia&c=lg','getresponse','',0,'Basic Cloaking','Page Top','NOINDEX','','','','',0,0,0,'aff_sub','',0,'',0,'ALL_EXCEPT','','',0,'0000-00-00 00:00:00','','',0,1,NULL,0,0,105,'',0,'','','','','',0,0,'{\"allocation\":[{\"destination_url\":\"https:\\/\\/www.getresponse.com\\/?a=mapmedia&c=lg\",\"weight\":\"\"}]}'),(18,'convertkit','https://mbsy.co/convertkit/34460509','','https://mbsy.co/convertkit/34460509','convertkit','',0,'Basic Cloaking','Page Top','NOINDEX','','','','',0,0,0,'aff_sub','',0,'',0,'ALL_EXCEPT','','',0,'0000-00-00 00:00:00','','',0,1,NULL,0,0,112,'',0,'','','','','',0,0,'{\"allocation\":[{\"destination_url\":\"https:\\/\\/mbsy.co\\/convertkit\\/34460509\",\"weight\":\"\"}]}'),(19,'Lendio','https://www.lendio.com/partner/lendgenius','','https://www.lendio.com/partner/lendgenius','lendio','',0,'Basic Cloaking','Page Top','NOINDEX','','','','',0,0,0,'aff_sub','',0,'',0,'ALL_EXCEPT','','',0,'0000-00-00 00:00:00','','',0,1,NULL,0,0,489,'',0,'','','','','',0,0,'{\"allocation\":[{\"destination_url\":\"https:\\/\\/www.lendio.com\\/partner\\/lendgenius\",\"weight\":\"\"}]}');
/*!40000 ALTER TABLE `wp_srty_links` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-01 18:50:00
