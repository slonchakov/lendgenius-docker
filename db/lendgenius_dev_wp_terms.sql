-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: lendgenius_dev
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wp_terms`
--

DROP TABLE IF EXISTS `wp_terms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_terms`
--

LOCK TABLES `wp_terms` WRITE;
/*!40000 ALTER TABLE `wp_terms` DISABLE KEYS */;
INSERT INTO `wp_terms` VALUES (1,'blog','blog',0),(2,'Header-Menu','header-menu',0),(3,'Footer-Menu1','footer-menu1',0),(5,'Footer-Menu2','footer-menu2',0),(6,'Main-Menu','main-menu',0),(7,'lendgenius','lendgenius',0),(9,'Blog Side Bar Menu','blog-side-bar-menu',0),(11,'loan products','loan-products',0),(12,'Router','router',0),(13,'Uncategorized','uncategorized',0),(14,'Credit Cards','credit-cards',0),(16,'Business Loans','business-loans',0),(17,'Right menu','right-menu',0),(18,'New footer menu','new-footer-menu',0),(19,'Headers','headers',0),(20,'Footers','footers',0),(21,'Hosting','hosting',0),(22,'Cash Advance','cash-advance',0),(23,'Installment Loans','installment-loans',0),(24,'Loan Types','loan-types',0),(25,'Payday Loans','payday-loans',0),(26,'Personal Loans','personal-loans',0),(27,'Title Loans','title-loans',0),(28,'Uncategorized','uncategorized',0),(29,'applying for a loan','applying-for-a-loan',0),(30,'Car Title Loan','car-title-loan',0),(31,'cash advance','cash-advance',0),(32,'cash loan','cash-loan',0),(33,'credit card advance','credit-card-advance',0),(34,'easy money','easy-money',0),(35,'financing','financing',0),(36,'installment loan','installment-loan',0),(37,'instant money','instant-money',0),(38,'loan approval','loan-approval',0),(39,'loan status','loan-status',0),(40,'loan type','loan-type',0),(41,'loans','loans',0),(42,'merchant cash advance','merchant-cash-advance',0),(43,'payday lenders','payday-lenders',0),(44,'payday loan','payday-loan',0),(45,'quick money','quick-money',0),(46,'repayment options','repayment-options',0),(47,'repayment period','repayment-period',0),(48,'Title Loans','title-loans',0),(49,'type of loan','type-of-loan',0),(50,'type of loans','type-of-loans',0),(51,'types of cash advances','types-of-cash-advances',0),(52,'types of loans','types-of-loans',0),(53,'footer-menu','footer-menu',0);
/*!40000 ALTER TABLE `wp_terms` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-01 18:50:09
